﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Nop.Plugin.Payments.AuthorizeNet;

namespace Nop.Tests
{
	[TestFixture]
	public class AuthorizeNetTests
	{
		[TestCase("4500park Glen Road", Result = "4500park Glen Road")]
		[TestCase("123 Oak St", Result = "123 Oak St")]
		[TestCase("123 45th St", Result = "123 street")]
		[TestCase("		 123		  45th St", Result = "123 street")]
		[TestCase("   123   ", Result = "123")]
		[TestCase("my street", Result = "my street")]
		public string FixAddressForAvs_works(string addr) {
			return AuthorizeNetPaymentProcessor.FixAddressForAvs(addr);
		}
	}
}
