﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201408191234)]
    public class SavedSearches : Migration
    {
        class SavedSearch
        {
            [Medium, NotNull] string EmailAddress;
            [Short, NotNull] string Frequency;
            [Short, NotNull] string Duration;
            [Max, NotNull] string SearchParameters;
            DateTime StartDate;
            DateTime EndDate;
            int SiteId;
        }

        public override void Up() 
        {
            Create.Table<SavedSearch>();
        }

        public override void Down()
        {
            Delete.Table<SavedSearch>();
        }
    }
}
