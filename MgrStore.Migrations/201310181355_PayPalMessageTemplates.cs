﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201310181355)]
    public class PayPalMessageTemplates : Migration
    {
        public override void Up()
        {
			Execute.Sql(@"
				INSERT INTO MessageTemplate (Name, Subject, Body, IsActive, EmailAccountId)
				SELECT
					'Store.PayPalComplete',
					'PayPal payment completed for Order #%Order.OrderNumber%',
					REPLACE(Body, 'has just placed', 'has just paid for'),
					1, 1
				FROM MessageTemplate WHERE Name = 'OrderPlaced.StoreOwnerNotification'");

	        Execute.Sql(@"
				INSERT INTO MessageTemplate (Name, Subject, Body, IsActive, EmailAccountId)
				SELECT
					'Store.PayPalCanceled',
					'PayPal payment canceled for Order #%Order.OrderNumber%',
					REPLACE(Body, 'has just placed', 'has just canceled payment for'),
					1, 1
				FROM MessageTemplate WHERE Name = 'OrderPlaced.StoreOwnerNotification'");
        }

        public override void Down() {
			Execute.Sql("DELETE FROM MessageTemplate WHERE Name IN ('Store.PayPalComplete', 'Store.PayPalCanceled')");
        }
    }
}
