﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201401061509)]
	public class AddProductClearanceLabel : Migration
	{
		public override void Up()
		{
			Insert.IntoTable("LocaleStringResource")
				.Row(new {
					LanguageId = 1,
					ResourceName = "Admin.Catalog.Products.Variants.Fields.Clearance",
					ResourceValue = "Clearance"
				})
				.Row(new {
					LanguageId = 1,
					ResourceName = "Admin.Catalog.Products.Variants.Fields.Clearance.Hint",
					ResourceValue = "Mark a product as being on clearance."
				});
		}


		public override void Down()
		{
			Delete.FromTable("LocaleStringResource")
				.Row(new {LanguageId = 1, ResourceName = "Admin.Catalog.Products.Variants.Fields.Clearance"})
				.Row(new {LanguageId = 1, ResourceName = "Admin.Catalog.Products.Variants.Fields.Clearance.Hint"});
		}
	}
}
