﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;


namespace MgrStore.Migrations
{
    [Migration(201309191555)]
    public class AddReedsCategory : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("Category").Row(new
            {
                Name = "Accessories - Reeds",
                CategoryTemplateId = 1,
                ParentCategoryId = 24,
                PageSize = 4,
                AllowCustomersToSelectPageSize = 1,
                PriceRanges = "-25;25-50;50-",
                Published = 1,
                CreatedOnUtc = DateTime.UtcNow,
                UpdatedOnUtc = DateTime.UtcNow,
                DrsCode = "ACRD",
                PictureId = 0,
                ShowOnHomePage = 0,
                HasDiscountsApplied = 0,
                Deleted = 0,
                DisplayOrder = 0
            });
        }

        public override void Down()
        {
            Execute.Sql(
@"DELETE Product_Category_Mapping
FROM Product_Category_Mapping pcm
INNER JOIN Category c ON c.Id=pcm.CategoryId
WHERE c.DrsCode='ACRD'");

            Delete.FromTable("Category").Row(new { DrsCode = "ACRD" });
        }
    }
}
