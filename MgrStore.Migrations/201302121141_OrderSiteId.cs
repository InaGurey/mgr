﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
	[Migration(201302121141)]
	public class OrderSiteId : AutoReversingMigration
	{
		public override void Up() {
			Create.Column("SiteId").OnTable("Order").AsInt32().NotNullable().Indexed("IX_Order_SiteId");
		}
	}
}
