﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgrStore.Migrations
{
    [Migration(201609021026)]
    public class DeleteProductsInCategory : Migration
    {
        public override void Up()
        {

            Delete.FromTable("Category")
                .Row(new {Id = 46, Name = "Violins, Violas Or Cellos"});
            Delete.FromTable("Category")
                .Row(new { Id = 69, Name = "Ethnic Percussion" });

        }

        public override void Down()
        {


            Insert.IntoTable("Category")
                .Row(new
                {
                    Id = 46,
                    Name = "Violins, Violas Or Cellos",

                });
            Insert.IntoTable("Category")
                .Row(new
                {
                    Id = 69,
                    Name = "Ethnic Percussion",
                });
        }
    }
}
