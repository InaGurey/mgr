﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
	[Migration(201302121420)]
	public class CustomerSiteId : Migration
	{
		public override void Up() {
			Create.Column("SiteId").OnTable("Customer").AsInt32().Nullable();
			Update.Table("Customer").Set(new { SiteId = 0 }).AllRows();
			Alter.Column("SiteId").OnTable("Customer").AsInt32().NotNullable().Indexed("IX_Customer_SiteId");
		}

		public override void Down() {
			Delete.Index("IX_Customer_SiteId").OnTable("Customer");
			Delete.Column("SiteId").FromTable("Customer");
		}
	}
}
