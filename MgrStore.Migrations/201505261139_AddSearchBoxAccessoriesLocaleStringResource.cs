﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201505261139)]
    public class AddSearchBoxAccessoriesLocaleStringResource : Migration
    {
        private const int languageId = 1;
        private const string localeStringResourceTableName = "LocaleStringResource";

        private const string searchBoxTooltipCorporateKey = "Search.SearchBox.Tooltip.Corporate";
        private const string searchBoxTooltipFranchiseeKey = "Search.SearchBox.Tooltip.Franchisee";
        private const string relatedAccessoriesKey = "Products.RelatedAccessories";

        public override void Up()
        {
            Insert.IntoTable(localeStringResourceTableName)
                .Row(
                    new
                    {
                        LanguageId = languageId,
                        ResourceName = searchBoxTooltipCorporateKey,
                        ResourceValue = "Search All Stores"
                    });

            Insert.IntoTable(localeStringResourceTableName)
                .Row(
                    new
                    {
                        LanguageId = languageId,
                        ResourceName = searchBoxTooltipFranchiseeKey,
                        ResourceValue = "Search Our Store"
                    });

            Insert.IntoTable(localeStringResourceTableName)
                .Row(
                    new
                    {
                        LanguageId = languageId,
                        ResourceName = relatedAccessoriesKey,
                        ResourceValue = "Related Accessories"
                    });
        }

        public override void Down()
        {
            Delete.FromTable(localeStringResourceTableName)
                .Row(new { LanguageId = languageId, ResourceName = searchBoxTooltipCorporateKey });

            Delete.FromTable(localeStringResourceTableName)
                .Row(new { LanguageId = languageId, ResourceName = searchBoxTooltipFranchiseeKey });

            Delete.FromTable(localeStringResourceTableName)
                .Row(new { LanguageId = languageId, ResourceName = relatedAccessoriesKey });
        }
    }
}
