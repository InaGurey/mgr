﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201304151055)]
    public class AddClearanceColumn : Migration
    {
        public override void Up()
        {
            Create.Column("Clearance").OnTable("ProductVariant").AsBoolean().WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Column("Clearance").FromTable("ProductVariant");
        }
    }
}
