﻿using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201211201123)]
	public class AddCategoryCode : Migration
	{
		public override void Up()
		{
			Create.Column("DrsCode").OnTable("Category")
			      .AsString(50).Nullable();
		}


		public override void Down()
		{
			Delete.Column("DrsCode").FromTable("Category");
		}
	}
}