﻿using System;
using FluentMigrator;

namespace MgrStore.Migrations
{
	[Migration(201308091444)]
	public class IndexProductStoreNumber : Migration
	{
		public override void Up() {
			Execute.Sql("CREATE NONCLUSTERED INDEX IX_Product_StoreNumber ON Product (StoreNumber) WITH FILLFACTOR = 90");
		}

		public override void Down() {
			Delete.Index("IX_Product_StoreNumber").OnTable("Product");
		}
	}
}
