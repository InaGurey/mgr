﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyMigrator;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201505111523)]
    public class UpsShippingAddOversizePackageResidentialAddressField : Migration
    {
        class UpsShipment
        {
            private bool? OversizePackage;
            private bool? ResidentialAddress;
        }

        public override void Up()
        {
            Create.Columns<UpsShipment>();
        }

        public override void Down()
        {
            Delete.Columns<UpsShipment>();
        }
    }
}
