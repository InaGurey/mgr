﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201505220928)]
    public class ProductSortingEnumChange : Migration
    {
        private const string nameAscKey = "Enums.Nop.Core.Domain.Catalog.ProductSortingEnum.NameAsc";
        private const string nameDescKey = "Enums.Nop.Core.Domain.Catalog.ProductSortingEnum.NameDesc";
        private const string oldNewKey = "Enums.Nop.Core.Domain.Catalog.ProductSortingEnum.DateListed";

        public override void Up()
        {
            Update.Table("LocaleStringResource").Set(new { ResourceValue = "Product Name: A to Z" }).Where(new { ResourceName = nameAscKey });
            Update.Table("LocaleStringResource").Set(new { ResourceValue = "Product Name: Z to A" }).Where(new { ResourceName = nameDescKey });
            Update.Table("LocaleStringResource").Set(new { ResourceValue = "Most Recently Added" }).Where(new { ResourceName = oldNewKey });
        }

        public override void Down()
        {
            Update.Table("LocaleStringResource").Set(new { ResourceValue = "Name: A to Z" }).Where(new { ResourceName = nameAscKey });
            Update.Table("LocaleStringResource").Set(new { ResourceValue = "Name: Z to A" }).Where(new { ResourceName = nameDescKey });
            Update.Table("LocaleStringResource").Set(new { ResourceValue = "New to Old" }).Where(new { ResourceName = oldNewKey });
        }
    }
}
