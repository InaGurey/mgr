﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201306031550)]
    public class AddFlatShippingColumns : Migration
    {
        public override void Up() {
			Create.Column("DefaultFlatShippingRate").OnTable("Category").AsDecimal(18, 4).Nullable();
			Create.Column("FlatShippingRate").OnTable("ProductVariant").AsDecimal(18, 4).Nullable();
			Execute.Sql("UPDATE Category SET DefaultFlatShippingRate = 50 WHERE DrsCode = 'BIOO'");
			Execute.Sql("UPDATE Category SET DefaultFlatShippingRate = 60 WHERE DrsCode = 'GUAB'");
			Execute.Sql("UPDATE Category SET DefaultFlatShippingRate = 60 WHERE DrsCode = 'KMAK'");
			Execute.Sql("UPDATE Category SET DefaultFlatShippingRate = 150 WHERE DrsCode = 'PEDK'");
	        Execute.Sql(@"
UPDATE ProductVariant SET FlatShippingRate = Category.DefaultFlatShippingRate
FROM ProductVariant
INNER JOIN Product_Category_Mapping PC ON ProductVariant.ProductId = PC.ProductId
INNER JOIN Category ON PC.CategoryId = Category.Id
WHERE Category.DefaultFlatShippingRate IS NOT NULL");
			Delete.Column("DefaultAllowShipping").FromTable("Category");
        }

        public override void Down()
        {
			Create.Column("DefaultAllowShipping").OnTable("Category").AsBoolean().NotNullable().WithDefaultValue(true);
			Execute.Sql("UPDATE Category SET DefaultAllowShipping = 0 WHERE DrsCode IN ('BIOO','GUAB','KMAK','PEDK')");
			Delete.Column("DefaultFlatShippingRate").FromTable("Category");
			Delete.Column("FlatShippingRate").FromTable("ProductVariant");
		}
    }
}
