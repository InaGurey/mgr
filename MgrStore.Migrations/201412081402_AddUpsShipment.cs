﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyMigrator;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201412081402)]
    public class AddUpsShipment : Migration
    {
        class Shipment
        {
            decimal? Length;
            decimal? Width;
            decimal? Height;
        }
        class UpsShipment
        {
            [Fk("Shipment")]
            int ShipmentId;
            string PackagingType;
            decimal? DeclaredValue;
            string Service;
            decimal? QuotedPrice;
            bool? SendEmailNotification;
            bool? ConfirmationDelivery;
            bool? RequireSignature;
            bool? SaturdayDelivery;
            bool? UpsCarbonNeutral;
        }
        public override void Up()
        {
            Create.Columns<Shipment>();
            Create.Table<UpsShipment>();
        }

        public override void Down()
        {
            Delete.Columns<Shipment>();
            Delete.Table<UpsShipment>();
        }
    }
}
