﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201301111302)]
	public class AddStoreNumberToProduct : Migration
	{
		public override void Up()
		{
			Create.Column("StoreNumber").OnTable("ProductVariant").AsString(5).Nullable();
		}


		public override void Down()
		{
			Delete.Column("StoreNumber").FromTable("ProductVariant");
		}
	}
}
