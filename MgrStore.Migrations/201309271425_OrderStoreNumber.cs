﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;


namespace MgrStore.Migrations
{
	[Migration(201309271425)]
    public class OrderStoreNumber : Migration
    {
        public override void Up() {
	        Create.Column("StoreNumber").OnTable("Order").AsString(50).Nullable();
			Execute.Sql(@"
UPDATE [Order] SET StoreNumber = Locations.StoreNumber
FROM portal3..Locations
INNER JOIN portal3..Accounts ON Locations.Id = Accounts.PrimaryLocationId
INNER JOIN portal3..Websites ON Accounts.Id = Websites.AccountId
WHERE [Order].SiteId = Websites.Id");
        }

        public override void Down() {
			Delete.Column("StoreNumber").FromTable("Order");
        }
    }
}
