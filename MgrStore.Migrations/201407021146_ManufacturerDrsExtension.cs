﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace PiasStore.Migrations
{
    [Migration(201407021146)]
    public class ManufacturerDrsExtension : Migration
    {
        class Manufacturer
        {
            int? DrsId;
        }

        public override void Up()
        {
            Create.Columns<Manufacturer>();
        }

        public override void Down()
        {
            Delete.Columns<Manufacturer>();
        }
    }
}
