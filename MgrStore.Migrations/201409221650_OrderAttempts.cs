﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201409221650)]
    public class OrderAttempts : Migration
    {
        class OrderAttempt
        {
            [Short] string IpAddress;
            int CustomerId;
            bool Failed;
            [Long] string Error;
            DateTime AttemptedAtUtc;
        }

        public override void Up() 
        {
            Create.Table<OrderAttempt>();
        }

        public override void Down()
        {
            Delete.Table<OrderAttempt>();
        }
    }
}
