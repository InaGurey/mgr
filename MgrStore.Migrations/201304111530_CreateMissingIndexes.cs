﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
	[Migration(201304111530)]
	public class CreateMissingIndexes : Migration
	{
		public override void Up() {
			Execute.Sql("CREATE NONCLUSTERED INDEX IX_Product_Picture_Mapping_ProductId ON [Product_Picture_Mapping] ([ProductId]) WITH FILLFACTOR = 90");
			Execute.Sql("CREATE NONCLUSTERED INDEX IX_Log_CustomerId ON [Log] ([CustomerId]) INCLUDE ([Id]) WITH FILLFACTOR = 90");
			Execute.Sql("CREATE NONCLUSTERED INDEX IX_Product_Category_Mapping_CategoryId_IsFeaturedProduct ON [Product_Category_Mapping] ([CategoryId], [IsFeaturedProduct]) WITH FILLFACTOR = 90");
		}

		public override void Down() {
			Delete.Index("IX_Product_Picture_Mapping_ProductId").OnTable("Product_Picture_Mapping");
			Delete.Index("IX_Log_CustomerId").OnTable("Log");
			Delete.Index("IX_Product_Category_Mapping_CategoryId_IsFeaturedProduct").OnTable("Product_Category_Mapping");
		}
	}
}
