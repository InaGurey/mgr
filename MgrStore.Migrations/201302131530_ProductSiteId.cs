﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
	[Migration(201302131530)]
	public class ProductSiteId : Migration
	{
		public override void Up() {
			Create.Column("SiteId").OnTable("Product").AsInt32().Nullable();

			// test data so we can see zee-specific products before MGR is migrated to the portal
			// 916 = joshisawesome (Plato's)
			// 40081 = MGR store # assigned to a few thousand products
			Execute.Sql("UPDATE Product SET SiteId = CASE StoreNumber WHEN '40081' THEN 916 ELSE 0 END");

			// now that it's populated, make it non-nullable and index it
			Alter.Column("SiteId").OnTable("Product").AsInt32().NotNullable().Indexed("IX_Product_SiteId");
		}

		public override void Down() {
			Delete.Index("IX_Product_SiteId").OnTable("Product");
			Delete.Column("SiteId").FromTable("Product");
		}
	}
}
