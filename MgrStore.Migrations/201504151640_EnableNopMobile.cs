﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201504151640)]
    public class EnableNopMobile : Migration
    {
        private const string mobileDeviceSupportedKey = "storeinformationsettings.mobiledevicessupported";
        public override void Up()
        {
            Update.Table("Setting").Set(new { Value = "True"}).Where(new {Name = mobileDeviceSupportedKey});
        }

        public override void Down()
        {
            Update.Table("Setting").Set(new { Value = "False"}).Where(new {Name = mobileDeviceSupportedKey});
        }
    }
}
