﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201505242330)]
    public class PictureHash : Migration
    {
        class Picture
        {
            [Index] long? ImageHashFNV1a;
        }

        public override void Up()
        {
            Create.Columns<Picture>();
        }

        public override void Down()
        {
            Delete.Columns<Picture>();
        }
    }
}
