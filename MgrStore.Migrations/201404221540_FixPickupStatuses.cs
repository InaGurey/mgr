﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201404221540)]
	public class FixPickupStatuses : Migration
	{
		public override void Up() 
		{
			UpdateShippingStatusForPickup(20, 50);
			UpdateShippingStatusForPickup(30, 60);
			UpdateShippingStatusForPickup(40, 70);
		}

		public override void Down()
		{
			UpdateShippingStatusForPickup(50, 20);
			UpdateShippingStatusForPickup(60, 30);
			UpdateShippingStatusForPickup(70, 40);
		}

		private void UpdateShippingStatusForPickup(int from, int to) {
			Update.Table("Order")
				.Set(new {ShippingStatusId = to})
				.Where(new { ShippingMethod = "In-Store Pickup", ShippingStatusId = from });			
		}
	}
}
