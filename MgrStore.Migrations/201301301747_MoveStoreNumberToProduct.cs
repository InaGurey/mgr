﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201301301747)]
	public class MoveStoreNumberToProduct : Migration
	{
		public override void Up()
		{
			Create.Column("StoreNumber").OnTable("Product").AsString(5).Nullable();
			Execute.Sql(
@"UPDATE Product SET Product.StoreNumber=pv.StoreNumber
	FROM Product p INNER JOIN ProductVariant pv 
	ON pv.ProductId=p.id");
			Delete.Column("StoreNumber").FromTable("ProductVariant");
		}


		public override void Down()
		{
			Create.Column("StoreNumber").OnTable("ProductVariant").AsString(5).Nullable();
			Execute.Sql(
@"UPDATE Product SET ProductVariant.StoreNumber=p.StoreNumber
	FROM ProductVariant pv INNER JOIN Product p 
	ON pv.ProductId=p.id");
			Delete.Column("StoreNumber").FromTable("Product");
		}
	}
}
