﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201310081700)]
    public class ChangeShowOnHomepageTitle : Migration
    {
        public override void Up()
        {
	        Update.Table("LocaleStringResource")
				.Set(new {ResourceValue = "Featured"})
				.Where(new {ResourceName = "Admin.Catalog.Products.Fields.ShowOnHomePage"});
        }

        public override void Down() {
	        Update.Table("LocaleStringResource")
				.Set(new {ResourceValue = "Show on home page"})
				.Where(new {ResourceName = "Admin.Catalog.Products.Fields.ShowOnHomePage"});
        }
    }
}
