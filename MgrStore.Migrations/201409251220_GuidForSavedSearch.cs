﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201409251220)]
    public class GuidForSavedSearch : Migration
    {
        class SavedSearch
        {
            [Short, NotNull, Default("temp")] string Slug;
        }

        public override void Up() 
        {
            Create.Columns<SavedSearch>();
            Execute.Sql("UPDATE SavedSearch SET Slug=LOWER(CONVERT(NVARCHAR(36), NEWID()))");
        }

        public override void Down()
        {
            Delete.Columns<SavedSearch>();
        }
    }
}
