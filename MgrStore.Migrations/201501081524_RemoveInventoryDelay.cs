﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;
using MgrStore.Migrations;


namespace MgrStore.Migrations
{
    [Migration(201501081524)]
    public class RemoveInventoryDelay : Migration
    {
        public override void Up()
        {
            Delete.Table("ProductImportDelay");
        }

        public override void Down()
        {
            Create.Table("ProductImportDelay")
                  .WithAutoIncPKColumn()
                  .WithColumn("StoreNumber").AsString(5).NotNullable()
                  .WithColumn("DelayDays").AsInt32().NotNullable();
        }
    }
}
