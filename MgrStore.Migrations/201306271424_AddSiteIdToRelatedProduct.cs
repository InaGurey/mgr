﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201306271424)]
	public class AddSiteIdToRelatedProduct : Migration
	{
		public override void Up() 
		{
			Create.Column("SiteId").OnTable("RelatedProduct").AsInt32().Nullable();
		}


		public override void Down()
		{
			Delete.Column("SiteId").FromTable("RelatedProduct");
		}
	}
}
