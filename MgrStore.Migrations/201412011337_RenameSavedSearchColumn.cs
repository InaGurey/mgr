﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201412011337)]
    public class RenameSavedSearchColumn : Migration
    {
        public override void Up()
        {
            Rename.Column("SearchParameters").OnTable("SavedSearch").To("SearchParametersQuerystring");
        }

        public override void Down()
        {
            Rename.Column("SearchParametersQuerystring").OnTable("SavedSearch").To("SearchParameters");
        }
    }
}
