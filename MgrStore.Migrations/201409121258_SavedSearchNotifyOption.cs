﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201409121258)]
    public class SavedSearchNotifyOption : Migration
    {
        class SavedSearch
        {
            [Default("0")] bool NotifyOnlyWhenNewResults;
        }

        public override void Up() 
        {
            Create.Columns<SavedSearch>();
        }

        public override void Down()
        {
            Delete.Columns<SavedSearch>();
        }
    }
}
