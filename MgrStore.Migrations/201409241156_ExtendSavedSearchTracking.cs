﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201409241156)]
    public class ExtendSavedSearchTracking : Migration
    {
        class SavedSearch
        {
            [Default("GETDATE()")] DateTime CreatedOnUtc;
            [Default("GETDATE()")] DateTime UpdatedOnUtc;
            DateTime? DeletedOnUtc;
        }

        public override void Up() 
        {
            Create.Columns<SavedSearch>();
            Execute.Sql("UPDATE SavedSearch SET CreatedOnUtc=StartDate, UpdatedOnUtc=StartDate");
        }

        public override void Down()
        {
            Delete.Columns<SavedSearch>();
        }
    }
}
