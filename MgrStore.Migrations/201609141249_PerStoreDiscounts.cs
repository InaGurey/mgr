﻿using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201609141249)]
    public class PerStoreDiscounts : Migration
    {
        public override void Up()
        {
            Create.Column("SiteId").OnTable("Discount").AsInt32().NotNullable().WithDefaultValue(0);
            Create.Column("SiteId").OnTable("Discount_AppliedToCategories").AsInt32().NotNullable().WithDefaultValue(0);
            Create.Column("SiteId").OnTable("Discount_AppliedToProductVariants").AsInt32().NotNullable().WithDefaultValue(0);
        }
        
        public override void Down()
        {
            Delete.Column("SiteId").FromTable("Discount_AppliedToProductVariants");
            Delete.Column("SiteId").FromTable("Discount_AppliedToCategories");
            Delete.Column("SiteId").FromTable("Discount");
        }
    }
}
