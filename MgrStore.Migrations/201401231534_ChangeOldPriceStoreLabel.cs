﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201401231534)]
    public class ChangeOldPriceStoreLabel : Migration
    {
        public override void Up()
        {
            Update.Table("LocaleStringResource")
                  .Set(new { ResourceValue = "Original price" })
                  .Where(new { LanguageId = 1, ResourceName = "Products.Price.OldPrice" });
        }

        public override void Down()
        {
            Update.Table("LocaleStringResource")
                  .Set(new { ResourceValue = "Old price" })
                  .Where(new { LanguageId = 1, ResourceName = "Products.Price.OldPrice" });
        }
    }
}
