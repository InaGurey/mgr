﻿using System.Linq;
using FluentMigrator;

namespace MgrStore.Migrations.Extensions
{
	public enum PermissionCategory
	{
		Standard,
		Stores,
		Users,
		Customers,
		Catalog,
		Configuration,
		ContentManagement,
		Orders,
		Promo,
		PublicStore
	}

	public enum Role
	{
		Administrators,
		Corporate,
		Zee,
		Registered,
		Guests,
	}

	public static class PermissionsExtensions
	{
		// yes, "Insert.Permission" woulda been cooler, but it woulda required a bad hack to get the context out of IInsertExpressionRoot
 		// in order to do things like Execute.Sql when necessary. 
		public static void AddPermission(this Migration migration, string systemName, string descriptiveName, PermissionCategory category, params Role[] assignTo) {
			migration.Insert.IntoTable("PermissionRecord").Row(new { Name = descriptiveName, SystemName = systemName, Category = category.ToString() });
			if (assignTo.Any()) {
				migration.Execute.Sql(string.Format(
					"INSERT INTO PermissionRecord_Role_Mapping (PermissionRecord_Id, CustomerRole_Id) " +
					"SELECT PermissionRecord.Id, CustomerRole.Id FROM PermissionRecord " +
					"CROSS JOIN CustomerRole " +
					"WHERE PermissionRecord.SystemName = {0} " +
					"AND CustomerRole.SystemName IN ({1})",
					MakeSqlString(systemName),
					string.Join(",", assignTo.Select(s => MakeSqlString(s)))));
			}
		}

		public static void RemovePermission(this Migration migration, string systemName) {
			migration.Execute.Sql(string.Format(
				"DELETE FROM PermissionRecord_Role_Mapping WHERE PermissionRecord_Id = (SELECT Id FROM PermissionRecord WHERE SystemName = {0})",
				MakeSqlString(systemName)));
			migration.Delete.FromTable("PermissionRecord").Row(new { SystemName = systemName });
		}

		// I feel so dirty right now
		private static string MakeSqlString(object obj) {
			return "'" + obj.ToString().Replace("'", "''") + "'";
		}
	}
}
