﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201406181230)]
	public class OrderCreatedOnSiteId : Migration
	{
		public override void Up() {
			Create.Column("CreatedOnSiteId").OnTable("Order").AsInt32().Nullable();
		}

		public override void Down() {
			Delete.Column("CreatedOnSiteId").FromTable("Order");
		}
	}
}
