﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201409231429)]
    public class ExtendOrderAttempts : Migration
    {
        class OrderAttempt
        {
            [Medium] string Email;
            [Medium] string Name;
        }

        public override void Up() 
        {
            Create.Columns<OrderAttempt>();
        }

        public override void Down()
        {
            Delete.Columns<OrderAttempt>();
        }
    }
}
