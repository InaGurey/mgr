﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyMigrator;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201604190921)]
    public class ProductAddDoNotAutoPublishFlag : Migration
    {
        class Product
        {
            private bool DoNotAutoPublish;
        }
        public override void Down()
        {
            Delete.Columns<Product>();
        }

        public override void Up()
        {
            Create.Columns<Product>();
        }
    }
}
