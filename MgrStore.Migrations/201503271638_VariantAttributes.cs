﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyMigrator;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201503271638)]
    public class VariantAttributes : Migration
    {
        class ProductVariant
        {
            int? GenderId;
            int? StyleId;
            int? SizeId;
            int? ColorId;
            int? KeyFeatureId;
            int? BrandQualityId;
            int? ConstructionId;
            int? WithAccessoryId;
            int? InstrumentTypeId;
            int? MaterialId;
            int? AccessoryTypeId;
            int? FinishId;
            int? TypeId;
            int? DeviceTypeId;
            int? AmountId;
        }

        public override void Up()
        {
            Create.Columns<ProductVariant>();
        }

        public override void Down()
        {
            Delete.Columns<ProductVariant>();
        }
    }
}
