﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201403281529)]
	public class ChangeMinSearchTermLength : Migration
	{
		public override void Up() 
		{
			Update.Table("Setting").Set(new {Value = "2"}).Where(new {Name = "catalogsettings.productsearchtermminimumlength"});
		}


		public override void Down()
		{
			Update.Table("Setting").Set(new {Value = "3"}).Where(new {Name = "catalogsettings.productsearchtermminimumlength"});
		}
	}
}
