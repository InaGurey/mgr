﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201301121608)]
	public class AddInventoryDelay : Migration
	{
		public override void Up()
		{
			Create.Table("ProductImportDelay")
			      .WithAutoIncPKColumn()
			      .WithColumn("StoreNumber").AsString(5).NotNullable()
			      .WithColumn("DelayDays").AsInt32().NotNullable();
		}


		public override void Down()
		{
			Delete.Table("ProductImportDelay");
		}
	}
}
