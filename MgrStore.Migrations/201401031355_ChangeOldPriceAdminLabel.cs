﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201401031355)]
    public class ChangeOldPriceAdminLabel : Migration
    {
        public override void Up()
        {
            Update.Table("LocaleStringResource")
                  .Set(new {ResourceValue = "Original price"})
                  .Where(new {LanguageId = 1, ResourceName = "Admin.Catalog.Products.Variants.Fields.OldPrice"});
        }

        public override void Down()
        {
            Update.Table("LocaleStringResource")
                  .Set(new {ResourceValue = "Old price"})
                  .Where(new {LanguageId = 1, ResourceName = "Admin.Catalog.Products.Variants.Fields.OldPrice"});
        }
    }
}
