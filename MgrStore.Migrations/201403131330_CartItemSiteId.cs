﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;

namespace MgrStore.Migrations
{
	[Migration(201403131330)]
    public class CartItemSiteId : Migration
    {
		public override void Up() {
			Create.Column("AddedFromSiteId").OnTable("ShoppingCartItem").AsInt32().Nullable();
		}

		public override void Down() {
			Delete.Column("AddedFromSiteId").FromTable("ShoppingCartItem");
		}
	}
}
