﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201408271542)]
    public class SavedSearchEmail : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("MessageTemplate")
                  .Row(new {
                      Name = "SavedSearch.Results",
                      IsActive = true,
                      EmailAccountId = 1,
                      Subject = "Music Go Round Saved Search Results",
                      Body =
@"<table style='max-width:640px'>
	<tr>
		<td style='background-color:black; padding:20px 35px;'>
			<img src='http://www.musicgoround.com/Themes/MGR/Content/images/logo.png' style='height:39px; width:268px;' height='39' width='268' />
		<td>
	</tr>
	<tr>
		<td style='padding:20px 20px'>
			<p style='margin-bottom:20px;'>There are %SavedSearch.NewMatches% new products that fit your search options!</p>
			
			<p>You requested that we send you these notifications %SavedSearch.Frequency% for the next %SavedSearch.Duration%. if you wish to edit or cancel your search please <a href='%SavedSearch.EditUrl%'>Click Here</a></p>
		</td>
	</tr>
	<tr>
		<td style='background-color:#f1f1f1; padding:25px 20px;'>
			<h2 style='font-size:16px;'>YOUR SEARCH OPTIONS - <a href='%SavedSearch.EditUrl%'>EDIT</a>
			
			<table>
				<tr>
					<td style='padding-right:10px;'><strong>Search Term:</strong> %SavedSearch.SearchTerm%</td>
					<td><strong>Search Radius:</strong> %SavedSearch.Radius%</td>
				</tr>
				<tr>
					<td style='padding-right:10px;'><strong>Category:</strong> %SavedSearch.Category%</td>
					<td><strong>Store:</strong> %SavedSearch.Store%</td>
				</tr>
				<tr>
					<td colspan='2'><strong>Brand:</strong> %SavedSearch.Brand%</td>
				</tr>
				<tr>
					<td colspan='2'><strong>Price Range:</strong> %SavedSearch.PriceRange%</td>
				</tr>
				<tr>
					<td style='padding-top:15px' colspan='2'>Only show products with a Price Reduction: %SavedSearch.ClearanceOnly%</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td style='padding:25 20px;'>
			<h3 style='font-size:16px; width:100%; border-bottom:1px solid #c7c7c7'>Current Products:</h3>
			<table>%SavedSearch.Products%</table>
		</td>
	</tr>
    <tr><td><a href='%SavedSearch.SearchUrl%'>View All Results</a></td></tr>
	<tr>
		<td>
		<table style='border: 0; width: 100%; background-color: #f1f1f1;'>
			<tbody>
			<tr>
				<td style='padding: 10px 10px; vertical-align: top;'>
					<h3 style='font-size: 18px; color: #000; margin-bottom: 10px;'>SHOP OUR STORE AGAIN</h3>
						Online: <a href='%SavedSearch.Store.Domain%'>%SavedSearch.Store.Domain%</a> <br /> <br /> 
						%SavedSearch.Store.Address%
				</td>
				<td style='padding: 10px 10px; vertical-align: top;'>
					%SavedSearch.Store.Connect%
				</td>
			</tr>
			</tbody>
			</table>
		</td>
	</tr>
</table>"
                  });
        }

        public override void Down()
        {
            Delete.FromTable("MessageTemplate").Row(new {Name = "SavedSearch.Results"});
        }
    }
}
