﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator.Builders.Create.Table;



namespace MgrStore.Migrations
{
	static internal class MigrationExtensions
	{
		static public ICreateTableColumnOptionOrWithColumnSyntax WithGuidPKColumn(this ICreateTableWithColumnSyntax tableWithColumnSyntax)
		{
			return tableWithColumnSyntax
					.WithColumn("Id")
					.AsGuid()
					.NotNullable()
					.PrimaryKey();
		}


		static public ICreateTableColumnOptionOrWithColumnSyntax WithAutoIncPKColumn(this ICreateTableWithColumnSyntax tableWithColumnSyntax)
		{
			return tableWithColumnSyntax
					.WithColumn("Id")
					.AsInt32()
					.NotNullable()
					.PrimaryKey()
					.Identity();
		}
	}
}
