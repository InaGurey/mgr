﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201409171333)]
    public class ChangeProductsPerPage : Migration
    {
        public override void Up() 
        {
            Update.Table("Setting").Set(new { Value = 29 }).Where(new { Name = "catalogsettings.searchpageproductsperpage" });
        }

        public override void Down() { }
    }
}
