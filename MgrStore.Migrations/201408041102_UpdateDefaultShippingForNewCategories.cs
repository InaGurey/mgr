﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
    [Migration(201408041102)]
    public class UpdateDefaultShippingForNewCategories : Migration
    {
        public override void Up() 
        {
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 70M }).Where(new { DrsCode = "BICE" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "BICO" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "BIFH" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 100M }).Where(new { DrsCode = "BITU" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "BIVI" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 40M }).Where(new { DrsCode = "BIVO" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 125M }).Where(new { DrsCode = "GUBS" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 125M }).Where(new { DrsCode = "GUGS" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "KMAC" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 20M }).Where(new { DrsCode = "PEHD" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "PESD" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "PSDJ" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 25M }).Where(new { DrsCode = "PSDM" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 40M }).Where(new { DrsCode = "PSLI" });
        }

        public override void Down()
        {
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "BICE" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "BICO" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "BIFH" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "BITU" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "BIVI" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 35M }).Where(new { DrsCode = "BIVO" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 100M }).Where(new { DrsCode = "GUBS" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 100M }).Where(new { DrsCode = "GUGS" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 70M }).Where(new { DrsCode = "KMAC" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 20M }).Where(new { DrsCode = "PEHD" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "PESD" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "PSDJ" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 30M }).Where(new { DrsCode = "PSDM" });
            Update.Table("Category").Set(new { DefaultFlatShippingRate = 50M }).Where(new { DrsCode = "PSLI" });
        }
    }
}
