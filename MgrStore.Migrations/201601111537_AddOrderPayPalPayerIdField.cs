﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyMigrator;
using FluentMigrator;

namespace MgrStore.Migrations
{
    [Migration(201601111537)]
    public class AddOrderPayPalPayerIdField : Migration
    {
        class Order
        {
            [Max]
            private string PayerId;
            [Max]
            private string SecureAcceptanceUrl;
        }

        public override void Down()
        {
            Delete.Columns<Order>();
        }

        public override void Up()
        {
            Create.Columns<Order>();
        }
    }
}
