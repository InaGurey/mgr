﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentMigrator;



namespace MgrStore.Migrations
{
	[Migration(201301121034)]
	public class AddPurchaseDateToProduct : Migration
	{
		public override void Up()
		{
			Create.Column("PurchaseDate").OnTable("ProductVariant").AsDateTime().Nullable();

		}


		public override void Down()
		{
			Delete.Column("PurchaseDate").FromTable("ProductVariant");
		}
	}
}
