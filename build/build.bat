@IF NOT EXIST "%PROGRAMFILES(x86)%\MSBuild\14.0\Bin\MSBuild.exe" @ECHO COULDN'T FIND MSBUILD: "%PROGRAMFILES(x86)%\MSBuild\14.0\Bin\MSBuild.exe" (Is .NET 4.6 installed?)

"%PROGRAMFILES(x86)%\MSBuild\14.0\Bin\MSBuild.exe" nop.proj /p:DebugSymbols=true /p:DebugType=None /maxcpucount /l:FileLogger,Microsoft.Build.Engine;logfile=log.log %*