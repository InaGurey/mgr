﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using WinmarkFranchise.Domain;
using WinmarkFranchise.Domain.Caching;

namespace Nop.Core
{
	#region models
	public class SsoData
	{
		public string Username { get; set; }
		public Guid CustomerGuid { get; set; }
		/// <summary>
		/// currently only needed on admin side
		/// </summary>
		public int? SiteId { get; set; }
		public bool RememberMe { get; set; }
	}
	#endregion

	public interface ISingleSignOnProvider
	{
		string CreateToken(Customer user, bool rememberMe);
		SsoData RetreiveToken(string token);
	}

	public class SingleSignOnProvider : ISingleSignOnProvider
	{
		private readonly IOutOfProcessCacheManager _cacheManager;

		public SingleSignOnProvider(IOutOfProcessCacheManager cacheManager) {
			_cacheManager = cacheManager;
		}

		public string CreateToken(Customer user, bool rememberMe) {
            string token;
            _cacheManager.Set(
                CacheKeys.Sso.Token(Franchises.MusicGoRound, out token),
                new SsoData {
                    CustomerGuid = user.CustomerGuid,
                    Username = user.Username,
                    RememberMe = rememberMe
                }, 10);
            return token;
		}

		public SsoData RetreiveToken(string token) {
            var key = CacheKeys.Sso.Token(Franchises.MusicGoRound, token);
            var data = _cacheManager.Get<SsoData>(key);
            if (data != null) {
                _cacheManager.Remove(key);
                return data;
            }
            throw new ArgumentException("SSO token not found.");
		}
	}
}
