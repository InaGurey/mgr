﻿

using System.Collections.Generic;

namespace Nop.Core
{
    public static class Extensions
    {
        public static bool IsNullOrDefault<T>(this T? value) where T : struct
        {
            return default(T).Equals(value.GetValueOrDefault());
        }

		public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key) {
			return dict.GetOrDefault(key, default(TValue));
		}

		public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue defaultVal) {
			return dict.ContainsKey(key) ? dict[key] : defaultVal;
		}
	}
}
