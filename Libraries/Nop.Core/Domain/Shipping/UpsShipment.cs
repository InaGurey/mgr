﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nop.Core.Domain.Shipping
{
    public partial class UpsShipment : BaseEntity
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public virtual int ShipmentId { get; set; }
        public virtual string PackagingType { get; set; }
        public virtual decimal? DeclaredValue { get; set; }
        public virtual string Service { get; set; }
        public virtual decimal? QuotedPrice { get; set; }
        public virtual bool SendEmailNotification { get; set; }
        public virtual bool ConfirmationDelivery { get; set; }
        public virtual bool RequireSignature { get; set; }
        public virtual bool SaturdayDelivery { get; set; }
        public virtual bool UpsCarbonNeutral { get; set; }
        public virtual bool? OversizePackage { get; set; }
        public virtual bool? ResidentialAddress { get; set; }
        public virtual Shipment Shipment { get; set; }
    }
}
