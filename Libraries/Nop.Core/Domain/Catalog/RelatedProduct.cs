namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents a related product
    /// </summary>
    public partial class RelatedProduct : BaseEntity
    {
        /// <summary>
        /// Gets or sets the first product identifier
        /// </summary>
        public virtual int ProductId1 { get; set; }

        /// <summary>
        /// Gets or sets the second product identifier
        /// </summary>
        public virtual int ProductId2 { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public virtual int DisplayOrder { get; set; }


		/// <summary>
		/// SiteId is used to differentiate between related products to show when viewing a specific zee site vs the corporate site.
		/// If null, the related product was added through the Admin (used) and probably should have priority over auto-related on the zee site and maybe also on corporate
		/// </summary>
		public virtual int? SiteId { get; set; }
    }

}
