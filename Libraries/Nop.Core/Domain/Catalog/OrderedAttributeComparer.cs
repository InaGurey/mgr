﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Catalog
{
    public class OrderedAttributeComparer<T>: IComparer<T>
    {
        private readonly IList<T> _orderedKeys;

        public OrderedAttributeComparer(IList<T> orderedKeys)
        {
            _orderedKeys = orderedKeys;
        }

        public int Compare(T x, T y)
        {
            var ix = _orderedKeys.IndexOf(x);
            var iy = _orderedKeys.IndexOf(y);

            return ix - iy;
        }
    }
}
