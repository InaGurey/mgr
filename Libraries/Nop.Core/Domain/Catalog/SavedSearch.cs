﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Nop.Core.Domain.Localization;


namespace Nop.Core.Domain.Catalog
{
    public partial class SavedSearch : BaseEntity, ILocalizedEntity
    {
        private bool _updatingParameters = false;

        public SavedSearch()
        {
            _searchParameters = new Lazy<IProductSearchParameters>(() => {
                var p = new ProductSearchParameters(SearchParametersQuerystring);
                p.Changed += delegate(object sender, EventArgs e) {
                    _updatingParameters = true;
                    SearchParametersQuerystring = SearchParameters.ToQuerystring();
                    _updatingParameters = false;
                };
                return p;
            });
        }

        public virtual string EmailAddress { get; set; }
        public virtual string Frequency { get; set; }
        public virtual string Duration { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual int SiteId { get; set; }
        public virtual bool NotifyOnlyWhenNewResults { get; set; }
        public virtual DateTime CreatedOnUtc { get; set; }
        public virtual DateTime UpdatedOnUtc { get; set; }
        public virtual DateTime? DeletedOnUtc { get; set; }
        public virtual string Slug { get; set; }

        private string _searchParameteresQuerystringStorage;
        public string SearchParametersQuerystring
        {
            get { return _searchParameteresQuerystringStorage; }
            set {
                if (value != _searchParameteresQuerystringStorage) {
                    _searchParameteresQuerystringStorage = value;
                    if (!_updatingParameters)
                        SearchParameters.FillFromQuerystring(_searchParameteresQuerystringStorage);
                }
            }
        }

        private readonly Lazy<IProductSearchParameters> _searchParameters;
        public IProductSearchParameters SearchParameters { get { return _searchParameters.Value; } }
    }
}
