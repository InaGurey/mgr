﻿namespace Nop.Core.Domain.Catalog
{
    /// <summary>
    /// Represents the product sorting
    /// All values above 10 are NOT used by customer
    /// </summary>
    public enum ProductSortingEnum
    {
        SkuAsc = 11,
        SkuDesc = 12,
        NameAsc = 3,
        NameDesc = 4,
        StoreNumAsc = 15,
        StoreNumDesc = 16,
        PriceAsc = 7,
        PriceDesc = 8,
        DateListed = 9,
        DistanceAsc= 20
    }
}