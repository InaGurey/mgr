﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Treefort.Common;


namespace Nop.Core.Domain.Catalog
{
    public interface IProductSearchParameters
    {
        string Keywords { get; set; }
        IEnumerable<int> CategoryIds { get; set; }
        int? CategoryId { get; set; }
        int? ManufacturerId { get; set; }
        decimal? MinimumPrice { get; set; }
        decimal? MaximumPrice { get; set; }
        bool ClearanceOnly { get; set; }
        IEnumerable<string> StoreNumbers { get; set; }
        string PostalCode { get; set; }
        int? MilesFromPostalCode { get; set; }
    }

    public class ProductSearchParameters : IProductSearchParameters
    {
        private bool _batchUpdating = false;
        private bool _changesDuringBatch = false;
        public event EventHandler Changed = delegate { };

        private void OnChanged()
        {
            if (_batchUpdating)
                _changesDuringBatch = true;
            else
                Changed(this, EventArgs.Empty);
        }

        public ProductSearchParameters() { }
        public ProductSearchParameters(string querystring) { this.FillFromQuerystring(querystring); }

        public IDisposable BatchUpdate(bool raiseEventIfChanges = false) { return new BatchUpdateScope(this, raiseEventIfChanges); }

        private class BatchUpdateScope : IDisposable
        {
            private readonly ProductSearchParameters _outer;
            private readonly bool _raiseEventIfChanges;

            public BatchUpdateScope(ProductSearchParameters outer, bool raiseEventIfChanges)
            {
                Enforce.NotNull(() => outer);
                _outer = outer;
                _outer._batchUpdating = true;
                _raiseEventIfChanges = raiseEventIfChanges;
            }

            public void Dispose()
            {
                _outer._batchUpdating = false;
                var batchHasChanges = _outer._changesDuringBatch;
                _outer._changesDuringBatch = false;
                if (_raiseEventIfChanges && batchHasChanges)
                    _outer.OnChanged();
            }
        }

        private string _keywords;
        public string Keywords
        {
            get { return _keywords; }
            set
            {
                if (value != _keywords) {
                    _keywords = value;
                    OnChanged();
                }
            }
        }

        private IEnumerable<int> _categoryIds;
        public IEnumerable<int> CategoryIds
        {
            get { return _categoryIds; }
            set
            {
                if (!(value == null && _categoryIds == null) &&
                    (value == null || _categoryIds == null ||
                    !value.OrderBy(o => o).SequenceEqual(_categoryIds.OrderBy(o => o)))) 
                {
                    _categoryIds = value;
                    OnChanged();
                }
            }
        }

        private int? _manufacturerId;
        public int? ManufacturerId
        {
            get { return _manufacturerId; }
            set
            {
                if (value != _manufacturerId) {
                    _manufacturerId = value;
                    OnChanged();
                }
            }
        }

        private decimal? _minimumPrice;
        public decimal? MinimumPrice
        {
            get { return _minimumPrice; }
            set
            {
                if (value != _minimumPrice) {
                    _minimumPrice = value;
                    OnChanged();
                }
            }
        }

        private decimal? _maximumPrice;
        public decimal? MaximumPrice
        {
            get { return _maximumPrice; }
            set
            {
                if (value != _maximumPrice) {
                    _maximumPrice = value;
                    OnChanged();
                }
            }
        }

        private bool _clearanceOnly;
        public bool ClearanceOnly
        {
            get { return _clearanceOnly; }
            set
            {
                if (value != _clearanceOnly) {
                    _clearanceOnly = value;
                    OnChanged();
                }
            }
        }

        private IEnumerable<string> _storeNumbers;
        public IEnumerable<string> StoreNumbers
        {
            get { return _storeNumbers; }
            set
            {
                if (!(value == null && _storeNumbers == null) &&
                    (value == null || _storeNumbers == null ||
                    !value.OrderBy(o => o).SequenceEqual(_storeNumbers.OrderBy(o => o)))) 
                {
                    _storeNumbers = value;
                    OnChanged();
                }
            }
        }

        private string _postalCode;
        public string PostalCode
        {
            get { return _postalCode; }
            set
            {
                if (value != _postalCode) {
                    _postalCode = value;
                    OnChanged();
                }
            }
        }

        private int? _milesFromPostalCode;
        public int? MilesFromPostalCode
        {
            get { return _milesFromPostalCode; }
            set
            {
                if (value != _milesFromPostalCode) {
                    _milesFromPostalCode = value;
                    OnChanged();
                }
            }
        }


        public int? CategoryId
        {
            get { return CategoryIds.IsNullOrEmpty() ? (int?)null : CategoryIds.First(); }
            set { CategoryIds = (value ?? 0) == 0 ? Enumerable.Empty<int>() : new[] { value.Value }; }
        }

        static public IProductSearchParameters FromQuerystring(string querystring) { return new ProductSearchParameters().FillFromQuerystring(querystring, false); }
    }

    static public class ProductSearchParametersExtensions
    {
        static public string ToQuerystring(this IProductSearchParameters parameters)
        {
            var qs = HttpUtility.ParseQueryString("");

            parameters.Keywords.IfNotNullOrEmpty(o => qs["Q"] = o);
            parameters.CategoryIds.IfNotNullOrEmpty(o => qs["Cid"] = string.Join(",", o));
            parameters.ManufacturerId.IfHasValue(o => qs["Mid"] = o.ToString());
            parameters.MinimumPrice.IfHasValue(o => qs["Pf"] = o.ToString());
            parameters.MaximumPrice.IfHasValue(o => qs["Pt"] = o.ToString());
            parameters.StoreNumbers.IfNotNullOrEmpty(o => qs["Sn"] = string.Join(",", o));
            parameters.PostalCode.IfNotNullOrEmpty(o => qs["Zip"] = o);
            parameters.MilesFromPostalCode.IfHasValue(o => qs["Dist"] = o.ToString());
            if (parameters.ClearanceOnly)
                qs["Cl"] = parameters.ClearanceOnly.ToString();

            return qs.ToString();
        }

        static public IProductSearchParameters FillFromQuerystring(this IProductSearchParameters parameters, string querystring, bool preserveParametersNotInQuerystring = false)
        {
            IDisposable batchScope = null;
            if (parameters is ProductSearchParameters)
                batchScope = ((ProductSearchParameters)parameters).BatchUpdate();

            try {
                var qs = HttpUtility.ParseQueryString(querystring ?? string.Empty);
                var fill = !preserveParametersNotInQuerystring;

                parameters.Keywords = qs["Q"].IfNotNullOrEmpty(o => o, fill ? null : parameters.Keywords);
                parameters.CategoryIds = qs["Cid"].IfNotNullOrEmpty(o => o.Split(',').Select(int.Parse), fill ? null : parameters.CategoryIds);
                parameters.ManufacturerId = qs["Mid"].IfNotNullOrEmpty(ParseInt, fill ? null : parameters.ManufacturerId);
                parameters.MinimumPrice = qs["Pf"].IfNotNullOrEmpty(ParseDecimal, fill ? null : parameters.MinimumPrice);
                parameters.MaximumPrice = qs["Pt"].IfNotNullOrEmpty(ParseDecimal, fill ? null : parameters.MaximumPrice);
                parameters.StoreNumbers = qs["Sn"].IfNotNullOrEmpty(o => o.Split(','), fill ? null : parameters.StoreNumbers);
                parameters.PostalCode = qs["Zip"].IfNotNullOrEmpty(o => o, fill ? null : parameters.PostalCode);
                parameters.MilesFromPostalCode = qs["Dist"].IfNotNullOrEmpty(ParseInt, fill ? null : parameters.MilesFromPostalCode);
                parameters.ClearanceOnly = qs["Cl"].IfNotNullOrEmpty(bool.Parse, fill ? false : parameters.ClearanceOnly);
            }
            finally {
                if (batchScope != null)
                    batchScope.Dispose();
            }

            return parameters;
        }

        static private int? ParseInt(string value)
        {
            var clean = value.IfNotNullOrEmpty(u => new string(u.Where(c => Char.IsDigit(c) || c == '.' || c == ',').ToArray()));
            if (clean.IsNullOrEmpty())
                return null;

            int amt;
            if (int.TryParse(clean, out amt))
                return amt;
            return null;
        }

        static private decimal? ParseDecimal(string value)
        {
            var clean = CleanPrice(value);
            if (clean.IsNullOrEmpty())
                return null;

            decimal amt;
            if (decimal.TryParse(clean, out amt))
                return amt;
            return null;
        }

        static private string CleanPrice(string unclean)
        {
            return unclean.IfNotNullOrEmpty(u => new string(u.Where(c => Char.IsDigit(c) || c == '.' || c == ',').ToArray()));
        }
    }
}
