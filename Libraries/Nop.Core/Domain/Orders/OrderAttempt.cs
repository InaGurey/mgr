﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Core.Domain.Orders
{
    public partial class OrderAttempt : BaseEntity
    {
        public virtual string IpAddress { get; set; }
        public virtual int CustomerId { get; set; }
        public virtual bool Failed { get; set; }
        public virtual string Error { get; set; }
        public virtual DateTime AttemptedAtUtc { get; set; }
        public virtual string Email { get; set; }
        public virtual string Name { get; set; }
    }
}
