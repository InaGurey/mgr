﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Core.Domain.Customers
{
	public enum AdminRole { SuperAdmin, Corporate, Zee, Trusted, ContentEditor, None }

	public class AdminUser : Customer
	{
		public int SiteId { get; set; }
		public AdminRole AdminRole { get; set; }
	}
}
