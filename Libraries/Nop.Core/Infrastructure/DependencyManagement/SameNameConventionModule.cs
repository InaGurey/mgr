﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Autofac;
using Module = Autofac.Module;


namespace Nop.Core.Infrastructure.DependencyManagement
{
    public class SameNameConventionModule : Module
    {
        private readonly IEnumerable<Type> _types;

        public SameNameConventionModule(Assembly assembly) { _types = assembly.GetTypes(); }

        public SameNameConventionModule(IEnumerable<Type> types) { _types = types; }

        protected override void Load(ContainerBuilder builder)
        {
            foreach (var type in _types.Where(t => !t.IsAbstract)) {
                var iface = type.GetInterface("I" + type.Name);
                if (iface == null)
                    continue;

                if (type.IsGenericTypeDefinition)
                    builder.RegisterGeneric(type).As(iface);
                else
                    builder.RegisterType(type).As(iface);
            }
        }
    }
}
