﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Core
{
	/// <summary>
	/// Paged list
	/// </summary>
	/// <typeparam name="T">T</typeparam>
	public class PagedList<T> : List<T>, IPagedList<T>
	{
		public PagedList(IEnumerable<int> sourceIds, int pageIndex, int pageSize, Func<int, T> materialize) {
			this.PageIndex = pageIndex;
			this.PageSize = pageSize;
			var ids = sourceIds.ToList();
			this.TotalCount = ids.Count;
			this.AddRange(ids.Skip(pageIndex * pageSize).Take(pageSize).Select(materialize));
		}

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="source">source</param>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		public PagedList(IEnumerable<T> source, int pageIndex, int pageSize) {
			this.PageIndex = pageIndex;
			this.PageSize = pageSize;
			var list = source.ToList();
			this.TotalCount = list.Count;
			this.AddRange(list.Skip(pageIndex * pageSize).Take(pageSize));
		}

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="source">source</param>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <param name="totalCount">Total count</param>
		public PagedList(IEnumerable<T> source, int pageIndex, int pageSize, int totalCount) {
			this.PageIndex = pageIndex;
			this.PageSize = pageSize;
			this.TotalCount = totalCount;
			this.AddRange(source);
		}

		public int TotalCount { get; private set; }
		public int PageIndex { get; private set; }
		public int PageSize { get; private set; }

		public int TotalPages {
			get {
				var result = TotalCount / PageSize;
				if (TotalCount % PageSize > 0)
					result++;
				return result;
			}
		}

		public bool HasPreviousPage {
			get { return (PageIndex > 0); }
		}

		public bool HasNextPage {
			get { return (PageIndex + 1 < TotalPages); }
		}
	}
}
