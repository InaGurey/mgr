﻿using System;
using System.Reflection;

namespace Nop.Core
{
    public interface IAttribute<T>
    {
        T Value { get; }
    }

    public static class EnumHelper
    {
        public static T2 GetAttribute<T1, T2>(this T1 enumValue)
            where T2 : Attribute
        {
            return enumValue.GetType().GetTypeInfo().GetDeclaredField(enumValue.ToString()).GetCustomAttribute<T2>();
        }

        public static T GetValueFromAttribute<T, TA>(this string attributeValue)
            where TA : Attribute, IAttribute<string>
        {
            var type = typeof (T);

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field, typeof (TA)) as TA;

                if (attribute != null)
                {
                    if (attribute.Value == attributeValue)
                    {
                        return (T) field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == attributeValue)
                        return (T) field.GetValue(null);
                }
            }

            return default(T);
        }
    }

    public sealed class CodeAttribute : Attribute, IAttribute<string>
    {
        private readonly string _value;

        public CodeAttribute(string value)
        {
            _value = value;
        }
        public string Value { get { return _value; } }
    }

    public sealed class NameAttribute : Attribute, IAttribute<string>
    {
        private readonly string _value;

        public NameAttribute(string value)
        {
            _value = value;
        }
        public string Value { get { return _value; } }
    }
}
