using System;

namespace Nop.Core.Caching
{
    /// <summary>
    /// Extensions
    /// </summary>
    public static class CacheExtensions
    {
		/// <summary>
		/// defaults to caching for 60 minutes
		/// </summary>
		public static T Get<T>(this IPerRequestCacheManager cacheManager, string key, Func<T> acquire)
        {
			return Get(cacheManager, key, 60, acquire);
		}

		public static T Get<T>(this IPerRequestCacheManager cacheManager, string key, int minutes, Func<T> acquire)
        {
			if (cacheManager.IsSet(key)) {
				return cacheManager.Get<T>(key);
			}
			else {
				var result = acquire();
				//if (result != null)
				cacheManager.Set(key, result, minutes);
				return result;
			}
		}

		public static T Get<T>(this IMemoryCacheManager cacheManager, string key, Func<T> acquire)
        {
			return Get(cacheManager, key, 60, acquire);
		}

		public static T Get<T>(this IMemoryCacheManager cacheManager, string key, int minutes, Func<T> acquire)
        {
			if (cacheManager.IsSet(key)) {
				return cacheManager.Get<T>(key);
			}
			else {
				var result = acquire();
				//if (result != null)
				cacheManager.Set(key, result, minutes);
				return result;
			}
		}

        public static T Get<T>(this IOutOfProcessCacheManager cacheManager, string key, Func<T> acquire)
        {
            return Get(cacheManager, key, 60, acquire);
        }

        public static T Get<T>(this IOutOfProcessCacheManager cacheManager, string key, int minutes, Func<T> acquire)
        {
            if (cacheManager.IsSet(key))
            {
                return cacheManager.Get<T>(key);
            }
            else
            {
                var result = acquire();
                //if (result != null)
                cacheManager.Set(key, result, minutes);
                return result;
            }
        }
	}
}
