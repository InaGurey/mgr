using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;


namespace Nop.Core.Caching
{
	// This awesomeness was brought to you by Treefort
	public partial class RedisCacheManager : IOutOfProcessCacheManager, IMemoryCacheManager
	{
        private readonly ICacheClient _redis;
	    private readonly ConnectionMultiplexer _redisConn;

        public RedisCacheManager(ICacheClient redis, ConnectionMultiplexer redisConn)
        {
			_redis = redis;
            _redisConn = redisConn;
        }

		// Someday soon let's replace everything with the methods below, which prepend "mgr:" to all keys. BUT - the
		// prefix would need to also be added to the key used in portal for sso/interop with mgr. Bad idea to do
		// this today as we're switching servers and dealing with dns propagation delay. (TM 9/12/13)

		//public T Get<T>(string key) {
		//	return _redis.ExecAs<T>(c => c.GetValue("mgr:" + key));
		//}

		//public void Set<T>(string key, T data, int minutes) {
		//	if (data == null)
		//		return;

		//	_redis.ExecAs<T>(c => c.SetEntry("mgr:" + key, data, TimeSpan.FromMinutes(minutes)));
		//}

		//public bool IsSet(string key) {
		//	return _redis.Exec(c => c.ContainsKey("mgr:" + key));
		//}

		//public void Remove(string key) {
		//	_redis.Exec(c => c.Remove("mgr:" + key));
		//}

		//public void RemoveByPattern(string pattern) {
		//	var regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
		//	_redis.Exec(c => {
		//		var keys = c.GetAllKeys().Where(k => {
		//			if (!k.StartsWith("mgr:"))
		//				return false;
		//			return regex.IsMatch(k.Substring(3));
		//		}).ToList();
		//		c.RemoveAll(keys);
		//	});
		//}

		//public void Clear() {
		//	_redis.Exec(c => {
		//		var keys = c.GetAllKeys().Where(k => k.StartsWith("mgr:"));
		//		c.RemoveAll(keys);
		//	});
		//}

	    public T Get<T>(string key)
	    {
	        var redisValue = _redis.Database.StringGet(key);
	        if (redisValue.IsNull)
	            return default(T);

            return JsonConvert.DeserializeObject<T>(redisValue);
	    }

	    private static Type[] castableTypes = {typeof (bool), typeof (int?)};

        public void Set<T>(string key, T data, int minutes)
        {
            _redis.Database.StringSet(key, JsonConvert.SerializeObject(data), TimeSpan.FromMinutes(minutes));
		}

		public bool IsSet(string key) {
			return _redis.Exists(key);
		}

		public void Remove(string key) {
			_redis.Remove(key);
		}

		public void RemoveByPattern(string pattern) {
			var regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

		    var matches = new List<RedisKey>();
		    foreach (var endpoint in _redisConn.GetEndPoints()) {
		        matches.AddRange(_redisConn.GetServer(endpoint).Keys().Where(k => regex.IsMatch(k)));
		    }
		    _redis.Database.KeyDelete(matches.ToArray());
		}

	    public void Clear()
	    {
	        foreach (var endpoint in _redisConn.GetEndPoints())
	            _redisConn.GetServer(endpoint).FlushDatabase();
	    }
	}
}