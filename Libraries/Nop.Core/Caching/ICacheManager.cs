namespace Nop.Core.Caching
{
	// yeah these interfaces are identical but I think the way Nop did it with ICacheManager
	// was a pointless abstraction and made troubleshooting suck. I think services should
	// explicitly decide which type of caching they need, not the ioc container. (TM 9/12/13)

	public interface IMemoryCacheManager
	{
        T Get<T>(string key);
        void Set<T>(string key, T data, int minutes);
		bool IsSet(string key);
		void Remove(string key);
		void RemoveByPattern(string pattern);
		void Clear();
	}

	public interface IOutOfProcessCacheManager
	{
		T Get<T>(string key);
        void Set<T>(string key, T data, int minutes);
		bool IsSet(string key);
		void Remove(string key);
		void RemoveByPattern(string pattern);
		void Clear();
	}

	public interface IPerRequestCacheManager
	{
        T Get<T>(string key);
        void Set<T>(string key, T data, int minutes);
		bool IsSet(string key);
		void Remove(string key);
		void RemoveByPattern(string pattern);
		void Clear();
	}


    /// <summary>
    /// Cache manager interface
    /// </summary>
    public interface ICacheManager
    {
        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="key">The key of the value to get.</param>
        /// <returns>The value associated with the specified key.</returns>
        T Get<T>(string key);

        /// <summary>
        /// Adds the specified key and object to the cache.
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="data">Data</param>
        /// <param name="cacheTime">Cache time</param>
        void Set(string key, object data, int cacheTime);

        /// <summary>
        /// Gets a value indicating whether the value associated with the specified key is cached
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>Result</returns>
        bool IsSet(string key);

        /// <summary>
        /// Removes the value with the specified key from the cache
        /// </summary>
        /// <param name="key">/key</param>
        void Remove(string key);

        /// <summary>
        /// Removes items by pattern
        /// </summary>
        /// <param name="pattern">pattern</param>
        void RemoveByPattern(string pattern);

        /// <summary>
        /// Clear all cache data
        /// </summary>
        void Clear();
    }
}
