﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Nop.Core.Domain.Orders;


namespace Nop.Data.Mapping.Orders
{
    public partial class OrderAttemptMap : EntityTypeConfiguration<OrderAttempt>
    {
        public OrderAttemptMap()
        {
            this.ToTable("OrderAttempt");
            this.HasKey(o => o.Id);
            this.Property(o => o.IpAddress).HasMaxLength(50);
            this.Property(o => o.CustomerId);
            this.Property(o => o.Failed);
            this.Property(o => o.Error).HasMaxLength(4000);
            this.Property(o => o.AttemptedAtUtc);
            this.Property(o => o.Email).HasMaxLength(255);
            this.Property(o => o.Name).HasMaxLength(255);
        }
    }
}
