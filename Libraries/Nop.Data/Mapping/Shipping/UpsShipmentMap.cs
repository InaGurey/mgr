﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Nop.Core.Domain.Shipping;

namespace Nop.Data.Mapping.Shipping
{
    public partial class UpsShipmentMap : EntityTypeConfiguration<UpsShipment>
    {
        public UpsShipmentMap()
        {
            this.ToTable("UpsShipment");

            this.Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(s => s.QuotedPrice).HasPrecision(18, 2);


            this.HasRequired(s => s.Shipment)
                .WithOptional(s => s.UpsShipment)
                .Map(s => s.MapKey("ShipmentId"));

            //this.Property(s => s.ShipmentId)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}