﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Nop.Core.Domain.Catalog;


namespace Nop.Data.Mapping.Catalog
{
    public partial class SavedSearchMap : EntityTypeConfiguration<SavedSearch>
    {
        public SavedSearchMap()
        {
            this.ToTable("SavedSearch");
            this.HasKey(m => m.Id);
            this.Property(m => m.EmailAddress).IsRequired().HasMaxLength(255);
            this.Property(m => m.Frequency).IsRequired().HasMaxLength(50);
            this.Property(m => m.Duration).IsRequired().HasMaxLength(50);
            this.Property(m => m.SearchParametersQuerystring).IsRequired();
            this.Property(m => m.StartDate);
            this.Property(m => m.EndDate);
            this.Property(m => m.SiteId);
            this.Property(m => m.NotifyOnlyWhenNewResults);
            this.Property(m => m.CreatedOnUtc);
            this.Property(m => m.UpdatedOnUtc);
            this.Property(m => m.DeletedOnUtc);
            this.Property(m => m.Slug).HasMaxLength(50);
            this.Ignore(m => m.SearchParameters);
        }
    }
}
