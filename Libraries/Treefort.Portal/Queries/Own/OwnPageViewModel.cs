﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Own
{
    public class OwnPageViewModel
    {
        public MetaData Meta { get; set; }
        public string PageBody { get; set; }
        public string PageTitle { get; set; }
        public IList<Files> File { get; set; }

        public class Files
        {
            public string FileUrl { get; set; }
            public int? Width { get; set; }
            public int? Height { get; set; }
            public string Caption { get; set; }
        }
    }

    public class MetaData
    {
        public string Title { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
    }
}
