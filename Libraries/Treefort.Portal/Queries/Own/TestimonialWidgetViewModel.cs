﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Treefort.Portal.Queries.Own
{
    public class TestimonialWidgetViewModel
    {
        public IList<VideoPlayerSettings> Videos { get; set; }
    }


    public class VideoPlayerSettings
    {
        public bool AutoPlay { get; set; }
        public string Description { get; set; }
        public bool EnableDownload { get; set; }
        public string ExpressInstall { get; set; }
        public string Id { get; set; }
        public string Mp4 { get; set; }
        public string Ogg { get; set; }
        public string Swf { get; set; }
        public string Poster { get; set; }
        public System.Drawing.Size Size { get; set; }
        public string Title { get; set; }

        public VideoPlayerSettings()
        {
            Id = string.Format("v{0}", Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 12));
        }

        public VideoPlayerSettings(XElement element)
        {
            this.Id = element.Attribute("video_uid").Value;
            this.Description = element.Attribute("description").Value;
            this.Title = element.Attribute("title").Value;
            this.Mp4 = element.Attribute("Mp4").Value;
            this.AutoPlay = true;
        }
    }
}