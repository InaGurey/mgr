﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using RemoteControl.Services;

namespace Treefort.Portal.Queries.Own
{
    public interface ITerritoriesViewModelQuery
    {
        TerritoriesViewModel Execute();
    }

    public class TerritoriesViewModelQuery : ITerritoriesViewModelQuery
    {
        private readonly IWebPageService _webPageService;


        public TerritoriesViewModelQuery(IWebPageService webPageService)
        {
            _webPageService = webPageService;
        }

        public TerritoriesViewModel Execute()
        {
            var accountId = KnownIds.RemoteControl.MGR;
            var relatedFileBaseUrl = "http://play-it-again-sports.rcfile.treefortrack.com/relatedfiles/";//new Guid(ConfigurationManager.AppSettings["RemoteControl.AccountId"]);

            _webPageService.IncludeRelatedFiles = true;
            _webPageService.RelatedFileBaseUrl = relatedFileBaseUrl;

            var model = new TerritoriesViewModel();

            var w = _webPageService.GetByRewriteUrl(accountId, "/Own/Territories");

            model.PageBody = w.Body;
            model.PageTitle = w.Title;
            model.Meta = new MetaData()
            {
                MetaDescription = w.MetaDescription,
                MetaKeywords = w.MetaKeywords,
                Title = w.MetaTitle
            }; 
            model.TerritoryInfoBaseUrl = "/Own/Territories";


            return model;
        }

    }
}