﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using RemoteControl.Services;

namespace Treefort.Portal.Queries.Own
{
    public interface IOwnPageViewModelQuery
    {
        OwnPageViewModel Execute(string url);
    }
    public class OwnPageViewModelQuery : IOwnPageViewModelQuery
    {
         private readonly IWebPageService _webPageService;


         public OwnPageViewModelQuery(IWebPageService webPageService)
        {
            _webPageService = webPageService;
        }

         public OwnPageViewModel Execute(string url)
        {
            var accountId = KnownIds.RemoteControl.MGR;
             var relatedFileBaseUrl = "http://play-it-again-sports.rcfile.treefortrack.com/relatedfiles/";//new Guid(ConfigurationManager.AppSettings["RemoteControl.AccountId"]);
            _webPageService.IncludeRelatedFiles = true;
            _webPageService.RelatedFileBaseUrl = relatedFileBaseUrl;

            var temp = _webPageService.GetByRewriteUrl(accountId, url);

            var tempList = new List<OwnPageViewModel.Files>();

            foreach (var f in temp.Images)
            {
                var x = new OwnPageViewModel.Files()
                {
                    Caption = f.Caption,
                    FileUrl = f.FileUrl,
                    Height = f.Height,
                    Width = f.Width
                };

                tempList.Add(x);
            }

            var model = new OwnPageViewModel()
            {
                PageBody = temp.Body,
                PageTitle = temp.Title,
                File = tempList,
            };

            var meta = new MetaData()
            {
                MetaDescription = temp.MetaDescription,
                MetaKeywords = temp.MetaKeywords,
                Title = temp.MetaTitle
            };

            model.Meta = meta;

            return model;
        }

    }
}