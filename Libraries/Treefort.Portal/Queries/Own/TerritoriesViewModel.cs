﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treefort.Portal.Queries.Own
{
    public class TerritoriesViewModel
    {
        public MetaData Meta { get; set; }
        public string PageBody { get; set; }
        public string PageTitle { get; set; }
        public string TerritoryInfoBaseUrl { get; set; }
    }
}