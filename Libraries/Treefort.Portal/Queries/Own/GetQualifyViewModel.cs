﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Treefort.Portal.Queries.Own
{
    public class GetQualifyViewModel
    {
        public IEnumerable<SelectListItem> Countries { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public IEnumerable<SelectListItem> HowTheyHeardOptions { get; set; }
    }
}