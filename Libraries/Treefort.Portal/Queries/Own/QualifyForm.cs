﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treefort.Portal.Queries.Own
{
        public interface IForm
        {
            IEnumerable<ControllerError> Validate();
        }

        public class QualifyForm : IForm
        {
            public bool? MeetFinancials { get; set; }
            public bool? HasPartner { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string StreetAddress { get; set; }
            public string City { get; set; }
            public string StateProvince { get; set; }
            public string Postal { get; set; }
            public string Country { get; set; }
            public string DayPhone { get; set; }
            public string Email { get; set; }
            public string ConfirmEmail { get; set; }
            public string LocationOfInterest { get; set; }
            public string HowTheyHeard { get; set; }
            public string Comments { get; set; }
            public bool AcceptsTerms { get; set; }

            public IEnumerable<ControllerError> Validate()
            {

                if (!MeetFinancials.HasValue)
                {
                    yield return new ControllerError
                    {
                        Key = "MeetFinancials",
                        Message = "You must state whether you meet the financial requirements of the Music Go Round franchise."
                    };
                }
                else if (!MeetFinancials.Value)
                {
                    if (!HasPartner.HasValue)
                    {
                        yield return new ControllerError
                        {
                            Key = "HasPartner",
                            Message = "You must state whether you have a potential business parter."
                        };
                    }
                }

                if (!AcceptsTerms)
                {
                    yield return new ControllerError
                    {
                        Key = "AcceptsTerms",
                        Message = "You must accept the terms and conditions."
                    };
                }

                if (string.IsNullOrEmpty(FirstName))
                {
                    yield return new ControllerError
                    {
                        Key = "FirstName",
                        Message = "First name is required."
                    };
                }

                if (string.IsNullOrEmpty(LastName))
                {
                    yield return new ControllerError
                    {
                        Key = "LastName",
                        Message = "Last name is required."
                    };
                }

                if (string.IsNullOrEmpty(StreetAddress))
                {
                    yield return new ControllerError
                    {
                        Key = "StreetAddress",
                        Message = "Street address is required."
                    };
                }

                if (string.IsNullOrEmpty(City))
                {
                    yield return new ControllerError
                    {
                        Key = "City",
                        Message = "City is required."
                    };
                }

                if (string.IsNullOrEmpty(StateProvince))
                {
                    yield return new ControllerError
                    {
                        Key = "StateProvince",
                        Message = "State / Province is required."
                    };
                }

                if (string.IsNullOrEmpty(Postal))
                {
                    yield return new ControllerError
                    {
                        Key = "Postal",
                        Message = "Postal code is required."
                    };
                }

                if (string.IsNullOrEmpty(Country))
                {
                    yield return new ControllerError
                    {
                        Key = "Country",
                        Message = "Country is required."
                    };
                }

                if (string.IsNullOrEmpty(DayPhone))
                {
                    yield return new ControllerError
                    {
                        Key = "DayPhone",
                        Message = "Day phone is required."
                    };
                }

                if (string.IsNullOrEmpty(Email))
                {
                    yield return new ControllerError
                    {
                        Key = "Email",
                        Message = "You must enter your email."
                    };
                }
                //else if (!SubSonic.Extensions.Validation.IsEmail(Email))
                //{
                //    yield return new ControllerError
                //    {
                //        Key = "Email",
                //        Message = "Email is not a valid address."
                //    };
                //}
                else if (string.Compare(Email, ConfirmEmail, true) != 0)
                {
                    yield return new ControllerError
                    {
                        Key = "ConfirmEmail",
                        Message = "The 'Confirm Email Address' doesn't match the first email address you entered."
                    };
                }

                if (string.IsNullOrEmpty(LocationOfInterest))
                {
                    yield return new ControllerError
                    {
                        Key = "LocationOfInterest",
                        Message = "Location of interest is required."
                    };
                }

            }
        }

}