﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Own
{
    public class ControllerError
    {
        public string Key { get; set; }
        public string Message { get; set; }
    }
}
