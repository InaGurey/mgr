﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using RemoteControl.Services;

namespace Treefort.Portal.Queries.Own
{
    public interface ITerritoryInfoViewModelQuery
    {
        TerritoryInfoViewModel Execute(string id);
    }


    public class TerritoryInfoViewModelQuery : ITerritoryInfoViewModelQuery
    {
        private readonly ITerritoryService _territoryService;
        private readonly IGeoService _geoService;


        public TerritoryInfoViewModelQuery(ITerritoryService territoryService, IGeoService geoService)
        {
            _territoryService = territoryService;
            _geoService = geoService;

        }

        public TerritoryInfoViewModel Execute(string id)
        {
            var model = new TerritoryInfoViewModel();
            var accountId = KnownIds.RemoteControl.MGR;
            
            var t = _territoryService.GetByCode(accountId, id);

            var territoryList = new List<string>();
            if (!string.IsNullOrWhiteSpace(t.Body))
            {
                territoryList = t.Body.Split("\n".ToCharArray()).ToList<string>();
            }

            model.Territories = territoryList;
            var stpr = _geoService.GetStateProvinceByCode(accountId, id);

            model.StateProvName = stpr.Name;
            model.StateProvCode = stpr.Code;

            return model;

        }
    }
}