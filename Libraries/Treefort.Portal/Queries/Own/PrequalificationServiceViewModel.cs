﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treefort.Portal.Queries.Own
{
    public class PrequalificationServiceViewModel
    {
        public bool MeetFinancials { get; set; }
        public bool HasPartner { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string Postal { get; set; }
        public string Country { get; set; }
        public string DayPhone { get; set; }
        public string Email { get; set; }
        public string LocationOfInterest { get; set; }
        public string HowTheyHeard { get; set; }
        public string Comments { get; set; }
    }
}
