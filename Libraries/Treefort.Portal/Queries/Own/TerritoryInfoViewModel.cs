﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treefort.Portal.Queries.Own
{
    public class TerritoryInfoViewModel
    {
        public List<string> Territories { get; set; }
        public string StateProvName { get; set; }
        public string StateProvCode { get; set; }
    }
}