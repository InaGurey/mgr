﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RemoteControl.Services;

namespace Treefort.Portal.Queries.Own
{
    public interface IGetQualifyViewModelQuery
    {
        GetQualifyViewModel Execute();
    }

    public class GetQualifyViewModelQuery : IGetQualifyViewModelQuery
    {
        private readonly IGeoService _geoService;


        public GetQualifyViewModelQuery(IGeoService geoService)
        {
            _geoService = geoService;
        }

        public GetQualifyViewModel Execute()
        {
            var model = new GetQualifyViewModel();
            var accountId = KnownIds.RemoteControl.MGR;

            // Populate Countries
            var countryList = new List<SelectListItem>();
            var countries = _geoService.GetCountries(accountId);
            if (countries.Count > 0)
            {
                foreach (var c in countries.OrderByDescending(c => c.Code))
                {
                    countryList.Add(new SelectListItem
                    {
                        Text = c.Code,
                        Value = c.Name
                    });
                };
            }
            model.Countries = countryList;

            // Populate States
            var stateList = new List<SelectListItem>();
            var states = _geoService.GetStates(accountId);
            if (states.Count > 0)
            {
                foreach (var s in states.OrderByDescending(s => s.CountryCode).ThenBy(s => s.Name))
                {
                    stateList.Add(new SelectListItem
                    {
                        Text = s.Name,
                        Value = s.Code
                    });
                };
            }
            model.States = stateList;

            // Populate HowTheyHeard Options
            var howTheyHeadOptions = new List<SelectListItem> { 
                new SelectListItem { Value = string.Empty }
                , new SelectListItem{Value = "Brand Website", Text = "Brand Website"}
                , new SelectListItem{Value = "Franchise Trade Publications", Text = "Franchise Trade Publications"}
                , new SelectListItem{Value = "Franchise Trade Websites", Text = "Franchise Trade Websites"}
                , new SelectListItem{Value = "I am a Customer", Text = "I'm A Customer"}
                , new SelectListItem{Value = "I am a Store Employee", Text = "I'm A Store Employee"}
                , new SelectListItem{Value = "I am an Existing Franchise", Text = "I'm An Existing Franchise"}
                , new SelectListItem{Value = "Other", Text = "Other"}
                , new SelectListItem{Value = "Radio Or Tv Commercial", Text = "Radio Or TV Commercial"}
                , new SelectListItem{Value = "Referral From Franchisee", Text = "Referral From Franchisee"}
                , new SelectListItem{Value = "Referral From Friend or Relative", Text = "Referral From Friend Or Relative"}
                , new SelectListItem{Value = "Social Media", Text = "Social Media"}};

            model.HowTheyHeardOptions = howTheyHeadOptions;

            return model;
        }
    }
}