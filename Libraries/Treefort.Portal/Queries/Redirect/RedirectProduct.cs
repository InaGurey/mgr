﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mindscape.LightSpeed;

namespace Treefort.Portal.Queries.Redirect
{
    public interface IRedirectProduct
    {
        RedirectProductViewModel Execute(int id);
    }

    public class RedirectProduct : IRedirectProduct
    {        
        private readonly LightSpeedContext<OldMgrInventoryUnitOfWork> _context;

        public RedirectProduct(LightSpeedContext<OldMgrInventoryUnitOfWork> context)
        {
			_context = context;
		}

        public RedirectProductViewModel Execute(int id)
        {

            using (var uow = _context.CreateUnitOfWork())
            {

                var prod = uow.MgrInventories.FirstOrDefault(x => x.Id == id);

                if (prod == null || prod.BDeleted)
                    return null;

                return new RedirectProductViewModel()
                    {
                        SKU = prod.ChItemSku,
                        StoreNumber = prod.IItemStoreNumber
                    };
            }
        }
    }

    public class RedirectProductViewModel
    {
        public string SKU { get; set; }
        public int StoreNumber { get; set; }
    }
}
