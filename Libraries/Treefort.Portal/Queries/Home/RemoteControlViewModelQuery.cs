﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteControl.Services;
using Treefort.Portal.Queries.Own;

namespace Treefort.Portal.Queries.Home
{
    public interface IRemoteControlViewModelQuery
    {
        RemoteControlPage Execute(string url);
    }

    public class RemoteControlViewModelQuery : IRemoteControlViewModelQuery
    {
        private readonly IWebPageService _webPageService;

        public RemoteControlViewModelQuery(IWebPageService webPageService)
        {
            _webPageService = webPageService;
        }

        public RemoteControlPage Execute(string url)
        {
            var model = new RemoteControlPage();

            //MANAGED CONTENT
            var webPage = _webPageService.GetByRewriteUrl(KnownIds.RemoteControl.MGR, url);
            model.PageBody = webPage.Body;
            model.PageTitle = webPage.Title;

            model.Meta = new MetaData()
            {
                MetaDescription = webPage.MetaDescription,
                MetaKeywords = webPage.MetaKeywords,
                Title = webPage.MetaTitle
            };

            return model;
        }

    }
}
