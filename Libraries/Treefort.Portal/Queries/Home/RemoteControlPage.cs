﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Own;

namespace Treefort.Portal.Queries.Home
{
    public class RemoteControlPage
    {
        public MetaData Meta { get; set; }
        public string PageBody { get; set; }
        public string PageTitle { get; set; }
    }
}
