﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Franchisee
{
    public class MetaData
    {
        public string Title { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
    }
}
