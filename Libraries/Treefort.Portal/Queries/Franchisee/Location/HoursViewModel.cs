﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Franchisee.Location
{
    public class HoursViewModel
    {
        public string monopen { get; set; }
        public string tueopen { get; set; }
        public string wedopen { get; set; }
        public string thuopen { get; set; }
        public string friopen { get; set; }
        public string satopen { get; set; }
        public string sunopen { get; set; }
        public string monclose { get; set; }
        public string tueclose { get; set; }
        public string wedclose { get; set; }
        public string thuclose { get; set; }
        public string friclose { get; set; }
        public string satclose { get; set; }
        public string sunclose { get; set; }
        public int locationID { get; set; }
    }
}
