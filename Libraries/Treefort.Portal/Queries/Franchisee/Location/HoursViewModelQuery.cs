﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mindscape.LightSpeed;

namespace Treefort.Portal.Queries.Franchisee.Location
{
    public interface IHoursViewModelQuery
    {
        HoursViewModel Execute(int locationId);
    }

    public class HoursViewModelQuery : IHoursViewModelQuery
    {
        private readonly LightSpeedContext<PortalUnitOfWork> _context;

        public HoursViewModelQuery(LightSpeedContext<PortalUnitOfWork> context)
        {
			_context = context;
		}

        public HoursViewModel Execute(int locationId)
        {
            using (var uow = _context.CreateUnitOfWork())
            {

                var lhours = uow.LocationHours.FirstOrDefault(x => x.LocationId == locationId);

                if (lhours != null)
                {
                    var mod = new HoursViewModel()
                    {
                        monopen = lhours.MondayOpenTime.HasValue ? lhours.MondayOpenTime.Value.ToString("h:mm tt") : "",
                        monclose = lhours.MondayCloseTime.HasValue ? lhours.MondayCloseTime.Value.ToString("h:mm tt") : "",
                        tueopen = lhours.TuesdayOpenTime.HasValue ? lhours.TuesdayOpenTime.Value.ToString("h:mm tt") : "",
                        tueclose = lhours.TuesdayCloseTime.HasValue ? lhours.TuesdayCloseTime.Value.ToString("h:mm tt") : "",
                        wedopen = lhours.WednesdayOpenTime.HasValue ? lhours.WednesdayOpenTime.Value.ToString("h:mm tt") : "",
                        wedclose = lhours.WednesdayCloseTime.HasValue ? lhours.WednesdayCloseTime.Value.ToString("h:mm tt") : "",
                        thuopen = lhours.ThursdayOpenTime.HasValue ? lhours.ThursdayOpenTime.Value.ToString("h:mm tt") : "",
                        thuclose = lhours.ThursdayCloseTime.HasValue ? lhours.ThursdayCloseTime.Value.ToString("h:mm tt") : "",
                        friopen = lhours.FridayOpenTime.HasValue ? lhours.FridayOpenTime.Value.ToString("h:mm tt") : "",
                        friclose = lhours.FridayCloseTime.HasValue ? lhours.FridayCloseTime.Value.ToString("h:mm tt") : "",
                        satopen = lhours.SaturdayOpenTime.HasValue ? lhours.SaturdayOpenTime.Value.ToString("h:mm tt") : "",
                        satclose = lhours.SaturdayCloseTime.HasValue ? lhours.SaturdayCloseTime.Value.ToString("h:mm tt") : "",
                        sunopen = lhours.SundayOpenTime.HasValue ? lhours.SundayOpenTime.Value.ToString("h:mm tt") : "",
                        sunclose = lhours.SundayCloseTime.HasValue ? lhours.SundayCloseTime.Value.ToString("h:mm tt") : "",
                        locationID = lhours.LocationId
                    };
                    return mod;
                }

            }

            return null;
        }
    }
}
