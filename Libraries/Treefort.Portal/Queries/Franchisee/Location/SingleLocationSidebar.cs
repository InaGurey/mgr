﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Franchisee.Website;

namespace Treefort.Portal.Queries.Franchisee.Location
{
    public class SingleLocationSidebar
    {
        public WebsiteOptions WebsiteOptions { get; set; }
        public GenericLocation Location { get; set; }
        public IList<GenericLocation> CrossBrandLocations { get; set; }
        public HoursViewModel StoreHours { get; set; }
        public IList<GenericPageViewModel.AdditionalPageNav> AdditionalPagesList { get; set; }
    }
}
