﻿using System.Collections.Generic;
using System.Linq;
using Treefort.Common;
using WinmarkFranchise.Domain;

namespace Treefort.Portal.Queries.Franchisee.Location
{
	public interface IGenericLocationViewModelQuery
	{
		List<GenericLocation> Execute(int accountId);
	}

	public class GenericLocationViewModelQuery : IGenericLocationViewModelQuery
	{
	    private readonly IWinmarkFranchiseUnitOfWork _uow;

		public GenericLocationViewModelQuery(IWinmarkFranchiseUnitOfWork uow) {
		    _uow = uow;
		}

		public List<GenericLocation> Execute(int accountId)
		{
		    var account = _uow.Accounts.GetById(accountId);
		    var locations = _uow.Locations.GetByAccount(account);
		    var currentFranchiseId = _uow.Franchises.GetByAbbreviation("mgr").Id;

		    return locations.Select(l => new GenericLocation
		    {
		        Id = l.Id,
		        Name = l.Name,
		        Address1 = l.Address.Line1,
		        Address2 = l.Address.Line2,
		        City = l.Address.City,
		        Region = l.Address.Region,
		        PostalCode = l.Address.PostalCode,
		        StoreNumber = l.StoreNumber,
		        Directions = l.Directions,
		        Email = l.EmailAddress,
		        Lat = l.GeographicCoordinates.HasValue ? l.GeographicCoordinates.Value.Latitude : 0d,
                Lon = l.GeographicCoordinates.HasValue ? l.GeographicCoordinates.Value.Longitude : 0d,
		        Phone = l.PhoneNumber,
		        Website = l.WebsiteUrl,
		        ImageFileName = l.ImageReference,
		        IsGoldStandard = l.IsGold,
		        HasGiftCard = l.HasGiftCards,
		        ConstactContactUrl = l.ConstantContactUrl,
		        FacebookUrl = l.FacebookUrl,
		        InstagramUrl = l.InstagramUrl,
		        TwitterUrl = l.TwitterUrl,
		        YouTubeUrl = l.YouTubeUrl,
		        LinkedInUrl = l.LinkedInUrl,
		        FourSquareUrl = l.FoursquareUrl,
                FranchiseId = l.FranchiseId,
                FranchiseName = l.Franchise.Name,
                IsCrossBrand = l.FranchiseId != currentFranchiseId,
                CrossBrandUrl =  l.FranchiseId != currentFranchiseId ? _uow.Domains.GetByLocation(l).IfNotNull(d => d.Name, string.Empty) : string.Empty
		    }).Distinct().ToList();
		}
	}
}