﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Franchisee.Location
{
    public class GenericLocation
    {
        public int Id { get; set; }
        public string StoreNumber { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Directions { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public bool? HasGiftCard { get; set; }
        public string ConstactContactUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string FourSquareUrl { get; set; }
        public string YouTubeUrl { get; set; }
        public string LinkedInUrl { get; set; }
        public bool? IsGoldStandard { get; set; }
        public string ImageFileName { get; set; }
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public bool IsCrossBrand { get; set; }
        public string CrossBrandUrl { get; set; }
    }
}
