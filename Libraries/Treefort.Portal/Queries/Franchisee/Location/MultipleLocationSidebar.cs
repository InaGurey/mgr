﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Franchisee.Website;

namespace Treefort.Portal.Queries.Franchisee.Location
{
    public class MultipleLocationSidebar
    {
        public WebsiteOptions WebsiteOptions { get; set; }
        public List<GenericLocation> Locations { get; set; }
        public bool IsCoop { get; set; }
    }
}
