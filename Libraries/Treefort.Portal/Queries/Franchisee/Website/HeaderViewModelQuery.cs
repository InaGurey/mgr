﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mindscape.LightSpeed;

namespace Treefort.Portal.Queries.Franchisee.Website
{
    public interface IHeaderViewModelQuery
    {
        HeaderViewModel Execute(int websiteId);
        FranchiseeMobileNavigationViewModel GetFranchiseMobileNavigation(int websiteId);
    }

    public class HeaderViewModelQuery : IHeaderViewModelQuery
    {
          private readonly LightSpeedContext<PortalUnitOfWork> _context;

          public HeaderViewModelQuery(LightSpeedContext<PortalUnitOfWork> context)
          {
			_context = context;
		}

        public HeaderViewModel Execute(int websiteId)
        {
            var model = new HeaderViewModel();

            using (var uow = _context.CreateUnitOfWork())
            {
                var pageTypes = uow.PageTypes.Where(x => x.FranchiseId == KnownIds.Franchise.MGR).ToList();
                var currentPages = uow.Pages.Where(x => x.WebsiteId == websiteId).ToList();

                var pageList = (from p in pageTypes
                                join c in currentPages on p.Id equals c.PageTypeId
                                select new { c.IsActive, p.Code }).ToList();

                model = new HeaderViewModel()
                {
                    IsVisibleAbout = pageList.Any(x => x.Code == "about") && pageList.Single(x => x.Code == "about").IsActive,
                    IsVisibleMostWanted = pageList.Any(x => x.Code == "mostwanted") && pageList.Single(x => x.Code == "mostwanted").IsActive,
                    IsVisibleCommunity = pageList.Any(x => x.Code == "community") && pageList.Single(x => x.Code == "community").IsActive,
                    IsVisiblePromotions = pageList.Any(x => x.Code == "promotions") && pageList.Single(x => x.Code == "promotions").IsActive,
                    IsVisibleLessons = pageList.Any(x => x.Code == "lessons") && pageList.Single(x => x.Code == "lessons").IsActive,
                    IsVisibleRepairs = pageList.Any(x => x.Code == "repairs") && pageList.Single(x => x.Code == "repairs").IsActive,
                    IsVisibleNewGear = pageList.Any(x => x.Code == "newgear") && pageList.Single(x => x.Code == "newgear").IsActive,
                    IsVisibleJobs = pageList.Any(x => x.Code == "jobs") && pageList.Single(x => x.Code == "jobs").IsActive
                };
            }

            return model;
        }

        public FranchiseeMobileNavigationViewModel GetFranchiseMobileNavigation(int websiteId)
        {
            var model = new FranchiseeMobileNavigationViewModel();

            using (var uow = _context.CreateUnitOfWork())
            {
                var pageTypes = uow.PageTypes.Where(x => x.FranchiseId == KnownIds.Franchise.MGR && x.IsMobile).ToList();
                var currentPages = uow.Pages.Where(x => x.WebsiteId == websiteId).ToList();

                var pageList = (from p in pageTypes
                                join c in currentPages on p.Id equals c.PageTypeId
                                select new { c.IsActive, p.Code }).ToList();

                model = new FranchiseeMobileNavigationViewModel()
                {
                    IsVisibleAbout = pageList.Any(x => x.Code == "about_mobile") && pageList.Single(x => x.Code == "about_mobile").IsActive,
                    IsVisibleCommunity = pageList.Any(x => x.Code == "community_mobile") && pageList.Single(x => x.Code == "community_mobile").IsActive,
                    IsVisiblePromotions = pageList.Any(x => x.Code == "promo_mobile") && pageList.Single(x => x.Code == "promo_mobile").IsActive,
                    IsVisibleLessons = pageList.Any(x => x.Code == "lessons_mobile") && pageList.Single(x => x.Code == "lessons_mobile").IsActive,
                    IsVisibleRepairs = pageList.Any(x => x.Code == "repairs_mobile") && pageList.Single(x => x.Code == "repairs_mobile").IsActive,
                    IsVisibleMostWanted = pageList.Any(x => x.Code == "wanted_mobile") && pageList.Single(x => x.Code == "wanted_mobile").IsActive,
                    IsVisibleNewGear = pageList.Any(x => x.Code == "newgear_mobile") && pageList.Single(x => x.Code == "newgear_mobile").IsActive,
                    IsVisibleJobs = pageList.Any(x => x.Code == "jobs_mobile") && pageList.Single(x => x.Code == "jobs_mobile").IsActive
                };
            }

            return model;
        }
    }
}
