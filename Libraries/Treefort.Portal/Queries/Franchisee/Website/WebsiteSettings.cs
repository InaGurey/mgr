﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Franchisee.Website
{
    public class WebsiteSettings
    {
        public int WebsiteId { get; set; }
        public int AccountId { get; set; }
        public string GoogleAnalyticsId { get; set; }
        public bool IsActive { get; set; }
        public bool AccountIsActive { get; set; }
        public bool NeedsPermanentRedirect { get; set; }
        public bool PreviewMode { get; set; }
        public int PreviewRevisionNumber { get; set; }
        public bool IsCoop { get; set; }
        public string Domain { get; set; }
        public WebsiteOptions WebsiteOptions { get; set; }
    }
}
