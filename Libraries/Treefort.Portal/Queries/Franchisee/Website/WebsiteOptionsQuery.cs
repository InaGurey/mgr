﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Mindscape.LightSpeed;
using Treefort.Common;
using WinmarkFranchise.Domain;

namespace Treefort.Portal.Queries.Franchisee.Website
{
	public interface IWebsiteOptionsQuery
	{
		WebsiteOptions Execute(int siteId);
	}

	public class WebsiteOptionsQuery : IWebsiteOptionsQuery
	{
	    private readonly IWinmarkFranchiseUnitOfWork _uow;

		public WebsiteOptionsQuery(IWinmarkFranchiseUnitOfWork uow) {
		    _uow = uow;
		}

		public WebsiteOptions Execute(int siteId)
		{
		    var website = _uow.Websites.GetById(siteId);
		    var mgrO = website.IfNotNull(w => _uow.MgrOptions.GetByWebsite(w)) ?? _uow.MgrOptions.GetDefault();

		    var options = new WebsiteOptions()
		    {
                ShowBuySellStatement = mgrO.ShowBuySellStatement,
                ShowBuySellSubtext = mgrO.ShowBuySellSubtext,
                BuySellSubtext = mgrO.BuySellSubtext,
                LogoSubtext = mgrO.LogoSubtext,
                HomepageContentAboveBuySellStatement = mgrO.HomepageContentAboveBuySellStatement,
                AboveMapText1 = mgrO.AboveMapText1,
                AboveMapText2 = mgrO.AboveMapText2,
                DomainVerification = mgrO.DomainVerification,
                HomepageProductRowOptions = new []
                {
                    new HomepageProductRowOptions
                    {
                        CategoryId = mgrO.HomepageRow1CategoryId,
                        Count = mgrO.HomepageRow1Number,
                        Visible = mgrO.HomepageRow1Visible,
                        Selection = mgrO.HomepageRow1Selection,
                        MinPrice = mgrO.HomepageRow1MinPrice
                    },
                    new HomepageProductRowOptions
                    {
                        CategoryId = mgrO.HomepageRow2CategoryId,
                        Count = mgrO.HomepageRow2Number,
                        Visible = mgrO.HomepageRow2Visible,
                        Selection = mgrO.HomepageRow2Selection,
                        MinPrice = mgrO.HomepageRow2MinPrice
                    },
                    new HomepageProductRowOptions
                    {
                        CategoryId = mgrO.HomepageRow3CategoryId,
                        Count = mgrO.HomepageRow3Number,
                        Visible = mgrO.HomepageRow3Visible,
                        Selection = mgrO.HomepageRow3Selection,
                        MinPrice = mgrO.HomepageRow3MinPrice
                    }
                },
                SellYourGearRecipient = mgrO.SellYourGearRecipient,
                AuthorizeNetApiLogin = mgrO.AuthorizeNetApiLogin,
                AuthorizeNetTxKey = mgrO.AuthorizeNetTxKey,
                PayPalEmail = mgrO.PayPalEmail,
                BuySellStatementAboveProducts = mgrO.BuySellStatementAboveProducts,
                HeaderEmbedScript = mgrO.HeadEmbed,
                FooterEmbedScript = mgrO.FooterEmbed,
                GoogleAdwordsConversionId = mgrO.GoogleAdwordsConversionId,
                HomepageHeroImageUrl = mgrO.HomepageHeroImageUrl,
                LocationTextLine1 = mgrO.LocationTextLine1,
                LocationTextLine2 = mgrO.LocationTextLine2
		    };

		    if (siteId == KnownIds.Website.Corporate)
		    {
		        options.HomepageProductRowOptions[0].MinPrice = 500.01M;
		        options.HomepageProductRowOptions[1].MinPrice = 200.01M;
		    }
		    else
		    {
		        var location = website.Account.PrimaryLocation;
		        if (location != null)
		        {
		            options.LocationName = location.Name;
		            options.City = location.Address.City;
		            options.State = location.Address.Region;
		            options.IsGoldStar = location.IsGold;
		        }
		    }

			return options;
		}
	}
}
