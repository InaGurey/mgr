﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Treefort.Common;
using WinmarkFranchise.Domain;
using WinmarkFranchise.Domain.Websites;


namespace Treefort.Portal.Queries.Franchisee.Website
{
    public interface IWebsiteSettingsQuery
    {
        WebsiteSettings Execute(int siteId);
    }

    public class WebsiteSettingsQuery : IWebsiteSettingsQuery
    {
        private readonly IWinmarkFranchiseUnitOfWork _uow;
        private readonly IWebsiteOptionsQuery _websiteOptionsQuery;
        private readonly HttpContextBase _httpContext;
        private readonly IDomainNameService _domainNameService;

        public WebsiteSettingsQuery(
                IWinmarkFranchiseUnitOfWork uow,
                IWebsiteOptionsQuery websiteOptionsQuery,
                HttpContextBase httpContext,
                IDomainNameService domainNameService)
        {
            _uow = uow;
            _websiteOptionsQuery = websiteOptionsQuery;
            _httpContext = httpContext;
            _domainNameService = domainNameService;
        }

        public WebsiteSettings Execute(int siteId)
        {
            var website = _uow.Websites.GetById(siteId);
            if (website == null)
                return new WebsiteSettings {
                    IsActive = false,
                    AccountIsActive = false,
                    NeedsPermanentRedirect = true
                };

            var model = new WebsiteSettings {
                WebsiteId = siteId,
                AccountId = website.AccountId,
                PreviewMode = false,
                IsActive = website.IsActive,
                AccountIsActive = website.Account.Status.IsActive,
                IsCoop = website.Account.IsCoop,
                NeedsPermanentRedirect = website.Account.Status.IsClosed,
                WebsiteOptions = _websiteOptionsQuery.Execute(website.Id),
            };

            var url = _httpContext.Request.Url;

            if (_domainNameService.IsTestDomain(url)) {
                var querystring = url.Query.IfNotNullOrEmpty(HttpUtility.ParseQueryString, new System.Collections.Specialized.NameValueCollection());
                model.Domain = querystring["previewdomain"].IfNotNullOrEmpty(d => _domainNameService.GetSecondLevelDomain(d));

                int parse;
                model.PreviewRevisionNumber = querystring["revision"].IfNotNullOrEmpty(r => int.TryParse(r, out parse) ? parse : -1, -1);

                model.PreviewMode = true;
                model.IsActive = true;
                model.AccountIsActive = true;
                model.NeedsPermanentRedirect = false;
                model.GoogleAnalyticsId = "UA-70616621-2";
            }
            else {
                var domain = website.PrimaryDomain;
                if (domain == null || !domain.IsActive || domain.WebsiteId != website.Id) {
                    model.IsActive = false;
                    model.AccountIsActive = false;
                    model.NeedsPermanentRedirect = true;
                }
                else {
                    model.Domain = domain.Name.ToLowerInvariant();
                    model.GoogleAnalyticsId = domain.GoogleAnalyticsProfileId ?? website.PrimaryDomain.IfNotNull(d => d.GoogleAnalyticsProfileId) ?? string.Empty;
                }
            }

            return model;
        }
    }
}
