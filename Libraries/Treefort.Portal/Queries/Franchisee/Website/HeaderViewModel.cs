﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Franchisee.Website
{
    public class HeaderViewModel
    {
        public bool IsVisibleAbout { get; set; }
        public bool IsVisibleNewGear { get; set; }
        public bool IsVisibleMostWanted { get; set; }
        public bool IsVisibleCommunity { get; set; }
        public bool IsVisiblePromotions { get; set; }
        public bool IsVisibleLessons { get; set; }
        public bool IsVisibleRepairs { get; set; }
        public bool IsVisibleJobs { get; set; }
    }

    public class FranchiseeMobileNavigationViewModel
    {
        public bool IsVisibleAbout { get; set; }
        public bool IsVisibleMostWanted { get; set; }
        public bool IsVisibleNewGear { get; set; }
        public bool IsVisibleCommunity { get; set; }
        public bool IsVisiblePromotions { get; set; }
        public bool IsVisibleLessons { get; set; }
        public bool IsVisibleRepairs { get; set; }
        public bool IsVisibleJobs { get; set; }
    }
}
