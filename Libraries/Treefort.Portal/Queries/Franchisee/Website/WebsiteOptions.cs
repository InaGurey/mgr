﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Franchisee.Website
{
    public class WebsiteOptions
    {
        public bool ShowBuySellStatement { get; set; }
        public bool ShowBuySellSubtext { get; set; }
        public string BuySellSubtext { get; set; }
        public string LogoSubtext { get; set; }
        public bool HomepageContentAboveBuySellStatement { get; set; }
        public string AboveMapText1 { get; set; }
        public string AboveMapText2 { get; set; }
        public string DomainVerification { get; set; }
		public HomepageProductRowOptions[] HomepageProductRowOptions { get; set; }
		public string SellYourGearRecipient { get; set; }
		public string AuthorizeNetApiLogin { get; set; }
		public string AuthorizeNetTxKey { get; set; }
		public string PayPalEmail { get; set; }
		public bool BuySellStatementAboveProducts { get; set; }
        public string HeaderEmbedScript { get; set; }
        public string FooterEmbedScript { get; set; }
        public string GoogleAdwordsConversionId { get; set; }
        public string LocationName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public bool IsGoldStar { get; set; }
        public string HomepageHeroImageUrl { get; set; }
        public string LessonsCalloutText { get; set; }
        public string LocationTextLine1 { get; set; }
        public string LocationTextLine2 { get; set; }

        public bool HasAdwordsConversion
        {
            get { return !string.IsNullOrWhiteSpace(GoogleAdwordsConversionId); }
        }
    }

	public class HomepageProductRowOptions
	{
		public bool Visible { get; set; }
		public int? CategoryId { get; set; }
		public int? Count { get; set; }
		public string Selection { get; set; }
		public decimal? MinPrice { get; set; }
	}
}
