﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using HtmlAgilityPack;
using Mindscape.LightSpeed;
using WinmarkFranchise.Domain;


namespace Treefort.Portal.Queries.Franchisee
{
    public interface IGenericPageViewModelQuery
    {
        GenericPageViewModel Execute(string pageCode, IWebsite website, bool previewSite, int previewRevisionNumber, int pageId = 0);
        IList<GenericPageViewModel.AdditionalPageNav> GetAdditionalPageNavs(int websiteId, int selectedPageId = 0);
        IList<GenericPageViewModel.CouponWidget> GetCouponWidgets(int websiteId, bool previewSite, int previewRevisionNumber);
        IList<GenericPageViewModel.CommunityEventWidget> GetCommunityEventWidgets(int websiteId, bool previewSite, int previewRevisionNumber);
    }

    public class GenericPageViewModelQuery : IGenericPageViewModelQuery
    {
        private readonly IWinmarkFranchiseUnitOfWork _uow;

        public GenericPageViewModelQuery(IWinmarkFranchiseUnitOfWork uow) { _uow = uow; }

        public GenericPageViewModel Execute(string pageCode, IWebsite website, bool previewSite = false, int previewRevisionNumber = 0, int pageId = 0)
        {
            var revisionNumber = 0;
            var model = new GenericPageViewModel();

            var page = GetPage(pageCode, website, previewSite, previewRevisionNumber, pageId);
            if (page != null && (page.IsActive || previewSite))
            {
                //setting the revision number to either the querystring revision number or the current one if it isn't a preview site

                revisionNumber = previewSite
                    ? previewRevisionNumber < 0
                        ? page.CurrentRevisionNumber
                        : previewRevisionNumber
                    : page.CurrentRevisionNumber;

                if (revisionNumber == 0)
                {
                    revisionNumber = page.CurrentRevisionNumber;
                }

                //getting the current page revision
                var pageRevision = _uow.PageRevisions.GetByPageRevisionNumber(page, revisionNumber);

                if (pageRevision == null)
                    return null;


                var widgets =
                        _uow.Widgets.GetByPageRevisionNumber(page, pageRevision.RevisionNumber);


                var widgetList = new List<GenericPageViewModel.Widget>();
                var isMobile = page.PageType.IsMobile;
                var mgrOptions = _uow.MgrOptions.GetByWebsite(website);

                //NOTE: All new widgets will have to be added after this and added to the model
                foreach (var w in widgets)
                {
                    var currentWidgetRevision = _uow.WidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                    //it should exist by this point, but if it doesn't....skip
                    if (currentWidgetRevision == null)
                        continue;

                    //skip if the widget is not active
                    if (!currentWidgetRevision.IsActive)
                        continue;


                    //Add additional Widgets here
                    //------------------------------
                    switch (w.WidgetTypeId)
                    {
                        //Text Widget
                        case 1:
                            var textWidget = _uow.TextWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (textWidget == null)
                                continue;

                            if (textWidget.UseDateRange && !(textWidget.StartDate <= DateTime.Now && DateTime.Now <= textWidget.EndDate))
                                continue;

                            var textContent = textWidget.Content;

                            //Need to pull out the width and height put on images by the wysiwyg. That way I can do max-width for mobile
                            if (isMobile && !string.IsNullOrEmpty(textContent))
                            {
                                var document = new HtmlDocument();
                                document.LoadHtml(textContent);

                                var images = document.DocumentNode.SelectNodes("//img");

                                if (images != null)
                                {
                                    foreach (var image in images)
                                    {
                                        if (image.Attributes["width"] != null)
                                            image.Attributes["width"].Remove();

                                        if (image.Attributes["height"] != null)
                                            image.Attributes["height"].Remove();
                                    }
                                }

                                textContent = document.DocumentNode.OuterHtml;
                            }



                            widgetList.Add(new GenericPageViewModel.Widget
                            {
                                TextWidgets = new GenericPageViewModel.TextWidget
                                {
                                    Content = textContent
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 1,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });

                            break;
                        //Embed Widget
                        case 3:
                            var embedWidget =
                                _uow.EmbedWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (embedWidget == null)
                                continue;

                            widgetList.Add(new GenericPageViewModel.Widget
                            {
                                EmbedWidgets = new GenericPageViewModel.EmbedWidget
                                {
                                    Code = embedWidget.Content
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 3,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });

                            break;

                        //Job Widget
                        case 4:
                            var jobsWidget =
                                _uow.JobWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (jobsWidget == null)
                                continue;

                            if (DateTime.Now > jobsWidget.ExpireDate)
                                continue;

                            widgetList.Add(new GenericPageViewModel.Widget
                            {
                                JobWidgets = new GenericPageViewModel.JobWidget
                                {
                                    Title = jobsWidget.Title,
                                    Compensation = jobsWidget.Compensation,
                                    Description = jobsWidget.Description,
                                    Summary = jobsWidget.Summary
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 4,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });

                            break;
                        //special Offer Widget
                        case 5:
                            var specialOfferWidget =
                                _uow.SpecialOfferWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (specialOfferWidget == null)
                                continue;

                            if (specialOfferWidget.EndDate > DateTime.Now && specialOfferWidget.StartDate < DateTime.Now)
                            {
                                widgetList.Add(new GenericPageViewModel.Widget
                                {
                                    SpecialOfferWidgets = new GenericPageViewModel.SpecialOfferWidget
                                    {
                                        Title = specialOfferWidget.Title,
                                        Text = specialOfferWidget.Text,
                                        ImageReference = specialOfferWidget.ImageReference,
                                        ImageAltText = specialOfferWidget.ImageAltText,
                                        DocumentReference = specialOfferWidget.DocumentReference
                                    },
                                    Order = currentWidgetRevision.SequenceNumber,
                                    Code = 5,
                                    PageRegion = new GenericPageViewModel.Region
                                    {
                                        Code = w.PageRegion.Code,
                                        SequenceNumber = w.PageRegion.SequenceNumber
                                    }
                                });
                            }

                            break;
                        //Event Widget
                        case 7:
                            var eventWidget =
                                _uow.EventWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (eventWidget == null)
                                continue;


                            if (eventWidget.EndDate > DateTime.Now && eventWidget.StartDate < DateTime.Now)
                            {

                                widgetList.Add(new GenericPageViewModel.Widget
                                {
                                    ThreeColumnsWidgets = new GenericPageViewModel.EventWidget
                                    {
                                        Title = eventWidget.Title,
                                        Subtitle1 = eventWidget.Subtitle,
                                        Subtitle2 = eventWidget.SecondTitle,
                                        Subtitle3 = eventWidget.ThirdTitle,
                                        ImageReference1 = eventWidget.TitleImageReference,
                                        ImageReference2 = eventWidget.ImageReference,
                                        ImageReference3 = eventWidget.ImageReference2,
                                        Image1AltText = eventWidget.TitleImageAltText,
                                        Image2AltText = eventWidget.Image1AltText,
                                        Image3AltText = eventWidget.Image2AltText,
                                        Text1 = eventWidget.Text,
                                        Text2 = eventWidget.SecondText,
                                        Text3 = eventWidget.ThirdText
                                    },
                                    Order = currentWidgetRevision.SequenceNumber,
                                    Code = 7,
                                    PageRegion = new GenericPageViewModel.Region
                                    {
                                        Code = w.PageRegion.Code,
                                        SequenceNumber = w.PageRegion.SequenceNumber
                                    }
                                });
                            }
                            break;
                        //Product Grid Widget
                        case 8:
                            var productGridWidget =
                                _uow.ProductGridWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (productGridWidget == null)
                                continue;


                            if (productGridWidget.EndDate > DateTime.Now && productGridWidget.StartDate < DateTime.Now)
                            {
                                var pgwidget = new GenericPageViewModel.ProductGridWidget
                                {
                                    Theme = productGridWidget.Theme,
                                    Title = productGridWidget.Title
                                };
                                for (var i = 1; i <= 4; i++)
                                {
                                    var fieldName = "Product" + i;
                                    var bundle = productGridWidget.GetType()
                                        .GetProperty(fieldName)
                                        .GetValue(productGridWidget, null) as IWidgetImageBundle;

                                    var bundleModel = GetImageBundle(bundle);
                                    if (bundleModel == null) continue;
                                    pgwidget.Products.Add(bundleModel);
                                }

                                if (pgwidget.Products.Count == 0) continue;

                                widgetList.Add(new GenericPageViewModel.Widget
                                {
                                    ProductGridWidgets = pgwidget,
                                    Order = currentWidgetRevision.SequenceNumber,
                                    Code = 8,
                                    PageRegion = new GenericPageViewModel.Region
                                    {
                                        Code = w.PageRegion.Code,
                                        SequenceNumber = w.PageRegion.SequenceNumber
                                    }
                                });
                            }
                            break;
                        //SlideShowWidget
                        case 9:
                            var slideshowWidget =
                                _uow.SlideshowWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (slideshowWidget == null)
                                continue;

                            var sswidget = new GenericPageViewModel.SlideshowWidget();
                            for (var i = 1; i <= 8; i++)
                            {
                                var fieldName = "Slide" + i;
                                var bundle = slideshowWidget.GetType()
                                    .GetProperty(fieldName)
                                    .GetValue(slideshowWidget, null) as IWidgetImageBundle;

                                var bundleModel = GetImageBundle(bundle);
                                if (bundleModel == null) continue;
                                sswidget.Slides.Add(bundleModel);
                            }

                            if (sswidget.Slides.Count == 0) continue;

                            widgetList.Add(new GenericPageViewModel.Widget
                            {
                                SlideshowWidgets = sswidget,
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 9,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });

                            break;
                        //Simple Text Widget
                        case 12:
                            var simpleTextWidget = _uow.SimpleTextWidgetRevisions.GetByWidgetRevisionNumber(w,
                                revisionNumber);

                            if (simpleTextWidget == null)
                                continue;

                            widgetList.Add(new GenericPageViewModel.Widget
                            {
                                SimpleTextWidgets = new GenericPageViewModel.SimpleTextWidget()
                                {
                                    Content = simpleTextWidget.Content
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 12,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });

                            break;
                        //Two Column Widget
                        case 13:
                            var twoColumnWidget = _uow.TwoColumnWidgetRevisions.GetByWidgetRevisionNumber(w,
                                revisionNumber);

                            if (twoColumnWidget == null)
                                continue;

                            if (twoColumnWidget.StartDate.HasValue && twoColumnWidget.StartDate.Value >= DateTime.Now)
                                continue;

                            if (twoColumnWidget.EndDate.HasValue && twoColumnWidget.EndDate.Value < DateTime.Now)
                                continue;


                            widgetList.Add(new GenericPageViewModel.Widget
                            {
                                TwoColumnWidgets = new GenericPageViewModel.TwoColumnWidget()
                                {
                                    Columns = new List<GenericPageViewModel.TwoColumnWidget.ColumnContent>()
                                    {
                                        new GenericPageViewModel.TwoColumnWidget.ColumnContent()
                                        {
                                            UseDesignedContent = twoColumnWidget.LeftUseDesignedContent,
                                            DesignedContent = twoColumnWidget.LeftDesignedContent,
                                            ImageReference = twoColumnWidget.LeftImageReference,
                                            Text = twoColumnWidget.LeftText,
                                            Title = twoColumnWidget.LeftTitle,
                                            ImageLink = twoColumnWidget.LeftImageLink,
                                            ImageAltText = twoColumnWidget.LeftImageAltText
                                        },
                                        new GenericPageViewModel.TwoColumnWidget.ColumnContent()
                                        {
                                            UseDesignedContent = twoColumnWidget.RightUseDesignedContent,
                                            DesignedContent = twoColumnWidget.RightDesignedContent,
                                            ImageReference = twoColumnWidget.RightImageReference,
                                            Text = twoColumnWidget.RightText,
                                            Title = twoColumnWidget.RightTitle,
                                            ImageLink = twoColumnWidget.RightImageLink,
                                            ImageAltText = twoColumnWidget.RightImageAltText
                                        }
                                    },
                                    StartDate = twoColumnWidget.StartDate,
                                    EndDate = twoColumnWidget.EndDate
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 13,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });

                            break;
                        case 14:
                            var staffWidget = _uow.StaffMemberWidgetRevisions.GetByWidgetRevisionNumber(w,
                                revisionNumber);

                            if (staffWidget == null)
                                continue;

                            widgetList.Add(new GenericPageViewModel.Widget()
                            {
                                StaffWidgets = new GenericPageViewModel.StaffWidget()
                                {
                                    ImageReference = staffWidget.ImageReference,
                                    Name = staffWidget.Name,
                                    Subtitle = staffWidget.Subtitle,
                                    Text = staffWidget.Text,
                                    ImageAltText = staffWidget.ImageAltText,
                                    Phone = staffWidget.Phone,
                                    Email = staffWidget.Email,
                                    Specialty = staffWidget.Specialties,
                                    Rate = staffWidget.Rate,
                                    PaymentMethod = staffWidget.PaymentMethods,
                                    Availability = staffWidget.Availability
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 14,
                                PageRegion = new GenericPageViewModel.Region
                                {
                                    Code = w.PageRegion.Code,
                                    SequenceNumber = w.PageRegion.SequenceNumber
                                }
                            });
                            break;
                        case 18:
                            var couponWidget = _uow.CouponWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (couponWidget == null) continue;

                            if (couponWidget.StartDate.HasValue && couponWidget.StartDate.Value >= DateTime.Now)
                                continue;

                            if (couponWidget.EndDate.HasValue && couponWidget.EndDate.Value < DateTime.Now)
                                continue;

                            widgetList.Add(new GenericPageViewModel.Widget()
                            {
                                CouponWidgets = new GenericPageViewModel.CouponWidget()
                                {
                                    Title = couponWidget.Title,
                                    Subtitle = couponWidget.Subtitle,
                                    Text = couponWidget.Text,
                                    ImageReference = couponWidget.ImageReference,
                                    DocumentReference = couponWidget.DocumentReference,
                                    StartDate = couponWidget.StartDate,
                                    EndDate = couponWidget.EndDate,
                                    DisplayLink = true
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 18
                            });

                            break;
                        case 19:
                            var communityEventWidget = _uow.CommunityEventWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (communityEventWidget == null) continue;

                            if (communityEventWidget.Date.HasValue && communityEventWidget.Date < DateTime.Now)
                                continue;

                            var ceWidgetModel = new GenericPageViewModel.CommunityEventWidget()
                            {
                                Title = communityEventWidget.Title,
                                Date = communityEventWidget.Date,
                                Content = communityEventWidget.Content,
                                Url = communityEventWidget.Url
                            };
                            // Check for previous 
                            var prevWidget = widgetList.LastOrDefault();
                            if (prevWidget != null && (prevWidget.Code == 19 || prevWidget.Code == 99))
                            {
                                // creates new feed
                                if (prevWidget.Code == 19)
                                {
                                    prevWidget.CommunityEventsFeed =
                                        new List<GenericPageViewModel.CommunityEventWidget>
                                        {
                                            prevWidget.CommunityEventWidgets,
                                            ceWidgetModel
                                        };
                                    prevWidget.Code = 99;
                                }
                                else
                                {
                                    // feed already exists
                                    prevWidget.CommunityEventsFeed.Add(ceWidgetModel);
                                }
                            }
                            else
                            {
                                widgetList.Add(new GenericPageViewModel.Widget()
                                {
                                    CommunityEventWidgets = ceWidgetModel,
                                    Order = currentWidgetRevision.SequenceNumber,
                                    Code = 19
                                });
                            }

                            break;
                        case 20:
                            var hiwFeedWidget = _uow.HowItWorksFeedWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (hiwFeedWidget == null) continue;

                            widgetList.Add(new GenericPageViewModel.Widget()
                            {
                                HowItWorksFeedWidget = new GenericPageViewModel.HowItWorksFeedWidget(),
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 20
                            });

                            break;
                        case 21:
                            var lessonsFeedWidget = _uow.LessonsFeedWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (lessonsFeedWidget == null) continue;

                            var lessonsFeedWidgetModel = new GenericPageViewModel.LessonsFeedWidget();
                            if (website.Account.PrimaryLocation != null)
                            {
                                lessonsFeedWidgetModel.LocationName = website.Account.PrimaryLocation.Name;
                                lessonsFeedWidgetModel.Text = mgrOptions.LessonsCalloutText;
                            }
                            widgetList.Add(new GenericPageViewModel.Widget()
                            {
                                LessonsFeedWidget = lessonsFeedWidgetModel,
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 21
                            });

                            break;
                        case 22:
                            var productsFeedWidget = _uow.ProductsFeedWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (productsFeedWidget == null) continue;

                            if (string.IsNullOrWhiteSpace(productsFeedWidget.VisibleCategoryIds)) continue;


                            widgetList.Add(new GenericPageViewModel.Widget()
                            {
                                ProductsFeedWidget = new GenericPageViewModel.ProductsFeedWidget()
                                {
                                    VisibleCategoryIds = productsFeedWidget.VisibleCategoryIds.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList(),
                                    MinimumPrice = productsFeedWidget.MinimumPrice
                                },
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 22
                            });

                            break;
                        case 23:
                            var promotionsFeedWidget = _uow.PromotionsFeedWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                            if (promotionsFeedWidget == null) continue;

                            widgetList.Add(new GenericPageViewModel.Widget()
                            {
                                PromotionsFeedWidget = new GenericPageViewModel.PromotionsFeedWidget(),
                                Order = currentWidgetRevision.SequenceNumber,
                                Code = 23
                            });

                            break;
                        default:
                            continue;
                            break;
                    }

                }

                //Ordering in the way they ordered it
                model.Widgets = widgetList.OrderBy(x => x.Order).ToList();

                model.Title = pageRevision.Headline;
                model.Meta = new MetaData
                {
                    MetaDescription = page.MetaDescription,
                    Title = page.BrowserTitle
                };

                model.IsPreview = previewSite;
                model.PageId = page.Id;

                return model;
            }

            return null;
        }
        public IList<GenericPageViewModel.AdditionalPageNav> GetAdditionalPageNavs(int websiteId, int selectedPageId = 0)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return
                _uow.AdditionalPages.GetPublicAdditionalPages()
                    .Where(ap => ap.Page.WebsiteId == websiteId && ap.Page.IsActive)
                    .Select(ap => new GenericPageViewModel.AdditionalPageNav
                    {
                        PageId = ap.Page.Id,
                        IsSelected = selectedPageId == ap.Page.Id,
                        NavigationTitle = ap.NavigationTitle,
                        NavigationUrl =
                            urlHelper.Action("AdditionalPage", "Franchisee",
                                new {pageId = ap.Page.Id, rewriteUrl = ap.RewriteUrl})
                    }).ToList();
        }

        public GenericPageViewModel.ImageBundleModel GetImageBundle(IWidgetImageBundle bundle)
        {
            if (bundle == null || string.IsNullOrWhiteSpace(bundle.ImageReference)) return null;
            var bundleModel = new GenericPageViewModel.ImageBundleModel
            {
                ImageReference = bundle.ImageReference,
                AltText = bundle.AltText,
                Title = bundle.Title,
                Link = bundle.Link,
                OpenInNewWindow = bundle.OpenInNewWindow != null && bundle.OpenInNewWindow.Value
            };
            return bundleModel;
        }

        public IList<GenericPageViewModel.CouponWidget> GetCouponWidgets(int websiteId, bool previewSite = false, int previewRevisionNumber = 0)
        {
            var couponWidgets = GetWidgetsOnPage(18, (w, revisionNumber, uow) =>
            {
                var couponWidget = _uow.CouponWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                if (couponWidget == null) return null;

                if (couponWidget.StartDate.HasValue && couponWidget.StartDate.Value >= DateTime.Now)
                    return null;

                if (couponWidget.EndDate.HasValue && couponWidget.EndDate.Value < DateTime.Now)
                    return null;

                return new GenericPageViewModel.CouponWidget()
                {
                    Title = couponWidget.Title,
                    Subtitle = couponWidget.Subtitle,
                    Text = couponWidget.Text,
                    ImageReference = couponWidget.ImageReference,
                    DocumentReference = couponWidget.DocumentReference,
                    StartDate = couponWidget.StartDate,
                    EndDate = couponWidget.EndDate,
                    DisplayLink = false
                };

            }, "promotions", websiteId, previewSite, previewRevisionNumber);

            return couponWidgets;
        }

        public IList<GenericPageViewModel.CommunityEventWidget> GetCommunityEventWidgets(int websiteId,
            bool previewSite = false, int previewRevisionNumber = 0)
        {
            var communityEventWidgets = GetWidgetsOnPage(19, (w, revisionNumber, uow) =>
            {
                var communityEventWidget = uow.CommunityEventWidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber);

                if (communityEventWidget == null) return null;

                if (communityEventWidget.Date.HasValue && communityEventWidget.Date < DateTime.Now) return null;

                return new GenericPageViewModel.CommunityEventWidget()
                {
                    Title = communityEventWidget.Title,
                    Date = communityEventWidget.Date,
                    Content = communityEventWidget.Content,
                    Url = communityEventWidget.Url
                };

            }, "community", websiteId, previewSite, previewRevisionNumber);

            return communityEventWidgets;
        }

        protected IList<T> GetWidgetsOnPage<T>(int widgetTypeId, Func<IWidget, int, IWinmarkFranchiseUnitOfWork, T> convertAction, string pageCode, int websiteId, bool previewSite = false, int previewRevisionNumber = 0)
        {

            var pageWidgets = new List<T>();

            var page = GetPage(pageCode, websiteId, previewSite, previewRevisionNumber);

            if (page != null && (page.IsActive || previewSite))
            {
                var revisionNumber = previewSite
                    ? previewRevisionNumber < 0
                        ? page.CurrentRevisionNumber
                        : previewRevisionNumber
                    : page.CurrentRevisionNumber;

                if (revisionNumber == 0)
                {
                    revisionNumber = page.CurrentRevisionNumber;
                }

                var pageRevision = _uow.PageRevisions.GetByPageRevisionNumber(page, revisionNumber);

                if (pageRevision == null)
                    return null;

                var widgets =
                    _uow.Widgets.GetByPageRevisionNumber(page, pageRevision.RevisionNumber)
                        .Where(x => x.WidgetTypeId == widgetTypeId);

                pageWidgets = widgets
                    .Select(w => new {widget = w, rev = _uow.WidgetRevisions.GetByWidgetRevisionNumber(w, revisionNumber)})
                    .Where(x => x.rev != null && x.rev.IsActive)
                    .OrderBy(x => x.rev.SequenceNumber)
                    .Select(x => convertAction(x.widget, revisionNumber, _uow))
                    .Where(x => x != null)
                    .ToList();
            }

            return pageWidgets;
        }

        protected IPage GetPage(string pageCode, int websiteId, bool previewSite = false,
            int previewRevisionNumber = 0, int pageId = 0)
        {

            if (websiteId == KnownIds.Website.Corporate)
                return null;

            var website = _uow.Websites.GetById(websiteId);
            if (website == null)
                return null;

            return GetPage(pageCode, website, previewSite, previewRevisionNumber, pageId);
        }

        protected IPage GetPage(string pageCode, IWebsite website, bool previewSite = false,
            int previewRevisionNumber = 0, int pageId = 0)
        {
            var franchise = _uow.Franchises.GetSpecific(Franchises.MusicGoRound);

            //Getting the page type
            var isAdditionalPage = pageCode.ToLower().Equals("additional") && pageId != 0;
            var pageType = _uow.PageTypes.GetByFranchise(franchise).FirstOrDefault(x => x.Code == pageCode);

            if (pageType == null)
                return null;

            //Finding the page that I'm looking for with the same pagetype
            var page = isAdditionalPage
                ? _uow.Pages.GetAdditionalPagesByWebsite(website).FirstOrDefault(x => x.Id == pageId)
                : _uow.Pages.GetByWebsite(website).FirstOrDefault(x => x.PageTypeId == pageType.Id);

            return page;
        }
    }
}
