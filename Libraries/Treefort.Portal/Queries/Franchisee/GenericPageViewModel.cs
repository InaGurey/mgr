﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Franchisee.Website;
using Treefort.Portal.Queries.Own;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Franchisee
{
    public class GenericPageViewModel
    {
        public MetaData Meta { get; set; }
        public int PageId { get; set; }
        public string Title { get; set; }
        public bool IsPreview { get; set; }
        public WebsiteOptions WebsiteOptions { get; set; }
        public IList<Widget> Widgets { get; set; }
        public bool ShowAdditionalPages { get; set; }


        public class AdditionalPageNav
        {
            public int PageId { get; set; }
            public bool IsSelected { get; set; }
            public string NavigationTitle { get; set; }
            public string NavigationUrl { get; set; }
        }

        //Add Individual Widgets to this generic Widget Class
        public class Widget
        {
            public int Order { get; set; }
            public int Code { get; set; }
            public Region PageRegion { get; set; }
            public TextWidget TextWidgets { get; set; }
            public EmbedWidget EmbedWidgets { get; set; }
            public JobWidget JobWidgets { get; set; }
            public SpecialOfferWidget SpecialOfferWidgets { get; set; }
            public SlideshowWidget SlideshowWidgets { get; set; }
            public ProductGridWidget ProductGridWidgets { get; set; }
            public SimpleTextWidget SimpleTextWidgets { get; set; }
            public EventWidget ThreeColumnsWidgets { get; set; }
            public TwoColumnWidget TwoColumnWidgets { get; set; }
            public StaffWidget StaffWidgets { get; set; }
            public CouponWidget CouponWidgets { get; set; }
            public CommunityEventWidget CommunityEventWidgets { get; set; }
            public HowItWorksFeedWidget HowItWorksFeedWidget { get; set; }
            public LessonsFeedWidget LessonsFeedWidget { get; set; }
            public ProductsFeedWidget ProductsFeedWidget { get; set; }
            public PromotionsFeedWidget PromotionsFeedWidget { get; set; }
            
            // Not really a widget, just a collection/feed
            public IList<CommunityEventWidget> CommunityEventsFeed { get; set; }
        }

        public class Region
        {
            public string Code { get; set; }
            public int SequenceNumber { get; set; }
        }

        //Any Individual Widgets
        public class TextWidget
        {
            public string Content { get; set; }

            public string MobileContent
            {
                get { return string.IsNullOrWhiteSpace(Content) ? Content : Content.GetMobileVersion(); }
            }
        }

        public class EmbedWidget
        {
            public string Code { get; set; }
            public string MobileCode { get { return string.IsNullOrWhiteSpace(Code) ? Code : Code.GetMobileVersion(); } }
        }

        public class JobWidget
        {
            public string Title { get; set; }
            public string Summary { get; set; }
            public string Compensation { get; set; }
            public string Description { get; set; }
        }
        public class SpecialOfferWidget
        {
            public string Title { get; set; }
            public string Text { get; set; }
            public string ImageReference { get; set; }
            public string ImageAltText { get; set; }
            public string DocumentReference { get; set; }
        }

        public class SlideshowWidget
        {
            public SlideshowWidget()
            {
                Slides = new List<ImageBundleModel>();
            }
            public IList<ImageBundleModel> Slides { get; set; }
        }


        public class EventWidget
        {
            public string Title { get; set; }
            public string Subtitle1 { get; set; }
            public string Subtitle2 { get; set; }
            public string Subtitle3 { get; set; }
            public string ImageReference1 { get; set; }
            public string ImageReference2 { get; set; }
            public string ImageReference3 { get; set; }
            public string Text1 { get; set; }
            public string Text2 { get; set; }
            public string Text3 { get; set; }
            public string Image1AltText { get; set; }
            public string Image2AltText { get; set; }
            public string Image3AltText { get; set; }
        }

        public class ProductGridWidget
        {
            public ProductGridWidget()
            {
                Products = new List<ImageBundleModel>();
            }
            public string Theme { get; set; }
            public string Title { get; set; }

            public IList<ImageBundleModel> Products { get; set; }
        }

        public class ImageBundleModel
        {
            public string ImageReference { get; set; }
            public string AltText { get; set; }
            public string Title { get; set; }
            public string Link { get; set; }
            public bool OpenInNewWindow { get; set; }
        }

        public class SimpleTextWidget
        {
            public string Content { get; set; }
        }

        public class TwoColumnWidget
        {
            public List<ColumnContent> Columns { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool MultipleStoreLocation { get; set; }


            public class ColumnContent
            {
                public bool UseDesignedContent { get; set; }
                public string DesignedContent { get; set; }
                public string Title { get; set; }
                public string ImageReference { get; set; }
                public string ImageLink { get; set; }
                public string Text { get; set; }
                public string ImageAltText { get; set; }

            }
        }

        public class StaffWidget
        {
            public string ImageReference { get; set; }
            public string Name { get; set; }
            public string Subtitle { get; set; }
            public string Text { get; set; }
            public string ImageAltText { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string Specialty { get; set; }
            public string Rate { get; set; }
            public string PaymentMethod { get; set; }
            public string Availability { get; set; }
        }


        public class CouponWidget
        {
            public string Title { get; set; }
            public string Subtitle { get; set; }
            public string Text { get; set; }
            public string ImageReference { get; set; }
            public string DocumentReference { get; set; }
            public bool DisplayLink { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }

        public class CommunityEventWidget
        {
            public string Title { get; set; }
            public DateTime? Date { get; set; }
            public string Content { get; set; }
            public string Url { get; set; }
        }

        public class HowItWorksFeedWidget
        {
            
        }
        public class LessonsFeedWidget
        {
            public string LocationName { get; set; }
            public string Text { get; set; }
        }
        public class ProductsFeedWidget
        {
            public ProductsFeedWidget()
            {
                VisibleCategoryIds = new List<int>();
            }
            public IList<int> VisibleCategoryIds { get; set; }
            public decimal? MinimumPrice { get; set; }
        }
        public class PromotionsFeedWidget
        {

        }
    }
}
