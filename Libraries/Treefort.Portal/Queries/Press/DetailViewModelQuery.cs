﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteControl.Services;
using Treefort.Portal.Queries.Own;

namespace Treefort.Portal.Queries.Press
{
    public interface IDetailViewModelQuery
    {
        DetailViewModel Execute(string urlSlug);
    }

    public class DetailViewModelQuery : IDetailViewModelQuery
    {
        private readonly INewsService _newsService;


        public DetailViewModelQuery(INewsService newsService)
        {
            _newsService = newsService;
        }

        public DetailViewModel Execute(string urlSlug)
        {
            var model = new DetailViewModel();

            _newsService.IncludeTags = true;

            var temp = _newsService.GetByRewriteUrl(KnownIds.RemoteControl.MGR, urlSlug);

            model.Body = temp.Body;
            model.Title = temp.Title;

            var meta = new MetaData()
            {
                MetaDescription = temp.MetaDescription,
                MetaKeywords = temp.MetaKeywords,
                Title = temp.MetaTitle
            };

            model.Meta = meta;

            return model;

        }

    }
}
