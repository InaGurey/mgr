﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RemoteControl.Services;
using Treefort.Portal.Queries.Own;

namespace Treefort.Portal.Queries.Press
{
    public interface IIndexViewModelQuery
    {
        IndexViewModel Execute();
    }

    public class IndexViewModelQuery : IIndexViewModelQuery
    {
        private readonly INewsService _newsService;
        private readonly IWebPageService _webPageService;

        public IndexViewModelQuery(IWebPageService webPageService, INewsService newsService)
        {
            _webPageService = webPageService;
            _newsService = newsService;
        }

        public IndexViewModel Execute()
        {
            var model = new IndexViewModel();
            _newsService.IncludeRelatedFiles = true;
            _newsService.IncludeTags = true;

            var tempPost = _newsService.GetPublished(KnownIds.RemoteControl.MGR);


            var tempList = new List<IndexViewModel.Post>();

            foreach (var post in tempPost)
            {
                var t = new IndexViewModel.Post();

                t.Title = post.Title;
                t.RewriteUrl = post.RewriteUrl;
                t.DirectLink = post.DirectLink;

                var tagList = new List<IndexViewModel.Post.Tag>();

                if (post.Tags != null)
                {
                    foreach (var tag in post.Tags)
                    {
                        var p = new IndexViewModel.Post.Tag();

                        p.Label = tag.Label;

                        tagList.Add(p);
                    }
                }

                t.Tags = tagList;

                if (post.Tags != null)
                    if (post.Tags.FirstOrDefault().Label == "Brafton-Feed")
                        continue;

                tempList.Add(t);
            }

            model.Posts = tempList;

            var tempPage = _webPageService.GetByRewriteUrl(KnownIds.RemoteControl.MGR, "/press-room");

            model.PageBody = tempPage.Body;
            model.PageTitle = tempPage.Title;

            var meta = new MetaData()
            {
                MetaDescription = tempPage.MetaDescription,
                MetaKeywords = tempPage.MetaKeywords,
                Title = tempPage.MetaTitle
            };

            model.Meta = meta;


            return model;
        }
    }
}
