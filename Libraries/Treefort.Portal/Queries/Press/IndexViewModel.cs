﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Own;

namespace Treefort.Portal.Queries.Press
{
    public class IndexViewModel
    {
        public MetaData Meta { get; set; }
        public string PageBody { get; set; }
        public string PageTitle { get; set; }
        public IList<Post> Posts { get; set; }

        public class Post
        {
            public string DirectLink { get; set; }
            public string Title { get; set; }
            public string RewriteUrl { get; set; }
            public IList<Tag> Tags { get; set; }


            public class Tag
            {
                public string Label { get; set; }
            }


        }
    }
}
