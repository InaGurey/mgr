﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Locations
{
    public interface IGeoListViewModelQuery
    {
        StoresNearYouViewModel Execute(double? lat, double? lng);
    }


    public class GeoListViewModelQuery : IGeoListViewModelQuery
    {
        private readonly ILocationService _locationService;


        public GeoListViewModelQuery(ILocationService locationService)
        {
            _locationService = locationService;
        }

        public StoresNearYouViewModel Execute(double? lat, double? lng)
        {
            var results = new List<GenericLocation>();

            var mod = new StoresNearYouViewModel();

            if (lat.HasValue && lng.HasValue)
            {
                var locations = _locationService.GetLocationsByCoordinates(lat.Value, lng.Value, 5, 250);

                results = (from location in locations
                           select new GenericLocation
                           {
                               Name = location.Name,
                               Address1 = location.AddressLine1,
                               Address2 = location.AddressLine2,
                               City = location.City,
                               Distance = location.Distance,
                               Region = location.Region,
                               PostalCode = location.PostalCode,
                               StoreNumber = location.StoreNumber,
                               Directions = location.Directions,
                               Email = location.EmailAddress,
                               Lat = location.Latitude,
                               Lon = location.Longitude,
                               Phone = location.PhoneNumber,
                               Website = location.WebsiteUrl,
                               ConstactContactUrl = location.ConstantContactUrl,
                               HasGiftCard = location.HasGiftCards,
                               ImageFileName = location.ImageFileName,
                               Id = location.Id,
                               IsGoldStandard = location.IsGold

                           }).ToList();
                mod.CityNear = "";
            }
            else
            {

                var ip = HttpContext.Current.Request.UserHostAddress;
                if (HttpContext.Current.Request.IsLocal || ip.StartsWith("192.168.1."))
                {
                    ip = "173.11.59.141";
                }
                var l = _locationService.GetLocationByIpAddress(ip);


                if (!(l.Latitude == 0 && l.Longitude == 0))
                {
                    var locations = _locationService.GetLocationsByCoordinates(l.Latitude, l.Longitude, 5, 250);
                    mod.CityNear = l.City;

                    results = (from location in locations
                               select new GenericLocation
                               {
                                   Name = location.Name,
                                   Address1 = location.AddressLine1,
                                   Address2 = location.AddressLine2,
                                   Distance = location.Distance,
                                   City = location.City,
                                   Region = location.Region,
                                   PostalCode = location.PostalCode,
                                   StoreNumber = location.StoreNumber,
                                   Directions = location.Directions,
                                   Email = location.EmailAddress,
                                   Lat = location.Latitude,
                                   Lon = location.Longitude,
                                   Phone = location.PhoneNumber,
                                   Website = location.WebsiteUrl,
                                   ConstactContactUrl = location.ConstantContactUrl,
                                   HasGiftCard = location.HasGiftCards,
                                   ImageFileName = location.ImageFileName,
                                   Id = location.Id,
                                   IsGoldStandard = location.IsGold

                               }).ToList();
                }
            }


            mod.Stores = results;

            return mod;
        }

    }
}