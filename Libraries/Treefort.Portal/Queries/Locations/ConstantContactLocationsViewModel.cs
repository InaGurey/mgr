﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treefort.Portal.Queries.Locations
{
    public class ConstantContactLocationsViewModel
    {
        public ConstantContactLocationsViewModel()
        {
            Locations = new List<ConstantContactLocation>();
            Regions = new List<ConstantContactRegion>();
        }
        public IList<ConstantContactLocation> Locations { get; set; }
        public IList<ConstantContactRegion> Regions { get; set; }
    }
    public class ConstantContactLocation
    {
        public string Name { get; set; }
        public string Region { get; set; }
        public string ConstantContactUrl { get; set; }
    }

    public class ConstantContactRegion
    {
        public string FullName { get; set; }
        public string Abbr { get; set; }
        public string Country { get; set; }
    }
}
