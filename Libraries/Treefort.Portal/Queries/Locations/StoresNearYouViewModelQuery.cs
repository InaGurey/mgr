﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Locations
{

    public interface IStoresNearYouViewModelQuery
    {
        StoresNearYouViewModel Execute(string code);
    }

    public class StoresNearYouViewModelQuery : IStoresNearYouViewModelQuery
    {
        private readonly ILocationService _locationService;


        public StoresNearYouViewModelQuery(ILocationService locationService)
        {
            _locationService = locationService;
        }

        public StoresNearYouViewModel Execute(string code)
        {
            var mod = new StoresNearYouViewModel();

            var results = new List<GenericLocation>();
            //For Initial Load -- just grab the nearest to them
            if(string.IsNullOrEmpty(code))
            {
                var ip = HttpContext.Current.Request.UserHostAddress;
                if (HttpContext.Current.Request.IsLocal || ip.StartsWith("192.168.1."))
                {
                    ip = "173.11.59.141";
                }

                var location = _locationService.GetLocationByIpAddress(ip);

                IList<Geo.LocationModel> locations = new List<Geo.LocationModel>();

                mod.CityNear = location.City;

                if (!(location.Latitude == 0 && location.Longitude == 0))
                {
                    locations = _locationService.GetLocationsByCoordinates(location.Latitude,
                                                                      location.Longitude, 5, 250);

                    results = (from x in locations
                               select new GenericLocation
                                          {
                                              Id = x.Id,
                                              Name = x.Name,
                                              Distance = x.Distance,
                                              Address1 = x.AddressLine1,
                                              Address2 = x.AddressLine2,
                                              City = x.City,
                                              Region = x.Region,
                                              PostalCode = x.PostalCode,
                                              StoreNumber = x.StoreNumber,
                                              Directions = x.Directions,
                                              Email = x.EmailAddress,
                                              Lat = x.Latitude,
                                              Lon = x.Longitude,
                                              Phone = x.PhoneNumber,
                                              Website = x.WebsiteUrl,
                                              ImageFileName = x.ImageFileName,
                                              IsGoldStandard = x.IsGold,
                                              HasGiftCard = x.HasGiftCards,
                                              ConstactContactUrl = x.ConstantContactUrl
                                          }).ToList();
                }

                mod.Stores = results;
                return mod;
            }

            mod.CityNear = code;

            var temp = _locationService.GetLocationsByPostalCode(code, 5, 100);

            if(!temp.Any())
            {
                temp = _locationService.GetLocationsByRegion(code);
            }

            results = (from x in temp
                        select new GenericLocation
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Address1 = x.AddressLine1,
                            Address2 = x.AddressLine2,
                            City = x.City,
                            Region = x.Region,
                            PostalCode = x.PostalCode,
                            StoreNumber = x.StoreNumber,
                            Directions = x.Directions,
                            Email = x.EmailAddress,
                            Lat = x.Latitude,
                            Lon = x.Longitude,
                            Phone = x.PhoneNumber,
                            Website = x.WebsiteUrl,
                            ImageFileName = x.ImageFileName,
                            IsGoldStandard = x.IsGold,
                            HasGiftCard = x.HasGiftCards
                        }).ToList();

                
            mod.Stores = results;
                
            return mod;
            
        }
    }
}