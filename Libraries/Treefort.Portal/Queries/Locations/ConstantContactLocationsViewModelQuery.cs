﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Locations
{
    public interface IConstantContactLocationsViewModelQuery
    {
        ConstantContactLocationsViewModel Execute();
    }

    public class ConstantContactLocationsViewModelQuery : IConstantContactLocationsViewModelQuery
    {
        private readonly ILocationService _locationService;

        public ConstantContactLocationsViewModelQuery(ILocationService locationService)
        {
            _locationService = locationService;
        }

        public ConstantContactLocationsViewModel Execute()
        {
            var locations = _locationService.GetLocationsWithConstantContact();

            var sandp = new List<ConstantContactRegion>()
            {
                new ConstantContactRegion() {Abbr = "AL", Country = "US", FullName = "Alabama"},
                new ConstantContactRegion() {Abbr = "AK", Country = "US", FullName = "Alaska"},
                new ConstantContactRegion() {Abbr = "AZ", Country = "US", FullName = "Arizona"},
                new ConstantContactRegion() {Abbr = "AR", Country = "US", FullName = "Arkansas"},
                new ConstantContactRegion() {Abbr = "CA", Country = "US", FullName = "California"},
                new ConstantContactRegion() {Abbr = "CO", Country = "US", FullName = "Colorado"},
                new ConstantContactRegion() {Abbr = "CT", Country = "US", FullName = "Connecticut"},
                new ConstantContactRegion() {Abbr = "DC", Country = "US", FullName = "D.C."},
                new ConstantContactRegion() {Abbr = "DE", Country = "US", FullName = "Delaware"},
                new ConstantContactRegion() {Abbr = "FL", Country = "US", FullName = "Florida"},
                new ConstantContactRegion() {Abbr = "GA", Country = "US", FullName = "Georgia"},
                new ConstantContactRegion() {Abbr = "HI", Country = "US", FullName = "Hawaii"},
                new ConstantContactRegion() {Abbr = "ID", Country = "US", FullName = "Idaho"},
                new ConstantContactRegion() {Abbr = "IL", Country = "US", FullName = "Illinois"},
                new ConstantContactRegion() {Abbr = "IN", Country = "US", FullName = "Indiana"},
                new ConstantContactRegion() {Abbr = "IA", Country = "US", FullName = "Iowa"},
                new ConstantContactRegion() {Abbr = "KS", Country = "US", FullName = "Kansas"},
                new ConstantContactRegion() {Abbr = "KY", Country = "US", FullName = "Kentucky"},
                new ConstantContactRegion() {Abbr = "LA", Country = "US", FullName = "Louisiana"},
                new ConstantContactRegion() {Abbr = "ME", Country = "US", FullName = "Maine"},
                new ConstantContactRegion() {Abbr = "MD", Country = "US", FullName = "Maryland"},
                new ConstantContactRegion() {Abbr = "MA", Country = "US", FullName = "Massachusetts"},
                new ConstantContactRegion() {Abbr = "MI", Country = "US", FullName = "Michigan"},
                new ConstantContactRegion() {Abbr = "MN", Country = "US", FullName = "Minnesota"},
                new ConstantContactRegion() {Abbr = "MS", Country = "US", FullName = "Mississippi"},
                new ConstantContactRegion() {Abbr = "MO", Country = "US", FullName = "Missouri"},
                new ConstantContactRegion() {Abbr = "MT", Country = "US", FullName = "Montana"},
                new ConstantContactRegion() {Abbr = "NE", Country = "US", FullName = "Nebraska"},
                new ConstantContactRegion() {Abbr = "NV", Country = "US", FullName = "Nevada"},
                new ConstantContactRegion() {Abbr = "NH", Country = "US", FullName = "New Hampshire"},
                new ConstantContactRegion() {Abbr = "NJ", Country = "US", FullName = "New Jersey"},
                new ConstantContactRegion() {Abbr = "NM", Country = "US", FullName = "New Mexico"},
                new ConstantContactRegion() {Abbr = "NY", Country = "US", FullName = "New York"},
                new ConstantContactRegion() {Abbr = "NC", Country = "US", FullName = "North Carolina"},
                new ConstantContactRegion() {Abbr = "ND", Country = "US", FullName = "North Dakota"},
                new ConstantContactRegion() {Abbr = "OH", Country = "US", FullName = "Ohio"},
                new ConstantContactRegion() {Abbr = "OK", Country = "US", FullName = "Oklahoma"},
                new ConstantContactRegion() {Abbr = "OR", Country = "US", FullName = "Oregon"},
                new ConstantContactRegion() {Abbr = "PA", Country = "US", FullName = "Pennsylvania"},
                new ConstantContactRegion() {Abbr = "RI", Country = "US", FullName = "Rhode Island"},
                new ConstantContactRegion() {Abbr = "SC", Country = "US", FullName = "South Carolina"},
                new ConstantContactRegion() {Abbr = "SD", Country = "US", FullName = "South Dakota"},
                new ConstantContactRegion() {Abbr = "TN", Country = "US", FullName = "Tennessee"},
                new ConstantContactRegion() {Abbr = "TX", Country = "US", FullName = "Texas"},
                new ConstantContactRegion() {Abbr = "UT", Country = "US", FullName = "Utah"},
                new ConstantContactRegion() {Abbr = "VT", Country = "US", FullName = "Vermont"},
                new ConstantContactRegion() {Abbr = "VA", Country = "US", FullName = "Virginia"},
                new ConstantContactRegion() {Abbr = "WA", Country = "US", FullName = "Washington"},
                new ConstantContactRegion() {Abbr = "WV", Country = "US", FullName = "West Virginia"},
                new ConstantContactRegion() {Abbr = "WI", Country = "US", FullName = "Wisconsin"},
                new ConstantContactRegion() {Abbr = "WY", Country = "US", FullName = "Wyoming"},
                new ConstantContactRegion() {Abbr = "AB", Country = "CA", FullName = "Alberta"},
                new ConstantContactRegion() {Abbr = "BC", Country = "CA", FullName = "British Columbia"},
                new ConstantContactRegion() {Abbr = "MB", Country = "CA", FullName = "Manitoba"},
                new ConstantContactRegion() {Abbr = "NB", Country = "CA", FullName = "New Brunswick"},
                new ConstantContactRegion() {Abbr = "NL", Country = "CA", FullName = "Newfoundland and Labrador"},
                new ConstantContactRegion() {Abbr = "NT", Country = "CA", FullName = "Northwest Territories"},
                new ConstantContactRegion() {Abbr = "NS", Country = "CA", FullName = "Nova Scotia"},
                new ConstantContactRegion() {Abbr = "NU", Country = "CA", FullName = "Nunavut"},
                new ConstantContactRegion() {Abbr = "ON", Country = "CA", FullName = "Ontario"},
                new ConstantContactRegion() {Abbr = "PE", Country = "CA", FullName = "Prince Edward Island"},
                new ConstantContactRegion() {Abbr = "QC", Country = "CA", FullName = "Quebec"},
                new ConstantContactRegion() {Abbr = "SK", Country = "CA", FullName = "Saskatchewan"},
                new ConstantContactRegion() {Abbr = "YT", Country = "CA", FullName = "Yukon"}
            };

            var uniqueRegions = locations.Select(x => x.Region).Distinct().ToList();
            var regions = sandp.Where(x => uniqueRegions.Contains(x.Abbr)).ToList();

            return new ConstantContactLocationsViewModel()
            {
                Regions = regions,
                Locations = locations
            };
        }
    }
}
