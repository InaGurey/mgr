﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mindscape.LightSpeed;
using Treefort.Portal;

namespace Treefort.Portal.Queries.Locations
{

     public interface IHoursViewModelQuery
    {
         HoursViewModel Execute(int locationId, string timeFormat = "h:mm tt");
         string GetTodayHours(int locationId, string timeFormat = "htt");
    }

    public class HoursViewModelQuery : IHoursViewModelQuery
    {
		private static LightSpeedContext<PortalUnitOfWork> _portal;
		
		public HoursViewModelQuery(LightSpeedContext<PortalUnitOfWork> portal) {
			_portal = portal;
		}

        public HoursViewModel Execute(int locationId, string timeFormat = "h:mm tt")
        {
            using (var uow = _portal.CreateUnitOfWork())
            {

                    var lhours = uow.LocationHours.FirstOrDefault(x => x.LocationId == locationId);

                    if (lhours != null)
                    {
                        var mod = new HoursViewModel()
                                      {
                                          monopen = lhours.MondayOpenTime.HasValue ? lhours.MondayOpenTime.Value.ToString(timeFormat) : "",
                                          monclose = lhours.MondayCloseTime.HasValue ? lhours.MondayCloseTime.Value.ToString(timeFormat) : "",
                                          tueopen = lhours.TuesdayOpenTime.HasValue ? lhours.TuesdayOpenTime.Value.ToString(timeFormat) : "",
                                          tueclose = lhours.TuesdayCloseTime.HasValue ? lhours.TuesdayCloseTime.Value.ToString(timeFormat) : "",
                                          wedopen = lhours.WednesdayOpenTime.HasValue ? lhours.WednesdayOpenTime.Value.ToString(timeFormat) : "",
                                          wedclose = lhours.WednesdayCloseTime.HasValue ? lhours.WednesdayCloseTime.Value.ToString(timeFormat) : "",
                                          thuopen = lhours.ThursdayOpenTime.HasValue ? lhours.ThursdayOpenTime.Value.ToString(timeFormat) : "",
                                          thuclose = lhours.ThursdayCloseTime.HasValue ? lhours.ThursdayCloseTime.Value.ToString(timeFormat) : "",
                                          friopen = lhours.FridayOpenTime.HasValue ? lhours.FridayOpenTime.Value.ToString(timeFormat) : "",
                                          friclose = lhours.FridayCloseTime.HasValue ? lhours.FridayCloseTime.Value.ToString(timeFormat) : "",
                                          satopen = lhours.SaturdayOpenTime.HasValue ? lhours.SaturdayOpenTime.Value.ToString(timeFormat) : "",
                                          satclose = lhours.SaturdayCloseTime.HasValue ? lhours.SaturdayCloseTime.Value.ToString(timeFormat) : "",
                                          sunopen = lhours.SundayOpenTime.HasValue ? lhours.SundayOpenTime.Value.ToString(timeFormat) : "",
                                          sunclose = lhours.SundayCloseTime.HasValue ? lhours.SundayCloseTime.Value.ToString(timeFormat) : "",
                                          locationID = lhours.LocationId
                                      };
                        return mod;
                    }

            }

            return null;
        }

        public string GetTodayHours(int locationId, string timeFormat = "htt")
        {
            string hoursToday = null;
            var hvmq = this.Execute(locationId, timeFormat);
            if (hvmq == null) return hoursToday;
            var tdow = DateTime.Today.DayOfWeek;
            var topen = "";
            var tclose = "";
            switch (tdow)
            {
                case (DayOfWeek.Sunday):
                {
                    topen = hvmq.sunopen;
                    tclose = hvmq.sunclose;
                    break;
                }
                case (DayOfWeek.Monday):
                {
                    topen = hvmq.monopen;
                    tclose = hvmq.monclose;
                    break;
                }
                case (DayOfWeek.Tuesday):
                {
                    topen = hvmq.tueopen;
                    tclose = hvmq.tueclose;
                    break;
                }
                case (DayOfWeek.Wednesday):
                {
                    topen = hvmq.wedopen;
                    tclose = hvmq.wedclose;
                    break;
                }
                case (DayOfWeek.Thursday):
                {
                    topen = hvmq.thuopen;
                    tclose = hvmq.thuclose;
                    break;
                }
                case (DayOfWeek.Friday):
                {
                    topen = hvmq.friopen;
                    tclose = hvmq.friclose;
                    break;
                }
                case (DayOfWeek.Saturday):
                {
                    topen = hvmq.satopen;
                    tclose = hvmq.satclose;
                    break;
                }
            }
            hoursToday = (string.IsNullOrEmpty(topen) || string.IsNullOrEmpty(tclose))
                ? "Closed Today"
                : string.Format("Open Today: {0}-{1}", topen, tclose);
            return hoursToday;
        }
    }
}