﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Geo;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Locations
{
    public interface ISearchViewModelQuery
    {
        SearchViewModel Execute(SearchViewModel info);
        SearchViewModel GetyByRegion(string region);
    }

    public class SearchViewModelQuery : ISearchViewModelQuery
    {
        private readonly ILocationService _locationService;

        public SearchViewModelQuery(ILocationService locationService)
        {
            _locationService = locationService;
        }

        public SearchViewModel Execute(SearchViewModel info)
        {
            if (string.IsNullOrEmpty(info.SearchedLocation) && info.Lat == 0.0)
                return new SearchViewModel() { Locations = new List<LocationModel>() };

            //administrative_area_level_1 is state
            var locs = info.GeocodeType == "administrative_area_level_1"
                //josh didn't like the space, so we are taking 8
                ? _locationService.GetLocationsByRegion(info.GeocodeShortName).ToList()
                : _locationService.GetLocationsByCoordinates(info.Lat, info.Lon, 15, 50);

            return new SearchViewModel()
            {
                SearchedLocation = info.SearchedLocation,
                Locations = locs.ToList()
            };
        }

        public SearchViewModel GetyByRegion(string region)
        {
            var locs = _locationService.GetLocationsByRegion(region).ToList();
            return new SearchViewModel()
            {
                SearchedLocation = region,
                Locations = locs
            };
        }

      
    }
}
