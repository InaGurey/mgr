﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Locations
{

    public interface IListViewModelQuery
    {
        ListViewModel Execute(string id);
    }

    internal class ListViewModelQuery : IListViewModelQuery
    {
        private readonly ILocationService _locationService;


        public ListViewModelQuery(ILocationService locationService)
        {
            _locationService = locationService;
        }

        public ListViewModel Execute(string id)
        {
            var code = string.Join(string.Empty, id.ToCharArray().Take(2).ToArray());
           
            var model = new ListViewModel();

              //setting the model's name and country code
            //model.StateProvince = tempsp;

            var tempLocations = _locationService.GetLocationsByRegion(code);

          
            var tempList = new List<GenericLocation>();

            foreach (var location in tempLocations)
            {
                var x = new GenericLocation()
                            {
                                Id = location.Id,
                                Name = location.Name,
                                Address1 = location.AddressLine1,
                                Address2 = location.AddressLine2,
                                City = location.City,
                                Region = location.Region,
                                PostalCode = location.PostalCode,
                                StoreNumber = location.StoreNumber,
                                Email = location.EmailAddress,
                                Lat = location.Latitude,
                                Lon = location.Longitude,
                                Phone = location.PhoneNumber,
                                Website = location.WebsiteUrl,
                                ImageFileName = location.ImageFileName,
                                IsGoldStandard = location.IsGold,
                                HasGiftCard = location.HasGiftCards,
                                ConstactContactUrl = location.ConstantContactUrl
                            };

                tempList.Add(x);
            }

            model.Locations = tempList;

            model.LocationCount = model.Locations.Count > 0 ? model.Locations.Count.ToString() : "No";

            return model;
        }
    }
}