﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Treefort.Portal.Queries.Geo;

namespace Treefort.Portal.Queries.Locations
{
    public class SearchViewModel : IpCountryModel
    {
        public string SearchedLocation { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string GeocodeShortName { get; set; }
        public string GeocodeType { get; set; }
        public string Units { get; set; }
        public List<LocationModel> Locations { get; set; } 
    }
}
