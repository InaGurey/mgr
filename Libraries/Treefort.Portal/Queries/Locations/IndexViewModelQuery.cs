﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Treefort.Portal.Queries.Home;
using Treefort.Portal.Services;

namespace Treefort.Portal.Queries.Locations
{

    public interface IIndexViewModelQuery
    {
        IndexViewModel Execute();
    }

    public class IndexViewModelQuery : IIndexViewModelQuery
    {
         private readonly ILocationService _locationService;
        private readonly IRemoteControlViewModelQuery _remoteControlViewModelQuery;


         public IndexViewModelQuery(ILocationService locationService, IRemoteControlViewModelQuery remoteControlViewModelQuery)
         {
             _locationService = locationService;
             _remoteControlViewModelQuery = remoteControlViewModelQuery;
         }


        public IndexViewModel Execute()
        {
            var ip = HttpContext.Current.Request.UserHostAddress;
            if (HttpContext.Current.Request.IsLocal || ip.StartsWith("192.168.1."))
            {
                ip = "173.11.59.141";
               // ip = "24.36.0.211";
            }

            var alllocations = _locationService.GetAllLocations();

            //getting all the distinct states
            var states = alllocations.Select(x => x.Region).Distinct().ToList();

            //getting full name and the abbr
            var stateInfo = _locationService.GetFullNameOfStates(states).OrderBy(x => x.StateFullName).ToList();

            var model = new IndexViewModel()
                            {
                                Locations = alllocations,
                                States = stateInfo,
                                RcPage = _remoteControlViewModelQuery.Execute("/locations")
                            };
            return model;
        }
    }
}