﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treefort.Portal.Queries.Locations
{
    public class ListViewModel
    {
        public string LocationCount { get; set; }
        //public SProvince StateProvince { get; set; }
        public IList<Locations.GenericLocation> Locations { get; set; }
        public string CityNear { get; set; }

        //public class SProvince
        //{
        //    public string Name { get; set; }
        //    public string CountryCode { get; set; }
        //}
    }
}