﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Treefort.Portal.Queries.Geo;
using Treefort.Portal.Queries.Home;

namespace Treefort.Portal.Queries.Locations
{
    public class IndexViewModel
    {
        public IList<LocationModel> Locations { get; set; }
        public IList<StateInfo> States { get; set; }
        public RemoteControlPage RcPage { get; set; }
       
    }

    public class StateInfo
    {
        public string StateFullName { get; set; }
        public string StateAbbr { get; set; }
    }


}