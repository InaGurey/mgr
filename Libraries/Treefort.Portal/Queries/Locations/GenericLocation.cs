﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treefort.Portal.Queries.Locations
{
    public class GenericLocation
    {
        public int Id { get; set; }
        public string StoreNumber { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Directions { get; set; }
        public bool? HasGiftCard { get; set; }
        public string ConstactContactUrl { get; set; }
        public string FacebookUrl { get; set; }
        public bool? IsGoldStandard { get; set; }
        public string ImageFileName { get; set; }
        public double? Distance { get; set; }

        //This is for the details page since they want this on the email subject
        public int ProductId { get; set; }
        public string ProductSKU { get; set; }
        public string ProductDescription { get; set; }
        public string HoursToday { get; set; }
    }
}