﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Treefort.Portal.Queries.Locations
{
    public class StoresNearYouViewModel
    {
        public IList<GenericLocation> Stores { get; set; }
        public string CityNear { get; set; }
    }
}