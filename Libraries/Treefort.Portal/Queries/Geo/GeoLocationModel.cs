﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal.Queries.Geo
{
    public class GeoLocationModel
    {
        public int AreaCode { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public int DmaCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int MetroCode { get; set; }
        public string PostalCode { get; set; }
        public string Region { get; set; }
        public string RegionName { get; set; }

        public bool HasCoordinateData
        {
            get
            {
                return Latitude != 0 && Longitude != 0;
            }
        }
    }

}
