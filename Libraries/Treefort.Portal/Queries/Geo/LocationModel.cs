﻿using System.Collections.Generic;

namespace Treefort.Portal.Queries.Geo
{
    public class LocationModel
    {
        public int Id { get; set; }
        public string StoreNumber { get; set; }
        public int FranchiseId { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string FranchiseeName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string MapUrl { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Directions { get; set; }
        public string EmailAddress { get; set; }
        public string WebsiteUrl { get; set; }
        public string CarbonCopy { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string MyspaceUrl { get; set; }
        public string FourSquareUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public bool IsGold { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool HasGiftCards { get; set; }
        public string ImageFileName { get; set; }
        public double? Distance { get; set; }
        public string ConstantContactUrl { get; set; }
        public bool DisplayWebsiteLink { get; set; }
        public bool IsComingSoon { get; set; }

        public IList<HoursLine> Hours { get; set; }

        public class HoursLine
        {
            public string DayName { get; set; }
            public string Open { get; set; }
            public string Close { get; set; }
        }
    }

    public class Coordinate
    {
        private double? _latitude;
        private double? _longitude;

        public Coordinate(double? latitude, double? longitude)
        {
            _latitude = latitude;
            _longitude = longitude;
        }

        public double? Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                this._latitude = value;
            }
        }

        public double? Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                this._longitude = value;
            }
        }
    }
}
