using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

namespace Treefort.Portal
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("MGR_Inventory", IdColumnName="i_item_id")]
  public partial class MgrInventory : Entity<int>
  {
    #region Fields
  
    [Column("i_item_store_number")]
    private int _iItemStoreNumber;
    [Column("ch_item_sku")]
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _chItemSku;
    [Column("ch_item_category_code")]
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _chItemCategoryCode;
    [Column("mny_item_sale_price")]
    private decimal _mnyItemSalePrice;
    [Column("dt_item_purchase_date")]
    private System.DateTime _dtItemPurchaseDate;
    [Column("vch_item_short_description")]
    [ValidateLength(0, 255)]
    private string _vchItemShortDescription;
    [Column("vch_item_extended_description")]
    [ValidateLength(0, 1745)]
    private string _vchItemExtendedDescription;
    [Column("vch_item_image_url")]
    [ValidateLength(0, 510)]
    private string _vchItemImageUrl;
    [Column("b_item_active_status")]
    private bool _bItemActiveStatus;
    [Column("b_item_feature_status")]
    private bool _bItemFeatureStatus;
    [Column("vch_item_keywords")]
    [ValidateLength(0, 2000)]
    private string _vchItemKeywords;
    [Column("tsp_timestamp")]
    private byte[] _tspTimestamp;
    [Column("i_category_id")]
    private int _iCategoryId;
    [Column("i_subcategory_id")]
    private int _iSubcategoryId;
    [Column("s_store_city")]
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _sStoreCity;
    [Column("s_store_state_name")]
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _sStoreStateName;
    [Column("s_store_postal_code")]
    [ValidatePresence]
    [ValidateLength(0, 20)]
    private string _sStorePostalCode;
    [Column("dt_delay_datetime")]
    private System.Nullable<System.DateTime> _dtDelayDatetime;
    [Column("dt_modified_datetime")]
    private System.DateTime _dtModifiedDatetime;
    [Column("dt_created_datetime")]
    private System.DateTime _dtCreatedDatetime;
    [Column("b_image_imported")]
    private bool _bImageImported;
    [Column("b_imported")]
    private bool _bImported;
    [Column("b_deleted")]
    private bool _bDeleted;
    [Column("b_clearance")]
    private bool _bClearance;
    [Column("mny_item_shipping_price")]
    private System.Nullable<decimal> _mnyItemShippingPrice;
    [Column("vch_paypal_txn_id")]
    [ValidateLength(0, 50)]
    private string _vchPaypalTxnId;
    [Column("mny_paypal_total")]
    private System.Nullable<decimal> _mnyPaypalTotal;
    [Column("dt_paypal_txn_date")]
    private System.Nullable<System.DateTime> _dtPaypalTxnDate;
    [Column("b_for_sale_online")]
    private System.Nullable<bool> _bForSaleOnline;
    [Column("vch_mpn")]
    [ValidateLength(0, 50)]
    private string _vchMpn;
    [Column("vch_upc")]
    [ValidateLength(0, 50)]
    private string _vchUpc;
    [Column("vch_item_textonly_description")]
    [ValidateLength(0, 1745)]
    private string _vchItemTextonlyDescription;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IItemStoreNumber entity attribute.</summary>
    public const string IItemStoreNumberField = "IItemStoreNumber";
    /// <summary>Identifies the ChItemSku entity attribute.</summary>
    public const string ChItemSkuField = "ChItemSku";
    /// <summary>Identifies the ChItemCategoryCode entity attribute.</summary>
    public const string ChItemCategoryCodeField = "ChItemCategoryCode";
    /// <summary>Identifies the MnyItemSalePrice entity attribute.</summary>
    public const string MnyItemSalePriceField = "MnyItemSalePrice";
    /// <summary>Identifies the DtItemPurchaseDate entity attribute.</summary>
    public const string DtItemPurchaseDateField = "DtItemPurchaseDate";
    /// <summary>Identifies the VchItemShortDescription entity attribute.</summary>
    public const string VchItemShortDescriptionField = "VchItemShortDescription";
    /// <summary>Identifies the VchItemExtendedDescription entity attribute.</summary>
    public const string VchItemExtendedDescriptionField = "VchItemExtendedDescription";
    /// <summary>Identifies the VchItemImageUrl entity attribute.</summary>
    public const string VchItemImageUrlField = "VchItemImageUrl";
    /// <summary>Identifies the BItemActiveStatus entity attribute.</summary>
    public const string BItemActiveStatusField = "BItemActiveStatus";
    /// <summary>Identifies the BItemFeatureStatus entity attribute.</summary>
    public const string BItemFeatureStatusField = "BItemFeatureStatus";
    /// <summary>Identifies the VchItemKeywords entity attribute.</summary>
    public const string VchItemKeywordsField = "VchItemKeywords";
    /// <summary>Identifies the TspTimestamp entity attribute.</summary>
    public const string TspTimestampField = "TspTimestamp";
    /// <summary>Identifies the ICategoryId entity attribute.</summary>
    public const string ICategoryIdField = "ICategoryId";
    /// <summary>Identifies the ISubcategoryId entity attribute.</summary>
    public const string ISubcategoryIdField = "ISubcategoryId";
    /// <summary>Identifies the SStoreCity entity attribute.</summary>
    public const string SStoreCityField = "SStoreCity";
    /// <summary>Identifies the SStoreStateName entity attribute.</summary>
    public const string SStoreStateNameField = "SStoreStateName";
    /// <summary>Identifies the SStorePostalCode entity attribute.</summary>
    public const string SStorePostalCodeField = "SStorePostalCode";
    /// <summary>Identifies the DtDelayDatetime entity attribute.</summary>
    public const string DtDelayDatetimeField = "DtDelayDatetime";
    /// <summary>Identifies the DtModifiedDatetime entity attribute.</summary>
    public const string DtModifiedDatetimeField = "DtModifiedDatetime";
    /// <summary>Identifies the DtCreatedDatetime entity attribute.</summary>
    public const string DtCreatedDatetimeField = "DtCreatedDatetime";
    /// <summary>Identifies the BImageImported entity attribute.</summary>
    public const string BImageImportedField = "BImageImported";
    /// <summary>Identifies the BImported entity attribute.</summary>
    public const string BImportedField = "BImported";
    /// <summary>Identifies the BDeleted entity attribute.</summary>
    public const string BDeletedField = "BDeleted";
    /// <summary>Identifies the BClearance entity attribute.</summary>
    public const string BClearanceField = "BClearance";
    /// <summary>Identifies the MnyItemShippingPrice entity attribute.</summary>
    public const string MnyItemShippingPriceField = "MnyItemShippingPrice";
    /// <summary>Identifies the VchPaypalTxnId entity attribute.</summary>
    public const string VchPaypalTxnIdField = "VchPaypalTxnId";
    /// <summary>Identifies the MnyPaypalTotal entity attribute.</summary>
    public const string MnyPaypalTotalField = "MnyPaypalTotal";
    /// <summary>Identifies the DtPaypalTxnDate entity attribute.</summary>
    public const string DtPaypalTxnDateField = "DtPaypalTxnDate";
    /// <summary>Identifies the BForSaleOnline entity attribute.</summary>
    public const string BForSaleOnlineField = "BForSaleOnline";
    /// <summary>Identifies the VchMpn entity attribute.</summary>
    public const string VchMpnField = "VchMpn";
    /// <summary>Identifies the VchUpc entity attribute.</summary>
    public const string VchUpcField = "VchUpc";
    /// <summary>Identifies the VchItemTextonlyDescription entity attribute.</summary>
    public const string VchItemTextonlyDescriptionField = "VchItemTextonlyDescription";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public int IItemStoreNumber
    {
      get { return Get(ref _iItemStoreNumber, "IItemStoreNumber"); }
      set { Set(ref _iItemStoreNumber, value, "IItemStoreNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ChItemSku
    {
      get { return Get(ref _chItemSku, "ChItemSku"); }
      set { Set(ref _chItemSku, value, "ChItemSku"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ChItemCategoryCode
    {
      get { return Get(ref _chItemCategoryCode, "ChItemCategoryCode"); }
      set { Set(ref _chItemCategoryCode, value, "ChItemCategoryCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public decimal MnyItemSalePrice
    {
      get { return Get(ref _mnyItemSalePrice, "MnyItemSalePrice"); }
      set { Set(ref _mnyItemSalePrice, value, "MnyItemSalePrice"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime DtItemPurchaseDate
    {
      get { return Get(ref _dtItemPurchaseDate, "DtItemPurchaseDate"); }
      set { Set(ref _dtItemPurchaseDate, value, "DtItemPurchaseDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchItemShortDescription
    {
      get { return Get(ref _vchItemShortDescription, "VchItemShortDescription"); }
      set { Set(ref _vchItemShortDescription, value, "VchItemShortDescription"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchItemExtendedDescription
    {
      get { return Get(ref _vchItemExtendedDescription, "VchItemExtendedDescription"); }
      set { Set(ref _vchItemExtendedDescription, value, "VchItemExtendedDescription"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchItemImageUrl
    {
      get { return Get(ref _vchItemImageUrl, "VchItemImageUrl"); }
      set { Set(ref _vchItemImageUrl, value, "VchItemImageUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BItemActiveStatus
    {
      get { return Get(ref _bItemActiveStatus, "BItemActiveStatus"); }
      set { Set(ref _bItemActiveStatus, value, "BItemActiveStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BItemFeatureStatus
    {
      get { return Get(ref _bItemFeatureStatus, "BItemFeatureStatus"); }
      set { Set(ref _bItemFeatureStatus, value, "BItemFeatureStatus"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchItemKeywords
    {
      get { return Get(ref _vchItemKeywords, "VchItemKeywords"); }
      set { Set(ref _vchItemKeywords, value, "VchItemKeywords"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public byte[] TspTimestamp
    {
      get { return Get(ref _tspTimestamp, "TspTimestamp"); }
      set { Set(ref _tspTimestamp, value, "TspTimestamp"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ICategoryId
    {
      get { return Get(ref _iCategoryId, "ICategoryId"); }
      set { Set(ref _iCategoryId, value, "ICategoryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int ISubcategoryId
    {
      get { return Get(ref _iSubcategoryId, "ISubcategoryId"); }
      set { Set(ref _iSubcategoryId, value, "ISubcategoryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SStoreCity
    {
      get { return Get(ref _sStoreCity, "SStoreCity"); }
      set { Set(ref _sStoreCity, value, "SStoreCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SStoreStateName
    {
      get { return Get(ref _sStoreStateName, "SStoreStateName"); }
      set { Set(ref _sStoreStateName, value, "SStoreStateName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SStorePostalCode
    {
      get { return Get(ref _sStorePostalCode, "SStorePostalCode"); }
      set { Set(ref _sStorePostalCode, value, "SStorePostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DtDelayDatetime
    {
      get { return Get(ref _dtDelayDatetime, "DtDelayDatetime"); }
      set { Set(ref _dtDelayDatetime, value, "DtDelayDatetime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime DtModifiedDatetime
    {
      get { return Get(ref _dtModifiedDatetime, "DtModifiedDatetime"); }
      set { Set(ref _dtModifiedDatetime, value, "DtModifiedDatetime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime DtCreatedDatetime
    {
      get { return Get(ref _dtCreatedDatetime, "DtCreatedDatetime"); }
      set { Set(ref _dtCreatedDatetime, value, "DtCreatedDatetime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BImageImported
    {
      get { return Get(ref _bImageImported, "BImageImported"); }
      set { Set(ref _bImageImported, value, "BImageImported"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BImported
    {
      get { return Get(ref _bImported, "BImported"); }
      set { Set(ref _bImported, value, "BImported"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BDeleted
    {
      get { return Get(ref _bDeleted, "BDeleted"); }
      set { Set(ref _bDeleted, value, "BDeleted"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BClearance
    {
      get { return Get(ref _bClearance, "BClearance"); }
      set { Set(ref _bClearance, value, "BClearance"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MnyItemShippingPrice
    {
      get { return Get(ref _mnyItemShippingPrice, "MnyItemShippingPrice"); }
      set { Set(ref _mnyItemShippingPrice, value, "MnyItemShippingPrice"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchPaypalTxnId
    {
      get { return Get(ref _vchPaypalTxnId, "VchPaypalTxnId"); }
      set { Set(ref _vchPaypalTxnId, value, "VchPaypalTxnId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> MnyPaypalTotal
    {
      get { return Get(ref _mnyPaypalTotal, "MnyPaypalTotal"); }
      set { Set(ref _mnyPaypalTotal, value, "MnyPaypalTotal"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> DtPaypalTxnDate
    {
      get { return Get(ref _dtPaypalTxnDate, "DtPaypalTxnDate"); }
      set { Set(ref _dtPaypalTxnDate, value, "DtPaypalTxnDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<bool> BForSaleOnline
    {
      get { return Get(ref _bForSaleOnline, "BForSaleOnline"); }
      set { Set(ref _bForSaleOnline, value, "BForSaleOnline"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchMpn
    {
      get { return Get(ref _vchMpn, "VchMpn"); }
      set { Set(ref _vchMpn, value, "VchMpn"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchUpc
    {
      get { return Get(ref _vchUpc, "VchUpc"); }
      set { Set(ref _vchUpc, value, "VchUpc"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string VchItemTextonlyDescription
    {
      get { return Get(ref _vchItemTextonlyDescription, "VchItemTextonlyDescription"); }
      set { Set(ref _vchItemTextonlyDescription, value, "VchItemTextonlyDescription"); }
    }

    #endregion
  }




  /// <summary>
  /// Provides a strong-typed unit of work for working with the OldMgrInventory model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class OldMgrInventoryUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<MgrInventory> MgrInventories
    {
      get { return this.Query<MgrInventory>(); }
    }
    
  }

}
