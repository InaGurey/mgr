using System;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Validation;
using Mindscape.LightSpeed.Linq;

namespace Treefort.Portal
{
  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("MgrOptions")]
  public partial class MgrOption : Entity<int>
  {
    #region Fields
  
    [ValidateLength(0, 150)]
    private System.Nullable<int> _homepageRow1CategoryId;
    private bool _homepageRow1Visible;
    private System.Nullable<int> _homepageRow1Number;
    [ValidateLength(0, 50)]
    private string _homepageRow1Selection;
    [ValidateLength(0, 150)]
    private System.Nullable<int> _homepageRow2CategoryId;
    private bool _homepageRow2Visible;
    private System.Nullable<int> _homepageRow2Number;
    [ValidateLength(0, 50)]
    private string _homepageRow2Selection;
    [ValidateLength(0, 150)]
    private System.Nullable<int> _homepageRow3CategoryId;
    private bool _homepageRow3Visible;
    private System.Nullable<int> _homepageRow3Number;
    [ValidateLength(0, 50)]
    private string _homepageRow3Selection;
    private bool _showBuySellStatement;
    private bool _showBuySellSubtext;
    private string _buySellSubtext;
    private string _logoSubtext;
    private bool _homepageContentAboveBuySellStatement;
    private string _aboveMapText1;
    private string _aboveMapText2;
    [ValidateLength(0, 4000)]
    private string _domainVerification;
    private string _sellYourGearRecipient;
    private System.Nullable<int> _inventoryDelayDays;
    [ValidateLength(0, 100)]
    private string _authorizeNetApiLogin;
    [ValidateLength(0, 100)]
    private string _authorizeNetTxKey;
    [ValidateLength(0, 100)]
    private string _payPalEmail;
    private bool _buySellStatementAboveProducts;
    [ValidateLength(0, 200)]
    private string _payPalPdtToken;
    private System.Nullable<decimal> _homepageRow1MinPrice;
    private System.Nullable<decimal> _homepageRow2MinPrice;
    private System.Nullable<decimal> _homepageRow3MinPrice;
    private bool _usePayPalExpress;
    [ValidateLength(0, 512)]
    private string _payPalMerchantId;
    private System.Nullable<int> _websiteId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the HomepageRow1CategoryId entity attribute.</summary>
    public const string HomepageRow1CategoryIdField = "HomepageRow1CategoryId";
    /// <summary>Identifies the HomepageRow1Visible entity attribute.</summary>
    public const string HomepageRow1VisibleField = "HomepageRow1Visible";
    /// <summary>Identifies the HomepageRow1Number entity attribute.</summary>
    public const string HomepageRow1NumberField = "HomepageRow1Number";
    /// <summary>Identifies the HomepageRow1Selection entity attribute.</summary>
    public const string HomepageRow1SelectionField = "HomepageRow1Selection";
    /// <summary>Identifies the HomepageRow2CategoryId entity attribute.</summary>
    public const string HomepageRow2CategoryIdField = "HomepageRow2CategoryId";
    /// <summary>Identifies the HomepageRow2Visible entity attribute.</summary>
    public const string HomepageRow2VisibleField = "HomepageRow2Visible";
    /// <summary>Identifies the HomepageRow2Number entity attribute.</summary>
    public const string HomepageRow2NumberField = "HomepageRow2Number";
    /// <summary>Identifies the HomepageRow2Selection entity attribute.</summary>
    public const string HomepageRow2SelectionField = "HomepageRow2Selection";
    /// <summary>Identifies the HomepageRow3CategoryId entity attribute.</summary>
    public const string HomepageRow3CategoryIdField = "HomepageRow3CategoryId";
    /// <summary>Identifies the HomepageRow3Visible entity attribute.</summary>
    public const string HomepageRow3VisibleField = "HomepageRow3Visible";
    /// <summary>Identifies the HomepageRow3Number entity attribute.</summary>
    public const string HomepageRow3NumberField = "HomepageRow3Number";
    /// <summary>Identifies the HomepageRow3Selection entity attribute.</summary>
    public const string HomepageRow3SelectionField = "HomepageRow3Selection";
    /// <summary>Identifies the ShowBuySellStatement entity attribute.</summary>
    public const string ShowBuySellStatementField = "ShowBuySellStatement";
    /// <summary>Identifies the ShowBuySellSubtext entity attribute.</summary>
    public const string ShowBuySellSubtextField = "ShowBuySellSubtext";
    /// <summary>Identifies the BuySellSubtext entity attribute.</summary>
    public const string BuySellSubtextField = "BuySellSubtext";
    /// <summary>Identifies the LogoSubtext entity attribute.</summary>
    public const string LogoSubtextField = "LogoSubtext";
    /// <summary>Identifies the HomepageContentAboveBuySellStatement entity attribute.</summary>
    public const string HomepageContentAboveBuySellStatementField = "HomepageContentAboveBuySellStatement";
    /// <summary>Identifies the AboveMapText1 entity attribute.</summary>
    public const string AboveMapText1Field = "AboveMapText1";
    /// <summary>Identifies the AboveMapText2 entity attribute.</summary>
    public const string AboveMapText2Field = "AboveMapText2";
    /// <summary>Identifies the DomainVerification entity attribute.</summary>
    public const string DomainVerificationField = "DomainVerification";
    /// <summary>Identifies the SellYourGearRecipient entity attribute.</summary>
    public const string SellYourGearRecipientField = "SellYourGearRecipient";
    /// <summary>Identifies the InventoryDelayDays entity attribute.</summary>
    public const string InventoryDelayDaysField = "InventoryDelayDays";
    /// <summary>Identifies the AuthorizeNetApiLogin entity attribute.</summary>
    public const string AuthorizeNetApiLoginField = "AuthorizeNetApiLogin";
    /// <summary>Identifies the AuthorizeNetTxKey entity attribute.</summary>
    public const string AuthorizeNetTxKeyField = "AuthorizeNetTxKey";
    /// <summary>Identifies the PayPalEmail entity attribute.</summary>
    public const string PayPalEmailField = "PayPalEmail";
    /// <summary>Identifies the BuySellStatementAboveProducts entity attribute.</summary>
    public const string BuySellStatementAboveProductsField = "BuySellStatementAboveProducts";
    /// <summary>Identifies the PayPalPdtToken entity attribute.</summary>
    public const string PayPalPdtTokenField = "PayPalPdtToken";
    /// <summary>Identifies the HomepageRow1MinPrice entity attribute.</summary>
    public const string HomepageRow1MinPriceField = "HomepageRow1MinPrice";
    /// <summary>Identifies the HomepageRow2MinPrice entity attribute.</summary>
    public const string HomepageRow2MinPriceField = "HomepageRow2MinPrice";
    /// <summary>Identifies the HomepageRow3MinPrice entity attribute.</summary>
    public const string HomepageRow3MinPriceField = "HomepageRow3MinPrice";
    /// <summary>Identifies the UsePayPalExpress entity attribute.</summary>
    public const string UsePayPalExpressField = "UsePayPalExpress";
    /// <summary>Identifies the PayPalMerchantId entity attribute.</summary>
    public const string PayPalMerchantIdField = "PayPalMerchantId";
    /// <summary>Identifies the WebsiteId entity attribute.</summary>
    public const string WebsiteIdField = "WebsiteId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("MgrOptions")]
    private readonly EntityHolder<Website> _website = new EntityHolder<Website>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Website Website
    {
      get { return Get(_website); }
      set { Set(_website, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> HomepageRow1CategoryId
    {
      get { return Get(ref _homepageRow1CategoryId, "HomepageRow1CategoryId"); }
      set { Set(ref _homepageRow1CategoryId, value, "HomepageRow1CategoryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HomepageRow1Visible
    {
      get { return Get(ref _homepageRow1Visible, "HomepageRow1Visible"); }
      set { Set(ref _homepageRow1Visible, value, "HomepageRow1Visible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> HomepageRow1Number
    {
      get { return Get(ref _homepageRow1Number, "HomepageRow1Number"); }
      set { Set(ref _homepageRow1Number, value, "HomepageRow1Number"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string HomepageRow1Selection
    {
      get { return Get(ref _homepageRow1Selection, "HomepageRow1Selection"); }
      set { Set(ref _homepageRow1Selection, value, "HomepageRow1Selection"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> HomepageRow2CategoryId
    {
      get { return Get(ref _homepageRow2CategoryId, "HomepageRow2CategoryId"); }
      set { Set(ref _homepageRow2CategoryId, value, "HomepageRow2CategoryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HomepageRow2Visible
    {
      get { return Get(ref _homepageRow2Visible, "HomepageRow2Visible"); }
      set { Set(ref _homepageRow2Visible, value, "HomepageRow2Visible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> HomepageRow2Number
    {
      get { return Get(ref _homepageRow2Number, "HomepageRow2Number"); }
      set { Set(ref _homepageRow2Number, value, "HomepageRow2Number"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string HomepageRow2Selection
    {
      get { return Get(ref _homepageRow2Selection, "HomepageRow2Selection"); }
      set { Set(ref _homepageRow2Selection, value, "HomepageRow2Selection"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> HomepageRow3CategoryId
    {
      get { return Get(ref _homepageRow3CategoryId, "HomepageRow3CategoryId"); }
      set { Set(ref _homepageRow3CategoryId, value, "HomepageRow3CategoryId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HomepageRow3Visible
    {
      get { return Get(ref _homepageRow3Visible, "HomepageRow3Visible"); }
      set { Set(ref _homepageRow3Visible, value, "HomepageRow3Visible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> HomepageRow3Number
    {
      get { return Get(ref _homepageRow3Number, "HomepageRow3Number"); }
      set { Set(ref _homepageRow3Number, value, "HomepageRow3Number"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string HomepageRow3Selection
    {
      get { return Get(ref _homepageRow3Selection, "HomepageRow3Selection"); }
      set { Set(ref _homepageRow3Selection, value, "HomepageRow3Selection"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool ShowBuySellStatement
    {
      get { return Get(ref _showBuySellStatement, "ShowBuySellStatement"); }
      set { Set(ref _showBuySellStatement, value, "ShowBuySellStatement"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool ShowBuySellSubtext
    {
      get { return Get(ref _showBuySellSubtext, "ShowBuySellSubtext"); }
      set { Set(ref _showBuySellSubtext, value, "ShowBuySellSubtext"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string BuySellSubtext
    {
      get { return Get(ref _buySellSubtext, "BuySellSubtext"); }
      set { Set(ref _buySellSubtext, value, "BuySellSubtext"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LogoSubtext
    {
      get { return Get(ref _logoSubtext, "LogoSubtext"); }
      set { Set(ref _logoSubtext, value, "LogoSubtext"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HomepageContentAboveBuySellStatement
    {
      get { return Get(ref _homepageContentAboveBuySellStatement, "HomepageContentAboveBuySellStatement"); }
      set { Set(ref _homepageContentAboveBuySellStatement, value, "HomepageContentAboveBuySellStatement"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AboveMapText1
    {
      get { return Get(ref _aboveMapText1, "AboveMapText1"); }
      set { Set(ref _aboveMapText1, value, "AboveMapText1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AboveMapText2
    {
      get { return Get(ref _aboveMapText2, "AboveMapText2"); }
      set { Set(ref _aboveMapText2, value, "AboveMapText2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string DomainVerification
    {
      get { return Get(ref _domainVerification, "DomainVerification"); }
      set { Set(ref _domainVerification, value, "DomainVerification"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SellYourGearRecipient
    {
      get { return Get(ref _sellYourGearRecipient, "SellYourGearRecipient"); }
      set { Set(ref _sellYourGearRecipient, value, "SellYourGearRecipient"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> InventoryDelayDays
    {
      get { return Get(ref _inventoryDelayDays, "InventoryDelayDays"); }
      set { Set(ref _inventoryDelayDays, value, "InventoryDelayDays"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AuthorizeNetApiLogin
    {
      get { return Get(ref _authorizeNetApiLogin, "AuthorizeNetApiLogin"); }
      set { Set(ref _authorizeNetApiLogin, value, "AuthorizeNetApiLogin"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AuthorizeNetTxKey
    {
      get { return Get(ref _authorizeNetTxKey, "AuthorizeNetTxKey"); }
      set { Set(ref _authorizeNetTxKey, value, "AuthorizeNetTxKey"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PayPalEmail
    {
      get { return Get(ref _payPalEmail, "PayPalEmail"); }
      set { Set(ref _payPalEmail, value, "PayPalEmail"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool BuySellStatementAboveProducts
    {
      get { return Get(ref _buySellStatementAboveProducts, "BuySellStatementAboveProducts"); }
      set { Set(ref _buySellStatementAboveProducts, value, "BuySellStatementAboveProducts"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PayPalPdtToken
    {
      get { return Get(ref _payPalPdtToken, "PayPalPdtToken"); }
      set { Set(ref _payPalPdtToken, value, "PayPalPdtToken"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> HomepageRow1MinPrice
    {
      get { return Get(ref _homepageRow1MinPrice, "HomepageRow1MinPrice"); }
      set { Set(ref _homepageRow1MinPrice, value, "HomepageRow1MinPrice"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> HomepageRow2MinPrice
    {
      get { return Get(ref _homepageRow2MinPrice, "HomepageRow2MinPrice"); }
      set { Set(ref _homepageRow2MinPrice, value, "HomepageRow2MinPrice"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<decimal> HomepageRow3MinPrice
    {
      get { return Get(ref _homepageRow3MinPrice, "HomepageRow3MinPrice"); }
      set { Set(ref _homepageRow3MinPrice, value, "HomepageRow3MinPrice"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool UsePayPalExpress
    {
      get { return Get(ref _usePayPalExpress, "UsePayPalExpress"); }
      set { Set(ref _usePayPalExpress, value, "UsePayPalExpress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PayPalMerchantId
    {
      get { return Get(ref _payPalMerchantId, "PayPalMerchantId"); }
      set { Set(ref _payPalMerchantId, value, "PayPalMerchantId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Website" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> WebsiteId
    {
      get { return Get(ref _websiteId, "WebsiteId"); }
      set { Set(ref _websiteId, value, "WebsiteId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Locations")]
  public partial class Location : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _storeNumber;
    private bool _isActive;
    [ValidateLength(0, 255)]
    private string _name;
    [ValidateLength(0, 255)]
    private string _franchiseeName;
    [ValidateLength(0, 255)]
    private string _addressLine1;
    [ValidateLength(0, 255)]
    private string _addressLine2;
    [ValidateLength(0, 255)]
    private string _city;
    [ValidateLength(0, 50)]
    private string _region;
    [ValidateLength(0, 50)]
    private string _postalCode;
    [ValidateLength(0, 50)]
    private string _country;
    [ValidateLength(0, 50)]
    private string _phoneNumber;
    [ValidateLength(0, 50)]
    private string _faxNumber;
    private string _directions;
    [ValidateEmailAddress]
    [ValidateLength(0, 255)]
    private string _emailAddress;
    [ValidateLength(0, 255)]
    private string _websiteUrl;
    [ValidateLength(0, 255)]
    private string _facebookUrl;
    [ValidateLength(0, 255)]
    private string _twitterUrl;
    [ValidateLength(0, 255)]
    private string _myspaceUrl;
    [ValidateLength(0, 255)]
    private string _constantContactUrl;
    [ValidateLength(0, 255)]
    private string _foursquareUrl;
    [ValidateLength(0, 255)]
    private string _youTubeUrl;
    [ValidateLength(0, 255)]
    private string _linkedInUrl;
    [ValidateLength(0, 255)]
    private string _pintrestUrl;
    private bool _isGold;
    private bool _hasGiftCards;
    private System.Nullable<double> _latitude;
    private System.Nullable<double> _longitude;
    [ValidateLength(0, 255)]
    private string _imageReference;
    private bool _isComingSoon;
    [ValidateLength(0, 255)]
    private string _instagramUrl;
    private int _franchiseId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StoreNumber entity attribute.</summary>
    public const string StoreNumberField = "StoreNumber";
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the FranchiseeName entity attribute.</summary>
    public const string FranchiseeNameField = "FranchiseeName";
    /// <summary>Identifies the AddressLine1 entity attribute.</summary>
    public const string AddressLine1Field = "AddressLine1";
    /// <summary>Identifies the AddressLine2 entity attribute.</summary>
    public const string AddressLine2Field = "AddressLine2";
    /// <summary>Identifies the City entity attribute.</summary>
    public const string CityField = "City";
    /// <summary>Identifies the Region entity attribute.</summary>
    public const string RegionField = "Region";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the Country entity attribute.</summary>
    public const string CountryField = "Country";
    /// <summary>Identifies the PhoneNumber entity attribute.</summary>
    public const string PhoneNumberField = "PhoneNumber";
    /// <summary>Identifies the FaxNumber entity attribute.</summary>
    public const string FaxNumberField = "FaxNumber";
    /// <summary>Identifies the Directions entity attribute.</summary>
    public const string DirectionsField = "Directions";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the WebsiteUrl entity attribute.</summary>
    public const string WebsiteUrlField = "WebsiteUrl";
    /// <summary>Identifies the FacebookUrl entity attribute.</summary>
    public const string FacebookUrlField = "FacebookUrl";
    /// <summary>Identifies the TwitterUrl entity attribute.</summary>
    public const string TwitterUrlField = "TwitterUrl";
    /// <summary>Identifies the MyspaceUrl entity attribute.</summary>
    public const string MyspaceUrlField = "MyspaceUrl";
    /// <summary>Identifies the ConstantContactUrl entity attribute.</summary>
    public const string ConstantContactUrlField = "ConstantContactUrl";
    /// <summary>Identifies the FoursquareUrl entity attribute.</summary>
    public const string FoursquareUrlField = "FoursquareUrl";
    /// <summary>Identifies the YouTubeUrl entity attribute.</summary>
    public const string YouTubeUrlField = "YouTubeUrl";
    /// <summary>Identifies the LinkedInUrl entity attribute.</summary>
    public const string LinkedInUrlField = "LinkedInUrl";
    /// <summary>Identifies the PintrestUrl entity attribute.</summary>
    public const string PintrestUrlField = "PintrestUrl";
    /// <summary>Identifies the IsGold entity attribute.</summary>
    public const string IsGoldField = "IsGold";
    /// <summary>Identifies the HasGiftCards entity attribute.</summary>
    public const string HasGiftCardsField = "HasGiftCards";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";
    /// <summary>Identifies the ImageReference entity attribute.</summary>
    public const string ImageReferenceField = "ImageReference";
    /// <summary>Identifies the IsComingSoon entity attribute.</summary>
    public const string IsComingSoonField = "IsComingSoon";
    /// <summary>Identifies the InstagramUrl entity attribute.</summary>
    public const string InstagramUrlField = "InstagramUrl";
    /// <summary>Identifies the FranchiseId entity attribute.</summary>
    public const string FranchiseIdField = "FranchiseId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Location")]
    private readonly EntityCollection<AccountsLocation> _accountsLocationsByLocation = new EntityCollection<AccountsLocation>();
    [ReverseAssociation("Location")]
    private readonly EntityCollection<LocationHour> _locationHours = new EntityCollection<LocationHour>();
    [ReverseAssociation("PrimaryLocation")]
    private readonly EntityCollection<Account> _ownerAccounts = new EntityCollection<Account>();
    [ReverseAssociation("Locations")]
    private readonly EntityHolder<Franchise> _franchise = new EntityHolder<Franchise>();

    private ThroughAssociation<AccountsLocation, Account> _accounts;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<AccountsLocation> AccountsLocationsByLocation
    {
      get { return Get(_accountsLocationsByLocation); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<LocationHour> LocationHours
    {
      get { return Get(_locationHours); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Account> OwnerAccounts
    {
      get { return Get(_ownerAccounts); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Franchise Franchise
    {
      get { return Get(_franchise); }
      set { Set(_franchise, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<AccountsLocation, Account> Accounts
    {
      get
      {
        if (_accounts == null)
        {
          _accounts = new ThroughAssociation<AccountsLocation, Account>(_accountsLocationsByLocation);
        }
        return Get(_accounts);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public string StoreNumber
    {
      get { return Get(ref _storeNumber, "StoreNumber"); }
      set { Set(ref _storeNumber, value, "StoreNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FranchiseeName
    {
      get { return Get(ref _franchiseeName, "FranchiseeName"); }
      set { Set(ref _franchiseeName, value, "FranchiseeName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AddressLine1
    {
      get { return Get(ref _addressLine1, "AddressLine1"); }
      set { Set(ref _addressLine1, value, "AddressLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AddressLine2
    {
      get { return Get(ref _addressLine2, "AddressLine2"); }
      set { Set(ref _addressLine2, value, "AddressLine2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string City
    {
      get { return Get(ref _city, "City"); }
      set { Set(ref _city, value, "City"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Region
    {
      get { return Get(ref _region, "Region"); }
      set { Set(ref _region, value, "Region"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Country
    {
      get { return Get(ref _country, "Country"); }
      set { Set(ref _country, value, "Country"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PhoneNumber
    {
      get { return Get(ref _phoneNumber, "PhoneNumber"); }
      set { Set(ref _phoneNumber, value, "PhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FaxNumber
    {
      get { return Get(ref _faxNumber, "FaxNumber"); }
      set { Set(ref _faxNumber, value, "FaxNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Directions
    {
      get { return Get(ref _directions, "Directions"); }
      set { Set(ref _directions, value, "Directions"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WebsiteUrl
    {
      get { return Get(ref _websiteUrl, "WebsiteUrl"); }
      set { Set(ref _websiteUrl, value, "WebsiteUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FacebookUrl
    {
      get { return Get(ref _facebookUrl, "FacebookUrl"); }
      set { Set(ref _facebookUrl, value, "FacebookUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string TwitterUrl
    {
      get { return Get(ref _twitterUrl, "TwitterUrl"); }
      set { Set(ref _twitterUrl, value, "TwitterUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string MyspaceUrl
    {
      get { return Get(ref _myspaceUrl, "MyspaceUrl"); }
      set { Set(ref _myspaceUrl, value, "MyspaceUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ConstantContactUrl
    {
      get { return Get(ref _constantContactUrl, "ConstantContactUrl"); }
      set { Set(ref _constantContactUrl, value, "ConstantContactUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FoursquareUrl
    {
      get { return Get(ref _foursquareUrl, "FoursquareUrl"); }
      set { Set(ref _foursquareUrl, value, "FoursquareUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string YouTubeUrl
    {
      get { return Get(ref _youTubeUrl, "YouTubeUrl"); }
      set { Set(ref _youTubeUrl, value, "YouTubeUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LinkedInUrl
    {
      get { return Get(ref _linkedInUrl, "LinkedInUrl"); }
      set { Set(ref _linkedInUrl, value, "LinkedInUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PintrestUrl
    {
      get { return Get(ref _pintrestUrl, "PintrestUrl"); }
      set { Set(ref _pintrestUrl, value, "PintrestUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsGold
    {
      get { return Get(ref _isGold, "IsGold"); }
      set { Set(ref _isGold, value, "IsGold"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HasGiftCards
    {
      get { return Get(ref _hasGiftCards, "HasGiftCards"); }
      set { Set(ref _hasGiftCards, value, "HasGiftCards"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ImageReference
    {
      get { return Get(ref _imageReference, "ImageReference"); }
      set { Set(ref _imageReference, value, "ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsComingSoon
    {
      get { return Get(ref _isComingSoon, "IsComingSoon"); }
      set { Set(ref _isComingSoon, value, "IsComingSoon"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string InstagramUrl
    {
      get { return Get(ref _instagramUrl, "InstagramUrl"); }
      set { Set(ref _instagramUrl, value, "InstagramUrl"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Franchise" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int FranchiseId
    {
      get { return Get(ref _franchiseId, "FranchiseId"); }
      set { Set(ref _franchiseId, value, "FranchiseId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Websites")]
  public partial class Website : Entity<int>
  {
    #region Fields
  
    private int _primaryDomainId;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _userDirectory;
    private bool _isActive;
    private bool _isCustomerApproved;
    private bool _isWinmarkApproved;
    private bool _isTreefortApproved;
    [ValidateLength(0, 50)]
    private string _googleAnalyticsId;
    [ValidateLength(0, 50)]
    private string _googleAnalyticsProfileId;
    private int _accountId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the PrimaryDomainId entity attribute.</summary>
    public const string PrimaryDomainIdField = "PrimaryDomainId";
    /// <summary>Identifies the UserDirectory entity attribute.</summary>
    public const string UserDirectoryField = "UserDirectory";
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the IsCustomerApproved entity attribute.</summary>
    public const string IsCustomerApprovedField = "IsCustomerApproved";
    /// <summary>Identifies the IsWinmarkApproved entity attribute.</summary>
    public const string IsWinmarkApprovedField = "IsWinmarkApproved";
    /// <summary>Identifies the IsTreefortApproved entity attribute.</summary>
    public const string IsTreefortApprovedField = "IsTreefortApproved";
    /// <summary>Identifies the GoogleAnalyticsId entity attribute.</summary>
    public const string GoogleAnalyticsIdField = "GoogleAnalyticsId";
    /// <summary>Identifies the GoogleAnalyticsProfileId entity attribute.</summary>
    public const string GoogleAnalyticsProfileIdField = "GoogleAnalyticsProfileId";
    /// <summary>Identifies the AccountId entity attribute.</summary>
    public const string AccountIdField = "AccountId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Website")]
    private readonly EntityCollection<Page> _pages = new EntityCollection<Page>();
    [ReverseAssociation("Website")]
    private readonly EntityCollection<PiasOption> _piasOptions = new EntityCollection<PiasOption>();
    [ReverseAssociation("Website")]
    private readonly EntityCollection<Domain> _domains = new EntityCollection<Domain>();
    [ReverseAssociation("Website")]
    private readonly EntityCollection<SellYourGearSubmission> _sellYourGearSubmissions = new EntityCollection<SellYourGearSubmission>();
    [ReverseAssociation("Website")]
    private readonly EntityCollection<MgrOption> _mgrOptions = new EntityCollection<MgrOption>();
    [ReverseAssociation("Website")]
    private readonly EntityHolder<Account> _account = new EntityHolder<Account>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Page> Pages
    {
      get { return Get(_pages); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PiasOption> PiasOptions
    {
      get { return Get(_piasOptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Domain> Domains
    {
      get { return Get(_domains); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SellYourGearSubmission> SellYourGearSubmissions
    {
      get { return Get(_sellYourGearSubmissions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<MgrOption> MgrOptions
    {
      get { return Get(_mgrOptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Account Account
    {
      get { return Get(_account); }
      set { Set(_account, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int PrimaryDomainId
    {
      get { return Get(ref _primaryDomainId, "PrimaryDomainId"); }
      set { Set(ref _primaryDomainId, value, "PrimaryDomainId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string UserDirectory
    {
      get { return Get(ref _userDirectory, "UserDirectory"); }
      set { Set(ref _userDirectory, value, "UserDirectory"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsCustomerApproved
    {
      get { return Get(ref _isCustomerApproved, "IsCustomerApproved"); }
      set { Set(ref _isCustomerApproved, value, "IsCustomerApproved"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsWinmarkApproved
    {
      get { return Get(ref _isWinmarkApproved, "IsWinmarkApproved"); }
      set { Set(ref _isWinmarkApproved, value, "IsWinmarkApproved"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsTreefortApproved
    {
      get { return Get(ref _isTreefortApproved, "IsTreefortApproved"); }
      set { Set(ref _isTreefortApproved, value, "IsTreefortApproved"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string GoogleAnalyticsId
    {
      get { return Get(ref _googleAnalyticsId, "GoogleAnalyticsId"); }
      set { Set(ref _googleAnalyticsId, value, "GoogleAnalyticsId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string GoogleAnalyticsProfileId
    {
      get { return Get(ref _googleAnalyticsProfileId, "GoogleAnalyticsProfileId"); }
      set { Set(ref _googleAnalyticsProfileId, value, "GoogleAnalyticsProfileId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Account" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int AccountId
    {
      get { return Get(ref _accountId, "AccountId"); }
      set { Set(ref _accountId, value, "AccountId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("UserTypes")]
  public partial class UserType : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    private bool _isContentEditor;
    private bool _isAccountAdministrator;
    private bool _isFranchiseAdministrator;
    private bool _isSystemAdministrator;
    private bool _isGoldbookUser;
    private bool _isTrustedUser;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the IsContentEditor entity attribute.</summary>
    public const string IsContentEditorField = "IsContentEditor";
    /// <summary>Identifies the IsAccountAdministrator entity attribute.</summary>
    public const string IsAccountAdministratorField = "IsAccountAdministrator";
    /// <summary>Identifies the IsFranchiseAdministrator entity attribute.</summary>
    public const string IsFranchiseAdministratorField = "IsFranchiseAdministrator";
    /// <summary>Identifies the IsSystemAdministrator entity attribute.</summary>
    public const string IsSystemAdministratorField = "IsSystemAdministrator";
    /// <summary>Identifies the IsGoldbookUser entity attribute.</summary>
    public const string IsGoldbookUserField = "IsGoldbookUser";
    /// <summary>Identifies the IsTrustedUser entity attribute.</summary>
    public const string IsTrustedUserField = "IsTrustedUser";


    #endregion
    
    #region Relationships

    [ReverseAssociation("UserType")]
    private readonly EntityCollection<User> _usersByUserType = new EntityCollection<User>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<User> UsersByUserType
    {
      get { return Get(_usersByUserType); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsContentEditor
    {
      get { return Get(ref _isContentEditor, "IsContentEditor"); }
      set { Set(ref _isContentEditor, value, "IsContentEditor"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsAccountAdministrator
    {
      get { return Get(ref _isAccountAdministrator, "IsAccountAdministrator"); }
      set { Set(ref _isAccountAdministrator, value, "IsAccountAdministrator"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsFranchiseAdministrator
    {
      get { return Get(ref _isFranchiseAdministrator, "IsFranchiseAdministrator"); }
      set { Set(ref _isFranchiseAdministrator, value, "IsFranchiseAdministrator"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsSystemAdministrator
    {
      get { return Get(ref _isSystemAdministrator, "IsSystemAdministrator"); }
      set { Set(ref _isSystemAdministrator, value, "IsSystemAdministrator"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsGoldbookUser
    {
      get { return Get(ref _isGoldbookUser, "IsGoldbookUser"); }
      set { Set(ref _isGoldbookUser, value, "IsGoldbookUser"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsTrustedUser
    {
      get { return Get(ref _isTrustedUser, "IsTrustedUser"); }
      set { Set(ref _isTrustedUser, value, "IsTrustedUser"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Accounts")]
  public partial class Account : Entity<int>
  {
    #region Fields
  
    private bool _isCoop;
    [ValidateLength(0, 50)]
    private string _storeNumber;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _firstName;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _lastName;
    [ValidatePresence]
    [ValidateEmailAddress]
    [ValidateLength(0, 255)]
    private string _emailAddress;
    [ValidateLength(0, 50)]
    private string _phoneNumber;
    private System.Nullable<System.DateTime> _renewalDate;
    private bool _isRenewing;
    private System.Nullable<System.DateTime> _expiredCardReminderSentDate;
    private System.Nullable<long> _paymentProfileId;
    [ValidateLength(0, 50)]
    private string _cardNumberMasked;
    private System.Nullable<System.DateTime> _paymentExpires;
    private System.Nullable<long> _customerProfileId;
    private int _primaryLocationId;
    private int _franchiseId;
    private int _statusId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IsCoop entity attribute.</summary>
    public const string IsCoopField = "IsCoop";
    /// <summary>Identifies the StoreNumber entity attribute.</summary>
    public const string StoreNumberField = "StoreNumber";
    /// <summary>Identifies the FirstName entity attribute.</summary>
    public const string FirstNameField = "FirstName";
    /// <summary>Identifies the LastName entity attribute.</summary>
    public const string LastNameField = "LastName";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the PhoneNumber entity attribute.</summary>
    public const string PhoneNumberField = "PhoneNumber";
    /// <summary>Identifies the RenewalDate entity attribute.</summary>
    public const string RenewalDateField = "RenewalDate";
    /// <summary>Identifies the IsRenewing entity attribute.</summary>
    public const string IsRenewingField = "IsRenewing";
    /// <summary>Identifies the ExpiredCardReminderSentDate entity attribute.</summary>
    public const string ExpiredCardReminderSentDateField = "ExpiredCardReminderSentDate";
    /// <summary>Identifies the PaymentProfileId entity attribute.</summary>
    public const string PaymentProfileIdField = "PaymentProfileId";
    /// <summary>Identifies the CardNumberMasked entity attribute.</summary>
    public const string CardNumberMaskedField = "CardNumberMasked";
    /// <summary>Identifies the PaymentExpires entity attribute.</summary>
    public const string PaymentExpiresField = "PaymentExpires";
    /// <summary>Identifies the CustomerProfileId entity attribute.</summary>
    public const string CustomerProfileIdField = "CustomerProfileId";
    /// <summary>Identifies the PrimaryLocationId entity attribute.</summary>
    public const string PrimaryLocationIdField = "PrimaryLocationId";
    /// <summary>Identifies the FranchiseId entity attribute.</summary>
    public const string FranchiseIdField = "FranchiseId";
    /// <summary>Identifies the StatusId entity attribute.</summary>
    public const string StatusIdField = "StatusId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Account")]
    private readonly EntityCollection<AccountsLocation> _accountsLocationsByAccount = new EntityCollection<AccountsLocation>();
    [ReverseAssociation("Account")]
    private readonly EntityCollection<User> _usersByAccount = new EntityCollection<User>();
    [ReverseAssociation("OwnerAccounts")]
    private readonly EntityHolder<Location> _primaryLocation = new EntityHolder<Location>();
    [ReverseAssociation("Accounts")]
    private readonly EntityHolder<Franchise> _franchise = new EntityHolder<Franchise>();
    [ReverseAssociation("Accounts")]
    private readonly EntityHolder<AccountStatus> _status = new EntityHolder<AccountStatus>();
    [ReverseAssociation("Account")]
    private readonly EntityHolder<Website> _website = new EntityHolder<Website>();

    private ThroughAssociation<AccountsLocation, Location> _locations;

    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<AccountsLocation> AccountsLocationsByAccount
    {
      get { return Get(_accountsLocationsByAccount); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<User> UsersByAccount
    {
      get { return Get(_usersByAccount); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Location PrimaryLocation
    {
      get { return Get(_primaryLocation); }
      set { Set(_primaryLocation, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Franchise Franchise
    {
      get { return Get(_franchise); }
      set { Set(_franchise, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public AccountStatus Status
    {
      get { return Get(_status); }
      set { Set(_status, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Website Website
    {
      get { return Get(_website); }
      set { Set(_website, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public ThroughAssociation<AccountsLocation, Location> Locations
    {
      get
      {
        if (_locations == null)
        {
          _locations = new ThroughAssociation<AccountsLocation, Location>(_accountsLocationsByAccount);
        }
        return Get(_locations);
      }
    }
    

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsCoop
    {
      get { return Get(ref _isCoop, "IsCoop"); }
      set { Set(ref _isCoop, value, "IsCoop"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StoreNumber
    {
      get { return Get(ref _storeNumber, "StoreNumber"); }
      set { Set(ref _storeNumber, value, "StoreNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FirstName
    {
      get { return Get(ref _firstName, "FirstName"); }
      set { Set(ref _firstName, value, "FirstName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LastName
    {
      get { return Get(ref _lastName, "LastName"); }
      set { Set(ref _lastName, value, "LastName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PhoneNumber
    {
      get { return Get(ref _phoneNumber, "PhoneNumber"); }
      set { Set(ref _phoneNumber, value, "PhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> RenewalDate
    {
      get { return Get(ref _renewalDate, "RenewalDate"); }
      set { Set(ref _renewalDate, value, "RenewalDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsRenewing
    {
      get { return Get(ref _isRenewing, "IsRenewing"); }
      set { Set(ref _isRenewing, value, "IsRenewing"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ExpiredCardReminderSentDate
    {
      get { return Get(ref _expiredCardReminderSentDate, "ExpiredCardReminderSentDate"); }
      set { Set(ref _expiredCardReminderSentDate, value, "ExpiredCardReminderSentDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> PaymentProfileId
    {
      get { return Get(ref _paymentProfileId, "PaymentProfileId"); }
      set { Set(ref _paymentProfileId, value, "PaymentProfileId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CardNumberMasked
    {
      get { return Get(ref _cardNumberMasked, "CardNumberMasked"); }
      set { Set(ref _cardNumberMasked, value, "CardNumberMasked"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> PaymentExpires
    {
      get { return Get(ref _paymentExpires, "PaymentExpires"); }
      set { Set(ref _paymentExpires, value, "PaymentExpires"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<long> CustomerProfileId
    {
      get { return Get(ref _customerProfileId, "CustomerProfileId"); }
      set { Set(ref _customerProfileId, value, "CustomerProfileId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="PrimaryLocation" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PrimaryLocationId
    {
      get { return Get(ref _primaryLocationId, "PrimaryLocationId"); }
      set { Set(ref _primaryLocationId, value, "PrimaryLocationId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Franchise" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int FranchiseId
    {
      get { return Get(ref _franchiseId, "FranchiseId"); }
      set { Set(ref _franchiseId, value, "FranchiseId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Status" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int StatusId
    {
      get { return Get(ref _statusId, "StatusId"); }
      set { Set(ref _statusId, value, "StatusId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("AccountsLocations")]
  public partial class AccountsLocation : Entity<int>
  {
    #region Fields
  
    private int _locationId;
    private int _accountId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LocationId entity attribute.</summary>
    public const string LocationIdField = "LocationId";
    /// <summary>Identifies the AccountId entity attribute.</summary>
    public const string AccountIdField = "AccountId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("AccountsLocationsByLocation")]
    private readonly EntityHolder<Location> _location = new EntityHolder<Location>();
    [ReverseAssociation("AccountsLocationsByAccount")]
    private readonly EntityHolder<Account> _account = new EntityHolder<Account>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Location Location
    {
      get { return Get(_location); }
      set { Set(_location, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Account Account
    {
      get { return Get(_account); }
      set { Set(_account, value); }
    }


    /// <summary>Gets or sets the ID for the <see cref="Location" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int LocationId
    {
      get { return Get(ref _locationId, "LocationId"); }
      set { Set(ref _locationId, value, "LocationId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Account" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int AccountId
    {
      get { return Get(ref _accountId, "AccountId"); }
      set { Set(ref _accountId, value, "AccountId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Users")]
  public partial class User : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    [ValidateLength(0, 50)]
    private string _password;
    [ValidateLength(0, 255)]
    private string _passwordEncrypted;
    private int _userTypeId;
    private System.Nullable<int> _accountId;
    private System.Nullable<int> _franchiseId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Password entity attribute.</summary>
    public const string PasswordField = "Password";
    /// <summary>Identifies the PasswordEncrypted entity attribute.</summary>
    public const string PasswordEncryptedField = "PasswordEncrypted";
    /// <summary>Identifies the UserTypeId entity attribute.</summary>
    public const string UserTypeIdField = "UserTypeId";
    /// <summary>Identifies the AccountId entity attribute.</summary>
    public const string AccountIdField = "AccountId";
    /// <summary>Identifies the FranchiseId entity attribute.</summary>
    public const string FranchiseIdField = "FranchiseId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("User")]
    private readonly EntityCollection<PageRevision> _pageRevisions = new EntityCollection<PageRevision>();
    [ReverseAssociation("UsersByUserType")]
    private readonly EntityHolder<UserType> _userType = new EntityHolder<UserType>();
    [ReverseAssociation("UsersByAccount")]
    private readonly EntityHolder<Account> _account = new EntityHolder<Account>();
    [ReverseAssociation("Users")]
    private readonly EntityHolder<Franchise> _franchise = new EntityHolder<Franchise>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PageRevision> PageRevisions
    {
      get { return Get(_pageRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public UserType UserType
    {
      get { return Get(_userType); }
      set { Set(_userType, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Account Account
    {
      get { return Get(_account); }
      set { Set(_account, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Franchise Franchise
    {
      get { return Get(_franchise); }
      set { Set(_franchise, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Password
    {
      get { return Get(ref _password, "Password"); }
      set { Set(ref _password, value, "Password"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PasswordEncrypted
    {
      get { return Get(ref _passwordEncrypted, "PasswordEncrypted"); }
      set { Set(ref _passwordEncrypted, value, "PasswordEncrypted"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="UserType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int UserTypeId
    {
      get { return Get(ref _userTypeId, "UserTypeId"); }
      set { Set(ref _userTypeId, value, "UserTypeId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Account" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> AccountId
    {
      get { return Get(ref _accountId, "AccountId"); }
      set { Set(ref _accountId, value, "AccountId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Franchise" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> FranchiseId
    {
      get { return Get(ref _franchiseId, "FranchiseId"); }
      set { Set(ref _franchiseId, value, "FranchiseId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("aspL_Provinces", IdColumnName="Prov_Id")]
  public partial class AspLProvince : Entity<int>
  {
    #region Fields
  
    [Column("Prov_Abb")]
    [ValidateLength(0, 50)]
    private string _provAbb;
    [Column("Prov_Name")]
    [ValidateLength(0, 50)]
    private string _provName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ProvAbb entity attribute.</summary>
    public const string ProvAbbField = "ProvAbb";
    /// <summary>Identifies the ProvName entity attribute.</summary>
    public const string ProvNameField = "ProvName";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string ProvAbb
    {
      get { return Get(ref _provAbb, "ProvAbb"); }
      set { Set(ref _provAbb, value, "ProvAbb"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ProvName
    {
      get { return Get(ref _provName, "ProvName"); }
      set { Set(ref _provName, value, "ProvName"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("aspL_CAPostalCodes")]
  public partial class AspLCaPostalCode : Entity<int>
  {
    #region Fields
  
    [ValidateLength(0, 7)]
    private string _postalCode;
    [ValidateLength(0, 64)]
    private string _cityName;
    [ValidateLength(0, 1)]
    private string _cityType;
    [ValidateLength(0, 64)]
    private string _provinceName;
    [ValidateLength(0, 2)]
    private string _provinceAbbr;
    private System.Nullable<double> _latitude;
    private System.Nullable<double> _longitude;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the CityName entity attribute.</summary>
    public const string CityNameField = "CityName";
    /// <summary>Identifies the CityType entity attribute.</summary>
    public const string CityTypeField = "CityType";
    /// <summary>Identifies the ProvinceName entity attribute.</summary>
    public const string ProvinceNameField = "ProvinceName";
    /// <summary>Identifies the ProvinceAbbr entity attribute.</summary>
    public const string ProvinceAbbrField = "ProvinceAbbr";
    /// <summary>Identifies the Latitude entity attribute.</summary>
    public const string LatitudeField = "Latitude";
    /// <summary>Identifies the Longitude entity attribute.</summary>
    public const string LongitudeField = "Longitude";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CityName
    {
      get { return Get(ref _cityName, "CityName"); }
      set { Set(ref _cityName, value, "CityName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CityType
    {
      get { return Get(ref _cityType, "CityType"); }
      set { Set(ref _cityType, value, "CityType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ProvinceName
    {
      get { return Get(ref _provinceName, "ProvinceName"); }
      set { Set(ref _provinceName, value, "ProvinceName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ProvinceAbbr
    {
      get { return Get(ref _provinceAbbr, "ProvinceAbbr"); }
      set { Set(ref _provinceAbbr, value, "ProvinceAbbr"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> Latitude
    {
      get { return Get(ref _latitude, "Latitude"); }
      set { Set(ref _latitude, value, "Latitude"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> Longitude
    {
      get { return Get(ref _longitude, "Longitude"); }
      set { Set(ref _longitude, value, "Longitude"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("aspL_States", IdColumnName="State_Id")]
  public partial class AspLState : Entity<int>
  {
    #region Fields
  
    [Column("State_Abb")]
    [ValidateLength(0, 50)]
    private string _stateAbb;
    [Column("State_Name")]
    [ValidateLength(0, 50)]
    private string _stateName;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the StateAbb entity attribute.</summary>
    public const string StateAbbField = "StateAbb";
    /// <summary>Identifies the StateName entity attribute.</summary>
    public const string StateNameField = "StateName";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string StateAbb
    {
      get { return Get(ref _stateAbb, "StateAbb"); }
      set { Set(ref _stateAbb, value, "StateAbb"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string StateName
    {
      get { return Get(ref _stateName, "StateName"); }
      set { Set(ref _stateName, value, "StateName"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("aspL_Locations", IdColumnName="Location_Id")]
  public partial class AspLLocation : Entity<int>
  {
    #region Fields
  
    [Column("Location_Name")]
    [ValidateLength(0, 100)]
    private string _locationName;
    [Column("Location_Address")]
    [ValidateLength(0, 100)]
    private string _locationAddress;
    [Column("Location_City")]
    [ValidateLength(0, 100)]
    private string _locationCity;
    [Column("Location_State")]
    [ValidateLength(0, 50)]
    private string _locationState;
    [Column("Location_Zip")]
    [ValidateLength(0, 50)]
    private string _locationZip;
    [Column("Location_Phone")]
    [ValidateLength(0, 100)]
    private string _locationPhone;
    [Column("Location_Extension")]
    [ValidateLength(0, 50)]
    private string _locationExtension;
    [Column("Location_Email")]
    [ValidateLength(0, 100)]
    private string _locationEmail;
    [Column("Location_Website")]
    [ValidateLength(0, 250)]
    private string _locationWebsite;
    [Column("Location_Lon")]
    private System.Nullable<double> _locationLon;
    [Column("Location_Lat")]
    private System.Nullable<double> _locationLat;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LocationName entity attribute.</summary>
    public const string LocationNameField = "LocationName";
    /// <summary>Identifies the LocationAddress entity attribute.</summary>
    public const string LocationAddressField = "LocationAddress";
    /// <summary>Identifies the LocationCity entity attribute.</summary>
    public const string LocationCityField = "LocationCity";
    /// <summary>Identifies the LocationState entity attribute.</summary>
    public const string LocationStateField = "LocationState";
    /// <summary>Identifies the LocationZip entity attribute.</summary>
    public const string LocationZipField = "LocationZip";
    /// <summary>Identifies the LocationPhone entity attribute.</summary>
    public const string LocationPhoneField = "LocationPhone";
    /// <summary>Identifies the LocationExtension entity attribute.</summary>
    public const string LocationExtensionField = "LocationExtension";
    /// <summary>Identifies the LocationEmail entity attribute.</summary>
    public const string LocationEmailField = "LocationEmail";
    /// <summary>Identifies the LocationWebsite entity attribute.</summary>
    public const string LocationWebsiteField = "LocationWebsite";
    /// <summary>Identifies the LocationLon entity attribute.</summary>
    public const string LocationLonField = "LocationLon";
    /// <summary>Identifies the LocationLat entity attribute.</summary>
    public const string LocationLatField = "LocationLat";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationName
    {
      get { return Get(ref _locationName, "LocationName"); }
      set { Set(ref _locationName, value, "LocationName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationAddress
    {
      get { return Get(ref _locationAddress, "LocationAddress"); }
      set { Set(ref _locationAddress, value, "LocationAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationCity
    {
      get { return Get(ref _locationCity, "LocationCity"); }
      set { Set(ref _locationCity, value, "LocationCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationState
    {
      get { return Get(ref _locationState, "LocationState"); }
      set { Set(ref _locationState, value, "LocationState"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationZip
    {
      get { return Get(ref _locationZip, "LocationZip"); }
      set { Set(ref _locationZip, value, "LocationZip"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationPhone
    {
      get { return Get(ref _locationPhone, "LocationPhone"); }
      set { Set(ref _locationPhone, value, "LocationPhone"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationExtension
    {
      get { return Get(ref _locationExtension, "LocationExtension"); }
      set { Set(ref _locationExtension, value, "LocationExtension"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationEmail
    {
      get { return Get(ref _locationEmail, "LocationEmail"); }
      set { Set(ref _locationEmail, value, "LocationEmail"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LocationWebsite
    {
      get { return Get(ref _locationWebsite, "LocationWebsite"); }
      set { Set(ref _locationWebsite, value, "LocationWebsite"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> LocationLon
    {
      get { return Get(ref _locationLon, "LocationLon"); }
      set { Set(ref _locationLon, value, "LocationLon"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> LocationLat
    {
      get { return Get(ref _locationLat, "LocationLat"); }
      set { Set(ref _locationLat, value, "LocationLat"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("LocationHours")]
  public partial class LocationHour : Entity<int>
  {
    #region Fields
  
    private System.Nullable<System.DateTime> _mondayOpenTime;
    private System.Nullable<System.DateTime> _mondayCloseTime;
    private System.Nullable<System.DateTime> _tuesdayOpenTime;
    private System.Nullable<System.DateTime> _tuesdayCloseTime;
    private System.Nullable<System.DateTime> _wednesdayOpenTime;
    private System.Nullable<System.DateTime> _wednesdayCloseTime;
    private System.Nullable<System.DateTime> _thursdayOpenTime;
    private System.Nullable<System.DateTime> _thursdayCloseTime;
    private System.Nullable<System.DateTime> _fridayOpenTime;
    private System.Nullable<System.DateTime> _fridayCloseTime;
    private System.Nullable<System.DateTime> _saturdayOpenTime;
    private System.Nullable<System.DateTime> _saturdayCloseTime;
    private System.Nullable<System.DateTime> _sundayOpenTime;
    private System.Nullable<System.DateTime> _sundayCloseTime;
    private int _locationId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the MondayOpenTime entity attribute.</summary>
    public const string MondayOpenTimeField = "MondayOpenTime";
    /// <summary>Identifies the MondayCloseTime entity attribute.</summary>
    public const string MondayCloseTimeField = "MondayCloseTime";
    /// <summary>Identifies the TuesdayOpenTime entity attribute.</summary>
    public const string TuesdayOpenTimeField = "TuesdayOpenTime";
    /// <summary>Identifies the TuesdayCloseTime entity attribute.</summary>
    public const string TuesdayCloseTimeField = "TuesdayCloseTime";
    /// <summary>Identifies the WednesdayOpenTime entity attribute.</summary>
    public const string WednesdayOpenTimeField = "WednesdayOpenTime";
    /// <summary>Identifies the WednesdayCloseTime entity attribute.</summary>
    public const string WednesdayCloseTimeField = "WednesdayCloseTime";
    /// <summary>Identifies the ThursdayOpenTime entity attribute.</summary>
    public const string ThursdayOpenTimeField = "ThursdayOpenTime";
    /// <summary>Identifies the ThursdayCloseTime entity attribute.</summary>
    public const string ThursdayCloseTimeField = "ThursdayCloseTime";
    /// <summary>Identifies the FridayOpenTime entity attribute.</summary>
    public const string FridayOpenTimeField = "FridayOpenTime";
    /// <summary>Identifies the FridayCloseTime entity attribute.</summary>
    public const string FridayCloseTimeField = "FridayCloseTime";
    /// <summary>Identifies the SaturdayOpenTime entity attribute.</summary>
    public const string SaturdayOpenTimeField = "SaturdayOpenTime";
    /// <summary>Identifies the SaturdayCloseTime entity attribute.</summary>
    public const string SaturdayCloseTimeField = "SaturdayCloseTime";
    /// <summary>Identifies the SundayOpenTime entity attribute.</summary>
    public const string SundayOpenTimeField = "SundayOpenTime";
    /// <summary>Identifies the SundayCloseTime entity attribute.</summary>
    public const string SundayCloseTimeField = "SundayCloseTime";
    /// <summary>Identifies the LocationId entity attribute.</summary>
    public const string LocationIdField = "LocationId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("LocationHours")]
    private readonly EntityHolder<Location> _location = new EntityHolder<Location>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Location Location
    {
      get { return Get(_location); }
      set { Set(_location, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MondayOpenTime
    {
      get { return Get(ref _mondayOpenTime, "MondayOpenTime"); }
      set { Set(ref _mondayOpenTime, value, "MondayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> MondayCloseTime
    {
      get { return Get(ref _mondayCloseTime, "MondayCloseTime"); }
      set { Set(ref _mondayCloseTime, value, "MondayCloseTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> TuesdayOpenTime
    {
      get { return Get(ref _tuesdayOpenTime, "TuesdayOpenTime"); }
      set { Set(ref _tuesdayOpenTime, value, "TuesdayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> TuesdayCloseTime
    {
      get { return Get(ref _tuesdayCloseTime, "TuesdayCloseTime"); }
      set { Set(ref _tuesdayCloseTime, value, "TuesdayCloseTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> WednesdayOpenTime
    {
      get { return Get(ref _wednesdayOpenTime, "WednesdayOpenTime"); }
      set { Set(ref _wednesdayOpenTime, value, "WednesdayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> WednesdayCloseTime
    {
      get { return Get(ref _wednesdayCloseTime, "WednesdayCloseTime"); }
      set { Set(ref _wednesdayCloseTime, value, "WednesdayCloseTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ThursdayOpenTime
    {
      get { return Get(ref _thursdayOpenTime, "ThursdayOpenTime"); }
      set { Set(ref _thursdayOpenTime, value, "ThursdayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ThursdayCloseTime
    {
      get { return Get(ref _thursdayCloseTime, "ThursdayCloseTime"); }
      set { Set(ref _thursdayCloseTime, value, "ThursdayCloseTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> FridayOpenTime
    {
      get { return Get(ref _fridayOpenTime, "FridayOpenTime"); }
      set { Set(ref _fridayOpenTime, value, "FridayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> FridayCloseTime
    {
      get { return Get(ref _fridayCloseTime, "FridayCloseTime"); }
      set { Set(ref _fridayCloseTime, value, "FridayCloseTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> SaturdayOpenTime
    {
      get { return Get(ref _saturdayOpenTime, "SaturdayOpenTime"); }
      set { Set(ref _saturdayOpenTime, value, "SaturdayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> SaturdayCloseTime
    {
      get { return Get(ref _saturdayCloseTime, "SaturdayCloseTime"); }
      set { Set(ref _saturdayCloseTime, value, "SaturdayCloseTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> SundayOpenTime
    {
      get { return Get(ref _sundayOpenTime, "SundayOpenTime"); }
      set { Set(ref _sundayOpenTime, value, "SundayOpenTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> SundayCloseTime
    {
      get { return Get(ref _sundayCloseTime, "SundayCloseTime"); }
      set { Set(ref _sundayCloseTime, value, "SundayCloseTime"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Location" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int LocationId
    {
      get { return Get(ref _locationId, "LocationId"); }
      set { Set(ref _locationId, value, "LocationId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("aspL_Zips", IdColumnName="Zip_Code", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class AspLZip : Entity<int>
  {
    #region Fields
  
    [Column("Zip_City")]
    [ValidateLength(0, 100)]
    private string _zipCity;
    [Column("Zip_State")]
    [ValidateLength(0, 100)]
    private string _zipState;
    [Column("Zip_AreaCode")]
    [ValidateLength(0, 100)]
    private string _zipAreaCode;
    [Column("Zip_Lat")]
    private System.Nullable<double> _zipLat;
    [Column("Zip_Lon")]
    private System.Nullable<double> _zipLon;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the ZipCity entity attribute.</summary>
    public const string ZipCityField = "ZipCity";
    /// <summary>Identifies the ZipState entity attribute.</summary>
    public const string ZipStateField = "ZipState";
    /// <summary>Identifies the ZipAreaCode entity attribute.</summary>
    public const string ZipAreaCodeField = "ZipAreaCode";
    /// <summary>Identifies the ZipLat entity attribute.</summary>
    public const string ZipLatField = "ZipLat";
    /// <summary>Identifies the ZipLon entity attribute.</summary>
    public const string ZipLonField = "ZipLon";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public string ZipCity
    {
      get { return Get(ref _zipCity, "ZipCity"); }
      set { Set(ref _zipCity, value, "ZipCity"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ZipState
    {
      get { return Get(ref _zipState, "ZipState"); }
      set { Set(ref _zipState, value, "ZipState"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ZipAreaCode
    {
      get { return Get(ref _zipAreaCode, "ZipAreaCode"); }
      set { Set(ref _zipAreaCode, value, "ZipAreaCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> ZipLat
    {
      get { return Get(ref _zipLat, "ZipLat"); }
      set { Set(ref _zipLat, value, "ZipLat"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<double> ZipLon
    {
      get { return Get(ref _zipLon, "ZipLon"); }
      set { Set(ref _zipLon, value, "ZipLon"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Franchises")]
  public partial class Franchise : Entity<int>
  {
    #region Fields
  
    private bool _isActive;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _abbreviation;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _parentDomain;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _userDirectory;
    [ValidateLength(0, 255)]
    private string _previewUrl;
    [ValidateLength(0, 255)]
    private string _commercialsUrl;
    [ValidateLength(0, 255)]
    private string _webmailUrl;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _wwbName;
    private bool _wwbIsWebsiteSpecific;
    private bool _hasCommerce;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Abbreviation entity attribute.</summary>
    public const string AbbreviationField = "Abbreviation";
    /// <summary>Identifies the ParentDomain entity attribute.</summary>
    public const string ParentDomainField = "ParentDomain";
    /// <summary>Identifies the UserDirectory entity attribute.</summary>
    public const string UserDirectoryField = "UserDirectory";
    /// <summary>Identifies the PreviewUrl entity attribute.</summary>
    public const string PreviewUrlField = "PreviewUrl";
    /// <summary>Identifies the CommercialsUrl entity attribute.</summary>
    public const string CommercialsUrlField = "CommercialsUrl";
    /// <summary>Identifies the WebmailUrl entity attribute.</summary>
    public const string WebmailUrlField = "WebmailUrl";
    /// <summary>Identifies the WwbName entity attribute.</summary>
    public const string WwbNameField = "WwbName";
    /// <summary>Identifies the WwbIsWebsiteSpecific entity attribute.</summary>
    public const string WwbIsWebsiteSpecificField = "WwbIsWebsiteSpecific";
    /// <summary>Identifies the HasCommerce entity attribute.</summary>
    public const string HasCommerceField = "HasCommerce";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Franchise")]
    private readonly EntityCollection<User> _users = new EntityCollection<User>();
    [ReverseAssociation("Franchise")]
    private readonly EntityCollection<Account> _accounts = new EntityCollection<Account>();
    [ReverseAssociation("Franchise")]
    private readonly EntityCollection<Location> _locations = new EntityCollection<Location>();
    [ReverseAssociation("Franchise")]
    private readonly EntityCollection<PageType> _pageTypes = new EntityCollection<PageType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<User> Users
    {
      get { return Get(_users); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Account> Accounts
    {
      get { return Get(_accounts); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Location> Locations
    {
      get { return Get(_locations); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PageType> PageTypes
    {
      get { return Get(_pageTypes); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Abbreviation
    {
      get { return Get(ref _abbreviation, "Abbreviation"); }
      set { Set(ref _abbreviation, value, "Abbreviation"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ParentDomain
    {
      get { return Get(ref _parentDomain, "ParentDomain"); }
      set { Set(ref _parentDomain, value, "ParentDomain"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string UserDirectory
    {
      get { return Get(ref _userDirectory, "UserDirectory"); }
      set { Set(ref _userDirectory, value, "UserDirectory"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PreviewUrl
    {
      get { return Get(ref _previewUrl, "PreviewUrl"); }
      set { Set(ref _previewUrl, value, "PreviewUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string CommercialsUrl
    {
      get { return Get(ref _commercialsUrl, "CommercialsUrl"); }
      set { Set(ref _commercialsUrl, value, "CommercialsUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WebmailUrl
    {
      get { return Get(ref _webmailUrl, "WebmailUrl"); }
      set { Set(ref _webmailUrl, value, "WebmailUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WwbName
    {
      get { return Get(ref _wwbName, "WwbName"); }
      set { Set(ref _wwbName, value, "WwbName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool WwbIsWebsiteSpecific
    {
      get { return Get(ref _wwbIsWebsiteSpecific, "WwbIsWebsiteSpecific"); }
      set { Set(ref _wwbIsWebsiteSpecific, value, "WwbIsWebsiteSpecific"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HasCommerce
    {
      get { return Get(ref _hasCommerce, "HasCommerce"); }
      set { Set(ref _hasCommerce, value, "HasCommerce"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("PageRegions")]
  public partial class PageRegion : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 16)]
    private string _code;
    [ValidateLength(0, 50)]
    private string _name;
    [ValidateLength(0, 255)]
    private string _adminMessage;
    private bool _isFixed;
    private int _sequenceNumber;
    private int _pageTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the AdminMessage entity attribute.</summary>
    public const string AdminMessageField = "AdminMessage";
    /// <summary>Identifies the IsFixed entity attribute.</summary>
    public const string IsFixedField = "IsFixed";
    /// <summary>Identifies the SequenceNumber entity attribute.</summary>
    public const string SequenceNumberField = "SequenceNumber";
    /// <summary>Identifies the PageTypeId entity attribute.</summary>
    public const string PageTypeIdField = "PageTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PageRegion")]
    private readonly EntityCollection<WidgetOption> _widgetOptions = new EntityCollection<WidgetOption>();
    [ReverseAssociation("PageRegion")]
    private readonly EntityCollection<Widget> _widgetsByPageRegion = new EntityCollection<Widget>();
    [ReverseAssociation("PageRegions")]
    private readonly EntityHolder<PageType> _pageType = new EntityHolder<PageType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<WidgetOption> WidgetOptions
    {
      get { return Get(_widgetOptions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Widget> WidgetsByPageRegion
    {
      get { return Get(_widgetsByPageRegion); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public PageType PageType
    {
      get { return Get(_pageType); }
      set { Set(_pageType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AdminMessage
    {
      get { return Get(ref _adminMessage, "AdminMessage"); }
      set { Set(ref _adminMessage, value, "AdminMessage"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsFixed
    {
      get { return Get(ref _isFixed, "IsFixed"); }
      set { Set(ref _isFixed, value, "IsFixed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SequenceNumber
    {
      get { return Get(ref _sequenceNumber, "SequenceNumber"); }
      set { Set(ref _sequenceNumber, value, "SequenceNumber"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="PageType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PageTypeId
    {
      get { return Get(ref _pageTypeId, "PageTypeId"); }
      set { Set(ref _pageTypeId, value, "PageTypeId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("PageTypes")]
  public partial class PageType : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 16)]
    private string _code;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    private int _sequenceNumber;
    private bool _isOptional;
    [ValidateLength(0, 255)]
    private string _previewUrl;
    private bool _isMobile;
    private bool _hasSidebarImage;
    private int _franchiseId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the SequenceNumber entity attribute.</summary>
    public const string SequenceNumberField = "SequenceNumber";
    /// <summary>Identifies the IsOptional entity attribute.</summary>
    public const string IsOptionalField = "IsOptional";
    /// <summary>Identifies the PreviewUrl entity attribute.</summary>
    public const string PreviewUrlField = "PreviewUrl";
    /// <summary>Identifies the IsMobile entity attribute.</summary>
    public const string IsMobileField = "IsMobile";
    /// <summary>Identifies the HasSidebarImage entity attribute.</summary>
    public const string HasSidebarImageField = "HasSidebarImage";
    /// <summary>Identifies the FranchiseId entity attribute.</summary>
    public const string FranchiseIdField = "FranchiseId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PageType")]
    private readonly EntityCollection<Page> _pages = new EntityCollection<Page>();
    [ReverseAssociation("PageType")]
    private readonly EntityCollection<PageRegion> _pageRegions = new EntityCollection<PageRegion>();
    [ReverseAssociation("PageTypes")]
    private readonly EntityHolder<Franchise> _franchise = new EntityHolder<Franchise>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Page> Pages
    {
      get { return Get(_pages); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PageRegion> PageRegions
    {
      get { return Get(_pageRegions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Franchise Franchise
    {
      get { return Get(_franchise); }
      set { Set(_franchise, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SequenceNumber
    {
      get { return Get(ref _sequenceNumber, "SequenceNumber"); }
      set { Set(ref _sequenceNumber, value, "SequenceNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsOptional
    {
      get { return Get(ref _isOptional, "IsOptional"); }
      set { Set(ref _isOptional, value, "IsOptional"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PreviewUrl
    {
      get { return Get(ref _previewUrl, "PreviewUrl"); }
      set { Set(ref _previewUrl, value, "PreviewUrl"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsMobile
    {
      get { return Get(ref _isMobile, "IsMobile"); }
      set { Set(ref _isMobile, value, "IsMobile"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool HasSidebarImage
    {
      get { return Get(ref _hasSidebarImage, "HasSidebarImage"); }
      set { Set(ref _hasSidebarImage, value, "HasSidebarImage"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Franchise" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int FranchiseId
    {
      get { return Get(ref _franchiseId, "FranchiseId"); }
      set { Set(ref _franchiseId, value, "FranchiseId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("PageRevisions")]
  public partial class PageRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    private System.Nullable<System.DateTime> _createdTime;
    [ValidateLength(0, 255)]
    private string _headline;
    [ValidateLength(0, 255)]
    private string _sidebarImageReference;
    private System.Nullable<int> _userId;
    private int _pageId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the CreatedTime entity attribute.</summary>
    public const string CreatedTimeField = "CreatedTime";
    /// <summary>Identifies the Headline entity attribute.</summary>
    public const string HeadlineField = "Headline";
    /// <summary>Identifies the SidebarImageReference entity attribute.</summary>
    public const string SidebarImageReferenceField = "SidebarImageReference";
    /// <summary>Identifies the UserId entity attribute.</summary>
    public const string UserIdField = "UserId";
    /// <summary>Identifies the PageId entity attribute.</summary>
    public const string PageIdField = "PageId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PageRevisions")]
    private readonly EntityHolder<User> _user = new EntityHolder<User>();
    [ReverseAssociation("PageRevisions")]
    private readonly EntityHolder<Page> _page = new EntityHolder<Page>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public User User
    {
      get { return Get(_user); }
      set { Set(_user, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Page Page
    {
      get { return Get(_page); }
      set { Set(_page, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> CreatedTime
    {
      get { return Get(ref _createdTime, "CreatedTime"); }
      set { Set(ref _createdTime, value, "CreatedTime"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Headline
    {
      get { return Get(ref _headline, "Headline"); }
      set { Set(ref _headline, value, "Headline"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string SidebarImageReference
    {
      get { return Get(ref _sidebarImageReference, "SidebarImageReference"); }
      set { Set(ref _sidebarImageReference, value, "SidebarImageReference"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="User" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> UserId
    {
      get { return Get(ref _userId, "UserId"); }
      set { Set(ref _userId, value, "UserId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Page" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PageId
    {
      get { return Get(ref _pageId, "PageId"); }
      set { Set(ref _pageId, value, "PageId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Pages")]
  public partial class Page : Entity<int>
  {
    #region Fields
  
    private bool _isActive;
    private int _currentRevisionNumber;
    [ValidateLength(0, 255)]
    private string _browserTitle;
    private string _metaDescription;
    private System.Nullable<int> _websiteId;
    private int _pageTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the CurrentRevisionNumber entity attribute.</summary>
    public const string CurrentRevisionNumberField = "CurrentRevisionNumber";
    /// <summary>Identifies the BrowserTitle entity attribute.</summary>
    public const string BrowserTitleField = "BrowserTitle";
    /// <summary>Identifies the MetaDescription entity attribute.</summary>
    public const string MetaDescriptionField = "MetaDescription";
    /// <summary>Identifies the WebsiteId entity attribute.</summary>
    public const string WebsiteIdField = "WebsiteId";
    /// <summary>Identifies the PageTypeId entity attribute.</summary>
    public const string PageTypeIdField = "PageTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Page")]
    private readonly EntityCollection<PageRevision> _pageRevisions = new EntityCollection<PageRevision>();
    [ReverseAssociation("Page")]
    private readonly EntityCollection<Widget> _widgetsByPage = new EntityCollection<Widget>();
    [ReverseAssociation("Pages")]
    private readonly EntityHolder<Website> _website = new EntityHolder<Website>();
    [ReverseAssociation("Pages")]
    private readonly EntityHolder<PageType> _pageType = new EntityHolder<PageType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<PageRevision> PageRevisions
    {
      get { return Get(_pageRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Widget> WidgetsByPage
    {
      get { return Get(_widgetsByPage); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Website Website
    {
      get { return Get(_website); }
      set { Set(_website, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public PageType PageType
    {
      get { return Get(_pageType); }
      set { Set(_pageType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int CurrentRevisionNumber
    {
      get { return Get(ref _currentRevisionNumber, "CurrentRevisionNumber"); }
      set { Set(ref _currentRevisionNumber, value, "CurrentRevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string BrowserTitle
    {
      get { return Get(ref _browserTitle, "BrowserTitle"); }
      set { Set(ref _browserTitle, value, "BrowserTitle"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string MetaDescription
    {
      get { return Get(ref _metaDescription, "MetaDescription"); }
      set { Set(ref _metaDescription, value, "MetaDescription"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Website" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> WebsiteId
    {
      get { return Get(ref _websiteId, "WebsiteId"); }
      set { Set(ref _websiteId, value, "WebsiteId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="PageType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PageTypeId
    {
      get { return Get(ref _pageTypeId, "PageTypeId"); }
      set { Set(ref _pageTypeId, value, "PageTypeId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("TextWidgetRevisions")]
  public partial class TextWidgetRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    private string _content;
    private int _widgetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the Content entity attribute.</summary>
    public const string ContentField = "Content";
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("TextWidgetRevisions")]
    private readonly EntityHolder<Widget> _widget = new EntityHolder<Widget>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Widget Widget
    {
      get { return Get(_widget); }
      set { Set(_widget, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Content
    {
      get { return Get(ref _content, "Content"); }
      set { Set(ref _content, value, "Content"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Widget" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  public partial class TextWidgetRevisionsBackup : Entity<int>
  {
    #region Fields
  
    private int _widgetId;
    private int _revisionNumber;
    private string _content;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the Content entity attribute.</summary>
    public const string ContentField = "Content";


    #endregion
    
    #region Properties



    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Content
    {
      get { return Get(ref _content, "Content"); }
      set { Set(ref _content, value, "Content"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("WidgetOptions")]
  public partial class WidgetOption : Entity<int>
  {
    #region Fields
  
    private int _sequenceNumber;
    private int _pageRegionId;
    private int _widgetTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the SequenceNumber entity attribute.</summary>
    public const string SequenceNumberField = "SequenceNumber";
    /// <summary>Identifies the PageRegionId entity attribute.</summary>
    public const string PageRegionIdField = "PageRegionId";
    /// <summary>Identifies the WidgetTypeId entity attribute.</summary>
    public const string WidgetTypeIdField = "WidgetTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("WidgetOptions")]
    private readonly EntityHolder<PageRegion> _pageRegion = new EntityHolder<PageRegion>();
    [ReverseAssociation("WidgetOptions")]
    private readonly EntityHolder<WidgetType> _widgetType = new EntityHolder<WidgetType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public PageRegion PageRegion
    {
      get { return Get(_pageRegion); }
      set { Set(_pageRegion, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public WidgetType WidgetType
    {
      get { return Get(_widgetType); }
      set { Set(_widgetType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int SequenceNumber
    {
      get { return Get(ref _sequenceNumber, "SequenceNumber"); }
      set { Set(ref _sequenceNumber, value, "SequenceNumber"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="PageRegion" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PageRegionId
    {
      get { return Get(ref _pageRegionId, "PageRegionId"); }
      set { Set(ref _pageRegionId, value, "PageRegionId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="WidgetType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetTypeId
    {
      get { return Get(ref _widgetTypeId, "WidgetTypeId"); }
      set { Set(ref _widgetTypeId, value, "WidgetTypeId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("WidgetTypes")]
  public partial class WidgetType : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 16)]
    private string _code;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";


    #endregion
    
    #region Relationships

    [ReverseAssociation("WidgetType")]
    private readonly EntityCollection<Widget> _widgets = new EntityCollection<Widget>();
    [ReverseAssociation("WidgetType")]
    private readonly EntityCollection<WidgetOption> _widgetOptions = new EntityCollection<WidgetOption>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Widget> Widgets
    {
      get { return Get(_widgets); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<WidgetOption> WidgetOptions
    {
      get { return Get(_widgetOptions); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("WidgetRevisions")]
  public partial class WidgetRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    private bool _isActive;
    private int _sequenceNumber;
    private int _widgetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the SequenceNumber entity attribute.</summary>
    public const string SequenceNumberField = "SequenceNumber";
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("WidgetRevisions")]
    private readonly EntityHolder<Widget> _widget = new EntityHolder<Widget>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Widget Widget
    {
      get { return Get(_widget); }
      set { Set(_widget, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SequenceNumber
    {
      get { return Get(ref _sequenceNumber, "SequenceNumber"); }
      set { Set(ref _sequenceNumber, value, "SequenceNumber"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Widget" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Widgets")]
  public partial class Widget : Entity<int>
  {
    #region Fields
  
    [ValidateLength(0, 50)]
    private string _name;
    private int _createdRevisionNumber;
    private System.Nullable<int> _deletedRevisionNumber;
    private int _pageRegionId;
    private int _pageId;
    private int _widgetTypeId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the CreatedRevisionNumber entity attribute.</summary>
    public const string CreatedRevisionNumberField = "CreatedRevisionNumber";
    /// <summary>Identifies the DeletedRevisionNumber entity attribute.</summary>
    public const string DeletedRevisionNumberField = "DeletedRevisionNumber";
    /// <summary>Identifies the PageRegionId entity attribute.</summary>
    public const string PageRegionIdField = "PageRegionId";
    /// <summary>Identifies the PageId entity attribute.</summary>
    public const string PageIdField = "PageId";
    /// <summary>Identifies the WidgetTypeId entity attribute.</summary>
    public const string WidgetTypeIdField = "WidgetTypeId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Widget")]
    private readonly EntityCollection<WidgetRevision> _widgetRevisions = new EntityCollection<WidgetRevision>();
    [ReverseAssociation("Widget")]
    private readonly EntityCollection<TextWidgetRevision> _textWidgetRevisions = new EntityCollection<TextWidgetRevision>();
    [ReverseAssociation("Widget")]
    private readonly EntityCollection<EmbedWidgetRevision> _embedWidgetRevisions = new EntityCollection<EmbedWidgetRevision>();
    [ReverseAssociation("Widget")]
    private readonly EntityCollection<JobWidgetRevision> _jobWidgetRevisions = new EntityCollection<JobWidgetRevision>();
    [ReverseAssociation("Widget")]
    private readonly EntityCollection<SpecialOfferWidgetRevision> _specialOfferWidgetRevisions = new EntityCollection<SpecialOfferWidgetRevision>();
    [ReverseAssociation("Widget")]
    private readonly EntityCollection<SlideshowWidgetRevision> _slideshowWidgetRevisions = new EntityCollection<SlideshowWidgetRevision>();
    [ReverseAssociation("WidgetsByPageRegion")]
    private readonly EntityHolder<PageRegion> _pageRegion = new EntityHolder<PageRegion>();
    [ReverseAssociation("WidgetsByPage")]
    private readonly EntityHolder<Page> _page = new EntityHolder<Page>();
    [ReverseAssociation("Widgets")]
    private readonly EntityHolder<WidgetType> _widgetType = new EntityHolder<WidgetType>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<WidgetRevision> WidgetRevisions
    {
      get { return Get(_widgetRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<TextWidgetRevision> TextWidgetRevisions
    {
      get { return Get(_textWidgetRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<EmbedWidgetRevision> EmbedWidgetRevisions
    {
      get { return Get(_embedWidgetRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<JobWidgetRevision> JobWidgetRevisions
    {
      get { return Get(_jobWidgetRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SpecialOfferWidgetRevision> SpecialOfferWidgetRevisions
    {
      get { return Get(_specialOfferWidgetRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<SlideshowWidgetRevision> SlideshowWidgetRevisions
    {
      get { return Get(_slideshowWidgetRevisions); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public PageRegion PageRegion
    {
      get { return Get(_pageRegion); }
      set { Set(_pageRegion, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public Page Page
    {
      get { return Get(_page); }
      set { Set(_page, value); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public WidgetType WidgetType
    {
      get { return Get(_widgetType); }
      set { Set(_widgetType, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int CreatedRevisionNumber
    {
      get { return Get(ref _createdRevisionNumber, "CreatedRevisionNumber"); }
      set { Set(ref _createdRevisionNumber, value, "CreatedRevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> DeletedRevisionNumber
    {
      get { return Get(ref _deletedRevisionNumber, "DeletedRevisionNumber"); }
      set { Set(ref _deletedRevisionNumber, value, "DeletedRevisionNumber"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="PageRegion" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PageRegionId
    {
      get { return Get(ref _pageRegionId, "PageRegionId"); }
      set { Set(ref _pageRegionId, value, "PageRegionId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Page" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int PageId
    {
      get { return Get(ref _pageId, "PageId"); }
      set { Set(ref _pageId, value, "PageId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="WidgetType" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetTypeId
    {
      get { return Get(ref _widgetTypeId, "WidgetTypeId"); }
      set { Set(ref _widgetTypeId, value, "WidgetTypeId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("EmbedWidgetRevisions")]
  public partial class EmbedWidgetRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    [ValidateLength(0, 50)]
    private string _name;
    private string _content;
    private int _widgetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the Content entity attribute.</summary>
    public const string ContentField = "Content";
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("EmbedWidgetRevisions")]
    private readonly EntityHolder<Widget> _widget = new EntityHolder<Widget>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Widget Widget
    {
      get { return Get(_widget); }
      set { Set(_widget, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Content
    {
      get { return Get(ref _content, "Content"); }
      set { Set(ref _content, value, "Content"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Widget" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("JobWidgetRevisions")]
  public partial class JobWidgetRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    [ValidateLength(0, 255)]
    private string _title;
    [ValidateLength(0, 1000)]
    private string _summary;
    private System.Nullable<System.DateTime> _expireDate;
    [ValidateLength(0, 255)]
    private string _compensation;
    private string _description;
    private int _widgetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the Title entity attribute.</summary>
    public const string TitleField = "Title";
    /// <summary>Identifies the Summary entity attribute.</summary>
    public const string SummaryField = "Summary";
    /// <summary>Identifies the ExpireDate entity attribute.</summary>
    public const string ExpireDateField = "ExpireDate";
    /// <summary>Identifies the Compensation entity attribute.</summary>
    public const string CompensationField = "Compensation";
    /// <summary>Identifies the Description entity attribute.</summary>
    public const string DescriptionField = "Description";
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("JobWidgetRevisions")]
    private readonly EntityHolder<Widget> _widget = new EntityHolder<Widget>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Widget Widget
    {
      get { return Get(_widget); }
      set { Set(_widget, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Title
    {
      get { return Get(ref _title, "Title"); }
      set { Set(ref _title, value, "Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Summary
    {
      get { return Get(ref _summary, "Summary"); }
      set { Set(ref _summary, value, "Summary"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> ExpireDate
    {
      get { return Get(ref _expireDate, "ExpireDate"); }
      set { Set(ref _expireDate, value, "ExpireDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Compensation
    {
      get { return Get(ref _compensation, "Compensation"); }
      set { Set(ref _compensation, value, "Compensation"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Description
    {
      get { return Get(ref _description, "Description"); }
      set { Set(ref _description, value, "Description"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Widget" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("SpecialOfferWidgetRevisions")]
  public partial class SpecialOfferWidgetRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    [ValidateLength(0, 255)]
    private string _title;
    private string _text;
    [ValidateLength(0, 255)]
    private string _imageReference;
    [ValidateLength(0, 255)]
    private string _documentReference;
    private System.Nullable<System.DateTime> _startDate;
    private System.Nullable<System.DateTime> _endDate;
    private int _widgetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the Title entity attribute.</summary>
    public const string TitleField = "Title";
    /// <summary>Identifies the Text entity attribute.</summary>
    public const string TextField = "Text";
    /// <summary>Identifies the ImageReference entity attribute.</summary>
    public const string ImageReferenceField = "ImageReference";
    /// <summary>Identifies the DocumentReference entity attribute.</summary>
    public const string DocumentReferenceField = "DocumentReference";
    /// <summary>Identifies the StartDate entity attribute.</summary>
    public const string StartDateField = "StartDate";
    /// <summary>Identifies the EndDate entity attribute.</summary>
    public const string EndDateField = "EndDate";
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SpecialOfferWidgetRevisions")]
    private readonly EntityHolder<Widget> _widget = new EntityHolder<Widget>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Widget Widget
    {
      get { return Get(_widget); }
      set { Set(_widget, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Title
    {
      get { return Get(ref _title, "Title"); }
      set { Set(ref _title, value, "Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Text
    {
      get { return Get(ref _text, "Text"); }
      set { Set(ref _text, value, "Text"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ImageReference
    {
      get { return Get(ref _imageReference, "ImageReference"); }
      set { Set(ref _imageReference, value, "ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string DocumentReference
    {
      get { return Get(ref _documentReference, "DocumentReference"); }
      set { Set(ref _documentReference, value, "DocumentReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> StartDate
    {
      get { return Get(ref _startDate, "StartDate"); }
      set { Set(ref _startDate, value, "StartDate"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<System.DateTime> EndDate
    {
      get { return Get(ref _endDate, "EndDate"); }
      set { Set(ref _endDate, value, "EndDate"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Widget" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("PiasOptions")]
  public partial class PiasOption : Entity<int>
  {
    #region Fields
  
    [ValidateLength(0, 255)]
    private string _logoCaptionLine1;
    [ValidateLength(0, 255)]
    private string _logoCaptionLine2;
    private bool _isLogoCaptionVisible;
    private bool _isRotatorVisible;
    [ValidateLength(0, 255)]
    private string _announcementLine1;
    [ValidateLength(0, 255)]
    private string _announcementLine2;
    private bool _isAnnouncementVisible;
    private bool _isNewsVisible;
    [ValidateLength(0, 255)]
    private string _homepageSubtext;
    private bool _isHomepageSubtextVisible;
    [ValidatePresence]
    private string _faqBoxContent;
    [ValidatePresence]
    private string _whatWeBuyBoxContent;
    private string _whatsItWorthRecipient;
    private System.Nullable<int> _websiteId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the LogoCaptionLine1 entity attribute.</summary>
    public const string LogoCaptionLine1Field = "LogoCaptionLine1";
    /// <summary>Identifies the LogoCaptionLine2 entity attribute.</summary>
    public const string LogoCaptionLine2Field = "LogoCaptionLine2";
    /// <summary>Identifies the IsLogoCaptionVisible entity attribute.</summary>
    public const string IsLogoCaptionVisibleField = "IsLogoCaptionVisible";
    /// <summary>Identifies the IsRotatorVisible entity attribute.</summary>
    public const string IsRotatorVisibleField = "IsRotatorVisible";
    /// <summary>Identifies the AnnouncementLine1 entity attribute.</summary>
    public const string AnnouncementLine1Field = "AnnouncementLine1";
    /// <summary>Identifies the AnnouncementLine2 entity attribute.</summary>
    public const string AnnouncementLine2Field = "AnnouncementLine2";
    /// <summary>Identifies the IsAnnouncementVisible entity attribute.</summary>
    public const string IsAnnouncementVisibleField = "IsAnnouncementVisible";
    /// <summary>Identifies the IsNewsVisible entity attribute.</summary>
    public const string IsNewsVisibleField = "IsNewsVisible";
    /// <summary>Identifies the HomepageSubtext entity attribute.</summary>
    public const string HomepageSubtextField = "HomepageSubtext";
    /// <summary>Identifies the IsHomepageSubtextVisible entity attribute.</summary>
    public const string IsHomepageSubtextVisibleField = "IsHomepageSubtextVisible";
    /// <summary>Identifies the FaqBoxContent entity attribute.</summary>
    public const string FaqBoxContentField = "FaqBoxContent";
    /// <summary>Identifies the WhatWeBuyBoxContent entity attribute.</summary>
    public const string WhatWeBuyBoxContentField = "WhatWeBuyBoxContent";
    /// <summary>Identifies the WhatsItWorthRecipient entity attribute.</summary>
    public const string WhatsItWorthRecipientField = "WhatsItWorthRecipient";
    /// <summary>Identifies the WebsiteId entity attribute.</summary>
    public const string WebsiteIdField = "WebsiteId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("PiasOptions")]
    private readonly EntityHolder<Website> _website = new EntityHolder<Website>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Website Website
    {
      get { return Get(_website); }
      set { Set(_website, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string LogoCaptionLine1
    {
      get { return Get(ref _logoCaptionLine1, "LogoCaptionLine1"); }
      set { Set(ref _logoCaptionLine1, value, "LogoCaptionLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string LogoCaptionLine2
    {
      get { return Get(ref _logoCaptionLine2, "LogoCaptionLine2"); }
      set { Set(ref _logoCaptionLine2, value, "LogoCaptionLine2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsLogoCaptionVisible
    {
      get { return Get(ref _isLogoCaptionVisible, "IsLogoCaptionVisible"); }
      set { Set(ref _isLogoCaptionVisible, value, "IsLogoCaptionVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsRotatorVisible
    {
      get { return Get(ref _isRotatorVisible, "IsRotatorVisible"); }
      set { Set(ref _isRotatorVisible, value, "IsRotatorVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AnnouncementLine1
    {
      get { return Get(ref _announcementLine1, "AnnouncementLine1"); }
      set { Set(ref _announcementLine1, value, "AnnouncementLine1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string AnnouncementLine2
    {
      get { return Get(ref _announcementLine2, "AnnouncementLine2"); }
      set { Set(ref _announcementLine2, value, "AnnouncementLine2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsAnnouncementVisible
    {
      get { return Get(ref _isAnnouncementVisible, "IsAnnouncementVisible"); }
      set { Set(ref _isAnnouncementVisible, value, "IsAnnouncementVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsNewsVisible
    {
      get { return Get(ref _isNewsVisible, "IsNewsVisible"); }
      set { Set(ref _isNewsVisible, value, "IsNewsVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string HomepageSubtext
    {
      get { return Get(ref _homepageSubtext, "HomepageSubtext"); }
      set { Set(ref _homepageSubtext, value, "HomepageSubtext"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsHomepageSubtextVisible
    {
      get { return Get(ref _isHomepageSubtextVisible, "IsHomepageSubtextVisible"); }
      set { Set(ref _isHomepageSubtextVisible, value, "IsHomepageSubtextVisible"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string FaqBoxContent
    {
      get { return Get(ref _faqBoxContent, "FaqBoxContent"); }
      set { Set(ref _faqBoxContent, value, "FaqBoxContent"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WhatWeBuyBoxContent
    {
      get { return Get(ref _whatWeBuyBoxContent, "WhatWeBuyBoxContent"); }
      set { Set(ref _whatWeBuyBoxContent, value, "WhatWeBuyBoxContent"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string WhatsItWorthRecipient
    {
      get { return Get(ref _whatsItWorthRecipient, "WhatsItWorthRecipient"); }
      set { Set(ref _whatsItWorthRecipient, value, "WhatsItWorthRecipient"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Website" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public System.Nullable<int> WebsiteId
    {
      get { return Get(ref _websiteId, "WebsiteId"); }
      set { Set(ref _websiteId, value, "WebsiteId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("Domains")]
  public partial class Domain : Entity<int>
  {
    #region Fields
  
    private bool _isActive;
    [ValidatePresence]
    [ValidateLength(0, 100)]
    private string _name;
    [ValidateLength(0, 50)]
    private string _googleAnalyticsProfileId;
    private int _websiteId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the GoogleAnalyticsProfileId entity attribute.</summary>
    public const string GoogleAnalyticsProfileIdField = "GoogleAnalyticsProfileId";
    /// <summary>Identifies the WebsiteId entity attribute.</summary>
    public const string WebsiteIdField = "WebsiteId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Domains")]
    private readonly EntityHolder<Website> _website = new EntityHolder<Website>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Website Website
    {
      get { return Get(_website); }
      set { Set(_website, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string GoogleAnalyticsProfileId
    {
      get { return Get(ref _googleAnalyticsProfileId, "GoogleAnalyticsProfileId"); }
      set { Set(ref _googleAnalyticsProfileId, value, "GoogleAnalyticsProfileId"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Website" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WebsiteId
    {
      get { return Get(ref _websiteId, "WebsiteId"); }
      set { Set(ref _websiteId, value, "WebsiteId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("AccountStatuses")]
  public partial class AccountStatus : Entity<int>
  {
    #region Fields
  
    [ValidatePresence]
    [ValidateLength(0, 16)]
    private string _code;
    [ValidatePresence]
    [ValidateLength(0, 50)]
    private string _name;
    private bool _isActive;
    private bool _isClosed;
    private int _sequenceNumber;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the Code entity attribute.</summary>
    public const string CodeField = "Code";
    /// <summary>Identifies the Name entity attribute.</summary>
    public const string NameField = "Name";
    /// <summary>Identifies the IsActive entity attribute.</summary>
    public const string IsActiveField = "IsActive";
    /// <summary>Identifies the IsClosed entity attribute.</summary>
    public const string IsClosedField = "IsClosed";
    /// <summary>Identifies the SequenceNumber entity attribute.</summary>
    public const string SequenceNumberField = "SequenceNumber";


    #endregion
    
    #region Relationships

    [ReverseAssociation("Status")]
    private readonly EntityCollection<Account> _accounts = new EntityCollection<Account>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public EntityCollection<Account> Accounts
    {
      get { return Get(_accounts); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string Code
    {
      get { return Get(ref _code, "Code"); }
      set { Set(ref _code, value, "Code"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Name
    {
      get { return Get(ref _name, "Name"); }
      set { Set(ref _name, value, "Name"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsActive
    {
      get { return Get(ref _isActive, "IsActive"); }
      set { Set(ref _isActive, value, "IsActive"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool IsClosed
    {
      get { return Get(ref _isClosed, "IsClosed"); }
      set { Set(ref _isClosed, value, "IsClosed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public int SequenceNumber
    {
      get { return Get(ref _sequenceNumber, "SequenceNumber"); }
      set { Set(ref _sequenceNumber, value, "SequenceNumber"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("SellYourGearSubmissions", IdColumnName="Id", IdentityMethod=IdentityMethod.IdentityColumn)]
  public partial class SellYourGearSubmission : Entity<int>
  {
    #region Fields
  
    [ValidateLength(0, 255)]
    private string _customerName;
    [ValidateLength(0, 255)]
    private string _emailAddress;
    [ValidateLength(0, 50)]
    private string _phoneNumber;
    [ValidateLength(0, 50)]
    private string _contactPreference;
    [ValidateLength(0, 255)]
    private string _city;
    [ValidateLength(0, 50)]
    private string _region;
    [ValidateLength(0, 50)]
    private string _postalCode;
    [ValidateLength(0, 50)]
    private string _gearType;
    [ValidateLength(0, 255)]
    private string _makeAndModel;
    [ValidateLength(0, 50)]
    private string _color;
    [ValidateLength(0, 50)]
    private string _year;
    [ValidateLength(0, 50)]
    private string _condition;
    private string _describe;
    private string _modifications;
    [ValidateLength(0, 255)]
    private string _imageReference1;
    [ValidateLength(0, 255)]
    private string _imageReference2;
    [ValidateLength(0, 255)]
    private string _imageReference3;
    [ValidateLength(0, 255)]
    private string _imageReference4;
    private bool _viewed;
    private bool _contacted;
    private bool _purchased;
    private System.DateTime _submittedOn;
    private int _websiteId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the CustomerName entity attribute.</summary>
    public const string CustomerNameField = "CustomerName";
    /// <summary>Identifies the EmailAddress entity attribute.</summary>
    public const string EmailAddressField = "EmailAddress";
    /// <summary>Identifies the PhoneNumber entity attribute.</summary>
    public const string PhoneNumberField = "PhoneNumber";
    /// <summary>Identifies the ContactPreference entity attribute.</summary>
    public const string ContactPreferenceField = "ContactPreference";
    /// <summary>Identifies the City entity attribute.</summary>
    public const string CityField = "City";
    /// <summary>Identifies the Region entity attribute.</summary>
    public const string RegionField = "Region";
    /// <summary>Identifies the PostalCode entity attribute.</summary>
    public const string PostalCodeField = "PostalCode";
    /// <summary>Identifies the GearType entity attribute.</summary>
    public const string GearTypeField = "GearType";
    /// <summary>Identifies the MakeAndModel entity attribute.</summary>
    public const string MakeAndModelField = "MakeAndModel";
    /// <summary>Identifies the Color entity attribute.</summary>
    public const string ColorField = "Color";
    /// <summary>Identifies the Year entity attribute.</summary>
    public const string YearField = "Year";
    /// <summary>Identifies the Condition entity attribute.</summary>
    public const string ConditionField = "Condition";
    /// <summary>Identifies the Describe entity attribute.</summary>
    public const string DescribeField = "Describe";
    /// <summary>Identifies the Modifications entity attribute.</summary>
    public const string ModificationsField = "Modifications";
    /// <summary>Identifies the ImageReference1 entity attribute.</summary>
    public const string ImageReference1Field = "ImageReference1";
    /// <summary>Identifies the ImageReference2 entity attribute.</summary>
    public const string ImageReference2Field = "ImageReference2";
    /// <summary>Identifies the ImageReference3 entity attribute.</summary>
    public const string ImageReference3Field = "ImageReference3";
    /// <summary>Identifies the ImageReference4 entity attribute.</summary>
    public const string ImageReference4Field = "ImageReference4";
    /// <summary>Identifies the Viewed entity attribute.</summary>
    public const string ViewedField = "Viewed";
    /// <summary>Identifies the Contacted entity attribute.</summary>
    public const string ContactedField = "Contacted";
    /// <summary>Identifies the Purchased entity attribute.</summary>
    public const string PurchasedField = "Purchased";
    /// <summary>Identifies the SubmittedOn entity attribute.</summary>
    public const string SubmittedOnField = "SubmittedOn";
    /// <summary>Identifies the WebsiteId entity attribute.</summary>
    public const string WebsiteIdField = "WebsiteId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SellYourGearSubmissions")]
    private readonly EntityHolder<Website> _website = new EntityHolder<Website>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Website Website
    {
      get { return Get(_website); }
      set { Set(_website, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public string CustomerName
    {
      get { return Get(ref _customerName, "CustomerName"); }
      set { Set(ref _customerName, value, "CustomerName"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string EmailAddress
    {
      get { return Get(ref _emailAddress, "EmailAddress"); }
      set { Set(ref _emailAddress, value, "EmailAddress"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PhoneNumber
    {
      get { return Get(ref _phoneNumber, "PhoneNumber"); }
      set { Set(ref _phoneNumber, value, "PhoneNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ContactPreference
    {
      get { return Get(ref _contactPreference, "ContactPreference"); }
      set { Set(ref _contactPreference, value, "ContactPreference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string City
    {
      get { return Get(ref _city, "City"); }
      set { Set(ref _city, value, "City"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Region
    {
      get { return Get(ref _region, "Region"); }
      set { Set(ref _region, value, "Region"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string PostalCode
    {
      get { return Get(ref _postalCode, "PostalCode"); }
      set { Set(ref _postalCode, value, "PostalCode"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string GearType
    {
      get { return Get(ref _gearType, "GearType"); }
      set { Set(ref _gearType, value, "GearType"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string MakeAndModel
    {
      get { return Get(ref _makeAndModel, "MakeAndModel"); }
      set { Set(ref _makeAndModel, value, "MakeAndModel"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Color
    {
      get { return Get(ref _color, "Color"); }
      set { Set(ref _color, value, "Color"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Year
    {
      get { return Get(ref _year, "Year"); }
      set { Set(ref _year, value, "Year"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Condition
    {
      get { return Get(ref _condition, "Condition"); }
      set { Set(ref _condition, value, "Condition"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Describe
    {
      get { return Get(ref _describe, "Describe"); }
      set { Set(ref _describe, value, "Describe"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Modifications
    {
      get { return Get(ref _modifications, "Modifications"); }
      set { Set(ref _modifications, value, "Modifications"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ImageReference1
    {
      get { return Get(ref _imageReference1, "ImageReference1"); }
      set { Set(ref _imageReference1, value, "ImageReference1"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ImageReference2
    {
      get { return Get(ref _imageReference2, "ImageReference2"); }
      set { Set(ref _imageReference2, value, "ImageReference2"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ImageReference3
    {
      get { return Get(ref _imageReference3, "ImageReference3"); }
      set { Set(ref _imageReference3, value, "ImageReference3"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string ImageReference4
    {
      get { return Get(ref _imageReference4, "ImageReference4"); }
      set { Set(ref _imageReference4, value, "ImageReference4"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Viewed
    {
      get { return Get(ref _viewed, "Viewed"); }
      set { Set(ref _viewed, value, "Viewed"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Contacted
    {
      get { return Get(ref _contacted, "Contacted"); }
      set { Set(ref _contacted, value, "Contacted"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public bool Purchased
    {
      get { return Get(ref _purchased, "Purchased"); }
      set { Set(ref _purchased, value, "Purchased"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public System.DateTime SubmittedOn
    {
      get { return Get(ref _submittedOn, "SubmittedOn"); }
      set { Set(ref _submittedOn, value, "SubmittedOn"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Website" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WebsiteId
    {
      get { return Get(ref _websiteId, "WebsiteId"); }
      set { Set(ref _websiteId, value, "WebsiteId"); }
    }

    #endregion
  }


  [Serializable]
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  [System.ComponentModel.DataObject]
  [Table("SlideshowWidgetRevisions")]
  public partial class SlideshowWidgetRevision : Entity<int>
  {
    #region Fields
  
    private int _revisionNumber;
    [ValidateLength(0, 255)]
    private string _slide1ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide1Title;
    [ValidateLength(0, 4000)]
    private string _slide1Link;
    [ValidateLength(0, 255)]
    private string _slide2ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide2Title;
    [ValidateLength(0, 4000)]
    private string _slide2Link;
    [ValidateLength(0, 255)]
    private string _slide3ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide3Title;
    [ValidateLength(0, 4000)]
    private string _slide3Link;
    [ValidateLength(0, 255)]
    private string _slide4ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide4Title;
    [ValidateLength(0, 4000)]
    private string _slide4Link;
    [ValidateLength(0, 255)]
    private string _slide5ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide5Title;
    [ValidateLength(0, 4000)]
    private string _slide5Link;
    [ValidateLength(0, 255)]
    private string _slide6ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide6Title;
    [ValidateLength(0, 4000)]
    private string _slide6Link;
    [ValidateLength(0, 255)]
    private string _slide7ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide7Title;
    [ValidateLength(0, 4000)]
    private string _slide7Link;
    [ValidateLength(0, 255)]
    private string _slide8ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide8Title;
    [ValidateLength(0, 4000)]
    private string _slide8Link;
    [ValidateLength(0, 255)]
    private string _slide9ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide9Title;
    [ValidateLength(0, 4000)]
    private string _slide9Link;
    [ValidateLength(0, 255)]
    private string _slide10ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide10Title;
    [ValidateLength(0, 4000)]
    private string _slide10Link;
    [ValidateLength(0, 255)]
    private string _slide11ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide11Title;
    [ValidateLength(0, 4000)]
    private string _slide11Link;
    [ValidateLength(0, 255)]
    private string _slide12ImageReference;
    [ValidateLength(0, 4000)]
    private string _slide12Title;
    [ValidateLength(0, 4000)]
    private string _slide12Link;
    private int _widgetId;

    #endregion
    
    #region Field attribute and view names
    
    /// <summary>Identifies the RevisionNumber entity attribute.</summary>
    public const string RevisionNumberField = "RevisionNumber";
    /// <summary>Identifies the Slide1ImageReference entity attribute.</summary>
    public const string Slide1ImageReferenceField = "Slide1ImageReference";
    /// <summary>Identifies the Slide1Title entity attribute.</summary>
    public const string Slide1TitleField = "Slide1Title";
    /// <summary>Identifies the Slide1Link entity attribute.</summary>
    public const string Slide1LinkField = "Slide1Link";
    /// <summary>Identifies the Slide2ImageReference entity attribute.</summary>
    public const string Slide2ImageReferenceField = "Slide2ImageReference";
    /// <summary>Identifies the Slide2Title entity attribute.</summary>
    public const string Slide2TitleField = "Slide2Title";
    /// <summary>Identifies the Slide2Link entity attribute.</summary>
    public const string Slide2LinkField = "Slide2Link";
    /// <summary>Identifies the Slide3ImageReference entity attribute.</summary>
    public const string Slide3ImageReferenceField = "Slide3ImageReference";
    /// <summary>Identifies the Slide3Title entity attribute.</summary>
    public const string Slide3TitleField = "Slide3Title";
    /// <summary>Identifies the Slide3Link entity attribute.</summary>
    public const string Slide3LinkField = "Slide3Link";
    /// <summary>Identifies the Slide4ImageReference entity attribute.</summary>
    public const string Slide4ImageReferenceField = "Slide4ImageReference";
    /// <summary>Identifies the Slide4Title entity attribute.</summary>
    public const string Slide4TitleField = "Slide4Title";
    /// <summary>Identifies the Slide4Link entity attribute.</summary>
    public const string Slide4LinkField = "Slide4Link";
    /// <summary>Identifies the Slide5ImageReference entity attribute.</summary>
    public const string Slide5ImageReferenceField = "Slide5ImageReference";
    /// <summary>Identifies the Slide5Title entity attribute.</summary>
    public const string Slide5TitleField = "Slide5Title";
    /// <summary>Identifies the Slide5Link entity attribute.</summary>
    public const string Slide5LinkField = "Slide5Link";
    /// <summary>Identifies the Slide6ImageReference entity attribute.</summary>
    public const string Slide6ImageReferenceField = "Slide6ImageReference";
    /// <summary>Identifies the Slide6Title entity attribute.</summary>
    public const string Slide6TitleField = "Slide6Title";
    /// <summary>Identifies the Slide6Link entity attribute.</summary>
    public const string Slide6LinkField = "Slide6Link";
    /// <summary>Identifies the Slide7ImageReference entity attribute.</summary>
    public const string Slide7ImageReferenceField = "Slide7ImageReference";
    /// <summary>Identifies the Slide7Title entity attribute.</summary>
    public const string Slide7TitleField = "Slide7Title";
    /// <summary>Identifies the Slide7Link entity attribute.</summary>
    public const string Slide7LinkField = "Slide7Link";
    /// <summary>Identifies the Slide8ImageReference entity attribute.</summary>
    public const string Slide8ImageReferenceField = "Slide8ImageReference";
    /// <summary>Identifies the Slide8Title entity attribute.</summary>
    public const string Slide8TitleField = "Slide8Title";
    /// <summary>Identifies the Slide8Link entity attribute.</summary>
    public const string Slide8LinkField = "Slide8Link";
    /// <summary>Identifies the Slide9ImageReference entity attribute.</summary>
    public const string Slide9ImageReferenceField = "Slide9ImageReference";
    /// <summary>Identifies the Slide9Title entity attribute.</summary>
    public const string Slide9TitleField = "Slide9Title";
    /// <summary>Identifies the Slide9Link entity attribute.</summary>
    public const string Slide9LinkField = "Slide9Link";
    /// <summary>Identifies the Slide10ImageReference entity attribute.</summary>
    public const string Slide10ImageReferenceField = "Slide10ImageReference";
    /// <summary>Identifies the Slide10Title entity attribute.</summary>
    public const string Slide10TitleField = "Slide10Title";
    /// <summary>Identifies the Slide10Link entity attribute.</summary>
    public const string Slide10LinkField = "Slide10Link";
    /// <summary>Identifies the Slide11ImageReference entity attribute.</summary>
    public const string Slide11ImageReferenceField = "Slide11ImageReference";
    /// <summary>Identifies the Slide11Title entity attribute.</summary>
    public const string Slide11TitleField = "Slide11Title";
    /// <summary>Identifies the Slide11Link entity attribute.</summary>
    public const string Slide11LinkField = "Slide11Link";
    /// <summary>Identifies the Slide12ImageReference entity attribute.</summary>
    public const string Slide12ImageReferenceField = "Slide12ImageReference";
    /// <summary>Identifies the Slide12Title entity attribute.</summary>
    public const string Slide12TitleField = "Slide12Title";
    /// <summary>Identifies the Slide12Link entity attribute.</summary>
    public const string Slide12LinkField = "Slide12Link";
    /// <summary>Identifies the WidgetId entity attribute.</summary>
    public const string WidgetIdField = "WidgetId";


    #endregion
    
    #region Relationships

    [ReverseAssociation("SlideshowWidgetRevisions")]
    private readonly EntityHolder<Widget> _widget = new EntityHolder<Widget>();


    #endregion
    
    #region Properties

    [System.Diagnostics.DebuggerNonUserCode]
    public Widget Widget
    {
      get { return Get(_widget); }
      set { Set(_widget, value); }
    }


    [System.Diagnostics.DebuggerNonUserCode]
    public int RevisionNumber
    {
      get { return Get(ref _revisionNumber, "RevisionNumber"); }
      set { Set(ref _revisionNumber, value, "RevisionNumber"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide1ImageReference
    {
      get { return Get(ref _slide1ImageReference, "Slide1ImageReference"); }
      set { Set(ref _slide1ImageReference, value, "Slide1ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide1Title
    {
      get { return Get(ref _slide1Title, "Slide1Title"); }
      set { Set(ref _slide1Title, value, "Slide1Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide1Link
    {
      get { return Get(ref _slide1Link, "Slide1Link"); }
      set { Set(ref _slide1Link, value, "Slide1Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide2ImageReference
    {
      get { return Get(ref _slide2ImageReference, "Slide2ImageReference"); }
      set { Set(ref _slide2ImageReference, value, "Slide2ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide2Title
    {
      get { return Get(ref _slide2Title, "Slide2Title"); }
      set { Set(ref _slide2Title, value, "Slide2Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide2Link
    {
      get { return Get(ref _slide2Link, "Slide2Link"); }
      set { Set(ref _slide2Link, value, "Slide2Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide3ImageReference
    {
      get { return Get(ref _slide3ImageReference, "Slide3ImageReference"); }
      set { Set(ref _slide3ImageReference, value, "Slide3ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide3Title
    {
      get { return Get(ref _slide3Title, "Slide3Title"); }
      set { Set(ref _slide3Title, value, "Slide3Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide3Link
    {
      get { return Get(ref _slide3Link, "Slide3Link"); }
      set { Set(ref _slide3Link, value, "Slide3Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide4ImageReference
    {
      get { return Get(ref _slide4ImageReference, "Slide4ImageReference"); }
      set { Set(ref _slide4ImageReference, value, "Slide4ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide4Title
    {
      get { return Get(ref _slide4Title, "Slide4Title"); }
      set { Set(ref _slide4Title, value, "Slide4Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide4Link
    {
      get { return Get(ref _slide4Link, "Slide4Link"); }
      set { Set(ref _slide4Link, value, "Slide4Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide5ImageReference
    {
      get { return Get(ref _slide5ImageReference, "Slide5ImageReference"); }
      set { Set(ref _slide5ImageReference, value, "Slide5ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide5Title
    {
      get { return Get(ref _slide5Title, "Slide5Title"); }
      set { Set(ref _slide5Title, value, "Slide5Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide5Link
    {
      get { return Get(ref _slide5Link, "Slide5Link"); }
      set { Set(ref _slide5Link, value, "Slide5Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide6ImageReference
    {
      get { return Get(ref _slide6ImageReference, "Slide6ImageReference"); }
      set { Set(ref _slide6ImageReference, value, "Slide6ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide6Title
    {
      get { return Get(ref _slide6Title, "Slide6Title"); }
      set { Set(ref _slide6Title, value, "Slide6Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide6Link
    {
      get { return Get(ref _slide6Link, "Slide6Link"); }
      set { Set(ref _slide6Link, value, "Slide6Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide7ImageReference
    {
      get { return Get(ref _slide7ImageReference, "Slide7ImageReference"); }
      set { Set(ref _slide7ImageReference, value, "Slide7ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide7Title
    {
      get { return Get(ref _slide7Title, "Slide7Title"); }
      set { Set(ref _slide7Title, value, "Slide7Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide7Link
    {
      get { return Get(ref _slide7Link, "Slide7Link"); }
      set { Set(ref _slide7Link, value, "Slide7Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide8ImageReference
    {
      get { return Get(ref _slide8ImageReference, "Slide8ImageReference"); }
      set { Set(ref _slide8ImageReference, value, "Slide8ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide8Title
    {
      get { return Get(ref _slide8Title, "Slide8Title"); }
      set { Set(ref _slide8Title, value, "Slide8Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide8Link
    {
      get { return Get(ref _slide8Link, "Slide8Link"); }
      set { Set(ref _slide8Link, value, "Slide8Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide9ImageReference
    {
      get { return Get(ref _slide9ImageReference, "Slide9ImageReference"); }
      set { Set(ref _slide9ImageReference, value, "Slide9ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide9Title
    {
      get { return Get(ref _slide9Title, "Slide9Title"); }
      set { Set(ref _slide9Title, value, "Slide9Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide9Link
    {
      get { return Get(ref _slide9Link, "Slide9Link"); }
      set { Set(ref _slide9Link, value, "Slide9Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide10ImageReference
    {
      get { return Get(ref _slide10ImageReference, "Slide10ImageReference"); }
      set { Set(ref _slide10ImageReference, value, "Slide10ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide10Title
    {
      get { return Get(ref _slide10Title, "Slide10Title"); }
      set { Set(ref _slide10Title, value, "Slide10Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide10Link
    {
      get { return Get(ref _slide10Link, "Slide10Link"); }
      set { Set(ref _slide10Link, value, "Slide10Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide11ImageReference
    {
      get { return Get(ref _slide11ImageReference, "Slide11ImageReference"); }
      set { Set(ref _slide11ImageReference, value, "Slide11ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide11Title
    {
      get { return Get(ref _slide11Title, "Slide11Title"); }
      set { Set(ref _slide11Title, value, "Slide11Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide11Link
    {
      get { return Get(ref _slide11Link, "Slide11Link"); }
      set { Set(ref _slide11Link, value, "Slide11Link"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide12ImageReference
    {
      get { return Get(ref _slide12ImageReference, "Slide12ImageReference"); }
      set { Set(ref _slide12ImageReference, value, "Slide12ImageReference"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide12Title
    {
      get { return Get(ref _slide12Title, "Slide12Title"); }
      set { Set(ref _slide12Title, value, "Slide12Title"); }
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public string Slide12Link
    {
      get { return Get(ref _slide12Link, "Slide12Link"); }
      set { Set(ref _slide12Link, value, "Slide12Link"); }
    }

    /// <summary>Gets or sets the ID for the <see cref="Widget" /> property.</summary>
    [System.Diagnostics.DebuggerNonUserCode]
    public int WidgetId
    {
      get { return Get(ref _widgetId, "WidgetId"); }
      set { Set(ref _widgetId, value, "WidgetId"); }
    }

    #endregion
  }




  /// <summary>
  /// Provides a strong-typed unit of work for working with the Portal model.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")]
  public partial class PortalUnitOfWork : Mindscape.LightSpeed.UnitOfWork
  {

    public System.Linq.IQueryable<MgrOption> MgrOptions
    {
      get { return this.Query<MgrOption>(); }
    }
    
    public System.Linq.IQueryable<Location> Locations
    {
      get { return this.Query<Location>(); }
    }
    
    public System.Linq.IQueryable<Website> Websites
    {
      get { return this.Query<Website>(); }
    }
    
    public System.Linq.IQueryable<UserType> UserTypes
    {
      get { return this.Query<UserType>(); }
    }
    
    public System.Linq.IQueryable<Account> Accounts
    {
      get { return this.Query<Account>(); }
    }
    
    public System.Linq.IQueryable<AccountsLocation> AccountsLocations
    {
      get { return this.Query<AccountsLocation>(); }
    }
    
    public System.Linq.IQueryable<User> Users
    {
      get { return this.Query<User>(); }
    }
    
    public System.Linq.IQueryable<AspLProvince> AspLProvinces
    {
      get { return this.Query<AspLProvince>(); }
    }
    
    public System.Linq.IQueryable<AspLCaPostalCode> AspLCaPostalCodes
    {
      get { return this.Query<AspLCaPostalCode>(); }
    }
    
    public System.Linq.IQueryable<AspLState> AspLStates
    {
      get { return this.Query<AspLState>(); }
    }
    
    public System.Linq.IQueryable<AspLLocation> AspLLocations
    {
      get { return this.Query<AspLLocation>(); }
    }
    
    public System.Linq.IQueryable<LocationHour> LocationHours
    {
      get { return this.Query<LocationHour>(); }
    }
    
    public System.Linq.IQueryable<AspLZip> AspLZips
    {
      get { return this.Query<AspLZip>(); }
    }
    
    public System.Linq.IQueryable<Franchise> Franchises
    {
      get { return this.Query<Franchise>(); }
    }
    
    public System.Linq.IQueryable<PageRegion> PageRegions
    {
      get { return this.Query<PageRegion>(); }
    }
    
    public System.Linq.IQueryable<PageType> PageTypes
    {
      get { return this.Query<PageType>(); }
    }
    
    public System.Linq.IQueryable<PageRevision> PageRevisions
    {
      get { return this.Query<PageRevision>(); }
    }
    
    public System.Linq.IQueryable<Page> Pages
    {
      get { return this.Query<Page>(); }
    }
    
    public System.Linq.IQueryable<TextWidgetRevision> TextWidgetRevisions
    {
      get { return this.Query<TextWidgetRevision>(); }
    }
    
    public System.Linq.IQueryable<TextWidgetRevisionsBackup> TextWidgetRevisionsBackups
    {
      get { return this.Query<TextWidgetRevisionsBackup>(); }
    }
    
    public System.Linq.IQueryable<WidgetOption> WidgetOptions
    {
      get { return this.Query<WidgetOption>(); }
    }
    
    public System.Linq.IQueryable<WidgetType> WidgetTypes
    {
      get { return this.Query<WidgetType>(); }
    }
    
    public System.Linq.IQueryable<WidgetRevision> WidgetRevisions
    {
      get { return this.Query<WidgetRevision>(); }
    }
    
    public System.Linq.IQueryable<Widget> Widgets
    {
      get { return this.Query<Widget>(); }
    }
    
    public System.Linq.IQueryable<EmbedWidgetRevision> EmbedWidgetRevisions
    {
      get { return this.Query<EmbedWidgetRevision>(); }
    }
    
    public System.Linq.IQueryable<JobWidgetRevision> JobWidgetRevisions
    {
      get { return this.Query<JobWidgetRevision>(); }
    }
    
    public System.Linq.IQueryable<SpecialOfferWidgetRevision> SpecialOfferWidgetRevisions
    {
      get { return this.Query<SpecialOfferWidgetRevision>(); }
    }
    
    public System.Linq.IQueryable<PiasOption> PiasOptions
    {
      get { return this.Query<PiasOption>(); }
    }
    
    public System.Linq.IQueryable<Domain> Domains
    {
      get { return this.Query<Domain>(); }
    }
    
    public System.Linq.IQueryable<AccountStatus> AccountStatuses
    {
      get { return this.Query<AccountStatus>(); }
    }
    
    public System.Linq.IQueryable<SellYourGearSubmission> SellYourGearSubmissions
    {
      get { return this.Query<SellYourGearSubmission>(); }
    }
    
    public System.Linq.IQueryable<SlideshowWidgetRevision> SlideshowWidgetRevisions
    {
      get { return this.Query<SlideshowWidgetRevision>(); }
    }
    
  }

}
