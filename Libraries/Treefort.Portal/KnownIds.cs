﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treefort.Portal
{
	public static class KnownIds
	{
		public static class Website
		{
			public const int Corporate = 0;
		}

        public static class RemoteControl
        {
            public static Guid MGR = new Guid("97f57374-1889-4354-814b-67390069c0f6");
        }

        public static class Franchise
        {
            public const int MGR = 1;
        }

		public static class GoogleAnalytics
		{
			public const string Corporate = "UA-48722027-1";
			public const string Rollup = "UA-41350657-1";
			public const string PureDriven = "UA-961717-27";
		}

        public static class Category
        {
            public const int Guitars = 19;
            public const int Percussion = 20;
            public const int Band = 21;
            public const int Keyboard = 22;
            public const int ProSound = 23;
            public const int Accessories = 24;
            public const int AccessoriesBandInstruments = 25;
            public const int AccessoriesDrumHeads = 26;
            public const int AccessoriesGuitarStrings = 27;
            public const int AccessoriesGuitars = 28;
            public const int AccessoriesKeyboards = 29;
            public const int AccessoriesOther = 30;
            public const int AccessoriesPercussion = 31;
            public const int AccessoriesProSound = 32;
            public const int MusicBooksVideos = 33;
            public const int AccessoriesDrumSticksMallets = 82;
            public const int AccessoriesReeds = 83;
        }

		public static class TaxCategory
		{
			public const int AllTaxable = 6;
		}

		public static class Country
		{
			public const int US = 1;
		}

        public static class AccountStatus
        {
            public const int Active = 1;
            public const int PaymentReview = 2;
            public const int Closed = 3;
            public const int PendingClosed = 4;
            public const int Expired = 5;
        }
	}
}
