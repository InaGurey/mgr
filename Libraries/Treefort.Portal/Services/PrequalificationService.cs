﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using Treefort.Portal.Queries.Own;

namespace Treefort.Portal.Services
{
    public interface IPrequalificationService
    {
        void WritePrequalificationFormToFile(PrequalificationServiceViewModel request, string directory);
        void SendPrequalificationFormEmail(PrequalificationServiceViewModel request);
    }

    public class PrequalificationService : IPrequalificationService
    {
        public void WritePrequalificationFormToFile(PrequalificationServiceViewModel request, string directory)
        {
			Func<string,string> cleanFilename = f => new string(f.Where(Char.IsLetterOrDigit).ToArray());
            var filenameTemplate = "{0:yyyy-MM-dd-HHmmss}-{1}-{2}.htm";
			var fullFilePath = System.IO.Path.Combine(directory,
				string.Format(filenameTemplate,
					DateTime.Now,
					cleanFilename(request.FirstName),
					cleanFilename(request.LastName)));

            //checking to see if Directory Exists...if not create it
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            System.IO.File.WriteAllText(fullFilePath, CreateQualificationRequestBody(request));
        }

        public void SendPrequalificationFormEmail(PrequalificationServiceViewModel request)
        {
            var mm = new MailMessage();
            mm.From = new MailAddress("info@musicgoround.com", "Music Go Round");
            mm.Subject = "Music Go Round Prequalification Form";
            mm.To.Add(new MailAddress("mgr-franchise-development@musicgoround.com"));

            mm.Body = CreateQualificationRequestBody(request);
            mm.IsBodyHtml = true;

            var smtp = new SmtpClient();
            smtp.Send(mm);
        }

        private string CreateQualificationRequestBody(PrequalificationServiceViewModel request)
        {
            var sb = new StringBuilder();
            sb.Append(request.MeetFinancials ? "Yes" : "No");
            sb.Append(" I meet the financial requirements of the Music Go Round franchise, either individually or with a financial partner: minimum of $350,000 net worth and $105,000 in cash or liquid assets. <br />");

            sb.Append(request.HasPartner ? "Yes" : "No");
            sb.Append(" If no, Do you have a potential business partner? <br />");

            sb.AppendFormat("{0} {1}<br/>", request.FirstName, request.LastName);
            sb.AppendFormat("{0}<br/>", request.StreetAddress);
            sb.AppendFormat("{0} {1} {2}<br/>", request.City, request.StateProvince, request.Postal);
            sb.AppendFormat("{0}<br/>", request.Country);
            sb.AppendFormat("Day Phone: {0}<br/>", request.DayPhone);
            sb.AppendFormat("Email: {0}<br/>", request.Email);
            sb.AppendFormat("Locations of interest for your Music Go Round Store: {0}<br/>", request.LocationOfInterest);
            sb.AppendFormat("How did you hear about Music Go Round?: {0}<br/>", request.HowTheyHeard);
            sb.AppendFormat("Comments: {0}<br/>", (request.Comments ?? string.Empty).Replace("\n", "<br/>"));

            return sb.ToString();
        }
    }
}