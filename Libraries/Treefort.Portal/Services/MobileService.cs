﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Treefort.Common;


namespace Treefort.Portal.Services
{
    public class MobileBools
    {
        public bool DontUseMobileVersion { get; set; }
        public bool ForceMobile { get; set; }
        public bool IgnoreMobile { get; set; }
        public bool IsMobileBrowser { get; set; }
        public bool IsMobile { get; set; }
        public string UA { get; set; }
        public string IP { get; set; }
        public string Guid { get; set; }
    }

    public static class MobileService
    {
        public static bool IsMobile(HttpRequest context, bool dontUseMobileVersion = false)
        {
            var ignoreMobileKey = "ignoreMobile";
            var ignoreMobileValue = "1";

            var forceMobileKey = "forceMobile";
            var forceMobile = context.QueryString[forceMobileKey].IfNotNullOrEmpty(s => s == "1" || s.ToLower() == "true", false);

            var ignoreMobile = !string.IsNullOrEmpty(context.QueryString[ignoreMobileKey]);


            if (!ignoreMobile)
            {
                // OK, ignoreMobile param is not in the querystring, 
                // try to find it in a cookie

                var cookie = context.Cookies[ignoreMobileKey];
                ignoreMobile = cookie == null
                    ? false
                    : cookie.Value == ignoreMobileValue;
            }
            else
            {
                // Since we found 'ignoreMobile' in the querystring
                // we will add a cookie version for subsequent requests.
                SetIgnoreMobile(new HttpContextWrapper(HttpContext.Current));
            }
            return !dontUseMobileVersion && (forceMobile || (!ignoreMobile && IsMobileBrowser(context.ServerVariables["HTTP_USER_AGENT"])));
        }

        public static MobileBools GetIsMobile(HttpRequest context, bool dontUseMobileVersion = false)
        {
            var ignoreMobileKey = "ignoreMobile";
            var ignoreMobileValue = "1";

            var forceMobileKey = "forceMobile";
            var forceMobile = context.QueryString[forceMobileKey].IfNotNullOrEmpty(s => s == "1" || s.ToLower() == "true", false);

            var ignoreMobile = !string.IsNullOrEmpty(context.QueryString[ignoreMobileKey]);


            if (!ignoreMobile)
            {
                // OK, ignoreMobile param is not in the querystring, 
                // try to find it in a cookie

                var cookie = context.Cookies[ignoreMobileKey];
                ignoreMobile = cookie == null
                    ? false
                    : cookie.Value == ignoreMobileValue;
            }
            else
            {
                // Since we found 'ignoreMobile' in the querystring
                // we will add a cookie version for subsequent requests.
                SetIgnoreMobile(new HttpContextWrapper(HttpContext.Current));
            }
            return new MobileBools
            {
                DontUseMobileVersion = dontUseMobileVersion,
                ForceMobile = forceMobile,
                IgnoreMobile = ignoreMobile,
                IsMobileBrowser = IsMobileBrowser(context.ServerVariables["HTTP_USER_AGENT"]),
                IsMobile = !dontUseMobileVersion && (forceMobile || (!ignoreMobile && IsMobileBrowser(context.ServerVariables["HTTP_USER_AGENT"]))),
                UA = context.ServerVariables["HTTP_USER_AGENT"],
                IP = GetIP(context)
            };
            
        }

        public static string GetIP(HttpRequest context)
        {
            String ip =
                context.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = context.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }

        public static void SetIgnoreMobile(HttpContextBase httpContext, int durationSeconds = 3600)
        {
            var ignoreMobileKey = "ignoreMobile";
            var ignoreCookie = httpContext.Request.Cookies[ignoreMobileKey] ?? new HttpCookie(ignoreMobileKey);

            ignoreCookie.Value = "1";
            ignoreCookie.Expires = DateTime.Now.AddSeconds(durationSeconds);
            httpContext.Response.Cookies.Set(ignoreCookie);
        }

        public static bool IsMobileBrowser(string u)
        {
            if (u.IsNullOrEmpty())
                return false;

            Regex b = new Regex(@"android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            //var googleBotUserAgentString =
            //    "Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12F70 Safari/600.1.4 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
            return b.IsMatch(u) || v.IsMatch(u.Substring(0, 4));
        }
    }

    public class ForceDesktop : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (MobileService.IsMobileBrowser(filterContext.HttpContext.Request.ServerVariables["HTTP_USER_AGENT"]))
            {
                MobileService.SetIgnoreMobile(filterContext.HttpContext);
            }

            this.OnActionExecuting(filterContext);
        }
    }

    public class MobileOnly : ActionFilterAttribute, IActionFilter
    {
        private string _action = "Index";
        private string _controller = "Home";

        public MobileOnly() { }
        public MobileOnly(string action, string controller)
        {
            _action = action;
            _controller = controller;
        }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!MobileService.IsMobile(HttpContext.Current.Request))
            {
                filterContext.Result =
                    new RedirectToRouteResult(new RouteValueDictionary(new {controller = _controller, action = _action}));

                filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
            }
            else
            {
                this.OnActionExecuting(filterContext);
            }
        }
    }

    public static class UrlExtensions
    {
        public static string GoogleMap(this UrlHelper helper, params string[] parts)
        {
            return string.Format("http://maps.google.com/?q={0}", HttpUtility.UrlEncode(string.Join(" ", parts).Trim()));
        }
    }
}
