﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using Mindscape.LightSpeed;
using Treefort.Common;
using Treefort.Portal;
using Treefort.Portal.Queries.Geo;
using Treefort.Portal.Queries.Locations;
using WinmarkFranchise.Domain;
using WinmarkFranchise.Domain.Geo;
using Coordinate = Treefort.Portal.Queries.Geo.Coordinate;


namespace Treefort.Portal.Services
{
    public interface ILocationService
    {
        Coordinate GetLatLngByIpAddress(string ip);
        GeoLocationModel GetLocationByIpAddress(string ip);
        GenericLocation GetLocationContactInfoBySiteId(int id);
        LocationModel GetSingleLocation(int id);
        IList<LocationModel> GetLocationsByCoordinates(double? lat, double? lng, int maxResults, int? maxDistance);
		IList<LocationModel> GetLocationsByPostalCode(string postalCode, int maxResults, int? maxDistance);
		IList<LocationModel> GetLocationsByRegion(string region);
        IList<LocationModel> GetAllLocations();
        LocationModel GetLocationByStoreNumber(string storeNumber);
        IList<StateInfo> GetFullNameOfStates(List<string> stateAbbr);
        IList<ConstantContactLocation> GetLocationsWithConstantContact();

        string GetCountryCodeByIpAddress(string ip);
    }

    public class LocationService : ILocationService
    {
        private readonly LightSpeedContext<PortalUnitOfWork> _portal;
        private readonly IWinmarkFranchiseUnitOfWork _uow;
        private readonly IGeoService _geo;

		public LocationService(LightSpeedContext<PortalUnitOfWork> portal, IGeoService geo, IWinmarkFranchiseUnitOfWork uow) {
			_portal = portal;
		    _geo = geo;
		    _uow = uow;
		}
		
        public Coordinate GetLatLngByIpAddress(string ip)
        {
            var location = GetLocationByIpAddress(ip);
            if (location != null)
            {
                if (location.HasCoordinateData)
                {
                    return new Coordinate(location.Latitude, location.Longitude);
                }
            }
            return new Coordinate(0, 0);
        }



        public GeoLocationModel GetLocationByIpAddress(string ip)
        {
            if (ip.IsNullOrEmpty())
                return new GeoLocationModel();

            var location = _geo.GetIpAddressLocation(ip);

            if (location != null)
            {
                return new GeoLocationModel
                {
                    City = location.City,
                    CountryCode = location.CountryCode,
                    Latitude = location.Latitude,
                    Longitude = location.Longitude,
                    PostalCode = location.PostalCode,
                    Region = location.RegionAbbreviation,
                };
            }

            return new GeoLocationModel();
        }

     public GenericLocation GetLocationContactInfoBySiteId(int siteId) {
			using (var uow = _portal.CreateUnitOfWork()) {
				var website = uow.FindById<Website>(siteId);
				if (website == null)
					return null;

				var account = website.Account;
				if (account == null)
					return null;

				var location = account.PrimaryLocation;
				if (location == null)
					return null;

			    return new GenericLocation()
			        {
                        Id = location.Id,
                        Name = location.Name,
			            Address1 = location.AddressLine1,
			            Address2 = location.AddressLine2,
                        City = location.City,
			            ConstactContactUrl = location.ConstantContactUrl,
                        FacebookUrl = location.FacebookUrl,
			            Email = location.EmailAddress,
                        Phone = location.PhoneNumber,
                        Region = location.Region,
                        PostalCode = location.PostalCode,
                        Website = location.WebsiteUrl
			        };
			}
		}

        public IList<LocationModel> GetLocationsByCoordinates (double? lat, double? lng, int maxResults, int? maxDistance) {
	        var data = (
		        from location in GetAllLocations()
		        let distance = calculateDistance(lat, lng, location.Latitude, location.Longitude)
		        where (maxDistance == null || distance < maxDistance)
				orderby distance
		        select new { location, distance }).Take(maxResults).ToList();

			data.ForEach(d => d.location.Distance = d.distance);
	        return data.Select(d => d.location).ToList();
        }

        public IList<LocationModel> GetLocationsByPostalCode(string postalCode, int maxResults, int? maxDistance)
        {
            if (postalCode.IsNullOrEmpty())
                return new List<LocationModel>();

            //For US Zipcodes since they will be numeric
            int pCode;
            if (Int32.TryParse(postalCode, out pCode)) {
                using (var uow = _portal.CreateUnitOfWork()) {
                    var z = uow.AspLZips.FirstOrDefault(x => x.Id == pCode);
                    if(z != null && z.ZipLat.HasValue && z.ZipLon.HasValue)
                        return GetLocationsByCoordinates(z.ZipLat.Value, z.ZipLon.Value, maxResults, maxDistance);
                }
            }
            else if (Regex.IsMatch(postalCode, @"\d")) {
                double? lat = 0, lng = 0;
                postalCode = postalCode.Replace("%20", " ");

                //checking to see if they forgot to enter a space...pretty much only for Americans who enter Canadian zipcodes...
                if (postalCode.Length == 6)
                    postalCode = postalCode.Substring(0, 3) + " " + postalCode.Substring(3, 3); // adding space

                using (var uow = _portal.CreateUnitOfWork()) {
                    var z = uow.AspLCaPostalCodes.FirstOrDefault(x => x.PostalCode == postalCode);
                    if (z != null && z.Latitude.HasValue && z.Longitude.HasValue)
                        return GetLocationsByCoordinates(z.Latitude.Value, z.Longitude.Value, maxResults, maxDistance);
                }
            }

            return new List<LocationModel>();
        }

        public IList<LocationModel> GetLocationsByRegion(string region)
        {
            using (var uow = _portal.CreateUnitOfWork()){

                //return nothing if empty
                if (string.IsNullOrEmpty(region))
                {
                    return new List<Queries.Geo.LocationModel>();
                }


                //for full names
                if(region.Length > 2)
                {
                    var obj = uow.AspLStates.FirstOrDefault(o => o.StateName == region);

                    if(obj != null)
                        region = obj.StateAbb;

                }

                //Check Canada if it can't find us
                if(region.Length > 2 )
                {
                    var obj = uow.AspLProvinces.FirstOrDefault(o => o.ProvName == region);

                    if (obj != null)
                        region = obj.ProvAbb;
                }


                var results = (from l in uow.Locations
                               where l.Region == region && l.FranchiseId == KnownIds.Franchise.MGR && l.IsActive
                               orderby l.City
                               select new LocationModel()
                                          {
                                              Id = l.Id,
                                              StoreNumber = l.StoreNumber,
                                              FranchiseId = l.FranchiseId,
                                              FranchiseeName = l.FranchiseeName,
                                              IsActive = l.IsActive,
                                              Name = l.Name,
                                              AddressLine1 = l.AddressLine1,
                                              AddressLine2 = l.AddressLine2,
                                              City = l.City,
                                              Region = l.Region,
                                              PostalCode = l.PostalCode,
                                              Country = l.Country,
                                              PhoneNumber = l.PhoneNumber,
                                              FaxNumber = l.FaxNumber,
                                              Directions = l.Directions,
                                              EmailAddress = l.EmailAddress,
                                              WebsiteUrl = l.WebsiteUrl,
                                              ConstantContactUrl = l.ConstantContactUrl,
                                              FacebookUrl = l.FacebookUrl,
                                              TwitterUrl = l.TwitterUrl,
                                              InstagramUrl = l.InstagramUrl,
                                              MyspaceUrl = l.MyspaceUrl,
                                              IsGold = l.IsGold,
                                              HasGiftCards = l.HasGiftCards,
                                              Latitude = l.Latitude,
                                              Longitude = l.Longitude,
                                              ImageFileName = l.ImageReference,
                                              MapUrl = GoogleMap("Music Go Round", l.Name, l.AddressLine1, l.City, l.Region, l.PostalCode),
                                              IsComingSoon = l.IsComingSoon
                                          }).ToList();

                if(results.Any())
                    return results;

            }

            return new List<LocationModel>();
        }

        public LocationModel GetLocationByStoreNumber(string storeNumber)
        {
            using (var uow = _portal.CreateUnitOfWork()) {
                return uow.Locations
                          .Where(l => l.FranchiseId == KnownIds.Franchise.MGR && l.IsActive)
                          .Where(l => l.StoreNumber == storeNumber)
                          .Select(projectLocationModel)
                          .FirstOrDefault();
            } 
        }

        public IList<LocationModel> GetAllLocations()
        {
            using (var uow = _portal.CreateUnitOfWork()) {
                return uow.Locations
                          .Where(l => l.FranchiseId == KnownIds.Franchise.MGR && l.IsActive)
                          .OrderBy(l => l.Region)
                          .ThenBy(l => l.City)
                          .Select(projectLocationModel)
                          .ToList();
            }
        }

        private static readonly Expression<Func<Location, LocationModel>> projectLocationModel = l =>
                    new LocationModel() {
                        Id = l.Id,
                        StoreNumber = l.StoreNumber,
                        FranchiseId = l.FranchiseId,
                        FranchiseeName = l.FranchiseeName,
                        IsActive = l.IsActive,
                        Name = l.Name,
                        AddressLine1 = l.AddressLine1,
                        AddressLine2 = l.AddressLine2,
                        City = l.City,
                        Region = l.Region,
                        PostalCode = l.PostalCode,
                        Country = l.Country,
                        PhoneNumber = l.PhoneNumber,
                        FaxNumber = l.FaxNumber,
                        Directions = l.Directions,
                        EmailAddress = l.EmailAddress,
                        WebsiteUrl = l.WebsiteUrl,
                        ConstantContactUrl = l.ConstantContactUrl,
                        FacebookUrl = l.FacebookUrl,
                        TwitterUrl = l.TwitterUrl,
                        InstagramUrl = l.InstagramUrl,
                        MyspaceUrl = l.MyspaceUrl,
                        IsGold = l.IsGold,
                        HasGiftCards = l.HasGiftCards,
                        Latitude = l.Latitude,
                        Longitude = l.Longitude,
                        ImageFileName = l.ImageReference,
                        MapUrl = GoogleMap("Music Go Round", l.Name, l.AddressLine1, l.City, l.Region, l.PostalCode),
                        IsComingSoon = l.IsComingSoon
                    };

        public IList<StateInfo> GetFullNameOfStates(List<string> stateAbbr)
        {
            using (var uow = _portal.CreateUnitOfWork())
            {
                var stateFull = uow.AspLStates.ToList();

                return (
                    from abbr in stateAbbr 
                    select stateFull.FirstOrDefault(x => x.StateAbb == abbr) 
                    into s where s != null 
                    select new StateInfo(){StateAbbr = s.StateAbb, StateFullName = s.StateName}).ToList();
            }

        }

        public LocationModel GetSingleLocation(int id)
        {
            var l = _uow.Locations.GetById(id);

            //How would that Error controller handle this error?
            if (l == null)
                throw new DataException("No Location Found");

            var model =
                new LocationModel()
                {
                    Id = l.Id,
                    StoreNumber = l.StoreNumber,
                    FranchiseId = l.FranchiseId,
                    FranchiseeName = l.FranchiseeName,
                    IsActive = l.IsActive,
                    Name = l.Name,
                    AddressLine1 = l.Address.Line1,
                    AddressLine2 = l.Address.Line2,
                    City = l.Address.City,
                    Region = l.Address.Region,
                    PostalCode = l.Address.PostalCode,
                    Country = l.Address.Country,
                    PhoneNumber = l.PhoneNumber,
                    FaxNumber = l.FaxNumber,
                    Directions = l.Directions,
                    EmailAddress = l.EmailAddress,
                    WebsiteUrl = l.WebsiteUrl,
                    ConstantContactUrl = l.ConstantContactUrl,
                    FacebookUrl = l.FacebookUrl,
                    TwitterUrl = l.TwitterUrl,
                    InstagramUrl = l.InstagramUrl,
                    MyspaceUrl = l.MyspaceUrl,
                    FourSquareUrl = l.FoursquareUrl,
                    YoutubeUrl = l.YouTubeUrl,
                    IsGold = l.IsGold,
                    HasGiftCards = l.HasGiftCards,
                    Latitude = l.GeographicCoordinates.HasValue ? l.GeographicCoordinates.Value.Latitude : 0,
                    Longitude = l.GeographicCoordinates.HasValue ? l.GeographicCoordinates.Value.Longitude : 0,
                    ImageFileName = l.ImageReference,
                    MapUrl = GoogleMap("Music Go Round", l.Name, l.Address.Line1, l.Address.City, l.Address.Region, l.Address.PostalCode),
                    IsComingSoon = l.IsComingSoon
                };

            return model;
        }

        public string GetCountryCodeByIpAddress(string ip)
        {
            return _geo.GetIpAddressLocation(ip).IfNotNull(l => l.CountryCode);
        }

        private double? calculateDistance(double? lat1, double? lon1, double? lat2, double? lon2)
        {
            if(lat2.HasValue && lon2.HasValue && lat1.HasValue && lon1.HasValue)
            {

            var R = 3959; // miles
            var dLat = Math.PI *  (lat2.Value - lat1.Value) / 180.0;
            
            var dLon = Math.PI * (lon2.Value - lon1.Value) / 180.0;
            lat1 = Math.PI * lat1 / 180.0;
            lat2 = Math.PI * lat2 / 180.0;

            
            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1.Value) * Math.Cos(lat2.Value);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c;

            return d;
            }
            else
            {
                return null;
            }
        }

        private static string GoogleMap(params string[] parts)
        {
            return string.Format("http://maps.google.com/?q={0}", HttpUtility.UrlEncode(string.Join(" ", parts).Trim()));
        }



        public IList<ConstantContactLocation> GetLocationsWithConstantContact()
        {
            var franchise = _uow.Franchises.GetSpecific(Franchises.MusicGoRound);
            return (from l in _uow.Locations.GetByFranchise(franchise)
                    where l.IsActive && !string.IsNullOrEmpty(l.ConstantContactUrl)
                    orderby l.Address.Region, l.Address.City
                    select new ConstantContactLocation()
                    {
                        Name = l.Name,
                        Region = l.Address.Region,
                        ConstantContactUrl = l.ConstantContactUrl
                    }).ToList();
        }
    }
}