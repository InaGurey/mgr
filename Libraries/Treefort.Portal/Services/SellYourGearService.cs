﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Logging;
using SmartFormat;
using Treefort.Portal.Queries.Franchisee.Website;



namespace Treefort.Portal.Services
{
	public interface ISellYourGearService
	{
		void Submit(SellYourGearSubmissionRequest submission);
	}

	public class SellYourGearSubmissionRequest
	{
		public int LocationId { get; set; }
		public string CustomerName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public string ContactPreference { get; set; }
		public string City { get; set; }
		public string Region { get; set; }
		public string PostalCode { get; set; }
		public string GearType { get; set; }
		public string MakeAndModel { get; set; }
		public string Color { get; set; }
		public string Year { get; set; }
		public string Condition { get; set; }
		public string Describe { get; set; }
		public string Modifications { get; set; }
		public UploadedPicture Picture1 { get; set; }
		public UploadedPicture Picture2 { get; set; }
		public UploadedPicture Picture3 { get; set; }
		public UploadedPicture Picture4 { get; set; }

		public class UploadedPicture
		{
			public byte[] Bytes { get; set; }
			public string MimeType { get; set; }
		}
	}


	public class SellYourGearService : ISellYourGearService
	{
        private readonly LightSpeedContext<PortalUnitOfWork> _portal;
		private readonly IWebsiteOptionsQuery _websiteOptionsQuery;
		private readonly ILocationService _locationService;

		public SellYourGearService(
				LightSpeedContext<PortalUnitOfWork> portal,
				IWebsiteOptionsQuery websiteOptionsQuery,
				ILocationService locationService
			)
		{
			_portal = portal;
			_websiteOptionsQuery = websiteOptionsQuery;
			_locationService = locationService;
		}

		public void Submit(SellYourGearSubmissionRequest submission)
		{
			string userDirectory;
			int? websiteId = GetWebsiteId(submission.LocationId, out userDirectory);
			if (websiteId == null) { return; }

		    SellYourGearSubmission syg = null;
			using (var uow = _portal.CreateUnitOfWork()) {
				syg = new Portal.SellYourGearSubmission {
					WebsiteId = websiteId.Value,
					CustomerName = submission.CustomerName,
					EmailAddress = submission.EmailAddress,
					PhoneNumber = submission.PhoneNumber,
					ContactPreference = submission.ContactPreference,
					City = submission.City,
					Region = submission.Region,
					PostalCode = submission.PostalCode,
					GearType = submission.GearType,
					MakeAndModel = submission.MakeAndModel,
					Color = submission.Color,
					Year = submission.Year,
					Condition = submission.Condition,
					Describe = submission.Describe,
					Modifications = submission.Modifications,
					SubmittedOn = DateTime.Now
				};

				uow.Add(syg);
				uow.SaveChanges();

				syg.ImageReference1 = SaveFile(submission.Picture1, userDirectory, syg.Id, 1);
				syg.ImageReference2 = SaveFile(submission.Picture2, userDirectory, syg.Id, 2);
				syg.ImageReference3 = SaveFile(submission.Picture3, userDirectory, syg.Id, 3);
				syg.ImageReference4 = SaveFile(submission.Picture4, userDirectory, syg.Id, 4);
				uow.SaveChanges();
			}

		    var options = _websiteOptionsQuery.Execute(websiteId.Value);
		    var recipient = options == null ? null : options.SellYourGearRecipient;
		    SendEmail(syg, recipient);
		}


		private string SaveFile(SellYourGearSubmissionRequest.UploadedPicture picture, string userDirectory, int submissionId, int num)
		{
			if (picture == null) { return null; }

            var usersParentDirectory = ConfigurationManager.AppSettings["RemoteControl.RelatedFilesPhysicalPath"];
			var subPart = string.Format(@"{0}\images\syg\{1}-{2}{3}", userDirectory, submissionId, num, GetFileExtensionFromMimeType(picture.MimeType));
			var savePath = usersParentDirectory + subPart;
			var saveDir = Path.GetDirectoryName(savePath);
			if (!Directory.Exists(saveDir)) {
				Directory.CreateDirectory(saveDir);
			}
			
			File.WriteAllBytes(savePath, picture.Bytes);

            var urlPart = "/users/" + subPart.Replace('\\', '/');
			return urlPart;
		}

        protected virtual string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            string[] parts = mimeType.Split('/');
            string lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }
            return "." + lastPart;
        }


		private int? GetWebsiteId(int locationId, out string userDirectory)
		{
			userDirectory = null;
            
			using (var uow = _portal.CreateUnitOfWork()) {
			    var accounts = uow.Accounts.Where(a =>
			                    a.FranchiseId == KnownIds.Franchise.MGR && a.Website != null &&
			                    (a.StatusId == KnownIds.AccountStatus.Active || a.StatusId == KnownIds.AccountStatus.PaymentReview));

                var account = accounts.FirstOrDefault(a => a.PrimaryLocationId == locationId && a.PrimaryLocation.IsActive) ??
                                accounts.FirstOrDefault(a => a.Locations.Any(l => l.Id == locationId && l.IsActive));

                if (account == null) { return null; }

			    var website = account.Website;
			    userDirectory = website.UserDirectory;
			    return website.Id;
			}
		}

		private void SendEmail(SellYourGearSubmission submission, string recipient)
		{
            if (string.IsNullOrEmpty(recipient)) { return; }

            var imgTextBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(submission.ImageReference1)) {
                imgTextBuilder.Append("Image 1: http://www.musicgoround.com");
                imgTextBuilder.AppendLine(submission.ImageReference1);
            }

            if (!string.IsNullOrEmpty(submission.ImageReference2)) {
                imgTextBuilder.Append("Image 2: http://www.musicgoround.com");
                imgTextBuilder.AppendLine(submission.ImageReference2);
            }

            if (!string.IsNullOrEmpty(submission.ImageReference3)) {
                imgTextBuilder.Append("Image 3: http://www.musicgoround.com");
                imgTextBuilder.AppendLine(submission.ImageReference3);
            }

            if (!string.IsNullOrEmpty(submission.ImageReference4)) {
                imgTextBuilder.Append("Image 4: http://www.musicgoround.com");
                imgTextBuilder.AppendLine(submission.ImageReference4);
            }


			var bodyText = @"A new Sell Your Gear request has been submitted.

{EmailAddress:Reply to {}
|}
Customer Name: {CustomerName}
Email Address: {EmailAddress}
Phone Number: {PhoneNumber}
Location: {City}, {Region} {PostalCode}
Gear: {GearType}
Make: {MakeAndModel}
Condition: {Condition}
Description: {Describe}
".FormatSmart(submission) + imgTextBuilder;


            var imgTagBuilder = new StringBuilder();

            // checks if we had any images in the text body
            imgTagBuilder.AppendLine(imgTextBuilder.Length == 0 ? "No Images" : "Images:<br/><br/>");

            if (!string.IsNullOrEmpty(submission.ImageReference1)) {
                imgTagBuilder.Append("<img alt='Image 1' src='http://www.musicgoround.com");
                imgTagBuilder.Append(submission.ImageReference1);
                imgTagBuilder.AppendLine("' /><br/<br/>");
            }

            if (!string.IsNullOrEmpty(submission.ImageReference2)) {
                imgTagBuilder.Append("<img alt='Image 2' src='http://www.musicgoround.com");
                imgTagBuilder.Append(submission.ImageReference2);
                imgTagBuilder.AppendLine("' /><br/<br/>");
            }

            if (!string.IsNullOrEmpty(submission.ImageReference3)) {
                imgTagBuilder.Append("<img alt='Image 3' src='http://www.musicgoround.com");
                imgTagBuilder.Append(submission.ImageReference3);
                imgTagBuilder.AppendLine("' /><br/<br/>");
            }

            if (!string.IsNullOrEmpty(submission.ImageReference4)) {
                imgTagBuilder.Append("<img alt='Image 4' src='http://www.musicgoround.com");
                imgTagBuilder.Append(submission.ImageReference4);
                imgTagBuilder.AppendLine("' /><br/<br/>");
            }

            // note the double braces in <style>, which is escaping for string formatting later
            var bodyHtml =
@"<!DOCTYPE html>
<html>
<head>
	<style>
		table {{ border: none; }}
		table td {{ padding: 4px; }}
	</style>
</head>
<body>
	<p>A new Sell Your Gear request has been submitted.</p>
	{EmailAddress:<p><b>Reply to <a href='mailto:{}'>{}</a></b></p>|}
	<p>
		<table border='0'>
			<tr><td>Customer Name:</td><td>{CustomerName}</td></tr>
			<tr><td>Email Address:</td><td>{EmailAddress}</td></tr>
			<tr><td>Phone Number:</td><td>{PhoneNumber}</td></tr>
			<tr><td>Location:</td><td>{City}, {Region} {PostalCode}</td></tr>
			<tr><td>Gear:</td><td>{GearType}</td></tr>
			<tr><td>Make:</td><td>{MakeAndModel}</td></tr>
			<tr><td>Condition:</td><td>{Condition}</td></tr>
			<tr><td>Description:</td><td>{Describe}</td></tr>
		</table>
	</p>

	<p>".FormatSmart(submission) + imgTagBuilder + @"</p>
</body>
</html>
";

            var htmlView = AlternateView.CreateAlternateViewFromString(bodyHtml, new ContentType("text/html"));

            var sender = "noreply@musicgoround.com";
            var subject = "Sell Your Gear - " + submission.MakeAndModel + " from " + submission.CustomerName;

            var msg = new MailMessage();
            msg.From = new MailAddress(sender);

            msg.To.Add(recipient);
            msg.Subject = subject;
            msg.Body = bodyText;
            msg.AlternateViews.Add(htmlView);

			if (!string.IsNullOrEmpty(submission.EmailAddress)) {
				msg.ReplyToList.Add(submission.EmailAddress);
			}

			using (var client = new SmtpClient()) {
				client.Send(msg);
			}
		}
	}
}
