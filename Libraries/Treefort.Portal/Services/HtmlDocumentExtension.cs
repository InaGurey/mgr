﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Treefort.Portal.Services
{
    public static class HtmlDocumentExtension
    {
        public static Regex WhitespaceRegex = new Regex(@"\s+");

        public static string GetMobileVersion(this string raw)
        {
            var document = new HtmlDocument();
            document.LoadHtml(raw);

            document.RemoveAttributesFromElement(new[] { "width", "height" }, new[] { "img", "iframe", "object", "embed" });
            var imgFix = new Dictionary<string, string>()
                    {
                        {"max-width", "100%"},
                        {"height", "auto"}
                    };
            document.SetElementStyle(imgFix, new[] { "img" });

            return document.DocumentNode.OuterHtml;
        }

        public static string ConvertToSelector(this string[] tags)
        {
            return string.Join(" | ", tags.Select(t => string.Format("//{0}", t)));
        }

        public static void RemoveAttributesFromElement(this HtmlDocument document, string[] attributes, string[] tags)
        {
            RemoveAttributesFromElement(document, attributes, tags.ConvertToSelector());
        }

        public static void RemoveAttributesFromElement(this HtmlDocument document, string[] attributes, string selector)
        {
            var elements = document.DocumentNode.SelectNodes(selector);

            if (elements != null)
            {
                foreach (var el in elements)
                {
                    foreach (var at in attributes)
                    {
                        if (el.Attributes[at] != null)
                            el.Attributes[at].Remove();
                    }
                }
            }
        }

        public static void SetElementStyle(this HtmlDocument document, Dictionary<string, string> styles,
            string[] tags)
        {
            SetElementStyle(document, styles, tags.ConvertToSelector());
        }

        public static void SetElementStyle(this HtmlDocument document, Dictionary<string, string> styles, string selector)
        {
            var elements = document.DocumentNode.SelectNodes(selector);

            if (elements != null)
            {
                foreach (var el in elements)
                {
                    var styleAttribute = el.Attributes["style"];
                    string nstyle;

                    if (styleAttribute != null)
                    {
                        var col = styleAttribute.Value.ParseInlineStyle();
                        col.ModifyStyle(styles);
                        nstyle = col.ConvertToInlineStyle();

                        el.Attributes.Remove(styleAttribute);
                    }
                    else
                    {
                        nstyle = styles.ConvertToInlineStyle();
                    }

                    el.Attributes.Add("style", nstyle);
                }
            }
        }

        public static Dictionary<string, string> ParseInlineStyle(this string inlineStyle)
        {
            var col = new Dictionary<string, string>();

            inlineStyle = WhitespaceRegex.Replace(inlineStyle, "");

            foreach (var x in inlineStyle.Split(';'))
            {
                var i = x.IndexOf(':');
                if (i < 0) continue;

                var property = x.Substring(0, i);
                var value = x.Substring(i+1, x.Length - i - 1);

                col.Add(property, value);
            }

            return col;
        }

        public static string ConvertToInlineStyle(this Dictionary<string, string> collection)
        {
            var inlineStyle = string.Join("", collection.Select(x => string.Format("{0}:{1};", x.Key, x.Value)));

            return inlineStyle;
        }

        public static void ModifyStyle(this Dictionary<string, string> collection, Dictionary<string, string> styles)
        {
            foreach (var s in styles)
            {
                collection[s.Key] = s.Value;
            }
        }
    }

}
