﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mindscape.LightSpeed;
using Treefort.Portal.Services;

namespace Treefort.Portal
{
	public class PortalUser
	{
		public string UserName { get; set; }
		public bool IsSuperAdmin { get; set; }
		public bool IsCorporate { get; set; }
		public bool IsZee { get; set; }
        public bool IsTrustedUser { get; set; }
		public bool IsContentEditor { get; set; }
	}

	public interface IPortalQueries
	{
		PortalUser GetUser(string username, int siteId);
		Website GetWebsite(int id);
		Location GetLocationFromWebsiteId(int websiteId);
		MgrOption GetMgrOptions(int websiteId);
		int GetNearestWebsiteIdFromZip(string zip);
		int? GetSiteIdFromDomainName(string domainName);
		string GetDomainNameFromSiteId(int siteId);
	    bool IsCoop(int siteId);
	}

	public class PortalQueries : IPortalQueries
	{
        private readonly LightSpeedContext<PortalUnitOfWork> _ls;

		// like most services, this one's registered in per-request scope, so we can cache it for the duration of a web request
		private PortalUser _user;
		private readonly ILocationService _locationService;

		public PortalQueries(LightSpeedContext<PortalUnitOfWork> ls, ILocationService locationService) {
			_ls = ls;
			_locationService = locationService;
		}

        public bool IsCoop(int siteId)
        {
            bool? isCoop = null;
            
            using (var uow = _ls.CreateUnitOfWork()) {
                isCoop = uow.Websites.Where(w => w.Id == siteId).Select(w => w.Account.IsCoop).FirstOrDefault();
            }

            return isCoop ?? false;
        }

		public PortalUser GetUser(string username, int siteId) {
			if (_user != null && _user.UserName == username)
				return _user;

			using (var uow = _ls.CreateUnitOfWork()) {
				var user = uow.Users.SingleOrDefault(u => u.Name == username);
				if (user == null)
					return null;

				if (!CanAdminSite(user, siteId))
					throw new UnauthorizedAccessException("You do not have permission to manage commerce for this site.");

				var result = new PortalUser {
					UserName = user.Name,
					IsSuperAdmin = user.UserType.IsSystemAdministrator,
					IsCorporate = user.UserType.IsFranchiseAdministrator,
					IsZee = user.UserType.IsAccountAdministrator,
                    IsTrustedUser = user.UserType.IsTrustedUser,
					IsContentEditor = user.UserType.IsContentEditor
				};

				if (!(result.IsZee || result.IsCorporate || result.IsSuperAdmin || result.IsTrustedUser || result.IsContentEditor))
					throw new UnauthorizedAccessException("You do not have permission to manage commerce.");

				_user = result;
				return result;
			}
		}

		public Website GetWebsite(int id) {
			using (var uow = _ls.CreateUnitOfWork()) {
				return uow.FindById<Website>(id);
			}
		}

		public MgrOption GetMgrOptions(int websiteId) {
			using (var uow = _ls.CreateUnitOfWork()) {
				return uow.MgrOptions.FirstOrDefault(o => o.WebsiteId == websiteId);
			}			
		}

		public Location GetLocationFromWebsiteId(int websiteId) {
			if (websiteId == 0) return null; // 0 indicates corporate

			using (var uow = _ls.CreateUnitOfWork()) {
				var site = uow.FindById<Website>(websiteId);
				if (site == null || site.Account == null)
					throw new Exception("Could not find account associated with website " + websiteId);
				return site.Account.PrimaryLocation;
			}
		}

		public int GetNearestWebsiteIdFromZip(string zip) {
			var locations = _locationService.GetLocationsByPostalCode(zip, 100, 99999);
			using (var uow = _ls.CreateUnitOfWork()) {
				foreach (var l in locations) {
					var location = uow.FindById<Location>(l.Id);
					if (location == null || location.OwnerAccounts == null)
						continue;
				    var acc = location.OwnerAccounts.FirstOrDefault(a => !a.IsCoop && a.Website != null);
				    if (acc == null) continue;
					return acc.Website.Id;
				}
			}
			throw new Exception("Could not find a website associated with an MGR store within 99999 miles of " + zip);
		}

		public int? GetSiteIdFromDomainName(string domainName) {
			using (var uow = _ls.CreateUnitOfWork()) {
				var domain = uow.Domains.FirstOrDefault(d => d.Name == domainName);
				return (domain == null) ? (int?)null : domain.WebsiteId;
			}
		}

		public string GetDomainNameFromSiteId(int siteId) {
			if (siteId == KnownIds.Website.Corporate)
				return "musicgoround.com";

			using (var uow = _ls.CreateUnitOfWork()) {
				var domain = uow.Domains.FirstOrDefault(d => d.WebsiteId == siteId);
				return (domain == null) ? (string)null : domain.Name;
			}
		}

		private bool CanAdminSite(User user, int siteId) {
			if (user == null || user.UserType == null)
				return false;

			// Treefort gods and Winmark - can admin any store
			if (user.UserType.IsSystemAdministrator || user.UserType.IsFranchiseAdministrator)
				return true;

			//  Zee admins and content editors - can only admin their store
			if (user.UserType.IsAccountAdministrator || user.UserType.IsTrustedUser || user.UserType.IsContentEditor) {
				var site = user.Account.Website;
				return (site != null && site.Id == siteId);
			}

			// not an admin user
			return false;
		}
	}
}
