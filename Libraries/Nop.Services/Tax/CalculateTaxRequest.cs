using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Shipping;
using Nop.Services.Common;
using Treefort.Portal;

namespace Nop.Services.Tax
{
    /// <summary>
    /// Represents a request for tax calculation
    /// </summary>
    public partial class CalculateTaxRequest
    {
        /// <summary>
        /// Gets or sets a customer
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets an address
        /// </summary>
        public Address CustomerAddress { get; set; }

		public Address StoreAddress { get; set; }

        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets a tax category identifier
        /// </summary>
        public int TaxCategoryId { get; set; }

        public bool InStorePickup
        {
            get
            {
                return Customer != null &&
                       (Customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.LastShippingOption) != null) &&
                       (Customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.LastShippingOption)
                           .IsInStorePickup);
            }
        }

	    public bool PurchasingFromSameState {
		    get {
				// in-store pickup? then you're in the same state. pony up the tax.
				if (Customer != null) {
					var shippingOption = Customer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.LastShippingOption);
					if (shippingOption != null && shippingOption.IsInStorePickup)
						return true;
				}
					
				return
					CustomerAddress != null &&
					StoreAddress != null &&
					CustomerAddress.CountryId == KnownIds.Country.US &&
					StoreAddress.CountryId == KnownIds.Country.US &&
					CustomerAddress.StateProvinceId.HasValue &&
					StoreAddress.StateProvinceId.HasValue &&
					CustomerAddress.StateProvinceId == StoreAddress.StateProvinceId;
		    }
	    }
    }
}
