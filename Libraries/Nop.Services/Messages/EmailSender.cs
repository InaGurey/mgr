﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Nop.Core.Domain.Messages;

namespace Nop.Services.Messages
{
    public partial class EmailSender:IEmailSender
    {
        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="fromAddress">From address</param>
        /// <param name="fromName">From display name</param>
        /// <param name="toAddress">To address</param>
        /// <param name="toName">To display name</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses ist</param>
        public void SendEmail(EmailAccount emailAccount, string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null)
        {
            SendEmail(emailAccount, subject, body, 
                new MailAddress(fromAddress, fromName), new MailAddress(toAddress, toName), 
                bcc, cc);
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="from">From address</param>
        /// <param name="to">To address</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses ist</param>
        public virtual void SendEmail(EmailAccount emailAccount, string subject, string body,
            MailAddress from, MailAddress to,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null)
        {
            var message = new MailMessage();
			message.From = from;
            message.To.Add(to);
	        message.ReplyToList.Add(from);

			if (null != bcc)
            {
                foreach (var address in bcc.Where(bccValue => !String.IsNullOrWhiteSpace(bccValue)))
                {
                    message.Bcc.Add(address.Trim());
                }
            }
            if (null != cc)
            {
                foreach (var address in cc.Where(ccValue => !String.IsNullOrWhiteSpace(ccValue)))
                {
                    message.CC.Add(address.Trim());
                }
            }
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            using (var smtpClient = new SmtpClient())
            {
				// to be safe, default to test mode unless it's explicitly "False"
	            if (ConfigurationManager.AppSettings["TestEmail"] == "False") {
					smtpClient.UseDefaultCredentials = emailAccount.UseDefaultCredentials;
					smtpClient.Host = emailAccount.Host;
					smtpClient.Port = emailAccount.Port;
					smtpClient.EnableSsl = emailAccount.EnableSsl;
					smtpClient.Credentials = emailAccount.UseDefaultCredentials ?
						CredentialCache.DefaultNetworkCredentials :
						new NetworkCredential(emailAccount.Username, emailAccount.Password);
				}
	            else {
					// a little code borrowin' from screenfeed
					smtpClient.Host = "smtp.gmail.com";
					smtpClient.Port = 587;
					smtpClient.EnableSsl = true;
					smtpClient.Timeout = 10000;
					smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
					smtpClient.UseDefaultCredentials = false;
					smtpClient.Credentials = new NetworkCredential("screenfeed.test@gmail.com", "crash4317");

					message.To.Clear();
					message.CC.Clear();
					message.Bcc.Clear();
					message.To.Add(ConfigurationManager.AppSettings["SendAllMailTo"] ?? "todd@screenfeed.com");
	            }

				smtpClient.Send(message);
            }
        }
    }
}
