using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Services.Events;
using Nop.Services.Orders;

namespace Nop.Services.Shipping
{
    /// <summary>
    /// Shipment service
    /// </summary>
    public partial class ShipmentService : IShipmentService
    {
        #region Fields

        private readonly IRepository<Shipment> _shipmentRepository;
        private readonly IRepository<UpsShipment> _upsShipmentRepository;
        private readonly IRepository<ShipmentOrderProductVariant> _sopvRepository;
        private readonly IEventPublisher _eventPublisher;
	    private readonly IOrderService _orderService;

	    #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="shipmentRepository">Shipment repository</param>
        /// <param name="upsShipmentRepository"></param>
        /// <param name="sopvRepository">Shipment order product variant repository</param>
        /// <param name="eventPublisher">Event published</param>
        public ShipmentService(IRepository<Shipment> shipmentRepository,
            IRepository<UpsShipment> upsShipmentRepository,
            IRepository<ShipmentOrderProductVariant> sopvRepository,
            IEventPublisher eventPublisher, IOrderService orderService)
        {
            this._shipmentRepository = shipmentRepository;
            this._sopvRepository = sopvRepository;
            this._upsShipmentRepository = upsShipmentRepository;
            _eventPublisher = eventPublisher;
	        _orderService = orderService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
        public virtual void DeleteShipment(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");
            if (shipment.UpsShipment != null)
            {
                _upsShipmentRepository.Delete(shipment.UpsShipment);
            }

            _shipmentRepository.Delete(shipment);

            //event notification
            _eventPublisher.EntityDeleted(shipment);
        }
        
        /// <summary>
        /// Search shipments
        /// </summary>
        /// <param name="createdFrom">Created date from; null to load all records</param>
        /// <param name="createdTo">Created date to; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Customer collection</returns>
        public virtual IPagedList<Shipment> GetAllShipments(DateTime? createdFrom, DateTime? createdTo, int pageIndex, int pageSize) {
	        var query = from s in _shipmentRepository.Table
	                    join o in _orderService.OrdersInThisSite() on s.OrderId equals o.Id
                        join u in _upsShipmentRepository.Table on s.Id equals u.Shipment.Id into us
                        from u in us.DefaultIfEmpty()
	                    select s;

            if (createdFrom.HasValue)
                query = query.Where(s => createdFrom.Value <= s.CreatedOnUtc);
            if (createdTo.HasValue)
                query = query.Where(s => createdTo.Value >= s.CreatedOnUtc);
            query = query.Where(s => s.Order != null && !s.Order.Deleted);
            query = query.OrderByDescending(s => s.CreatedOnUtc);

            var shipments = new PagedList<Shipment>(query, pageIndex, pageSize);
            return shipments;
        }

        /// <summary>
        /// Get shipment by identifiers
        /// </summary>
        /// <param name="shipmentIds">Shipment identifiers</param>
        /// <returns>Shipments</returns>
        public virtual IList<Shipment> GetShipmentsByIds(int[] shipmentIds)
        {
            if (shipmentIds == null || shipmentIds.Length == 0)
                return new List<Shipment>();

            var query = from o in _shipmentRepository.Table
                        where shipmentIds.Contains(o.Id)
                        select o;
            var shipments = query.ToList();
            //sort by passed identifiers
            var sortedOrders = new List<Shipment>();
            foreach (int id in shipmentIds)
            {
                var shipment = shipments.Find(x => x.Id == id);
                if (shipment != null)
                    shipment.UpsShipment = _upsShipmentRepository.Table.FirstOrDefault(u => u.Shipment.Id == id);
                    sortedOrders.Add(shipment);
            }
            return sortedOrders;
        }

        /// <summary>
        /// Gets a shipment
        /// </summary>
        /// <param name="shipmentId">Shipment identifier</param>
        /// <returns>Shipment</returns>
        public virtual Shipment GetShipmentById(int shipmentId)
        {
            if (shipmentId == 0)
                return null;

            var shipment = _shipmentRepository.GetById(shipmentId);
            if (shipment != null)
            {
                shipment.UpsShipment = _upsShipmentRepository.Table.FirstOrDefault(s => s.Shipment.Id == shipmentId);
            }
            return shipment;
        }

        /// <summary>
        /// Inserts a shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
        public virtual void InsertShipment(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            _shipmentRepository.Insert(shipment);

            //event notification
            _eventPublisher.EntityInserted(shipment);
        }

        /// <summary>
        /// Updates the shipment
        /// </summary>
        /// <param name="shipment">Shipment</param>
        public virtual void UpdateShipment(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            _shipmentRepository.Update(shipment);
            if (shipment.UpsShipment != null)
            {
                _upsShipmentRepository.Update(shipment.UpsShipment);
            }

            //event notification
            _eventPublisher.EntityUpdated(shipment);
        }
        
        /// <summary>
        /// Deletes a shipment order product variant
        /// </summary>
        /// <param name="sopv">Shipment order product variant</param>
        public virtual void DeleteShipmentOrderProductVariant(ShipmentOrderProductVariant sopv)
        {
            if (sopv == null)
                throw new ArgumentNullException("sopv");

            _sopvRepository.Delete(sopv);

            //event notification
            _eventPublisher.EntityDeleted(sopv);
        }

        /// <summary>
        /// Gets a shipment order product variant
        /// </summary>
        /// <param name="sopvId">Shipment order product variant identifier</param>
        /// <returns>Shipment order product variant</returns>
        public virtual ShipmentOrderProductVariant GetShipmentOrderProductVariantById(int sopvId)
        {
            if (sopvId == 0)
                return null;

            var sopv = _sopvRepository.GetById(sopvId);
            return sopv;
        }
        
        /// <summary>
        /// Inserts a shipment order product variant
        /// </summary>
        /// <param name="sopv">Shipment order product variant</param>
        public virtual void InsertShipmentOrderProductVariant(ShipmentOrderProductVariant sopv)
        {
            if (sopv == null)
                throw new ArgumentNullException("sopv");

            _sopvRepository.Insert(sopv);

            //event notification
            _eventPublisher.EntityInserted(sopv);
        }

        /// <summary>
        /// Updates the shipment order product variant
        /// </summary>
        /// <param name="sopv">Shipment order product variant</param>
        public virtual void UpdateShipmentOrderProductVariant(ShipmentOrderProductVariant sopv)
        {
            if (sopv == null)
                throw new ArgumentNullException("sopv");

            _sopvRepository.Update(sopv);

            //event notification
            _eventPublisher.EntityUpdated(sopv);
        }
        #endregion
    }
}
