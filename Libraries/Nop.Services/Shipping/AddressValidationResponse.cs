﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Common;

namespace Nop.Services.Shipping
{
    public class AddressValidationResponse
    {
        public enum ValidationStatus
        {
            ValidAddressIndicator,
            AmbiguousAddressIndicator,
            NoCandidatesIndicator,
        }

        public ValidationStatus StatusCode { get; set; }

        public string Status { get { return StatusCode.ToString(); } }

        public IList<Address> Candidates { get; set; }

        public bool Success { get { return StatusCode == ValidationStatus.ValidAddressIndicator; } }
    }
}
