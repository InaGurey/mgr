﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Treefort.Common;
using Treefort.Portal;
using Treefort.Portal.Services;
using WinmarkFranchise.Domain;
using WinmarkFranchise.Domain.Websites;


namespace Nop.Services.CustomReport
{
    public class CustomReportService : ICustomReportService
    {
        private readonly IRepository<Order> _orders;
        private readonly IRepository<OrderProductVariant> _orderProductVariants;
        private readonly IRepository<ProductVariant> _productVariants;
        private readonly IRepository<Product> _products;
        private readonly IRepository<Category> _categories;
        private readonly IRepository<Manufacturer> _manufacturers;
        private readonly IRepository<Core.Domain.Catalog.SavedSearch> _savedSearch;
        private readonly Core.IWorkContext _context;
        private readonly IPortalQueries _portalQueries;
        private readonly ILocationService _locationService;
        private readonly IStoreNumberService _storeNumberService;
        private readonly IProductService _productService;

        public CustomReportService(IRepository<Order> orders,
            IRepository<OrderProductVariant> orderProductVariants,
            IRepository<ProductVariant> productVariants,
            IRepository<Product> products,
            IRepository<Category> categories,
            IRepository<Manufacturer> manufacturers,
            IRepository<Core.Domain.Catalog.SavedSearch> savedSearch,
            Core.IWorkContext context,
            IPortalQueries portalQueries,
            ILocationService locationService,
            IStoreNumberService storeNumberService,
            IProductService productService)
        {
            _orders = orders;
            _orderProductVariants = orderProductVariants;
            _productVariants = productVariants;
            _products = products;
            _categories = categories;
            _manufacturers = manufacturers;
            _savedSearch = savedSearch;
            _context = context;
            _portalQueries = portalQueries;
            _locationService = locationService;
            _storeNumberService = storeNumberService;
            _productService = productService;
        }


        public ItemSalesResult ItemSales(DateTime? startDate, DateTime? endDate, string storeNumber)
        {
            var query = GetBaseQuery(startDate, endDate);
            storeNumber = GetRestrictedStoreNumber(storeNumber);
            if (storeNumber != null) { query = query.Where(o => o.StoreNumber == storeNumber); }

            var joined = from o in query
                         join opv in _orderProductVariants.Table on o equals opv.Order
                         join pv in _productVariants.Table on opv.ProductVariant equals pv
                         join p in _products.Table on pv.Product equals p
                         join c in _categories.Table on p.DrsImportCategoryCode equals c.DrsCode
                         orderby o.CreatedOnUtc, o.Id
                         select new ItemSalesDetails {
                            TransactionDate = o.CreatedOnUtc,
                            OrderId = o.Id,
                            ProductName = p.Name,
                            ProductPrice = opv.PriceExclTax,
                            OrderShippingCost = o.OrderShippingExclTax,
                            CategoryName = c.Name,
                            ActiveDate = pv.PurchaseDate,
                        };

            var itemSales = joined.ToList();

            foreach (var i in itemSales) {
                i.TransactionDate = i.TransactionDate.ToLocalTime().Date;
                i.ProductAge = i.ActiveDate.HasValue
                                   ? DateTime.Today.Subtract(i.ActiveDate.Value)
                                   : (TimeSpan?)null;
            }

            var result = new ItemSalesResult {Sales = itemSales};
            return result;
        }

        public SavedSearchesResult SavedSearches(DateTime? startDate, DateTime? endDate, string storeNumber)
        {
            var query = _savedSearch.Table;
            if (startDate.HasValue)
                query = query.Where(o => o.StartDate >= startDate.Value);

            if (endDate.HasValue)
                query = query.Where(o => o.StartDate <= endDate.Value);

            storeNumber = GetRestrictedStoreNumber(storeNumber);
            if (!storeNumber.IsNullOrEmpty()) {
                var acct = _storeNumberService.GetAccount(Franchises.MusicGoRound, storeNumber);
                if (acct != null && acct.Website != null)
                    query = query.Where(o => o.SiteId == acct.Website.Id);
            }

            var dbSearches = query.OrderBy(o => o.StartDate).ToList();
            var results = dbSearches.Select(o => new SavedSearch {
                Status = o.DeletedOnUtc.HasValue && o.DeletedOnUtc.Value <= DateTime.Today
                                            ? SavedSearchStatus.Cancelled
                                            : o.EndDate <= DateTime.Today
                                                  ? SavedSearchStatus.Expired
                                                  : SavedSearchStatus.Active,
                DateOfActivation = o.StartDate,
                DateOfExpiration = o.EndDate,
                DateOfCancellation = o.DeletedOnUtc,
                Occurrence = o.Frequency.IfNotNullOrEmpty(f => Char.ToUpper(f[0]) + f.Substring(1)),
                Duration = o.Duration.IfNotNullOrEmpty(TimeSpan.Parse, TimeSpan.Zero),
                DurationName = o.Duration == "7.00:00:00" ? "One week" :
                                o.Duration == "30.00:00:00" ? "One month" :
                                o.Duration == "90.00:00:00" ? "Three months" : null,
                NotifyOnlyWhenNewResults = o.NotifyOnlyWhenNewResults,
                Keywords = o.SearchParameters.Keywords,
                CategoryName = o.SearchParameters.CategoryId.IfHasValue(i => _categories.GetById(i).IfNotNull(c => c.Name)),
                BrandName = o.SearchParameters.ManufacturerId.IfHasValue(i => _manufacturers.GetById(i).IfNotNull(m => m.Name)),
                ClearanceOnly = o.SearchParameters.ClearanceOnly,
                StoreLocation = o.SearchParameters.StoreNumbers.IfNotNullOrEmpty(s => string.Join(",", s)),
                PostalCode = o.SearchParameters.PostalCode,
                MilesFromPostalCode = o.SearchParameters.PostalCode.IfNotNullOrEmpty(s => o.SearchParameters.MilesFromPostalCode, (int?)null),
                MinimumPrice = o.SearchParameters.MinimumPrice,
                MaximumPrice = o.SearchParameters.MaximumPrice,
                PriceRange = 
                    !o.SearchParameters.MinimumPrice.HasValue && !o.SearchParameters.MaximumPrice.HasValue
                        ? null
                        : !o.SearchParameters.MaximumPrice.HasValue
                            ? "More than " + o.SearchParameters.MinimumPrice.Value.ToString("C2")
                            : !o.SearchParameters.MinimumPrice.HasValue
                                ? "Less than " + o.SearchParameters.MaximumPrice.Value.ToString("C2")
                                : o.SearchParameters.MinimumPrice.Value.ToString("C2") + " - " + o.SearchParameters.MaximumPrice.Value.ToString("C2")
            });

            return new SavedSearchesResult {SavedSearches = results};
        }

        public TotalSalesResult TotalSales(DateTime? startDate, DateTime? endDate, string storeNumber, bool includeTaxAndShipping, TotalSalesSort sort)
        {
            var query = GetBaseQuery(startDate, endDate, includeRefundedOrders: true);
            var storeQuery = GetStoreQuery(query, GetRestrictedStoreNumber(storeNumber));

			var systemTotals = !UserIsCorporateAdmin() ? null : query.GroupBy(o => "System").Select(g => new StoreSales {
		        StoreNumber = g.Key,
		        NumberOfOrders = g.Count(),
		        TotalSales = g.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax),
		        RefundedSales = g.Sum(o => o.RefundedAmount == o.OrderTotal && !includeTaxAndShipping ? o.OrderSubtotalExclTax : o.RefundedAmount)
	        }).FirstOrDefault();

	        var storeTotals = storeQuery.Select(g => new StoreSales {
		        StoreNumber = g.Key,
		        NumberOfOrders = g.Count(),
		        TotalSales = g.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax),
		        RefundedSales = g.Sum(o => o.RefundedAmount == o.OrderTotal && !includeTaxAndShipping ? o.OrderSubtotalExclTax : o.RefundedAmount)
	        });

			switch (sort) {
				case TotalSalesSort.TotalSales:
					storeTotals = storeTotals.OrderByDescending(s => s.TotalSales);
					break;

				case TotalSalesSort.Orders:
					storeTotals = storeTotals.OrderByDescending(s => s.NumberOfOrders);
					break;

				case TotalSalesSort.AverageSale:
					storeTotals = storeTotals.OrderByDescending(s => s.TotalSales / s.NumberOfOrders);
					break;

				case TotalSalesSort.StoreNumber:
					storeTotals = storeTotals.OrderBy(s => s.StoreNumber);
					break;
			}

            var results = new TotalSalesResult {SystemSales = systemTotals, StoreSales = storeTotals.ToList() };
            FillLocationStoreDisplayName(results.StoreSales);

            if (sort == TotalSalesSort.Location)
                results.StoreSales = results.StoreSales.OrderBy(s => s.StoreDisplayName);

            return results;
        }

        private void FillLocationStoreDisplayName(IEnumerable<IStoreNumberAndName> results)
        {
            foreach (var r in results) {
                var l = _locationService.GetLocationByStoreNumber(r.StoreNumber);
                r.StoreDisplayName =
                    l == null
                        ? r.StoreNumber
                        : l.Region + " - " + l.City;
            }
        }


        public PaymentMethodsResult PaymentMethods(DateTime? startDate, DateTime? endDate, string storeNumber,
            bool includeTaxAndShipping, PaymentMethodsSort sort, PaymentMethodsDisplay display)
        {
            var query = GetBaseQuery(startDate, endDate);
            var storeQuery = GetStoreQuery(query, GetRestrictedStoreNumber(storeNumber));

            var systemTotals = !UserIsCorporateAdmin()
                ? null
                : query.GroupBy(o => "System").Select(g => new StorePaymentMethods
                {
                    StoreNumber = g.Key,
                    AuthorizeNetPayments =
                        g.Where(o => o.PaymentMethodSystemName == AuthorizeNetPaymentMethodDetails.SystemName)
                            .GroupBy(o => o.PaymentMethodSystemName)
                            .Select(g2 => new AuthorizeNetPaymentMethodDetails
                            {
                                NumberOfOrders = g2.Count(),
                                TotalSales = g2.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax)
                            }).FirstOrDefault(),

                    PaypalPayments =
                        g.Where(o => o.PaymentMethodSystemName == PaypalPaymentMethodDetails.SystemName)
                            .GroupBy(o => o.PaymentMethodSystemName)
                            .Select(g2 => new PaypalPaymentMethodDetails
                            {
                                NumberOfOrders = g2.Count(),
                                TotalSales = g2.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax)
                            }).FirstOrDefault(),
                    PayPalExpressPayment =
                        g.Where(o => o.PaymentMethodSystemName == PayPalExpressPaymentMethodDetails.SystemName)
                            .GroupBy(o => o.PaymentMethodSystemName)
                            .Select(g2 => new PayPalExpressPaymentMethodDetails
                            {
                                NumberOfOrders = g2.Count(),
                                TotalSales = g2.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax)
                            }).FirstOrDefault()
                }).FirstOrDefault();

            switch (sort)
            {
                case PaymentMethodsSort.Total:
                    switch (display)
                    {
                        case PaymentMethodsDisplay.Sales:
                            storeQuery = storeQuery.OrderByDescending(g => g.Sum(o => o.OrderTotal));
                            break;

                        case PaymentMethodsDisplay.Orders:
                            storeQuery = storeQuery.OrderByDescending(g => g.Count());
                            break;
                    }
                    break;

                case PaymentMethodsSort.StoreNumber:
                    storeQuery = storeQuery.OrderBy(g => g.Key);
                    break;
            }

            var storeTotals = storeQuery.Select(g => new StorePaymentMethods
            {
                StoreNumber = g.Key,
                AuthorizeNetPayments =
                    g.Where(o => o.PaymentMethodSystemName == AuthorizeNetPaymentMethodDetails.SystemName)
                        .GroupBy(o => 1)
                        .Select(g2 => new AuthorizeNetPaymentMethodDetails
                        {
                            NumberOfOrders = g2.Count(),
                            TotalSales = g2.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax)
                        }).FirstOrDefault(),

                PaypalPayments =
                    g.Where(o => o.PaymentMethodSystemName == PaypalPaymentMethodDetails.SystemName)
                        .GroupBy(o => 1)
                        .Select(g2 => new PaypalPaymentMethodDetails
                        {
                            NumberOfOrders = g2.Count(),
                            TotalSales = g2.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax)
                        }).FirstOrDefault(),


                PayPalExpressPayment =
                    g.Where(o => o.PaymentMethodSystemName == PayPalExpressPaymentMethodDetails.SystemName)
                        .GroupBy(o => 1)
                        .Select(g2 => new PayPalExpressPaymentMethodDetails
                        {
                            NumberOfOrders = g2.Count(),
                            TotalSales = g2.Sum(o => includeTaxAndShipping ? o.OrderTotal : o.OrderSubtotalExclTax)
                        }).FirstOrDefault()
            });

            var results = new PaymentMethodsResult {SystemPayments = systemTotals, StorePayments = storeTotals.ToList()};
            FillLocationStoreDisplayName(results.StorePayments);

            if (sort == PaymentMethodsSort.Location)
                results.StorePayments = results.StorePayments.OrderBy(s => s.StoreDisplayName);

            return results;
        }


        public CategorySalesResult CategorySales(DateTime? startDate, DateTime? endDate, string storeNumber, bool showSubcategories, CategorySalesSort sort)
        {
            var query = GetBaseQuery(startDate, endDate);
            storeNumber = GetRestrictedStoreNumber(storeNumber);
            if (storeNumber != null) { query = query.Where(o => o.StoreNumber == storeNumber); }

            var categoryCodeLength = showSubcategories ? 4 : 2;
            var joined = from o in query
                         join opv in _orderProductVariants.Table on o equals opv.Order
                         join pv in _productVariants.Table on opv.ProductVariant equals pv
                         join p in _products.Table on pv.Product equals p
                         join c in _categories.Table on p.DrsImportCategoryCode.Substring(0, categoryCodeLength) equals c.DrsCode
                         where !c.Deleted
                         select new { o, opv, pv, p, c };

            var categoryTotals = joined.GroupBy(x => x.c).OrderBy(g => g.Key.Name).Select(g => new CategorySales {
                CategoryName = g.Key.Name,
                TotalSales = g.Sum(x => x.opv.PriceExclTax)
            });

	        switch (sort) {
				case CategorySalesSort.Total:
			        categoryTotals = categoryTotals.OrderByDescending(c => c.TotalSales);
			        break;

				case CategorySalesSort.Name:
			        categoryTotals = categoryTotals.OrderBy(c => c.CategoryName);
			        break;
	        }

            var results = new CategorySalesResult { CategorySales = categoryTotals };
            return results;
        }


        public AccessorySalesResult AccessorySales(DateTime? startDate, DateTime? endDate, string storeNumber, AccessorySalesSort sort, AccessorySalesDisplay display)
        {
            var query = GetBaseQuery(startDate, endDate);
            storeNumber = GetRestrictedStoreNumber(storeNumber);
            if (storeNumber != null) { query = query.Where(o => o.StoreNumber == storeNumber); }

            var joined = from o in query
                         join opv in _orderProductVariants.Table on o equals opv.Order
                         join pv in _productVariants.Table on opv.ProductVariant equals pv
                         where opv.ProductVariant.Product.SiteId == 0
                         select new { o, opv, pv };

            var accesoryTotals = joined.GroupBy(x => x.pv).Select(g => new AccessorySales {
                ProductId = g.Key.ProductId,
                Sku = g.Key.Sku,
                ProductName = g.Key.Product.Name,
                TotalSales = g.Sum(x => x.opv.PriceExclTax),
                QuantitySold = g.Sum(x => x.opv.Quantity)
            }).OrderByDescending(a => a.QuantitySold);

	        switch (sort) {
				case AccessorySalesSort.Total:
					switch (display) {
						case AccessorySalesDisplay.Sales:
							accesoryTotals = accesoryTotals.OrderByDescending(a => a.TotalSales);
							break;

						case AccessorySalesDisplay.Items:
							accesoryTotals = accesoryTotals.OrderByDescending(a => a.QuantitySold);
							break;
					}
			        break;

				case AccessorySalesSort.Sku:
			        accesoryTotals = accesoryTotals.OrderBy(a => a.Sku);
			        break;

				case AccessorySalesSort.Name:
			        accesoryTotals = accesoryTotals.OrderBy(a => a.ProductName);
			        break;
	        }

            var results = new AccessorySalesResult { AccessorySales = accesoryTotals };
            return results;
        }


        private IQueryable<Order> GetBaseQuery(DateTime? startDate, DateTime? endDate, bool includeRefundedOrders = false, bool includeCancelledOrders = false)
        {
            var query = _orders.Table;

            query = query.Where(o => !o.Deleted);
            query = query.Where(o => o.PaymentStatusId != (int)PaymentStatus.Refunded || includeRefundedOrders);

            // only look at cancelled orders if we're trying to get them specifically or refunded orders
            query = query.Where(o => (o.OrderStatusId != (int)OrderStatus.Cancelled || includeCancelledOrders) ||
                                     (o.PaymentStatusId == (int)PaymentStatus.Refunded && includeRefundedOrders));
            
            // if it's a pending paypal order it's not considered complete, they may not have finished paying at PayPal's site
            query = query.Where(o => !(o.PaymentStatusId == (int)PaymentStatus.Pending &&
                                       o.OrderStatusId == (int)OrderStatus.Pending &&
                                       o.PaymentMethodSystemName == PaypalPaymentMethodDetails.SystemName));


            if (startDate.HasValue) {
                var utcStartDate = startDate.Value.Date.ToUniversalTime();
                query = query.Where(o => o.CreatedOnUtc >= utcStartDate);
            }

            if (endDate.HasValue) {
                var utcEndDate = endDate.Value.Date.AddDays(1).ToUniversalTime();
                query = query.Where(o => o.CreatedOnUtc < utcEndDate);
            }

            return query;
        }

        private IQueryable<IGrouping<string, Order>> GetStoreQuery(IQueryable<Order> baseQuery, string storeNumber)
        {
            return (storeNumber == null ? baseQuery : baseQuery.Where(o => o.StoreNumber == storeNumber)).GroupBy(o => o.StoreNumber);
        }

        private string GetRestrictedStoreNumber(string storeNumber)
        {
            return UserIsCorporateAdmin() ? storeNumber :
                _portalQueries.GetLocationFromWebsiteId(_context.SiteId).IfNotNull(l => l.StoreNumber, "NOAUTH");
        }

        private bool UserIsCorporateAdmin()
        {
            var adminRole = _context.CurrentUser.AdminRole();
            return adminRole == AdminRole.SuperAdmin || adminRole == AdminRole.Corporate;
        }
    }
}
