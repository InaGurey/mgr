﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Services.CustomReport
{
    public interface ICustomReportService
    {
        TotalSalesResult TotalSales(DateTime? startDate, DateTime? endDate, string storeNumber, bool includeTaxAndShipping, TotalSalesSort sort);
        PaymentMethodsResult PaymentMethods(DateTime? startDate, DateTime? endDate, string storeNumber, bool includeTaxAndShipping, PaymentMethodsSort sort, PaymentMethodsDisplay display);
        CategorySalesResult CategorySales(DateTime? startDate, DateTime? endDate, string storeNumber, bool showSubcategories, CategorySalesSort sort);
        AccessorySalesResult AccessorySales(DateTime? startDate, DateTime? endDate, string storeNumber, AccessorySalesSort sort, AccessorySalesDisplay display);
        SavedSearchesResult SavedSearches(DateTime? startDate, DateTime? endDate, string storeNumber);
        ItemSalesResult ItemSales(DateTime? startDate, DateTime? endDate, string storeNumber);
    }

    public interface IStoreNumberAndName
    {
        string StoreNumber { get; set; }
        string StoreDisplayName { get; set; }
    }


    public enum TotalSalesSort { TotalSales, Orders, AverageSale, StoreNumber, Location }

    public class TotalSalesResult
    {
        public StoreSales SystemSales { get; set; }
        public IEnumerable<StoreSales> StoreSales { get; set; }
    }

    public class StoreSales : IStoreNumberAndName
    {
        public string StoreNumber { get; set; }
        public string StoreDisplayName { get; set; }
        public decimal TotalSales { get; set; }
        public decimal CompletedSales { get { return TotalSales - RefundedSales; } }
        public decimal RefundedSales { get; set; }
        public int NumberOfOrders { get; set; }
        public decimal AverageOrderTotal { get { return NumberOfOrders == 0 ? 0 : TotalSales / NumberOfOrders; } }
    }


    public enum PaymentMethodsDisplay { Sales, Orders }
    public enum PaymentMethodsSort { Total, StoreNumber, Location }

    public class PaymentMethodsResult
    {
        public PaymentMethodsDisplay Display { get; set; }

        private StorePaymentMethods _systemPayments;
        public StorePaymentMethods SystemPayments
        {
            get { return _systemPayments; }
            set { _systemPayments = value; }
        }

        private IEnumerable<StorePaymentMethods> _storePayments;
        public IEnumerable<StorePaymentMethods> StorePayments
        {
            get { return _storePayments ?? Enumerable.Empty<StorePaymentMethods>(); }
            set { _storePayments = value; }
        }
    }

    public class StorePaymentMethods : IStoreNumberAndName
    {
        public string StoreNumber { get; set; }
        public string StoreDisplayName { get; set; }

        private AuthorizeNetPaymentMethodDetails _authorizeNetPayments;
        public AuthorizeNetPaymentMethodDetails AuthorizeNetPayments
        {
            get { return _authorizeNetPayments ?? new AuthorizeNetPaymentMethodDetails(); } 
            set { _authorizeNetPayments = value; }
        }

        private PaypalPaymentMethodDetails _paypalPayments;
        public PaypalPaymentMethodDetails PaypalPayments
        {
            get { return _paypalPayments ?? new PaypalPaymentMethodDetails(); }
            set { _paypalPayments = value; }
        }

        private PayPalExpressPaymentMethodDetails _payPalExpressPayment;
        public PayPalExpressPaymentMethodDetails PayPalExpressPayment
        {
            get { return _payPalExpressPayment ?? new PayPalExpressPaymentMethodDetails(); }
            set { _payPalExpressPayment = value; }
        }
    }

    abstract public class PaymentMethodDetails
    {
        abstract public string DisplayName { get; }
        public int NumberOfOrders { get; set; }
        public decimal TotalSales { get; set; }

        public decimal GetChartValue(PaymentMethodsDisplay display)
        {
            return display == PaymentMethodsDisplay.Orders ? NumberOfOrders : TotalSales;
        }
    }

    public class AuthorizeNetPaymentMethodDetails : PaymentMethodDetails
    {
        public const string SystemName = "Payments.AuthorizeNet";
        public override string DisplayName { get { return "CC"; } }
    }

    public class PaypalPaymentMethodDetails : PaymentMethodDetails
    {
        public const string SystemName = "Payments.PayPalStandard";
        public override string DisplayName { get { return "PP"; } }
    }

    public class PayPalExpressPaymentMethodDetails : PaymentMethodDetails
    {
        public const string SystemName = "Payments.PayPalExpress";
        public override string DisplayName { get { return "PPE"; } }
    }


	public enum CategorySalesDisplay { Sales, Items }
	public enum CategorySalesSort { Total, Name }

    public class CategorySalesResult
    {
        public IEnumerable<CategorySales> CategorySales { get; set; }
    }

    public class CategorySales
    {
        public string CategoryName { get; set; }
        public decimal TotalSales { get; set; }
    }


	public enum AccessorySalesDisplay { Sales, Items }
	public enum AccessorySalesSort { Total, Sku, Name }

    public class AccessorySalesResult
    {
        public IEnumerable<AccessorySales> AccessorySales { get; set; }
    }

    public class AccessorySales
    {
        public int ProductId { get; set; }
        public string Sku { get; set; }
        public string ProductName { get; set; }
        public decimal TotalSales { get; set; }
        public int QuantitySold { get; set; }

		public decimal GetChartValue(AccessorySalesDisplay display)
		{
			switch (display) {
				case AccessorySalesDisplay.Sales: return this.TotalSales;
				case AccessorySalesDisplay.Items: 
				default: return this.QuantitySold;
			}
		}
    }

    public class SavedSearchesResult
    {
        public IEnumerable<SavedSearch> SavedSearches { get; set; }
    }

    public enum SavedSearchStatus { Active, Expired, Cancelled }

    public class SavedSearch
    {
        public SavedSearchStatus Status { get; set; }
        public DateTime DateOfActivation { get; set; }
        public DateTime DateOfExpiration { get; set; }
        public DateTime? DateOfCancellation { get; set; }
        public string Occurrence { get; set; }
        public TimeSpan Duration { get; set; }
        public string DurationName { get; set; }
        public bool NotifyOnlyWhenNewResults { get; set; }
        public string Keywords { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
        public bool ClearanceOnly { get; set; }
        public string StoreLocation { get; set; }
        public string PostalCode { get; set; }
        public int? MilesFromPostalCode { get; set; }
        public decimal? MinimumPrice { get; set; }
        public decimal? MaximumPrice { get; set; }
        public string PriceRange { get; set; }
    }


    public class ItemSalesResult
    {
        public IEnumerable<ItemSalesDetails> Sales { get; set; }
    }

    public class ItemSalesDetails
    {
        public DateTime TransactionDate { get; set; }
        public DateTime? ActiveDate { get; set; }
        public int OrderId { get; set; }
        public TimeSpan? ProductAge { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal OrderShippingCost { get; set; }
        public string CategoryName { get; set; }
    }
}
