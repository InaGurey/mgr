using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;

namespace Nop.Services.Catalog
{
	public class ProductSearchOptions
	{
		public IList<int> CategoryIds { get; set; }
		public int? ManufacturerId { get; set; }
		public bool? FeaturedProducts { get; set; }
		public bool? ShowOnHomePageProducts { get; set; }
		public decimal? PriceMin { get; set; }
		public decimal? PriceMax { get; set; }
		public int? ProductTagId { get; set; }
		public string Keywords { get; set; }
		public bool SearchDescriptions { get; set; }
		public bool SearchProductTags { get; set; }
		public bool SearchCategoryName { get; set; }
		public bool? WithImages { get; set; }
		public bool? WithDescrip { get; set; }
		public bool? Published { get; set; }
		public bool? Used { get; set; }
		public IList<int> FilteredSpecs { get; set; }
		public ProductSortingEnum SortBy { get; set; }
		public int PageIndex { get; set; }
		public int PageSize { get; set; }
		public bool SearchAllStores { get; set; }
		public bool ExcludeCorporateProducts { get; set; }
		public bool ShowHidden { get; set; }
		public bool WaitingOnDelay { get; set; }
		public bool ClearanceOnly { get; set; }
	    public IList<string> StoreNumbers { get; set; }
        public bool? InStock { get; set; }
        public bool ExcludeAccessories { get; set; }
        public bool ExcludePriceReduction { get; set; }

		/// <summary>
		/// Shortcut to set CategoryIds to a collection of 1 value (or nothing if null or zero). Overwrites any existing!
		/// </summary>
		public int? CategoryId {
			set { CategoryIds = (value == null || value == 0) ? null : new List<int> { value.Value }; }
		}

		public ProductSearchOptions() {
			SearchCategoryName = true;
			SortBy = ProductSortingEnum.NameAsc;
			PageSize = int.MaxValue;
		    InStock = true;
		}
	}

	/// <summary>
    /// Product service
    /// </summary>
    public partial interface IProductService
    {
        #region Products

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="product">Product</param>
        void DeleteProduct(Product product);

        /// <summary>
        /// Gets all products
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product collection</returns>
        IList<Product> GetAllProducts(bool showHidden = false);

        /// <summary>
        /// Gets all products displayed on the home page
        /// </summary>
        /// <returns>Product collection</returns>
        IList<Product> GetAllProductsDisplayedOnHomePage();

        /// <summary>
        /// Gets product
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Product</returns>
        Product GetProductById(int productId);

        /// <summary>
        /// Gets products by identifier
        /// </summary>
        /// <param name="productIds">Product identifiers</param>
        /// <returns>Products</returns>
        IList<Product> GetProductsByIds(int[] productIds);

        /// <summary>
        /// Inserts a product
        /// </summary>
        /// <param name="product">Product</param>
        void InsertProduct(Product product);

        /// <summary>
        /// Updates the product
        /// </summary>
        /// <param name="product">Product</param>
        void UpdateProduct(Product product);

		IPagedList<int> GetProductIds(ProductSearchOptions options);
			
		IPagedList<Product> SearchProducts(ProductSearchOptions options);

        /// <summary>
        /// Trimmed down version to optimize home page
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
	    IList<int> GetHomePageProductIds(ProductSearchOptions options);

        /// <summary>
        /// Update product review totals
        /// </summary>
        /// <param name="product">Product</param>
        void UpdateProductReviewTotals(Product product);

        #endregion

        #region Product variants
        
        /// <summary>
        /// Get low stock product variants
        /// </summary>
        /// <returns>Result</returns>
        IList<ProductVariant> GetLowStockProductVariants();

        /// <summary>
        /// Gets a product variant
        /// </summary>
        /// <param name="productVariantId">Product variant identifier</param>
        /// <returns>Product variant</returns>
        ProductVariant GetProductVariantById(int productVariantId);

        /// <summary>
        /// Get product variants by product identifiers
        /// </summary>
        /// <param name="productIds">Product identifiers</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variants</returns>
        IList<ProductVariant> GetProductVariantsByProductIds(int[] productIds, bool showHidden = false);

        /// <summary>
        /// Gets a product variant by SKU
        /// </summary>
        /// <param name="sku">SKU</param>
        /// <returns>Product variant</returns>
		ProductVariant GetProductVariantBySku(string sku);

        /// <summary>
        /// Inserts a product variant
        /// </summary>
        /// <param name="productVariant">The product variant</param>
        void InsertProductVariant(ProductVariant productVariant);

        /// <summary>
        /// Updates the product variant
        /// </summary>
        /// <param name="productVariant">The product variant</param>
        void UpdateProductVariant(ProductVariant productVariant);

        /// <summary>
        /// Gets product variants by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variant collection</returns>
        IList<ProductVariant> GetProductVariantsByProductId(int productId, bool showHidden = false);

        /// <summary>
        /// Get all (including no longer available) product variants by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <param name="lastAvailableDateTime"></param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variant collection</returns>
	    IList<ProductVariant> GetAllProductVariantsByProductIdLastAvailableDate(int productId, DateTime lastAvailableDateTime, bool showHidden = false);

        /// <summary>
        /// Delete a product variant
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        void DeleteProductVariant(ProductVariant productVariant);
        
        /// <summary>
        /// Adjusts inventory
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        /// <param name="decrease">A value indicating whether to increase or descrease product variant stock quantity</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="attributesXml">Attributes in XML format</param>
        void AdjustInventory(ProductVariant productVariant, bool decrease,
            int quantity, string attributesXml);

        /// <summary>
        /// Search product variants
        /// </summary>
        /// <param name="categoryId">Category identifier; 0 to load all records</param>
        /// <param name="manufacturerId">Manufacturer identifier; 0 to load all records</param>
        /// <param name="keywords">Keywords</param>
        /// <param name="searchDescriptions">A value indicating whether to search in descriptions</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variants</returns>
        IPagedList<ProductVariant> SearchProductVariants(int categoryId, int manufacturerId, 
            string keywords, bool searchDescriptions, int pageIndex, int pageSize, bool showHidden = false);

        /// <summary>
        /// Update HasTierPrices property (used for performance optimization)
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        void UpdateHasTierPricesProperty(ProductVariant productVariant);


        /// <summary>
        /// Update HasDiscountsApplied property (used for performance optimization)
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        void UpdateHasDiscountsApplied(ProductVariant productVariant);

        #endregion

        #region Related products

        /// <summary>
        /// Deletes a related product
        /// </summary>
        /// <param name="relatedProduct">Related product</param>
        void DeleteRelatedProduct(RelatedProduct relatedProduct);

        /// <summary>
        /// Gets a related product collection by product identifier
        /// </summary>
        /// <param name="productId1">The first product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Related product collection</returns>
        IList<RelatedProduct> GetRelatedProductsByProductId1(int productId1, bool showHidden = false, int? siteId = null);

        /// <summary>
        /// Gets a related product
        /// </summary>
        /// <param name="relatedProductId">Related product identifier</param>
        /// <returns>Related product</returns>
        RelatedProduct GetRelatedProductById(int relatedProductId);

        /// <summary>
        /// Inserts a related product
        /// </summary>
        /// <param name="relatedProduct">Related product</param>
        void InsertRelatedProduct(RelatedProduct relatedProduct);

        /// <summary>
        /// Updates a related product
        /// </summary>
        /// <param name="relatedProduct">Related product</param>
        void UpdateRelatedProduct(RelatedProduct relatedProduct);

        #endregion

        #region Cross-sell products

        /// <summary>
        /// Deletes a cross-sell product
        /// </summary>
        /// <param name="crossSellProduct">Cross-sell</param>
        void DeleteCrossSellProduct(CrossSellProduct crossSellProduct);

        /// <summary>
        /// Gets a cross-sell product collection by product identifier
        /// </summary>
        /// <param name="productId1">The first product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Cross-sell product collection</returns>
        IList<CrossSellProduct> GetCrossSellProductsByProductId1(int productId1, bool showHidden = false);

        /// <summary>
        /// Gets a cross-sell product
        /// </summary>
        /// <param name="crossSellProductId">Cross-sell product identifier</param>
        /// <returns>Cross-sell product</returns>
        CrossSellProduct GetCrossSellProductById(int crossSellProductId);

        /// <summary>
        /// Inserts a cross-sell product
        /// </summary>
        /// <param name="crossSellProduct">Cross-sell product</param>
        void InsertCrossSellProduct(CrossSellProduct crossSellProduct);

        /// <summary>
        /// Updates a cross-sell product
        /// </summary>
        /// <param name="crossSellProduct">Cross-sell product</param>
        void UpdateCrossSellProduct(CrossSellProduct crossSellProduct);
        
        /// <summary>
        /// Gets a cross-sells
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <param name="numberOfProducts">Number of products to return</param>
        /// <returns>Cross-sells</returns>
        IList<Product> GetCrosssellProductsByShoppingCart(IList<ShoppingCartItem> cart, int numberOfProducts);

        #endregion
        
        #region Tier prices

        /// <summary>
        /// Deletes a tier price
        /// </summary>
        /// <param name="tierPrice">Tier price</param>
        void DeleteTierPrice(TierPrice tierPrice);

        /// <summary>
        /// Gets a tier price
        /// </summary>
        /// <param name="tierPriceId">Tier price identifier</param>
        /// <returns>Tier price</returns>
        TierPrice GetTierPriceById(int tierPriceId);

        /// <summary>
        /// Inserts a tier price
        /// </summary>
        /// <param name="tierPrice">Tier price</param>
        void InsertTierPrice(TierPrice tierPrice);

        /// <summary>
        /// Updates the tier price
        /// </summary>
        /// <param name="tierPrice">Tier price</param>
        void UpdateTierPrice(TierPrice tierPrice);

        #endregion

        #region Product pictures

        /// <summary>
        /// Deletes a product picture
        /// </summary>
        /// <param name="productPicture">Product picture</param>
        void DeleteProductPicture(ProductPicture productPicture);

        /// <summary>
        /// Gets a product pictures by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <returns>Product pictures</returns>
        IList<ProductPicture> GetProductPicturesByProductId(int productId);

        /// <summary>
        /// Gets a product picture
        /// </summary>
        /// <param name="productPictureId">Product picture identifier</param>
        /// <returns>Product picture</returns>
        ProductPicture GetProductPictureById(int productPictureId);

        /// <summary>
        /// Inserts a product picture
        /// </summary>
        /// <param name="productPicture">Product picture</param>
        void InsertProductPicture(ProductPicture productPicture);

        /// <summary>
        /// Updates a product picture
        /// </summary>
        /// <param name="productPicture">Product picture</param>
        void UpdateProductPicture(ProductPicture productPicture);

        #endregion


	    void InsertSavedSearch(SavedSearch savedSearch);
	    void UpdateSavedSearch(SavedSearch savedSearch);
	    void CancelSavedSearch(SavedSearch savedSearch);
	    SavedSearch GetSavedSearch(int id);
        SavedSearch GetSavedSearch(string slug);
	    IEnumerable<SavedSearch> GetSavedSearchesToNotify();
	    IEnumerable<SavedSearch> FindSavedSearches(DateTime? startDate, DateTime? endDate);
        IEnumerable<SavedSearch> FindSavedSearches(DateTime? startDate, DateTime? endDate, int siteId);
	    IEnumerable<SavedSearch> FindSavedSearches(DateTime? startDate, DateTime? endDate, int? siteId);
    }
}
