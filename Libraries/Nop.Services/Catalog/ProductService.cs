using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Data;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Treefort.Portal;
using Treefort.Portal.Services;
using iTextSharp.text;

namespace Nop.Services.Catalog
{
    /// <summary>
    /// Product service
    /// </summary>
    public partial class ProductService : IProductService
    {
        #region Constants
        private const string PRODUCTS_BY_ID_KEY = "Nop.product.id-{0}";
        private const string PRODUCTVARIANTS_ALL_KEY = "Nop.productvariant.all-{0}-{1}";
        private const string PRODUCTVARIANTS_ALL_AVAILABLE_KEY = "Nop.productvariant.all-{0}-{1}-{2}";
        private const string PRODUCTVARIANTS_BY_ID_KEY = "Nop.productvariant.id-{0}";
        private const string PRODUCTS_PATTERN_KEY = "Nop.product.";
        private const string PRODUCTVARIANTS_PATTERN_KEY = "Nop.productvariant.";
        private const string TIERPRICES_PATTERN_KEY = "Nop.tierprice.";
        private const string CLOSEST_LOCATIONS_BY_CITY_STATE = "Nop.locations.closest-{0}-{1}";
        #endregion

        #region Fields

        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductVariant> _productVariantRepository;
        private readonly IRepository<ProductCategory> _productCategoryRepository;
        private readonly IRepository<Category> _categoryRepository; 
        private readonly IRepository<RelatedProduct> _relatedProductRepository;
        private readonly IRepository<CrossSellProduct> _crossSellProductRepository;
        private readonly IRepository<TierPrice> _tierPriceRepository;
        private readonly IRepository<LocalizedProperty> _localizedPropertyRepository;
        private readonly IRepository<ProductPicture> _productPictureRepository;
        private readonly IRepository<ProductSpecificationAttribute> _productSpecificationAttributeRepository;
        private readonly IRepository<SavedSearch> _savedSearchRepository;
		private readonly IPortalQueries _portalQueries;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;
        private readonly IPerRequestCacheManager _cacheManager;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CommonSettings _commonSettings;
        private readonly IEventPublisher _eventPublisher;
		private readonly IWorkContext _workContext;
        private readonly ILocationService _locationService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="productVariantRepository">Product variant repository</param>
        /// <param name="relatedProductRepository">Related product repository</param>
        /// <param name="crossSellProductRepository">Cross-sell product repository</param>
        /// <param name="tierPriceRepository">Tier price repository</param>
        /// <param name="localizedPropertyRepository">Localized property repository</param>
        /// <param name="productPictureRepository">Product picture repository</param>
        /// <param name="productSpecificationAttributeRepository">Product specification attribute repository</param>
        /// <param name="productAttributeService">Product attribute service</param>
        /// <param name="productAttributeParser">Product attribute parser service</param>
        /// <param name="languageService">Language service</param>
        /// <param name="workflowMessageService">Workflow message service</param>
        /// <param name="dataProvider">Data provider</param>
        /// <param name="dbContext">Database Context</param>
        /// <param name="localizationSettings">Localization settings</param>
        /// <param name="commonSettings">Common settings</param>
        /// <param name="eventPublisher">Event published</param>
        public ProductService(IPerRequestCacheManager cacheManager,
            IRepository<Product> productRepository,
            IRepository<ProductVariant> productVariantRepository,
            IRepository<ProductCategory> productCategoryRepository,
            IRepository<Category> categoryRepository,
            IRepository<RelatedProduct> relatedProductRepository,
            IRepository<CrossSellProduct> crossSellProductRepository,
            IRepository<TierPrice> tierPriceRepository,
            IRepository<ProductPicture> productPictureRepository,
            IRepository<LocalizedProperty> localizedPropertyRepository,
            IRepository<ProductSpecificationAttribute> productSpecificationAttributeRepository,
            IRepository<SavedSearch> savedSearchRepository,
            IPortalQueries portalQueries,
            IProductAttributeService productAttributeService,
            IProductAttributeParser productAttributeParser,
            ILanguageService languageService,
            IWorkflowMessageService workflowMessageService,
            IDataProvider dataProvider, IDbContext dbContext,
            LocalizationSettings localizationSettings, CommonSettings commonSettings,
            IEventPublisher eventPublisher,
			IWorkContext workContext,
            ILocationService locationService)
        {
            this._cacheManager = cacheManager;
            this._productRepository = productRepository;
            this._productVariantRepository = productVariantRepository;
            this._productCategoryRepository = productCategoryRepository;
            this._relatedProductRepository = relatedProductRepository;
            this._crossSellProductRepository = crossSellProductRepository;
            this._tierPriceRepository = tierPriceRepository;
            this._productPictureRepository = productPictureRepository;
            this._localizedPropertyRepository = localizedPropertyRepository;
            this._productSpecificationAttributeRepository = productSpecificationAttributeRepository;
            this._savedSearchRepository = savedSearchRepository;
			this._portalQueries = portalQueries;
            this._productAttributeService = productAttributeService;
            this._productAttributeParser = productAttributeParser;
            this._languageService = languageService;
            this._workflowMessageService = workflowMessageService;
            this._dataProvider = dataProvider;
            this._dbContext = dbContext;
            this._localizationSettings = localizationSettings;
            this._commonSettings = commonSettings;
            this._eventPublisher = eventPublisher;
			this._workContext = workContext;
            _categoryRepository = categoryRepository;
            _locationService = locationService;
        }

        #endregion

        #region Methods

		private IQueryable<Product> ProductsInThisSite(bool excludeCorporateProducts = false) {
			var siteId = _workContext.SiteId;
			var query = _productRepository.Table;

			// corporate store or admin - everything
			if (siteId == KnownIds.Website.Corporate)
				return query;

			// zee admin - my site's products only
			if (_workContext.IsAdmin || excludeCorporateProducts)
				return query.Where(p => p.SiteId == siteId);

			// zee store - my site's products + corporate products
			return query.Where(p => p.SiteId == siteId || p.SiteId == KnownIds.Website.Corporate);
		}

		#region Products

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="product">Product</param>
        public virtual void DeleteProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            product.Deleted = true;
            //delete product
            UpdateProduct(product);

            //delete product variants
            foreach (var productVariant in product.ProductVariants)
                DeleteProductVariant(productVariant);
        }

        /// <summary>
        /// Gets all products
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product collection</returns>
        public virtual IList<Product> GetAllProducts(bool showHidden = false)
        {
            var query = from p in ProductsInThisSite()
                        orderby p.Name
						where (showHidden || p.Published)
                        && !p.Deleted
                        select p;
            var products = query.ToList();
            return products;
        }

        /// <summary>
        /// Gets all products displayed on the home page
        /// </summary>
        /// <returns>Product collection</returns>
		public virtual IList<Product> GetAllProductsDisplayedOnHomePage()
        {
            var query = from p in ProductsInThisSite()
                        orderby p.Name
                        where p.Published &&
                        !p.Deleted &&
                        p.ShowOnHomePage
                        select p;
            var products = query.ToList();
            return products;
        }

        /// <summary>
        /// Gets product
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Product</returns>
        public virtual Product GetProductById(int productId)
        {
            if (productId == 0)
                return null;

            string key = string.Format(PRODUCTS_BY_ID_KEY, productId);
            return _cacheManager.Get(key, () => _productRepository.Table.FirstOrDefault(p => p.Id == productId));
        }

        /// <summary>
        /// Get products by identifiers
        /// </summary>
        /// <param name="productIds">Product identifiers</param>
        /// <returns>Products</returns>
        public virtual IList<Product> GetProductsByIds(int[] productIds)
        {
            if (productIds == null || productIds.Length == 0)
                return new List<Product>();

			var query = from p in _productRepository.Table
                        where productIds.Contains(p.Id)
                        select p;
            var products = query.ToList();
            //sort by passed identifiers
            var sortedProducts = new List<Product>();
            foreach (int id in productIds)
            {
                var product = products.Find(x => x.Id == id);
                if (product != null)
                    sortedProducts.Add(product);
            }
            return sortedProducts;
        }

        /// <summary>
        /// Inserts a product
        /// </summary>
        /// <param name="product">Product</param>
        public virtual void InsertProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            _productRepository.Insert(product);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(product);
        }

        /// <summary>
        /// Updates the product
        /// </summary>
        /// <param name="product">Product</param>
        public virtual void UpdateProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            _productRepository.Update(product);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(product);
        }

		public virtual IPagedList<int> GetProductIds(ProductSearchOptions options) {
			var query = from p in (options.SearchAllStores ? _productRepository.Table : ProductsInThisSite(options.ExcludeCorporateProducts))
			            join v in _productVariantRepository.Table on p.Id equals v.ProductId
						where !p.Deleted && !v.Deleted
			            select new { p, v };

			var nowUtc = DateTime.UtcNow;

		    if (options.StoreNumbers != null) {
				if (options.StoreNumbers.Any()) {
					query = query.Where(q => options.StoreNumbers.Contains(q.p.StoreNumber));
				}
				else {
					return new PagedList<int>(Enumerable.Empty<int>(), 0, 1);
				}
			}

			if (options.Used.HasValue) {
				query = options.Used.Value ?
					query.Where(q => q.p.SiteId != KnownIds.Website.Corporate) :
					query.Where(q => q.p.SiteId == KnownIds.Website.Corporate);
			}
            
		    //search by keyword
		    if (!String.IsNullOrWhiteSpace(options.Keywords)) {
			    var words = options.Keywords.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
				if (options.SearchCategoryName && options.SearchDescriptions) {
				    query = from q in query
				            from pc in q.p.ProductCategories.DefaultIfEmpty()
                            where words.All(w =>
                                q.p.Name.Contains(w) ||
                                q.p.Name.Contains(w.Replace("-", "")) ||
                                q.p.Name.Contains(w.Replace("-", " ")) ||
                                q.p.Name.Replace("-", "").Contains(w) || 
                                q.p.Name.Replace("-", " ").Contains(w) || 
                                q.p.ShortDescription.Contains(w) ||
                                q.p.ShortDescription.Contains(w.Replace("-", "")) ||
                                q.p.ShortDescription.Contains(w.Replace("-", " ")) ||
                                q.p.ShortDescription.Replace("-", "").Contains(w) ||
                                q.p.ShortDescription.Replace("-", " ").Contains(w) ||
                                q.p.FullDescription.Contains(w) ||
                                q.p.FullDescription.Contains(w.Replace("-", "")) ||
                                q.p.FullDescription.Contains(w.Replace("-", " ")) ||
                                q.p.FullDescription.Replace("-", "").Contains(w) ||
                                q.p.FullDescription.Replace("-", " ").Contains(w) ||
                                pc.Category.Name.Contains(w))
				            select q;
			    }
				else if (options.SearchCategoryName) {
					query = from q in query
							from pc in q.p.ProductCategories.DefaultIfEmpty()
							where words.All(w => 
                                q.p.Name.Contains(w) ||
                                q.p.Name.Contains(w.Replace("-", "")) ||
                                q.p.Name.Contains(w.Replace("-", " ")) ||
                                q.p.Name.Replace("-", "").Contains(w) || 
                                q.p.Name.Replace("-", " ").Contains(w) ||
                                pc.Category.Name.Contains(w))
                            select q;
				}
				else if (options.SearchDescriptions) {
                    query = query.Where(q => words.All(w =>
                                q.p.Name.Contains(w) ||
                                q.p.Name.Contains(w.Replace("-", "")) ||
                                q.p.Name.Contains(w.Replace("-", " ")) ||
                                q.p.Name.Replace("-", "").Contains(w) ||
                                q.p.Name.Replace("-", " ").Contains(w) ||
                                q.p.ShortDescription.Contains(w) ||
                                q.p.ShortDescription.Contains(w.Replace("-", "")) ||
                                q.p.ShortDescription.Contains(w.Replace("-", " ")) ||
                                q.p.ShortDescription.Replace("-", "").Contains(w) ||
                                q.p.ShortDescription.Replace("-", " ").Contains(w) ||
                                q.p.FullDescription.Contains(w) ||
                                q.p.FullDescription.Contains(w.Replace("-", "")) ||
                                q.p.FullDescription.Contains(w.Replace("-", " ")) ||
                                q.p.FullDescription.Replace("-", "").Contains(w) ||
                                q.p.FullDescription.Replace("-", " ").Contains(w)));
				}
				else {
					query = query.Where(q => words.All(w => 
                                q.p.Name.Contains(w) ||
                                q.p.Name.Contains(w.Replace("-", "")) ||
                                q.p.Name.Contains(w.Replace("-", " ")) ||
                                q.p.Name.Replace("-", "").Contains(w) ||
                                q.p.Name.Replace("-", " ").Contains(w)));
				}
			}

			// search by price
		    if (options.PriceMin.HasValue || options.PriceMax.HasValue) {
			    query = from q in query
			            let price =
				            !q.v.SpecialPrice.HasValue ? q.v.Price :
					        q.v.SpecialPriceStartDateTimeUtc.HasValue && q.v.SpecialPriceStartDateTimeUtc.Value > nowUtc ? q.v.Price :
						    q.v.SpecialPriceEndDateTimeUtc.HasValue && q.v.SpecialPriceEndDateTimeUtc.Value < nowUtc ? q.v.Price :
							q.v.SpecialPrice.Value
			            where (!options.PriceMin.HasValue || price >= options.PriceMin.Value)
							&& (!options.PriceMax.HasValue || price <= options.PriceMax.Value)
			            select q;
		    }

		    //search by specs
		    if (options.FilteredSpecs != null && options.FilteredSpecs.Count > 0) {
			    query = from q in query
						where !options.FilteredSpecs
				            .Except(
					            q.p.ProductSpecificationAttributes.Where(psa => psa.AllowFiltering).Select(
						            psa => psa.SpecificationAttributeOptionId))
				            .Any()
			            select q;
		    }

		    //category filtering
			if (options.CategoryIds != null && options.CategoryIds.Any()) {
				if (options.CategoryIds.Count == 1) {
					var icategoryId = options.CategoryIds.FirstOrDefault();
				    var hasParent = _categoryRepository.Table.First(x => x.Id == icategoryId).ParentCategoryId != 0;
				    if (!hasParent) {
					    var cats = _categoryRepository.Table.Where(x => x.ParentCategoryId == icategoryId).Select(x => x.Id);
					    foreach (var c in cats) {
							options.CategoryIds.Add(c);
					    }
				    }

			    }

				query = from q in query
						from pc in q.p.ProductCategories
						where options.CategoryIds.Contains(pc.CategoryId)
						&& (!options.FeaturedProducts.HasValue || options.FeaturedProducts.Value == pc.IsFeaturedProduct)
						select q;
			}

		    if (options.ManufacturerId.HasValue && options.ManufacturerId > 0) {
			    query = from q in query
						from pm in q.p.ProductManufacturers.Where(pm => pm.ManufacturerId == options.ManufacturerId)
						where (!options.FeaturedProducts.HasValue || options.FeaturedProducts.Value == pm.IsFeaturedProduct)
			            select q;
		    }

		    if (options.ClearanceOnly) {
			    query = query.Where(q => q.v.Clearance);
		    }

			if (options.ShowOnHomePageProducts.HasValue) {
				query = query.Where(q => options.ShowOnHomePageProducts.Value == q.p.ShowOnHomePage);
			}

		    //tag filtering
		    if (options.ProductTagId.HasValue && options.ProductTagId > 0) {
			    query = from q in query
						from pt in q.p.ProductTags.Where(pt => pt.Id == options.ProductTagId)
			            select q;
		    }

			if (!options.ShowHidden) {
				query = from q in query
				        where q.p.Published &&
					        (!q.v.AvailableStartDateTimeUtc.HasValue || nowUtc >= q.v.AvailableStartDateTimeUtc) &&
					        (!q.v.AvailableEndDateTimeUtc.HasValue || nowUtc <= q.v.AvailableEndDateTimeUtc)
				        select q;
			}

			if (options.WaitingOnDelay) {
				query = query.Where(q => q.v.AvailableStartDateTimeUtc.HasValue && q.v.AvailableStartDateTimeUtc > nowUtc);
			}

			if (options.WithImages.HasValue) {
				query = options.WithImages.Value ?
					query.Where(q => q.p.ProductPictures.Any()) :
					query.Where(q => !q.p.ProductPictures.Any());
			}

			if (options.WithDescrip.HasValue) {
				query = options.WithDescrip.Value ?
					query.Where(q => q.p.FullDescription != null && q.p.FullDescription.Length > 0) :
					query.Where(q => q.p.FullDescription == null || q.p.FullDescription.Length == 0);
			}

			if (options.Published.HasValue) {
				query = options.Published.Value ?
					query.Where(q => q.p.Published) :
					query.Where(q => !q.p.Published);
			}

		    if (options.InStock.HasValue && options.InStock.Value)
		    {
		        query = query.Where(q => q.v.StockQuantity > 0 || !q.p.Name.ToLower().Contains("used") || string.IsNullOrEmpty(q.p.StoreNumber));
		    }

			query = query.Distinct();

			switch (options.SortBy) {
				case ProductSortingEnum.NameAsc:
					query = query.OrderBy(q => q.p.Name);
					break;
				case ProductSortingEnum.NameDesc:
					query = query.OrderByDescending(q => q.p.Name);
					break;
                case ProductSortingEnum.SkuAsc:
                    query = query.OrderBy(q => q.v.Sku);
                    break;
                case ProductSortingEnum.SkuDesc:
                    query = query.OrderByDescending(q => q.v.Sku);
                    break;
				case ProductSortingEnum.PriceAsc:
					query = query.OrderBy(q => q.v.Price);
					break;
				case ProductSortingEnum.PriceDesc:
					query = query.OrderByDescending(q => q.v.Price);
					break;
                case ProductSortingEnum.StoreNumAsc:
                    query = query.OrderBy(q => q.p.StoreNumber);
                    break;
                case ProductSortingEnum.StoreNumDesc:
                    query = query.OrderByDescending(q => q.p.StoreNumber);
                    break;
				case ProductSortingEnum.DateListed:
					query = query.OrderByDescending(q => q.p.CreatedOnUtc);
					break;
                case ProductSortingEnum.DistanceAsc:
			        var ip = System.Web.HttpContext.Current.Request.UserHostAddress;
                    if (System.Web.HttpContext.Current.Request.IsLocal || ip.StartsWith("192.168.1."))
                    {
                        ip = "173.11.59.141";
                    }
			        var loc = _locationService.GetLocationByIpAddress(ip);
			        if (loc == null || (loc.Latitude == 0 && loc.Longitude == 0)) break;
			        var storeNumbers = _cacheManager.Get(string.Format(CLOSEST_LOCATIONS_BY_CITY_STATE, loc.City, loc.Region), 30, () =>
			        {
			            var stores = _locationService.GetLocationsByCoordinates(loc.Latitude, loc.Longitude, 40, null);
			            return stores.Select(x => x.StoreNumber).ToList();
			        });
                    var orderedStringComparer = new OrderedAttributeComparer<string>(storeNumbers);
			        query = query.AsEnumerable().OrderBy(x => x.p.StoreNumber, orderedStringComparer).AsQueryable();

			        break;
				default: // not reachable, but future-proof it anyway
					query = query.OrderBy(q => q.p.Name);
					break;
			}

			// Treefort "enhancement" - Simplifying the method signature and I don't think we need this.

		    //get filterable specification attribute option identifier
			//var filterableSpecificationAttributeOptionIds = new List<int>();
			//if (options.LoadFilterableSpecificationAttributeOptionIds) {
			//	var querySpecs = from p in query
			//					 join psa in _productSpecificationAttributeRepository.Table on p.Id equals psa.ProductId
			//					 where psa.AllowFiltering
			//					 select psa.SpecificationAttributeOptionId;
			//	//only distinct attributes
			//	filterableSpecificationAttributeOptionIds = querySpecs
			//		.Distinct()
			//		.ToList();
			//}

			return new PagedList<int>(query.Select(q => q.p.Id), options.PageIndex, options.PageSize);
	    }

	    public virtual IPagedList<Product> SearchProducts(ProductSearchOptions options) {
		    var ids = GetProductIds(options);
			return new PagedList<Product>(ids.Select(id => _productRepository.GetById(id)), ids.PageIndex, ids.PageSize, ids.TotalCount);
	    }

        public virtual IList<int> GetHomePageProductIds(ProductSearchOptions options)
        {
            var query = from p in (options.SearchAllStores ? _productRepository.Table : ProductsInThisSite(options.ExcludeCorporateProducts))
                        join v in _productVariantRepository.Table on p.Id equals v.ProductId
                        where !p.Deleted && !v.Deleted
                        select new { p, v };


            var nowUtc = DateTime.UtcNow;
            
            //// Get products added within the last week
            //var lastWeekUtc = nowUtc.AddDays(-7);
            //query = query.Where(q => q.p.CreatedOnUtc > lastWeekUtc);

            if (options.Used.HasValue)
            {
                query = options.Used.Value ?
                    query.Where(q => q.p.SiteId != KnownIds.Website.Corporate) :
                    query.Where(q => q.p.SiteId == KnownIds.Website.Corporate);
            }

            // search by price
            if (options.PriceMin.HasValue || options.PriceMax.HasValue)
            {
                query = from q in query
                        let price =
                            !q.v.SpecialPrice.HasValue ? q.v.Price :
                            q.v.SpecialPriceStartDateTimeUtc.HasValue && q.v.SpecialPriceStartDateTimeUtc.Value > nowUtc ? q.v.Price :
                            q.v.SpecialPriceEndDateTimeUtc.HasValue && q.v.SpecialPriceEndDateTimeUtc.Value < nowUtc ? q.v.Price :
                            q.v.SpecialPrice.Value
                        where (!options.PriceMin.HasValue || price >= options.PriceMin.Value)
                            && (!options.PriceMax.HasValue || price <= options.PriceMax.Value)
                        select q;
            }


            //category filtering
            if (options.CategoryIds != null && options.CategoryIds.Any())
            {
                if (options.CategoryIds.Count == 1)
                {
                    var icategoryId = options.CategoryIds.FirstOrDefault();
                    var hasParent = _categoryRepository.Table.First(x => x.Id == icategoryId).ParentCategoryId != 0;
                    if (!hasParent)
                    {
                        var cats = _categoryRepository.Table.Where(x => x.ParentCategoryId == icategoryId).Select(x => x.Id);
                        foreach (var c in cats)
                        {
                            options.CategoryIds.Add(c);
                        }
                    }

                }

                query = from q in query
                        from pc in q.p.ProductCategories
                        where options.CategoryIds.Contains(pc.CategoryId)
                        && (!options.FeaturedProducts.HasValue || options.FeaturedProducts.Value == pc.IsFeaturedProduct)
                        select q;
            }

            if (options.ExcludeAccessories)
            {
                var accessoryCategoryIds = new[]
                {
                    KnownIds.Category.Accessories, KnownIds.Category.AccessoriesBandInstruments,
                    KnownIds.Category.AccessoriesDrumHeads, KnownIds.Category.AccessoriesDrumSticksMallets,
                    KnownIds.Category.AccessoriesGuitars, KnownIds.Category.AccessoriesGuitarStrings,
                    KnownIds.Category.AccessoriesKeyboards, KnownIds.Category.AccessoriesOther,
                    KnownIds.Category.AccessoriesPercussion, KnownIds.Category.AccessoriesProSound,
                    KnownIds.Category.AccessoriesReeds,KnownIds.Category.MusicBooksVideos
                };
                query = from q in query
                    from pc in q.p.ProductCategories
                    where !accessoryCategoryIds.Contains(pc.CategoryId)
                    select q;
            }

            if (options.ManufacturerId.HasValue && options.ManufacturerId > 0)
            {
                query = from q in query
                        from pm in q.p.ProductManufacturers.Where(pm => pm.ManufacturerId == options.ManufacturerId)
                        where (!options.FeaturedProducts.HasValue || options.FeaturedProducts.Value == pm.IsFeaturedProduct)
                        select q;
            }

            if (options.ClearanceOnly)
            {
                query = query.Where(q => q.v.Clearance);
            }

            if (options.ExcludePriceReduction)
            {
                query = query.Where(q => q.v.Clearance == false);
            }

            if (options.ShowOnHomePageProducts.HasValue)
            {
                query = query.Where(q => options.ShowOnHomePageProducts.Value == q.p.ShowOnHomePage);
            }


            if (!options.ShowHidden)
            {
                query = from q in query
                        where q.p.Published &&
                            (!q.v.AvailableStartDateTimeUtc.HasValue || nowUtc >= q.v.AvailableStartDateTimeUtc) &&
                            (!q.v.AvailableEndDateTimeUtc.HasValue || nowUtc <= q.v.AvailableEndDateTimeUtc)
                        select q;
            }

            if (options.WaitingOnDelay)
            {
                query = query.Where(q => q.v.AvailableStartDateTimeUtc.HasValue && q.v.AvailableStartDateTimeUtc > nowUtc);
            }

            if (options.WithImages.HasValue)
            {
                query = options.WithImages.Value ?
                    query.Where(q => q.p.ProductPictures.Any()) :
                    query.Where(q => !q.p.ProductPictures.Any());
            }

            if (options.WithDescrip.HasValue)
            {
                query = options.WithDescrip.Value ?
                    query.Where(q => q.p.FullDescription != null && q.p.FullDescription.Length > 0) :
                    query.Where(q => q.p.FullDescription == null || q.p.FullDescription.Length == 0);
            }

            if (options.Published.HasValue)
            {
                query = options.Published.Value ?
                    query.Where(q => q.p.Published) :
                    query.Where(q => !q.p.Published);
            }

            if (options.InStock.HasValue && options.InStock.Value)
            {
                query = query.Where(q => q.v.StockQuantity > 0 || !q.p.Name.ToLower().Contains("used") || string.IsNullOrEmpty(q.p.StoreNumber));
            }

            query = query.Distinct();
            var pcount = options.PageSize == 0 ? 8 : options.PageSize;
            // sort by publish date by default
            query = query.OrderByDescending(q => q.p.CreatedOnUtc).ThenByDescending(q => q.p.UpdatedOnUtc);
            // Return first n if not corporate
            if (!options.SearchAllStores) return query.Take(pcount).Select(q => q.p.Id).ToList();

            // Take one for each store if it's corporate
            var rnd = new Random();
            var grouped =
                query.Where(q => q.p.StoreNumber != null)
                    .GroupBy(q => q.p.StoreNumber, q => q.p.Id, (k, v) => new {StoreNumber = k, ProductIds = v});

            var selectedStores = grouped.Select(x => x.StoreNumber).ToList().OrderBy(x => rnd.Next()).Take(pcount);

            return grouped.Where(x => selectedStores.Contains(x.StoreNumber)).Select(x => x.ProductIds.FirstOrDefault()).ToList();
        }

	    /// <summary>
        /// Update product review totals
        /// </summary>
        /// <param name="product">Product</param>
        public virtual void UpdateProductReviewTotals(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            int approvedRatingSum = 0;
            int notApprovedRatingSum = 0; 
            int approvedTotalReviews = 0;
            int notApprovedTotalReviews = 0;
            var reviews = product.ProductReviews;
            foreach (var pr in reviews)
            {
                if (pr.IsApproved)
                {
                    approvedRatingSum += pr.Rating;
                    approvedTotalReviews ++;
                }
                else
                {
                    notApprovedRatingSum += pr.Rating;
                    notApprovedTotalReviews++;
                }
            }

            product.ApprovedRatingSum = approvedRatingSum;
            product.NotApprovedRatingSum = notApprovedRatingSum;
            product.ApprovedTotalReviews = approvedTotalReviews;
            product.NotApprovedTotalReviews = notApprovedTotalReviews;
            UpdateProduct(product);
        }

        #endregion

        #region Product variants
        
        /// <summary>
        /// Get low stock product variants
        /// </summary>
        /// <returns>Result</returns>
		public virtual IList<ProductVariant> GetLowStockProductVariants()
        {
            //Track inventory for product variant
            var query1 = from pv in _productVariantRepository.Table
                         orderby pv.MinStockQuantity
                         where !pv.Deleted &&
                         pv.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStock &&
                         pv.MinStockQuantity >= pv.StockQuantity
                         select pv;
            var productVariants1 = query1.ToList();
            //Track inventory for product variant by product attributes
            var query2 = from pv in _productVariantRepository.Table
                         from pvac in pv.ProductVariantAttributeCombinations
                         where !pv.Deleted &&
                         pv.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStockByAttributes &&
                         pvac.StockQuantity <= 0
                         select pv;
            //only distinct products (group by ID)
            //if we use standard Distinct() method, then all fields will be compared (low performance)
            query2 = from pv in query2
                    group pv by pv.Id into pGroup
                    orderby pGroup.Key
                    select pGroup.FirstOrDefault();
            var productVariants2 = query2.ToList();

            var result = new List<ProductVariant>();
            result.AddRange(productVariants1);
            result.AddRange(productVariants2);
            return result;
        }
        
        /// <summary>
        /// Gets a product variant
        /// </summary>
        /// <param name="productVariantId">Product variant identifier</param>
        /// <returns>Product variant</returns>
        public virtual ProductVariant GetProductVariantById(int productVariantId)
        {
            if (productVariantId == 0)
                return null;

            string key = string.Format(PRODUCTVARIANTS_BY_ID_KEY, productVariantId);
            return _cacheManager.Get(key, () =>
            {
                var pv = _productVariantRepository.GetById(productVariantId);
                return pv;
            });
        }
        
        /// <summary>
        /// Get product variants by product identifiers
        /// </summary>
        /// <param name="productIds">Product identifiers</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variants</returns>
        public virtual IList<ProductVariant> GetProductVariantsByProductIds(int[] productIds, bool showHidden = false)
        {
            if (productIds == null || productIds.Length == 0)
                return new List<ProductVariant>();

            var query = _productVariantRepository.Table;
            if (!showHidden)
            {
                //The function 'CurrentUtcDateTime' is not supported by SQL Server Compact. 
                //That's why we pass the date value
                var nowUtc = DateTime.UtcNow;
                query = query.Where(pv =>
                        !pv.AvailableStartDateTimeUtc.HasValue ||
                        pv.AvailableStartDateTimeUtc <= nowUtc);
                query = query.Where(pv =>
                        !pv.AvailableEndDateTimeUtc.HasValue ||
                        pv.AvailableEndDateTimeUtc >= nowUtc);
            }
            query = query.Where(pv => !pv.Deleted);
            query = query.Where(pv => productIds.Contains(pv.ProductId));
            query = query.OrderBy(pv => pv.DisplayOrder);

            var productVariants = query.ToList();
            return productVariants;
        }

        /// <summary>
        /// Gets a product variant by SKU
        /// </summary>
        /// <param name="sku">SKU</param>
        /// <returns>Product variant</returns>
        public virtual ProductVariant GetProductVariantBySku(string sku)
        {
            if (String.IsNullOrEmpty(sku))
                return null;

            sku = sku.Trim();

            var query = from pv in _productVariantRepository.Table
						join p in ProductsInThisSite() on pv.ProductId equals p.Id
                        where !pv.Deleted &&
                        pv.Sku == sku
                        orderby pv.DisplayOrder, pv.Id
                        select pv;
            var productVariant = query.FirstOrDefault();
            return productVariant;
        }
        
        /// <summary>
        /// Inserts a product variant
        /// </summary>
        /// <param name="productVariant">The product variant</param>
        public virtual void InsertProductVariant(ProductVariant productVariant)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            _productVariantRepository.Insert(productVariant);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(productVariant);
        }

        /// <summary>
        /// Updates the product variant
        /// </summary>
        /// <param name="productVariant">The product variant</param>
        public virtual void UpdateProductVariant(ProductVariant productVariant)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            _productVariantRepository.Update(productVariant);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(productVariant);
        }
        
        /// <summary>
        /// Gets product variants by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variant collection</returns>
        public virtual IList<ProductVariant> GetProductVariantsByProductId(int productId, bool showHidden = false)
        {
            string key = string.Format(PRODUCTVARIANTS_ALL_KEY, showHidden, productId);
            return _cacheManager.Get(key, () =>
            {
                var query = _productVariantRepository.Table;
                if (!showHidden)
                {
                    //The function 'CurrentUtcDateTime' is not supported by SQL Server Compact. 
                    //That's why we pass the date value
                    var nowUtc = DateTime.UtcNow;
                    query = query.Where(pv =>
                            !pv.AvailableStartDateTimeUtc.HasValue ||
                            pv.AvailableStartDateTimeUtc <= nowUtc);
                    query = query.Where(pv =>
                            !pv.AvailableEndDateTimeUtc.HasValue ||
                            pv.AvailableEndDateTimeUtc >= nowUtc);
                }
                query = query.Where(pv => !pv.Deleted);
                query = query.Where(pv => pv.ProductId == productId);
                query = query.OrderBy(pv => pv.DisplayOrder);

                var productVariants = query.ToList();
                return productVariants;
            });
        }

        public virtual IList<ProductVariant> GetAllProductVariantsByProductIdLastAvailableDate(int productId,
            DateTime lastAvailableDateTime, bool showHidden = false)
        {
            string key = string.Format(PRODUCTVARIANTS_ALL_AVAILABLE_KEY, showHidden, productId, lastAvailableDateTime.ToShortDateString());
            return _cacheManager.Get(key, () =>
            {
                var query = _productVariantRepository.Table;
                if (!showHidden)
                {
                    //The function 'CurrentUtcDateTime' is not supported by SQL Server Compact. 
                    //That's why we pass the date value
                    var nowUtc = DateTime.UtcNow;
                    query = query.Where(pv =>
                            !pv.AvailableStartDateTimeUtc.HasValue ||
                            pv.AvailableStartDateTimeUtc <= nowUtc);
                    query = query.Where(pv =>
                            !pv.AvailableEndDateTimeUtc.HasValue ||
                            pv.AvailableEndDateTimeUtc >= lastAvailableDateTime);
                }
                query = query.Where(pv => !pv.Deleted);
                query = query.Where(pv => pv.ProductId == productId);
                query = query.OrderBy(pv => pv.DisplayOrder);

                var productVariants = query.ToList();
                return productVariants;
            });
        }


        /// <summary>
        /// Delete a product variant
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        public virtual void DeleteProductVariant(ProductVariant productVariant)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            productVariant.Deleted = true;
            UpdateProductVariant(productVariant);
        }
        
        /// <summary>
        /// Adjusts inventory
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        /// <param name="decrease">A value indicating whether to increase or descrease product variant stock quantity</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="attributesXml">Attributes in XML format</param>
        public virtual void AdjustInventory(ProductVariant productVariant, bool decrease, int quantity, string attributesXml)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            var prevStockQuantity = productVariant.StockQuantity;

            switch (productVariant.ManageInventoryMethod)
            {
                case ManageInventoryMethod.DontManageStock:
                    {
                        //do nothing
                        return;
                    }
                case ManageInventoryMethod.ManageStock:
					productVariant.StockQuantity += (decrease ? -quantity : quantity);
		            var inventoryIsLow = (productVariant.StockQuantity < productVariant.MinStockQuantity);

	                switch (productVariant.LowStockActivity) {
						case LowStockActivity.DisableBuyButton:
							productVariant.DisableBuyButton = productVariant.DisableWishlistButton = inventoryIsLow;
			                break;
		                case LowStockActivity.Unpublish:
							var product = productVariant.Product;
							if (product.Published == inventoryIsLow) {
								product.Published = !inventoryIsLow;
								UpdateProduct(product);
							}
			                break;
	                }

					UpdateProductVariant(productVariant);

	                //send email notification
					if (decrease && productVariant.NotifyAdminForQuantityBelow > productVariant.StockQuantity)
		                _workflowMessageService.SendQuantityBelowStoreOwnerNotification(productVariant, _localizationSettings.DefaultAdminLanguageId);
		            break;
                case ManageInventoryMethod.ManageStockByAttributes:
                    {
                        var combination = _productAttributeParser.FindProductVariantAttributeCombination(productVariant, attributesXml);
                        if (combination != null)
                        {
                            int newStockQuantity = 0;
                            if (decrease)
                                newStockQuantity = combination.StockQuantity - quantity;
                            else
                                newStockQuantity = combination.StockQuantity + quantity;

                            combination.StockQuantity = newStockQuantity;
                            _productAttributeService.UpdateProductVariantAttributeCombination(combination);
                        }
                    }
                    break;
                default:
                    break;
            }

            //TODO send back in stock notifications?
            //if (productVariant.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
            //    productVariant.BackorderMode == BackorderMode.NoBackorders &&
            //    productVariant.AllowBackInStockSubscriptions &&
            //    productVariant.StockQuantity > 0 &&
            //    prevStockQuantity <= 0 &&
            //    productVariant.Published &&
            //    !productVariant.Deleted)
            //{
            //    //_backInStockSubscriptionService.SendNotificationsToSubscribers(productVariant);
            //}
        }

        /// <summary>
        /// Search product variants (used only in admin)
        /// </summary>
        /// <param name="categoryId">Category identifier; 0 to load all records</param>
        /// <param name="manufacturerId">Manufacturer identifier; 0 to load all records</param>
        /// <param name="keywords">Keywords</param>
        /// <param name="searchDescriptions">A value indicating whether to search in descriptions</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product variants</returns>
        public virtual IPagedList<ProductVariant> SearchProductVariants(int categoryId, int manufacturerId,
             string keywords, bool searchDescriptions, int pageIndex, int pageSize, bool showHidden = false)
        {
            //products
            var query = _productVariantRepository.Table;
            query = query.Where(pv => !pv.Deleted);
            query = query.Where(pv => !pv.Product.Deleted);
            if (!showHidden)
            {
                query = query.Where(pv => pv.Product.Published);
            }

            //searching by keyword
            if (!String.IsNullOrWhiteSpace(keywords))
            {
                query = from pv in query
                        where (pv.Product.Name.Contains(keywords)) ||
                        (searchDescriptions && pv.Product.ShortDescription.Contains(keywords)) ||
                        (searchDescriptions && pv.Product.FullDescription.Contains(keywords)) ||
                        (pv.Name.Contains(keywords)) ||
                        (searchDescriptions && pv.Description.Contains(keywords))
                        select pv;
            }

            //category filtering
            if (categoryId > 0)
            {
                query = from pv in query
                        from pc in pv.Product.ProductCategories.Where(pc => pc.CategoryId == categoryId)
                        select pv;
            }

            //manufacturer filtering
            if (manufacturerId > 0)
            {
                query = from pv in query
                        from pm in pv.Product.ProductManufacturers.Where(pm => pm.ManufacturerId == manufacturerId)
                        select pv;
            }

            //only distinct products (group by ID)
            //if we use standard Distinct() method, then all fields will be compared (low performance)
            //it'll not work in SQL Server Compact when searching products by a keyword)
            query = from pv in query
                    group pv by pv.Id into pvGroup
                    orderby pvGroup.Key
                    select pvGroup.FirstOrDefault();

            query = query.OrderBy(pv => pv.Product.Name).ThenBy(pv => pv.DisplayOrder);
            var productVariants = new PagedList<ProductVariant>(query, pageIndex, pageSize);
            return productVariants;
        }
        
        /// <summary>
        /// Update HasTierPrices property (used for performance optimization)
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        public virtual void UpdateHasTierPricesProperty(ProductVariant productVariant)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            productVariant.HasTierPrices = productVariant.TierPrices.Count > 0;
            UpdateProductVariant(productVariant);
        }

        /// <summary>
        /// Update HasDiscountsApplied property (used for performance optimization)
        /// </summary>
        /// <param name="productVariant">Product variant</param>
        public virtual void UpdateHasDiscountsApplied(ProductVariant productVariant)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            productVariant.HasDiscountsApplied = productVariant.AppliedDiscounts.Count > 0;
            UpdateProductVariant(productVariant);
        }

        #endregion

        #region Related products

        /// <summary>
        /// Deletes a related product
        /// </summary>
        /// <param name="relatedProduct">Related product</param>
        public virtual void DeleteRelatedProduct(RelatedProduct relatedProduct)
        {
            if (relatedProduct == null)
                throw new ArgumentNullException("relatedProduct");

            _relatedProductRepository.Delete(relatedProduct);

            //event notification
            _eventPublisher.EntityDeleted(relatedProduct);
        }

        /// <summary>
        /// Gets a related product collection by product identifier
        /// </summary>
        /// <param name="productId1">The first product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <param name="siteId">A value indicating whether to filter related products on the site id being viewed</param>
        /// <returns>Related product collection</returns>
        public virtual IList<RelatedProduct> GetRelatedProductsByProductId1(int productId1, bool showHidden = false, int? siteId = null)
        {
            var query = from rp in _relatedProductRepository.Table
                        join p in _productRepository.Table on rp.ProductId2 equals p.Id
                        where rp.ProductId1 == productId1 &&
                        !p.Deleted &&
                        (showHidden || p.Published)
                        orderby rp.DisplayOrder
                        select rp;
			if (siteId.HasValue) { query = query.Where(rp => rp.SiteId == null || rp.SiteId == siteId.Value); }
            var relatedProducts = query.ToList();

            return relatedProducts;
        }

        /// <summary>
        /// Gets a related product
        /// </summary>
        /// <param name="relatedProductId">Related product identifier</param>
        /// <returns>Related product</returns>
        public virtual RelatedProduct GetRelatedProductById(int relatedProductId)
        {
            if (relatedProductId == 0)
                return null;
            
            var relatedProduct = _relatedProductRepository.GetById(relatedProductId);
            return relatedProduct;
        }

        /// <summary>
        /// Inserts a related product
        /// </summary>
        /// <param name="relatedProduct">Related product</param>
        public virtual void InsertRelatedProduct(RelatedProduct relatedProduct)
        {
            if (relatedProduct == null)
                throw new ArgumentNullException("relatedProduct");

            _relatedProductRepository.Insert(relatedProduct);

            //event notification
            _eventPublisher.EntityInserted(relatedProduct);
        }

        /// <summary>
        /// Updates a related product
        /// </summary>
        /// <param name="relatedProduct">Related product</param>
        public virtual void UpdateRelatedProduct(RelatedProduct relatedProduct)
        {
            if (relatedProduct == null)
                throw new ArgumentNullException("relatedProduct");

            _relatedProductRepository.Update(relatedProduct);

            //event notification
            _eventPublisher.EntityUpdated(relatedProduct);
        }

        #endregion

        #region Cross-sell products

        /// <summary>
        /// Deletes a cross-sell product
        /// </summary>
        /// <param name="crossSellProduct">Cross-sell identifier</param>
        public virtual void DeleteCrossSellProduct(CrossSellProduct crossSellProduct)
        {
            if (crossSellProduct == null)
                throw new ArgumentNullException("crossSellProduct");

            _crossSellProductRepository.Delete(crossSellProduct);

            //event notification
            _eventPublisher.EntityDeleted(crossSellProduct);
        }

        /// <summary>
        /// Gets a cross-sell product collection by product identifier
        /// </summary>
        /// <param name="productId1">The first product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Cross-sell product collection</returns>
        public virtual IList<CrossSellProduct> GetCrossSellProductsByProductId1(int productId1, bool showHidden = false)
        {
            var query = from csp in _crossSellProductRepository.Table
                        join p in _productRepository.Table on csp.ProductId2 equals p.Id
                        where csp.ProductId1 == productId1 &&
                        !p.Deleted &&
                        (showHidden || p.Published)
                        orderby csp.Id
                        select csp;
            var crossSellProducts = query.ToList();
            return crossSellProducts;
        }

        /// <summary>
        /// Gets a cross-sell product
        /// </summary>
        /// <param name="crossSellProductId">Cross-sell product identifier</param>
        /// <returns>Cross-sell product</returns>
        public virtual CrossSellProduct GetCrossSellProductById(int crossSellProductId)
        {
            if (crossSellProductId == 0)
                return null;

            var crossSellProduct = _crossSellProductRepository.GetById(crossSellProductId);
            return crossSellProduct;
        }

        /// <summary>
        /// Inserts a cross-sell product
        /// </summary>
        /// <param name="crossSellProduct">Cross-sell product</param>
        public virtual void InsertCrossSellProduct(CrossSellProduct crossSellProduct)
        {
            if (crossSellProduct == null)
                throw new ArgumentNullException("crossSellProduct");

            _crossSellProductRepository.Insert(crossSellProduct);

            //event notification
            _eventPublisher.EntityInserted(crossSellProduct);
        }

        /// <summary>
        /// Updates a cross-sell product
        /// </summary>
        /// <param name="crossSellProduct">Cross-sell product</param>
        public virtual void UpdateCrossSellProduct(CrossSellProduct crossSellProduct)
        {
            if (crossSellProduct == null)
                throw new ArgumentNullException("crossSellProduct");

            _crossSellProductRepository.Update(crossSellProduct);

            //event notification
            _eventPublisher.EntityUpdated(crossSellProduct);
        }

        /// <summary>
        /// Gets a cross-sells
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <param name="numberOfProducts">Number of products to return</param>
        /// <returns>Cross-sells</returns>
        public virtual IList<Product> GetCrosssellProductsByShoppingCart(IList<ShoppingCartItem> cart, int numberOfProducts)
        {
            var result = new List<Product>();

            if (numberOfProducts == 0)
                return result;

            if (cart == null || cart.Count == 0)
                return result;

            var cartProductIds = new List<int>();
            foreach (var sci in cart)
            {
                int prodId = sci.ProductVariant.ProductId;
                if (!cartProductIds.Contains(prodId))
                    cartProductIds.Add(prodId);
            }

            foreach (var sci in cart)
            {
                var crossSells = GetCrossSellProductsByProductId1(sci.ProductVariant.ProductId);
                foreach (var crossSell in crossSells)
                {
                    //validate that this product is not added to result yet
                    //validate that this product is not in the cart
                    if (result.Find(p => p.Id == crossSell.ProductId2) == null &&
                        !cartProductIds.Contains(crossSell.ProductId2))
                    {
                        var productToAdd = GetProductById(crossSell.ProductId2);
                        //validate product
                        if (productToAdd == null || productToAdd.Deleted || !productToAdd.Published)
                            continue;
                        //at least one variant should be valid and available
                        if (GetProductVariantsByProductId(productToAdd.Id).Count == 0)
                            continue;

                        //add a product to result
                        result.Add(productToAdd);
                        if (result.Count >= numberOfProducts)
                            return result;
                    }
                }
            }
            return result;
        }
        #endregion
        
        #region Tier prices
        
        /// <summary>
        /// Deletes a tier price
        /// </summary>
        /// <param name="tierPrice">Tier price</param>
        public virtual void DeleteTierPrice(TierPrice tierPrice)
        {
            if (tierPrice == null)
                throw new ArgumentNullException("tierPrice");

            _tierPriceRepository.Delete(tierPrice);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(tierPrice);
        }

        /// <summary>
        /// Gets a tier price
        /// </summary>
        /// <param name="tierPriceId">Tier price identifier</param>
        /// <returns>Tier price</returns>
        public virtual TierPrice GetTierPriceById(int tierPriceId)
        {
            if (tierPriceId == 0)
                return null;
            
            var tierPrice = _tierPriceRepository.GetById(tierPriceId);
            return tierPrice;
        }

        /// <summary>
        /// Inserts a tier price
        /// </summary>
        /// <param name="tierPrice">Tier price</param>
        public virtual void InsertTierPrice(TierPrice tierPrice)
        {
            if (tierPrice == null)
                throw new ArgumentNullException("tierPrice");

            _tierPriceRepository.Insert(tierPrice);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(tierPrice);
        }

        /// <summary>
        /// Updates the tier price
        /// </summary>
        /// <param name="tierPrice">Tier price</param>
        public virtual void UpdateTierPrice(TierPrice tierPrice)
        {
            if (tierPrice == null)
                throw new ArgumentNullException("tierPrice");

            _tierPriceRepository.Update(tierPrice);

            _cacheManager.RemoveByPattern(PRODUCTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTVARIANTS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(TIERPRICES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(tierPrice);
        }

        #endregion

        #region Product pictures

        /// <summary>
        /// Deletes a product picture
        /// </summary>
        /// <param name="productPicture">Product picture</param>
        public virtual void DeleteProductPicture(ProductPicture productPicture)
        {
            if (productPicture == null)
                throw new ArgumentNullException("productPicture");

            _productPictureRepository.Delete(productPicture);

            //event notification
            _eventPublisher.EntityDeleted(productPicture);
        }

        /// <summary>
        /// Gets a product pictures by product identifier
        /// </summary>
        /// <param name="productId">The product identifier</param>
        /// <returns>Product pictures</returns>
        public virtual IList<ProductPicture> GetProductPicturesByProductId(int productId)
        {
            var query = from pp in _productPictureRepository.Table
                        where pp.ProductId == productId
                        orderby pp.DisplayOrder
                        select pp;
            var productPictures = query.ToList();
            return productPictures;
        }

        /// <summary>
        /// Gets a product picture
        /// </summary>
        /// <param name="productPictureId">Product picture identifier</param>
        /// <returns>Product picture</returns>
        public virtual ProductPicture GetProductPictureById(int productPictureId)
        {
            if (productPictureId == 0)
                return null;

            var pp = _productPictureRepository.GetById(productPictureId);
            return pp;
        }

        /// <summary>
        /// Inserts a product picture
        /// </summary>
        /// <param name="productPicture">Product picture</param>
        public virtual void InsertProductPicture(ProductPicture productPicture)
        {
            if (productPicture == null)
                throw new ArgumentNullException("productPicture");

            _productPictureRepository.Insert(productPicture);

            //event notification
            _eventPublisher.EntityInserted(productPicture);
        }

        /// <summary>
        /// Updates a product picture
        /// </summary>
        /// <param name="productPicture">Product picture</param>
        public virtual void UpdateProductPicture(ProductPicture productPicture)
        {
            if (productPicture == null)
                throw new ArgumentNullException("productPicture");

            _productPictureRepository.Update(productPicture);

            //event notification
            _eventPublisher.EntityUpdated(productPicture);
        }

        #endregion


        public void InsertSavedSearch(SavedSearch savedSearch)
        {
            if (savedSearch == null)
                throw new ArgumentNullException("savedSearch");

            savedSearch.CreatedOnUtc = DateTime.UtcNow;
            savedSearch.UpdatedOnUtc = DateTime.UtcNow;
            savedSearch.DeletedOnUtc = null;
            savedSearch.Slug = Guid.NewGuid().ToString();
            _savedSearchRepository.Insert(savedSearch);

            //event notification
            _eventPublisher.EntityInserted(savedSearch);
        }

        public void UpdateSavedSearch(SavedSearch savedSearch)
        {
            if (savedSearch == null)
                throw new ArgumentNullException("savedSearch");

            savedSearch.UpdatedOnUtc = DateTime.UtcNow;
            savedSearch.DeletedOnUtc = null;
            _savedSearchRepository.Update(savedSearch);

            //event notification
            _eventPublisher.EntityUpdated(savedSearch);
        }

        public void CancelSavedSearch(SavedSearch savedSearch)
        {
            if (savedSearch == null)
                throw new ArgumentNullException("savedSearch");

            savedSearch.DeletedOnUtc = DateTime.UtcNow;
            _savedSearchRepository.Update(savedSearch);
        }

        public SavedSearch GetSavedSearch(int id)
        {
            return _savedSearchRepository.GetById(id);
        }

        public SavedSearch GetSavedSearch(string slug)
        {
            if (slug == null)
                throw new ArgumentNullException("slug");

            return _savedSearchRepository.Table.SingleOrDefault(ss => ss.Slug == slug);
        }

        public IEnumerable<SavedSearch> GetSavedSearchesToNotify()
        {
            var searchList = _savedSearchRepository.Table.Where(s => s.DeletedOnUtc == null && s.StartDate <= DateTime.Today && DateTime.Today <= s.EndDate).ToList();
            searchList = searchList.Where(s => s.Frequency == "daily" || (s.Frequency == "weekly" && DateTime.Today.Subtract(s.StartDate).Days % 7 == 0)).ToList();
            return searchList;
        }

        public IEnumerable<SavedSearch> FindSavedSearches(DateTime? startDate, DateTime? endDate)
        {
            return FindSavedSearches(startDate, endDate, null);
        }

        public IEnumerable<SavedSearch> FindSavedSearches(DateTime? startDate, DateTime? endDate, int siteId)
        {
            return FindSavedSearches(startDate, endDate, siteId);
        }

        public IEnumerable<SavedSearch> FindSavedSearches(DateTime? startDate, DateTime? endDate, int? siteId)
        {
            var q = _savedSearchRepository.Table;
            if (startDate.HasValue)
                q = q.Where(ss => ss.StartDate >= startDate.Value);
            if (endDate.HasValue)
                q = q.Where(ss => ss.StartDate <= endDate.Value);
            if (siteId.HasValue)
                q = q.Where(ss => ss.SiteId == siteId.Value);
            return q;
        }

        #endregion

       
    }
}
