using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Services.Catalog;
using Nop.Services.Topics;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee.Website;

namespace Nop.Services.Seo
{
    /// <summary>
    /// Represents a sitemap generator
    /// </summary>
    public partial class SitemapGenerator : BaseSitemapGenerator, ISitemapGenerator
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ITopicService _topicService;
        private readonly CommonSettings _commonSettings;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _context;
        private readonly IHeaderViewModelQuery _headerViewModelQuery;


        public SitemapGenerator(ICategoryService categoryService,
            IProductService productService, IManufacturerService manufacturerService,
            ITopicService topicService, CommonSettings commonSettings, IWebHelper webHelper, IHeaderViewModelQuery headerViewModelQuery, IWorkContext context)
        {
            this._categoryService = categoryService;
            this._productService = productService;
            this._manufacturerService = manufacturerService;
            this._topicService = topicService;
            this._commonSettings = commonSettings;
            this._webHelper = webHelper;
            _headerViewModelQuery = headerViewModelQuery;
            _context = context;
        }

        /// <summary>
        /// Method that is overridden, that handles creation of child urls.
        /// Use the method WriteUrlLocation() within this method.
        /// </summary>
        protected override void GenerateUrlNodes()
        {
            if (_commonSettings.SitemapIncludeCategories)
            {
                WriteCategories(0);
            }

            if(KnownIds.Website.Corporate != _context.SiteId){
                var pages = _headerViewModelQuery.Execute(_context.SiteId);

                if (pages.IsVisibleAbout)
                {
                    WriteGenericPage("about-us");
                }

                if (pages.IsVisibleCommunity)
                {
                    WriteGenericPage("community-links");
                }

                if (pages.IsVisibleLessons)
                {
                    WriteGenericPage("lessons");
                }

                if (pages.IsVisibleRepairs)
                {
                    WriteGenericPage("repairs");
                }
                if (pages.IsVisibleNewGear)
                {
                    WriteGenericPage("new-gear");
                }

                if (pages.IsVisiblePromotions)
                {
                    WriteGenericPage("promotions");
                }

                if (pages.IsVisibleJobs)
                {
                    WriteGenericPage("jobs");
                }

                if (pages.IsVisibleMostWanted)
                {
                    WriteGenericPage("most-wanted");
                }
            }
            WriteGenericPage("sell-us-your-gear");

            if (KnownIds.Website.Corporate == _context.SiteId)
            {
                WriteGenericPage("locations");
                WriteGenericPage("about-us");
                WriteGenericPage("sell-us-your-gear");
                WriteGenericPage("Own");
                WriteGenericPage("privacy-policy");
                WriteGenericPage("return-policy");
                WriteGenericPage("shipping-policy");
                WriteGenericPage("conditions-of-use");
                WriteGenericPage("contact-us");
                WriteGenericPage("what-to-expect");
                WriteGenericPage("test-mobile");
            }

            //if (_commonSettings.SitemapIncludeManufacturers)
            //{
            //    WriteManufacturers();
            //}

            //if (_commonSettings.SitemapIncludeProducts)
            //{
            //    WriteProducts();
            //}

            //if (_commonSettings.SitemapIncludeTopics)
            //{
            //    WriteTopics();
            //}
        }

        private void WriteGenericPage(string pageUrl)
        {
            //TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
            var url = string.Format("{0}{1}", _webHelper.GetStoreLocation(false), pageUrl);
            var updateFrequency = UpdateFrequency.Weekly;
            WriteUrlLocation(url, updateFrequency, null);
        }

        private void WriteCategories(int parentCategoryId)
        {
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, false);
            foreach (var category in categories)
            {
                //TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
                var url = string.Format("{0}c/{1}/{2}", _webHelper.GetStoreLocation(false), category.Id, category.GetSeName());
                var updateFrequency = UpdateFrequency.Daily;
                WriteUrlLocation(url, updateFrequency, null);

                WriteCategories(category.Id);
            }
        }

        private void WriteManufacturers()
        {
            var manufacturers = _manufacturerService.GetAllManufacturers(false);
            foreach (var manufacturer in manufacturers)
            {
                //TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
                var url = string.Format("{0}m/{1}/{2}", _webHelper.GetStoreLocation(false), manufacturer.Id, manufacturer.GetSeName());
                var updateFrequency = UpdateFrequency.Weekly;
                var updateTime = manufacturer.UpdatedOnUtc;
                WriteUrlLocation(url, updateFrequency, updateTime);
            }
        }

        private void WriteProducts()
        {
            var products = _productService.GetAllProducts(false);
            foreach (var product in products)
            {
                //TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
                var url = string.Format("{0}p/{1}/{2}", _webHelper.GetStoreLocation(false), product.Id, product.GetSeName());
                var updateFrequency = UpdateFrequency.Weekly;
                var updateTime = product.UpdatedOnUtc;
                WriteUrlLocation(url, updateFrequency, updateTime);
            }
        }

        private void WriteTopics()
        {
            var topics = _topicService.GetAllTopics().ToList().FindAll(t => t.IncludeInSitemap);
            foreach (var topic in topics)
            {
                //TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
                var url = string.Format("{0}t/{1}", _webHelper.GetStoreLocation(false), topic.SystemName.ToLowerInvariant());
                var updateFrequency = UpdateFrequency.Weekly;
                var updateTime = DateTime.UtcNow;
                WriteUrlLocation(url, updateFrequency, updateTime);
            }
        }
    }
}
