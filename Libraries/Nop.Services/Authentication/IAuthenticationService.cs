using Nop.Core.Domain.Customers;

namespace Nop.Services.Authentication
{
    /// <summary>
    /// Authentication service interface
    /// </summary>
    public partial interface IAuthenticationService 
    {
        void SignIn(Customer customer, bool createPersistentCookie);
		Customer AuthenticatePortalUser(string userName, int siteId);
        void SignOut();
        Customer GetAuthenticatedUser(int siteId, bool isAdmin);
    }
}