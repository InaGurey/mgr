using System;
using System.Web;
using System.Web.Security;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;
using Treefort.Portal;

namespace Nop.Services.Authentication
{
    /// <summary>
    /// Authentication service
    /// </summary>
    public partial class FormsAuthenticationService : IAuthenticationService
    {
        private Customer _cachedUser;

        private readonly HttpContextBase _httpContext;
        private readonly ICustomerService _customerService;
        private readonly CustomerSettings _customerSettings;
        private readonly TimeSpan _expirationTimeSpan;
		private readonly IPortalQueries _portalQueries;
	    private readonly ISingleSignOnProvider _ssoProvider;

	    /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="httpContext">HTTP context</param>
        /// <param name="customerService">Customer service</param>
        /// <param name="customerSettings">Customer settings</param>
        public FormsAuthenticationService(
			HttpContextBase httpContext,
            ICustomerService customerService,
			CustomerSettings customerSettings,
			IPortalQueries portalQueries, ISingleSignOnProvider ssoProvider)
        {
            this._httpContext = httpContext;
            this._customerService = customerService;
            this._customerSettings = customerSettings;
			this._portalQueries = portalQueries;
		    _ssoProvider = ssoProvider;
		    this._expirationTimeSpan = FormsAuthentication.Timeout;
        }

        public virtual void SignIn(Customer customer, bool createPersistentCookie) {
	        SignIn(customer, createPersistentCookie, "customer");
        }

        private void SignIn(Customer customer, bool createPersistentCookie, string userData)
        {
            var now = DateTime.UtcNow.ToLocalTime();

			var ticket = new FormsAuthenticationTicket(
				1 /*version*/,
				customer.Email ?? customer.Username,
				now,
				now.Add(_expirationTimeSpan),
				createPersistentCookie,
				userData,
				FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            if (FormsAuthentication.CookieDomain != null)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }

            _httpContext.Response.Cookies.Add(cookie);
            _cachedUser = customer;
        }

		public virtual Customer AuthenticatePortalUser(string userName, int siteId) {
			var user = MakeNopUserFromPortalUser(userName, siteId);
			SignIn(user, true, "admin:" + siteId);
			return user;
		}

	    public virtual void SignOut() {
		    _cachedUser = null;
		    if (_httpContext != null && _httpContext.User != null && _httpContext.User.Identity != null && _httpContext.User.Identity.IsAuthenticated)
			    FormsAuthentication.SignOut();
	    }

	    public virtual Customer GetAuthenticatedUser(int siteId, bool isAdmin)
        {
            if (_cachedUser != null)
                return _cachedUser;

            if (_httpContext == null ||
                _httpContext.Request == null ||
                !_httpContext.Request.IsAuthenticated ||
                !(_httpContext.User.Identity is FormsIdentity))
            {
                return null;
            }

            var formsIdentity = (FormsIdentity)_httpContext.User.Identity;

			if (formsIdentity.Ticket == null)
                throw new ArgumentNullException("ticket");

            var username = formsIdentity.Ticket.Name;

            if (String.IsNullOrWhiteSpace(username))
                return null;

			var user = MakeNopUserFromPortalUser(username, siteId);

            if (user != null && user.Active && !user.Deleted && user.IsRegistered())
                _cachedUser = user;
            return _cachedUser;
        }

		private Customer MakeNopUserFromPortalUser(string username, int siteId) {
			var portalUser = _portalQueries.GetUser(username, siteId);
			if (portalUser == null)
				return null;

			return new AdminUser {
				Username = username,
				SiteId = siteId,
				AdminRole =
					portalUser.IsSuperAdmin ? AdminRole.SuperAdmin : 
					portalUser.IsCorporate ? AdminRole.Corporate : 
					portalUser.IsZee ? AdminRole.Zee :
                    portalUser.IsTrustedUser ? AdminRole.Trusted : 
					portalUser.IsContentEditor ? AdminRole.ContentEditor : 
					AdminRole.None,
				Active = true
			};
		}
	}
}