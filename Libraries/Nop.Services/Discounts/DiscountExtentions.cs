using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Discounts;
using Treefort.Portal;

namespace Nop.Services.Discounts
{
    public static class DiscountExtentions
    {
        public static Discount GetPreferredDiscount(this IList<Discount> discounts,
            decimal amount)
        {
            Discount preferredDiscount = null;
            decimal maximumDiscountValue = decimal.Zero;
            foreach (var discount in discounts)
            {
                decimal currentDiscountValue = discount.GetDiscountAmount(amount);
                if (currentDiscountValue > maximumDiscountValue)
                {
                    maximumDiscountValue = currentDiscountValue;
                    preferredDiscount = discount;
                }
            }

            return preferredDiscount;
        }

        public static bool ContainsDiscount(this IList<Discount> discounts,
            Discount discount)
        {
            if (discounts == null)
                throw new ArgumentNullException("discounts");

            if (discount == null)
                throw new ArgumentNullException("discount");

            foreach (var dis1 in discounts)
                if (discount.Id == dis1.Id)
                    return true;

            return false;
        }

        public static IEnumerable<Discount> ForStore(this IEnumerable<Discount> discounts, int siteId, bool? withGlobal = null)
        {
            withGlobal = withGlobal ?? siteId != KnownIds.Website.Corporate;
            return siteId == 0
                    ? withGlobal.Value ? discounts : discounts.Where(d => d.SiteId == 0)
                    : withGlobal.Value ? discounts.Where(d => d.SiteId == siteId || d.SiteId == 0) : discounts.Where(d => d.SiteId == siteId);
        }
    }
}
