using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Media;
using OfficeOpenXml;
using Treefort.Portal;

namespace Nop.Services.ExportImport
{
    /// <summary>
    /// Import manager
    /// </summary>
    public partial class ImportManager : IImportManager
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPictureService _pictureService;

        public ImportManager(IProductService productService, ICategoryService categoryService,
            IManufacturerService manufacturerService, IPictureService pictureService)
        {
            this._productService = productService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._pictureService = pictureService;
        }

        protected virtual int GetColumnIndex(string[] properties, string columnName)
        {
            if (properties == null)
                throw new ArgumentNullException("properties");

            if (columnName == null)
                throw new ArgumentNullException("columnName");

            for (var i = 0; i < properties.Length; i++)
                if (properties[i].Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return i + 1; //excel indexes start from 1
            return 0;
        }

        protected bool TryParse(object rawVal, out decimal val)
        {
            val = 0;
            return rawVal != null && decimal.TryParse(rawVal.ToString(), out val);
        }

        protected bool TryParse(object rawVal, out DateTime? val)
        {
            val = null;
            if (rawVal == null) return false;

            DateTime tempDate;
            if (DateTime.TryParse(rawVal.ToString(), out tempDate))
            {
                val = tempDate;
                return true;
            }

            double dblDate;
            if (!double.TryParse(rawVal.ToString(), out dblDate)) return false;
            val = DateTime.FromOADate(dblDate);
            return true;
        }

        /// <summary>
        /// Import products from XLSX file
        /// </summary>
        /// <param name="filePath">Excel file path</param>
        public virtual ImportResult ImportProductsFromXlsx(string filePath)
        {
            var result = new ImportResult();
            var newFile = new FileInfo(filePath);
            // ok, we can run the real code of the sample now
            using (var xlPackage = new ExcelPackage(newFile))
            {
                // get the first worksheet in the workbook
                var worksheet = xlPackage.Workbook.Worksheets.FirstOrDefault();
                if (worksheet == null)
                    throw new NopException("No worksheet found");

                //the columns
                var properties = new[]
                {
                    "Id",
                    "SKU",
                    "Name",
                    "CreatedOn",
                    "ShortDescription",
                    "FullDescription",
                    "Featured",
                    "Published",
                    "MPN",
                    "GTIN",
                    "Price",
                    "SpecialPrice",
                    "SpecialPriceStartDate",
                    "SpecialPriceEndDate",
                    "Clearance",
                    "DisableBuyButton",
                    "AvailableStartDate",
                    "AvailableEndDate",
                    "IsShipEnabled",
                    "IsFreeShipping",
                    "ShippingCost",
                    "Weight",
                    "Length",
                    "Width",
                    "Height",
                    "MetaTitle",
                    "MetaDescription",
                    "CategoryIds",
                    "ManufacturerIds",
                    "AdminComment"
                };

                var idxId = GetColumnIndex(properties, "Id");
                var idxSku = GetColumnIndex(properties, "SKU");
                var idxCreatedOn = GetColumnIndex(properties, "CreatedOn");
                var idxName = GetColumnIndex(properties, "Name");
                var idxDesc = GetColumnIndex(properties, "ShortDescription");
                var idxFullDesc = GetColumnIndex(properties, "FullDescription");
                var idxFeatured = GetColumnIndex(properties, "Featured");
                var idxPublished = GetColumnIndex(properties, "Published");
                var idxMpn = GetColumnIndex(properties, "MPN");
                var idxGtin = GetColumnIndex(properties, "GTIN");
                var idxPrice = GetColumnIndex(properties, "Price");
                var idxSpecialPrice = GetColumnIndex(properties, "SpecialPrice");
                var idxSpecialStart = GetColumnIndex(properties, "SpecialPriceStartDate");
                var idxSpecialEnd = GetColumnIndex(properties, "SpecialPriceEndDate");
                var idxClearance = GetColumnIndex(properties, "Clearance");
                var idxDisableBuy = GetColumnIndex(properties, "DisableBuyButton");
                var idxAvailableStart = GetColumnIndex(properties, "AvailableStartDate");
                var idxAvailableEnd = GetColumnIndex(properties, "AvailableEndDate");
                var idxShipEnabled = GetColumnIndex(properties, "IsShipEnabled");
                var idxFreeShipping = GetColumnIndex(properties, "IsFreeShipping");
                var idxShippingCost = GetColumnIndex(properties, "ShippingCost");
                var idxWeight = GetColumnIndex(properties, "Weight");
                var idxLength = GetColumnIndex(properties, "Length");
                var idxWidth = GetColumnIndex(properties, "Width");
                var idxHeight = GetColumnIndex(properties, "Height");
                var idxMetaTitle = GetColumnIndex(properties, "MetaTitle");
                var idxMetaDesc = GetColumnIndex(properties, "MetaDescription");
                var idxCatIds = GetColumnIndex(properties, "CategoryIds");
                var idxManufacturerIds = GetColumnIndex(properties, "ManufacturerIds");
                var idxAdminComment = GetColumnIndex(properties, "AdminComment");

                var criticalErrors = new List<string>();
                var warnings = new List<string>();

                var iRow = 1;
                var total = 0;
                var processed = 0;
                var utcNow = DateTime.UtcNow;
                while (true)
                {
                    //next product
                    iRow++;
                    var allColumnsAreEmpty = true;
                    for (var i = 1; i <= properties.Length; i++)
                        if (worksheet.Cells[iRow, i].Value != null &&
                            !string.IsNullOrEmpty(worksheet.Cells[iRow, i].Value.ToString()))
                        {
                            allColumnsAreEmpty = false;
                            break;
                        }
                    if (allColumnsAreEmpty)
                        break;

                    total++;
                    int variantId;
                    decimal price, shippingCost, weight, length, width, height;
                    DateTime? specialPriceStartDateTimeUtc = null,
                        specialPriceEndDateTimeUtc = null,
                        availableStartDateUtc = null,
                        availableEndDateUtc = null;

                    ProductVariant productVariant;

                    var strVariantId = worksheet.Cells[iRow, idxId].Value;
                    if (
                        !int.TryParse(strVariantId.ToString(), NumberStyles.Number,
                            CultureInfo.CurrentCulture.NumberFormat, out variantId))
                    {
                        criticalErrors.Add(string.Format("Row {0}: Invalid Id", iRow));
                        continue;
                    }
                    else
                    {
                        productVariant = _productService.GetProductVariantById(variantId);

                        // Disable inserting new products
                        if (productVariant == null)
                        {
                            criticalErrors.Add(string.Format("Row {0}: Invalid Id {1}. Product not found.", iRow, variantId));
                            continue;
                        }
                    }

                    var product = productVariant.Product;

                    var sku = worksheet.Cells[iRow, idxSku].Value as string;
                    var name = worksheet.Cells[iRow, idxName].Value as string;
                    var shortDescription = worksheet.Cells[iRow, idxDesc].Value as string;

                    productVariant.Sku = sku;
                    product.Name = name;

                    DateTime createdOn;
                    if (worksheet.Cells[iRow, idxCreatedOn] != null)
                    {
                        if (DateTime.TryParse(worksheet.Cells[iRow, idxCreatedOn].Value.ToString(), out createdOn))
                        {
                            product.CreatedOnUtc = createdOn;
                            productVariant.CreatedOnUtc = createdOn;
                        }
                    }
                    else
                    {
                        warnings.Add(string.Format("Row {0}: Invalid Created On Date", iRow));
                    }

                    product.ShortDescription = shortDescription;

                    var fullDescription = worksheet.Cells[iRow, idxFullDesc].Value as string;
                    var tagRegex = new Regex(@"<[^>]+>");
                    if (fullDescription != null && tagRegex.IsMatch(fullDescription))
                    {
                        warnings.Add(string.Format("Row {0}: Full Description contains HTML.", iRow));
                    }
                    else
                    {
                        product.FullDescription = fullDescription;
                    }

                    var featured = Convert.ToBoolean(worksheet.Cells[iRow, idxFeatured].Value);
                    var published = Convert.ToBoolean(worksheet.Cells[iRow, idxPublished].Value);
                    var mpn = worksheet.Cells[iRow, idxMpn].Value as string;
                    var gtin = worksheet.Cells[iRow, idxGtin].Value as string;

                    product.ShowOnHomePage = featured;
                    productVariant.Published = published;
                    product.Published = published;
                    productVariant.ManufacturerPartNumber = mpn;
                    productVariant.Gtin = gtin;

                    if (!TryParse(worksheet.Cells[iRow, idxPrice].Value, out price))
                    {
                        criticalErrors.Add(string.Format("Row {0}: Invalid Price", iRow));
                        continue;
                    }
                    else
                    {
                        productVariant.Price = price;
                    }

                    var specialPriceExcel = worksheet.Cells[iRow, idxSpecialPrice].Value;

                    if (specialPriceExcel != null)
                    {
                        decimal tempSpecialPrice;
                        if (!decimal.TryParse(specialPriceExcel.ToString(), out tempSpecialPrice))
                        {
                            warnings.Add(string.Format("Row {0}: Invalid Special Price", iRow));
                        }
                        else
                        {
                            decimal? specialPrice = tempSpecialPrice;
                            productVariant.SpecialPrice = specialPrice;
                        }
                    }

                    if (worksheet.Cells[iRow, idxSpecialStart].Value != null)
                    {
                        if (!TryParse(worksheet.Cells[iRow, idxSpecialStart].Value, out specialPriceStartDateTimeUtc))
                        {
                            warnings.Add(string.Format("Row {0}: Invalid Special Price Start Date", iRow));
                        }
                        else
                        {
                            productVariant.SpecialPriceStartDateTimeUtc = specialPriceStartDateTimeUtc;
                        }
                    }

                    if (worksheet.Cells[iRow, idxSpecialEnd].Value != null)
                    {
                        if (!TryParse(worksheet.Cells[iRow, idxSpecialEnd].Value, out specialPriceEndDateTimeUtc))
                        {
                            warnings.Add(string.Format("Row {0}: Invalid Special Price End Date", iRow));
                        }
                        else
                        {
                            productVariant.SpecialPriceEndDateTimeUtc = specialPriceEndDateTimeUtc;
                        }
                    }


                    var isShipEnabled = Convert.ToBoolean(worksheet.Cells[iRow, idxShipEnabled].Value);
                    var isFreeShipping = Convert.ToBoolean(worksheet.Cells[iRow, idxFreeShipping].Value);
                    productVariant.IsShipEnabled = isShipEnabled;
                    productVariant.IsFreeShipping = isFreeShipping;

                    if (!TryParse(worksheet.Cells[iRow, idxShippingCost].Value, out shippingCost))
                    {
                        warnings.Add(string.Format("Row {0}: Invalid Shipping Cost Format", iRow));
                    }
                    else
                    {
                        productVariant.FlatShippingRate = shippingCost;
                    }

                    var clearance = Convert.ToBoolean(worksheet.Cells[iRow, idxClearance].Value);
                    var disableBuyButton = Convert.ToBoolean(worksheet.Cells[iRow, idxDisableBuy].Value);

                    productVariant.Clearance = clearance;
                    productVariant.DisableBuyButton = disableBuyButton;


                    if (worksheet.Cells[iRow, idxAvailableStart].Value != null)
                    {
                        if (!TryParse(worksheet.Cells[iRow, idxAvailableStart].Value, out availableStartDateUtc))
                        {
                            warnings.Add(string.Format("Row {0}: Invalid Available Start Date", iRow));
                        }
                        else
                        {
                            productVariant.AvailableStartDateTimeUtc = availableStartDateUtc;
                        }
                    }

                    if (worksheet.Cells[iRow, idxAvailableEnd].Value != null)
                    {
                        if (!TryParse(worksheet.Cells[iRow, idxAvailableEnd].Value, out availableEndDateUtc))
                        {
                            warnings.Add(string.Format("Row {0}: Invalid Available End Date", iRow));
                        }
                        else
                        {
                            productVariant.AvailableEndDateTimeUtc = availableEndDateUtc;
                        }
                    }


                    if (!TryParse(worksheet.Cells[iRow, idxWeight].Value, out weight))
                    {
                        warnings.Add(string.Format("Row {0}: Invalid Weight Format", iRow));
                    }
                    else
                    {
                        productVariant.Weight = weight;
                    }
                    if (!TryParse(worksheet.Cells[iRow, idxLength].Value, out length))
                    {
                        warnings.Add(string.Format("Row {0}: Invalid Length Format", iRow));
                    }
                    else
                    {
                        productVariant.Length = length;
                    }
                    if (!TryParse(worksheet.Cells[iRow, idxWidth].Value, out width))
                    {
                        warnings.Add(string.Format("Row {0}: Invalid Width Format", iRow));
                    }
                    else
                    {
                        productVariant.Width = width;
                    }
                    if (!TryParse(worksheet.Cells[iRow, idxHeight].Value, out height))
                    {
                        warnings.Add(string.Format("Row {0}: Invalid Height Format", iRow));
                    }
                    else
                    {
                        productVariant.Height = height;
                    }

                    var metaTitle = worksheet.Cells[iRow, idxMetaTitle].Value as string;
                    var metaDescription = worksheet.Cells[iRow, idxMetaDesc].Value as string;
                    var categoryIds = worksheet.Cells[iRow, idxCatIds].Value as string;
                    var manufacturerIds = worksheet.Cells[iRow, idxManufacturerIds].Value as string;
                    var adminComment = worksheet.Cells[iRow, idxAdminComment].Value as string;

                    product.MetaTitle = metaTitle;
                    product.MetaDescription = metaDescription;
                    product.UpdatedOnUtc = utcNow;
                    productVariant.UpdatedOnUtc = utcNow;
                    product.AdminComment = adminComment;

                    _productService.UpdateProduct(product);
                    _productService.UpdateProductVariant(productVariant);

                    //category mappings
                    if (!string.IsNullOrEmpty(categoryIds))
                    {
                        var currentProductCategories = productVariant.Product.ProductCategories;
                        var parsedCategoryIds =
                            categoryIds.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                                .Select(x => Convert.ToInt32(x.Trim()))
                                .ToList();

                        // Insert possible new categories
                        foreach (var id in parsedCategoryIds)
                        {
                            if (currentProductCategories.FirstOrDefault(x => x.CategoryId == id) != null)
                                continue;

                            //ensure that category exists
                            var category = _categoryService.GetCategoryById(id);
                            if (category == null)
                            {
                                warnings.Add(string.Format("Row {0}: Category Id {1} not found.", iRow, id));
                                continue;
                            }

                            var productCategory = new ProductCategory()
                            {
                                ProductId = productVariant.Product.Id,
                                CategoryId = category.Id,
                                IsFeaturedProduct = false,
                                DisplayOrder = 1
                            };
                            _categoryService.InsertProductCategory(productCategory);
                        }

                        // Remove productCategories
                        foreach (var pc in currentProductCategories.Where(x => !parsedCategoryIds.Contains(x.CategoryId)))
                        {
                            _categoryService.DeleteProductCategory(pc);
                        }
                    }

                    //manufacturer mappings
                    if (!string.IsNullOrEmpty(manufacturerIds))
                    {
                        var currentProductManufacturers = productVariant.Product.ProductManufacturers;
                        var parsedManufacturerIds =
                            manufacturerIds.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                                .Select(x => Convert.ToInt32(x.Trim()))
                                .ToList();

                        foreach (var id in parsedManufacturerIds)
                        {
                            if (currentProductManufacturers.FirstOrDefault(x => x.ManufacturerId == id) != null)
                                continue;

                            //ensure that manufacturer exists
                            var manufacturer = _manufacturerService.GetManufacturerById(id);
                            if (manufacturer == null)
                            {
                                warnings.Add(string.Format("Row {0}: Manufacturer Id {1} not found.", iRow, id));
                                continue;
                            }

                            var productManufacturer = new ProductManufacturer()
                            {
                                ProductId = productVariant.Product.Id,
                                ManufacturerId = manufacturer.Id,
                                IsFeaturedProduct = false,
                                DisplayOrder = 1
                            };
                            _manufacturerService.InsertProductManufacturer(productManufacturer);
                        }

                        // Remove productCategories
                        foreach (var pm in currentProductManufacturers.Where(x => !parsedManufacturerIds.Contains(x.ManufacturerId)))
                        {
                            _manufacturerService.DeleteProductManufacturer(pm);
                        }
                    }

                    //update "HasTierPrices" and "HasDiscountsApplied" properties
                    _productService.UpdateHasTierPricesProperty(productVariant);
                    _productService.UpdateHasDiscountsApplied(productVariant);

                    processed++;
                }

                result.Processed = processed;
                result.Total = total;
                result.Errors = criticalErrors;
                result.Warnings = warnings;
            }

            return result;
        }
    }
}
