﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.ExportImport
{
    public class ImportResult
    {
        public ImportResult()
        {
            Errors = new List<string>();
            Warnings = new List<string>();
        }

        public int Processed { get; set; }
        public int Total { get; set; }

        public IList<string> Errors { get; set; }
        public IList<string> Warnings { get; set; }
    }
}
