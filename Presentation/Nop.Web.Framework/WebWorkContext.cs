﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Localization;
using Treefort.Common;
using Treefort.Portal;
using WinmarkFranchise.Domain.Websites;


namespace Nop.Web.Framework
{
    /// <summary>
    /// Working context for web application
    /// </summary>
    public partial class WebWorkContext : IWorkContext
    {
		private const string SITE_ISCOOP_KEY = "Mgr.site-iscoop-{0}";
		private const string CUSTOMER_COOKIE_NAME = "Nop.customer";

		private Customer _cachedUser;

		private readonly HttpContextBase _httpContext;
        private readonly ICustomerService _customerService;
        private readonly ILanguageService _languageService;
        private readonly TaxSettings _taxSettings;
        private readonly LocalizationSettings _localizationSettings;
	    private readonly IPortalQueries _portalQueries;
	    private readonly IMemoryCacheManager _cacheManager;
	    private readonly ISingleSignOnProvider _ssoProvider;
		private readonly IWebHelper _webHelper;
		private readonly IAuthenticationService _authenticationService;
        private readonly IWebsitePreviewCookieService _websitePreviewCookieService;
        private readonly IDomainNameService _domainNameService;

	    public WebWorkContext(HttpContextBase httpContext,
            ICustomerService customerService,
            ILanguageService languageService,
            TaxSettings taxSettings,
            LocalizationSettings localizationSettings,
            IPortalQueries portalQueries,
            IMemoryCacheManager cacheManager,
            ISingleSignOnProvider ssoProvider,
            IWebHelper webHelper,
            IAuthenticationService authenticationService,
            IWebsitePreviewCookieService websitePreviewCookieService,
            IDomainNameService domainNameService)
        {
            this._httpContext = httpContext;
            this._customerService = customerService;
            this._languageService = languageService;
            this._taxSettings = taxSettings;
            this._localizationSettings = localizationSettings;
            _portalQueries = portalQueries;
            _cacheManager = cacheManager;
            _ssoProvider = ssoProvider;
            _webHelper = webHelper;
            _authenticationService = authenticationService;
            _websitePreviewCookieService = websitePreviewCookieService;
            _domainNameService = domainNameService;
        }

	    public Customer CurrentUser {
			get {
				if (_cachedUser == null && !IsAdmin) { // admin sets the user in AdminAuthorizeAttribute. store side is lazy
					_cachedUser = GetFirstActiveUser(
						GetSearchEngineCustomer,
						GetCustomerFromSso,
						GetAuthenticatedCustomer,
						GetTempCustomerFromCookie,
						CreateTempCustomer);
				}
				return _cachedUser;
			}
			set {
				_cachedUser = value;
                SetTempCustomerCookie(_httpContext, null);
			}
	    }

	    public int SiteId {
			get {
				return
					IsAdmin ? GetSiteIdFromAuthCookie() :
                    _httpContext.Request.IsSecureConnection ? KnownIds.Website.Corporate :
                    (GetSiteIdFromQueryString() ?? GetSiteIdFromDomain() ?? _websitePreviewCookieService.PreviewWebsiteId ?? KnownIds.Website.Corporate);
            }
		}

		public bool IsAdmin {
			get { return (string)_httpContext.Request.RequestContext.RouteData.DataTokens["area"] == "Admin"; }
		}

		private bool SupportMultipleDomains {
			get { return ConfigurationManager.AppSettings["SupportMultipleDomains"] == "true"; }
		}

		public bool IsCoop {
            get {
                var siteId = this.SiteId;
                if (siteId == KnownIds.Website.Corporate) { return false; }
                var key = string.Format(SITE_ISCOOP_KEY, siteId);
                var isCoop = _cacheManager.Get(key, () => _portalQueries.IsCoop(siteId));
                return isCoop;
            }
        }

	    public ShippingOption CurrentShippingOption {
		    get {
			    return this.CurrentUser.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.LastShippingOption);
		    }
	    }

        /// <summary>
        /// Gets or sets the original customer (in case the current one is impersonated)
        /// </summary>
		public Customer OriginalCustomerIfImpersonated { get; set; }

        /// <summary>
        /// Get or set current user working language
        /// </summary>
        public Language WorkingLanguage
        {
            get
            {
                //get language from URL (if possible)
                if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
                {
                    if (_httpContext != null)
                    {
                        string virtualPath = _httpContext.Request.AppRelativeCurrentExecutionFilePath;
                        string applicationPath = _httpContext.Request.ApplicationPath;
                        if (virtualPath.IsLocalizedUrl(applicationPath, false))
                        {
                            var seoCode = virtualPath.GetLanguageSeoCodeFromUrl(applicationPath, false);
                            if (!String.IsNullOrEmpty(seoCode))
                            {
                                var langByCulture = _languageService.GetAllLanguages()
                                    .Where(l => seoCode.Equals(l.UniqueSeoCode, StringComparison.InvariantCultureIgnoreCase))
                                    .FirstOrDefault();
                                if (langByCulture != null && langByCulture.Published)
                                {
                                    //the language is found. now we need to save it
                                    if (this.CurrentUser != null &&
                                        !langByCulture.Equals(this.CurrentUser.Language))
                                    {
                                        this.CurrentUser.Language = langByCulture;
                                        _customerService.UpdateCustomer(this.CurrentUser);
                                    }
                                }
                            }
                        }
                    }
                }
                if (this.CurrentUser != null &&
                    this.CurrentUser.Language != null &&
                    this.CurrentUser.Language.Published)
                    return this.CurrentUser.Language;

                var lang = _languageService.GetAllLanguages().FirstOrDefault();
                return lang;
            }
            set
            {
                if (this.CurrentUser == null)
                    return;

                this.CurrentUser.Language = value;
                _customerService.UpdateCustomer(this.CurrentUser);
            }
        }

		// Treefort "enhancement" (best ever)
	    private static readonly Currency THE_MIGHTY_DOLLAR = new Currency {
			Id = 1,
			CreatedOnUtc = DateTime.Parse("1/1/2012"),
			CurrencyCode = "USD",
			DisplayLocale = "en-US",
			DisplayOrder = 1,
			Name = "US Dollar",
			Published = true,
			Rate = 1,
			UpdatedOnUtc = DateTime.Parse("1/1/2012")
	    };

	    /// <summary>
        /// Get or set current user working currency
        /// </summary>
        public Currency WorkingCurrency
        {
            get {
	            return THE_MIGHTY_DOLLAR;
            }
            set { }
        }

        /// <summary>
        /// Get or set current tax display type
        /// </summary>
        public TaxDisplayType TaxDisplayType
        {
            get
            {
                if (_taxSettings.AllowCustomersToSelectTaxDisplayType)
                {
                    if (this.CurrentUser != null)
                        return this.CurrentUser.TaxDisplayType;
                }

                return _taxSettings.TaxDisplayType;
            }
            set
            {
                if (!_taxSettings.AllowCustomersToSelectTaxDisplayType)
                    return;

                this.CurrentUser.TaxDisplayType = value;
                _customerService.UpdateCustomer(this.CurrentUser);
            }
        }

		private Customer GetFirstActiveUser(params Func<Customer>[] strategies) {
			return strategies.Select(get => get()).FirstOrDefault(c => c != null && c.Active && !c.Deleted);
		}

		private Customer GetSearchEngineCustomer() {
			return (_webHelper.IsSearchEngine(_httpContext)) ?
				_customerService.GetCustomerBySystemName(SystemCustomerNames.SearchEngine) : null;
		}

		private Customer GetAuthenticatedCustomer() {
			var identity = _httpContext.User.Identity as FormsIdentity;
			if (identity == null || identity.Ticket == null)
				return null;

			return _customerService.GetCustomerByEmail(identity.Ticket.Name);
		}

		private Customer GetCustomerFromSso() {
			var ssoToken = _httpContext.Request.QueryString["ssotoken"];
			if (ssoToken != null) {
				SsoData ssoData = null;
				try {
					ssoData = _ssoProvider.RetreiveToken(ssoToken);
				}
				catch (Exception) {
					return null;
				}
				var customer = _customerService.GetCustomerByGuid(ssoData.CustomerGuid);
				if (customer.IsRegistered()) {
                    SetTempCustomerCookie(_httpContext, null);
					_authenticationService.SignIn(customer, ssoData.RememberMe);
				}
				else {
					_authenticationService.SignOut();
					SetTempCustomerCookie(_httpContext, customer);
				}
				return customer;
			}
			return null;
		}

		private Customer CreateTempCustomer() {
			var customer = _customerService.InsertGuestCustomer(this.SiteId);
			SetTempCustomerCookie(_httpContext, customer);
			return customer;
		}

		private int GetSiteIdFromAuthCookie() {
			if (_httpContext != null && _httpContext.User != null) {
				var fid = _httpContext.User.Identity as FormsIdentity;
				if (fid != null) {
					var userData = fid.Ticket.UserData.Split(':');
					int siteId;
					if (userData.Length > 1 && int.TryParse(userData[1], out siteId))
						return siteId;
				}
			}
			return KnownIds.Website.Corporate;
		}

        private int? GetSiteIdFromQueryString()
        {
            if (_domainNameService.IsTestDomain(_httpContext.Request.Url)) {
                var qsWebsite = _httpContext.Request.QueryString["website"];
                int siteId;
                if (!string.IsNullOrEmpty(qsWebsite) && int.TryParse(qsWebsite, out siteId))
                    return _websitePreviewCookieService.PreviewWebsiteId = siteId;
            }

            return null;
        }

        private int? GetSiteIdFromDomain()
        {
            var url = _httpContext.Request.Url;
            var isTestDomain = _domainNameService.IsTestDomain(url);

            var name = isTestDomain
                           ? _httpContext.Request.QueryString["previewdomain"].IfNotNullOrEmpty(d => _domainNameService.GetSecondLevelDomain(d))
                           : _domainNameService.GetSecondLevelDomain(url);
            if (string.IsNullOrEmpty(name))
                return null;

            if (string.IsNullOrEmpty(name))
                return null;

            if (name.StartsWith("www."))
                name = name.Substring(4);

            var key = string.Format("Mgr.site-{0}", name);
            var siteId = _cacheManager.Get(key, () => _portalQueries.GetSiteIdFromDomainName(name));

            if (siteId.HasValue)
                _websitePreviewCookieService.PreviewWebsiteId = _domainNameService.IsTestDomain(url) ? siteId : (int?)null;

            return siteId;
        }

		private Customer GetTempCustomerFromCookie() {
			var cookie = _httpContext.Request.Cookies[CUSTOMER_COOKIE_NAME];
			if (cookie == null || string.IsNullOrEmpty(cookie.Value))
				return null;

			Guid guid;
			if (Guid.TryParse(cookie.Value, out guid)) {
				return _customerService.GetCustomerByGuid(guid);
			}
			return null;
		}

		private void SetTempCustomerCookie(HttpContextBase httpContext, Customer customer) {
            if (_httpContext.Response.Cookies[CUSTOMER_COOKIE_NAME] != null)
                httpContext.Response.Cookies.Remove(CUSTOMER_COOKIE_NAME);

            var cookie = new HttpCookie(CUSTOMER_COOKIE_NAME);
            if (customer != null && customer.CustomerGuid != Guid.Empty) {
                cookie.Value = customer.CustomerGuid.ToString();
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            else {
                cookie.Value = null;
                cookie.Expires = DateTime.Now.AddYears(-1);
            }
            httpContext.Response.Cookies.Add(cookie);
        }
    }
}
