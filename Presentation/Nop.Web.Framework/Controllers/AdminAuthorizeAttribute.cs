﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication;
using Nop.Services.Security;

namespace Nop.Web.Framework.Controllers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited=true, AllowMultiple=true)]
    public class AdminAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        private void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }

        private IEnumerable<AdminAuthorizeAttribute> GetAdminAuthorizeAttributes(ActionDescriptor descriptor)
        {
            return descriptor.GetCustomAttributes(typeof(AdminAuthorizeAttribute), true)
                .Concat(descriptor.ControllerDescriptor.GetCustomAttributes(typeof(AdminAuthorizeAttribute), true))
                .OfType<AdminAuthorizeAttribute>();
        }

        private bool IsAdminPageRequested(AuthorizationContext filterContext)
        {
            var adminAttributes = GetAdminAuthorizeAttributes(filterContext.ActionDescriptor);
            if (adminAttributes != null && adminAttributes.Any())
                return true;
            return false;
        }

	    public void OnAuthorization(AuthorizationContext filterContext) {
		    if (filterContext == null)
			    throw new ArgumentNullException("filterContext");

		    if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
			    throw new InvalidOperationException(
				    "You cannot use [AdminAuthorize] attribute when a child action cache is active");

		    if (IsAdminPageRequested(filterContext)) {
				var workContext = EngineContext.Current.Resolve<IWorkContext>();
			    var authService = EngineContext.Current.Resolve<IAuthenticationService>();
			    var ssoProvider = EngineContext.Current.Resolve<ISingleSignOnProvider>();
				var permissionService = EngineContext.Current.Resolve<IPermissionService>();

				var ssoToken = filterContext.HttpContext.Request.QueryString["token"];
				if (ssoToken != null) {
					var ssoData = ssoProvider.RetreiveToken(ssoToken);
					if (!ssoData.SiteId.HasValue)
						throw new Exception("invalid sso token. missing SiteId.");

				    workContext.CurrentUser = authService.AuthenticatePortalUser(ssoData.Username, ssoData.SiteId.Value);
				    filterContext.Result = new RedirectResult("/admin");
			    }
			    else {
				    workContext.CurrentUser = authService.GetAuthenticatedUser(workContext.SiteId, true);
			    }

				if (workContext.CurrentUser == null)
					HandleUnauthorizedRequest(filterContext);

				if (workContext.IsAdmin && !permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
				    HandleUnauthorizedRequest(filterContext);
		    }
	    }
    }
}
