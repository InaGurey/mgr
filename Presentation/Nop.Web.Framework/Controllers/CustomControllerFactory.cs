﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;



namespace Nop.Web.Framework.Controllers
{
    public class CustomControllerFactory : DefaultControllerFactory
    {
        // http://sapoval.org/per-action-custom-sessionstateattribute
        protected override SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null) { return SessionStateBehavior.Default; }
            var actionName = requestContext.RouteData.Values["action"].ToString();
            MethodInfo actionMethodInfo;

            try {
                actionMethodInfo = controllerType.GetMethod(actionName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            }
            catch (AmbiguousMatchException ex) {
                var httpRequestTypeAttr =
                    requestContext.HttpContext.Request.RequestType.Equals("POST")
                        ? typeof(HttpPostAttribute)
                        : typeof(HttpGetAttribute);

                actionMethodInfo = controllerType.GetMethods().FirstOrDefault(mi =>
                                        mi.Name.Equals(actionName, StringComparison.CurrentCultureIgnoreCase)
                                        && mi.GetCustomAttributes(httpRequestTypeAttr, false).Length > 0);
            }

            if (actionMethodInfo != null) {
                var actionSessionStateAttr = actionMethodInfo.GetCustomAttributes(typeof(ActionSessionStateAttribute), false)
                                                 .OfType<ActionSessionStateAttribute>()
                                                 .FirstOrDefault();
                if (actionSessionStateAttr != null) { return actionSessionStateAttr.Behavior; }
            }

            return base.GetControllerSessionBehavior(requestContext, controllerType);
        }


        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            var ic = base.CreateController(requestContext, controllerName);
            var c = ic as Controller;

            if (ic != null && c != null) {
                var sessionBehavior = this.GetControllerSessionBehavior(requestContext, ic.GetType());
                if (sessionBehavior == SessionStateBehavior.Disabled || sessionBehavior == SessionStateBehavior.Default) {
                    c.TempDataProvider = new CookieTempDataProvider();
                }
            }
            return ic;
        }
    }
}
