﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace Nop.Web.Framework.UI
{
    public static class DataListExtensions
    {
        public static IHtmlString DataList<T>(this HtmlHelper helper, IEnumerable<T> items, int columns,
                                              Func<T, HelperResult> template)
            where T : class
        {
            return DataList(helper, items, columns, template, null);
        }

        public static IHtmlString DataList<T>(this HtmlHelper helper, IEnumerable<T> items, int columns,
            Func<T, HelperResult> template, Func<T, HelperResult> specialTemplate) 
            where T : class
        {
            if (items == null && specialTemplate == null)
                return new HtmlString("");

            var sb = new StringBuilder();
            sb.Append("<table role=\"presentation\">");

            int cellIndex = 0;
            bool specialTemplateWritten = false;

            foreach (T item in items)
            {
                if (specialTemplate != null && !specialTemplateWritten && cellIndex + 1 == columns) {
                    cellIndex = WriteCell(sb, cellIndex, columns, specialTemplate(null).ToHtmlString());
                    specialTemplateWritten = true;
                }

                cellIndex = WriteCell(sb, cellIndex, columns, template(item).ToHtmlString());
            }

            if (cellIndex != 0)
            {
                for (; cellIndex < columns; cellIndex++)
                {
                    if (specialTemplate != null && !specialTemplateWritten && cellIndex + 1 == columns) {
                        sb.Append("<td>");
                        sb.Append(specialTemplate(null).ToHtmlString());
                        sb.Append("</td>");
                        specialTemplateWritten = true;
                    }
                    else 
                        sb.Append("<td>&nbsp;</td>");
                }

                sb.Append("</tr>");
            }

            if (specialTemplate != null && !specialTemplateWritten) {
                sb.Append("<td>");
                sb.Append(specialTemplate(null).ToHtmlString());
                sb.Append("</td>");
                specialTemplateWritten = true;
                for (; cellIndex < columns; cellIndex++)
                    sb.Append("<td>&nbsp;</td>");
            }

            sb.Append("</table>");

            return new HtmlString(sb.ToString());
        }

        private static int WriteCell(StringBuilder sb, int cellIndex, int columns, string template)
        {
            if (cellIndex == 0)
                sb.Append("<tr>");

            cellIndex++;

            sb.Append("<td>");
            sb.Append(template);
            sb.Append("</td>");

            if (cellIndex == columns) {
                cellIndex = 0;
                sb.Append("</tr>");
            }

            return cellIndex;
        }
    }
}
