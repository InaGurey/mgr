﻿using System;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Security;
using Nop.Core.Infrastructure;

namespace Nop.Web.Framework.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class NopHttpsRequirementAttribute : FilterAttribute, IAuthorizationFilter
    {
	    public NopHttpsRequirementAttribute(SslRequirement sslRequirement) {
		    this.SslRequirement = sslRequirement;
	    }

	    public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            //don't apply filter to child methods
            if (filterContext.IsChildAction)
                return;
            
            // only redirect for GET requests, 
            // otherwise the browser might not propagate the verb and request body correctly.
            if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                return;

            var currentConnectionSecured = filterContext.HttpContext.Request.IsSecureConnection;
            //currentConnectionSecured = webHelper.IsCurrentConnectionSecured();


            if (!DataSettingsHelper.DatabaseIsInstalled())
                return;
            var securitySettings = EngineContext.Current.Resolve<SecuritySettings>();
            if (securitySettings.ForceSslForAllPages)
                //all pages are forced to be SSL no matter of the specified value
                this.SslRequirement = SslRequirement.Yes;

            switch (this.SslRequirement)
            {
                case SslRequirement.Yes:
                    {
                        if (!currentConnectionSecured)
                        {
							// switching from HTTP to HTTPS
                            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
                            if (webHelper.SslEnabled())
                            {
                                string url = webHelper.GetThisPageUrl(true, true);
	                            filterContext.Result = RedirectWithSsoToken(url);
                            }
                        }
                    }
                    break;
                case SslRequirement.No:
                    {
                        if (currentConnectionSecured)
                        {
							//switching from HTTPS to HTTP
                            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
                            var url = webHelper.GetThisPageUrl(true, false);
                            filterContext.Result = RedirectWithSsoToken(url);
                        }
                    }
                    break;
                case SslRequirement.NoMatter:
                    {
                        //do nothing
                    }
                    break;
                default:
                    throw new NopException("Not supported SslProtected parameter");
            }
        }

        public SslRequirement SslRequirement { get; set; }

		private ActionResult RedirectWithSsoToken(string url) {
			var workContext = EngineContext.Current.Resolve<IWorkContext>();
			if (workContext.CurrentUser != null) {
				var sso = EngineContext.Current.Resolve<ISingleSignOnProvider>();
				var token = sso.CreateToken(workContext.CurrentUser, false);
				url += url.Contains("?") ? "&" : "?";
				url += "ssotoken=" + token;
			}
			return new RedirectResult(url);
		}
    }
}
