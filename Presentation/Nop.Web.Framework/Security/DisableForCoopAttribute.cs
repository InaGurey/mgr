﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Infrastructure;

namespace Nop.Web.Framework.Security
{
    public class DisableForCoopAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = EngineContext.Current.Resolve<IWorkContext>();

            if (context.IsCoop)
            {
                if (filterContext.IsChildAction || filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new EmptyResult();
                }
                else
                {
                    filterContext.Result = new RedirectResult("/");
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
