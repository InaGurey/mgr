﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace Nop.Web.Framework
{
	public class GoogleAnalytics
	{
		public class Tracker
		{
			public string AccountId { get; set; }
			public string Name { get; set; }
		}

		public IList<Tracker> Trackers { get; private set; }
		public IList<object[]> Commands { get; private set; }

		public GoogleAnalytics() {
			Trackers = new List<Tracker>();
			Commands = new List<object[]>();
		}

		/// <summary>
		/// AddCommand will ensure the tracker exists, so no need to call this if you're going to call AddCommand with the same account.
		/// </summary>
		public Tracker CreateTracker(string accountId) {
			var tracker = Trackers.FirstOrDefault(t => t.AccountId == accountId);
			if (tracker == null) {
				Trackers.Add(tracker = new Tracker {
					AccountId = accountId,
					Name = "t" + Trackers.Count
				});
				Commands.Add(tracker == Trackers[0] ?
					new object[] { "create", tracker.AccountId, "auto", new { allowLinker = true } } :
					new object[] { "create", tracker.AccountId, "auto", new { name = tracker.Name } });
			}
			return tracker;
		}

		/// <summary>
		/// Adds a ga command, first ensuring a tracker for the account exists.
		/// </summary>
		public void AddCommand(string accountId, string command, params object[] args) {
			var tracker = CreateTracker(accountId);
			if (tracker != Trackers[0])
				command = tracker.Name + "." + command;

			var allArgs = new object[args.Length + 1];
			allArgs[0] = command;
			Array.Copy(args, 0, allArgs, 1, args.Length);
			Commands.Add(allArgs);
		}

		/// <summary>
		/// Makes a js array of all commands args (array of arrays, including tracker creates), which can be spun through and called via ga.apply
		/// </summary>
		public string ToJsArray() {
			return new JavaScriptSerializer().Serialize(this.Commands.OrderBy(SortOrder));
		}

		private int SortOrder(object[] commandArgs) {
			var cmd = commandArgs[0].ToString().Split('.').Last();
			switch (cmd) {
				case "create": return 1;
				case "require": return 2;
				case "send": return 999;
				default: return 3;
			}
		}
	}
}
