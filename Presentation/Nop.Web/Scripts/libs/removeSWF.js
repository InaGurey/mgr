﻿/* Cross-browser SWF removal
- Especially needed to safely and completely remove a SWF in Internet Explorer
*/
function removeSWF(id) {
    var obj = document.getElementById(id);
    if (obj && obj.nodeName == "OBJECT") {
        if ($.browser.msie) {
            obj.style.display = "none";
            (function () {
                if (obj.readyState == 4) {
                    removeObjectInIE(id);
                }
                else {
                    setTimeout(arguments.callee, 10);
                }
            })();
        }
        else {
            obj.parentNode.removeChild(obj);
        }
    }
}

function removeObjectInIE(id) {
    var obj = document.getElementById(id);
    if (obj) {
        for (var i in obj) {
            if (typeof obj[i] == "function") {
                obj[i] = null;
            }
        }
        obj.parentNode.removeChild(obj);
    }
}