﻿var app = new angular.module('mgr', []);
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);
app.service('globalServices', ["$timeout", function ($timeout) {
    var serviceObj = {
        advancedSearchService: {
            searchTerm: null,
            showHomepageSearch: false
        },
        locationSearchResultService: {
            show: false,
            locations: []
        }
    };
    this.advancedSearchService = serviceObj.advancedSearchService;
    this.locationSearchResultService = serviceObj.locationSearchResultService;
    this.shoppingCartService = serviceObj.shoppingCartService;
}]);

app.factory('searchService', ["$http", "$q", function ($http, $q) {
    var service = {};

    service.currentModel = null;
    var initializing = false;
    var extendModel = function (orig, imp) {
        orig.Q = imp.Q;
        orig.Cid = imp.Cid;
        orig.Isc = imp.Isc;
        orig.Sn = imp.Sn;
        orig.AllStores = imp.AllStores;
        orig.Cl = imp.Cl;
        orig.Mid = imp.Mid;
        orig.Pf = imp.Pf;
        orig.Pt = imp.Pt;
        orig.Dca = imp.Dca;
        orig.Sid = imp.Sid;
        orig.Zip = imp.Zip;
        orig.Dist = imp.DIst;
        orig.SortByClosest = imp.SortByClosest;
    }

    service.extendModel = extendModel;
    service.initializeModel = function () {
        var deferred = $q.defer();
        if (service.currentModel != null) {
            deferred.resolve(service.currentModel);
        }else if (initializing) {
            deferred.resolve(service.currentModel);
        } else {
            initializing = true;
            $http({
                method: 'POST',
                url: '/Catalog/GetAdvancedSearchModel'
            }).then(function successCallback(response) {
                service.currentModel = response.data;
                deferred.resolve(service.currentModel);
            }, function errorCallback(response) {
                deferred.reject('Error getting search model');
            });
            initializing = false;
        }
        return deferred.promise;
    }
    return service;
}]);

app.factory('shoppingCartService', [
    '$http', '$q', function($http, $q) {
        var service = {
            displayTax: false,
            items: [],
            productCount: 0,
            orderTotal: '',
            requiresShipping: false,
            shipping: '',
            subtotal: '',
            tax: '',
            orderTotalDiscount: ''
        };

        var updateTotalsViewModel = function(model) {
            service.displayTax = model.DisplayTax;
            service.requiresShipping = model.RequiresShipping;
            service.shipping = model.Shipping;
            service.subtotal = model.SubTotal;
            service.tax = model.Tax;
            service.orderTotalDiscount = model.OrderTotalDiscount;
            service.orderTotal = model.OrderTotal;
        }
        var updateViewModel = function(model) {
            service.items = model.Items;
            service.productCount = service.items.length;
        }

        service.updateOrderTotals = function(isEditable) {
            if (isEditable === undefined) {
                isEditable = false;
            }
            return $http({
                method: 'POST',
                url: '/ShoppingCart/GetOrderTotals',
                data: { isEditable: isEditable }
            })
            .then(function successCallback(response) {
                if (response.data != null) {
                    updateTotalsViewModel(response.data);
                }
            });
        }

        service.removeProductVariantFromCart = function(sciId, pvId, isEditable) {
            return $http({
                    method: 'POST',
                    url: '/ShoppingCart/RemoveProductVariantFromCart',
                    data: { shoppingCartId: sciId, productVariantId: pvId }
                }
            ).then(function successCallback(response) {
                if (response.data != null) {
                    updateViewModel(response.data);
                    return service.updateOrderTotals(isEditable);
                }
            });
        };

        service.updateProductVariantQuantity = function(sciId, pvId, qty, isEditable) {
            var deferred = $q.defer();
            if (qty < 1) {
                deferred.reject('Quantity cannot be less than 1');
                return deferred.promise;
            }

            return $http({
                method: 'POST',
                url: '/ShoppingCart/UpdateProductVariantQuantity',
                data: { shoppingCartId: sciId, productVariantId: pvId, quantity: qty }
            }).then(function successCallback(response) {
                if (response.data != null) {
                    if (response.data.Warnings.length > 0) {
                        displayBarNotification(response.data.Warnings[0], 'error', 0);
                    }
                    updateViewModel(response.data);
                    return service.updateOrderTotals(isEditable);
                }
            });
        }

        service.updateCart = function() {
            return $http({
                method: 'GET',
                url: '/ShoppingCart/GetCartAsync'
            }).then(function successCallback(response) {
                updateViewModel(response.data);
            });
        }

        service.addProductToCart = function(pvId) {
            return $http({
                method: 'POST',
                url: '/addproductvarianttocart/' + pvId + '/1'
            }).then(function successCallback(response) {
                displayBarNotification(response.data.message, 'success', 3500);
                return $q.all([service.updateCart(), service.updateOrderTotals(false)]);
            });
        }

        return service;
    }
]);

app.directive('mgrLocationSearch', [function () {
    return {
        templateUrl: '/Locations/FindLocationBox',
        scope: {
            buttonLabel: '=label',
            placeHolderText: '=placeHolderText',
            useAsync: '=useAsync',
            actionUrl: '=actionUrl',
            layout: '=layout',
            formId: '=formId'
        }
    }
}]);

app.directive('mgrConstantContact', ['$http', function ($http) {
    return {
        templateUrl: '/Locations/ConstantContactForm',
        controller: ['$scope', function ($scope) {
            window.cc = $scope;
            $scope.regions = [];
            $scope.selectedRegion = "";
            $scope.selectedStore = "";
            $scope.locations = [];

            $http.post("/Locations/ConstantContactLocations").success(function (data) {
                var jsonData = angular.fromJson(data);

                $scope.regions = jsonData.Regions;
                $scope.locations = jsonData.Locations;
            });

            $scope.locationsByRegion = function () {
                var filteredLocations = [];
                angular.forEach($scope.locations, function (val, key) {
                    if (val.Region == $scope.selectedRegion)
                        this.push(val);
                }, filteredLocations);

                return filteredLocations;
            }

            $scope.resetStores = function () {
                $scope.selectedStore = "";
            }
        }]
    }
}]);

app.directive('pager', [function () {
    return {
        templateUrl: '/Catalog/Pagers',
        scope: {
            control: '=',
            onChange: '&onChange'
        },
        link: function (scope, element, attrs) {
            scope.internalControl = scope.control;
            scope.internalControl.updateShown = function () {
                if (scope.internalControl.totalPages <= scope.internalControl.maxShown) {

                    scope.internalControl.currentMin = 0;
                    scope.internalControl.currentMax = scope.internalControl.maxShown + 1;
                } else {
                    var pad = scope.internalControl.maxShown / 2;
                    scope.internalControl.currentMin = scope.internalControl.pageNumber - pad;
                    scope.internalControl.currentMax = scope.internalControl.pageNumber + pad;

                    if (scope.internalControl.currentMin < 1) {
                        scope.internalControl.currentMax = scope.internalControl.currentMax + 1 - scope.internalControl.currentMin;
                    }
                    if (scope.internalControl.currentMax > scope.internalControl.totalPages) {
                        scope.internalControl.curretMin = scope.internalControl.currentMin - scope.internalControl.currentMax + scope.internalControl.totalPages;
                    }
                }
            }
            scope.internalControl.changePage = function (ev, pn) {
                ev.preventDefault();
                scope.internalControl.pageNumber = pn;
                scope.internalControl.updateShown();
                scope.onChange({ ev: ev, pn: pn });
            }
            scope.internalControl.nextPage = function (ev) {
                if (scope.internalControl.pageNumber < scope.internalControl.totalPages) {
                    scope.internalControl.changePage(ev, scope.internalControl.pageNumber + 1);
                }
            }
            scope.internalControl.prevPage = function (ev) {
                if (scope.internalControl.pageNumber > 1) {
                    scope.internalControl.changePage(ev, scope.internalControl.pageNumber - 1);
                }
            }
            scope.generateRange = function (min, max, step) {
                step = step || 1;
                var rng = [];
                for (var i = min; i <= max; i += step) {
                    rng.push(i);
                }
                return rng;
            };
            scope.internalControl.initPager = function () {
                scope.internalControl.range = scope.generateRange(1, scope.internalControl.totalPages);
                scope.internalControl.updateShown();
            }

            scope.internalControl.initPager();
        }
    }
}]);

app.directive('mgrAdvancedSearch', ["globalServices", "$timeout", function(globalServices, $timeout) {
    return {
        templateUrl: '/Catalog/AdvancedSearch',
        scope: {
            containerId: '=',
            currentForm: '=',
            search: '=',
            submitForm: '&',
            ignoreSearchTerm: '='
        },
        link: function (scope, element, attrs) {
            scope.formIdSeed = Math.random();
            scope.isCategoryVisible = function(cId) {
                return scope.selectedManufacturer.BrandListIds == null || scope.selectedManufacturer.BrandListIds.indexOf(cId) > -1;
            }
            scope.changeCategory = function(category) {
                scope.selectedCategory = category;
                scope.search.Cid = category.Value;
            }
            scope.changeMainCategory = function (mainCategoryId, categoryId) {
                if (scope.selectedMainCategory.Value == mainCategoryId) return;
                for (var i = 0; i < scope.search.CategoryOptions.length; i++) {
                    if (scope.search.CategoryOptions[i].Value == mainCategoryId) {
                        scope.selectedMainCategory = scope.search.CategoryOptions[i];
                        scope.changeCategory(scope.selectedMainCategory.AvailableCategories[0]);
                        break;
                    }
                }
                scope.changeManufacturer(scope.defaultManufacturer);
            }
            scope.changePrice = function (min, max) {
                scope.search.Pf = min;
                scope.search.Pt = max;
            }
            scope.changeManufacturer = function(mn) {
                scope.selectedManufacturer = mn;
                scope.search.Mid = mn.Value;
            }
            scope.resetScrollbar = function (last, sl) {
                if (last) {
                    $(element).find(sl).initCustomNano();
                }
            }
            scope.onModelLoaded = function () {
                if (!scope.search.Cid || scope.search.Cid == 0) {
                    scope.changeMainCategory(0);
                } else {
                    for (var i = 0; i < scope.search.CategoryOptions.length; i++) {
                        for (var j = 0; j < scope.search.CategoryOptions[i].AvailableCategories.length; j++) {
                            if (scope.search.CategoryOptions[i].AvailableCategories[j].Value == scope.search.Cid) {
                                scope.selectedMainCategory = scope.search.CategoryOptions[i];
                                scope.changeCategory(scope.search.CategoryOptions[i].AvailableCategories[j]);
                                break;
                            }
                        }
                    }
                }
                if (!scope.search.Mid || scope.search.Mid == 0) {
                    scope.defaultManufacturer = scope.search.AvailableManufacturers[0];
                    scope.changeManufacturer(scope.defaultManufacturer);
                }
            };
            
            scope.gsAdv = globalServices.advancedSearchService;

            scope.selectedMainCategory = {};
            scope.selectedManufacturer = {};
            scope.defaultManufacturer = {};
            scope.availableSubCategories = [];
            scope.submitText = attrs.submitText;
            scope.homepageSearch = attrs.homepageSearch;

            if (!scope.currentForm) {
                scope.currentForm = {};
            }
            scope.currentForm.onModelLoaded = scope.onModelLoaded;
            scope.currentForm.defaultSubmit = function(ev) {
                ev.preventDefault();

                if (scope.search.Sn == 'default') {
                    scope.search.Sn = '';
                }
                var el = element;
                el.find('form')[0].submit();
            }

            if (scope.search != null) {
                scope.onModelLoaded();
            } else {
                var watchSearch = scope.$watch("search", function (nv, ov) {
                    if (scope.search != null) {
                        watchSearch();
                    }
                    else if (nv != null) {
                        scope.onModelLoaded();
                        watchSearch();
                    }
                });
            }
            var nanoScrollerWatch = scope.$watch("gsAdv.showHomepageSearch", function (nv) {
                if (nv) {
                    $timeout(function() {
                        $(element).find('.nano').initCustomNano();
                        nanoScrollerWatch();
                    }, 500);
                }
            });
            var cidWatch = scope.$watch("search.Cid", function(nv) {
                $(element).find('.brand-panel .nano').initCustomNano();
            });
            if (typeof ignoreSearchTerm === "undefined" || !ignoreSearchTerm) {
                var searchTermWatch = scope.$watch("gsAdv.searchTerm", function (nv) {
                    if (scope.search != null && nv) {
                        scope.search.Q = nv;
                    }
                });
            }
        }
    }
}]);

app.controller("HelperCtrl", ['$scope', 'globalServices', 'shoppingCartService', function ($scope, globalServices, shoppingCartService) {
    window.ngHelperScope = $scope;
    $scope.advancedSearchService = globalServices.advancedSearchService;
    $scope.locationSearchResultService = globalServices.locationSearchResultService;
    $scope.shoppingCartService = shoppingCartService;
    $scope.updateSearchTerm = function(sbSelector) {
        $scope.advancedSearchService.searchTerm = $(sbSelector).val();
    }
    $scope.toggleHomeAdvancedSearch = function(callback) {
        if ($scope.advancedSearchService.showHomepageSearch) {
            $scope.hideHomeAdvancedSearch(callback);
        } else {
            $scope.showHomeAdvancedSearch(callback);
        }
    }
    $scope.showHomeAdvancedSearch = function (callback) {
        $scope.locationSearchResultService.show = false;
        $scope.advancedSearchService.showHomepageSearch = true;

        $('.one-line-search-bar input, .one-line-search-bar label, .one-line-search-bar button').attr('disabled', 'disabled');
        if (typeof callback == "function") {
            callback($scope.locationSearchResultService.show);
        }
    }
    $scope.hideHomeAdvancedSearch = function (callback) {
        $scope.advancedSearchService.showHomepageSearch = false;
        $('.one-line-search-bar input, .one-line-search-bar label, .one-line-search-bar button').removeAttr('disabled');
        if (typeof callback == "function") {
            callback($scope.locationSearchResultService.show);
        }
    }
    $scope.scrollIntoElement = function (selector) {
        $('body').animate({
            scrollTop: $(selector).parent().offset().top
        }, 500);
    }
    $scope.addProductToCart = function (pvId) {
        showSpinner();
        $scope.shoppingCartService.addProductToCart(pvId)
            .finally(hideSpinner);
    }
    $scope.updateOrderTotals = function () {
        showSpinner();
        $scope.shoppingCartService.updateOrderTotals()
            .finally(hideSpinner);
    }
}]);

app.controller("LocationCtrl", ['$scope', '$http', 'globalServices', function ($scope, $http, globalServices) {
    window.locationSearchScope = $scope;
    $scope.search = {
        SearchedLocation: '',
        Lat: 0.0,
        Lon: 0.0,
        GeocodeShortName: '',
        GeocodeType: '',
        HCheck: ''
    };

    $scope.searchResult = globalServices.locationSearchResultService;

    $scope.loadMap = function (centerLoc, titles, addresses) {
        var mapElement = document.getElementsByClassName('google-map-result')[0];

        var gmap = new google.maps.Map(mapElement, {
            center: centerLoc,
            zoom: 7
        });

        var markers = [];
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();

        var geocodeCallback = function(rn) {
            var cb = function(data, status) {
                var p = data.results[0].geometry.location;
                var latlng = new google.maps.LatLng(p.lat, p.lng);

                var marker = new google.maps.Marker({
                    map: gmap,
                    place: {
                        placeId: data.results[0].place_id,
                        location: p
                    }
                });

                google.maps.event.addListener(marker, 'click', function () {
                    var content = '<div style="overflow:hidden; max-width: 125px;">' + data.results[0].formatted_address + '</div>';
                    infowindow.setContent(content);
                    infowindow.open(gmap, this);
                });

                markers.push(marker);
                bounds.extend(latlng);
                gmap.fitBounds(bounds);
            };
            return cb;
        };

        for (var i = 0; i < addresses.length; i++) {
            $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + addresses[i] + '&sensor=false', geocodeCallback(i));
        }
    }

    $scope.findStoresAsync = function (ev, centerLoc) {
        $http({
            method: 'POST',
            url: ev.target.action,
            data: $scope.search
        }).then(function successCallback(response) {

            var locationsResult = [];
            for (var i = 0; i < response.data.Locations.length; i++) {
                var lr = response.data.Locations[i];
                if (!lr.IsComingSoon) {
                    locationsResult.push(lr);
                }
                if (locationsResult.length >= 4) {
                    break;
                }
            }
            $scope.searchResult.locations = locationsResult;
            $scope.searchResult.show = true;
            globalServices.advancedSearchService.show = false;

            var titles = [];
            var addresses = [];
            for (var j = 0; j < locationsResult.length; j++) {
                var loc = locationsResult[j];
                titles.push("Music Go Round " + loc.Name);
                addresses.push(loc.AddressLine1 + ", " + loc.City + ", " + loc.Region + " " + loc.PostalCode);
            }
            $scope.loadMap(centerLoc.geometry.location, titles, addresses);
        }, function errorCallback(response) {

        });
    }

    $scope.findLocation = function (ev) {
        ev.preventDefault();

        var geocoder = new google.maps.Geocoder();
        if ($scope.search.HCheck != '')
            return;

        if ($scope.search.SearchedLocation == '') {
            alert("You need to enter a location to search");
            return;
        }

        geocoder.geocode({ 'address': $scope.search.SearchedLocation }, function (results, status) {
            console.log("Results " + results + " Status " + status);
            if (status == google.maps.GeocoderStatus.OK) {
                var location = results[0];

                $scope.search.Lat = location.geometry.location.lat();
                $scope.search.Lon = location.geometry.location.lng();
                $scope.search.GeocodeShortName = location.address_components[0].short_name;
                $scope.search.GeocodeType = location.address_components[0].types[0];

                $scope.$apply();

                if ($scope.useAsync) {
                    $scope.findStoresAsync(ev, location);
                } else {
                    ev.target.submit();
                }

            } else {
                alert('Finding that location was not successful for the following reason: ' + status);
            }
        });
    }
}]);

app.controller("AdvancedSearchCtrl", ['$scope', 'searchService', '$timeout', function ($scope, searchService, $timeout) {

    $scope.search = null;
    $scope.currentForm = {};

    $scope.onAdvancedOpen = function(sbSelector) {
        $scope.search.Q = $(sbSelector).val();
    };
    $scope.submitAdvancedSearch = function (ev) {
        $scope.currentForm.defaultSubmit(ev);
    }
    $scope.initAdvancedSearch = function (delay) {
        if (!delay) delay = 0;
        $timeout(function() {
            searchService.initializeModel();
            var modelWatch = $scope.$watch(function() {
                return searchService.currentModel;
            }, function(nv, ov) {
                if (nv != null) {
                    $scope.search = nv;
                    modelWatch();
                }
            });
        }, delay);
    }
}]);

app.controller("SavedSearchCtrl", ['$scope', '$http', 'globalServices', function ($scope, $http, globalServices) {
    window.savedSearchScope = $scope;
    $scope.currentForm = {};

    $scope.submitSaveSearch = function (ev) {
        ev.preventDefault();

        var searchModel = $scope.search;
        delete searchModel.AvailableDurations;
        delete searchModel.AvailableFrequencies;

        $http({
            method: 'POST',
            url: '/Catalog/SavedSearch',
            data: {model: searchModel}
        }).then(function successCallback(response) {
            if (response.data.error) {
                $scope.search = response.data.model;
            } else {
                $scope.search = response.data;
                $scope.searchCompleted = true;
            }
        }, function errorCallback(response) {

        }).finally(function() {
        });
    }
    $scope.cancelSavedSearch = function(e) {
        e.preventDefault();
        var searchModel = $scope.search;
        delete searchModel.AvailableDurations;
        delete searchModel.AvailableFrequencies;

        $http({
            method: 'POST',
            url: $scope.savedSearchUrl,
            data: { model: searchModel, action: "cancel" }
        }).then(function successCallback(response) {
            if (response.data.error) {
                $scope.search = response.data.model;
            } else {
                $scope.search = response.data;
                $scope.searchCompleted = true;
            }
        }, function errorCallback(response) {

        }).finally(function () {
        });
    }
    $scope.viewSearchResult = function (ev) {
        $scope.currentForm.defaultSubmit(ev);
    }
    $scope.editSavedSearch = function (e) {
        e.preventDefault();
        $scope.searchCompleted = false;
    }

    $scope.init = function (params) {
        $scope.savedSearchUrl = '/Catalog/SavedSearch';
        $scope.search = params.model;
        $scope.frequencies = params.model.AvailableFrequencies;
        $scope.durations = params.model.AvailableDurations;
    }
}]);

// TODO: Refactor this into service and directive
app.controller("ProductsListCtrl", ['$scope', '$http', function ($scope, $http) {
    window.productsListScope = $scope;

    $scope.showSavedSearch = function () {
        showPopup('#savedSearchPopup');
    }

    var getCurrentCatalogUrl = function () {
        var priceStr = $scope.current.priceMin == null && $scope.current.priceMax == null ?
            '' :
            ($scope.current.priceMin == null ? '' : $scope.current.priceMin) + '-' + ($scope.current.priceMax == null ? '' : $scope.current.priceMax);
        var param = {
            br: $scope.current.brandId,
            cl: $scope.current.isClearanceOnly,
            orderby: $scope.current.orderByInt,
            pagenumber: $scope.current.pager.pageNumber < 2 ? null : $scope.current.pager.pageNumber,
            price: priceStr,
            used: $scope.current.used
        }
        var qsa = [];
        for (var p in param) {
            if (param[p]) {
                qsa.push(encodeURIComponent(p) + "=" + encodeURIComponent(param[p]));
            }
        }
        var queryString = qsa.join("&");
        var nurl = '/' + 'c/' + $scope.current.categoryId + '/' + $scope.current.categorySeName;
        if (queryString) {
            nurl = nurl + '?' + queryString;
        }
        return nurl;
    }

    var getCurrentSearchUrl = function () {
        var param = {
            Q: $scope.current.searchQuery,
            Cid: $scope.current.categoryId,
            Pf: $scope.current.priceMin,
            Pt: $scope.current.priceMax,
            Mid: $scope.current.brandId,
            Used: $scope.current.used,
            Cl: $scope.current.isClearanceOnly,
            Isc: $scope.current.includeSubCategory,
            Sid: $scope.current.searchInDescription,
            Advanced: $scope.current.advanced,
            Zip: $scope.current.zip,
            Dist: $scope.current.distance,
            Sn: $scope.current.storeNumber,
            AllStores: $scope.current.allStores,
            PageNumber: $scope.current.pager.pageNumber,
            OrderBy: $scope.current.orderByInt
        }
        var qsa = [];
        for (var p in param) {
            if (param[p]) {
                qsa.push(encodeURIComponent(p) + "=" + encodeURIComponent(param[p]));
            }
        }
        var queryString = qsa.join("&");
        var nurl = '/search';
        if (queryString) {
            nurl = nurl + '?' + queryString;
        }
        return nurl;
    }

    var onPush = function(currentUrl) {
        history.pushState({
            url: currentUrl,
            data: {
                Query: $scope.current.searchQuery,
                Used: $scope.current.used,
                IncludeSubcategory: $scope.current.includeSubCategory,
                SearchInDescription: $scope.current.searchInDescription,
                Advanced: $scope.current.advanced,
                Zip: $scope.current.zip,
                Distance: $scope.current.distance,
                StoreNumber: $scope.current.storeNumber,
                AllStores: $scope.current.allStores,
                CategoryId: $scope.current.categoryId,
                PageNumber: $scope.current.pager.pageNumber,
                OrderBy: $scope.current.orderByInt,
                PriceMin: $scope.current.priceMin,
                PriceMax: $scope.current.priceMax,
                Br: $scope.current.brandId,
                IsClearanceOnly: $scope.current.isClearanceOnly
            }
        }, '', currentUrl);
    }
    var onPop = function(ev) {
        if (ev.state) {
            var data = ev.state.data;
            $scope.current.searchQuery = data.Query;
            $scope.current.used = data.Used;
            $scope.current.includeSubCategory = data.IncludeSubcategory;
            $scope.current.searchInDescription = data.SearchInDescription;
            $scope.current.advanced = data.Advanced;
            $scope.current.zip = data.Zip;
            $scope.current.distance = data.Distance;
            $scope.current.storeNumber = data.StoreNumber;
            $scope.current.allStores = data.AllStores;
            $scope.current.categoryId = data.CategoryId;
            $scope.current.pager.pageNumber = data.PageNumber;
            $scope.current.orderByInt = data.OrderBy;
            $scope.current.priceMin = data.PriceMin;
            $scope.current.priceMax = data.PriceMax;
            $scope.current.brandId = data.Br;
            $scope.current.isClearanceOnly = data.IsClearanceOnly;

            angular.forEach($scope.current.sortOptions, function(v) {
                if ($scope.getValueFromUrl('orderby', v.Value) == $scope.current.orderByInt) {
                    $scope.current.orderBy = v;
                }
            });

            $scope.refreshProducts(true);
        }
    }
    window.addEventListener('popstate', function(ev) {
        onPop(ev);
    });
    $scope.sendGaPageView = function() {
        for (var i = 0; i < gaCmds.length; i++) {
            if (gaCmds[i][0].indexOf("send") > -1 && gaCmds[i][1].indexOf("pageview") > -1) {
                ga.apply(null, gaCmds[i]);
            }
        }
    }
    $scope.searchProducts = function (isPop) {
        showSpinner();
        var modelQuery = {
            Q: $scope.current.searchQuery,
            Cid: $scope.current.categoryId,
            Pf: $scope.current.priceMin,
            Pt: $scope.current.priceMax,
            Mid: $scope.current.brandId,
            Used: $scope.current.used,
            Cl: $scope.current.isClearanceOnly,
            Isc: $scope.current.includeSubCategory,
            Sid: $scope.current.searchInDescription,
            Advanced: $scope.current.advanced,
            Zip: $scope.current.zip,
            Dist: $scope.current.distance,
            Sn: $scope.current.storeNumber,
            AllStores: $scope.current.allStores
        };
        var commandQuery = {
            PageNumber: $scope.current.pager.pageNumber,
            OrderBy: $scope.current.orderByInt
        };

        $http({
            method: 'POST',
            url: '/Catalog/SearchAsync',
            data: {model: modelQuery, command: commandQuery}
        }
        ).then(function successCallback(response) {
            if (response.data.null)
                return;
            var model = response.data;
            $scope.current.products = model.Products;
            $scope.current.totalResults = model.TotalResults;

            if (model.PagingFilteringContext.TotalPages != $scope.current.pager.totalPages) {
                $scope.current.pager.pageNumber = model.PagingFilteringContext.PageNumber;
                $scope.current.pager.totalPages = model.PagingFilteringContext.TotalPages;
                $scope.current.pager.initPager();
            } else if (model.PagingFilteringContext.PageNumber != $scope.current.pager.pageNumber) {
                $scope.current.pager.pageNumber = model.PagingFilteringContext.PageNumber;
                $scope.current.pager.updateShown();
            }

            if (!isPop) {
                var currentUrl = getCurrentSearchUrl();
                onPush(currentUrl);
                $scope.sendGaPageView();
            }

            var titleBound = $scope.dom.pageTitle.getBoundingClientRect();
            if ((titleBound.top + titleBound.height) < 0) {
                $scope.dom.pageTitle.scrollIntoView();
            }
        }, function errorCallback(response) {

        }).finally(function() {
            hideSpinner();
        });
    }
    $scope.catalogBrowse = function(isPop) {
        showSpinner();
        var productQuery = {
            CategoryId: $scope.current.categoryId,
            PageNumber: $scope.current.pager.pageNumber,
            OrderBy: $scope.current.orderByInt,
            PriceMin: $scope.current.priceMin,
            PriceMax: $scope.current.priceMax,
            Br: $scope.current.brandId,
            Used: $scope.current.used,
            IsClearanceOnly: $scope.current.isClearanceOnly
        };
        $http({
                method: 'POST',
                url: '/Catalog/GetProductsAsync',
                data: productQuery
            }
        ).then(function successCallback(response) {
            if (response.data.null)
                return;
            var model = response.data;
            $scope.current.products = model.Products;
            $scope.current.pageTitle = (model.UsedCategory ? 'Used ' : '') + model.Name;
            document.title = $scope.current.pageTitle + ' | Music Go Round';
            $scope.current.totalResults = model.TotalResults;

            if (model.PagingFilteringContext.TotalPages != $scope.current.pager.totalPages) {
                $scope.current.pager.pageNumber = model.PagingFilteringContext.PageNumber;
                $scope.current.pager.totalPages = model.PagingFilteringContext.TotalPages;
                $scope.current.pager.initPager();
            } else if (model.PagingFilteringContext.PageNumber != $scope.current.pager.pageNumber) {
                $scope.current.pager.pageNumber = model.PagingFilteringContext.PageNumber;
                $scope.current.pager.updateShown();
            }

            $scope.current.productFilters.priceRanges = model.PagingFilteringContext.PriceRangeFilter.Items;

            var titleBound = $scope.dom.pageTitle.getBoundingClientRect();
            if ((titleBound.top + titleBound.height) < 0) {
                $scope.dom.pageTitle.scrollIntoView();
            }

            if (!isPop) {
                var updatedUrl = getCurrentCatalogUrl();
                onPush(updatedUrl);
                $scope.sendGaPageView();
            }

        }, function errorCallback(response) {

        }).finally(function () {
            hideSpinner();
        });;
    };

    $scope.changeCategory = function(cId, cSeName) {
        if (cId != $scope.current.categoryId) {
            $scope.prev.categoryId = $scope.current.categoryId;
            $scope.current.categoryId = cId;
            $scope.current.categorySeName = cSeName;
            $scope.current.pager.pageNumber = 1;
            $scope.current.priceMin = null;
            $scope.current.priceMax = null;
            $scope.refreshProducts();
        }
    };
    $scope.changePageNumber = function(ev, pn) {
        $scope.refreshProducts();
    };
    $scope.changePrice = function(pf, pt) {
        $scope.current.priceMin = $scope.toPriceFilterString(pf);
        $scope.current.priceMax = $scope.toPriceFilterString(pt);
        $scope.current.pager.pageNumber = 1;

        $scope.refreshProducts();
    };
    $scope.sortProducts = function(ob) {
        $scope.current.orderByInt = Number(ob);
        $scope.refreshProducts();
    };
    $scope.getValueFromUrl = function(name, url) {
        if (!url) url = location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? "" : results[1];
    };
    $scope.toPriceFilterString = function(dp) {
        return dp == 0 ? null : dp.toFixed(2);
    };
    $scope.formatPriceRange = function(p) {
        if (!p.From) {
            return "Under " + p.To;
        } else if (!p.To) {
            return "Over " + p.From;
        } else {
            return p.From + " - " + p.To;
        }
    };
    $scope.initCtrl = function (paramName, plFunc) {
        var params = window[paramName];
        var currentUrl = window.location.href;
        $scope.current = {
            advanced: params.advanced,
            allStores: params.allStores,
            categoryId: params.cId,
            categorySeName: params.cSeName,
            distance: params.distance,
            includeSubCategory: params.includeSubCategory,
            isClearanceOnly: $scope.getValueFromUrl('Cl', currentUrl).toLowerCase() == "true",
            orderBy: params.sortOptions[3],
            orderByInt: 8,
            pageTitle: params.pageTitle,
            priceMin: params.priceMin,
            priceMax: params.priceMax,
            productFilters: {},
            products: params.products,
            pager: params.pager,
            searchInDescription: params.searchInDescription,
            sortOptions: params.sortOptions,
            searchQuery: params.searchQuery,
            storeNumber: params.storeNumber,
            totalResults: params.totalResults,
            used: $scope.getValueFromUrl('used', currentUrl).toLowerCase() == "true",
            zip: params.zip
        };

        var brId = $scope.getValueFromUrl('br', currentUrl);
        if (!brId) {
            brId = $scope.getValueFromUrl('Mid', currentUrl);
        }
        $scope.current.brandId = brId;
        $scope.prev = {
            categoryId: params.cId
        };
        $scope.dom = {
            pageTitle: document.getElementsByClassName('big-page-title')[0],
            productsList: document.getElementsByClassName('products-list-wrapper')[0]
        };
        $scope.refreshProducts = plFunc;
    }
}]);

app.controller("ShoppingCartCtrl", ['$scope', '$http', 'shoppingCartService', function ($scope, $http, shoppingCartService) {
    window.shoppingCartScope = $scope;
    $scope.shoppingCartService = shoppingCartService;

    $scope.updateViewModel = function (model) {
        $scope.isEditable = model.IsEditable;
        $scope.shoppingCartService.items = model.Items;
        $scope.shoppingCartService.productCount = $scope.shoppingCartService.items.length;
    }
    $scope.updateProductVariantQuantity = function (sciId, pvId, qty) {
        if (qty < 1) return;
        showSpinner();
        $scope.shoppingCartService.updateProductVariantQuantity(sciId, pvId, qty, $scope.isEditable)
            .finally(hideSpinner);
    }
    $scope.removeProductVariantFromCart = function (sciId, pvId) {
        showSpinner();
        $scope.shoppingCartService.removeProductVariantFromCart(sciId, pvId, $scope.isEditable)
            .finally(hideSpinner);
    };
    $scope.initShoppingCartFull = function(paramName) {
        var params = window[paramName];
        showSpinner();
        $scope.checkoutUrl = params.CheckoutUrl;
        $scope.updateViewModel(params);
        $scope.shoppingCartService.updateOrderTotals($scope.isEditable)
            .finally(hideSpinner);

        ga('linker:decorate', document.getElementById('checkoutLink'));
    }
    $scope.initShoppingCartMini = function (checkoutUrl) {
        $scope.isEditable = false;
        $scope.checkoutUrl = checkoutUrl;
        $scope.shoppingCartService.updateCart()
            .then($scope.shoppingCartService.updateOrderTotals());

        ga('linker:decorate', document.getElementById('checkoutLinkMini'));
    };
}]);
