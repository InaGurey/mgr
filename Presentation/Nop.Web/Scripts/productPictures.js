﻿$(document).ready(function () {
    $('#etalage').etalage({
        thumb_image_width: 535,
        thumb_image_height: 535,
        zoom_area_height: 400,
        zoom_area_width: 540,
        small_thumbs: 6,
        source_image_height: 1080,
        source_image_width: 1080,
        autoplay:false
    });
    
    // This catches the click event:
    $('#prevImage').on('click', function (e) {
        e.preventDefault();
        etalage_previous();
    });

    $('#nextImage').on('click', function (e) {
        e.preventDefault();
        etalage_next();
    });

});