﻿$(function () {
    if (!navigator.geolocation) {
        console.log("DOES NOT HAVE GEO");
        $('#findStoreForMe').hide();
    }


    $('#autoFind').click(function (e) {
        e.preventDefault();

        function success(position) {
            $('#Lat').val(position.coords.latitude);
            $('#Lon').val(position.coords.longitude);

            $('#locationSearch').submit();
        };

        function error() {
            alert("We could not find your location automatically. Please use the form instead");
        };

        //will try this out, if we get inconsistant values then I'll move to the geo.js file we used in others
        navigator.geolocation.getCurrentPosition(success, error);
    });

    $('#geoSearch').click(function (e) {
        e.preventDefault();

        if ($('#HVerify').val() != '')
            return;

        if ($('#SearchedLocation').val() == '') {
            alert("You need to enter a location to search");
            return;
        }

        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'address': $('#SearchedLocation').val() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var location = results[0];

                $('#Lat').val(location.geometry.location.lat());
                $('#Lon').val(location.geometry.location.lng());
                $('#GeocodeShortName').val(location.address_components[0].short_name);
                $('#GeocodeType').val(location.address_components[0].types[0]);

                $('#locationSearch').submit();
            } else {
                alert('Finding that location was not successful for the following reason: ' + status);
            }
        });
    });

});