﻿/* TREEFORT MODAL FOR MOBILE 
 * ©2015 Treefort
 * Convert traditional HTML DOM into a pop up modal window
 * Currently supported DOMs:
 * - Select/Dropdown
 * - List (ul, ol)
 */

var _tm;

$(function () {
    if (!_tm) {
        InitializeGlobalmd();
    }
});

function InitializeGlobalmd() {
    _tm = {
        DimOverlayClass: 'treefortmodal-overlay',
        TreefortModalClass: 'treefort-modal',
        ModalTitleClass: 'title',
        ModalCloseClass: 'close',
        ModalCloseButtonClass: 'close-button',
        ModalOptionsListClass: 'options-list',
        ModalOptionClass: 'option',
        TreefortModalIdPrefix: 'treefortmodal_',
        ValuePropName: 'treefortmodal-value',
        TextPropName: 'treefortmodal-text',
        ApplyClassPropName: 'treefortmodal-apply-class',
        TreefortModalInitializedKey: 'treefortmodal-initialized'
    };

    var dimOverlay = $('<div/>', {
        'class': _tm.DimOverlayClass
    });

    $('body').append(dimOverlay);
    _tm.DimOverlay = dimOverlay;

    _tm.showModalWindow = function (modalId) {
        $(_tm.DimOverlay).show();
        $('#' + modalId).show();
        $('body').css('overflow', 'hidden');
        _tm.setModalWindowHeight(modalId);
    }

    _tm.hideModalWindow = function (modalId) {
        $(_tm.DimOverlay).hide();
        $('#' + modalId).hide();
        $('body').css('overflow', '');
    }

    _tm.setModalWindowHeight = function (modalId) {
        var modal = $('#' + modalId);
        var modalHeight = modal.outerHeight(true);
        var titleHeight = modal.find('.' + _tm.ModalTitleClass).outerHeight(true);
        var closeHeight = modal.find('.' + _tm.ModalCloseClass).outerHeight(true);
        var optionsList = modal.find('.' + _tm.ModalOptionsListClass);
        var optionsListHeight = optionsList.outerHeight(true);
        var totalHeight = titleHeight + closeHeight + optionsListHeight;

        if ($(window).height() * 0.85 > totalHeight) return;
        optionsList.css('height', modalHeight - titleHeight - closeHeight);
    }
}

function getTreefortModalId(dropdownSelector) {
    var sanitized = dropdownSelector.replace('.', '').replace('#', '');
    return _tm.TreefortModalIdPrefix + sanitized + Math.random().toString().replace('.', '');
}

function GetTreefortModalTitleArea(title) {
    var divTitle = $('<div/>', {
        'class': _tm.ModalTitleClass,
        'html': title
    });

    return divTitle;
}

function GetTreefortModalCloseArea(modalId) {

    var divClose = $('<div/>', {
        'class': _tm.ModalCloseClass
    });

    var divCloseButton = $('<button/>', {
        'class': _tm.ModalCloseButtonClass,
        'html': 'Close'
    });

    $(divCloseButton).on('click', function (e) {
        e.preventDefault();
        _tm.hideModalWindow(modalId);
    });

    divClose = $(divClose);
    divClose.append(divCloseButton);

    return divClose;
}

function GetModalSelectorOptionsList($dropdown) {
    var $options = $dropdown.children('option');

    var divParent = $('<div/>', {
        'class': _tm.ModalOptionsListClass
    });

    var optionsList = {
        parent: $(divParent),
        options: []
    };

    $.each($options, function(i, op) {
        op = $(op);
        if (op.is(':disabled')) return true;

        var divChild = $('<div/>', {
            'class': _tm.ModalOptionClass,
            'data-treefortmodal-value': op.val(),
            'data-treefortmodal-text': op.text(),
            'html': op.text()
        });

        divChild = $(divChild);
        if (op.is(':selected')) {
            divChild.addClass('selected');
        }

        if (op.data(_tm.ApplyClassPropName)) {
            divChild.addClass(op.data(_tm.ApplyClassPropName));
        }

        divParent.append(divChild);
        optionsList.options.push(divChild);
    });

    $dropdown.on('change', function (e) {
        var v = $dropdown.val();
        $.each(optionsList.options, function (i, o) {
            o = $(o);
            if (o.data(_tm.ValuePropName) == v) o.addClass('selected');
            else o.removeClass('selected');
        });
    });

    return optionsList;
}

function GetModalListOptionsList($list) {
    var $options = $list.children('li');

    var divParent = $('<div/>', {
        'class': _tm.ModalOptionsListClass
    });

    var optionsList = {
        parent: $(divParent),
        options: []
    };

    $.each($options, function (i, op) {
        op = $(op);
        if (op.is(':disabled')) return true;

        var divChild = $('<div/>', {
            'class': _tm.ModalOptionClass,
            'data-treefortmodal-text': op.text(),
            'html': op.html()
        });
        divChild = $(divChild);

        divParent.append(divChild);
        optionsList.options.push(divChild);
    });

    return optionsList;
}

function InitializeModalDropdown(dropdownSelector, title) {
    var $dropdown = $(dropdownSelector);

    if (!$dropdown.length || !$dropdown.is('select')) return null;

    var mbClass = _tm.TreefortModalClass;
    var mbId = getTreefortModalId(dropdownSelector);

    var modalBox = $('<div/>', {
        'id' : mbId,
        'class': mbClass
    });

    modalBox.append(GetTreefortModalTitleArea(title));
    var optionsList = GetModalSelectorOptionsList($dropdown);
    modalBox.append(optionsList.parent);

    $dropdown.on('click', function(e) {
        e.preventDefault();
        this.blur();
        window.focus();
        _tm.showModalWindow(mbId);
    });

    $(_tm.DimOverlay).on('click', function (e) {
        _tm.hideModalWindow(mbId);
    });

    modalBox.appendTo('body');

    $dropdown.data(_tm.TreefortModalInitializedKey, true);

    var modalWindow = {
        Id: mbId,
        Window: $(modalBox),
        OptionsList: optionsList
    }

    return modalWindow;
}

function InitializeModalList(listSelector, showTrigger, title) {
    var $list = $(listSelector);

    if (!$list.length || !$list.is('ul, ol')) return null;

    var mbClass = _tm.TreefortModalClass;
    var mbId = getTreefortModalId(listSelector);

    var modalBox = $('<div/>', {
        'id': mbId,
        'class': mbClass
    });

    modalBox.append(GetTreefortModalTitleArea(title));
    var optionsList = GetModalListOptionsList($list);
    modalBox.append(optionsList.parent);
    modalBox.append(GetTreefortModalCloseArea(mbId));

    $(showTrigger).on('click', function (e) {
        e.preventDefault();
        this.blur();
        window.focus();
        _tm.showModalWindow(mbId);
    });

    $(_tm.DimOverlay).on('click', function (e) {
        _tm.hideModalWindow(mbId);
    });

    modalBox.appendTo('body');

    $list.data(_tm.TreefortModalInitializedKey, true);

    var modalWindow = {
        Id: mbId,
        Window: $(modalBox),
        OptionsList: optionsList
    }

    return modalWindow;
}

function ModalDropdown(options) {

    var md = {
        Selector: options.Selector,
        Title: options.Title,
        callback: options.callback
    };

    var $dropdown = $(md.Selector);
    if (!$dropdown.data(_tm.TreefortModalInitializedKey)) {
        md.ModalWindow = InitializeModalDropdown(md.Selector, md.Title);
    }

    md.show = function (){ _tm.showModalWindow(md.ModalWindow.Id);}
    md.hide = function () {_tm.hideModalWindow(md.ModalWindow.Id);}

    md.ModalWindow.OptionsList.parent.children('.' + _tm.ModalOptionClass).on('click', function(e) {
        e.preventDefault();

        var si = $(e.currentTarget);
        var v = si.data(_tm.ValuePropName);
        md.SelectedValue = v;

        md.hide();

        if (md.callback) {
            md.callback();
        }
    });

    Object.defineProperty(md, 'SelectedValue', {
        get: function() {
            return $dropdown.val();
        },
        set: function (v) {
            $dropdown.val(v).trigger('change');
        }
    });

    return md;
};

function ModalList(list) {

    var ml = {
        Selector: list.Selector,
        Title: list.Title,
        ShowTrigger: list.ShowTrigger,
        callback: list.callback
    };

    var $list = $(ml.Selector);
    if (!$list.data(_tm.TreefortModalInitializedKey)) {
        ml.ModalWindow = InitializeModalList(ml.Selector, ml.ShowTrigger, ml.Title);
    }

    ml.show = function () { _tm.showModalWindow(md.ModalWindow.Id); }
    ml.hide = function () { _tm.hideModalWindow(md.ModalWindow.Id); }

    return ml;
}
