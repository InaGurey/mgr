﻿
$.fn.openSelect = function () {
    return this.each(function (idx, domEl) {
        if (document.createEvent) {
            var event = document.createEvent("MouseEvents");
            event.initMouseEvent("mousedown", true, true, window);
            domEl.dispatchEvent(event);
        } else if (element.fireEvent) {
            domEl.fireEvent("onmousedown");
        }
    });
}
$.fn.initCustomNano = function () {
    $(this).nanoScroller({ alwaysVisible: true, tabIndex: -1, preventPageScrolling: true });
    $(this).nanoScroller({ scroll: 'top' });
}


function check_small_search_form(rid) {
    var stb = $("#searchterms" + rid);
    if (stb.val() == "") {
        alert('Please enter keyword to begin search.');
        stb.focus();
        return false;
    }

    return true;
}
function showSpinner() {
    document.getElementById('globalSpinnerOverlay').style.display = 'block';
}
function hideSpinner() {
    document.getElementById('globalSpinnerOverlay').style.display = 'none';
}
function showPopup(selector) {
    var $pp = $(selector);
    if ($pp.length === 0) return;

    $pp.closest('.overlay-popup').show();
    $('body').css('overflow', 'hidden');
    $pp.show();
    $pp.find('input:focusable').eq(0).focus();

    $pp.find('.nano').initCustomNano();
}
function hidePopup(selector) {
    var $pp = $(selector);
    if ($pp.length === 0) return;

    $pp.closest('.overlay-popup').hide();
    $('body').css('overflow', '');
    $pp.hide();
}

function initLatestAdditionWidgets() {

    $.fn.changeCategory = function(sn) {
        $(this).find('.categories-list li a').removeClass('selected');
        $(this).find('.categories-list li a[data-se-name=' + sn + ']').addClass('selected');

        $(this).find('.product-flex-grid').fadeOut();
        $(this).find('.product-flex-grid[data-se-name=' + sn + ']').fadeIn();
    }
    $.fn.getCategories = function() {
        var categories = $(this).find('.categories-list li a').map(function (i, v) {
            return $(v).data('se-name');
        });
        return categories;
    }

    $('.home-page-products').each(function (i, v) {
        var categories = $(v).getCategories();
        $(v).data('categories', categories);
        $(v).changeCategory(categories[0]);
    });

    $('.home-page-products .products-wrapper .prev').on('click', function (e) {
        e.preventDefault();
        var $sc = $(this).closest('.home-page-products');
        var currentCatIndex = parseInt($sc.data('current-cat-index'));
        var categories = $sc.data('categories');

        currentCatIndex = currentCatIndex === 0 ? categories.length - 1 : currentCatIndex - 1;
        $sc.data('current-cat-index', currentCatIndex);
        $sc.changeCategory(categories[currentCatIndex]);
    });
    $('.home-page-products .products-wrapper .next').on('click', function (e) {
        e.preventDefault();
        var $sc = $(this).closest('.home-page-products');
        var currentCatIndex = parseInt($sc.data('current-cat-index'));
        var categories = $sc.data('categories');

        currentCatIndex = currentCatIndex === categories.length - 1 ? 0 : currentCatIndex + 1;
        $sc.data('current-cat-index', currentCatIndex);
        $sc.changeCategory(categories[currentCatIndex]);
    });
    $('.home-page-products .categories-list li a').on('click', function (e) {
        e.preventDefault();
        var $sc = $(this).closest('.home-page-products');
        var categories = $sc.data('categories');

        var seName = $(this).data('se-name');
        currentCatIndex = $.inArray(seName, categories);
        $sc.data('current-cat-index', currentCatIndex);
        $sc.changeCategory(seName);
    });
}

$(function () {

    function initAutoComplete(rid, options) {
        if (options.enableAutoComplete) {
            var sbSelector = '#searchterms' + rid;
            $(sbSelector).autocomplete({
                delay: 500,
                minLength: options.minLength,
                position: {
                    my: 'left top',
                    at: 'left bottom',
                    of: '#sb' + rid + '.search-box'
                },
                source: options.sourceUrl,
                select: function (event, ui) {
                    $(sbSelector).val(ui.item.label);
                    setLocation(ui.item.producturl);
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                var t = item.label;
                //html encode
                t = htmlEncode(t);
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append(options.itemTemplate)
                    .append("<a>" + (options.showImage ? "<img src='\" + item.productpictureurl + \"'>" : "") + t + "</a>")
                    .appendTo(ul);
            };
        }
    };
    function initMenuHover() {
        var $dropdowns = $('.dropdown');
        var $topLinks = $('.top-menu-main-link');

        $dropdowns.each(function () {
            var $ua = $(this).children('.uparrow');
            var $tl = $(this).siblings('.top-menu-main-link');
            var os = $tl.position().left + ($tl.width() / 2) - ($ua.outerWidth() / 4);
            $ua.css('left', os);
        });

        function showDd($dd, $lm) {
            $dd.children('.uparrow').show();
            $dd.stop(true, true).fadeIn('fast');
            $lm.attr('aria-expanded', true);
        }

        function hideDd($dd, $lm) {
            $dd.children('.uparrow').hide();
            $dd.delay(200).fadeOut('fast');
            $lm.attr('aria-expanded', false);
        }

        $topLinks.on('mouseenter focus', function () {
            showDd($(this).next('.dropdown'), $(this));
        });
        $topLinks.on('mouseleave blur', function () {
            hideDd($(this).next('.dropdown'), $(this));
        });
        $dropdowns.on('mouseenter focusin', function () {
            showDd($(this), $(this).siblings('.top-menu-main-link'));
        });
        $dropdowns.on('mouseleave focusout', function () {
            hideDd($(this), $(this).siblings('.top-menu-main-link'));
        });
    }
    function initFlyoutHover() {
        var $tcLink = $('#topcartlink');
        var $flCart = $('#flyout-cart');
        $tcLink.live('mouseenter focusin', function () {
            $flCart.stop(true, true).fadeIn('fast');
        });
        $tcLink.live('mouseleave focusout', function () {
            $flCart.delay(200).fadeOut('fast');
        });
        $flCart.live('mouseenter focusin', function () {
            $flCart.stop(true, true).fadeIn('fast');
        });
        $flCart.live('mouseleave focusout', function () {
            $flCart.delay(200).fadeOut('fast');
        });

    }
    Modernizr.addTest('mix-blend-mode', function() {
        return Modernizr.testProp('mixBlendMode');
    });
    Modernizr.addTest('firefox', function() {
        return navigator.userAgent.match(/firefox/i);
    });
    Modernizr.addTest('trident', function () {
        return navigator.userAgent.match(/trident/i);
    });

    if (typeof searchBoxes !== "undefined" && searchBoxes instanceof Array) {
        for (var sidx = 0; sidx < searchBoxes.length; sidx++) {
            initAutoComplete(searchBoxes[sidx], searchBoxOption);
        }
    }
    initMenuHover();
    initFlyoutHover();
    initLatestAdditionWidgets();

    $('.location-hours .hours-link.non-angular-link').on('click', function(e) {
        e.preventDefault();

        var $eh = $(this).closest('.location-hours').find('.expanded-hours');
        $eh.toggle();

        if ($eh.is(':visible')) {
            $(this).attr('aria-expanded', true);
            $(this).children('.icon-chevron-down').removeClass('icon-chevron-down').addClass('icon-chevron-up');
        } else {
            $(this).attr('aria-expanded', false);
            $(this).children('.icon-chevron-up').removeClass('icon-chevron-up').addClass('icon-chevron-down');
        }
    });
    $('.location-box .gold-star-wrapper, .location-box .location-img-wrapper').on('mouseenter focusin', function() {
        $(this).closest('.location-box').find('.four-star-description').css('opacity', 1);
    });
    $('.location-box .gold-star-wrapper, .location-box .location-img-wrapper').on('mouseleave focusout', function() {
        $(this).closest('.location-box').find('.four-star-description').css('opacity', 0);
    });
    $('.location-state-nav li a').click(function (e) {
        e.preventDefault();
        var stateAbbr = $(this).data('state');

        $('.location-state-nav li a').removeClass('selected');
        $(this).addClass('selected');

        $('.location-box').hide().filter(function () {
            if (stateAbbr != null) {
                return $(this).data('state') == stateAbbr || $(this).data('state') == 'own-box';
            } else {
                return true;
            }
        }).show();
    });

    $('.header-search-bar .sb-advanced-search-link').on('click', function(e) {
        e.preventDefault();
        showPopup('#advancedSearchPopup');
    });

    $('.overlay-popup').on('click', function (e) {
        hidePopup($(this).find('.popup'));
    });
    $('.popup-close').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        hidePopup($(this).closest('.popup'));
    });
    $('.overlay-popup .popup').on('click', function (e) {
        e.stopPropagation();
    });

    $('.popup-opener').on('click', function(e) {
        e.preventDefault();
        var selector = '#' + $(this).attr('aria-controls');
        showPopup(selector);
    });

    $(document).delegate('.custom-select-dropdown + .icon-chevron-down', 'click', function(e) {
        e.preventDefault();
        $(this).prev('select').openSelect();
    });

    $('.honeypot-input').each(function(i, v) {
        $(v).attr('aria-labelledby', 'honeypotLabel');
    });

    $(document).on('submit', '.email-store-form form', function (e) {
        var $form = $(this);
        if ($form[0].checkValidity()) {
            e.preventDefault();
            $.post($form.attr('action'), $form.serialize(), function(data) {
                $form.hide();
                $form.siblings('#emailStoreMessage').html(data.message);
            });
        }
    });
});