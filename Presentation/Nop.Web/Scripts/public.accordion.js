/*
** nopCommerce custom accordion
*/

var Accordion = {
    checkAllow: false,
    disallowAccessToNextSections: false,
    sections: [],
    sectionTrail: [],
    headers: [],

    init: function (elem, clickableEntity, checkAllow) {
        this.checkAllow = checkAllow || false;
        this.disallowAccessToNextSections = false;
        this.sections = $('#' + elem + ' .section');
        var headers = $('#' + elem + ' .section ' + clickableEntity);
        headers.click(function () {
            Accordion.headerClicked($(this));
        });
    },
    
    currentSection: function() {
    	return $('#' + this.sectionTrail[this.sectionTrail.length - 1]);
    },

    headerClicked: function (section) {
        Accordion.openSection(section.parent('.section'));
    },

    openSection: function(section) {
        var section = $(section);

        if (this.checkAllow && !section.hasClass('allow'))
            return;

        if (section.attr('id') == this.currentSection().attr('id'))
	        return;

        this.closeExistingSection();
        this.sectionTrail.push(section.attr('id'));
	    this.currentSection().addClass('active');
        var contents = section.children('.a-item');
        $(contents[0]).show();

        if (this.disallowAccessToNextSections) {
            var pastCurrentSection = false;
            for (var i = 0; i < this.sections.length; i++) {
                if (pastCurrentSection) {
	                $(this.sections[i]).removeClass('allow');
                }
                if ($(this.sections[i]).attr('id') == section.attr('id')) {
                    pastCurrentSection = true;
                }
            }
        }
    },

    closeSection: function (section) {
        var section = $(section);
        section.removeClass('active');
        var contents = section.children('.a-item');
        $(contents[0]).hide();
    },

    openNextSection: function (setAllow) {
        for (section in this.sections) {
            var nextIndex = parseInt(section) + 1;
            if (this.sections[section].id == this.currentSection().attr('id') && this.sections[nextIndex]) {
                if (setAllow) {
	                $(this.sections[nextIndex]).addClass('allow');
                }
                this.openSection(this.sections[nextIndex]);
                return;
            }
        }
    },

    openPrevSection: function(setAllow) {
	    this.closeExistingSection();
    	this.sectionTrail.pop();
	    var section = this.currentSection();
	    this.sectionTrail.pop(); // double pop because openSection is gonna push
	    if (setAllow)
	    	section.addClass('allow');
    	this.openSection(section);
    },
    
    closeExistingSection: function() {
	    if (this.sectionTrail.length)
	       	this.closeSection(this.currentSection());
    }
};