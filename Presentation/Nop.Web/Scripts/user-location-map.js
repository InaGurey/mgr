﻿String.format = String.prototype.format = function () {
    var string = this;
    var i = 0;
    if (typeof (this) == "function") {
        string = arguments[0];
        i++;
    }
    for (; i < arguments.length; i++)
        string = string.replace(/\{\d+?\}/, arguments[i]);
    return string;
}

$(function () {
    $("#map-loading-indicator").slideDown();

    var customId = $.cookie("lon");
    var url = "";
    if (customId) {

        url = "/locations/Geo/?lat=" + $.cookie("lat") + "&ln=" + customId;

        $("#map-list").load(url, bindLocationData);
        $('#cityNear span').html($.cookie("code"));

    } else {
        $("#map-list").load('/locations/geo/', bindLocationData);
    }

    $("#map-prompt input").css("color", "#ABABAB");

    $("#map-prompt input").click(function (e) {
        e.preventDefault();

        if ($(this).val() == "City & State or Postal Code") {
            $(this).val("");
            $(this).css("color", "#444");
        }
    });

    // $("#map-change button").click(showChangeLocation);
    $("#map-prompt button").click(changeLocation);
    $("#map-prompt input").keypress(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            changeLocation();
        }
    }
    );
    

    $('#changeLocation').click(function (e) {
        e.preventDefault();

        if ($('#map-prompt').is(':visible')) {
            $('#map-prompt').slideUp();
        }else{
            $('#map-prompt').slideDown();
        }
    });
});


function showChangeLocation() {
    $("#map-change").slideUp();
    $("#map-prompt").slideDown();
    $("#map-prompt input").focus();
}

function changeLocation() {
    $("#map-list").slideUp(function () {
        $("#map-prompt").slideUp(function () {
            $('#changeLocation').fadeOut();
            $("#cityNear").fadeOut(function () {
               
                $("#map-loading-indicator").slideDown(function () {
                    var geocoder = new google.maps.Geocoder();

                    var url = '';

                    geocoder.geocode({ 'address': $("#map-prompt input").val() }, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            url = "/locations/geo?lat=" + results[0].geometry.location.lat() + "&ln=" + results[0].geometry.location.lng();
                            $.cookie("lat", results[0].geometry.location.lat(), { path: '/' });
                            $.cookie("lon", results[0].geometry.location.lng(), { path: '/' });
                            $.cookie("code", $("#map-prompt input").val(), { path: '/' });

                        } else {
                            url = '/locations/postal?code=' + encodeURIComponent($("#map-prompt input").val());

                            console.log("Geocode was not successful for the following reason: " + status);
                        }


                        $("#map-list").load(url, bindLocationData);

                        var customId = $("#map-prompt input").val();
                        // var date = new Date();
                        // date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));



                        $('#cityNear span').html($("#map-prompt input").val());
                        $("#map-prompt input").val("City & State or Postal Code");
                        $("#map-prompt input").css("color", "#ABABAB");
                        $("#map-prompt input").blur();
                    });



                });
            });
        });

    });
}

function bindLocationData() {
    bindLocationLinks();

    var map, lat, lng, li, goldStandard, marker;
    $("#map-list div").each(function (i, el) {
        li = $(el);
        lat = parseFloat(li.attr("lat"));
        lng = parseFloat(li.attr("lng"));
        //goldStandard = li.attr("data-goldStandard");

        if (i == 0) {
            li.addClass("selected");
            var myOptions = {
                zoom: 11,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        }

        if (i == 4) {
            map.setZoom(7);
        }

        if (i == 8) {
            map.setZoom(5);
        }

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            icon: '/Themes/MGR/Content/images/location-pin-small.png'
        });


        li.click(function () {
            var lat = parseFloat($(this).attr("lat"));
            var lng = parseFloat($(this).attr("lng"));
            if (isNaN(lat))
                return;

            map.panTo(new google.maps.LatLng(lat, lng));
            map.setZoom(11);
            $('#map-list div').removeClass("selected");
            $(this).addClass("selected");
        });
    });

    $("#map-loading-indicator").slideToggle(function() {
        $("#map-list").slideDown();
        $("#cityNear").fadeIn();
        $('#changeLocation').fadeIn();
    }); //$("#map-prompt").slideDown(function () {  }); });

}

var bindLocationLinksFlag = false;
function bindLocationLinks() {

    if (bindLocationLinksFlag) return;
    bindLocationLinksFlag = true;

    $("a.email-list").live("click", function (e) {
        e.preventDefault();
        window.open($(this).attr("href"), "emaillist", "width=600,height=525,toolbar=0");
        return false;
    });

    $("a.website,a.directions").live("click", function (e) {
        e.preventDefault();
        window.open($(this).attr("href"));
        return false;
    });

    if ($('#locationInfo').attr("data-city") != "") {
        $('#cityNear span').html($('#locationInfo').attr("data-city"));
    }

    $(".hours-link").live("click", function (e) {
    	e.preventDefault();
	    
        var table = $("#" + $(this).attr("data-id"));

        $('.location-hours').hide();

    	if (table.length == 0) {
            var parent = $(this).parent();
            $.get($(this).attr("href"), function (responseText) {
                html = $(responseText);
                $("tr:odd", html).addClass("odd");
                parent.append(html);
            });
        } else if (table.is(":visible")) {
            table.hide();
        } else {
            table.show();
        }
        return false;
    });

    $(".location img").one('error', function () {
        $(this).attr("src", "/media/images/no-photo.gif");
    });
}