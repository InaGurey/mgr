﻿/*
** nopCommerce custom js functions
*/


function getE(name) {
    //Obsolete since nopCommerce 2.60. But still here for backwards compatibility (in case of some plugin developers used it in their plugins or customized solutions)
    if (document.getElementById)
        var elem = document.getElementById(name);
    else if (document.all)
        var elem = document.all[name];
    else if (document.layers)
        var elem = document.layers[name];
    return elem;
}

function OpenWindow(query, w, h, scroll) {
    var l = (screen.width - w) / 2;
    var t = (screen.height - h) / 2;

    winprops = 'resizable=0, height=' + h + ',width=' + w + ',top=' + t + ',left=' + l + 'w';
    if (scroll) winprops += ',scrollbars=1';
    var f = window.open(query, "_blank", winprops);
}

function setLocation(url) {
    window.location.href = url;
}

function displayAjaxLoading(display) {
    if (display) {
        $('.ajax-loading-block-window').show();
    }
    else {
        $('.ajax-loading-block-window').hide('slow');
    }
}

function displayPopupNotification(message, messagetype, modal) {
    //types: success, error
    var container;
    if (messagetype == 'success') {
        //success
        container = $('#dialog-notifications-success');
    }
    else if (messagetype == 'error') {
        //error
        container = $('#dialog-notifications-error');
    }
    else {
        //other
        container = $('#dialog-notifications-success');
    }

    //we do not encode displayed message
    var htmlcode = '';
    if ((typeof message) == 'string') {
        htmlcode = '<p>' + message + '</p>';
    } else {
        for (var i = 0; i < message.length; i++) {
            htmlcode = htmlcode + '<p>' + message[i] + '</p>';
        }
    }

    container.html(htmlcode);

    var isModal = (modal ? true : false);
    container.dialog({modal:isModal});
}


var barNotificationTimeout;
function displayBarNotification(message, messagetype, timeout) {
    clearTimeout(barNotificationTimeout);

    //types: success, error
    var cssclass = 'success';
    if (messagetype == 'success') {
        cssclass = 'success';
    }
    else if (messagetype == 'error') {
        cssclass = 'error';
    }
    //remove previous CSS classes and notifications
    $('#bar-notification')
        .removeClass('success')
        .removeClass('error');
    $('#bar-notification .content').remove();

    //we do not encode displayed message

    //add new notifications
    var htmlcode = '';
    if ((typeof message) == 'string') {
        htmlcode = '<p class="content">' + message + '</p>';
    } else {
        for (var i = 0; i < message.length; i++) {
            htmlcode = htmlcode + '<p class="content">' + message[i] + '</p>';
        }
    }
    $('#bar-notification').append(htmlcode)
        .addClass(cssclass)
        .slideDown('slow')
        .mouseenter(function ()
            {
                clearTimeout(barNotificationTimeout);
            });

    $('#bar-notification .close').unbind('click').click(function () {
        $('#bar-notification').slideUp('slow');
    });

    //timeout (if set)
    if (timeout > 0) {
        barNotificationTimeout = setTimeout(function () {
            $('#bar-notification').slideUp('slow');
        }, timeout);
    }
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

$(document).on('click', 'a.cross-domain', function(e) {
	e.preventDefault();
	var url = this.href;
	$.get('/GetSsoToken', function(data) {
		location.href = url + '?ssotoken=' + data;
	});
});

$(function() {
    $overlay = $('#overlay');
    $('body').on('click', '._savedSearch', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $form = $($this.parents('#advanced-search-wrapper').find('form')[0]);

        $.get($(this).attr('href'), $form.serialize(), function (data) {
            $modal = $(data);

            $('body').append($modal);

            $modal.css({
                width: '1020px',
                height: 'auto'
            });

            var top, left;
            top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
            left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

            $modal.css({
                top: top + $(window).scrollTop(),
                left: left + $(window).scrollLeft()
            });

            $overlay.show();
            $modal.show();
            //need this for the stylized dropdowns
            Custom.init();

            $('.saved-search h2').focus();

            $('.saved-search form').on('submit', function (e) {
                var $email = $('.saved-search input[name=Email]');
                var email = $email.val();
                if (!email) {
                    if ($('.saved-search form .field-validation-error[data-valmsg-for=Email]').length == 0)
                        $email.after('<br /><span class="field-validation-error" data-valmsg-for="Email">Please enter your email address.</span>');
                    $email.focus();
                    e.preventDefault();
                }
            });
        });
    });

    $('.saved-search input[name=Email]').focus();

    function hideForm() {
        $('#modal').remove();
        $('#modal-saved-search').remove();
        $overlay.hide();
    }

    $('body').on('click', '#close, #overlay,#closeLink', function (e) {
        hideForm();
    });
});