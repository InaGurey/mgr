﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Telerik.Web.Mvc;

namespace Nop.Admin.Models.Catalog
{
    public partial class ProductListModel : BaseNopModel
    {
        public ProductListModel()
        {
            AvailableCategories = new List<SelectListItem>();
            AvailableManufacturers = new List<SelectListItem>();
        }

        public bool CanAddProduct { get; set; }
        public bool EnableImport { get; set; }

        public GridModel<ProductModel> Products { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
        [AllowHtml]
        public string SearchProductName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchCategory")]
        public int SearchCategoryId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchManufacturer")]
        public int SearchManufacturerId { get; set; }

		// treefort enhancement
		public string SearchStoreNumber { get; set; }
		public bool ShowStoreNumber { get; set; }

		public bool? SearchWithImages { get; set; }
		public bool? SearchWithDescrip { get; set; }
		public bool? SearchPublished { get; set; }
        public bool SearchClearanceOnly { get; set; }
		public ProductSortingEnum SortOption { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Products.List.GoDirectlyToSku")]
        [AllowHtml]
        public string GoDirectlyToSku { get; set; }

        public bool DisplayProductPictures { get; set; }
        public bool DisplayPdfDownloadCatalog { get; set; }

        public IList<SelectListItem> AvailableCategories { get; set; }
		public IList<SelectListItem> AvailableManufacturers { get; set; }
		public IList<SelectListItem> ImageOptions { get; set; }
		public IList<SelectListItem> DescriptionOptions { get; set; }
		public IList<SelectListItem> PublishedOptions { get; set; }
		public IList<SelectListItem> SortOptions { get; set; }

		public ProductStatsModel Stats { get; set; }
	}

	public class ProductStatsModel
	{
		public int TotalCount { get; set; }
		public int PublishedCount { get; set; }
		public int UnpublishedCount { get; set; }
		public int WaitingOnDelayCount { get; set; }
		public int WithoutImagesCount { get; set; }
		public int WithoutDescripCount { get; set; }
		public int WithoutImagesAndDescripCount { get; set; }
	}
}