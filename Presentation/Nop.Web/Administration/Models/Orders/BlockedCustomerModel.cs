﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nop.Web.Framework.Mvc;


namespace Nop.Admin.Models.Orders
{
    public class BlockedCustomerModel : BaseNopEntityModel
    {
        public string IpAddress { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime AttemptedAt { get; set; }
    }
}