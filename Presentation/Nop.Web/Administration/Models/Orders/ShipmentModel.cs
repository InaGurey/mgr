﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Orders
{
    public partial class ShipmentModel : BaseNopEntityModel
    {
        public ShipmentModel()
        {
            this.Products = new List<ShipmentOrderProductVariantModel>();
        }
        [NopResourceDisplayName("Admin.Orders.Shipments.ID")]
        public override int Id { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.OrderID")]
        public int OrderId { get; set; }
        public int ShipmentPackageId { get; set; }

        [NopResourceDisplayName("Admin.Orders.Shipments.TotalWeight")]
        public string TotalWeight
        {
            get { return string.Format("{0:F2} [{1}]", Weight, BaseWeightIn); }
        }

        [NopResourceDisplayName("Admin.Orders.Shipments.TrackingNumber")]
        public string TrackingNumber { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.ShippedDate")]
        public string ShippedDate { get; set; }
        public bool CanShip { get; set; }
        [NopResourceDisplayName("Admin.Orders.Shipments.DeliveryDate")]
        public string DeliveryDate { get; set; }
        public bool CanDeliver { get; set; }
        [Display(Name = "Package Length")]
        public decimal Length { get; set; }
        [Display(Name = "Package Width")]
        public decimal Width { get; set; }
        [Display(Name = "Package Height")]
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public string BaseWeightIn { get; set; }

        public UpsShipmentModel UpsShipment { get; set; }
        public Address ShippingAddress { get; set; }
        public IEnumerable<SelectListItem> AvailableStates { get; set; }

        public List<ShipmentOrderProductVariantModel> Products { get; set; }

		public string CustomerLastName { get; set; }

        public bool DisplayPdfPackagingSlip { get; set; }

        #region Nested classes

        [Serializable]
        public partial class ShipmentOrderProductVariantModel : BaseNopEntityModel
        {
            public int OrderProductVariantId { get; set; }
            public int ProductVariantId { get; set; }
            public string FullProductName { get; set; }
            public string Sku { get; set; }
            public string AttributeInfo { get; set; }
            
            //weight of one item (product variant)
            public string ItemWeight { get; set; }
            public string ItemDimensions { get; set; }

            public int QuantityToAdd { get; set; }
            public int QuantityOrdered { get; set; }
            public int QuantityInThisShipment { get; set; }
			public string Price { get; set; }
            public decimal PriceDecimal { get; set; }
            public int QuantityInAllShipments { get; set; }
        }
        public partial class UpsShipmentModel : BaseNopEntityModel
        {
            public int ShipmentId { get; set; }
            [Display(Name="Packaging Type")]
            public string PackagingType { get; set; }
            public IList<KeyValuePair<string, string>> AvailablePackagingTypes { get; set; }
            [Display(Name = "Declared Value")]
            public decimal DeclaredValue { get; set; }
            [Display(Name = "Shipping Service")]
            public string Service { get; set; }
            public IList<KeyValuePair<string, string>> AvailableServices { get; set; }
            public decimal QuotedPrice { get; set; }
            [Display(Name = "Send Email Notification?")]
            public bool SendEmailNotification { get; set; }
            [Display(Name = "Receive Confirmation of Delivery")]
            public bool ConfirmationDelivery { get; set; }
            [Display(Name = "Require Signature?")]
            public bool RequireSignature { get; set; }
            [Display(Name = "Delivery On Saturday")]
            public bool SaturdayDelivery { get; set; }
            [Display(Name = "Offset Climate Impact")]
            public bool UpsCarbonNeutral { get; set; }
            [Display(Name = "Oversize Package")]
            public bool OversizePackage { get; set; }
            [Display(Name = "Residential Address")]
            public bool ResidentialAddress { get; set; }
        }
        #endregion
    }

}