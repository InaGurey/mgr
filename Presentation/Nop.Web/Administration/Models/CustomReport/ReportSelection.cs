﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Services.CustomReport;



namespace Nop.Admin.Models.CustomReport
{
    public class CustomReportModel
	{
        public IList<SelectListItem> Stores { get; set; }
        public IList<ReportType> ReportTypes { get; set; }

		public CustomReportFormBase Form { get; set; }
	}

    public class ReportType
    {
        public string Name { get; set; }
        public string ReportViewUrl { get; set; }
        public string ReportSpreadsheetUrl { get; set; }
    }

	public class CustomReportFormBase
	{
        public CustomReportFormBase()
        {
            var lastmonth = DateTime.Now.AddMonths(-1);
            StartDate = new DateTime(lastmonth.Year, lastmonth.Month, 1);
            EndDate = StartDate.AddMonths(1).AddDays(-1);
        }

        [UIHint("Date")] public DateTime StartDate { get; set; }
        [UIHint("Date")] public DateTime EndDate { get; set; }
        public string StoreNumber { get; set; }
        public string ReportType { get; set; }

		protected void FillBaseClone(CustomReportFormBase form)
		{
			form.StartDate = this.StartDate;
			form.EndDate = this.EndDate;
			form.StoreNumber = this.StoreNumber;
			form.ReportType = this.ReportType;
		}
	}


    public class TotalSalesModel
    {
        public TotalSalesResult ReportData { get; set; }
        public TotalSalesForm Form { get; set; }
    }

    public class TotalSalesForm : CustomReportFormBase
    {
        public TotalSalesForm()
        {
            Sort = TotalSalesSort.TotalSales;
            IncludeTaxAndShipping = false;
        }

        public bool IncludeTaxAndShipping { get; set; }
        public TotalSalesSort Sort { get; set; }


		public TotalSalesForm WithTaxAndShipping() { return CloneForTaxAndShipping(true); }
		public TotalSalesForm WithoutTaxAndShipping() { return CloneForTaxAndShipping(false); }
		public TotalSalesForm SortByTotalSales() { return CloneForSort(TotalSalesSort.TotalSales); }
		public TotalSalesForm SortByOrders() { return CloneForSort(TotalSalesSort.Orders); }
		public TotalSalesForm SortByAverageSale() { return CloneForSort(TotalSalesSort.AverageSale); }
		public TotalSalesForm SortByStoreNumber() { return CloneForSort(TotalSalesSort.StoreNumber); }
		public TotalSalesForm SortByLocation() { return CloneForSort(TotalSalesSort.Location); }

		public TotalSalesForm CloneForSort(TotalSalesSort sort)
		{
			var form = new TotalSalesForm();
			FillBaseClone(form);
			form.IncludeTaxAndShipping = this.IncludeTaxAndShipping;
			form.Sort = sort;
			return form;
		}

		public TotalSalesForm CloneForTaxAndShipping(bool includeTaxAndShipping)
		{
			var form = new TotalSalesForm();
			FillBaseClone(form);
			form.Sort = this.Sort;
			form.IncludeTaxAndShipping = includeTaxAndShipping;
			return form;
		}
    }


	public class PaymentMethodsModel
	{
		public PaymentMethodsResult ReportData { get; set; }
        public PaymentMethodsForm Form { get; set; }
	}

	public class PaymentMethodsForm : CustomReportFormBase
	{
        public PaymentMethodsForm()
        {
            Display = PaymentMethodsDisplay.Sales;
            Sort = PaymentMethodsSort.Total;
        }

        public bool IncludeTaxAndShipping { get; set; }
        public PaymentMethodsDisplay Display { get; set; }
		public PaymentMethodsSort Sort { get; set; }


        public PaymentMethodsForm WithTaxAndShipping() { return CloneForTaxAndShipping(true); }
        public PaymentMethodsForm WithoutTaxAndShipping() { return CloneForTaxAndShipping(false); }
        public PaymentMethodsForm DisplaySales() { return CloneForDisplay(PaymentMethodsDisplay.Sales); }
		public PaymentMethodsForm DisplayOrders() { return CloneForDisplay(PaymentMethodsDisplay.Orders); }
		public PaymentMethodsForm SortByTotal() { return CloneForSort(PaymentMethodsSort.Total); }
		public PaymentMethodsForm SortByStoreNumber() { return CloneForSort(PaymentMethodsSort.StoreNumber); }
		public PaymentMethodsForm SortByLocation() { return CloneForSort(PaymentMethodsSort.Location); }

		public PaymentMethodsForm CloneForSort(PaymentMethodsSort sort)
		{
			var form = new PaymentMethodsForm();
			FillBaseClone(form);
			form.Display = this.Display;
			form.Sort = sort;
			return form;
		}

		public PaymentMethodsForm CloneForDisplay(PaymentMethodsDisplay display)
		{
			var form = new PaymentMethodsForm();
			FillBaseClone(form);
			form.Sort = this.Sort;
			form.Display = display;
			return form;
		}

        public PaymentMethodsForm CloneForTaxAndShipping(bool includeTaxAndShipping)
        {
            var form = new PaymentMethodsForm();
            FillBaseClone(form);
            form.Sort = this.Sort;
            form.Display = this.Display;
            form.IncludeTaxAndShipping = includeTaxAndShipping;
            return form;
        }
    }


    public class CategorySalesModel
    {
        public CategorySalesResult ReportData { get; set; }
        public CategorySalesForm Form { get; set; }
    }

    public class CategorySalesForm : CustomReportFormBase
    {
        public CategorySalesForm()
        {
            ShowSubcategories = true;
            Display = CategorySalesDisplay.Sales;
            Sort = CategorySalesSort.Total;
        }

        public bool ShowSubcategories { get; set; }
		public CategorySalesDisplay Display { get; set; }
		public CategorySalesSort Sort { get; set; }


		public CategorySalesForm CloneForDisplay(bool showSubcategories)
		{
			var form = new CategorySalesForm();
			FillBaseClone(form);
			form.Display = this.Display;
			form.Sort = this.Sort;
			form.ShowSubcategories = showSubcategories;
			return form;
		}

		public CategorySalesForm CloneForSort(CategorySalesSort sort)
		{
			var form = new CategorySalesForm();
			FillBaseClone(form);
			form.ShowSubcategories = this.ShowSubcategories;
			form.Display = this.Display;
			form.Sort = sort;
			return form;
		}

		public CategorySalesForm CloneForDisplay(CategorySalesDisplay display)
		{
			var form = new CategorySalesForm();
			FillBaseClone(form);
			form.ShowSubcategories = this.ShowSubcategories;
			form.Sort = this.Sort;
			form.Display = display;
			return form;
		}
	}


    public class AccessorySalesModel
    {
        public AccessorySalesResult ReportData { get; set; }
        public AccessorySalesForm Form { get; set; }
    }

    public class AccessorySalesForm : CustomReportFormBase
    {
        public AccessorySalesForm()
        {
            Display = AccessorySalesDisplay.Items;
            Sort = AccessorySalesSort.Total;
        }

		public AccessorySalesDisplay Display { get; set; }
		public AccessorySalesSort Sort { get; set; }


		public AccessorySalesForm CloneForSort(AccessorySalesSort sort)
		{
			var form = new AccessorySalesForm();
			FillBaseClone(form);
			form.Display = this.Display;
			form.Sort = sort;
			return form;
		}

		public AccessorySalesForm CloneForDisplay(AccessorySalesDisplay display)
		{
			var form = new AccessorySalesForm();
			FillBaseClone(form);
			form.Sort = this.Sort;
			form.Display = display;
			return form;
		}
    }

    public class SavedSearchesModel
    {
        public SavedSearchesResult ReportData { get; set; }
        public SavedSearchesForm Form { get; set; }
    }

    public class SavedSearchesForm : CustomReportFormBase
    {
    }

    public class ItemSalesModel
    {
        public ItemSalesResult ReportData { get; set; }
        public ItemSalesForm Form { get; set; }
    }

    public class ItemSalesForm : CustomReportFormBase
    {
    }
}