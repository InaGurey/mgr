﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Nop.Admin.Models.Orders;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Telerik.Web.Mvc;
using WinmarkFranchise.Domain;
using Address = Nop.Core.Domain.Common.Address;

namespace Nop.Admin.Controllers
{
	[AdminAuthorize]
	public partial class OrderController : BaseNopController
    {
        #region Fields

        private readonly IOrderService _orderService;
        private readonly IOrderReportService _orderReportService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IEncryptionService _encryptionService;
        private readonly IPaymentService _paymentService;
        private readonly IMeasureService _measureService;
        private readonly IPdfService _pdfService;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IProductService _productService;
        private readonly IExportManager _exportManager;
        private readonly IPermissionService _permissionService;
	    private readonly IWorkflowMessageService _workflowMessageService;
	    private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
	    private readonly IProductAttributeService _productAttributeService;
	    private readonly IProductAttributeParser _productAttributeParser;
	    private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGiftCardService _giftCardService;
        private readonly IDownloadService _downloadService;
	    private readonly IShipmentService _shipmentService;
	    private readonly IShippingService _shippingService;
	    private readonly IWinmarkFranchiseUnitOfWork _uow;

        private readonly CatalogSettings _catalogSettings;
        private readonly CurrencySettings _currencySettings;
        private readonly TaxSettings _taxSettings;
        private readonly MeasureSettings _measureSettings;
        private readonly PdfSettings _pdfSettings;

	    #endregion

        #region Ctor

        public OrderController(IOrderService orderService, 
            IOrderReportService orderReportService, IOrderProcessingService orderProcessingService,
            IDateTimeHelper dateTimeHelper, IPriceFormatter priceFormatter, ILocalizationService localizationService,
            IWorkContext workContext, ICurrencyService currencyService,
            IEncryptionService encryptionService, IPaymentService paymentService,
            IMeasureService measureService, IPdfService pdfService,
            IAddressService addressService, ICountryService countryService,
            IStateProvinceService stateProvinceService, IProductService productService,
            IExportManager exportManager, IPermissionService permissionService,
            IWorkflowMessageService workflowMessageService,
            ICategoryService categoryService, IManufacturerService manufacturerService,
            IProductAttributeService productAttributeService, IProductAttributeParser productAttributeParser,
            IProductAttributeFormatter productAttributeFormatter, IShoppingCartService shoppingCartService,
            IGiftCardService giftCardService, IDownloadService downloadService,
            IShipmentService shipmentService, IShippingService shippingService, IWinmarkFranchiseUnitOfWork uow,
            CatalogSettings catalogSettings, CurrencySettings currencySettings, TaxSettings taxSettings,
            MeasureSettings measureSettings, PdfSettings pdfSettings)
		{
            this._orderService = orderService;
            this._orderReportService = orderReportService;
            this._orderProcessingService = orderProcessingService;
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = priceFormatter;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._encryptionService = encryptionService;
            this._paymentService = paymentService;
            this._measureService = measureService;
            this._pdfService = pdfService;
            this._addressService = addressService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._productService = productService;
            this._exportManager = exportManager;
            this._permissionService = permissionService;
            this._workflowMessageService = workflowMessageService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._productAttributeService = productAttributeService;
            this._productAttributeParser = productAttributeParser;
            this._productAttributeFormatter = productAttributeFormatter;
            this._shoppingCartService = shoppingCartService;
            this._giftCardService = giftCardService;
            this._downloadService = downloadService;
            this._shipmentService = shipmentService;
            this._shippingService = shippingService;
            this._uow = uow;

            this._catalogSettings = catalogSettings;
            this._currencySettings = currencySettings;
            this._taxSettings = taxSettings;
            this._measureSettings = measureSettings;
            this._pdfSettings = pdfSettings;
		}
        
        #endregion

        #region Utilities

        [NonAction]
        protected void PrepareOrderDetailsModel(OrderModel model, Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = order.Id;
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.OrderGuid = order.OrderGuid;
	        model.StoreNumber = order.StoreNumber ?? "";
	        model.ShowStoreNumber = _permissionService.Authorize("ManageMultipleStores");
            model.CustomerId = order.CustomerId;
            model.CustomerIp = order.CustomerIp;
            model.VatNumber = order.VatNumber;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.DisplayPdfInvoice = _pdfSettings.Enabled;
            model.AllowCustomersToSelectTaxDisplayType = _taxSettings.AllowCustomersToSelectTaxDisplayType;
            model.TaxDisplayType = _taxSettings.TaxDisplayType;
            model.AffiliateId = order.AffiliateId;
	        model.IsInStorePickup = order.IsInStorePickup;
            
            #region Order totals

            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);

            //subtotal
            model.OrderSubtotalInclTax = _priceFormatter.FormatPrice(order.OrderSubtotalInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            model.OrderSubtotalExclTax = _priceFormatter.FormatPrice(order.OrderSubtotalExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            model.OrderSubtotalInclTaxValue = order.OrderSubtotalInclTax;
            model.OrderSubtotalExclTaxValue = order.OrderSubtotalExclTax;
            //discount (applied to order subtotal)
            string orderSubtotalDiscountInclTaxStr = _priceFormatter.FormatPrice(order.OrderSubTotalDiscountInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            string orderSubtotalDiscountExclTaxStr = _priceFormatter.FormatPrice(order.OrderSubTotalDiscountExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            if (order.OrderSubTotalDiscountInclTax > decimal.Zero)
                model.OrderSubTotalDiscountInclTax = orderSubtotalDiscountInclTaxStr;
            if (order.OrderSubTotalDiscountExclTax > decimal.Zero)
                model.OrderSubTotalDiscountExclTax = orderSubtotalDiscountExclTaxStr;
            model.OrderSubTotalDiscountInclTaxValue = order.OrderSubTotalDiscountInclTax;
            model.OrderSubTotalDiscountExclTaxValue = order.OrderSubTotalDiscountExclTax;

            //shipping
            model.OrderShippingInclTax = _priceFormatter.FormatShippingPrice(order.OrderShippingInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            model.OrderShippingExclTax = _priceFormatter.FormatShippingPrice(order.OrderShippingExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            model.OrderShippingInclTaxValue = order.OrderShippingInclTax;
            model.OrderShippingExclTaxValue = order.OrderShippingExclTax;

            //payment method additional fee
            if (order.PaymentMethodAdditionalFeeInclTax > decimal.Zero)
            {
                model.PaymentMethodAdditionalFeeInclTax = _priceFormatter.FormatPaymentMethodAdditionalFee(order.PaymentMethodAdditionalFeeInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
                model.PaymentMethodAdditionalFeeExclTax = _priceFormatter.FormatPaymentMethodAdditionalFee(order.PaymentMethodAdditionalFeeExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            }
            model.PaymentMethodAdditionalFeeInclTaxValue = order.PaymentMethodAdditionalFeeInclTax;
            model.PaymentMethodAdditionalFeeExclTaxValue = order.PaymentMethodAdditionalFeeExclTax;


            //tax
            model.Tax = _priceFormatter.FormatPrice(order.OrderTax, true, false);
            SortedDictionary<decimal, decimal> taxRates = order.TaxRatesDictionary;
            bool displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
            bool displayTax = !displayTaxRates;
            foreach (var tr in order.TaxRatesDictionary)
            {
                model.TaxRates.Add(new OrderModel.TaxRate()
                {
                    Rate = _priceFormatter.FormatTaxRate(tr.Key),
                    Value = _priceFormatter.FormatPrice(tr.Value, true, false),
                });
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.TaxValue = order.OrderTax;
            model.TaxRatesValue = order.TaxRates;

            //discount
            if (order.OrderDiscount > 0)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-order.OrderDiscount, true, false);
            model.OrderTotalDiscountValue = order.OrderDiscount;

            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderModel.GiftCard()
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-gcuh.UsedValue, true, false),
                });
            }

            //reward points
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-order.RedeemedRewardPointsEntry.UsedAmount, true, false);
            }

            //total
            model.OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false);
            model.OrderTotalValue = order.OrderTotal;

            //refunded amount
            if (order.RefundedAmount > decimal.Zero)
                model.RefundedAmount = _priceFormatter.FormatPrice(order.RefundedAmount, true, false);


            #endregion

            #region Payment info

            if (order.AllowStoringCreditCardNumber)
            {
                //card type
                model.CardType = _encryptionService.DecryptText(order.CardType);
                //cardholder name
                model.CardName = _encryptionService.DecryptText(order.CardName);
                //card number
                model.CardNumber = _encryptionService.DecryptText(order.CardNumber);
                //cvv
                model.CardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                //expiry date
                string cardExpirationMonthDecrypted = _encryptionService.DecryptText(order.CardExpirationMonth);
                if (!String.IsNullOrEmpty(cardExpirationMonthDecrypted) && cardExpirationMonthDecrypted != "0")
                    model.CardExpirationMonth = cardExpirationMonthDecrypted;
                string cardExpirationYearDecrypted = _encryptionService.DecryptText(order.CardExpirationYear);
                if (!String.IsNullOrEmpty(cardExpirationYearDecrypted) && cardExpirationYearDecrypted != "0")
                    model.CardExpirationYear = cardExpirationYearDecrypted;

                model.AllowStoringCreditCardNumber = true;
            }
            else
            {
                string maskedCreditCardNumberDecrypted = _encryptionService.DecryptText(order.MaskedCreditCardNumber);
                if (!String.IsNullOrEmpty(maskedCreditCardNumberDecrypted))
                    model.CardNumber = maskedCreditCardNumberDecrypted;
            }


            //purchase order number (we have to find a better to inject this information because it's related to a certain plugin)
            var pm = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            if (pm != null && pm.PluginDescriptor.SystemName.Equals("Payments.PurchaseOrder", StringComparison.InvariantCultureIgnoreCase))
            {
                model.DisplayPurchaseOrderNumber = true;
                model.PurchaseOrderNumber = order.PurchaseOrderNumber;
            }

            //payment transaction info
            model.AuthorizationTransactionId = order.AuthorizationTransactionId;
            model.CaptureTransactionId = order.CaptureTransactionId;
            model.SubscriptionTransactionId = order.SubscriptionTransactionId;

            //payment method info
            model.PaymentMethod = pm != null ? pm.PluginDescriptor.FriendlyName : order.PaymentMethodSystemName;
            model.PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);

            //payment method buttons
            model.CanCancelOrder = _orderProcessingService.CanCancelOrder(order);
            model.CanCapture = _orderProcessingService.CanCapture(order);
            model.CanMarkOrderAsPaid = _orderProcessingService.CanMarkOrderAsPaid(order);
            model.CanRefund = _orderProcessingService.CanRefund(order);
            model.CanRefundOffline = _orderProcessingService.CanRefundOffline(order);
            model.CanPartiallyRefund = _orderProcessingService.CanPartiallyRefund(order, decimal.Zero);
            model.CanPartiallyRefundOffline = _orderProcessingService.CanPartiallyRefundOffline(order, decimal.Zero);
            model.CanVoid = _orderProcessingService.CanVoid(order);
            model.CanVoidOffline = _orderProcessingService.CanVoidOffline(order);
            
            model.PrimaryStoreCurrencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode;
            model.MaxAmountToRefund = order.OrderTotal - order.RefundedAmount;

            #endregion

            #region Billing & shipping info

            model.BillingAddress = order.BillingAddress.ToModel();
            if (order.BillingAddress.Country != null)
                model.BillingAddress.CountryName = order.BillingAddress.Country.Name;
            if (order.BillingAddress.StateProvince != null)
                model.BillingAddress.StateProvinceName = order.BillingAddress.StateProvince.Name;

	        model.ShippingStatus = order.ShippingStatus;
            model.ShippingStatusText = order.ShippingStatusText;

            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;

                model.ShippingAddress = order.ShippingAddress.ToModel();
                if (order.ShippingAddress.Country != null)
                    model.ShippingAddress.CountryName = order.ShippingAddress.Country.Name;
                if (order.ShippingAddress.StateProvince != null)
                    model.ShippingAddress.StateProvinceName = order.ShippingAddress.StateProvince.Name;

                model.ShippingMethod = order.ShippingMethod;

                model.ShippingAddressGoogleMapsUrl = string.Format("http://maps.google.com/maps?f=q&hl=en&ie=UTF8&oe=UTF8&geocode=&q={0}", Server.UrlEncode(order.ShippingAddress.Address1 + " " + order.ShippingAddress.ZipPostalCode + " " + order.ShippingAddress.City + " " + (order.ShippingAddress.Country != null ? order.ShippingAddress.Country.Name : "")));
                model.CanAddNewShipments = order.HasItemsToAddToShipment();
            }

            #endregion

            #region Products
            var baseWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            var baseWeightIn = baseWeight != null ? baseWeight.Name : "";
            var baseDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var baseDimensionIn = baseDimension != null ? baseDimension.Name : "";
            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;
            bool hasDownloadableItems = false;
            foreach (var opv in order.OrderProductVariants)
            {
                if (opv.ProductVariant != null && opv.ProductVariant.IsDownload)
                    hasDownloadableItems = true;
                var maxQty = opv.GetTotalNumberOfItemsCanBeAddedToShipment();

                var opvModel = new OrderModel.OrderProductVariantModel()
                {
                    Id = opv.Id,
                    ProductId = opv.ProductVariant.ProductId,
                    ProductVariantId = opv.ProductVariantId,
                    Sku = opv.ProductVariant.Sku,
                    Quantity = opv.Quantity,
                    IsDownload = opv.ProductVariant.IsDownload,
                    DownloadCount = opv.DownloadCount,
                    DownloadActivationType = opv.ProductVariant.DownloadActivationType,
                    IsDownloadActivated = opv.IsDownloadActivated,
                    LicenseDownloadId = opv.LicenseDownloadId,
                    ItemWeight = opv.ItemWeight.HasValue ? string.Format("{0:F2} [{1}]", opv.ItemWeight, baseWeightIn) : "",
                    ItemDimensions = string.Format("{0:F2} x {1:F2} x {2:F2} [{3}]", opv.ProductVariant.Length, opv.ProductVariant.Width, opv.ProductVariant.Height, baseDimensionIn),
                    QuantityToShip = maxQty,
                    QuantityToAdd = maxQty
                };

                //product name
                if (!String.IsNullOrEmpty(opv.ProductVariant.Name))
                    opvModel.FullProductName = string.Format("{0} ({1})", opv.ProductVariant.Product.Name, opv.ProductVariant.Name);
                else
                    opvModel.FullProductName = opv.ProductVariant.Product.Name;

                //unit price
                opvModel.UnitPriceInclTaxValue = opv.UnitPriceInclTax;
                opvModel.UnitPriceExclTaxValue = opv.UnitPriceExclTax;
                opvModel.UnitPriceInclTax = _priceFormatter.FormatPrice(opv.UnitPriceInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                opvModel.UnitPriceExclTax = _priceFormatter.FormatPrice(opv.UnitPriceExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);
                //discounts
                opvModel.DiscountInclTaxValue = opv.DiscountAmountInclTax;
                opvModel.DiscountExclTaxValue = opv.DiscountAmountExclTax;
                opvModel.DiscountInclTax = _priceFormatter.FormatPrice(opv.DiscountAmountInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                opvModel.DiscountExclTax = _priceFormatter.FormatPrice(opv.DiscountAmountExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);
                //subtotal
                opvModel.SubTotalInclTaxValue = opv.PriceInclTax;
                opvModel.SubTotalExclTaxValue = opv.PriceExclTax;
                opvModel.SubTotalInclTax = _priceFormatter.FormatPrice(opv.PriceInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                opvModel.SubTotalExclTax = _priceFormatter.FormatPrice(opv.PriceExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);

                opvModel.AttributeInfo = opv.AttributeDescription;
                if (opv.ProductVariant.IsRecurring)
                    opvModel.RecurringInfo = string.Format(_localizationService.GetResource("Admin.Orders.Products.RecurringPeriod"), opv.ProductVariant.RecurringCycleLength, opv.ProductVariant.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                //return requests
                opvModel.ReturnRequestIds = _orderService.SearchReturnRequests(0, opv.Id, null)
                    .Select(rr=> rr.Id).ToList();
                //gift cards
                opvModel.PurchasedGiftCardIds = _giftCardService.GetGiftCardsByPurchasedWithOrderProductVariantId(opv.Id)
                    .Select(gc => gc.Id).ToList();

                model.Items.Add(opvModel);
            }
            model.HasDownloadableProducts = hasDownloadableItems;
            #endregion
        }

        [NonAction]
        protected OrderModel.AddOrderProductModel.ProductDetailsModel PrepareAddProductToOrderModel(int orderId, int productVariantId)
        {

            var productVariant = _productService.GetProductVariantById(productVariantId);
            var model = new OrderModel.AddOrderProductModel.ProductDetailsModel()
            {
                ProductVariantId = productVariantId,
                OrderId = orderId,
                Name = productVariant.FullProductName,
                UnitPriceExclTax = decimal.Zero,
                UnitPriceInclTax = decimal.Zero,
                Quantity = 1,
                SubTotalExclTax = decimal.Zero,
                SubTotalInclTax = decimal.Zero
            };

            //attributes
            var productVariantAttributes = _productAttributeService.GetProductVariantAttributesByProductVariantId(productVariant.Id);
            foreach (var attribute in productVariantAttributes)
            {
                var pvaModel = new OrderModel.AddOrderProductModel.ProductVariantAttributeModel()
                {
                    Id = attribute.Id,
                    ProductAttributeId = attribute.ProductAttributeId,
                    Name = attribute.ProductAttribute.Name,
                    TextPrompt = attribute.TextPrompt,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var pvaValues = _productAttributeService.GetProductVariantAttributeValues(attribute.Id);
                    foreach (var pvaValue in pvaValues)
                    {
                        var pvaValueModel = new OrderModel.AddOrderProductModel.ProductVariantAttributeValueModel()
                        {
                            Id = pvaValue.Id,
                            Name = pvaValue.Name,
                            IsPreSelected = pvaValue.IsPreSelected
                        };
                        pvaModel.Values.Add(pvaValueModel);
                    }
                }

                model.ProductVariantAttributes.Add(pvaModel);
            }
            //gift card
            model.GiftCard.IsGiftCard = productVariant.IsGiftCard;
            if (model.GiftCard.IsGiftCard)
            {
                model.GiftCard.GiftCardType = productVariant.GiftCardType;
            }
            return model;
        }

        [NonAction]
        protected ShipmentModel PrepareShipmentModel(Shipment shipment, bool prepareProducts)
        {
            //measures
            var baseWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            var baseWeightIn = baseWeight != null ? baseWeight.Name : "";
            var baseDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var baseDimensionIn = baseDimension != null ? baseDimension.Name : "";

            var model = new ShipmentModel()
            {
                Id = shipment.Id,
                OrderId = shipment.OrderId,
                TrackingNumber = shipment.TrackingNumber,
				CustomerLastName = shipment.Order.ShippingAddress.LastName,
                BaseWeightIn = baseWeightIn,
                Weight = shipment.TotalWeight.HasValue ? shipment.TotalWeight.Value : 0m,
                ShippedDate = shipment.ShippedDateUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc).ToString() : _localizationService.GetResource("Admin.Orders.Shipments.ShippedDate.NotYet"),
                CanShip = !shipment.ShippedDateUtc.HasValue,
                DeliveryDate = shipment.DeliveryDateUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc).ToString() : _localizationService.GetResource("Admin.Orders.Shipments.DeliveryDate.NotYet"),
                CanDeliver = shipment.ShippedDateUtc.HasValue && !shipment.DeliveryDateUtc.HasValue,
                DisplayPdfPackagingSlip = _pdfSettings.Enabled,
            };

            if (shipment.UpsShipment != null)
            {
                var ups = _shippingService.LoadShippingRateComputationMethodBySystemName("Shipping.UPS");
                model.UpsShipment = new ShipmentModel.UpsShipmentModel
                {
                    ShipmentId = shipment.Id,
                    PackagingType = shipment.UpsShipment.PackagingType,
                    DeclaredValue =
                        shipment.UpsShipment.DeclaredValue.HasValue ? shipment.UpsShipment.DeclaredValue.Value : 0m,
                    Service = shipment.UpsShipment.Service,
                    QuotedPrice =
                        shipment.UpsShipment.QuotedPrice.HasValue ? shipment.UpsShipment.QuotedPrice.Value : 0m,
                    SendEmailNotification = shipment.UpsShipment.SendEmailNotification,
                    ConfirmationDelivery = shipment.UpsShipment.ConfirmationDelivery,
                    RequireSignature = shipment.UpsShipment.RequireSignature,
                    SaturdayDelivery = shipment.UpsShipment.SaturdayDelivery,
                    UpsCarbonNeutral = shipment.UpsShipment.UpsCarbonNeutral,
                    OversizePackage = shipment.UpsShipment.OversizePackage.HasValue && shipment.UpsShipment.OversizePackage.Value,
                    ResidentialAddress = shipment.UpsShipment.ResidentialAddress.HasValue && shipment.UpsShipment.ResidentialAddress.Value,
                    AvailableServices = ups.GetAllShippingOptionsAdmin()
                        .ShippingOptions.Select(s => new KeyValuePair<string, string>(s.Name, s.Description))
                        .ToList()
                };
            }

            if (prepareProducts)
            {
                foreach (var sopv in shipment.ShipmentOrderProductVariants)
                {
                    var opv = _orderService.GetOrderProductVariantById(sopv.OrderProductVariantId);
                    if (opv == null)
                        continue;

                    //quantities
                    var qtyInThisShipment = sopv.Quantity;
                    var maxQtyToAdd = opv.GetTotalNumberOfItemsCanBeAddedToShipment();
                    var qtyOrdered = opv.Quantity;
                    var qtyInAllShipments = opv.GetTotalNumberOfItemsInAllShipment();

                    var sopvModel = new ShipmentModel.ShipmentOrderProductVariantModel()
                    {
                        Id = sopv.Id,
                        OrderProductVariantId = opv.Id,
                        ProductVariantId = opv.ProductVariantId,
                        Sku = opv.ProductVariant.Sku,
                        AttributeInfo = opv.AttributeDescription,
                        ItemWeight = opv.ItemWeight.HasValue ? string.Format("{0:F2} [{1}]", opv.ItemWeight, baseWeightIn) : "",
                        ItemDimensions = string.Format("{0:F2} x {1:F2} x {2:F2} [{3}]", opv.ProductVariant.Length, opv.ProductVariant.Width, opv.ProductVariant.Height, baseDimensionIn),
                        QuantityOrdered = qtyOrdered,
                        QuantityInThisShipment = qtyInThisShipment,
						Price = (qtyInThisShipment * opv.PriceExclTax).ToString("C"),
                        QuantityInAllShipments = qtyInAllShipments,
                        QuantityToAdd = maxQtyToAdd,
                    };

                    //product name
                    if (!String.IsNullOrEmpty(opv.ProductVariant.Name))
                        sopvModel.FullProductName = string.Format("{0} ({1})", opv.ProductVariant.Product.Name,
                                                                  opv.ProductVariant.Name);
                    else
                        sopvModel.FullProductName = opv.ProductVariant.Product.Name;
                    model.Products.Add(sopvModel);
                }
            }
            return model;
        }

        #endregion

        #region Order list

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

		[HttpPost]
		public ActionResult List(OrderListModel model, string downloadExcel) {
			if (downloadExcel != null)
				return ExportExcelAll(model);

			return List();
		}

        //Create Search Action Method that takes the editor string method and receives the "SearchBox" criteria
        //public ActionResult Search(string SearchBox)
        //{
        ////    //Create LINQ Statement(Select * from Orders O where O.LastName = SearchBox
        ////    //var OrderAttempt = (from oa in db.OrderAttempt
        ////    //             where oa.Name.Contains(SearchBox)
        ////    //             || oa.Email.Contains(SearchBox)
        ////    //             select oa).ToList();
        ////    //var OrderAttempt = _orderService.SearchFailedOrderAttempts()

        ////    //Call the BlockedList index and pass it to the Order Controller-
        ////    //return View("BlockedList", OrderAttempt);

        //    var attempts = _orderService.SearchFailedOrderAttempts(0,50, SearchBox);
        //    var gridModel = new GridModel<BlockedCustomerModel>
        //    {
        //        Data = attempts.Select(a => new BlockedCustomerModel
        //        {
        //            Id = a.Id,
        //            AttemptedAt = _dateTimeHelper.ConvertToUserTime(a.AttemptedAtUtc, DateTimeKind.Utc),
        //            Email = a.Email,
        //            IpAddress = a.IpAddress,
        //            Name = a.Name
        //        }),
        //        Total = attempts.TotalCount
        //    };
        //    return new JsonResult { Data = gridModel
        //};
//}



	    public ActionResult BlockedList()
	    {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

	        return View();
	    }

		[GridAction(EnableCustomBinding = true)]
	    public ActionResult OrderBlockedList(GridCommand command, string searchBox)
	    {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var attempts = _orderService.SearchFailedOrderAttempts(command.Page - 1, command.PageSize, searchBox);
            var gridModel = new GridModel<BlockedCustomerModel> {
		        Data = attempts.Select(a => new BlockedCustomerModel {
		            Id = a.Id,
		            AttemptedAt = _dateTimeHelper.ConvertToUserTime(a.AttemptedAtUtc, DateTimeKind.Utc),
		            Email = a.Email,
		            IpAddress = a.IpAddress,
		            Name = a.Name
		        }),
		        Total = attempts.TotalCount
		    };
		    return new JsonResult {Data = gridModel};
	    }

	    public ActionResult Unblock(string ipAddress)
	    {
	        _orderService.ClearBlock(ipAddress);
	        return RedirectToAction("BlockedList");
	    }

		[HttpGet]
        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new OrderListModel();
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            model.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.AvailableShippingStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

	        model.ShowStoreNumber = _permissionService.Authorize("ManageMultipleStores");

            return View(model);
		}

		[GridAction(EnableCustomBinding = true)]
		public ActionResult OrderList(GridCommand command, OrderListModel model)
        {
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
				return AccessDeniedView();

			DateTime? startDateValue = (model.StartDate == null) ? null
							: (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

			DateTime? endDateValue = (model.EndDate == null) ? null
							: (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

			OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
			PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
			ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

			var orders = _orderService.SearchOrders(
				startDateValue, endDateValue, orderStatus, paymentStatus, shippingStatus, model.CustomerEmail,
				model.StoreNumber, model.OrderGuid, command.Page - 1, command.PageSize);			

            var gridModel = new GridModel<OrderModel>
            {
                Data = orders.Select(order =>
                {
                    return new OrderModel()
                    {
                        Id = order.Id,
						OrderGuid = order.OrderGuid,
						TransID = order.AuthorizationCodeNormalized,
						StoreNumber = order.StoreNumber ?? "",
						OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
                        OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
						ShippingStatus = order.ShippingStatus,
                        ShippingStatusText = order.ShippingStatusText,
                        CustomerEmail = order.BillingAddress.Email,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc)
                    };
                }),
                Total = orders.TotalCount
            };

            //summary report
            //implemented as a workaround described here: http://www.telerik.com/community/forums/aspnet-mvc/grid/gridmodel-aggregates-how-to-use.aspx
		    var reportSummary = _orderReportService.GetOrderAverageReportLine
		        (orderStatus, paymentStatus, shippingStatus, startDateValue, endDateValue, model.CustomerEmail);
		    var profit = _orderReportService.ProfitReport
                (orderStatus, paymentStatus, shippingStatus, startDateValue, endDateValue, model.CustomerEmail);
		    var aggregator = new OrderModel()
		    {
                aggregatorprofit = _priceFormatter.FormatPrice(profit, true, false),
                aggregatortax = _priceFormatter.FormatPrice(reportSummary.SumTax, true, false),
		        aggregatortotal = _priceFormatter.FormatPrice(reportSummary.SumOrders, true, false)
		    };
		    gridModel.Aggregates = aggregator;
			return new JsonResult
			{
				Data = gridModel
			};
		}
        
        public ActionResult GoToOrder(string id) {
	        int numId;
	        if (int.TryParse(id, out numId)) {
		        var order = _orderService.GetOrderById(numId);
		        if (order != null)
			        return RedirectToAction("Edit", "Order", new { id = order.Id });
	        }
	        return RedirectToAction("List");
        }

        #endregion

        #region Export / Import

        public ActionResult ExportXmlAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            try
            {
                var orders = _orderService.SearchOrders(null, null, null, null, null, null, null, null, 0, int.MaxValue);
                var fileName = string.Format("orders_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
                var xml = _exportManager.ExportOrdersToXml(orders);
                return new XmlDownloadResult(xml, fileName);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        public ActionResult ExportXmlSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var orders = new List<Order>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                orders.AddRange(_orderService.GetOrdersByIds(ids));
            }

            var fileName = string.Format("orders_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            var xml = _exportManager.ExportOrdersToXml(orders);
            return new XmlDownloadResult(xml, fileName);
        }

		public ActionResult ExportExcelAll(OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            try
            {
				DateTime? startDateValue = (model.StartDate == null) ? null
				: (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

				DateTime? endDateValue = (model.EndDate == null) ? null
								: (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

				OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
				PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
				ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

				var orders = _orderService.SearchOrders(
					startDateValue, endDateValue, orderStatus, paymentStatus, shippingStatus, model.CustomerEmail,
					model.StoreNumber, model.OrderGuid, 0, int.MaxValue);

                string fileName = string.Format("orders_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
                string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);

                _exportManager.ExportOrdersToXlsx(filePath, orders);

                var bytes = System.IO.File.ReadAllBytes(filePath);
                return File(bytes, "text/xls", fileName);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        public ActionResult ExportExcelSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var orders = new List<Order>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                orders.AddRange(_orderService.GetOrdersByIds(ids));
            }

            string fileName = string.Format("orders_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
            string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);

            _exportManager.ExportOrdersToXlsx(filePath, orders);

            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "text/xls", fileName);
        }

        #endregion

        #region Order details

        #region Payments and other order workflow

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("cancelorder")]
        public ActionResult CancelOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");
            
            try
            {
                _orderProcessingService.CancelOrder(order, true);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("markreadyforpickup")]
		public ActionResult MarkReadyForPickup(int id) {
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
				return AccessDeniedView();

			var order = _orderService.GetOrderById(id);
			if (order == null)
				//No order found with the specified id
				return RedirectToAction("List");

			try {
				_orderProcessingService.MarkOrderReadyForPickup(order);
				var model = new OrderModel();
				PrepareOrderDetailsModel(model, order);
				return View(model);
			}
			catch (Exception exc) {
				//error
				var model = new OrderModel();
				PrepareOrderDetailsModel(model, order);
				ErrorNotification(exc, false);
				return View(model);
			}
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("markpickedup")]
		public ActionResult MarkPickedUp(int id) {
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
				return AccessDeniedView();

			var order = _orderService.GetOrderById(id);
			if (order == null)
				//No order found with the specified id
				return RedirectToAction("List");

			try {
				_orderProcessingService.MarkOrderPickedUp(order);
				var model = new OrderModel();
				PrepareOrderDetailsModel(model, order);
				return View(model);
			}
			catch (Exception exc) {
				//error
				var model = new OrderModel();
				PrepareOrderDetailsModel(model, order);
				ErrorNotification(exc, false);
				return View(model);
			}
		}

		[HttpPost, ActionName("Edit")]
        [FormValueRequired("captureorder")]
        public ActionResult CaptureOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");
            
            try
            {
                var errors = _orderProcessingService.Capture(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }

        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("markorderaspaid")]
        public ActionResult MarkOrderAsPaid(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");
            
            try
            {
                _orderProcessingService.MarkOrderAsPaid(order);
                _orderProcessingService.SendCustomerReceipt(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("refundorder")]
        public ActionResult RefundOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            try
            {
                var errors = _orderProcessingService.Refund(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("refundorderoffline")]
        public ActionResult RefundOrderOffline(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            try
            {
                _orderProcessingService.RefundOffline(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("voidorder")]
        public ActionResult VoidOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            try
            {
                var errors = _orderProcessingService.Void(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("voidorderoffline")]
        public ActionResult VoidOrderOffline(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            try
            {
                _orderProcessingService.VoidOffline(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }
        
        public ActionResult PartiallyRefundOrderPopup(int id, bool online)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            return View(model);
        }

        [HttpPost]
        [FormValueRequired("partialrefundorder")]
        public ActionResult PartiallyRefundOrderPopup(string btnId, string formId, int id, bool online, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            try
            {
                decimal amountToRefund = model.AmountToRefund;
                if (amountToRefund <= decimal.Zero)
                    throw new NopException("Enter amount to refund");

                decimal maxAmountToRefund = order.OrderTotal - order.RefundedAmount;
                if (amountToRefund > maxAmountToRefund)
                    amountToRefund = maxAmountToRefund;

                var errors = new List<string>();
                if (online)
                    errors = _orderProcessingService.PartiallyRefund(order, amountToRefund).ToList();
                else
                    _orderProcessingService.PartiallyRefundOffline(order, amountToRefund);

                if (errors.Count == 0)
                {
                    //success
                    ViewBag.RefreshPage = true;
                    ViewBag.btnId = btnId;
                    ViewBag.formId = formId;

                    PrepareOrderDetailsModel(model, order);
                    return View(model);
                }
                else
                {
                    //error
                    PrepareOrderDetailsModel(model, order);
                    foreach (var error in errors)
                        ErrorNotification(error, false);
                    return View(model);
                }
            }
            catch (Exception exc)
            {
                //error
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        #endregion

        #region Edit, delete

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null || order.Deleted)
                //No order found with the specified id
                return RedirectToAction("List");

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            _orderProcessingService.DeleteOrder(order);
            return RedirectToAction("List");
        }

        public ActionResult PdfInvoice(int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            var orders = new List<Order>();
            orders.Add(order);
            string fileName = string.Format("order_{0}_{1}.pdf", order.OrderGuid, DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);
            _pdfService.PrintOrdersToPdf(orders, _workContext.WorkingLanguage, filePath);
            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "application/pdf", fileName);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnSaveCC")]
        public ActionResult EditCreditCardInfo(int id, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            if (order.AllowStoringCreditCardNumber)
            {
                string cardType = model.CardType;
                string cardName = model.CardName;
                string cardNumber = model.CardNumber;
                string cardCvv2 = model.CardCvv2;
                string cardExpirationMonth = model.CardExpirationMonth;
                string cardExpirationYear = model.CardExpirationYear;

                order.CardType = _encryptionService.EncryptText(cardType);
                order.CardName = _encryptionService.EncryptText(cardName);
                order.CardNumber = _encryptionService.EncryptText(cardNumber);
                order.MaskedCreditCardNumber = _encryptionService.EncryptText(_paymentService.GetMaskedCreditCardNumber(cardNumber));
                order.CardCvv2 = _encryptionService.EncryptText(cardCvv2);
                order.CardExpirationMonth = _encryptionService.EncryptText(cardExpirationMonth);
                order.CardExpirationYear = _encryptionService.EncryptText(cardExpirationYear);
                _orderService.UpdateOrder(order);
            }

            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnSaveOrderTotals")]
        public ActionResult EditOrderTotals(int id, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            order.OrderSubtotalInclTax = model.OrderSubtotalInclTaxValue;
            order.OrderSubtotalExclTax = model.OrderSubtotalExclTaxValue;
            order.OrderSubTotalDiscountInclTax = model.OrderSubTotalDiscountInclTaxValue;
            order.OrderSubTotalDiscountExclTax = model.OrderSubTotalDiscountExclTaxValue;
            order.OrderShippingInclTax = model.OrderShippingInclTaxValue;
            order.OrderShippingExclTax = model.OrderShippingExclTaxValue;
            order.PaymentMethodAdditionalFeeInclTax = model.PaymentMethodAdditionalFeeInclTaxValue;
            order.PaymentMethodAdditionalFeeExclTax = model.PaymentMethodAdditionalFeeExclTaxValue;
            order.TaxRates = model.TaxRatesValue;
            order.OrderTax = model.TaxValue;
            order.OrderDiscount = model.OrderTotalDiscountValue;
            order.OrderTotal = model.OrderTotalValue;
            _orderService.UpdateOrder(order);

            PrepareOrderDetailsModel(model, order);
            return View(model);
        }
        
        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnSaveOpv")]
        [ValidateInput(false)]
        public ActionResult EditOrderProductVariant(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            ViewData["selectedTab"] = "products";

            //get order product variant identifier
            int opvId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnSaveOpv", StringComparison.InvariantCultureIgnoreCase))
                    opvId = Convert.ToInt32(formValue.Substring("btnSaveOpv".Length));

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == opvId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");


            decimal unitPriceInclTax, unitPriceExclTax, discountInclTax, discountExclTax,priceInclTax,priceExclTax;
            int quantity;
            if (!decimal.TryParse(form["pvUnitPriceInclTax" + opvId], out unitPriceInclTax))
                unitPriceInclTax =orderProductVariant.UnitPriceInclTax;
            if (!decimal.TryParse(form["pvUnitPriceExclTax" + opvId], out unitPriceExclTax))
                unitPriceExclTax = orderProductVariant.UnitPriceExclTax;
            if (!int.TryParse(form["pvQuantity" + opvId], out quantity))
                quantity = orderProductVariant.Quantity;
            if (!decimal.TryParse(form["pvDiscountInclTax" + opvId], out discountInclTax))
                discountInclTax = orderProductVariant.DiscountAmountInclTax;
            if (!decimal.TryParse(form["pvDiscountExclTax" + opvId], out discountExclTax))
                discountExclTax = orderProductVariant.DiscountAmountExclTax;
            if (!decimal.TryParse(form["pvPriceInclTax" + opvId], out priceInclTax))
                priceInclTax = orderProductVariant.PriceInclTax;
            if (!decimal.TryParse(form["pvPriceExclTax" + opvId], out priceExclTax))
                priceExclTax = orderProductVariant.PriceExclTax;

            if (quantity > 0)
            {
                orderProductVariant.UnitPriceInclTax = unitPriceInclTax;
                orderProductVariant.UnitPriceExclTax = unitPriceExclTax;
                orderProductVariant.Quantity = quantity;
                orderProductVariant.DiscountAmountInclTax = discountInclTax;
                orderProductVariant.DiscountAmountExclTax = discountExclTax;
                orderProductVariant.PriceInclTax = priceInclTax;
                orderProductVariant.PriceExclTax = priceExclTax;
                _orderService.UpdateOrder(order);
            }
            else
            {
                _orderService.DeleteOrderProductVariant(orderProductVariant);
            }

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnDeleteOpv")]
        [ValidateInput(false)]
        public ActionResult DeleteOrderProductVariant(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            ViewData["selectedTab"] = "products";

            //get order product variant identifier
            int opvId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnDeleteOpv", StringComparison.InvariantCultureIgnoreCase))
                    opvId = Convert.ToInt32(formValue.Substring("btnDeleteOpv".Length));

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == opvId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");

            _orderService.DeleteOrderProductVariant(orderProductVariant);

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnResetDownloadCount")]
        [ValidateInput(false)]
        public ActionResult ResetDownloadCount(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            ViewData["selectedTab"] = "products";

            //get order product variant identifier
            int opvId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnResetDownloadCount", StringComparison.InvariantCultureIgnoreCase))
                    opvId = Convert.ToInt32(formValue.Substring("btnResetDownloadCount".Length));

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == opvId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");

            orderProductVariant.DownloadCount = 0;
            _orderService.UpdateOrder(order);

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnPvActivateDownload")]
        [ValidateInput(false)]
        public ActionResult ActivateDownloadOrderProductVariant(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            ViewData["selectedTab"] = "products";

            //get order product variant identifier
            int opvId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnPvActivateDownload", StringComparison.InvariantCultureIgnoreCase))
                    opvId = Convert.ToInt32(formValue.Substring("btnPvActivateDownload".Length));

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == opvId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");

            orderProductVariant.IsDownloadActivated = !orderProductVariant.IsDownloadActivated;
            _orderService.UpdateOrder(order);

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        public ActionResult UploadLicenseFilePopup(int id, int opvId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == opvId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");
            
            if (!orderProductVariant.ProductVariant.IsDownload)
                throw new ArgumentException("Product variant is not downloadable");

            var model = new OrderModel.UploadLicenseModel()
            {
                LicenseDownloadId = orderProductVariant.LicenseDownloadId.HasValue ? orderProductVariant.LicenseDownloadId.Value : 0,
                OrderId = order.Id,
                OrderProductVariantId = orderProductVariant.Id
            };

            return View(model);
        }

        [HttpPost]
        [FormValueRequired("uploadlicense")]
        public ActionResult UploadLicenseFilePopup(string btnId, string formId, OrderModel.UploadLicenseModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == model.OrderProductVariantId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");

            //attach license
            if (model.LicenseDownloadId > 0)
                orderProductVariant.LicenseDownloadId = model.LicenseDownloadId;
            else
                orderProductVariant.LicenseDownloadId = null;
            _orderService.UpdateOrder(order);

            //success
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            return View(model);
        }

        [HttpPost, ActionName("UploadLicenseFilePopup")]
        [FormValueRequired("deletelicense")]
        public ActionResult DeleteLicenseFilePopup(string btnId, string formId, OrderModel.UploadLicenseModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var orderProductVariant = order.OrderProductVariants.Where(x => x.Id == model.OrderProductVariantId).FirstOrDefault();
            if (orderProductVariant == null)
                throw new ArgumentException("No order product variant found with the specified id");

            //attach license
            orderProductVariant.LicenseDownloadId = null;
            _orderService.UpdateOrder(order);

            //success
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            return View(model);
        }

		//public ActionResult AddProductToOrder(int orderId)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
		//		return AccessDeniedView();

		//	var model = new OrderModel.AddOrderProductModel();
		//	model.OrderId = orderId;
		//	//categories
		//	model.AvailableCategories.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
		//	foreach (var c in _categoryService.GetAllCategories(true))
		//		model.AvailableCategories.Add(new SelectListItem() { Text = c.GetCategoryNameWithPrefix(_categoryService), Value = c.Id.ToString() });

		//	//manufacturers
		//	model.AvailableManufacturers.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
		//	foreach (var m in _manufacturerService.GetAllManufacturers(true))
		//		model.AvailableManufacturers.Add(new SelectListItem() { Text = m.Name, Value = m.Id.ToString() });
            
		//	return View(model);
		//}

		//[HttpPost, GridAction(EnableCustomBinding = true)]
		//public ActionResult AddProductToOrder(GridCommand command, OrderModel.AddOrderProductModel model)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
		//		return AccessDeniedView();

		//	var gridModel = new GridModel();
		//	var productVariants = _productService.SearchProductVariants(model.SearchCategoryId,
		//		model.SearchManufacturerId, model.SearchProductName, false,
		//		command.Page - 1, command.PageSize, true);
		//	gridModel.Data = productVariants.Select(x =>
		//	{
		//		var productVariantModel = new OrderModel.AddOrderProductModel.ProductVariantLineModel()
		//		{
		//			Id = x.Id,
		//			Name =  x.FullProductName,
		//			Sku = x.Sku,
		//		};

		//		return productVariantModel;
		//	});
		//	gridModel.Total = productVariants.TotalCount;
		//	return new JsonResult
		//	{
		//		Data = gridModel
		//	};
		//}

		//public ActionResult AddProductToOrderDetails(int orderId, int productVariantId)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
		//		return AccessDeniedView();

		//	var model = PrepareAddProductToOrderModel(orderId, productVariantId);
		//	return View(model);
		//}

		//[HttpPost]
		//public ActionResult AddProductToOrderDetails(int orderId, int productVariantId, FormCollection form)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
		//		return AccessDeniedView();

		//	var order = _orderService.GetOrderById(orderId);
		//	var productVariant = _productService.GetProductVariantById(productVariantId);
		//	//save order item

		//	//basic properties
		//	var unitPriceInclTax = decimal.Zero;
		//	decimal.TryParse(form["UnitPriceInclTax"], out unitPriceInclTax);
		//	var unitPriceExclTax = decimal.Zero;
		//	decimal.TryParse(form["UnitPriceExclTax"], out unitPriceExclTax);
		//	var quantity = 1;
		//	int.TryParse(form["Quantity"], out quantity);
		//	var priceInclTax = decimal.Zero;
		//	decimal.TryParse(form["SubTotalInclTax"], out priceInclTax);
		//	var priceExclTax = decimal.Zero;
		//	decimal.TryParse(form["SubTotalExclTax"], out priceExclTax);

		//	//attributes
		//	//warnings
		//	var warnings = new List<string>();
		//	string attributes = "";

		//	#region Product attributes
		//	string selectedAttributes = string.Empty;
		//	var productVariantAttributes = _productAttributeService.GetProductVariantAttributesByProductVariantId(productVariant.Id);
		//	foreach (var attribute in productVariantAttributes)
		//	{
		//		string controlId = string.Format("product_attribute_{0}_{1}", attribute.ProductAttributeId, attribute.Id);
		//		switch (attribute.AttributeControlType)
		//		{
		//			case AttributeControlType.DropdownList:
		//				{
		//					var ddlAttributes = form[controlId];
		//					if (!String.IsNullOrEmpty(ddlAttributes))
		//					{
		//						int selectedAttributeId = int.Parse(ddlAttributes);
		//						if (selectedAttributeId > 0)
		//							selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//								attribute, selectedAttributeId.ToString());
		//					}
		//				}
		//				break;
		//			case AttributeControlType.RadioList:
		//				{
		//					var rblAttributes = form[controlId];
		//					if (!String.IsNullOrEmpty(rblAttributes))
		//					{
		//						int selectedAttributeId = int.Parse(rblAttributes);
		//						if (selectedAttributeId > 0)
		//							selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//								attribute, selectedAttributeId.ToString());
		//					}
		//				}
		//				break;
		//			case AttributeControlType.Checkboxes:
		//				{
		//					var cblAttributes = form[controlId];
		//					if (!String.IsNullOrEmpty(cblAttributes))
		//					{
		//						foreach (var item in cblAttributes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
		//						{
		//							int selectedAttributeId = int.Parse(item);
		//							if (selectedAttributeId > 0)
		//								selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//									attribute, selectedAttributeId.ToString());
		//						}
		//					}
		//				}
		//				break;
		//			case AttributeControlType.TextBox:
		//				{
		//					var txtAttribute = form[controlId];
		//					if (!String.IsNullOrEmpty(txtAttribute))
		//					{
		//						string enteredText = txtAttribute.Trim();
		//						selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//							attribute, enteredText);
		//					}
		//				}
		//				break;
		//			case AttributeControlType.MultilineTextbox:
		//				{
		//					var txtAttribute = form[controlId];
		//					if (!String.IsNullOrEmpty(txtAttribute))
		//					{
		//						string enteredText = txtAttribute.Trim();
		//						selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//							attribute, enteredText);
		//					}
		//				}
		//				break;
		//			case AttributeControlType.Datepicker:
		//				{
		//					var day = form[controlId + "_day"];
		//					var month = form[controlId + "_month"];
		//					var year = form[controlId + "_year"];
		//					DateTime? selectedDate = null;
		//					try
		//					{
		//						selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
		//					}
		//					catch { }
		//					if (selectedDate.HasValue)
		//					{
		//						selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//							attribute, selectedDate.Value.ToString("D"));
		//					}
		//				}
		//				break;
		//			case AttributeControlType.FileUpload:
		//				{
		//					var httpPostedFile = this.Request.Files[controlId];
		//					if ((httpPostedFile != null) && (!String.IsNullOrEmpty(httpPostedFile.FileName)))
		//					{
		//						int fileMaxSize = _catalogSettings.FileUploadMaximumSizeBytes;
		//						if (httpPostedFile.ContentLength > fileMaxSize)
		//						{
		//							warnings.Add(string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), (int)(fileMaxSize / 1024)));
		//						}
		//						else
		//						{
		//							//save an uploaded file
		//							var download = new Download()
		//							{
		//								DownloadGuid = Guid.NewGuid(),
		//								UseDownloadUrl = false,
		//								DownloadUrl = "",
		//								DownloadBinary = httpPostedFile.GetDownloadBits(),
		//								ContentType = httpPostedFile.ContentType,
		//								Filename = System.IO.Path.GetFileNameWithoutExtension(httpPostedFile.FileName),
		//								Extension = System.IO.Path.GetExtension(httpPostedFile.FileName),
		//								IsNew = true
		//							};
		//							_downloadService.InsertDownload(download);
		//							//save attribute
		//							selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
		//								attribute, download.DownloadGuid.ToString());
		//						}
		//					}
		//				}
		//				break;
		//			default:
		//				break;
		//		}
		//	}
		//	attributes = selectedAttributes;

		//	#endregion

		//	#region Gift cards

		//	string recipientName = "";
		//	string recipientEmail = "";
		//	string senderName = "";
		//	string senderEmail = "";
		//	string giftCardMessage = "";
		//	if (productVariant.IsGiftCard)
		//	{
		//		foreach (string formKey in form.AllKeys)
		//		{
		//			if (formKey.Equals("giftcard.RecipientName", StringComparison.InvariantCultureIgnoreCase))
		//			{
		//				recipientName = form[formKey];
		//				continue;
		//			}
		//			if (formKey.Equals("giftcard.RecipientEmail", StringComparison.InvariantCultureIgnoreCase))
		//			{
		//				recipientEmail = form[formKey];
		//				continue;
		//			}
		//			if (formKey.Equals("giftcard.SenderName", StringComparison.InvariantCultureIgnoreCase))
		//			{
		//				senderName = form[formKey];
		//				continue;
		//			}
		//			if (formKey.Equals("giftcard.SenderEmail", StringComparison.InvariantCultureIgnoreCase))
		//			{
		//				senderEmail = form[formKey];
		//				continue;
		//			}
		//			if (formKey.Equals("giftcard.Message", StringComparison.InvariantCultureIgnoreCase))
		//			{
		//				giftCardMessage = form[formKey];
		//				continue;
		//			}
		//		}

		//		attributes = _productAttributeParser.AddGiftCardAttribute(attributes,
		//			recipientName, recipientEmail, senderName, senderEmail, giftCardMessage);
		//	}

		//	#endregion

		//	//warnings
		//	warnings.AddRange(_shoppingCartService.GetShoppingCartItemAttributeWarnings(ShoppingCartType.ShoppingCart, productVariant, attributes));
		//	warnings.AddRange(_shoppingCartService.GetShoppingCartItemGiftCardWarnings(ShoppingCartType.ShoppingCart, productVariant, attributes));
		//	if (warnings.Count == 0)
		//	{
		//		//no errors

		//		//attributes
		//		string attributeDescription = _productAttributeFormatter.FormatAttributes(productVariant, attributes, order.Customer);

		//		//save item
		//		var opv = new OrderProductVariant()
		//		{
		//			OrderProductVariantGuid = Guid.NewGuid(),
		//			Order = order,
		//			ProductVariantId = productVariant.Id,
		//			UnitPriceInclTax = unitPriceInclTax,
		//			UnitPriceExclTax = unitPriceExclTax,
		//			PriceInclTax = priceInclTax,
		//			PriceExclTax = priceExclTax,
		//			AttributeDescription = attributeDescription,
		//			AttributesXml = attributes,
		//			Quantity = quantity,
		//			DiscountAmountInclTax = decimal.Zero,
		//			DiscountAmountExclTax = decimal.Zero,
		//			DownloadCount = 0,
		//			IsDownloadActivated = false,
		//			LicenseDownloadId = 0
		//		};
		//		order.OrderProductVariants.Add(opv);
		//		_orderService.UpdateOrder(order);

		//		//gift cards
		//		if (productVariant.IsGiftCard)
		//		{
		//			for (int i = 0; i < opv.Quantity; i++)
		//			{
		//				var gc = new GiftCard()
		//				{
		//					GiftCardType = productVariant.GiftCardType,
		//					PurchasedWithOrderProductVariant = opv,
		//					Amount = unitPriceExclTax,
		//					IsGiftCardActivated = false,
		//					GiftCardCouponCode = _giftCardService.GenerateGiftCardCode(),
		//					RecipientName = recipientName,
		//					RecipientEmail = recipientEmail,
		//					SenderName = senderName,
		//					SenderEmail = senderEmail,
		//					Message = giftCardMessage,
		//					IsRecipientNotified = false,
		//					CreatedOnUtc = DateTime.UtcNow
		//				};
		//				_giftCardService.InsertGiftCard(gc);
		//			}
		//		}

		//		//redirect to order details page
		//		return RedirectToAction("Edit", "Order", new { id = order.Id });
		//	}
		//	else
		//	{
		//		//errors
		//		var model = PrepareAddProductToOrderModel(order.Id, productVariant.Id);
		//		model.Warnings.AddRange(warnings);
		//		return View(model);
		//	}
		//}

        #endregion

        #endregion

        #region Addresses

        public ActionResult AddressEdit(int addressId, int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var address = _addressService.GetAddressById(addressId);
            if (address == null)
                throw new ArgumentException("No address found with the specified id", "addressId");

            var model = new OrderAddressModel();
            model.OrderId = orderId;
            model.Address = address.ToModel();
            //countries
            //model.Address.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == address.CountryId) });
            //states
            var states = _stateProvinceService.GetStateProvincesByCountryId(address.Country == null ? 1 : address.Country.Id, true).ToList();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

            return View(model);
        }

        [HttpPost]
        public ActionResult AddressEdit(OrderAddressModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var address = _addressService.GetAddressById(model.Address.Id);
            if (address == null)
                throw new ArgumentException("No address found with the specified id");

            if (ModelState.IsValid)
            {
                address = model.Address.ToEntity(address);
                _addressService.UpdateAddress(address);

                return RedirectToAction("AddressEdit", new { addressId = model.Address.Id, orderId = model.OrderId });
            }

            //If we got this far, something failed, redisplay form
            model.OrderId = order.Id;
            model.Address = address.ToModel();
            //countries
            //model.Address.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == address.CountryId) });
            //states
            var states = _stateProvinceService.GetStateProvincesByCountryId(address.Country == null ? 1 : address.Country.Id, true).ToList();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });

            return View(model);
        }

        #endregion
        
        #region Shipments


        public ActionResult ShipmentList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new ShipmentListModel();
            model.DisplayPdfPackagingSlip = _pdfSettings.Enabled;
            return View(model);
		}

		[GridAction(EnableCustomBinding = true)]
        public ActionResult ShipmentListSelect(GridCommand command, ShipmentListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null 
                            :(DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            //load shipments
            var shipments = _shipmentService.GetAllShipments(startDateValue, endDateValue, 
                command.Page - 1, command.PageSize);
            var gridModel = new GridModel<ShipmentModel>
            {
                Data = shipments.Select(shipment => PrepareShipmentModel(shipment, false)),
                Total = shipments.TotalCount
            };
			return new JsonResult
			{
				Data = gridModel
			};
		}
        
        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult ShipmentsSelect(int orderId, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //shipments
            var shipmentModels = new List<ShipmentModel>();
            var shipments = order.Shipments.OrderBy(s => s.CreatedOnUtc).ToList();
            foreach (var shipment in shipments)
                shipmentModels.Add(PrepareShipmentModel(shipment, false));

            var model = new GridModel<ShipmentModel>
            {
                Data = shipmentModels,
                Total = shipmentModels.Count
            };

            return new JsonResult
            {
                Data = model
            };
        }

        [HttpPost]
	    public ActionResult StartShipment(int orderId, string jsonItemsQuantities)
        {
            TempData["ProductsToShip"] = jsonItemsQuantities;
	        return Json("Success");
	    }

        public ActionResult AddShipment(int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var model = JsonConvert.DeserializeObject<List<ShipmentModel>>(TempData["ProductsToShip"] as string);
            model.ForEach(s => s.OrderId = orderId);

            //measures
            var baseWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            var baseWeightIn = baseWeight != null ? baseWeight.Name : "";
            var baseDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var baseDimensionIn = baseDimension != null ? baseDimension.Name : "";

            var ups = _shippingService.LoadShippingRateComputationMethodBySystemName("Shipping.UPS");

            foreach (var package in model)
            {
                foreach (var product in package.Products)
                {
                    var opv = order.OrderProductVariants.First(p => p.Id == product.Id);
                    //we can ship only shippable products
                    if (!opv.ProductVariant.IsShipEnabled)
                        continue;

                    //quantities
                    var qtyInThisShipment = product.QuantityInThisShipment;

                    var maxQtyToAdd = opv.GetTotalNumberOfItemsCanBeAddedToShipment();
                    product.QuantityOrdered= opv.Quantity;
                    product.QuantityInAllShipments = opv.GetTotalNumberOfItemsInAllShipment();

                    //ensure that this product variant can be added to a shipment
                    if (maxQtyToAdd <= 0)
                        continue;

                    product.OrderProductVariantId = opv.Id;
                    product.ProductVariantId = opv.ProductVariantId;
                    product.Sku = opv.ProductVariant.Sku;
                    product.AttributeInfo = opv.AttributeDescription;
                    product.ItemWeight =
                        opv.ItemWeight.HasValue ? string.Format("{0:F2} [{1}]", opv.ItemWeight, baseWeightIn) : "";
                    product.ItemDimensions =
                        string.Format("{0:F2} x {1:F2} x {2:F2} [{3}]", opv.ProductVariant.Length,
                            opv.ProductVariant.Width, opv.ProductVariant.Height, baseDimensionIn);
                    product.Price = (qtyInThisShipment*opv.PriceExclTax).ToString("C");
                    product.PriceDecimal = (qtyInThisShipment*opv.PriceExclTax);


                    //product name
                    if (!String.IsNullOrEmpty(opv.ProductVariant.Name))
                        product.FullProductName = string.Format("{0} ({1})", opv.ProductVariant.Product.Name,
                            opv.ProductVariant.Name);
                    else
                        product.FullProductName = opv.ProductVariant.Product.Name;

                    package.Height = package.Height > opv.ProductVariant.Height
                        ? package.Height
                        : opv.ProductVariant.Height;
                    package.Length = package.Length > opv.ProductVariant.Length
                        ? package.Length
                        : opv.ProductVariant.Length;
                    package.Width = package.Width > opv.ProductVariant.Width
                        ? package.Width
                        : opv.ProductVariant.Width;

                    package.Weight += opv.ProductVariant.Weight;
                }


                package.UpsShipment = new ShipmentModel.UpsShipmentModel
                {
                    SendEmailNotification = true,
                    ConfirmationDelivery = false,
                    RequireSignature = false,
                    SaturdayDelivery = false,
                    UpsCarbonNeutral = false,
                    OversizePackage = false,
                    ResidentialAddress = false,
                    DeclaredValue = Math.Round(package.Products.Sum(p => p.PriceDecimal)),
                    AvailablePackagingTypes = ups.GetPackagingTypes(),
                    AvailableServices =
                        ups.GetAllShippingOptionsAdmin()
                            .ShippingOptions.Select(s => new KeyValuePair<string, string>(s.Name, s.Description))
                            .ToList()
                };

                package.ShippingAddress = order.ShippingAddress;
                package.AvailableStates =
                    _stateProvinceService.GetStateProvincesByCountryId(order.ShippingAddress.Country.Id)
                        .Select(s => new SelectListItem
                        {
                            Text = s.GetLocalized(x => x.Name),
                            Value = s.Id.ToString()
                        }).ToList();
            }

            TempData.Keep("ProductsToShip");

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormNameAttribute("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult AddShipment(int orderId, FormCollection form, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var packageIds =
                form["ShipmentPackageIds"].Replace("[", "")
                    .Replace("]", "")
                    .Split(',')
                    .Select(int.Parse)
                    .ToList();
            var b = packageIds;
            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var shipments = new Dictionary<int, Shipment>();
            packageIds.ForEach(
                pId =>
                    shipments.Add(pId,
                        new Shipment
                        {
                            OrderId = order.Id,
                            TrackingNumber = form[string.Format("Shipment.TrackingNumber.{0}", pId)],
                            CreatedOnUtc = DateTime.UtcNow,
                            TotalWeight = null,
                            ShippedDateUtc = null,
                            DeliveryDateUtc = null
                        }));

            foreach (var opv in order.OrderProductVariants)
            {
                //is shippable
                if (!opv.ProductVariant.IsShipEnabled)
                    continue;
                
                //ensure that this product variant can be shipped (have at least one item to ship)
                var maxQtyToAdd = opv.GetTotalNumberOfItemsCanBeAddedToShipment();
                if (maxQtyToAdd <= 0)
                    continue;

                int qtyToAdd = 0; //parse quantity
                int.TryParse(form[string.Format("qtyToAdd{0}", opv.Id)], out qtyToAdd);

                //validate quantity
                if (qtyToAdd <= 0)
                    continue;
                if (qtyToAdd > maxQtyToAdd)
                    qtyToAdd = maxQtyToAdd;

                var pId = int.Parse(form["shipmentPackage" + opv.Id]);
                var shipment = shipments[pId];

                var opvTotalWeight = opv.ItemWeight.HasValue ? opv.ItemWeight * qtyToAdd : null;
                if (opvTotalWeight.HasValue)
                {
                    if (!shipment.TotalWeight.HasValue)
                        shipment.TotalWeight = 0;
                    shipment.TotalWeight += opvTotalWeight.Value;
                }

                //create a shipment order product variant
                var sopv = new ShipmentOrderProductVariant()
                {
                    OrderProductVariantId = opv.Id,
                    Quantity = qtyToAdd,
                };
                shipment.ShipmentOrderProductVariants.Add(sopv);
            }

            // Update Order Info
            order.ShippingAddress.Address1 = form["defaultPackage.ShippingAddress.Address1"];
            order.ShippingAddress.Address2 = form["defaultPackage.ShippingAddress.Address2"];
            order.ShippingAddress.City = form["defaultPackage.ShippingAddress.City"];
            order.ShippingAddress.StateProvinceId = Convert.ToInt32(form["defaultPackage.ShippingAddress.StateProvinceId"]);
            order.ShippingAddress.StateProvince =
                _stateProvinceService.GetStateProvinceById(order.ShippingAddress.StateProvinceId.Value);
            order.ShippingAddress.ZipPostalCode = form["defaultPackage.ShippingAddress.ZipPostalCode"];


            foreach (var s in shipments)
            {
                var shipment = s.Value;
                var pId = s.Key;

                decimal overrideWeight;
                if (form[string.Format("Shipment.Weight.{0}", pId)] != null && Decimal.TryParse(form[string.Format("Shipment.Weight.{0}", pId)], out overrideWeight))
                    shipment.TotalWeight = overrideWeight;


                //if we have at least one item in the shipment, then save it
                if (shipment.ShipmentOrderProductVariants.Count > 0)
                {
                    decimal length;
                    if (form[string.Format("Shipment.Length.{0}", pId)] != null && Decimal.TryParse(form[string.Format("Shipment.Length.{0}", pId)], out length))
                        shipment.Length = length;

                    decimal width;
                    if (form[string.Format("Shipment.Width.{0}", pId)] != null && Decimal.TryParse(form[string.Format("Shipment.Width.{0}", pId)], out width))
                        shipment.Width = width;

                    decimal height;
                    if (form[string.Format("Shipment.Height.{0}", pId)] != null && Decimal.TryParse(form[string.Format("Shipment.Height.{0}", pId)], out height))
                        shipment.Height = height;

                    shipment.UpsShipment = new UpsShipment
                    {
                        PackagingType = form["defaultPackage.UpsShipment.PackagingType"],
                        Service = form["defaultPackage.UpsShipment.Service"],
                        SendEmailNotification =
                            (bool)
                                form.GetValue("defaultPackage.UpsShipment.SendEmailNotification")
                                    .ConvertTo(typeof (bool)),
                        ConfirmationDelivery =
                            (bool)
                                form.GetValue("defaultPackage.UpsShipment.ConfirmationDelivery")
                                    .ConvertTo(typeof (bool)),
                        RequireSignature =
                            (bool) form.GetValue("defaultPackage.UpsShipment.RequireSignature").ConvertTo(typeof (bool)),
                        SaturdayDelivery =
                            (bool) form.GetValue("defaultPackage.UpsShipment.SaturdayDelivery").ConvertTo(typeof (bool)),
                        UpsCarbonNeutral =
                            (bool)form.GetValue("defaultPackage.UpsShipment.UpsCarbonNeutral").ConvertTo(typeof(bool)),
                        OversizePackage =
                            (bool)form.GetValue("defaultPackage.UpsShipment.OversizePackage").ConvertTo(typeof(bool)),
                        ResidentialAddress = 
                            (bool)form.GetValue("defaultPackage.UpsShipment.ResidentialAddress").ConvertTo(typeof(bool)),
                        QuotedPrice = Convert.ToDecimal(form["defaultPackage.UpsShipment.QuotedPrice"])
                    };
                    shipment.UpsShipment.DeclaredValue =
                        Convert.ToDecimal(form[string.Format("Shipment.DeclaredValue.{0}", pId)]);

                    _shipmentService.InsertShipment(shipment);
                    _orderService.UpdateOrder(order);

                    if ((bool)form.GetValue("MarkAsShipped").ConvertTo(typeof(bool)))
                    {
                        _orderProcessingService.Ship(shipment, true);
                    }

                    SuccessNotification(_localizationService.GetResource("Admin.Orders.Shipments.Added"));
                }
                
            }

            if (!shipments.Values.Any(s => s.ShipmentOrderProductVariants.Count > 0))
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.Shipments.NoProductsSelected"));
                return RedirectToAction("AddShipment", new {orderId = orderId});
            }

            return continueEditing
                ? RedirectToAction("ShipmentDetails", new {id = shipments.Values.First().Id})
                : RedirectToAction("Edit", new {id = orderId});
        }

        public ActionResult ShipmentDetails(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            var model = PrepareShipmentModel(shipment, true);
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteShipment(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            var orderId = shipment.OrderId;

            SuccessNotification(_localizationService.GetResource("Admin.Orders.Shipments.Deleted"));
            _shipmentService.DeleteShipment(shipment);
            return RedirectToAction("Edit", new { id = orderId });
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("settrackingnumber")]
        public ActionResult SetTrackingNumber(ShipmentModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(model.Id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            shipment.TrackingNumber = model.TrackingNumber;
            _shipmentService.UpdateShipment(shipment);
            _orderProcessingService.Ship(shipment, true);

            return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setasshipped")]
        public ActionResult SetAsShipped(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            try
            {
                _orderProcessingService.Ship(shipment, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
            catch (Exception exc)
            {
                //error
                ErrorNotification(exc, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setasdelivered")]
        public ActionResult SetAsDelivered(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            try
            {
                _orderProcessingService.Deliver(shipment, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
            catch (Exception exc)
            {
                //error
                ErrorNotification(exc, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
        }
        
        public ActionResult PdfPackagingSlip(int shipmentId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                //no shipment found with the specified id
                return RedirectToAction("List");

            var order = shipment.Order;

            var shipments = new List<Shipment>();
            shipments.Add(shipment);
            string fileName = string.Format("packagingslip_{0}_{1}_{2}.pdf", order.OrderGuid, shipment.Id, DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);
            _pdfService.PrintPackagingSlipsToPdf(shipments, _workContext.WorkingLanguage, filePath);
            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "application/pdf", fileName);
        }

        public ActionResult PdfPackagingSlipAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = _shipmentService.GetAllShipments(null, null, 0, int.MaxValue);
            string fileName = string.Format("packagingslips_{0}.pdf", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);
            _pdfService.PrintPackagingSlipsToPdf(shipments, _workContext.WorkingLanguage, filePath);
            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "application/pdf", fileName);
        }

        public ActionResult PdfPackagingSlipSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                shipments.AddRange(_shipmentService.GetShipmentsByIds(ids));
            }

            string fileName = string.Format("packagingslips_{0}.pdf", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);
            _pdfService.PrintPackagingSlipsToPdf(shipments, _workContext.WorkingLanguage, filePath);
            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "application/pdf", fileName);
        }

        [HttpPost]
	    public ActionResult ValidateAddress(Address address)
	    {
	        var ups = _shippingService.LoadShippingRateComputationMethodBySystemName("Shipping.UPS");

	        address.Country = _countryService.GetCountryById(1);
	        if (address.StateProvinceId != null)
	            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);
	        var response = ups.ValidateAddress(address);
            var jsonResponse = JsonConvert.SerializeObject(response,
                new JsonSerializerSettings {PreserveReferencesHandling = PreserveReferencesHandling.Objects});
            return Content(jsonResponse, "application/json");
	    }

	    [HttpPost]
	    public ActionResult GetShippingRate(int orderId, Object request)
	    {
	        var jsonString = request as string[];
	        if (jsonString == null) return Json("Failed");

	        var upsRequest = JsonConvert.DeserializeObject<UpsShippingOptionRequest>(jsonString[0]);

	        var order = _orderService.GetOrderById(orderId);
	        var store = _uow.Locations.GetByFranchiseStoreNumber(_uow.Franchises.MusicGoRound, order.StoreNumber);

	        var criteria = _uow.Websites.NewSearchCriteria();
	        criteria.FranchiseId = _uow.Franchises.MusicGoRound.Id;
	        criteria.StoreNumber = store.StoreNumber;
	        var website = _uow.Websites.Search(criteria).Items.FirstOrDefault();
	        if (website == null)
	            return Json(new GetShippingOptionResponse {Errors = new List<string>() {"Could not find website associated with this store"}});

	        var mgrOptions = _uow.MgrOptions.GetByWebsite(website);
	        if (mgrOptions == null || string.IsNullOrWhiteSpace(mgrOptions.UpsAccountNumber))
	            return
	                Json(new GetShippingOptionResponse {Errors = new List<string>() {"Could not find UPS Account Number for this store"}});

	        upsRequest.ShipperNumber = mgrOptions.UpsAccountNumber;
	        upsRequest.ShipperUsername = mgrOptions.UpsUsername;
	        upsRequest.ShipperDeveloperKey = mgrOptions.UpsDeveloperKey;
	        upsRequest.OrderId = orderId;

	        if (upsRequest.ShippingAddress.StateProvinceId != null)
	            upsRequest.ShippingAddress.StateProvince = _stateProvinceService.GetStateProvinceById(upsRequest.ShippingAddress.StateProvinceId.Value);
	        upsRequest.ShippingAddress.Country = order.ShippingAddress.Country;
	        upsRequest.ShippingAddress.Email = order.ShippingAddress.Email;

	        upsRequest.Customer = order.Customer;
	        upsRequest.ShipperAddress = new Address
	        {
                Company = string.Format("{0} {1}", "Music Go Round", store.Name),
	            Address1 = store.Address.Line1,
	            Address2 = store.Address.Line2,
	            City = store.Address.City,
	            StateProvince = _stateProvinceService.GetStateProvinceByAbbreviation(store.Address.Region),
	            Country = _countryService.GetCountryByTwoLetterIsoCode(store.Address.Country),
                ZipPostalCode = store.Address.PostalCode,
                Email = store.EmailAddress
	        };

	        var ups = _shippingService.LoadShippingRateComputationMethodBySystemName("Shipping.UPS");

	        var response = ups.GetShippingRate(upsRequest);
	        var errors = response.Errors;
            response.Errors = errors.Select(e =>
            {
                var xml = new XmlDocument();
                xml.LoadXml(e);
                return JsonConvert.SerializeXmlNode(xml);
            }).ToList();
	        return Json(response);
	    }

	    [HttpPost]
	    public ActionResult AcceptAndShip(string ShipmentDigest)
	    {
	        var ups = _shippingService.LoadShippingRateComputationMethodBySystemName("Shipping.UPS");
	        var response = ups.AcceptShipmentQuote(ShipmentDigest);
	        var jsonResponse = JsonConvert.SerializeObject(response, new KeyValuePairConverter());
	        return Content(jsonResponse, "application/json");
	    }

        #endregion

        #region Order notes
        
        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult OrderNotesSelect(int orderId, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //order notes
            var orderNoteModels = new List<OrderModel.OrderNote>();
            foreach (var orderNote in order.OrderNotes
                .OrderByDescending(on => on.CreatedOnUtc))
            {
                orderNoteModels.Add(new OrderModel.OrderNote()
                {
                    Id = orderNote.Id,
                    OrderId = orderNote.OrderId,
                    DisplayToCustomer = orderNote.DisplayToCustomer,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }

            var model = new GridModel<OrderModel.OrderNote>
            {
                Data = orderNoteModels,
                Total = orderNoteModels.Count
            };

            return new JsonResult
            {
                Data = model
            };
        }
        
        [ValidateInput(false)]
        public ActionResult OrderNoteAdd(int orderId, bool displayToCustomer, string message)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            var orderNote = new OrderNote()
            {
                DisplayToCustomer = displayToCustomer,
                Note = message,
                CreatedOnUtc = DateTime.UtcNow,
            };
            order.OrderNotes.Add(orderNote);
            _orderService.UpdateOrder(order);

            //new order notification
            if (displayToCustomer)
            {
                //email
                _workflowMessageService.SendNewOrderNoteAddedCustomerNotification(
                    orderNote, _workContext.WorkingLanguage.Id);

            }

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }
        
        [GridAction(EnableCustomBinding = true)]
        public ActionResult OrderNoteDelete(int orderId, int orderNoteId, GridCommand command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            var orderNote = order.OrderNotes.Where(on => on.Id == orderNoteId).FirstOrDefault();
            if (orderNote == null)
                throw new ArgumentException("No order note found with the specified id");
            _orderService.DeleteOrderNote(orderNote);

            return OrderNotesSelect(orderId, command);
        }


        #endregion

        #region Reports

        [NonAction]
        protected IList<BestsellersReportLineModel> GetBestsellersBriefReportModel(int recordsToReturn, int orderBy)
        {
            var report = _orderReportService.BestSellersReport(null, null,
                null, null, null, recordsToReturn, orderBy, true);

            var model = report.Select(x =>
            {
                var m = new BestsellersReportLineModel()
                {
                    ProductVariantId = x.ProductVariantId,
                    TotalAmount = _priceFormatter.FormatPrice(x.TotalAmount, true, false),
                    TotalQuantity = x.TotalQuantity,
                };
                var productVariant = _productService.GetProductVariantById(x.ProductVariantId);
                if (productVariant != null)
                    m.ProductVariantFullName = productVariant.Product.Name + " " + productVariant.Name;
                return m;
            }).ToList();

            return model;
        }
        public ActionResult BestsellersBriefReportByQuantity()
        {
            var model = GetBestsellersBriefReportModel(5, 1);
            return PartialView(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult BestsellersBriefReportByQuantityList(GridCommand command)
        {
            var model = GetBestsellersBriefReportModel(5, 1);
            var gridModel = new GridModel<BestsellersReportLineModel>
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
        public ActionResult BestsellersBriefReportByAmount()
        {
            var model = GetBestsellersBriefReportModel(5, 2);
            return PartialView(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult BestsellersBriefReportByAmountList(GridCommand command)
        {
            var model = GetBestsellersBriefReportModel(5, 2);
            var gridModel = new GridModel<BestsellersReportLineModel>
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }




        public ActionResult BestsellersReport()
        {
            var model = new BestsellersReportModel();
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult BestsellersReportList(GridCommand command, BestsellersReportModel model)
        {
            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;


            //return first 100 records
            var items = _orderReportService.BestSellersReport(startDateValue, endDateValue,
                orderStatus, paymentStatus, null, 100, 2, true);
            var gridModel = new GridModel<BestsellersReportLineModel>
            {
                Data = items.Select(x =>
                {
                    var m = new BestsellersReportLineModel()
                    {
                        ProductVariantId = x.ProductVariantId,
                        TotalAmount = _priceFormatter.FormatPrice(x.TotalAmount, true, false),
                        TotalQuantity = x.TotalQuantity,
                    };
                    var productVariant = _productService.GetProductVariantById(x.ProductVariantId);
                    if (productVariant != null)
                        m.ProductVariantFullName = productVariant.Product.Name + " " + productVariant.Name;
                    return m;
                }),
                Total = items.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }



        public ActionResult NeverSoldReport()
        {
            var model = new NeverSoldReportModel();
            return View(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult NeverSoldReportList(GridCommand command, NeverSoldReportModel model)
        {
            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);
            
            var items = _orderReportService.ProductsNeverSold(startDateValue, endDateValue,
                command.Page - 1, command.PageSize, true);
            var gridModel = new GridModel<NeverSoldReportLineModel>
            {
                Data = items.Select(x =>
                    new NeverSoldReportLineModel()
                    {
                        ProductVariantId = x.Id,
                        ProductVariantFullName = x.Product.Name + " " + x.Name,
                    }),
                Total = items.TotalCount
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }


        [NonAction]
        protected virtual IList<OrderAverageReportLineSummaryModel> GetOrderAverageReportModel()
        {
            var report = new List<OrderAverageReportLineSummary>();
            report.Add(_orderReportService.OrderAverageReport(OrderStatus.Pending));
            report.Add(_orderReportService.OrderAverageReport(OrderStatus.Processing));
            report.Add(_orderReportService.OrderAverageReport(OrderStatus.Complete));
            report.Add(_orderReportService.OrderAverageReport(OrderStatus.Cancelled));
            var model = report.Select(x =>
            {
                return new OrderAverageReportLineSummaryModel()
                {
                    OrderStatus = x.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                    SumTodayOrders = _priceFormatter.FormatPrice(x.SumTodayOrders, true, false),
                    SumThisWeekOrders = _priceFormatter.FormatPrice(x.SumThisWeekOrders, true, false),
                    SumThisMonthOrders = _priceFormatter.FormatPrice(x.SumThisMonthOrders, true, false),
                    SumThisYearOrders = _priceFormatter.FormatPrice(x.SumThisYearOrders, true, false),
                    SumAllTimeOrders = _priceFormatter.FormatPrice(x.SumAllTimeOrders, true, false),
                };
            }).ToList();

            return model;
        }
        [ChildActionOnly]
        public ActionResult OrderAverageReport()
        {
            var model = GetOrderAverageReportModel();
            return PartialView(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult OrderAverageReportList(GridCommand command)
        {
            var model = GetOrderAverageReportModel();
            var gridModel = new GridModel<OrderAverageReportLineSummaryModel>
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [NonAction]
        protected virtual IList<OrderIncompleteReportLineModel> GetOrderIncompleteReportModel()
        {
            var model = new List<OrderIncompleteReportLineModel>();
            //not paid
            var psPending = _orderReportService.GetOrderAverageReportLine(null, PaymentStatus.Pending, null, null, null, null, true);
            model.Add(new OrderIncompleteReportLineModel()
            {
                Item = _localizationService.GetResource("Admin.SalesReport.Incomplete.TotalUnpaidOrders"),
                Count = psPending.CountOrders,
                Total = _priceFormatter.FormatPrice(psPending.SumOrders, true, false)
            });
            //not shipped
            var ssPending = _orderReportService.GetOrderAverageReportLine(null, null, ShippingStatus.NotYetShipped, null, null, null, true);
            model.Add(new OrderIncompleteReportLineModel()
            {
                Item = _localizationService.GetResource("Admin.SalesReport.Incomplete.TotalNotShippedOrders"),
                Count = ssPending.CountOrders,
                Total = _priceFormatter.FormatPrice(ssPending.SumOrders, true, false)
            });
            //pending
            var osPending = _orderReportService.GetOrderAverageReportLine(OrderStatus.Pending, null, null, null, null, null, true);
            model.Add(new OrderIncompleteReportLineModel()
            {
                Item = _localizationService.GetResource("Admin.SalesReport.Incomplete.TotalIncompleteOrders"),
                Count = osPending.CountOrders,
                Total = _priceFormatter.FormatPrice(osPending.SumOrders, true, false)
            });
            return model;
        }
        [ChildActionOnly]
        public ActionResult OrderIncompleteReport()
        {
            var model = GetOrderIncompleteReportModel();
            return PartialView(model);
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult OrderIncompleteReportList(GridCommand command)
        {
            var model = GetOrderIncompleteReportModel();
            var gridModel = new GridModel<OrderIncompleteReportLineModel>
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }
        
        #endregion
    }
}
