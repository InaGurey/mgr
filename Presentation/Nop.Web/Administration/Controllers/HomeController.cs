﻿using System;
using System.Net;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Services.Configuration;
using Nop.Web.Framework.Controllers;

namespace Nop.Admin.Controllers
{
    [AdminAuthorize]
    public partial class HomeController : BaseNopController
    {
        #region Fields
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly CommonSettings _commonSettings;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public HomeController(StoreInformationSettings storeInformationSettings,
            CommonSettings commonSettings, ISettingService settingService)
        {
            this._storeInformationSettings = storeInformationSettings;
            this._commonSettings = commonSettings;
            this._settingService = settingService;
        }

        #endregion

        #region Methods

        public ActionResult Dashboard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NopCommerceNewsHideAdv()
        {
            _commonSettings.HideAdvertisementsOnAdminArea = !_commonSettings.HideAdvertisementsOnAdminArea;
            _settingService.SaveSetting(_commonSettings);
            return Content("Setting changed");
        }

        #endregion
    }
}
