﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Models.CustomReport;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.CustomReport;
using Nop.Services.Customers;
using Nop.Web.Framework.Controllers;
using NPoco.Expressions;
using OfficeOpenXml;
using SmartFormat;
using Treefort.Common;


namespace Nop.Admin.Controllers
{
    [AdminAuthorize]
    public class CustomReportController : BaseNopController
    {
        private readonly IWorkContext _workContext;
        private readonly Treefort.Portal.Services.ILocationService _locationService;
        private readonly ICustomReportService _reportService;
        
        public CustomReportController(
            IWorkContext workContext,
            Treefort.Portal.Services.ILocationService locationService,
            ICustomReportService reportService)
        {
            this._workContext = workContext;
            this._locationService = locationService;
            this._reportService = reportService;
        }


        [HttpGet]
        public ActionResult Index()
        {
	        var model = new CustomReportModel {Form = new CustomReportFormBase()};
			FillDropDowns(model);
            return View(model);
        }


        public ActionResult TotalSales(TotalSalesForm form)
        {
            var model = new TotalSalesModel { Form = form };
            model.ReportData = _reportService.TotalSales(form.StartDate, form.EndDate, form.StoreNumber, form.IncludeTaxAndShipping, form.Sort);
	        return PartialView(model);
        }


		public ActionResult PaymentMethods(PaymentMethodsForm form)
		{
			var model = new PaymentMethodsModel {Form = form};
            model.ReportData = _reportService.PaymentMethods(form.StartDate, form.EndDate, form.StoreNumber, form.IncludeTaxAndShipping, form.Sort, form.Display);
			return PartialView(model);
		}


        public ActionResult CategorySales(CategorySalesForm form)
        {
            var model = new CategorySalesModel { Form = form };
            model.ReportData = _reportService.CategorySales(form.StartDate, form.EndDate, form.StoreNumber, form.ShowSubcategories, form.Sort);
			return PartialView(model);
        }


        public ActionResult AccessorySales(AccessorySalesForm form)
        {
            var model = new AccessorySalesModel { Form = form };
            model.ReportData = _reportService.AccessorySales(form.StartDate, form.EndDate, form.StoreNumber, form.Sort, form.Display);
			return PartialView(model);
        }


        public ActionResult SavedSearches(SavedSearchesForm form)
        {
            var model = new SavedSearchesModel {Form = form};
            model.ReportData = _reportService.SavedSearches(form.StartDate, form.EndDate, form.StoreNumber);
            return PartialView(model);
        }

        public ActionResult SavedSearchesSpreadsheet(SavedSearchesForm form)
        {
            var data = _reportService.SavedSearches(form.StartDate, form.EndDate, form.StoreNumber);

            using (var xlp = new ExcelPackage()) {
                var wkbk = xlp.Workbook;
                var wsh = wkbk.Worksheets.Add("Saved Searches {StartDate:ddd, MMM d, yyyy} - {EndDate:ddd, MMM d, yyyy}".FormatSmart(form));

                wsh.Cells["A1"].Value = "Status";
                wsh.Cells["B1"].Value = "Activated";
                wsh.Cells["C1"].Value = "Expires";
                wsh.Cells["D1"].Value = "Cancelled";
                wsh.Cells["E1"].Value = "Occurence";
                wsh.Cells["F1"].Value = "Duration";
                wsh.Cells["G1"].Value = "Search Term";
                wsh.Cells["H1"].Value = "Category";
                wsh.Cells["I1"].Value = "Brand";
                wsh.Cells["J1"].Value = "Just new gear?";
                wsh.Cells["K1"].Value = "Price reduction only?";
                wsh.Cells["L1"].Value = "Price Range";
                wsh.Cells["M1"].Value = "Store Number";
                wsh.Cells["N1"].Value = "Postal Code";
                wsh.Cells["O1"].Value = "Distance";

                var dataCount = data.SavedSearches.Count();
                for (int i = 0; i < dataCount; i++) {
                    var row = i + 2;
                    var ss = data.SavedSearches.ElementAt(i);

                    wsh.Cells["A" + row].Value = ss.Status.ToString();
                    wsh.Cells["B" + row].Value = ss.DateOfActivation.ToShortDateString();
                    wsh.Cells["C" + row].Value = ss.DateOfExpiration.ToShortDateString();
                    wsh.Cells["D" + row].Value = ss.DateOfCancellation.IfHasValue(d => d.ToShortDateString());
                    wsh.Cells["E" + row].Value = ss.Occurrence;
                    wsh.Cells["F" + row].Value = ss.DurationName;
                    wsh.Cells["G" + row].Value = ss.Keywords;
                    wsh.Cells["H" + row].Value = ss.CategoryName;
                    wsh.Cells["I" + row].Value = ss.BrandName;
                    wsh.Cells["J" + row].Value = ss.NotifyOnlyWhenNewResults ? "Yes" : "No";
                    wsh.Cells["K" + row].Value = ss.ClearanceOnly ? "Yes" : "No";
                    wsh.Cells["L" + row].Value = ss.PriceRange;
                    wsh.Cells["M" + row].Value = ss.StoreLocation;
                    wsh.Cells["N" + row].Value = ss.PostalCode;
                    wsh.Cells["O" + row].Value = ss.MilesFromPostalCode.IfHasValue(v => v + " miles", (string)null);
                }

                var stream = new MemoryStream();
                xlp.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);
                return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
        }


        public ActionResult ItemSales(ItemSalesForm form)
        {
            var model = new ItemSalesModel { Form = form };
            model.ReportData = _reportService.ItemSales(form.StartDate, form.EndDate, form.StoreNumber);
            //model.ReportData.Sales = model.ReportData.Sales.Take(100);
            return PartialView(model);
        }

        public ActionResult ItemSalesSpreadsheet(ItemSalesForm form)
        {
            var data = _reportService.ItemSales(form.StartDate, form.EndDate, form.StoreNumber);

            using (var xlp = new ExcelPackage()) {
                var wkbk = xlp.Workbook;
                var wsh = wkbk.Worksheets.Add("Sold Items {StartDate:ddd, MMM d, yyyy} - {EndDate:ddd, MMM d, yyyy}".FormatSmart(form));

                wsh.Cells["A1"].Value = "Sale Date";
                wsh.Cells["B1"].Value = "Active Date";
                wsh.Cells["C1"].Value = "Days on Site";
                wsh.Cells["D1"].Value = "Order Id";
                wsh.Cells["E1"].Value = "Product Price";
                wsh.Cells["F1"].Value = "Shipping Charge";
                wsh.Cells["G1"].Value = "Category";
                wsh.Cells["H1"].Value = "Product Name";

                var dataCount = data.Sales.Count();
                for (int i = 0; i < dataCount; i++) {
                    var row = i + 2;
                    var ss = data.Sales.ElementAt(i);

                    wsh.Cells["A" + row].Value = ss.TransactionDate.ToShortDateString();
                    wsh.Cells["B" + row].Value = ss.ActiveDate.IfHasValue(d => d.ToShortDateString());
                    wsh.Cells["C" + row].Value = ss.ProductAge.IfHasValue(a => a.Days.ToString());
                    wsh.Cells["D" + row].Value = ss.OrderId.ToString();
                    wsh.Cells["E" + row].Value = ss.ProductPrice;
                    wsh.Cells["F" + row].Value = ss.OrderShippingCost;
                    wsh.Cells["G" + row].Value = ss.CategoryName;
                    wsh.Cells["H" + row].Value = ss.ProductName;
                }

                var stream = new MemoryStream();
                xlp.SaveAs(stream);
                stream.Seek(0, SeekOrigin.Begin);
                return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
        }


        private void FillDropDowns(CustomReportModel model)
        {
            var adminRole = _workContext.CurrentUser.AdminRole();
            if (adminRole == AdminRole.SuperAdmin || adminRole == AdminRole.Corporate) {
                model.Stores = _locationService.GetAllLocations().Select(l => new SelectListItem {
                    Text = l.Region + " - " + l.City,
                    Value = l.StoreNumber
                }).ToList();

                model.Stores.Insert(0, new SelectListItem { Text = "Show Totals for entire system", Value = "" });
            }

            model.ReportTypes = new List<ReportType> {
                    new ReportType {Name = "Select Report...", ReportViewUrl = ""},
                    new ReportType {Name = "Sales Report", ReportViewUrl = Url.Action("TotalSales")},
                    new ReportType {Name = "Payment Method", ReportViewUrl = Url.Action("PaymentMethods")},
                    new ReportType {Name = "Sales By Category", ReportViewUrl = Url.Action("CategorySales")},
                    new ReportType {Name = "New Accessory Sales", ReportViewUrl = Url.Action("AccessorySales")},
                    new ReportType {Name = "Saved Searches", ReportViewUrl = Url.Action("SavedSearches"), ReportSpreadsheetUrl = Url.Action("SavedSearchesSpreadsheet")},
                    new ReportType {Name = "Sold Items", ReportViewUrl = Url.Action("ItemSales"), ReportSpreadsheetUrl = Url.Action("ItemSalesSpreadsheet")},
                };
        }
    }
}
