﻿using System.Web.Mvc;

namespace Nop.Admin
{
	public class AdminAreaRegistration : AreaRegistration
	{
		public override string AreaName {
			get { return "Admin"; }
		}

		public override void RegisterArea(AreaRegistrationContext context) {
            context.MapRoute(
                "Admin_CustomReports",
                "Admin/Reports/{action}",
                new { controller = "CustomReport", action = "Index", area = "Admin", id = "" },
                new[] { "Nop.Admin.Controllers" });
            
            context.MapRoute(
				"Admin_StoreContext",
				"Admin/Store/{storeId}/{controller}/{action}/{id}",
				new { controller = "Product", action = "List", area = "Admin", id = "" },
				new { storeId = @"\d+" },
				new[] { "Nop.Admin.Controllers" });

			context.MapRoute(
				"Admin_default",
				"Admin/{controller}/{action}/{id}",
				new { controller = "Product", action = "List", area = "Admin", id = "" },
				new[] { "Nop.Admin.Controllers" });
		}
	}
}
