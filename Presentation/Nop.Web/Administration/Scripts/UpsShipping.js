﻿var $packageIds;
var $trackingNumbers;
var $quotedPrice;
var $address1;
var $address2;
var $city;
var $stateProvince;
var $zipPostalCode;


$(document).ready(function () {
    $packageIds = $('input#ShipmentPackageIds').data('package-ids');
    $trackingNumbers = $('input[name^="Shipment.TrackingNumber."]');
    $quotedPrice = $('[name="defaultPackage.UpsShipment.QuotedPrice"]');
    $address1 = $('[name="defaultPackage.ShippingAddress.Address1"]');
    $address2 = $('[name="defaultPackage.ShippingAddress.Address2"]');
    $city = $('[name="defaultPackage.ShippingAddress.City"]');
    $stateProvince = $('[name="defaultPackage.ShippingAddress.StateProvinceId"]');
    $zipPostalCode = $('[name="defaultPackage.ShippingAddress.ZipPostalCode"]');
});

function GetUpdatedAddress() {
    return {
        Address1: $address1.val(),
        Address2: $address2.val(),
        City: $city.val(),
        StateProvinceId: $stateProvince.val(),
        ZipPostalCode: $zipPostalCode.val()
    };
}

function ResetUpsForm() {
    $('#upsStatus #suggestedAddress table tbody').empty();
    $('#upsStatus #suggestedAddress').hide();
    $('#upsStatus #shipQuoteContainer #shipConfirmationContainer img').attr('src', '');
    $('#upsStatus #shipQuoteContainer #shipConfirmationContainer').hide();
    $('#upsStatus #shipQuoteContainer').hide();
    $('#upsStatus').hide();

}

function GetQuote(orderId) {
    ResetUpsForm();

    $('#upsStatus').data('orderId', orderId);

    $.ajax({
        url: '/Admin/Order/ValidateAddress',
        type: 'POST',
        data: GetUpdatedAddress(),
        success: function (response) {
            handleAddressValidation(response);
            if (response.Success) GetShippingRate(orderId, GetUpdatedAddress());
            scrollPage();
        },
        error: function (response) {
            var e = response;
        }
    });
}

function scrollPage() {
    $('html, body').animate({
        scrollTop: $('input#getQuote').offset().top
    }, 100);
}

function handleAddressValidation(response) {
    var $upsStatus = $('#upsStatus');
    $upsStatus.show();
    var $addressContainer = $upsStatus.children('#addressContainer');
    var $addressStatus = $addressContainer.find('#addressStatus');

    $addressContainer.show();
    $addressStatus.show();

    if (response.StatusCode == 0) {
        $addressStatus.html("Address validated");
        var address = response.Candidates[0];

        $address1.val(address.Address1);
        $address2.val(address.Address2);
        $city.val(address.City);
        $stateProvince.val(address.StateProvince.Id);
        $zipPostalCode.val(address.ZipPostalCode);
    }
    else if (response.StatusCode == 1) {
        $addressStatus.html("Ambiguous address. Please select one of the following addresses or make changes manually.");
        var $table = $addressContainer.children('#suggestedAddress');
        $table.show();
        $table.data('candidates', response.Candidates);

        var $tableBody = $table.find('table tbody');
        if (response.Candidates) {
            $.each(response.Candidates, function(index, value) {
                var row =
                    "<tr> " +
                        "<td style='text-align:center;'>" +
                        "<input type='button' value='Select' onclick='selectAddress(" + index + ");'/>" +
                        "</td>" +
                        "<td>" +
                        "<div>" + value.Address1 + "</div>" +
                        "<div>" + (value.Address2 ? value.Address2 : "") + "</div>" +
                        "<div>" + value.City + ", " + value.StateProvince.Abbreviation + " " + value.ZipPostalCode + "</div>" +
                        "</td>" +
                        "</tr>";

                $tableBody.append(row);
            });
        }

    } else {
        $addressStatus.html("Invalid address. Please check the address again.");
    }
}

function selectAddress(i) {
    var $table = $('#upsStatus #suggestedAddress');
    var candidates = $table.data('candidates');
    var selected = candidates[i];

    $address1.val(selected.Address1);
    $address2.val(selected.Address2);
    $city.val(selected.City);
    $stateProvince.val(selected.StateProvince.Id);
    $zipPostalCode.val(selected.ZipPostalCode);

    $('#upsStatus #addressStatus').html('Address has been changed');
}

function GetShippingRate(orderId, address) {
    if (!orderId && !address) return;

    var request = {
        Packages: [],
        ShippingAddress: address,
        ShippingMethod: $('[name="defaultPackage.UpsShipment.Service"]').val(),
        PackageType: $('[name="defaultPackage.UpsShipment.PackagingType"]').val(),
        SendEmailNotification: $('[name="defaultPackage.UpsShipment.SendEmailNotification"][type="checkbox"]').prop("checked"),
        ConfirmationDelivery: $('[name="defaultPackage.UpsShipment.ConfirmationDelivery"][type="checkbox"]').prop("checked"),
        RequireSignature: $('[name="defaultPackage.UpsShipment.RequireSignature"][type="checkbox"]').prop("checked"),
        SaturdayDelivery: $('[name="defaultPackage.UpsShipment.SaturdayDelivery"][type="checkbox"]').prop("checked"),
        UpsCarbonNeutral: $('[name="defaultPackage.UpsShipment.UpsCarbonNeutral"][type="checkbox"]').prop("checked"),
        OversizePackage: $('[name="defaultPackage.UpsShipment.OversizePackage"][type="checkbox"]').prop("checked"),
        ResidentialAddress: $('[name="defaultPackage.UpsShipment.ResidentialAddress"][type="checkbox"]').prop("checked"),
        UpsPassword: $('input#upsPassword').val()
    };

    $.each($packageIds, function(i, pId) {

        var pkg = {
            DeclaredValue: $('[name="Shipment.DeclaredValue.' + pId + '"]').val(),
            Length: $('input[name="Shipment.Length.' + pId +'"]').val(),
            Width: $('input[name="Shipment.Width.' + pId + '"]').val(),
            Height: $('input[name="Shipment.Height.' + pId + '"]').val(),
            PackageWeight: $('input[name="Shipment.Weight.' + pId + '"]').val()
        };
        request.Packages.push(pkg);
    });

    $.ajax({
        url: '/Admin/Order/GetShippingRate',
        type: 'POST',
        data: { request: JSON.stringify(request), orderId: orderId },
        success: function (response) {
            var $ship = $('#upsStatus #shipQuoteContainer');
            $ship.children().hide();
            $ship.show();
            if (response.Success) {
                $ship.find('#acceptContainer').show();
                $ship.find('#quotedRate').html("$" + response.ShippingOptions[0].Rate);
                $ship.data('shipmentDigest', response.ShippingOptions[0].Description);

                var $acceptButton = $ship.find('#btnAcceptRate');
                $acceptButton.show();
            } else {
                DisplayErrors(response.Errors);;
            }

            scrollPage();
        }
    });
}

function AcceptAndShip() {
    var $ship = $('#upsStatus #shipQuoteContainer');
    var shipmentDigest = $ship.data("shipmentDigest");

    $.ajax({
        url: '/Admin/Order/AcceptAndShip',
        type: 'POST',
        data: { ShipmentDigest: shipmentDigest },
        success: function (response) {
            if (response.Success) {

                $ship.find('[id^="shipConfirmationContainer"]:not([id="shipConfirmationContainerTemplate"])').remove();
                $.each(response.ShippingOptions, function (i, so) {
                    var imgData = "data:image/gif;base64,";
                    var additionalInfo = so.AdditionalInfo;
                    var rawData = getValueFromKey(additionalInfo, 'Label');
                    var trackingNumber = getValueFromKey(additionalInfo, 'TrackingNumber');

                    var $cc = $ship.find('#shipConfirmationContainerTemplate').clone();
                    $cc.attr('id', 'shipConfirmationContainer' + i);
                    $cc.show();
                    $cc.find('#trackingNumber').html(getValueFromKey(additionalInfo, 'TrackingNumber'));
                    $trackingNumbers.eq(i).val(trackingNumber);
                    $quotedPrice.val(so.Rate);
                    $quotedPrice.attr('value', so.Rate);
                    $quotedPrice.focus();
                    $quotedPrice.blur();

                    var $img = $cc.find('img');
                    $img.attr('src', imgData + rawData);
                    $img.data('base64', rawData);
                    $img.data('trackingNumber', trackingNumber);

                    var $dlink = $cc.find('a#downloadShippingLabel');
                    $dlink.prop('href', imgData + rawData);
                    $dlink.prop('download', trackingNumber + '.gif');

                    if (keyHasValue(additionalInfo, 'ControlLogReceipt')) {
                        var $clrLink = $cc.find('a#downloadClr');
                        var clrStr = getValueFromKey(additionalInfo, 'ControlLogReceipt');

                        var clrHtml = $('<div/>').html(clrStr).html();

                        $clrLink.click(function(e) {
                            e.preventDefault();

                            var w = window.open();
                            $(w.document.body).html(clrHtml);
                        });
                        $clrLink.show();
                    }

                    $ship.append($cc);

                });

                var $acceptButton = $ship.find('#btnAcceptRate');
                $acceptButton.hide();
            } else {
                DisplayErrors(response.Errors);
            }
            scrollPage();
        }
    });
}

function keyHasValue(col, key) {
    return col.filter(function(v, i, k) {
        return v.Key == key;
    }).length > 0;
}

function getValueFromKey(col, key) {
    return col.filter(function(v, i, k) {
        return v.Key == key;
    })[0].Value;
}

function DisplayErrors(errors) {
    var $ship = $('#upsStatus #shipQuoteContainer');
    $ship.find('#shipQuoteErrors').show();
    $ship.find('#shipQuoteErrors ul').empty();
    $.each(errors, function (i, v) {
        var errorMessage = v;
        try {
            var jsonError = JSON.parse(v);
            var ec = jsonError.Errors.ErrorDetail.PrimaryErrorCode;
            errorMessage = "Error Code " + ec.Code + ": " + ec.Description;
        } finally {
            var errorItem = "<li>" + errorMessage + "</li>";
            $ship.find('#shipQuoteErrors ul').append(errorItem);
        }
    });
}