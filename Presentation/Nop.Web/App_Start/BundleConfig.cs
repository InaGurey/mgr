﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;


namespace Nop.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                       "~/Scripts/jquery-1.7.1.min.js"
                       , "~/Scripts/libs/jquery.etalage.min.js"
                       , "~/Scripts/jquery.slides.min.js"
                       , "~/Scripts/jquery.cookie.js"
                       , "~/Scripts/public.ajaxcart.js"
                       , "~/Scripts/public.common.js"
                       , "~/Scripts/jquery-ui.min.js"
                       , "~/Scripts/jquery.validate.min.js"
                       , "~/Scripts/jquery.validate.unobtrusive.min.js"
                       , "~/Scripts/jquery.unobtrusive-ajax.min.js"
                       , "~/Scripts/libs/modernizr-custom.js"
                       , "~/Scripts/libs/jquery.nanoscroller.js"
                       , "~/Scripts/site.js"
                       , "~/Scripts/angular/mgr.js"
                       ));
            bundles.Add(new ScriptBundle("~/bundles/mobilehead").Include(
                       "~/Scripts/jquery-1.7.1.js"
                       , "~/Scripts/jquery.unobtrusive-ajax.min.js"
                       , "~/Scripts/jquery.validate.min.js"
                       , "~/Scripts/jquery.validate.unobtrusive.min.js"
                       , "~/Scripts/treefort/mobile-homepage.js"
                       ));

            bundles.Add(
                new StyleBundle("~/Styles/mobilecss")
                    .Include("~/Themes/MGR/Content/normalize-mobile.min.css")
                    .Include("~/Themes/MGR/Content/styles-mobile.css")
                    .Include("~/Themes/MGR/Content/icomoon.css", new CssRewriteUrlTransform()));
        }
    }
}