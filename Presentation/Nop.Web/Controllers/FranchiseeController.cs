﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Services.Common;
using Nop.Web.Models.Common;
using Telerik.Web.Mvc.Extensions;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee;
using Treefort.Portal.Queries.Franchisee.Location;
using Treefort.Portal.Queries.Franchisee.Website;
using Treefort.Portal.Queries.Home;
using Treefort.Portal.Services;


namespace Nop.Web.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class FranchiseeController : BaseNopController
    {
        private readonly IGenericPageViewModelQuery _genericPageViewModelQuery;
        private readonly IHeaderViewModelQuery _headerViewModelQuery;
        private readonly IWebsiteSettingsQuery _websiteSettingsQuery;
        private readonly IGenericLocationViewModelQuery _genericLocationViewModelQuery;
        private readonly IHoursViewModelQuery _hoursViewModelQuery;
        private readonly IWorkContext _context;
        private readonly IRemoteControlViewModelQuery _remoteControlViewModelQuery;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        private readonly IOutOfProcessCacheManager _cacheManager;

        public FranchiseeController(
            IGenericPageViewModelQuery genericPageViewModelQuery, 
            IHeaderViewModelQuery headerViewModelQuery, 
            IWebsiteSettingsQuery websiteSettingsQuery, 
            IWorkContext context, 
            IGenericLocationViewModelQuery genericLocationViewModelQuery, 
            IHoursViewModelQuery hoursViewModelQuery, 
            IRemoteControlViewModelQuery remoteControlViewModelQuery,
            IMobileDeviceHelper mobileDeviceHelper, IOutOfProcessCacheManager cacheManager)
        {
            _genericPageViewModelQuery = genericPageViewModelQuery;
            _headerViewModelQuery = headerViewModelQuery;
            _websiteSettingsQuery = websiteSettingsQuery;
            _context = context;
            _genericLocationViewModelQuery = genericLocationViewModelQuery;
            _hoursViewModelQuery = hoursViewModelQuery;
            _remoteControlViewModelQuery = remoteControlViewModelQuery;
            _mobileDeviceHelper = mobileDeviceHelper;
            _cacheManager = cacheManager;
        }

        public ActionResult AboutUs()
        {
            if (_context.SiteId == KnownIds.Website.Corporate) {
                var corpModel = _remoteControlViewModelQuery.Execute("about-us");
                return View("~/Views/Home/AboutUs.cshtml", corpModel);
            }

            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "about_mobile"
                : "about";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult HowItWorks()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var model = _genericPageViewModelQuery.Execute("howitworks", Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("HowItWorks", model);
        }

        public ActionResult CommunityLinks()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "community_mobile"
                : "community";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult CashForGear()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var model = _genericPageViewModelQuery.Execute("cashforgear", Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult MostWanted()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");
            
            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "wanted_mobile"
                : "mostwanted";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult Jobs()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var isMobile = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());
            var pageCode = isMobile ? "jobs_mobile" : "jobs";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            // Get job widgets from desktop
            if (isMobile)
            {
                var desktop = _genericPageViewModelQuery.Execute("jobs", Website, WebsiteSettings.PreviewMode,
                    WebsiteSettings.PreviewRevisionNumber);
                if (desktop != null)
                {
                    model.Widgets.AddRange(desktop.Widgets.Where(w => w.JobWidgets != null));
                }
            }

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult Lessons()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "lessons_mobile"
                : "lessons";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult Repairs()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "lessons_mobile"
                : "repairs";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult NewGear()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "newgear_mobile"
                : "newgear";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult Promotions()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var pageCode = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion())
                ? "promo_mobile"
                : "promotions";

            var model = _genericPageViewModelQuery.Execute(pageCode, Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;
            model.ShowAdditionalPages = true;

            return View("GenericPage", model);
        }

        public ActionResult SpecialPromotion()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var model = _genericPageViewModelQuery.Execute("special_promo", Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        public ActionResult SpecialPromotionalEvent()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
                RedirectToAction("Index", "Home");

            var model = _genericPageViewModelQuery.Execute("special_promo_ev", Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.WebsiteOptions = WebsiteSettings.WebsiteOptions;

            return View("GenericPage", model);
        }

        [ChildActionOnly]
        [OutputCache(VaryByParam = "None", VaryByCustom = "Host", Duration = 600)]
        public ActionResult PromotionsFeed()
        {
            var model = new PromotionsFeedModel();
            if (_context.SiteId == KnownIds.Website.Corporate)
            {
                return null;
            }

            model.Promotions = _genericPageViewModelQuery.GetCouponWidgets(_context.SiteId, WebsiteSettings.PreviewMode,
                WebsiteSettings.PreviewRevisionNumber);

            var location = _genericLocationViewModelQuery.Execute(WebsiteSettings.AccountId).FirstOrDefault();
            if (location != null)
            {
                model.ConstantContactUrl = location.ConstactContactUrl;
            }
                
            return View("~/Views/Franchisee/_PromotionsFeed.cshtml", model);
        }

        [ChildActionOnly]
        [OutputCache(VaryByParam = "None", VaryByCustom = "Host", Duration = 600)]
        public ActionResult CommunityEventsFeed()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
            {
                return null;
            }
            var model = _genericPageViewModelQuery.GetCommunityEventWidgets(_context.SiteId, WebsiteSettings.PreviewMode,
                WebsiteSettings.PreviewRevisionNumber);

            return View("~/Views/Franchisee/_CommunityEventsFeed.cshtml", model);
        }

        [OutputCache(VaryByParam = "None", VaryByCustom = "Host", Duration = 600)]
        public ActionResult LocationSidebar()
        {
            var locations = _genericLocationViewModelQuery.Execute(WebsiteSettings.AccountId);

            if (locations.Any())
            {
                if (locations.Count(l => !l.IsCrossBrand) == 1)
                {
                    var m = new SingleLocationSidebar();
                    m.WebsiteOptions = WebsiteSettings.WebsiteOptions;
                    m.Location = locations.FirstOrDefault();
                    m.StoreHours = _hoursViewModelQuery.Execute(m.Location.Id);
                    m.CrossBrandLocations = locations.Where(l => l.IsCrossBrand).ToList();

                    return PartialView("_SingleLocationSidebar", m);
                }

                var model = new MultipleLocationSidebar();
                model.Locations = locations;
                model.WebsiteOptions = WebsiteSettings.WebsiteOptions;
                model.IsCoop = _context.IsCoop;

                return PartialView("_MultipleLocationSidebar", model);
            }

            return null;
        }

        [OutputCache(VaryByParam = "None", VaryByCustom = "Host", Duration = 600)]
        public ActionResult LocationSidebarWithAdditionalPages()
        {
            var locations = _genericLocationViewModelQuery.Execute(WebsiteSettings.AccountId);

            if (locations.Any())
            {
                if (locations.Count(l => !l.IsCrossBrand) == 1)
                {
                    var m = new SingleLocationSidebar();
                    m.WebsiteOptions = WebsiteSettings.WebsiteOptions;
                    m.Location = locations.FirstOrDefault();
                    m.StoreHours = _hoursViewModelQuery.Execute(m.Location.Id);
                    m.AdditionalPagesList = _genericPageViewModelQuery.GetAdditionalPageNavs(WebsiteSettings.WebsiteId);

                    return PartialView("_SingleLocationAdditionalPages", m);
                }

                var model = new MultipleLocationSidebar();
                model.Locations = locations;
                model.WebsiteOptions = WebsiteSettings.WebsiteOptions;
                model.IsCoop = _context.IsCoop;

                return PartialView("_MultipleLocationSidebar", model);
            }

            return null;
        }

        public ActionResult AdditionalPagesNav(int pageId = 0)
        {
            var cacheKey = string.Format("Nop.additionalpages.nav-{0}", _context.SiteId);
            var cachedModel = _cacheManager.Get(cacheKey, 10,
                () => _genericPageViewModelQuery.GetAdditionalPageNavs(_context.SiteId));

            foreach (var apn in cachedModel)
            {
                apn.IsSelected = apn.PageId == pageId;
            }

            return View("~/Views/Franchisee/_AdditionalPagesNav.cshtml", cachedModel);
        }

        // Additional Page
        public ActionResult AdditionalPage(int pageId, string rewriteUrl = "")
        {
            var model = _genericPageViewModelQuery.Execute("additional", Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber, pageId);

            if (model == null && WebsiteSettings.PreviewMode)
                return RedirectToAction("NoRevision", "Errors");

            if (model == null)
                return RedirectToAction("NotFound", "Errors");

            model.ShowAdditionalPages = true;

            return View("GenericPage", model);
        }
    }
}
