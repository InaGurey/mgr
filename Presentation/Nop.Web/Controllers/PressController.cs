﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Nop.Core;
using Treefort.Portal;
using Treefort.Portal.Queries.Press;

namespace Nop.Web.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class PressController : BaseNopController
    {
        private readonly IWorkContext _context;
        private IIndexViewModelQuery _indexViewModelQuery { get; set; }
        private IDetailViewModelQuery _detailViewModelQuery { get; set; }

        public PressController(IWorkContext context, IIndexViewModelQuery indexViewModelQuery, IDetailViewModelQuery detailViewModelQuery)
        {
            _context = context;
            _indexViewModelQuery = indexViewModelQuery;
            _detailViewModelQuery = detailViewModelQuery;
        }

        public virtual ActionResult Index()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/press-room/");
        }

        public virtual ActionResult Detail(string urlSlug)
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/press-room/");
        }
    }
}
