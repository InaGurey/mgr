﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Services.Common;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Home;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee;
using Treefort.Portal.Queries.Franchisee.Website;
using Treefort.Portal.Queries.Home;
using Treefort.Portal.Services;
using WinmarkFranchise.Domain;
using WinmarkFranchise.Domain.Websites;


namespace Nop.Web.Controllers
{
    public class HomeController : BaseNopController
    {
	    private readonly IWorkContext _context;
        private readonly IGenericPageViewModelQuery _genericPageViewModelQuery;
        private readonly IWebsiteSettingsQuery _websiteSettingsQuery;
        private readonly IWebsiteOptionsQuery _websiteOptionsQuery;
        private readonly IRemoteControlViewModelQuery _remoteControlViewModelQuery;
        private readonly IOutOfProcessCacheManager _cacheManager;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        private readonly IHeaderViewModelQuery _headerViewModelQuery;
        private readonly IDomainNameService _domainNameService;

        public HomeController(IWorkContext context
            , IGenericPageViewModelQuery genericPageViewModelQuery
            , IWebsiteSettingsQuery websiteSettingsQuery
            , IRemoteControlViewModelQuery remoteControlViewModelQuery
            , IWebsiteOptionsQuery websiteOptionsQuery
            , IOutOfProcessCacheManager cacheManager
            , IMobileDeviceHelper mobileDeviceHelper
            , IHeaderViewModelQuery headerViewModelQuery
            , IDomainNameService domainNameService)
        {
	        _context = context;
            _websiteSettingsQuery = websiteSettingsQuery;
            _genericPageViewModelQuery = genericPageViewModelQuery;
            _remoteControlViewModelQuery = remoteControlViewModelQuery;
            _websiteOptionsQuery = websiteOptionsQuery;
            _cacheManager = cacheManager;
            _mobileDeviceHelper = mobileDeviceHelper;
            _headerViewModelQuery = headerViewModelQuery;
            _domainNameService = domainNameService;
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Index()
        {
            if (_context.SiteId != KnownIds.Website.Corporate)
            {
                var fm = _genericPageViewModelQuery.Execute("home", Website, WebsiteSettings.PreviewMode, WebsiteSettings.PreviewRevisionNumber);

                var m = new IndexViewModel()
                    {
                        FranchiseeIndex = fm,
                        IsFranchiseHomepage = true,
                        IsCoop = _context.IsCoop
                    };

                if (fm == null && WebsiteSettings.PreviewMode)
                    return RedirectToAction("NoRevision", "Errors");

                if (fm == null)
                    return RedirectToAction("NotFound", "Errors");

                m.FranchiseeIndex.WebsiteOptions = WebsiteSettings.WebsiteOptions;
                if (MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                    _mobileDeviceHelper.CustomerDontUseMobileVersion()))
                {
                    m.MobileNavigation = _headerViewModelQuery.GetFranchiseMobileNavigation(Website.Id);
                }

                return View(m);

            }

            var model = new IndexViewModel {IsFranchiseHomepage = false};
            model.WebsiteOptions = _websiteOptionsQuery.Execute(_context.SiteId);
            model.RemoteControlPage = _remoteControlViewModelQuery.Execute("home");

            return View(model);
        }

        [MobileOnly]
        public ActionResult ShopOnline()
        {
            MobileService.SetIgnoreMobile(HttpContext, 7200);
            return RedirectToAction("Index");
        }

        [MobileOnly]
        public ActionResult GiftCards()
        {
            return View();
        }

        public ActionResult Remarketing()
        {
            var useRemarketing = _context.SiteId == KnownIds.Website.Corporate;

            return View("_Remarketing", useRemarketing);
        }

        public ActionResult ReturnPolicy()
        {
            var model = _remoteControlViewModelQuery.Execute("return-policy");
            return View("GenericRemoteControl", model);
        }

        public ActionResult ContactUs()
        {
            var model = _remoteControlViewModelQuery.Execute("contact-us");
            return View("GenericRemoteControl", model);
        }

        public ActionResult Faq()
        {
            var model = _remoteControlViewModelQuery.Execute("faq");
            return View("GenericRemoteControl", model);
        }

        public ActionResult WhatToExpect()
        {
            var model = _remoteControlViewModelQuery.Execute("how_it_all_works.aspx");
            return View("GenericRemoteControl", model);
        }

        public ActionResult SpecialEvents()
        {
             return RedirectToActionPermanent("Index", "Home");
            //var model = _remoteControlViewModelQuery.Execute("special-events");
            //return View("GenericRemoteControl", model);
        }

        public ActionResult ConditionsOfUse()
        {
            var model = _remoteControlViewModelQuery.Execute("conditions-of-use");
            return View("GenericRemoteControl", model);
        }

        public ActionResult PrivacyPolicy()
        {
            var model = _remoteControlViewModelQuery.Execute("privacy-policy");
            return View("GenericRemoteControl", model);
        }

        public ActionResult ConferenceVendors()
        {
            var model = _remoteControlViewModelQuery.Execute("conference_vendors");
            return View("GenericRemoteControl", model);
        }

        public ActionResult ShippingPolicy()
        {
            var model = _remoteControlViewModelQuery.Execute("shipping-policy");
            return View("GenericRemoteControl", model);
        }

        public ActionResult HowItWorks(bool autoplay = false)
        {
            return RedirectToActionPermanent("Index", "SellYourGear");
        }

        public ActionResult Robots()
        {
            Response.ContentType = "text/plain";
            var isPreview = _domainNameService.IsTestDomain(System.Web.HttpContext.Current.Request.Url);
            return View(isPreview);
        }

        public ActionResult TestMobile()
        {
            var getIsMobileObj = MobileService.GetIsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());
            getIsMobileObj.Guid = _mobileDeviceHelper.GetCurrentCustomer();


            return View("TestMobile", null, getIsMobileObj);
        }
    }
}
