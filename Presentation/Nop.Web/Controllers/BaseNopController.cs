﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Services.Logging;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.UI;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee.Website;
using WinmarkFranchise.Domain;
using WinmarkFranchise.Domain.Websites;


namespace Nop.Web.Controllers
{
	//[CustomerAuthorize]
    //[StoreLastVisitedPage]
    //[CheckAffiliate]
    //[StoreClosedAttribute]
    //[PublicStoreAllowNavigation]
    //[NopHttpsRequirement(SslRequirement.NoMatter)]
    [SessionState(SessionStateBehavior.Disabled)]
    public abstract partial class BaseNopController : Controller
    {
        private readonly Lazy<WebsiteSettings> _websiteSettings;
        [InjectDependency] private IWorkContext _workContext;
        [InjectDependency] private IWebsiteSettingsQuery _websiteSettingsQuery;
        [InjectDependency] private IOutOfProcessCacheManager _cacheManager;
        [InjectDependency] private IWinmarkFranchiseUnitOfWork _portalUow;
        [InjectDependency] private IDomainNameService _domainNameService;
        [InjectDependency] private ILogger _logger;

        protected WebsiteSettings WebsiteSettings { get { return _websiteSettings.Value; } }
        protected IWinmarkFranchiseUnitOfWork PortalUow { get { return _portalUow; } }
        protected IAccount Account { get { return _portalUow.Accounts.GetById(WebsiteSettings.AccountId); } }
        protected IWebsite Website { get { return _portalUow.Websites.GetById(WebsiteSettings.WebsiteId); } }

        protected BaseNopController()
        {
            _websiteSettings = new Lazy<WebsiteSettings>(() =>
                _workContext.SiteId == KnownIds.Website.Corporate
                    ? null
                    : _domainNameService.IsTestDomain(Request.Url)
                        ? _websiteSettingsQuery.Execute(_workContext.SiteId)
                        : _cacheManager.Get("website-settings:" + _workContext.SiteId, 10, () => _websiteSettingsQuery.Execute(_workContext.SiteId)));
        }


        #region Log Exceptions

        private readonly Action<Action> _swallowExceptionsPolicy = a => { try { a(); } catch { } };

        /// <summary>
		/// Log exception
		/// </summary>
		/// <param name="exc">Exception</param>
		private void LogException(Exception exc)
		{
			_swallowExceptionsPolicy(() => LogExceptionToNop(exc));
		}

		private void LogExceptionToNop(Exception exc)
		{
            var workContext = EngineContext.Current.Resolve<IWorkContext>();
            var logger = EngineContext.Current.Resolve<ILogger>();

            var customer = workContext.CurrentUser;
            logger.Error(exc.Message, exc, customer);
		}

        #endregion


        #region Notifications

        /// <summary>
        /// Display success notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void SuccessNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Success, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display error notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void ErrorNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Error, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display error notification
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        /// <param name="logException">A value indicating whether exception should be logged</param>
        protected virtual void ErrorNotification(Exception exception, bool persistForTheNextRequest = true, bool logException = true)
        {
            if (logException)
                LogException(exception);
            AddNotification(NotifyType.Error, exception.Message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display notification
        /// </summary>
        /// <param name="type">Notification type</param>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void AddNotification(NotifyType type, string message, bool persistForTheNextRequest)
        {
            string dataKey = string.Format("nop.notifications.{0}", type);
            if (persistForTheNextRequest)
            {
                if (TempData[dataKey] == null)
                    TempData[dataKey] = new List<string>();
                ((List<string>)TempData[dataKey]).Add(message);
            }
            else
            {
                if (ViewData[dataKey] == null)
                    ViewData[dataKey] = new List<string>();
                ((List<string>)ViewData[dataKey]).Add(message);
            }
        }

        #endregion


        /// <summary>
		/// Convenient way to add Google Analytics commands from different places (base class, actions, filters, etc)
		/// in context of ViewBag, making sure duplicate trackers aren't created etc.
		/// </summary>
	    protected GoogleAnalytics Ga {
		    get {
			    var ga = ViewBag.GoogleAnalytics as GoogleAnalytics;
			    if (ga == null)
				    ga = (ViewBag.GoogleAnalytics = new GoogleAnalytics());
			    return ga;
		    }
	    }

        protected ActionResult RedirectToCorporateVersionOfUrl()
        {
            var uriBuilder = new UriBuilder(Request.Url);
            uriBuilder.Host = string.Join(".", uriBuilder.Host.Split('.').Select(p => p.StartsWith("musicgoround") ? "musicgoround" : p));
            return RedirectPermanent(uriBuilder.Uri.ToString());
        }

	    protected override void OnActionExecuting(ActionExecutingContext filterContext) {
		    base.OnActionExecuting(filterContext);

			if (filterContext.IsChildAction || filterContext.HttpContext.Request.IsAjaxRequest())
				return;

            if (WebsiteSettings != null && !WebsiteSettings.PreviewMode) {
                if (!WebsiteSettings.IsActive || !WebsiteSettings.AccountIsActive) {
                    filterContext.Result = new RedirectResult(
						ConfigurationManager.AppSettings["CorporateStoreUrl"] ?? "http://www.musicgoround.com",
                        WebsiteSettings.NeedsPermanentRedirect);
					return;
			    }
		    }

		    var trackingSiteId = GetGaTrackingSiteId(_workContext);
		    if (trackingSiteId == KnownIds.Website.Corporate) {
			    Ga.AddCommand(KnownIds.GoogleAnalytics.Corporate, "require", "linker");
			    Ga.AddCommand(KnownIds.GoogleAnalytics.Corporate, "send", "pageview");
			    Ga.AddCommand(KnownIds.GoogleAnalytics.Rollup, "send", "pageview");
			    Ga.AddCommand(KnownIds.GoogleAnalytics.PureDriven, "send", "pageview");
		    }
		    else {
                string acct = null;
                if (trackingSiteId == _workContext.SiteId) {
                    acct = WebsiteSettings.IfNotNull(s => s.GoogleAnalyticsId);
                }
                else {
                    _logger.Information("Tracking GA across domains: trackingSiteId=" + trackingSiteId);
                    var settings = _websiteSettingsQuery.Execute(trackingSiteId);
                    if (settings == null)
                        _logger.Information("Tracking GA across domains: no WebsiteSettings for site " + trackingSiteId);
                    else if (settings.WebsiteId == 0)
                        _logger.Information("Tracking GA across domains: website lookup failed for site " + trackingSiteId);
                    else if (string.IsNullOrWhiteSpace(settings.GoogleAnalyticsId))
                        _logger.Information(string.Format("Tracking GA across domains: no GA account found for site={0}, domain={1}, PreviewMode={2}",
                            trackingSiteId, settings.Domain, settings.PreviewMode));
                    else {
                        _logger.Information(string.Format("Tracking GA across domains: found GA Account for site {0} ({1})", trackingSiteId, settings.GoogleAnalyticsId));
                        acct = settings.GoogleAnalyticsId;
                    }
                }
                if (!string.IsNullOrWhiteSpace(acct)) {
                    Ga.AddCommand(acct, "require", "linker");
                    Ga.AddCommand(acct, "linker:autoLink", new[] { "musicgoround.com" }, false, true);
                    Ga.AddCommand(acct, "send", "pageview");
                }
                Ga.AddCommand(KnownIds.GoogleAnalytics.Rollup, "send", "pageview");
            }

            // Store bool IsCorporate for cross brand bar
            ViewBag.IsCorporate = _workContext == null || _workContext.SiteId == KnownIds.Website.Corporate;
	    }

		/// <summary>
		/// If user has a cart, get the site ID they were on when they added their first item to it.
		/// Otherwise, get the current site ID
		/// </summary>
		/// <returns></returns>
	    private int GetGaTrackingSiteId(IWorkContext context) {
			var user = context.CurrentUser;
		    if (user == null)
				return context.SiteId;

		    var cartItems = user.ShoppingCartItems;
		    if (cartItems == null || !cartItems.Any())
				return context.SiteId;

		    var item = cartItems.FirstOrDefault(i => i.AddedFromSiteId.HasValue);
		    if (item == null)
				return context.SiteId;

		    return item.AddedFromSiteId.Value;
	    }
    }
}
