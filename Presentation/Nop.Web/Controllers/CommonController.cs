﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Forums;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Topics;
using Nop.Web.Extensions;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Themes;
using Nop.Web.Framework.UI.Captcha;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Common;
using Treefort.Common;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee.Website;
using Treefort.Portal.Services;

namespace Nop.Web.Controllers
{
    public partial class CommonController : BaseNopController
    {
        #region Fields

        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ITopicService _topicService;
        private readonly ILanguageService _languageService;
        private readonly ICurrencyService _currencyService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ISitemapGenerator _sitemapGenerator;
        private readonly IThemeContext _themeContext;
        private readonly IThemeProvider _themeProvider;
        private readonly IForumService _forumservice;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWebHelper _webHelper;
        private readonly IPermissionService _permissionService;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        private readonly HttpContextBase _httpContext;
        private readonly IOutOfProcessCacheManager _cacheManager;
        private readonly IWebsiteOptionsQuery _websiteOptionsQuery;
        private readonly IHeaderViewModelQuery _headerViewModelQuery;
        private readonly IWorkContext _context;
        private readonly ILocationService _locationService;

        private readonly CustomerSettings _customerSettings;
        private readonly TaxSettings _taxSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly CommonSettings _commonSettings;
        private readonly BlogSettings _blogSettings;
        private readonly ForumSettings _forumSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CaptchaSettings _captchaSettings;

        #endregion

        #region Constructors

        public CommonController(ICategoryService categoryService, IProductService productService,
            IManufacturerService manufacturerService, ITopicService topicService,
            ILanguageService languageService,
            ICurrencyService currencyService, ILocalizationService localizationService,
            IWorkContext workContext,
            IQueuedEmailService queuedEmailService, IEmailAccountService emailAccountService,
            ISitemapGenerator sitemapGenerator, IThemeContext themeContext,
            IThemeProvider themeProvider, IForumService forumService,
            IGenericAttributeService genericAttributeService, IWebHelper webHelper,
            IPermissionService permissionService, IMobileDeviceHelper mobileDeviceHelper,
			HttpContextBase httpContext, IOutOfProcessCacheManager cacheManager,
            CustomerSettings customerSettings, 
            TaxSettings taxSettings, CatalogSettings catalogSettings,
            StoreInformationSettings storeInformationSettings, EmailAccountSettings emailAccountSettings,
            CommonSettings commonSettings, BlogSettings blogSettings, ForumSettings forumSettings,
            LocalizationSettings localizationSettings, CaptchaSettings captchaSettings,
            IWorkContext context, IWebsiteOptionsQuery websiteOptionsQuery, IHeaderViewModelQuery headerViewModelQuery, 
            ILocationService locationService)
        {
            this._categoryService = categoryService;
            this._productService = productService;
            this._manufacturerService = manufacturerService;
            this._topicService = topicService;
            this._languageService = languageService;
            this._currencyService = currencyService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._queuedEmailService = queuedEmailService;
            this._emailAccountService = emailAccountService;
            this._sitemapGenerator = sitemapGenerator;
            this._themeContext = themeContext;
            this._themeProvider = themeProvider;
            this._forumservice = forumService;
            this._genericAttributeService = genericAttributeService;
            this._webHelper = webHelper;
            this._permissionService = permissionService;
            this._mobileDeviceHelper = mobileDeviceHelper;
            this._httpContext = httpContext;
            this._cacheManager = cacheManager;
            _context = context;
            _websiteOptionsQuery = websiteOptionsQuery;
            _headerViewModelQuery = headerViewModelQuery;
            _locationService = locationService;

            this._customerSettings = customerSettings;
            this._taxSettings = taxSettings;
            this._catalogSettings = catalogSettings;
            this._storeInformationSettings = storeInformationSettings;
            this._emailAccountSettings = emailAccountSettings;
            this._commonSettings = commonSettings;
            this._blogSettings = blogSettings;
            this._forumSettings = forumSettings;
            this._localizationSettings = localizationSettings;
            this._captchaSettings = captchaSettings;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected LanguageSelectorModel PrepareLanguageSelectorModel()
        {
            var availableLanguages = _cacheManager.Get(ModelCacheEventConsumer.AVAILABLE_LANGUAGES_MODEL_KEY, () =>
            {
                var result = _languageService
                    .GetAllLanguages()
                    .Select(x => x.ToModel())
                    .ToList();
                return result;
            });

            var model = new LanguageSelectorModel()
            {
                CurrentLanguage = _workContext.WorkingLanguage.ToModel(),
                AvailableLanguages = availableLanguages,
                UseImages = _localizationSettings.UseImagesForLanguageSelection
            };
            return model;
        }

        [NonAction]
        protected CurrencySelectorModel PrepareCurrencySelectorModel()
        {
            var availableCurrencies = _cacheManager.Get(string.Format(ModelCacheEventConsumer.AVAILABLE_CURRENCIES_MODEL_KEY, _workContext.WorkingLanguage.Id), () =>
            {
                var result = _currencyService
                    .GetAllCurrencies()
                    .Select(x => x.ToModel())
                    .ToList();
                return result;
            });

            var model = new CurrencySelectorModel()
            {
                CurrentCurrency = _workContext.WorkingCurrency.ToModel(),
                AvailableCurrencies = availableCurrencies
            };
            return model;
        }

        [NonAction]
        protected TaxTypeSelectorModel PrepareTaxTypeSelectorModel()
        {
            var model = new TaxTypeSelectorModel()
            {
                Enabled = _taxSettings.AllowCustomersToSelectTaxDisplayType,
                CurrentTaxType = _workContext.TaxDisplayType
            };
            return model;
        }

        [NonAction]
        protected int GetUnreadPrivateMessages()
        {
            var result = 0;
            var customer = _workContext.CurrentUser;
            if (_forumSettings.AllowPrivateMessages && !customer.IsGuest())
            {
                var privateMessages = _forumservice.GetAllPrivateMessages(0, customer.Id, false, null, false, string.Empty, 0, 1);

                if (privateMessages.TotalCount > 0)
                {
                    result = privateMessages.TotalCount;
                }
            }

            return result;
        }

        #endregion

        #region Methods

        //language
        [ChildActionOnly]
        public ActionResult LanguageSelector()
        {
            var model = PrepareLanguageSelectorModel();
            return PartialView(model);
        }
        public ActionResult SetLanguage(int langid)
        {
            var language = _languageService.GetLanguageById(langid);
            if (language != null && language.Published)
            {
                _workContext.WorkingLanguage = language;
            }


            if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
            {
                string applicationPath = HttpContext.Request.ApplicationPath;
                if (HttpContext.Request.UrlReferrer != null)
                {
                    string redirectUrl = HttpContext.Request.UrlReferrer.PathAndQuery;
                    if (redirectUrl.IsLocalizedUrl(applicationPath, true))
                    {
                        //already localized URL
                        redirectUrl = redirectUrl.RemoveLocalizedPathFromRawUrl(applicationPath);
                    }
                    redirectUrl = redirectUrl.AddLocalizedPathToRawUrl(applicationPath, _workContext.WorkingLanguage);
                    return Redirect(redirectUrl);
                }
                else
                {
                    string redirectUrl = Url.RouteUrl("HomePage");
                    redirectUrl = redirectUrl.AddLocalizedPathToRawUrl(applicationPath, _workContext.WorkingLanguage);
                    return Redirect(redirectUrl);
                }
            }
            else
            {
                //TODO: URL referrer is null in IE 8. Fix it
                if (HttpContext.Request.UrlReferrer != null)
                {
                    return Redirect(HttpContext.Request.UrlReferrer.PathAndQuery);
                }
                else
                {
                    return RedirectToRoute("HomePage");
                }
            }
        }

        //currency
        [ChildActionOnly]
        public ActionResult CurrencySelector()
        {
            var model = PrepareCurrencySelectorModel();
            return PartialView(model);
        }
        public ActionResult CurrencySelected(int customerCurrency)
        {
            var currency = _currencyService.GetCurrencyById(customerCurrency);
            if (currency != null)
                _workContext.WorkingCurrency = currency;

            //TODO: URL referrer is null in IE 8. Fix it
            if (HttpContext.Request.UrlReferrer != null)
            {
                return Redirect(HttpContext.Request.UrlReferrer.PathAndQuery);
            }
            else
            {
                return RedirectToRoute("HomePage");
            }
        }

        //tax type
        [ChildActionOnly]
        public ActionResult TaxTypeSelector()
        {
            var model = PrepareTaxTypeSelectorModel();
            return PartialView(model);
        }
        public ActionResult TaxTypeSelected(int customerTaxType)
        {
            var taxDisplayType = (TaxDisplayType)Enum.ToObject(typeof(TaxDisplayType), customerTaxType);
            _workContext.TaxDisplayType = taxDisplayType;

            //TODO: URL referrer is null in IE 8. Fix it
            if (HttpContext.Request.UrlReferrer != null)
            {
                return Redirect(HttpContext.Request.UrlReferrer.PathAndQuery);
            }
            else
            {
                return RedirectToRoute("HomePage");
            }
        }

        //Configuration page (used on mobile devices)
        [ChildActionOnly]
        public ActionResult ConfigButton()
        {
            var langModel = PrepareLanguageSelectorModel();
            var currModel = PrepareCurrencySelectorModel();
            var taxModel = PrepareTaxTypeSelectorModel();
            //should we display the button?
            if (langModel.AvailableLanguages.Count > 1 ||
                currModel.AvailableCurrencies.Count > 1 ||
                taxModel.Enabled)
                return PartialView();
            else
                return Content("");
        }

        public ActionResult Config()
        {
            return View();
        }
        
        //footer
        [ChildActionOnly]
        public ActionResult JavaScriptDisabledWarning()
        {
            if (!_commonSettings.DisplayJavaScriptDisabledWarning)
                return Content("");

            return PartialView();
        }

        //header links
        [DisableForCoop]
        [ChildActionOnly]
        public ActionResult HeaderLinks()
        {
            var customer = _workContext.CurrentUser;
			if (customer == null) {
				return PartialView(new HeaderLinksModel { ShoppingCartEnabled = true });
			}

            var unreadMessageCount = GetUnreadPrivateMessages();
            var unreadMessage = string.Empty;
            var alertMessage = string.Empty;
            if (unreadMessageCount > 0)
            {
                unreadMessage = string.Format(_localizationService.GetResource("PrivateMessages.TotalUnread"), unreadMessageCount);

                //notifications here
                if (_forumSettings.ShowAlertForPM && 
                    !customer.GetAttribute<bool>(SystemCustomerAttributeNames.NotifiedAboutNewPrivateMessages))
                {
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.NotifiedAboutNewPrivateMessages, true);
                    alertMessage = string.Format(_localizationService.GetResource("PrivateMessages.YouHaveUnreadPM"), unreadMessageCount);
                }
            }

            var model = new HeaderLinksModel()
            {
                IsAuthenticated = customer.IsRegistered(),
                CustomerEmailUsername = customer.IsRegistered() ? (_customerSettings.UsernamesEnabled ? customer.Username : customer.Email) : "",
                CustomerFirstName = customer.IsRegistered() ? customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName) : string.Empty,
                IsCustomerImpersonated = _workContext.OriginalCustomerIfImpersonated != null,
                DisplayAdminLink = _permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel),
                ShoppingCartEnabled = _permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart),
                ShoppingCartItems = customer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList().GetTotalProducts(),
                WishlistEnabled = _permissionService.Authorize(StandardPermissionProvider.EnableWishlist),
                WishlistItems = customer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist).ToList().GetTotalProducts(),
                AllowPrivateMessages = _forumSettings.AllowPrivateMessages,
                UnreadPrivateMessages = unreadMessage,
                AlertMessage = alertMessage,
            };

            return PartialView(model);
        }



        //footer
        [ChildActionOnly]
        public ActionResult Footer(bool hideOwnMgrFooter = false)
        {
            var model = new FooterModel()
            {
                StoreName = _storeInformationSettings.StoreName,
                IsFranchiseSite = _context.SiteId != KnownIds.Website.Corporate,
                IsCoop = _context.IsCoop,
                Categories = GetMenuCategories(),
                HideSellYourGear = _context.SiteId == 585
            };
            
            if (_context.SiteId != KnownIds.Website.Corporate)
            {
                model.FranchiseeLinks = _headerViewModelQuery.Execute(_context.SiteId);
                var location = _locationService.GetLocationContactInfoBySiteId(_context.SiteId);
                if (location != null)
                    model.ConstantContactUrl = location.ConstactContactUrl;
            }
            ViewBag.HideOwnMgrFooter = hideOwnMgrFooter;
            return PartialView(model);
        }

        private MenuModel.MenuCategories GetMenuCategories()
        {
            var cacheKey = "menu-categories";
            var categoryCodesToHideUsedOnNav = new[] { "ACGS", "ACHD", "ACDS" };

            Action<IList<MenuModel.CategoryInfo>> moveOtherToBottom = x => {
                var o = x.FirstOrDefault(y => y.Name.EndsWith("Other", StringComparison.Ordinal));
                if (o != null) {
                    x.Remove(o);
                    x.Add(o);
                }
            };

            var cacheModel = _cacheManager.Get(cacheKey, () => {
                var model = new MenuModel.MenuCategories();

                model.GuitarSubC =
                     _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.Guitars)
                                    .Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, SeName = x.GetSeName() })
                                    .ToList();
                moveOtherToBottom(model.GuitarSubC);

                model.PercussionSubC =
                    _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.Percussion)
                                    .Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, SeName = x.GetSeName() })
                                    .ToList();
                moveOtherToBottom(model.PercussionSubC);

                model.BandSubC =
                    _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.Band)
                                    .Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, SeName = x.GetSeName() })
                                    .ToList();
                moveOtherToBottom(model.BandSubC);

                model.KeyboardSubC =
                    _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.Keyboard)
                                    .Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, SeName = x.GetSeName() })
                                    .ToList();
                moveOtherToBottom(model.KeyboardSubC);

                model.ProSoundSubC =
                    _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.ProSound)
                                    .Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, SeName = x.GetSeName() })
                                    .ToList();
                moveOtherToBottom(model.ProSoundSubC);

                model.AccessoriesSubC =
                    _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.Accessories)
                                    .Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, SeName = x.GetSeName(), HideUsedFromNav = categoryCodesToHideUsedOnNav.Contains(x.DrsCode) })
                                    .ToList();
                moveOtherToBottom(model.AccessoriesSubC);

                Func<Category, MenuModel.CategoryInfo> catToMenu = c => new MenuModel.CategoryInfo { Id = c.Id,DrsCode = c.DrsCode, Name = c.Name, SeName = c.GetSeName() };
                model.Guitars = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.Guitars));
                model.Percussion = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.Percussion));
                model.Band = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.Band));
                model.Keyboard = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.Keyboard));
                model.ProSound = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.ProSound));
                model.Accessories = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.Accessories));
                model.AccessoriesBandInstruments = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesBandInstruments));
                model.AccessoriesDrumHeads = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesDrumHeads));
                model.AccessoriesGuitarStrings = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesGuitarStrings));
                model.AccessoriesGuitars = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesGuitars));
                model.AccessoriesKeyboards = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesKeyboards));
                model.AccessoriesOther = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesOther));
                model.AccessoriesPercussion = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesPercussion));
                model.AccessoriesProSound = catToMenu(_categoryService.GetCategoryById(KnownIds.Category.AccessoriesProSound));

                return model;
            });
            return cacheModel;
        }

        //menu
        [DisableForCoop]
        [ChildActionOnly, OutputCache(VaryByParam = "None", VaryByCustom = "Host", Duration = 600)]
        public ActionResult Menu()
        {
            var cacheKey = string.Format("menu-categories-brands-{0}", _context.SiteId);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var model = new MenuModel {Categories = GetMenuCategories()};
                var manufacturers = _manufacturerService.GetAllManufacturers();
                var rand = new Random();
                Func<MenuModel.CategoryInfo, IList<MenuModel.CategoryInfo>> getBrandMenusByCategory = (c =>
                {
                    var brandListId = c.DrsCode.GetBrandListId();
                    var brandDrsIds = brandListId.GetBrandDrsIds();
                    return manufacturers.Where(x => x.DrsId.HasValue && brandDrsIds.Contains(x.DrsId.Value))
                        .Select(
                            x =>
                                new MenuModel.CategoryInfo()
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    filterUrl =
                                        _webHelper.ModifyQueryString(
                                            Url.Action("Category", "Catalog",
                                                new {categoryId = c.Id, seName = x.GetSeName()}), "br=" + x.Id, null)
                                })
                        .Shuffle(rand)
                        .Take(30)
                        .OrderBy(x => x.Name)
                        .ToList();
                });
                if (_context.SiteId == KnownIds.Website.Corporate)
                {
                    model.GuitarBrands = CorporateNav.Guitars.Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, filterUrl = _webHelper.ModifyQueryString(Url.Action("Category", "Catalog", new { categoryId = KnownIds.Category.Guitars, seName = x.Name }), "br=" + x.Id, null) }).ToList();
                    model.PercussionBrands = CorporateNav.Percussion.Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, filterUrl = _webHelper.ModifyQueryString(Url.Action("Category", "Catalog", new { categoryId = KnownIds.Category.Percussion, seName = x.Name }), "br=" + x.Id, null) }).ToList();
                    model.BandBrands = CorporateNav.Band.Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, filterUrl = _webHelper.ModifyQueryString(Url.Action("Category", "Catalog", new { categoryId = KnownIds.Category.Band, seName = x.Name }), "br=" + x.Id, null) }).ToList();
                    model.KeyboardBrands = CorporateNav.Keyboard.Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, filterUrl = _webHelper.ModifyQueryString(Url.Action("Category", "Catalog", new { categoryId = KnownIds.Category.Keyboard, seName = x.Name }), "br=" + x.Id, null) }).ToList();
                    model.ProSoundBrands = CorporateNav.ProSound.Select(x => new MenuModel.CategoryInfo() { Id = x.Id, Name = x.Name, filterUrl = _webHelper.ModifyQueryString(Url.Action("Category", "Catalog", new { categoryId = KnownIds.Category.ProSound, seName = x.Name }), "br=" + x.Id, null) }).ToList();
                }
                else
                {
                    model.GuitarBrands =
                        _manufacturerService.GetProductManufacturersByCategoryId(KnownIds.Category.Guitars,
                            _context.SiteId)
                            .Select(
                                x =>
                                    new MenuModel.CategoryInfo()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        filterUrl =
                                            _webHelper.ModifyQueryString(
                                                Url.Action("Category", "Catalog",
                                                    new {categoryId = KnownIds.Category.Guitars, seName = x.GetSeName()}),
                                                "br=" + x.Id, null)
                                    })
                            .Shuffle(rand)
                            .Take(30)
                            .OrderBy(x => x.Name)
                            .ToList();
                    model.PercussionBrands =
                        _manufacturerService.GetProductManufacturersByCategoryId(KnownIds.Category.Percussion,
                            _context.SiteId)
                            .Select(
                                x =>
                                    new MenuModel.CategoryInfo()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        filterUrl =
                                            _webHelper.ModifyQueryString(
                                                Url.Action("Category", "Catalog",
                                                    new
                                                    {
                                                        categoryId = KnownIds.Category.Percussion,
                                                        seName = x.GetSeName()
                                                    }), "br=" + x.Id, null)
                                    })
                            .Shuffle(rand)
                            .Take(30)
                            .OrderBy(x => x.Name)
                            .ToList();
                    model.BandBrands =
                        _manufacturerService.GetProductManufacturersByCategoryId(KnownIds.Category.Band, _context.SiteId)
                            .Select(
                                x =>
                                    new MenuModel.CategoryInfo()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        filterUrl =
                                            _webHelper.ModifyQueryString(
                                                Url.Action("Category", "Catalog",
                                                    new {categoryId = KnownIds.Category.Band, seName = x.GetSeName()}),
                                                "br=" + x.Id, null)
                                    })
                            .ToList();
                    model.KeyboardBrands =
                        _manufacturerService.GetProductManufacturersByCategoryId(KnownIds.Category.Keyboard,
                            _context.SiteId)
                            .Select(
                                x =>
                                    new MenuModel.CategoryInfo()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        filterUrl =
                                            _webHelper.ModifyQueryString(
                                                Url.Action("Category", "Catalog",
                                                    new
                                                    {
                                                        categoryId = KnownIds.Category.Keyboard,
                                                        seName = x.GetSeName()
                                                    }), "br=" + x.Id, null)
                                    })
                            .Shuffle(rand)
                            .Take(30)
                            .OrderBy(x => x.Name)
                            .ToList();
                    model.ProSoundBrands =
                        _manufacturerService.GetProductManufacturersByCategoryId(KnownIds.Category.ProSound,
                            _context.SiteId)
                            .Select(
                                x =>
                                    new MenuModel.CategoryInfo()
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        filterUrl =
                                            _webHelper.ModifyQueryString(
                                                Url.Action("Category", "Catalog",
                                                    new
                                                    {
                                                        categoryId = KnownIds.Category.ProSound,
                                                        seName = x.GetSeName()
                                                    }), "br=" + x.Id, null)
                                    })
                            .Shuffle(rand)
                            .Take(30)
                            .OrderBy(x => x.Name)
                            .ToList();
                }
                return model;
            });

            cacheModel.RecentlyAddedProductsEnabled = _catalogSettings.RecentlyAddedProductsEnabled;
            cacheModel.BlogEnabled = _blogSettings.Enabled;
            cacheModel.ForumEnabled = _forumSettings.ForumsEnabled;
            cacheModel.CurrentUrl = _webHelper.GetThisPageUrl(true);
            cacheModel.MenuVisible = _headerViewModelQuery.Execute(_context.SiteId);
            cacheModel.IsCoop = _context.IsCoop;
            
            return PartialView(cacheModel);
        }

        //header links
        [ChildActionOnly]
        public ActionResult Header()
        {
            if (_context.SiteId != KnownIds.Website.Corporate)
            {
                var options = _websiteOptionsQuery.Execute(_context.SiteId);

                //Denver Co-Op site does not want sell us your gear visible - so let's hide it
                var model = new FranchiseHeaderModel
                {
                    LogoSubtext = options.LogoSubtext,
                    IsCoop = _context.IsCoop,
                    HideSellUsYourGear = _context.SiteId == 585,
                    BackgroundImageUrl = options.HomepageHeroImageUrl
                };
                model.FranchiseeLinks = _headerViewModelQuery.Execute(_context.SiteId);

                //var model = _locationService.GetLocationContactInfoBySiteId(_context.SiteId);
                return PartialView("FranchiseHeader", model);
            }

            return PartialView("Header");
        }


        //info block
        [ChildActionOnly]
        public ActionResult InfoBlock()
        {
            var model = new InfoBlockModel()
            {
                RecentlyAddedProductsEnabled = _catalogSettings.RecentlyAddedProductsEnabled,
                RecentlyViewedProductsEnabled = _catalogSettings.RecentlyViewedProductsEnabled,
                CompareProductsEnabled = _catalogSettings.CompareProductsEnabled,
                BlogEnabled = _blogSettings.Enabled,
                SitemapEnabled = _commonSettings.SitemapEnabled,
                ForumEnabled = _forumSettings.ForumsEnabled,
                AllowPrivateMessages = _forumSettings.AllowPrivateMessages,
            };

            return PartialView(model);
        }

        //contact us page
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ContactUs()
        {
            var model = new ContactUsModel()
            {
                Email = _workContext.CurrentUser.Email,
                FullName = _workContext.CurrentUser.GetFullName(),
                DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnContactUsPage
            };
            return View(model);
        }

        [HttpPost, ActionName("ContactUs")]
        [CaptchaValidator]
        public ActionResult ContactUsSend(ContactUsModel model, bool captchaValid)
        {
            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnContactUsPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
            }

            if (ModelState.IsValid)
            {
                string email = model.Email.Trim();
                string fullName = model.FullName;
                string subject = string.Format(_localizationService.GetResource("ContactUs.EmailSubject"), _storeInformationSettings.StoreName);

                var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                if (emailAccount == null)
                    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();

                string from = null;
                string fromName = null;
                string body = Core.Html.HtmlHelper.FormatText(model.Enquiry, false, true, false, false, false, false);
                //required for some SMTP servers
                if (_commonSettings.UseSystemEmailForContactUsForm)
                {
                    from = emailAccount.Email;
                    fromName = emailAccount.DisplayName;
                    body = string.Format("<strong>From</strong>: {0} - {1}<br /><br />{2}", 
                        Server.HtmlEncode(fullName), 
                        Server.HtmlEncode(email), body);
                }
                else
                {
                    from = email;
                    fromName = fullName;
                }
                _queuedEmailService.InsertQueuedEmail(new QueuedEmail()
                {
                    From = from,
                    FromName = fromName,
                    To = emailAccount.Email,
                    ToName = emailAccount.DisplayName,
                    Priority = 5,
                    Subject = subject,
                    Body = body,
                    CreatedOnUtc = DateTime.UtcNow,
                    EmailAccountId = emailAccount.Id
                });
                
                model.SuccessfullySent = true;
                model.Result = _localizationService.GetResource("ContactUs.YourEnquiryHasBeenSent");
                return View(model);
            }

            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnContactUsPage;
            return View(model);
        }

        //sitemap page
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Sitemap()
        {
            if (!_commonSettings.SitemapEnabled)
                return RedirectToRoute("HomePage");

            var model = new SitemapModel();
            if (_commonSettings.SitemapIncludeCategories)
            {
                var categories = _categoryService.GetAllCategories();
                model.Categories = categories.Select(x => x.ToModel()).ToList();
            }
            if (_commonSettings.SitemapIncludeManufacturers)
            {
                var manufacturers = _manufacturerService.GetAllManufacturers();
                model.Manufacturers = manufacturers.Select(x => x.ToModel()).ToList();
            }
            if (_commonSettings.SitemapIncludeProducts)
            {
                //limit product to 200 until paging is supported on this page
	            var products = _productService.SearchProducts(new ProductSearchOptions { PageSize = 200 });

                model.Products = products.Select(product => new ProductOverviewModel()
                {
                    Id = product.Id,
                    Name = product.GetLocalized(x => x.Name),
                    ShortDescription = product.GetLocalized(x => x.ShortDescription),
                    FullDescription = product.GetLocalized(x => x.FullDescription),
                    SeName = product.GetSeName(),
                }).ToList();
            }
            if (_commonSettings.SitemapIncludeTopics)
            {
                var topics = _topicService.GetAllTopics().ToList().FindAll(t => t.IncludeInSitemap);
                model.Topics = topics.Select(x => x.ToModel()).ToList();
            }

            model.IsCorpSite = KnownIds.Website.Corporate == _context.SiteId;
            model.IsCoopSite = _context.IsCoop;

            if(!model.IsCorpSite){
                model.FranchiseePages = _headerViewModelQuery.Execute(_context.SiteId);
            }
            return View(model);
        }

        //SEO sitemap page
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult SitemapSeo()
        {
            if (!_commonSettings.SitemapEnabled)
                return RedirectToRoute("HomePage");

            string siteMap = _sitemapGenerator.Generate();
            return Content(siteMap, "text/xml");
        }

        //store theme
        [ChildActionOnly]
        public ActionResult StoreThemeSelector()
        {
            if (!_storeInformationSettings.AllowCustomerToSelectTheme)
                return Content("");

            var model = new StoreThemeSelectorModel();
            var currentTheme = _themeProvider.GetThemeConfiguration(_themeContext.WorkingDesktopTheme);
            model.CurrentStoreTheme = new StoreThemeModel()
            {
                Name = currentTheme.ThemeName,
                Title = currentTheme.ThemeTitle
            };
            model.AvailableStoreThemes = _themeProvider.GetThemeConfigurations()
                //do not display themes for mobile devices
                .Where(x => !x.MobileTheme)
                .Select(x =>
                {
                    return new StoreThemeModel()
                    {
                        Name = x.ThemeName,
                        Title = x.ThemeTitle
                    };
                })
                .ToList();
            return PartialView(model);
        }

        public ActionResult StoreThemeSelected(string themeName)
        {
            _themeContext.WorkingDesktopTheme = themeName;
            
            var model = new StoreThemeSelectorModel();
            var currentTheme = _themeProvider.GetThemeConfiguration(_themeContext.WorkingDesktopTheme);
            model.CurrentStoreTheme = new StoreThemeModel()
            {
                Name = currentTheme.ThemeName,
                Title = currentTheme.ThemeTitle
            };
            model.AvailableStoreThemes = _themeProvider.GetThemeConfigurations()
                //do not display themes for mobile devices
                .Where(x => !x.MobileTheme)
                .Select(x =>
                {
                    return new StoreThemeModel()
                    {
                        Name = x.ThemeName,
                        Title = x.ThemeTitle
                    };
                })
                .ToList();
            return PartialView("StoreThemeSelector", model);
        }

        //favicon
        [ChildActionOnly]
        public ActionResult Favicon()
        {
            var model = new FaviconModel()
            {
                Uploaded = System.IO.File.Exists(System.IO.Path.Combine(Request.PhysicalApplicationPath, "favicon.ico")),
                FaviconUrl = _webHelper.GetStoreLocation() + "favicon.ico"
            };
            
            return PartialView(model);
        }

        /// <summary>
        /// Change presentation layer (desktop or mobile version)
        /// </summary>
        /// <param name="dontUseMobileVersion">True - use desktop version; false - use version for mobile devices</param>
        /// <returns>Action result</returns>
        public ActionResult ChangeDevice(bool dontUseMobileVersion)
        {
			if (_workContext.CurrentUser != null && _workContext.CurrentUser.SystemName != SystemCustomerNames.SearchEngine)
				_genericAttributeService.SaveAttribute(_workContext.CurrentUser, SystemCustomerAttributeNames.DontUseMobileVersion, dontUseMobileVersion);

            //TODO: URL referrer is null in IE 8. Fix it
            if (HttpContext.Request.UrlReferrer != null)
            {
                return Redirect(HttpContext.Request.UrlReferrer.PathAndQuery);
            }
            else
            {
                return RedirectToRoute("HomePage");
            }
        }
        [ChildActionOnly]
        public ActionResult ChangeDeviceBlock()
        {
            if (!_mobileDeviceHelper.MobileDevicesSupported())
                //mobile devices support is disabled
                return Content("");

            if (!_mobileDeviceHelper.IsMobileDevice(_httpContext))
                //request is made by a desktop computer
                return Content("");

            return View();
        }





        [ChildActionOnly]
        public ActionResult EuCookieLaw()
        {
            if (!_storeInformationSettings.DisplayEuCookieLawWarning)
                //disabled
                return Content("");

            if (_workContext.CurrentUser.GetAttribute<bool>("EuCookieLaw.Accepted"))
                //already accepted
                return Content("");

            return PartialView();
        }

        [HttpPost]
        public ActionResult EuCookieLawAccept()
        {
            if (!_storeInformationSettings.DisplayEuCookieLawWarning)
                //disabled
                return Json(new { stored = false });

            //save setting
			if (_workContext.CurrentUser != null)
				_genericAttributeService.SaveAttribute(_workContext.CurrentUser, "EuCookieLaw.Accepted", true);
            return Json(new { stored = true });
        }

        // Franchise Header and Footer Embed
        [ChildActionOnly]
        public ActionResult FranchiseHeaderEmbed()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
            {
                return new EmptyResult();
            }
            var options = _websiteOptionsQuery.Execute(_context.SiteId);
            return PartialView("_FranchiseScriptEmbed", options.HeaderEmbedScript);
        }

        [ChildActionOnly]
        public ActionResult FranchiseFooterEmbed()
        {
            if (_context.SiteId == KnownIds.Website.Corporate)
            {
                return new EmptyResult();
            }

            var options = _websiteOptionsQuery.Execute(_context.SiteId);
            return PartialView("_FranchiseScriptEmbed", options.FooterEmbedScript);
        }

        #endregion
    }
}
