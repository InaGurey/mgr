﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Xml.Linq;
using Nop.Core;
using Nop.Services.Logging;
using Nop.Web.Framework.Controllers;
using SimpleHoneypot.ActionFilters;
using SimpleHoneypot.Extensions;
using Treefort.Common.Mvc;
using Treefort.Portal;
using Treefort.Portal.Queries.Home;
using Treefort.Portal.Queries.Own;
using Treefort.Portal.Services;
using Winmark.CrmLeadIntegration;

namespace Nop.Web.Controllers
{
    public class OwnController : BaseNopController
    {
        private readonly IWorkContext _context;
        private ITerritoryInfoViewModelQuery _territoryInfoViewModelQuery { get; set; }
        private ITerritoriesViewModelQuery _territoriesViewModelQuery { get; set; }
        private IOwnPageViewModelQuery _ownPageViewModelQuery { get; set; }
        private IGetQualifyViewModelQuery _getQualifyViewModelQuery { get; set; }
        private IRemoteControlViewModelQuery _remoteControlViewModelQuery { get; set; }
		private ILogger _logger { get; set; }

        private readonly IPrequalificationService _mailService;

        public OwnController(
            IWorkContext context
            , ITerritoryInfoViewModelQuery territoryInfoViewModelQuery
            , ITerritoriesViewModelQuery territoriesViewModelQuery
            , IOwnPageViewModelQuery ownPageViewModelQuery
            , IGetQualifyViewModelQuery getQualifyViewModelQuery
            , IPrequalificationService mailService
            , IRemoteControlViewModelQuery remoteControlViewModelQuery
			, ILogger logger)
        {
            _context = context;
            _territoryInfoViewModelQuery = territoryInfoViewModelQuery;
            _territoriesViewModelQuery = territoriesViewModelQuery;
            _ownPageViewModelQuery = ownPageViewModelQuery;
            _getQualifyViewModelQuery = getQualifyViewModelQuery;
            _mailService = mailService;
            _remoteControlViewModelQuery = remoteControlViewModelQuery;
	        _logger = logger;
        }


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (_context.SiteId != KnownIds.Website.Corporate) {
                filterContext.Result = RedirectToCorporateVersionOfUrl();
                return;
            }
            base.OnActionExecuting(filterContext);
        }


        public virtual ActionResult Index()
        {
            var model = _ownPageViewModelQuery.Execute("/own");
            ViewBag.HideOwnMgrFooter = true;
            return View(model);
        }

        public virtual ActionResult FAQ()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/about/frequently-asked-questions/");
        }

        public virtual ActionResult FranchiseeTestimonials()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/testimonials");
        }

        public virtual ActionResult Support()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/support");
        }
        public virtual ActionResult Investment()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/investment");
        }

        //public virtual ActionResult CanadianInvestment()
        //{
        //    var model = _ownPageViewModelQuery.Execute("/own-a-pias/canadian-investment");

        //    return View(model);
        //}

        public virtual ActionResult Territories(string id = "")
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/territories");
        }

        public virtual ActionResult Qualify()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/qualification-form");
        }

       

        public virtual ActionResult QualifyTerms()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/support");
        }

      
        public virtual ActionResult QualifyConfirm()
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/support");
        }

        public virtual ActionResult Commercials(string videoNumber)
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/franchise-commercials");
        }

        public virtual ActionResult FranchiseMovies(string videoNumber)
        {
            var model = new CommercialVideoPlayerViewModel();
            model.videoNumber = String.IsNullOrEmpty(videoNumber) ? "1" : videoNumber;
            return View(model);
        }

        //public virtual ActionResult StoreView(string videoNumber)
        //{
        //    var model = new CommercialVideoPlayerViewModel();

        //    model.videoNumber = String.IsNullOrEmpty(videoNumber) ? "1" : videoNumber;

        //    return View(model);
        //}

        public virtual ActionResult Testimonials(string id)
        {
            return RedirectPermanent("http://www.winmarkfranchises.com/music-go-round/testimonials");
        }

        public virtual ActionResult TestimonialLinks(string id)
        {
            var m = new TestimonialLinksViewModel();
            m.Videos = GetTestimonialsFromXml();
            return PartialView("_TestimonialLinks", m);
        }

        public virtual ActionResult TestimonialWidget()
        {
            var model = new TestimonialWidgetViewModel();

            model.Videos = GetTestimonialsFromXml();

            foreach (var v in model.Videos)
            {
                v.Size = new System.Drawing.Size(300, 221);
            }

            return PartialView("_TestimonialWidget", model);
        }

        private List<VideoPlayerSettings> GetTestimonialsFromXml()
        {
            var cacheKey = "testimonials";
            var filePath = Server.MapPath("~/App_Data/testimonials.xml");

            var results = (ControllerContext.HttpContext.Cache[cacheKey] as List<VideoPlayerSettings>);
            if (results == null)
            {
                var doc = XDocument.Load(filePath);
                if (doc.Descendants("video").Any())
                {
                    results = (from e in doc.Descendants("video") select new VideoPlayerSettings(e)).ToList();

                    foreach (var v in results)
                    {

                        v.EnableDownload = false;
                        v.ExpressInstall = "https://s3.amazonaws.com/playitagainsports.com/expressInstall.swf";
                        v.Swf = "https://s3.amazonaws.com/playitagainsports.com/flowplayer-3.2.4.swf";
                    }

                    ControllerContext.HttpContext.Cache.Insert(cacheKey, results, new System.Web.Caching.CacheDependency(filePath));
                }
                if (results == null)
                {
                    results = new List<VideoPlayerSettings>();
                }
            }
            return results;
        }

        public bool IsAjaxRequest
        {
            get { return (HttpContext.Request.Headers["X-Requested-With"] ?? string.Empty) == "XMLHttpRequest"; }
        }

        public void AddErrors(IEnumerable<ControllerError> errors)
        {
            if (errors != null)
            {
                foreach (var e in errors)
                {
                    AddError(e);
                }
            }
        }
        public bool HasErrors
        {
            get
            {
                return this.Errors.Count > 0;
            }
        }

        public void AddError(ControllerError e)
        {
            this.Errors.Add(e);
            ModelState.AddModelError(e.Key, e.Message);
        }

        private List<ControllerError> _Errors = new List<ControllerError>();
        public List<ControllerError> Errors
        {
            get { return _Errors; }
            set { _Errors = value; }
        }

    }
}
