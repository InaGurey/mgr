﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Services.Media;
using Nop.Web.Framework.Security;
using Nop.Web.Models.SellYourGear;
using SimpleHoneypot.ActionFilters;
using SimpleHoneypot.Extensions;
using Treefort.Common.Mvc;
using Treefort.Portal;
using Treefort.Portal.Queries.Home;
using Treefort.Portal.Services;



namespace Nop.Web.Controllers
{
    public class SellYourGearController : BaseNopController
    {
        private readonly IWorkContext _context;
        private readonly ISellYourGearService _sellYourGearService;
        private readonly IPictureService _pictureService;
        private readonly IRemoteControlViewModelQuery _remoteControlViewModelQuery;
        private readonly ILocationService _locationService;

        public SellYourGearController(IWorkContext context
            , IRemoteControlViewModelQuery remoteControlViewModelQuery
			, ISellYourGearService sellYourGearService
			, IPictureService pictureService
            , ILocationService locationService)
        {
	        _context = context;
            _remoteControlViewModelQuery = remoteControlViewModelQuery;
	        _sellYourGearService = sellYourGearService;
	        _pictureService = pictureService;
            _locationService = locationService;
        }


        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.AutoPlay = false;
            var model = new CommonViewModel();
            model.RcContent = _remoteControlViewModelQuery.Execute("sellusyourgear");
            model.IsFranchiseSite = _context.SiteId != KnownIds.Website.Corporate;

            return View(model);
        }


        [HttpGet]
        public ActionResult Submitted()
        {
            var model = new CommonViewModel();
            model.RcContent = _remoteControlViewModelQuery.Execute("sellusyourgear");
            model.IsFranchiseSite = _context.SiteId != KnownIds.Website.Corporate;

            return View(model);
        }


        public ActionResult FindLocations(string zip)
        {
            ViewBag.Zip = zip;
            var locations = _locationService.GetLocationsByPostalCode(zip, int.MaxValue, 60);
            return locations.Any() ? PartialView("_SelectLocation", locations) : PartialView("_OutOfRange");
        }

        [HttpGet]
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Form(int locationId, string zip)
        {
            var model = new FormViewModel {LocationId = locationId, Zip = zip};
            FillSellYourGearDropDowns(model);
            model.PictureOne = new FormViewModel.SellUsPicture();
            model.PictureTwo = new FormViewModel.SellUsPicture();
            model.PictureThree = new FormViewModel.SellUsPicture();
            model.PictureFour = new FormViewModel.SellUsPicture();
            model.RcContent = _remoteControlViewModelQuery.Execute("sellusyourgear");
            model.IsFranchiseSite = _context.SiteId != KnownIds.Website.Corporate;

            return View(model);
        }

        [HttpPost]
        [Honeypot(true)]
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult Form(FormViewModel model)
        {
            if (Request.HoneypotFaild())
            {
                return RedirectToAction("Submitted");
            }

            if (string.IsNullOrEmpty(model.Phone) && string.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError("Phone", "Please provide either a phone number or an email address for us to reach you.");
            }

            if (!ModelState.IsValid)
            {
                FillSellYourGearDropDowns(model);
                model.RcContent = _remoteControlViewModelQuery.Execute("sellusyourgear");
                model.IsFranchiseSite = _context.SiteId != KnownIds.Website.Corporate;
                return View(model);
            }

            var request = new SellYourGearSubmissionRequest
            {
                LocationId = model.LocationId,
                CustomerName = model.Name,
                EmailAddress = model.Email,
                PhoneNumber = model.Phone,
                ContactPreference = model.ContactPreference,
                City = model.City,
                Region = model.State,
                PostalCode = model.Zip,
                GearType = model.GearType,
                MakeAndModel = model.MakeAndModel,
                Color = model.Color,
                Year = model.Year,
                Condition = model.Condition,
                Describe = model.Describe,
                Modifications = model.Modifications,
                Picture1 = GetPictureBytes(model.PictureOne),
                Picture2 = GetPictureBytes(model.PictureTwo),
                Picture3 = GetPictureBytes(model.PictureThree),
                Picture4 = GetPictureBytes(model.PictureFour)
            };

            _sellYourGearService.Submit(request);

            return RedirectToAction("Submitted");
        }

        private SellYourGearSubmissionRequest.UploadedPicture GetPictureBytes(FormViewModel.SellUsPicture uploadedPicture)
        {
            if (uploadedPicture == null || uploadedPicture.PictureId == 0) { return null; }

            var pic = _pictureService.GetPictureById(uploadedPicture.PictureId);
            if (pic == null) { return null; }

            var bytes = _pictureService.LoadPictureBinary(pic);

            return bytes == null || bytes.Length == 0
                        ? null
                        : new SellYourGearSubmissionRequest.UploadedPicture { Bytes = bytes, MimeType = pic.MimeType };
        }

        private void FillSellYourGearDropDowns(FormViewModel model)
        {
            model.GearTypeOptions = new BindingList<SelectListItem> {
                    new SelectListItem {Value = "Guitar", Text = "Guitar", Selected = true},
                    new SelectListItem {Value = "Percussion", Text = "Percussion"},
                    new SelectListItem {Value = "Band", Text = "Band"},
                    new SelectListItem {Value = "Keyboard", Text = "Keyboard"},
                    new SelectListItem {Value = "Pro Sound", Text = "Pro Sound",},
                    new SelectListItem {Value = "Accessories", Text = "Accessories"},
                    new SelectListItem {Value = "Other", Text = "Other"}
                };

            model.ConditionOptions = new BindingList<SelectListItem> {
                    new SelectListItem {Value = "Very Good", Text = "Very Good - little wear or use", Selected = true},
                    new SelectListItem {Value = "Good", Text = "Good - used but still in great shape"},
                    new SelectListItem {Value = "Average", Text = "Average - plenty of use, but still usable"}
                };

            model.ContactPreferenceOptions = new BindingList<SelectListItem> {
                    new SelectListItem {Value = "", Text = "", Selected = true},
                    new SelectListItem {Value = "Phone", Text = "Phone"},
                    new SelectListItem {Value = "Email", Text = "Email"}
                };
        }

    }
}
