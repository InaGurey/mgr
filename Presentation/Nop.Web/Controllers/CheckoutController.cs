﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.Common;
using Treefort.Common;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee.Location;
using Treefort.Portal.Services;
using ObjectExtensions = Nop.Core.ObjectExtensions;

namespace Nop.Web.Controllers
{
    [DisableForCoop]
    [SessionState(SessionStateBehavior.Required)]
    [NopHttpsRequirement(SslRequirement.Yes)]
    public partial class CheckoutController : BaseNopController
    {
		#region Fields

        private readonly IWorkContext _workContext;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILocalizationService _localizationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly HttpContextBase _httpContext;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        

        private readonly OrderSettings _orderSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PaymentSettings _paymentSettings;
	    private readonly IPortalQueries _portalQueries;
	    private readonly IAddressService _addressService;
        private readonly IGenericLocationViewModelQuery _genericLocationViewModelQuery;

	    #endregion

		#region Constructors

        public CheckoutController(IWorkContext workContext,
            IShoppingCartService shoppingCartService, ILocalizationService localizationService, 
            ITaxService taxService, ICurrencyService currencyService, 
            IPriceFormatter priceFormatter, IOrderProcessingService orderProcessingService,
            ICustomerService customerService,  IGenericAttributeService genericAttributeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService, IShippingService shippingService, 
            IPaymentService paymentService, IOrderTotalCalculationService orderTotalCalculationService,
            ILogger logger, IOrderService orderService, IWebHelper webHelper,
            HttpContextBase httpContext, IMobileDeviceHelper mobileDeviceHelper,
            OrderSettings orderSettings, RewardPointsSettings rewardPointsSettings,
            PaymentSettings paymentSettings, IPortalQueries portalQueries, IAddressService addressService,
            IGenericLocationViewModelQuery genericLocationViewModelQuery)
        {
            this._workContext = workContext;
            this._shoppingCartService = shoppingCartService;
            this._localizationService = localizationService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._customerService = customerService;
            this._genericAttributeService = genericAttributeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._paymentService = paymentService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._logger = logger;
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._httpContext = httpContext;
            this._mobileDeviceHelper = mobileDeviceHelper;

            this._orderSettings = orderSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._paymentSettings = paymentSettings;
	        _portalQueries = portalQueries;
	        _addressService = addressService;
            this._genericLocationViewModelQuery = genericLocationViewModelQuery;
        }

        #endregion

        #region Utilities

        public ProcessPaymentRequest CurrentPaymentInfo
        {
            get { return _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest; }
            set { _httpContext.Session["OrderPaymentInfo"] = value; }
        }

        [NonAction]
        protected bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false) {
	        return _orderTotalCalculationService.GetShoppingCartSubTotal(cart) > decimal.Zero;
        }

        [NonAction]
        protected CheckoutBillingAddressModel PrepareBillingAddressModel(IList<ShoppingCartItem> cart, int? selectedCountryId = null)
        {
            var model = new CheckoutBillingAddressModel();
            //existing addresses
            var addresses = _workContext.CurrentUser.Addresses.Where(a => a.StateProvince != null && a.Country != null && a.Country.AllowsBilling).ToList();
            foreach (var address in addresses)
                model.ExistingAddresses.Add(address.ToModel());

            //new address model
            model.NewAddress = new AddressModel();
            //countries
            //model.NewAddress.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountriesForBilling())
                model.NewAddress.AvailableCountries.Add(new SelectListItem() { Text = c.GetLocalized(x => x.Name), Value = c.Id.ToString(), Selected = (selectedCountryId.HasValue && c.Id == selectedCountryId.Value) });
            //states
            var states = _stateProvinceService.GetStateProvincesByCountryId(selectedCountryId ?? 1).ToList();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.NewAddress.AvailableStates.Add(new SelectListItem() { Text = s.GetLocalized(x => x.Name), Value = s.Id.ToString() });
            }
            else
                model.NewAddress.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

            model.NewAddress.AvailableStates.Insert(0, new SelectListItem() { Text = "Choose State", Value = "-1" });

			model.Stores = GetStores(cart);

            return model;
        }

        [NonAction]
        protected CheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null)
        {
            var model = new CheckoutShippingAddressModel();
            //existing addresses
            var addresses = _workContext.CurrentUser.Addresses.Where(a => a.Country == null || a.Country.AllowsShipping).ToList();
            foreach (var address in addresses)
                model.ExistingAddresses.Add(address.ToModel());

            //new address model
            model.NewAddress = new AddressModel();
            //countries
            //model.NewAddress.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountriesForShipping())
                model.NewAddress.AvailableCountries.Add(new SelectListItem() { Text = c.GetLocalized(x => x.Name), Value = c.Id.ToString(), Selected = (selectedCountryId.HasValue && c.Id == selectedCountryId.Value) });
            //states
            var states = _stateProvinceService.GetStateProvincesByCountryId(selectedCountryId ?? 1).ToList();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.NewAddress.AvailableStates.Add(new SelectListItem() { Text = s.GetLocalized(x => x.Name), Value = s.Id.ToString() });
            }
            else
                model.NewAddress.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

            return model;
        }

        [NonAction]
        protected CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutShippingMethodModel();

            var getShippingOptionResponse = _shippingService.GetShippingOptions(cart, _workContext.CurrentUser.ShippingAddress);
            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentUser, SystemCustomerAttributeNames.OfferedShippingOptions, getShippingOptionResponse.ShippingOptions);

				//var storeCarts = _shoppingCartService.SplitCartByStore(cart, _workContext.CurrentCustomer.ShippingAddress.ZipPostalCode);

                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel()
                    {
                        Name = shippingOption.Name,
                        Description = shippingOption.Description,
                        ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                    };

                    //adjust rate
					Discount appliedDiscount = null;
	                decimal rate = decimal.Zero;
	                if (!shippingOption.IsInStorePickup) {
		                var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(shippingOption.Rate, cart, out appliedDiscount);
		                rate = _currencyService.ConvertFromPrimaryStoreCurrency(shippingTotal, _workContext.WorkingCurrency);
	                }
	                soModel.Fee = shippingOption.IsInStorePickup ? string.Empty : _priceFormatter.FormatShippingPrice(rate, true);

                    model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var lastShippingOption = _workContext.CurrentUser.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.LastShippingOption);
                if (lastShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                        .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(lastShippingOption.Name, StringComparison.InvariantCultureIgnoreCase) &&
                        !String.IsNullOrEmpty(so.ShippingRateComputationMethodSystemName) && so.ShippingRateComputationMethodSystemName.Equals(lastShippingOption.ShippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.Where(so => so.Selected).FirstOrDefault() == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }

	            model.Stores = GetStores(cart);
            }
            else
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);

            return model;
        }

        [NonAction]
        protected CheckoutPaymentMethodModel PreparePaymentMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutPaymentMethodModel();

            //reward points
            if (_rewardPointsSettings.Enabled && !cart.IsRecurring())
            {
                int rewardPointsBalance = _workContext.CurrentUser.GetRewardPointsBalance();
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero)
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                }
            }

            var boundPaymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentUser.Id)
                .Where(
                    pm =>
                        pm.PaymentMethodType == PaymentMethodType.Standard ||
                        pm.PaymentMethodType == PaymentMethodType.Redirection ||
                        pm.PaymentMethodType == PaymentMethodType.Button)
                .ToList();

            foreach (var pm in boundPaymentMethods)
            {
				var storeCarts = _shoppingCartService.SplitCartByStore(cart);

				// Treefort enhancement
				if (pm.IsPayPal() || pm.IsPayPalExpress()) {
					if (ObjectExtensions.IfNotNull(_workContext.CurrentShippingOption, so => so.IsInStorePickup, true) || cart.Any(i => !i.IsShipEnabled))
						continue; // no PayPal if in-store pickup (just doing what I'm told)

					if (storeCarts.Count > 1)
						continue; // no PayPal if ordering from multiple stores (saves us some headaches)

					string whyNot;
					if (!pm.VerifyCredentials(storeCarts.First().SiteId, out whyNot))
						continue; // no PayPal if not properly configured for this store
				}
				else {
					// not paypal, must be A.NET. If zee isn't set up properly, throw big fat exception.
					foreach (var storeCart in storeCarts) {
						string whyNot;
						if (!pm.VerifyCredentials(storeCarts.First().SiteId, out whyNot)) {
							var msg = "Purchase cannot be completed. ";
							var location = storeCart.StoreLocation;
							msg += (location != null) ?
								string.Format("Music Go Round {0} is not correctly set up to accept online payments.", location.Name) :
								whyNot;
							throw new Exception(msg);
						}
					}
				}

                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel()
                {
                    Name = pm.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id),
                    PaymentMethodSystemName = pm.PluginDescriptor.SystemName,
                };
                //payment method additional fee
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(pm.PluginDescriptor.SystemName);
                decimal rateBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentUser);
                decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                if (rate > decimal.Zero)
                    pmModel.Fee = _priceFormatter.FormatPaymentMethodAdditionalFee(rate, true);

                model.PaymentMethods.Add(pmModel);
            }
            
            //find a selected (previously) payment method
            if (!String.IsNullOrEmpty(_workContext.CurrentUser.SelectedPaymentMethodSystemName))
            {
                var paymentMethodToSelect = model.PaymentMethods.ToList()
                    .Find(pm => pm.PaymentMethodSystemName.Equals(_workContext.CurrentUser.SelectedPaymentMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }
            //if no option has been selected, let's do it for the first one
            if (model.PaymentMethods.Where(so => so.Selected).FirstOrDefault() == null)
            {
                var paymentMethodToSelect = model.PaymentMethods.FirstOrDefault();
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }

            return model;
        }

        [NonAction]
        protected CheckoutPaymentInfoModel PreparePaymentInfoModel(IPaymentMethod paymentMethod, IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutPaymentInfoModel();
            string actionName;
            string controllerName;
            RouteValueDictionary routeValues;
            paymentMethod.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);
            model.PaymentInfoActionName = actionName;
            model.PaymentInfoControllerName = controllerName;
            model.DisplayOrderTotals = _orderSettings.OnePageCheckoutDisplayOrderTotalsOnPaymentInfoTab;

            if (paymentMethod.IsPayPalExpress())
            {
                var storeCarts = _shoppingCartService.SplitCartByStore(cart);
                var total = GetOrderTotal(cart);
                routeValues.Add("customerId", _workContext.CurrentUser.Id);
                routeValues.Add("siteId", storeCarts.First().SiteId);
                routeValues.Add("orderTotal", total);
            }

            model.PaymentInfoRouteValues = routeValues;

            return model;
        }

        [NonAction]
        protected CheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutConfirmModel();

			// Treefort "enhancement" - skip this check. I would require doing the order split and getting store address(es)

            //min order amount validation
			//bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
			//if (!minOrderTotalAmountOk)
			//{
			//	decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
			//	model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
			//}

            return model;
        }

        [NonAction]
        protected bool UseOnePageCheckout()
        {
            bool useMobileDevice = _mobileDeviceHelper.IsMobileDevice(_httpContext)
                && _mobileDeviceHelper.MobileDevicesSupported()
                && !_mobileDeviceHelper.CustomerDontUseMobileVersion();

            //mobile version doesn't support one-page checkout
            if (useMobileDevice)
                return false;

            //check the appropriate setting
            return _orderSettings.OnePageCheckoutEnabled;
        }

        [NonAction]
        protected bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrderPlaced = _orderService.GetOrdersByCustomerId(_workContext.CurrentUser.Id)
                .FirstOrDefault();
            if (lastOrderPlaced == null)
                return true;
            
            var interval = DateTime.UtcNow - lastOrderPlaced.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

		[NonAction]
		protected IList<CheckoutStoreModel> GetStores(IList<ShoppingCartItem> cart) {
			return (
				from sc in _shoppingCartService.SplitCartByStore(cart)
				where sc.StoreLocation != null
				group sc by sc.StoreLocation.Id into g
				let location = g.First().StoreLocation
				select new CheckoutStoreModel {
					LocationId = location.Id,
					Name = location.Name,
					Address1 = location.AddressLine1,
					Address2 = location.AddressLine2,
					City = location.City,
					State = location.Region,
					Zip = location.PostalCode,
					Email = location.EmailAddress,
					Phone = location.PhoneNumber,
					HasInStorePickupOnlyItems = g.Any(c => c.Items.Any(i => !i.IsShipEnabled))
				}).ToList();
		}

        [NonAction]
        protected void SaveShippingMethodInternal(string selectedOption, IList<ShoppingCartItem> cart)
        {
            //parse selected method 
            string shippingoption = selectedOption;
            if (String.IsNullOrEmpty(shippingoption))
                throw new Exception("Selected shipping method can't be parsed");
            var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 2)
                throw new Exception("Selected shipping method can't be parsed");
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];

            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentUser.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions);
            if (shippingOptions == null || shippingOptions.Count == 0)
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentUser.ShippingAddress, shippingRateComputationMethodSystemName)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                throw new Exception("Selected shipping method can't be loaded");

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentUser, SystemCustomerAttributeNames.LastShippingOption, shippingOption);
        }

        [NonAction]
        protected decimal GetOrderTotal(IList<ShoppingCartItem> cart)
        {
            var storeCarts = _shoppingCartService.SplitCartByStore(cart);

            //total
            var orderTotalDiscountAmountBase = decimal.Zero;
            Discount orderTotalAppliedDiscount = null;
            var appliedGiftCards = new List<AppliedGiftCard>();
            var redeemedRewardPoints = 0;
            var redeemedRewardPointsAmount = decimal.Zero;
            var shoppingCartTotalBase = decimal.Zero;

            foreach (var storeCart in storeCarts)
            {
                decimal otdab = decimal.Zero;
                Discount otad = null;
                List<AppliedGiftCard> agc = null;
                int rrp = 0;
                decimal rrpa = decimal.Zero;
                shoppingCartTotalBase +=
                    _orderTotalCalculationService.GetShoppingCartTotal(storeCart.StoreLocation, storeCart.Items, out otdab, out otad, out agc, out rrp, out rrpa) ?? 0;

                orderTotalDiscountAmountBase += otdab;
                if (otad != null) orderTotalAppliedDiscount = otad;
                if (agc != null) appliedGiftCards.AddRange(agc);
                redeemedRewardPoints += rrp;
                redeemedRewardPointsAmount += rrpa;
            }

            return _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTotalBase, _workContext.WorkingCurrency);
        }

        protected object GetJsonCommand(IList<ShoppingCartItem> cart, string section)
        {
            if (section == "payment_method")
            {

                var paymentMethodModel = PreparePaymentMethodModel(cart);

                return new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-method",
                        html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                    },
                    goto_section = "payment_method"
                };
            }
            else if (section == "payment_info")
            {
                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(_workContext.CurrentUser.SelectedPaymentMethodSystemName);
                if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                    throw new Exception("Selected payment method can't be parsed");


                var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst, cart);
                return new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    },
                    goto_section = "payment_info"
                };
            }
            else if (section == "confirm_order")
            {
                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                };
            }

            return null;
        }

        #endregion

        #region Methods (multistep checkout)

        public ActionResult Index(string _ga)
        {
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart", new { _ga });

            if (_orderService.IsCurrentCustomerBlocked())
                return Redirect(string.Format("{0}checkout/CheckoutBlocked", _webHelper.GetStoreLocation()));

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //reset checkout data
            _customerService.ResetCheckoutData(_workContext.CurrentUser);

            //validation (cart)
            var scWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, _workContext.CurrentUser.CheckoutAttributes, true);
            if (scWarnings.Count > 0)
                return RedirectToRoute("ShoppingCart", new { _ga });
            //validation (each shopping cart item)
            foreach (ShoppingCartItem sci in cart)
            {
                var sciWarnings = _shoppingCartService.GetShoppingCartItemWarnings(_workContext.CurrentUser,
                    sci.ShoppingCartType,
                    sci.ProductVariant,
					sci.AddedFromSiteId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.Quantity,
                    false);
                if (sciWarnings.Count > 0)
                    return RedirectToRoute("ShoppingCart", new { _ga });
            }

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage", new { _ga });
            else
                return RedirectToRoute("CheckoutBillingAddress", new { _ga });
        }

        public ActionResult BillingAddress()
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareBillingAddressModel(cart);
            return View(model);
        }

        /// <summary>
        /// Modified to be both Billing + Shipping Address at once
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectBillingAddress(int addressId)
        {
            var address = _workContext.CurrentUser.Addresses.Where(a => a.Id == addressId).FirstOrDefault();
            if (address == null)
                return RedirectToRoute("CheckoutBillingAddress");

            _workContext.CurrentUser.BillingAddress = address;
            _workContext.CurrentUser.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentUser);

            return RedirectToRoute("CheckoutShippingMethod");
        }
        [HttpPost, ActionName("BillingAddress")]
        [FormValueRequired("nextstep")]
        public ActionResult NewBillingAddress(CheckoutBillingAddressModel model)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (ModelState.IsValid)
            {
                var address = model.NewAddress.ToEntity();
                address.CreatedOnUtc = DateTime.UtcNow;
                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _workContext.CurrentUser.Addresses.Add(address);
                _workContext.CurrentUser.BillingAddress = address;
                _workContext.CurrentUser.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentUser);

                return RedirectToRoute("CheckoutShippingMethod");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareBillingAddressModel(cart, model.NewAddress.CountryId);
            return View(model);
        }

        public ActionResult ShippingAddress()
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentUser.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentUser);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            //model
            var model = PrepareShippingAddressModel();
            return View(model);
        }
        public ActionResult SelectShippingAddress(int addressId)
        {
            var address = _workContext.CurrentUser.Addresses.Where(a => a.Id == addressId).FirstOrDefault();
            if (address == null)
                return RedirectToRoute("CheckoutShippingAddress");

            _workContext.CurrentUser.ShippingAddress = address;
            _customerService.UpdateCustomer(_workContext.CurrentUser);

            return RedirectToRoute("CheckoutShippingMethod");
        }
        [HttpPost, ActionName("ShippingAddress")]
        [FormValueRequired("nextstep")]
        public ActionResult NewShippingAddress(CheckoutShippingAddressModel model)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                _workContext.CurrentUser.ShippingAddress = null;
                _customerService.UpdateCustomer(_workContext.CurrentUser);
                return RedirectToRoute("CheckoutShippingMethod");
            }

            if (ModelState.IsValid)
            {
                var address = model.NewAddress.ToEntity();
                address.CreatedOnUtc = DateTime.UtcNow;
                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _workContext.CurrentUser.Addresses.Add(address);
                _workContext.CurrentUser.ShippingAddress = address;
                _customerService.UpdateCustomer(_workContext.CurrentUser);

                return RedirectToRoute("CheckoutShippingMethod");
            }


            //If we got this far, something failed, redisplay form
            model = PrepareShippingAddressModel(model.NewAddress.CountryId);
            return View(model);
        }
        

        public ActionResult ShippingMethod()
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            // Always require shipping for MGR
            //if (!cart.RequiresShipping())
            //{
            //    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentUser, SystemCustomerAttributeNames.LastShippingOption, null);
            //    return RedirectToRoute("CheckoutPaymentMethod");
            //}
            
            
            //model
            var model = PrepareShippingMethodModel(cart);
            return View(model);
        }
        [HttpPost, ActionName("ShippingMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectShippingMethod(string shippingoption)
        {
            try
            {
                //validation
                var cart =
                    _workContext.CurrentUser.ShoppingCartItems.Where(
                        sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    return RedirectToRoute("ShoppingCart");

                if (UseOnePageCheckout())
                    return RedirectToRoute("CheckoutOnePage");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    return new HttpUnauthorizedResult();

                // Always require shipping method for MGR
                //if (!cart.RequiresShipping())
                //{
                //    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentUser,
                //        SystemCustomerAttributeNames.LastShippingOption, null);
                //    return RedirectToRoute("CheckoutPaymentMethod");
                //}

                SaveShippingMethodInternal(shippingoption, cart);

                return RedirectToRoute("CheckoutPaymentMethod");
            }
            catch(Exception e)
            {
                return ShippingMethod();
            }
        }
        
        
        public ActionResult PaymentMethod(bool jumpToSection = false)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
            {
                if (jumpToSection) return RedirectToRoute("CheckoutOnePage", new { jumpToSection = "payment_method" });
                return RedirectToRoute("CheckoutOnePage");
            }

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (!isPaymentWorkflowRequired)
            {
                _workContext.CurrentUser.SelectedPaymentMethodSystemName = "";
                _customerService.UpdateCustomer(_workContext.CurrentUser);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            //model
            var paymentMethodModel = PreparePaymentMethodModel(cart);

            if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
            {
                //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                //so customer doesn't have to choose a payment method
                _workContext.CurrentUser.SelectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                _customerService.UpdateCustomer(_workContext.CurrentUser);
                return RedirectToRoute("CheckoutPaymentInfo");
            }

            return View(paymentMethodModel);
        }
        [HttpPost, ActionName("PaymentMethod")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult SelectPaymentMethod(string paymentmethod, CheckoutPaymentMethodModel model)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //reward points
            _workContext.CurrentUser.UseRewardPointsDuringCheckout = model.UseRewardPoints;
            _customerService.UpdateCustomer(_workContext.CurrentUser);

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                _workContext.CurrentUser.SelectedPaymentMethodSystemName = "";
                _customerService.UpdateCustomer(_workContext.CurrentUser);
                return RedirectToRoute("CheckoutPaymentInfo");
            }
            //payment method 
            if (String.IsNullOrEmpty(paymentmethod))
                return PaymentMethod();

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
            if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                return PaymentMethod();

            //save
            _workContext.CurrentUser.SelectedPaymentMethodSystemName = paymentmethod;
            _customerService.UpdateCustomer(_workContext.CurrentUser);

            return RedirectToRoute("CheckoutPaymentInfo");
        }


        public ActionResult PaymentInfo(bool jumpToSection = false)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
            {
                if (jumpToSection) return RedirectToRoute("CheckoutOnePage", new {jumpToSection = "payment_info"});
                return RedirectToRoute("CheckoutOnePage");
            }

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }
            
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(_workContext.CurrentUser.SelectedPaymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            //model
            var model = PreparePaymentInfoModel(paymentMethod, cart);
            return View(model);
        }
        [HttpPost, ActionName("PaymentInfo")]
        [FormValueRequired("nextstep")]
        [ValidateInput(false)]
        public ActionResult EnterPaymentInfo(FormCollection form)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //Check whether payment workflow is required
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
            if (!isPaymentWorkflowRequired)
            {
                return RedirectToRoute("CheckoutConfirm");
            }

            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(_workContext.CurrentUser.SelectedPaymentMethodSystemName);
            if (paymentMethod == null)
                return RedirectToRoute("CheckoutPaymentMethod");

            var paymentControllerType = paymentMethod.GetControllerType();
            var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BaseNopPaymentController;
            var warnings = paymentController.ValidatePaymentForm(form);
            foreach (var warning in warnings)
                ModelState.AddModelError("", warning);
            if (ModelState.IsValid)
            {
                //get payment info
                var paymentInfo = paymentController.GetPaymentInfo(form);
                //session save
                CurrentPaymentInfo = paymentInfo;
                return RedirectToRoute("CheckoutConfirm");
            }

            //If we got this far, something failed, redisplay form
            //model
            var model = PreparePaymentInfoModel(paymentMethod, cart);
            return View(model);
        }
        

        public ActionResult Confirm(bool jumpToSection = false)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
            {
                if (jumpToSection) return RedirectToRoute("CheckoutOnePage", new {jumpToSection = "confirm_order"});
                return RedirectToRoute("CheckoutOnePage");
            }

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //model
            var model = PrepareConfirmOrderModel(cart);
            return View(model);
        }
        [HttpPost, ActionName("Confirm")]
        [ValidateInput(false)]
        public ActionResult ConfirmOrder()
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (UseOnePageCheckout())
                return RedirectToRoute("CheckoutOnePage");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();


            //model
            var model = new CheckoutConfirmModel();
            try
            {
                var processPaymentRequest = CurrentPaymentInfo;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                        return RedirectToRoute("CheckoutPaymentInfo");
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }
                
                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentUser))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                if (_orderService.IsCurrentCustomerBlocked())
                    return Redirect(string.Format("{0}checkout/CheckoutBlocked", _webHelper.GetStoreLocation()));

                //place order
                processPaymentRequest.CustomerId = _workContext.CurrentUser.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentUser.SelectedPaymentMethodSystemName;
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _orderService.RecordOrderSuccess();
                    CurrentPaymentInfo = null;

	                foreach (var order in placeOrderResult.PlacedOrders) {
		                _paymentService.PostProcessPayment(new PostProcessPaymentRequest { Order = order });
	                }

	                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        return Content("Redirected");
                    }
                    else
                    {
                        //if no redirection has been done (to a third-party payment page)
                        //theoretically it's not possible
		                var ids = string.Join(",", placeOrderResult.PlacedOrders.Select(o => o.Id.ToString()));
		                return RedirectToAction("Details", "Order", new { orderIds = ids, submitted = true });
		                //return RedirectToRoute("CheckoutCompleted", new { OrderId = ids });
                    }
                }
                else
                {
                    foreach (var error in placeOrderResult.Errors)
                        model.Warnings.Add(error);
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }


        public ActionResult Completed()
        {
            //validation
            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            //model
            var model = new CheckoutCompletedModel();

            var orders = _orderService.GetOrdersByCustomerId(_workContext.CurrentUser.Id);
            if (orders.Count == 0)
                return RedirectToRoute("HomePage");
            else
            {
                var lastOrder = orders[0];
                model.OrderId = lastOrder.Id;
            }

            return View(model);
        }
        
        [ChildActionOnly]
        public ActionResult CheckoutProgress(CheckoutProgressStep step)
        {
            var model = new CheckoutProgressModel() {CheckoutProgressStep = step};
            return PartialView(model);
        }
        #endregion


        public ActionResult CheckoutBlocked()
        {
            string phoneNumber = null;
            if (_workContext.SiteId != KnownIds.Website.Corporate) {
                var location = _portalQueries.GetLocationFromWebsiteId(_workContext.SiteId);
                if (location != null && !string.IsNullOrEmpty(location.PhoneNumber))
                    phoneNumber = location.PhoneNumber;
            }

            return View((object)phoneNumber);
        }

        #region Methods (one page checkout)

        public ActionResult OnePageCheckout(string jumpToSection = null)
        {
            //validation
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (_orderService.IsCurrentCustomerBlocked())
                return Redirect(string.Format("{0}checkout/CheckoutBlocked", _webHelper.GetStoreLocation()));

            if (!UseOnePageCheckout())
                return RedirectToRoute("Checkout");

            if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            object jsonCommand = GetJsonCommand(cart, jumpToSection);

            var model = new OnePageCheckoutModel()
            {
                ShippingRequired = true, // always require shipping steps for mgr //cart.RequiresShipping()
                JsonCommand = jsonCommand
            };
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult OpcBillingForm()
        {
			var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
			var billingAddressModel = PrepareBillingAddressModel(cart);
            return PartialView("OpcBillingAddress", billingAddressModel);
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveBilling(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentUser.Addresses.Where(a => a.Id == billingAddressId).FirstOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentUser.BillingAddress = address;
	                _workContext.CurrentUser.ShippingAddress = address; // Treefort enhancement - default to shipping addr = billing addr
                    _customerService.UpdateCustomer(_workContext.CurrentUser);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);

                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(cart, model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("OpcBillingAddress", billingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentUser.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);

                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
						address.Country = _countryService.GetCountryById(address.CountryId ?? 1);
						address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId ?? 0);
						address.CreatedOnUtc = DateTime.UtcNow;
                        _workContext.CurrentUser.Addresses.Add(address);
                    }

                    _workContext.CurrentUser.BillingAddress = address;
	                _workContext.CurrentUser.ShippingAddress = address; // Treefort enhancement - default to shipping addr = billing addr
					_customerService.UpdateCustomer(_workContext.CurrentUser);
                }

                if (true) { // always require shipping steps for mgr //cart.RequiresShipping()) {
					// Treefort enhancement - Winmark wants to enforce that billing addr = shipping addr, so skip the shipping addr step
					var shippingMethodModel = PrepareShippingMethodModel(cart);
					return Json(new {
						update_section = new UpdateSectionJsonModel() {
							name = "shipping-method",
							html = this.RenderPartialViewToString("OpcShippingMethods", shippingMethodModel)
						},
						goto_section = "shipping_method"
					});

                    //shipping is required
					//var shippingAddressModel = PrepareShippingAddressModel();
					//return Json(new
					//{
					//	update_section = new UpdateSectionJsonModel()
					//	{
					//		name = "shipping",
					//		html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
					//	},
					//	goto_section = "shipping"
					//});
                }
                else
                {
                    //shipping is not required
                    _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentUser, SystemCustomerAttributeNames.LastShippingOption, null);


                    //Check whether payment workflow is required
                    //we ignore reward points during cart total calculation
                    bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
                    if (isPaymentWorkflowRequired)
                    {
                        //payment is required
                        var paymentMethodModel = PreparePaymentMethodModel(cart);

                        if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                            paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                        {
                            //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                            //so customer doesn't have to choose a payment method
                            _workContext.CurrentUser.SelectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                            _customerService.UpdateCustomer(_workContext.CurrentUser);

                            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(_workContext.CurrentUser.SelectedPaymentMethodSystemName);
                            if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                                throw new Exception("Selected payment method can't be parsed");


                            var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst, cart);
                            return Json(new
                            {
                                update_section = new UpdateSectionJsonModel()
                                {
                                    name = "payment-info",
                                    html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                                },
                                goto_section = "payment_info"
                            });
                        }
                        else
                        {
                            //customer have to choose a payment method
                            return Json(new
                            {
                                update_section = new UpdateSectionJsonModel()
                                {
                                    name = "payment-method",
                                    html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                                },
                                goto_section = "payment_method"
                            });
                        }
                    }
                    else
                    {
                        //payment is not required
                        _workContext.CurrentUser.SelectedPaymentMethodSystemName = "";
                        _customerService.UpdateCustomer(_workContext.CurrentUser);

                        var confirmOrderModel = PrepareConfirmOrderModel(cart);
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "confirm-order",
                                html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                            },
                            goto_section = "confirm_order"
                        });
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShipping(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                int shippingAddressId = 0;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentUser.Addresses.Where(a => a.Id == shippingAddressId).FirstOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentUser.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentUser);
                }
                else
                {
                    //new address
                    var model = new CheckoutShippingAddressModel();
                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                        shippingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentUser.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentUser.Addresses.Add(address);
                    }
                    _workContext.CurrentUser.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentUser);
                }

                var shippingMethodModel = PrepareShippingMethodModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "shipping-method",
                        html = this.RenderPartialViewToString("OpcShippingMethods", shippingMethodModel)
                    },
                    goto_section = "shipping_method"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShippingMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");
                
				// always require shipping steps for mgr
				//if (!cart.RequiresShipping())
				//	throw new Exception("Shipping is not required");

                //parse selected method 
                string shippingoption = form["shippingoption"];
                SaveShippingMethodInternal(shippingoption, cart);

                //Check whether payment workflow is required
                //we ignore reward points during cart total calculation
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
                if (isPaymentWorkflowRequired)
                {
                    //payment is required
                    var paymentMethodModel = PreparePaymentMethodModel(cart);

                    if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                        paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                    {
                        //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                        //so customer doesn't have to choose a payment method
                        _workContext.CurrentUser.SelectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                        _customerService.UpdateCustomer(_workContext.CurrentUser);

                        var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(_workContext.CurrentUser.SelectedPaymentMethodSystemName);
                        if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                            throw new Exception("Selected payment method can't be parsed");


                        var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst, cart);
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "payment-info",
                                html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                            },
                            goto_section = "payment_info"
                        });
                    }
                    else
                    {
                        //customer have to choose a payment method
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "payment-method",
                                html = this.RenderPartialViewToString("OpcPaymentMethods", paymentMethodModel)
                            },
                            goto_section = "payment_method"
                        });
                    }
                }
                else
                {
                    //payment is not required
                    _workContext.CurrentUser.SelectedPaymentMethodSystemName = "";
                    _customerService.UpdateCustomer(_workContext.CurrentUser);

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                string paymentmethod = form["paymentmethod"];
                //payment method 
                if (String.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");


                var model = new CheckoutPaymentMethodModel();
                TryUpdateModel(model);

                //reward points
                _workContext.CurrentUser.UseRewardPointsDuringCheckout = model.UseRewardPoints;
                _customerService.UpdateCustomer(_workContext.CurrentUser);

                //Check whether payment workflow is required
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
                if (!isPaymentWorkflowRequired)
                {
                    //payment is not required
                    _workContext.CurrentUser.SelectedPaymentMethodSystemName = "";
                    _customerService.UpdateCustomer(_workContext.CurrentUser);

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
                if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _workContext.CurrentUser.SelectedPaymentMethodSystemName = paymentmethod;
                _customerService.UpdateCustomer(_workContext.CurrentUser);

				if (paymentMethodInst.IsPayPal()) {
					return OpcSavePaymentInfo(new FormCollection());
				}

                var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst, cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    },
                    goto_section = "payment_info"
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentInfo(FormCollection form)
        {
            try
            {
                //validation
                var cart =
                    _workContext.CurrentUser.ShoppingCartItems.Where(
                        sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(
                        _workContext.CurrentUser.SelectedPaymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController =
                    DependencyResolver.Current.GetService(paymentControllerType) as BaseNopPaymentController;

				foreach (var warning in paymentController.ValidatePaymentForm(form))
                    ModelState.AddModelError("", warning);

				if (ModelState.IsValid)
                {
                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    //session save
                    CurrentPaymentInfo = paymentInfo;

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = PreparePaymentInfoModel(paymentMethod, cart);
                // Get paypal token, transaction id, etc. back to pass on
                // Prevent calling multiple authorization requests
                if (paymentMethod.IsPayPalExpress())
                {
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    paymenInfoModel.PaymentInfoRouteValues.Add("token", paymentInfo.PaypalToken);
                    paymenInfoModel.PaymentInfoRouteValues.Add("transactionId", paymentInfo.PayPalTransactionId);
                    paymenInfoModel.PaymentInfoRouteValues.Add("merchantId", paymentInfo.PayPalMerchantId);
                    paymenInfoModel.PaymentInfoRouteValues.Add("payerId", paymentInfo.PaypalPayerId);
                }

                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("OpcPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcConfirmOrder()
        {
            try
            {
                //validation
                var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentUser))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                if (_orderService.IsCurrentCustomerBlocked())
                    return Redirect(string.Format("{0}checkout/CheckoutBlocked", _webHelper.GetStoreLocation()));

                //place order
                var processPaymentRequest = CurrentPaymentInfo;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered");
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }

                processPaymentRequest.CustomerId = _workContext.CurrentUser.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentUser.SelectedPaymentMethodSystemName;
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
	            if (placeOrderResult.Success) {
                    _orderService.RecordOrderSuccess();
		            CurrentPaymentInfo = null;

		            foreach (var order in placeOrderResult.PlacedOrders) {
			            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
			            if (paymentMethod != null) { //payment method could be null if order total is 0
							var postProcessPaymentRequest = new PostProcessPaymentRequest() { Order = order };
				            if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection) {
					            //Redirection will not work because it's AJAX request.
					            //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

				                var redirectUrl = string.IsNullOrWhiteSpace(order.SecureAcceptanceUrl)
				                    ? string.Format("{0}checkout/OpcCompleteRedirectionPayment", _webHelper.GetStoreLocation())
				                    : order.SecureAcceptanceUrl;

					            //redirect
					            return Json(new {
						            redirect = redirectUrl
					            });
				            }
				            else {
					            _paymentService.PostProcessPayment(postProcessPaymentRequest);
				            }
			            }
		            }
		            return Json(new {
			            success = 1,
						redirect = Url.Action("Details", "Order", new { orderIds = string.Join(",", placeOrderResult.PlacedOrders.Select(o => o.Id)), submitted = true })
		            });
	            }
	            else {
		            //error
	                var sb = new StringBuilder();
		            var confirmOrderModel = new CheckoutConfirmModel();
	                foreach (var error in placeOrderResult.Errors) {
	                    if (sb.Length > 0)
	                        sb.AppendLine();
	                    sb.Append(error);
	                    confirmOrderModel.Warnings.Add(error);
	                }
                    _orderService.RecordOrderFailure(sb.ToString());

	                if (_orderService.IsCurrentCustomerBlocked())
                        return Json(new {
                            redirect = string.Format("{0}checkout/CheckoutBlocked", _webHelper.GetStoreLocation())
                        });
                    else
		                return Json(new {
			                update_section = new UpdateSectionJsonModel() {
				                name = "confirm-order",
				                html = this.RenderPartialViewToString("OpcConfirmOrder", confirmOrderModel)
			                },
			                goto_section = "confirm_order"
		                });
	            }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public ActionResult OpcCompleteRedirectionPayment()
        {
            try
            {
                //validation
                if (!UseOnePageCheckout())
                    return RedirectToRoute("HomePage");

                if ((_workContext.CurrentUser.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    return new HttpUnauthorizedResult();

                //get the order
                var orders = _orderService.GetOrdersByCustomerId(_workContext.CurrentUser.Id);
                if (orders.Count == 0)
                    return RedirectToRoute("HomePage");

                
                var order = orders[0];
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("HomePage");
                if (paymentMethod.PaymentMethodType != PaymentMethodType.Redirection)
                    return RedirectToRoute("HomePage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("HomePage");


                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest()
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }
                else
                {
                    //if no redirection has been done (to a third-party payment page)
                    //theoretically it's not possible
                    return RedirectToRoute("CheckoutCompleted");
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentUser);
                return Content(exc.Message);
            }
        }

        #endregion
    }
}
