﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.UI.Captcha;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Common;
using Nop.Web.Models.Media;
using Nop.Services.Logging;
using NPoco.Expressions;
using SimpleHoneypot.ActionFilters;
using SimpleHoneypot.Extensions;
using Treefort.Common;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee.Website;
using Treefort.Portal.Queries.Locations;
using Treefort.Portal.Services;
using WinmarkFranchise.Domain.Repository;
using ObjectExtensions = Nop.Core.ObjectExtensions;


namespace Nop.Web.Controllers
{
    [DisableForCoop]
    public partial class CatalogController : BaseNopController
    {
        #region Fields

        private const string ADVANCED_SEARCH_MODEL_KEY = "Nop.advanced-search.model";
        private const string STORE_TODAY_HOURS_KEY = "Nop.product-details.store.today-hours-{0}";

        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IProductService _productService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly ICategoryTemplateService _categoryTemplateService;
        private readonly IManufacturerTemplateService _manufacturerTemplateService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IWorkContext _workContext;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IWebHelper _webHelper;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly ICustomerContentService _customerContentService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IRecentlyViewedProductsService _recentlyViewedProductsService;
        private readonly ICompareProductsService _compareProductsService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IProductTagService _productTagService;
        private readonly IOrderReportService _orderReportService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly IPermissionService _permissionService;
        private readonly IDownloadService _downloadService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly Treefort.Portal.Services.ILocationService _locationService;
        private readonly IWorkContext _context;
        private readonly IOrderService _orderService;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        private readonly IHoursViewModelQuery _hoursViewModelQuery;

        private readonly MediaSettings _mediaSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly IOutOfProcessCacheManager _cacheManager;
        private readonly CaptchaSettings _captchaSettings;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;

        #endregion

        #region Constructors

        public CatalogController(ICategoryService categoryService,
            IManufacturerService manufacturerService, IProductService productService,
            IProductTemplateService productTemplateService,
            ICategoryTemplateService categoryTemplateService,
            IManufacturerTemplateService manufacturerTemplateService,
            IProductAttributeService productAttributeService, IProductAttributeParser productAttributeParser,
            IWorkContext workContext, ITaxService taxService, ICurrencyService currencyService,
            IPictureService pictureService, ILocalizationService localizationService,
            IPriceCalculationService priceCalculationService, IPriceFormatter priceFormatter,
            IWebHelper webHelper, ISpecificationAttributeService specificationAttributeService,
            ICustomerContentService customerContentService, IDateTimeHelper dateTimeHelper,
            IShoppingCartService shoppingCartService,
            IRecentlyViewedProductsService recentlyViewedProductsService, ICompareProductsService compareProductsService,
            IWorkflowMessageService workflowMessageService, IProductTagService productTagService,
            IOrderReportService orderReportService, IGenericAttributeService genericAttributeService,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            IPermissionService permissionService, IDownloadService downloadService,
            ICustomerActivityService customerActivityService,
            MediaSettings mediaSettings, CatalogSettings catalogSettings,
            ShoppingCartSettings shoppingCartSettings, StoreInformationSettings storeInformationSettings,
            LocalizationSettings localizationSettings, CustomerSettings customerSettings,
            CaptchaSettings captchaSettings,
            IOutOfProcessCacheManager cacheManager,
            Treefort.Portal.Services.ILocationService locationService,
            IWorkContext context, IOrderTotalCalculationService orderTotalCalculationService,
            IOrderService orderService, IMobileDeviceHelper mobileDeviceHelper, IHoursViewModelQuery hoursViewModelQuery)
        {
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._productService = productService;
            this._productTemplateService = productTemplateService;
            this._categoryTemplateService = categoryTemplateService;
            this._manufacturerTemplateService = manufacturerTemplateService;
            this._productAttributeService = productAttributeService;
            this._productAttributeParser = productAttributeParser;
            this._workContext = workContext;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._webHelper = webHelper;
            this._specificationAttributeService = specificationAttributeService;
            this._customerContentService = customerContentService;
            this._dateTimeHelper = dateTimeHelper;
            this._shoppingCartService = shoppingCartService;
            this._recentlyViewedProductsService = recentlyViewedProductsService;
            this._compareProductsService = compareProductsService;
            this._workflowMessageService = workflowMessageService;
            this._productTagService = productTagService;
            this._orderReportService = orderReportService;
            this._genericAttributeService = genericAttributeService;
            this._backInStockSubscriptionService = backInStockSubscriptionService;
            this._permissionService = permissionService;
            this._downloadService = downloadService;
            this._customerActivityService = customerActivityService;
            this._hoursViewModelQuery = hoursViewModelQuery;
            _locationService = locationService;
            _context = context;
            _orderTotalCalculationService = orderTotalCalculationService;
            _orderService = orderService;
            _mobileDeviceHelper = mobileDeviceHelper;


            this._mediaSettings = mediaSettings;
            this._catalogSettings = catalogSettings;
            this._shoppingCartSettings = shoppingCartSettings;
            this._storeInformationSettings = storeInformationSettings;
            this._localizationSettings = localizationSettings;
            this._customerSettings = customerSettings;
            this._captchaSettings = captchaSettings;

            this._cacheManager = cacheManager;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected List<int> GetChildCategoryIds(int parentCategoryId, bool showHidden = false)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_CHILD_IDENTIFIERS_MODEL_KEY, parentCategoryId, showHidden);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var categoriesIds = new List<int>();
                var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, showHidden);
                foreach (var category in categories)
                {
                    categoriesIds.Add(category.Id);
                    categoriesIds.AddRange(GetChildCategoryIds(category.Id, showHidden));
                }
                return categoriesIds;
            });
            return cacheModel;
        }

        [NonAction]
        protected IList<Category> GetCategoryBreadCrumb(Category category)
        {
            if (category == null)
                throw new ArgumentNullException("category");

            var breadCrumb = new List<Category>();

            while (category != null && //category is not null
                !category.Deleted && //category is not deleted
                category.Published) //category is published
            {
                breadCrumb.Add(category);
                category = _categoryService.GetCategoryById(category.ParentCategoryId);
            }
            breadCrumb.Reverse();
            return breadCrumb;
        }

        protected IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(
            IEnumerable<int> productIds,
            int? productThumbPictureSize = null,
            bool forceRedirectionAfterAddingToCart = false, bool AllSearch = false)
        {
            var productList = productIds.Select(id => _cacheManager.Get(ModelCacheEventConsumer.PRODUCT_OVERVIEW_MODEL_KEY + id, () =>
            {
                var p = _productService.GetProductById(id);
                return BuildProductOverviewModel(p, productThumbPictureSize, forceRedirectionAfterAddingToCart);
            }));

            return productList;
        }

        [NonAction]
        protected IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(
            IEnumerable<Product> products,
            int? productThumbPictureSize = null,
            bool forceRedirectionAfterAddingToCart = false, bool AllSearch = false)
        {
            var productList = products.Select(p => _cacheManager.Get(ModelCacheEventConsumer.PRODUCT_OVERVIEW_MODEL_KEY + p.Id, () =>
            {
                return BuildProductOverviewModel(p, productThumbPictureSize, forceRedirectionAfterAddingToCart);
            }));

            return productList;
        }

        private void GetPricing(ProductVariant variant, Action<string> setPrice, Action<string> setOldPrice, Action<string> setPriceWithDiscount,
                                Action<decimal> setPriceValue, Action<decimal> setPriceWithDiscountValue, Action<bool> setHasSpecialPrice,
                                Action<string> setShipping, Action<decimal> setShippingValue)
        {
            decimal finalPriceWithoutDiscountBase = _priceCalculationService.GetFinalPrice(variant, false);
            decimal finalPriceWithDiscountBase = _priceCalculationService.GetFinalPrice(variant, true);
            decimal shipping = GetShippingPrice(variant);

            decimal oldPriceBase = variant.IsSpecialPriceInEffect ? variant.Price : variant.OldPrice;

            decimal oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(oldPriceBase, _workContext.WorkingCurrency);
            decimal finalPriceWithoutDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceWithoutDiscountBase, _workContext.WorkingCurrency);
            decimal finalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceWithDiscountBase, _workContext.WorkingCurrency);

            if (finalPriceWithoutDiscountBase != oldPriceBase && oldPriceBase > decimal.Zero)
                (setOldPrice ?? (x => { }))(_priceFormatter.FormatPrice(oldPrice));

            (setPrice ?? (x => { }))(_priceFormatter.FormatPrice(finalPriceWithoutDiscount));

            if (finalPriceWithoutDiscountBase != finalPriceWithDiscountBase)
                (setPriceWithDiscount ?? (x => { }))(_priceFormatter.FormatPrice(finalPriceWithDiscount));

            (setPriceValue ?? (x => { }))(finalPriceWithoutDiscount);
            (setPriceWithDiscountValue ?? (x => { }))(finalPriceWithDiscount);
            (setHasSpecialPrice ?? (x => { }))(variant.IsSpecialPriceInEffect);
            (setShipping ?? (x => { }))(!variant.IsShipEnabled ? "N/A" : (variant.IsFreeShipping || shipping == 0m ? "Free Shipping" : _priceFormatter.FormatPrice(shipping)));
            (setShippingValue ?? (x => { }))(shipping);
        }

        public decimal GetShippingPrice(ProductVariant variant)
        {
            if (variant.Product.SiteId != KnownIds.Website.Corporate)
                return _priceCalculationService.GetMgrUsedProductShippingPrice(variant);

            // it's an accessory, inspect cart to see if we get free shipping.
            // 1. if there are any used products in the cart, accessories ship free.
            // 2. if there's more than $25 worth of accessories (including this one), they ship free.
            // 3. if there's only accessories totaling < $25, it's a flat $8 for all of them.
            //    BUT...if there are accessories in the cart already then the $8 is already reflected
            //    in the cart total and we don't need to charge more for THIS accessory.
            // Hence, free shipping on THIS product if there's ANYTHING in the cart.
            var user = _workContext.CurrentUser;
            if (user != null && user.ShoppingCartItems != null && user.ShoppingCartItems.Any())
                return 0m;

            // one lonely accessory ships for 8 bucks if it's worth under $25, otherwise free
            var itemPrice = _priceCalculationService.GetFinalPrice(variant, false);
            return itemPrice > 25m ? 0m : 8m;
        }

        private ProductOverviewModel BuildProductOverviewModel(Product product, int? productThumbPictureSize, bool forceRedirectionAfterAddingToCart)
        {
            var pom = new ProductOverviewModel
            {
                Id = product.Id,
                Name = product.Name,
                SeName = SeoExtensions.GetSeName(product.Name, false, true, false),
                CreatedOnUtc = product.CreatedOnUtc,
                Used = product.Name.StartsWith("Used", StringComparison.OrdinalIgnoreCase)
            };
            pom.Url = Url.Action("Product", "Catalog", new {productId = pom.Id, SeName = pom.SeName});
            var variant = product.ProductVariants.FirstOrDefault();
            if (variant != null)
            {
                GetPricing(variant, x => pom.ProductPrice.Price = x, x => pom.ProductPrice.OldPrice = x, null, null, null, x => pom.ProductPrice.SpecialPrice = x, null, null);
                pom.ProductPrice.ForceRedirectionAfterAddingToCart = forceRedirectionAfterAddingToCart;
                pom.ProductPrice.Clearance = variant.Clearance || (0 < variant.OldPrice && variant.OldPrice > variant.Price);
            }

            var picture = product.GetDefaultProductPicture(_pictureService);

            if (picture != null)
            {
                pom.DefaultPictureModel = new PictureModel
                {

                    ImageUrl = _pictureService.GetPictureUrl(picture.Id, "?width=204&height=204&mode=crop", true, _webHelper.IsCurrentConnectionSecured()),
                    LargeBoxUrl = _pictureService.GetPictureUrl(picture.Id, "?width=244&height=244&mode=crop", true, _webHelper.IsCurrentConnectionSecured()),
                    MidImageUrl = _pictureService.GetPictureUrl(picture.Id, "?width=1080&height=1080&mode=max", true, _webHelper.IsCurrentConnectionSecured()),
                    MobileImageUrl = _pictureService.GetPictureUrl(picture.Id, "?width=768&height=768&mode=crop&scale=both", true, _webHelper.IsCurrentConnectionSecured()),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture.Id, "", true, _webHelper.IsCurrentConnectionSecured()),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), product.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), product.Name)
                };
            }

            if (product.SiteId != KnownIds.Website.Corporate)
            {
                pom.ProductLocation = _locationService.GetLocationContactInfoBySiteId(product.SiteId);
            }


            return pom;
        }

        [NonAction]
        protected IList<ProductSpecificationModel> PrepareProductSpecificationModel(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            string cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_SPECS_MODEL_KEY, product.Id, _workContext.WorkingLanguage.Id);
            return _cacheManager.Get(cacheKey, () =>
            {
                var model = _specificationAttributeService.GetProductSpecificationAttributesByProductId(product.Id, null, true)
                   .Select(psa =>
                   {
                       return new ProductSpecificationModel()
                       {
                           SpecificationAttributeId = psa.SpecificationAttributeOption.SpecificationAttributeId,
                           SpecificationAttributeName = psa.SpecificationAttributeOption.SpecificationAttribute.GetLocalized(x => x.Name),
                           SpecificationAttributeOption = psa.SpecificationAttributeOption.GetLocalized(x => x.Name)
                       };
                   }).ToList();
                return model;
            });
        }

        [NonAction]
        protected IList<CategoryNavigationModel> GetChildCategoryNavigationModel(IList<Category> breadCrumb, int rootCategoryId, Category currentCategory, int level)
        {
            var result = new List<CategoryNavigationModel>();
            foreach (var category in _categoryService.GetAllCategoriesByParentCategoryId(rootCategoryId))
            {
                var model = new CategoryNavigationModel()
                {
                    Id = category.Id,
                    Name = category.GetLocalized(x => x.Name),
                    SeName = category.GetSeName(),
                    IsActive = currentCategory != null && currentCategory.Id == category.Id,
                    NumberOfParentCategories = level
                };

                if (_catalogSettings.ShowCategoryProductNumber)
                {
                    model.DisplayNumberOfProducts = true;
                    var categoryIds = new List<int>();
                    categoryIds.Add(category.Id);
                    if (_catalogSettings.ShowCategoryProductNumberIncludingSubcategories)
                    {
                        //include subcategories
                        categoryIds.AddRange(GetChildCategoryIds(category.Id));
                    }
                    model.NumberOfProducts = _productService.SearchProducts(new ProductSearchOptions
                    {
                        CategoryIds = categoryIds,
                        PageSize = 1
                    }).TotalCount;
                }
                result.Add(model);

                for (int i = 0; i <= breadCrumb.Count - 1; i++)
                    if (breadCrumb[i].Id == category.Id)
                        result.AddRange(GetChildCategoryNavigationModel(breadCrumb, category.Id, currentCategory, level + 1));
            }

            return result;
        }

        [NonAction]
        protected ProductDetailsModel PrepareProductDetailsPageModel(Product product, bool isPreview = false)
        {
            if (product == null)
                throw new ArgumentNullException("product");


            var model = new ProductDetailsModel
            {
                Id = product.Id,
                Name = product.GetLocalized(x => x.Name),
                CategoryName = product.ProductCategories.FirstOrDefault()?.Category?.Name,
                ShortDescription = product.GetLocalized(x => x.ShortDescription),
                FullDescription = product.GetLocalized(x => x.FullDescription),
                MetaKeywords = product.GetLocalized(x => x.MetaKeywords),
                MetaDescription = product.GetLocalized(x => x.MetaDescription),
                MetaTitle = product.GetLocalized(x => x.MetaTitle),
                SeName = product.GetSeName(),
                IsCorpProduct = product.SiteId == Treefort.Portal.KnownIds.Website.Corporate,
                IsClearance = product.ProductVariants.Any() && product.ProductVariants.First().Clearance,
                IsSpecialPrice = product.ProductVariants.Any() && product.ProductVariants.First().IsSpecialPriceInEffect,
                Deleted = product.Deleted,
                IsCorporateSite = _context.SiteId == KnownIds.Website.Corporate
            };

            //template


            var templateCacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_TEMPLATE_MODEL_KEY, product.ProductTemplateId);
            model.ProductTemplateViewPath = _cacheManager.Get(templateCacheKey, () =>
            {
                var template = _productTemplateService.GetProductTemplateById(product.ProductTemplateId);
                if (template == null)
                    template = _productTemplateService.GetAllProductTemplates().FirstOrDefault();
                return template.ViewPath;
            });

            //pictures
            model.DefaultPictureZoomEnabled = _mediaSettings.DefaultPictureZoomEnabled;
            var pictures = _pictureService.GetPicturesByProductId(product.Id);
            if (pictures.Count > 0)
            {
                //default picture
                model.DefaultPictureModel = new PictureModel()
                {
                    ImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault(), "?w=535&h=535&mode=max"),
                    MidImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault(), "?w=535&h=535&mode=crop"),
                    MobileImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault(), "?width=768&height=768&mode=crop&scale=both"),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(pictures.FirstOrDefault(), "?w=1080&h=1080&mode=crop"),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name),
                };
                //all pictures
                foreach (var picture in pictures)
                {
                    model.PictureModels.Add(new PictureModel()
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, "?w=102&h=102&mode=crop"),
                        MidImageUrl = _pictureService.GetPictureUrl(picture, "?w=535&h=535&mode=crop"),
                        MobileImageUrl = _pictureService.GetPictureUrl(picture, "?width=768&height=768&mode=crop&scale=both"),
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture, "?w=1080&h=1080&mode=crop"),
                        Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name),
                    });
                }
            }
            else
            {
                //no images. set the default one
                model.DefaultPictureModel = new PictureModel()
                {
                    ImageUrl = _pictureService.GetDefaultPictureUrl("?w=535&h=535&mode=max"),
                    MidImageUrl = _pictureService.GetDefaultPictureUrl("?w=1080&h=1080&mode=crop"),
                    MobileImageUrl = _pictureService.GetDefaultPictureUrl("?width=768&height=768&mode=crop&scale=both"),
                    FullSizeImageUrl = _pictureService.GetDefaultPictureUrl(),
                    Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name),
                    AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name),
                };
            }


            var productSiteId = product.SiteId;
            //If we are on a franchisee site and we get to a "corporate" product then show the address for that franchisee
            if (product.SiteId == Treefort.Portal.KnownIds.Website.Corporate &&
                _context.SiteId != Treefort.Portal.KnownIds.Website.Corporate)
            {
                productSiteId = _context.SiteId;
            }
            model.ProductLocation = _locationService.GetLocationContactInfoBySiteId(productSiteId);
            if (productSiteId != KnownIds.Website.Corporate)
            {
                var storeHoursKey = string.Format(STORE_TODAY_HOURS_KEY, model.ProductLocation.Id);
                var hoursTodayCached = _cacheManager.Get(storeHoursKey, 30, () => _hoursViewModelQuery.GetTodayHours(model.ProductLocation.Id));
                model.ProductLocation.HoursToday = hoursTodayCached;
            }

            var categoryId = (from pc in product.ProductCategories select pc.CategoryId).FirstOrDefault();

            //product variants
            var utcNow = DateTime.UtcNow;
            var recentVariants = _productService.GetProductVariantsByProductId(product.Id, isPreview);
            foreach (var variant in recentVariants)
            {
                if (variant.StockQuantity == 0 && (variant.Product.Name.Contains("used", StringComparison.OrdinalIgnoreCase) || !string.IsNullOrWhiteSpace(variant.Product.StoreNumber))) continue;
                var pv = PrepareProductVariantModel(new ProductDetailsModel.ProductVariantModel(), variant, categoryId);
                pv.AddToCart.ProductLocation = model.ProductLocation;

                model.ProductVariantModels.Add(pv);
            }

            model.HasRecentlyAvailableVariant = recentVariants.Any(pv =>
            {
                if ((!product.Deleted && pv.StockQuantity > 0) || !pv.Product.Name.Contains("used", StringComparison.OrdinalIgnoreCase) || string.IsNullOrWhiteSpace(pv.Product.StoreNumber)) return true;

                var lastOrder = GetLastOrderByProductVariantId(pv.Id);
                var lastAvailableDate = lastOrder != null
                    ? lastOrder.CreatedOnUtc
                    : pv.AvailableStartDateTimeUtc ?? (pv.PurchaseDate ?? pv.CreatedOnUtc);

                return (lastAvailableDate >= utcNow.AddYears(-1));
            });

            if (model.ProductLocation != null)
            {
                model.ProductLocation.ProductSKU = model.ProductVariantModels.Any() ? model.ProductVariantModels.First().Sku : "";
                model.ProductLocation.ProductDescription = model.Name;
                model.ProductLocation.ProductId = model.Id;
            }

            return model;
        }

        [NonAction]
        protected Order GetLastOrderByProductVariantId(int productVariantId)
        {
            var orders =
                _orderService.GetOrdersByProductVariantId(productVariantId).OrderByDescending(o => o.CreatedOnUtc);

            return orders.Any() ? orders.First() : null;
        }

        [NonAction]
        protected void PrepareProductReviewsModel(ProductReviewsModel model, Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            if (model == null)
                throw new ArgumentNullException("model");

            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();

            var productReviews = product.ProductReviews.Where(pr => pr.IsApproved).OrderBy(pr => pr.CreatedOnUtc);
            foreach (var pr in productReviews)
            {
                model.Items.Add(new ProductReviewModel()
                {
                    Id = pr.Id,
                    CustomerId = pr.CustomerId,
                    CustomerName = pr.Customer.FormatUserName(),
                    AllowViewingProfiles = _customerSettings.AllowViewingProfiles && pr.Customer != null && !pr.Customer.IsGuest(),
                    Title = pr.Title,
                    ReviewText = pr.ReviewText,
                    Rating = pr.Rating,
                    Helpfulness = new ProductReviewHelpfulnessModel()
                    {
                        ProductReviewId = pr.Id,
                        HelpfulYesTotal = pr.HelpfulYesTotal,
                        HelpfulNoTotal = pr.HelpfulNoTotal,
                    },
                    WrittenOnStr = _dateTimeHelper.ConvertToUserTime(pr.CreatedOnUtc, DateTimeKind.Utc).ToString("g"),
                });
            }

            model.AddProductReview.CanCurrentCustomerLeaveReview = _catalogSettings.AllowAnonymousUsersToReviewProduct || !_workContext.CurrentUser.IsGuest();
            model.AddProductReview.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnProductReviewPage;
        }

        [NonAction]
        protected ProductDetailsModel.ProductVariantModel PrepareProductVariantModel(ProductDetailsModel.ProductVariantModel model, ProductVariant productVariant, int categoryId)
        {
            if (productVariant == null)
                throw new ArgumentNullException("productVariant");

            if (model == null)
                throw new ArgumentNullException("model");

            #region Properties

            model.Id = productVariant.Id;
            model.Name = productVariant.GetLocalized(x => x.Name);
            model.ProductName = productVariant.Product.Name;
            model.Condition = productVariant.Product.Name.Contains("Used") ? "UsedCondition" : "NewCondition";
            model.ShowSku = _catalogSettings.ShowProductSku;
            model.Sku = productVariant.Sku;
            model.Description = productVariant.GetLocalized(x => x.Description);
            model.ShowManufacturerPartNumber = _catalogSettings.ShowManufacturerPartNumber;
            model.ManufacturerPartNumber = productVariant.ManufacturerPartNumber;
            model.ShowGtin = _catalogSettings.ShowGtin;
            model.Gtin = productVariant.Gtin;
            model.HasStoreId = !string.IsNullOrEmpty(productVariant.Product.StoreNumber);
            model.StockAvailablity = productVariant.FormatStockMessage(_localizationService);
            model.StockAvailabilityGsFormat = string.IsNullOrEmpty(model.StockAvailablity) || model.StockAvailablity.Trim() == "In Stock" ? "InStock" : "OutOfStock";
            model.PictureModel.FullSizeImageUrl = _pictureService.GetPictureUrl(productVariant.PictureId, "", false);
            model.PictureModel.ImageUrl = _pictureService.GetPictureUrl(productVariant.PictureId, "?width=125&height=125&mode=crop", false);
            model.PictureModel.Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name);
            model.PictureModel.AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name);
            model.HasSampleDownload = productVariant.IsDownload && productVariant.HasSampleDownload;
            model.IsCurrentCustomerRegistered = _workContext.CurrentUser.IsRegistered();

            //back in stock subscriptions)
            if (productVariant.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
                productVariant.BackorderMode == BackorderMode.NoBackorders &&
                productVariant.AllowBackInStockSubscriptions &&
                productVariant.StockQuantity <= 0)
            {
                //out of stock
                model.DisplayBackInStockSubscription = true;
                model.BackInStockAlreadySubscribed = _backInStockSubscriptionService.FindSubscription(_workContext.CurrentUser.Id, productVariant.Id) != null;
            }

            #endregion

            #region Product variant price
            model.ProductVariantPrice.ProductVariantId = productVariant.Id;
            model.ProductVariantPrice.DynamicPriceUpdate = _catalogSettings.EnableDynamicPriceUpdate;
            //if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
            //{
            model.ProductVariantPrice.HidePrices = false;
            if (productVariant.CustomerEntersPrice)
            {
                model.ProductVariantPrice.CustomerEntersPrice = true;
            }
            else
            {
                if (productVariant.CallForPrice)
                {
                    model.ProductVariantPrice.CallForPrice = true;
                }
                else
                {
                    GetPricing(productVariant,
                        x => model.ProductVariantPrice.Price = x,
                        x => model.ProductVariantPrice.OldPrice = x,
                        x => model.ProductVariantPrice.PriceWithDiscount = x,
                        x => model.ProductVariantPrice.PriceValue = x,
                        x => model.ProductVariantPrice.PriceWithDiscountValue = x,
                        x => model.ProductVariantPrice.HasSpecialPrice = x,
                        x => model.ProductVariantPrice.Shipping = x,
                        x => model.ProductVariantPrice.ShippingValue = x);
                }
            }
            //}
            //else
            //{
            //	model.ProductVariantPrice.HidePrices = true;
            //	model.ProductVariantPrice.OldPrice = null;
            //	model.ProductVariantPrice.Price = null;
            //}
            #endregion

            #region 'Add to cart' model

            model.AddToCart.ProductVariantId = productVariant.Id;

            //quantity
            model.AddToCart.EnteredQuantity = productVariant.OrderMinimumQuantity;

            //'add to cart', 'add to wishlist' buttons
            model.AddToCart.InStorePickupOnly = !productVariant.IsShipEnabled;
            model.AddToCart.DisableBuyButton = productVariant.DisableBuyButton; // || !_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart);
            model.AddToCart.DisableWishlistButton = productVariant.DisableWishlistButton; // || !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist);
            //if (!_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
            //{
            //	model.AddToCart.DisableBuyButton = true;
            //	model.AddToCart.DisableWishlistButton = true;
            //}
            //pre-order
            model.AddToCart.AvailableForPreOrder = productVariant.AvailableForPreOrder;
            model.AddToCart.CategoryId = categoryId;

            //customer entered price
            model.AddToCart.CustomerEntersPrice = productVariant.CustomerEntersPrice;
            if (model.AddToCart.CustomerEntersPrice)
            {
                decimal minimumCustomerEnteredPrice = _currencyService.ConvertFromPrimaryStoreCurrency(productVariant.MinimumCustomerEnteredPrice, _workContext.WorkingCurrency);
                decimal maximumCustomerEnteredPrice = _currencyService.ConvertFromPrimaryStoreCurrency(productVariant.MaximumCustomerEnteredPrice, _workContext.WorkingCurrency);

                model.AddToCart.CustomerEnteredPrice = minimumCustomerEnteredPrice;
                model.AddToCart.CustomerEnteredPriceRange = string.Format(_localizationService.GetResource("Products.EnterProductPrice.Range"),
                    _priceFormatter.FormatPrice(minimumCustomerEnteredPrice, false, false),
                    _priceFormatter.FormatPrice(maximumCustomerEnteredPrice, false, false));
            }
            //allowed quantities
            var allowedQuantities = productVariant.ParseAllowedQuatities();
            foreach (var qty in allowedQuantities)
            {
                model.AddToCart.AllowedQuantities.Add(new SelectListItem()
                {
                    Text = qty.ToString(),
                    Value = qty.ToString()
                });
            }

            #endregion

            #region Gift card

            model.GiftCard.IsGiftCard = productVariant.IsGiftCard;
            if (model.GiftCard.IsGiftCard)
            {
                model.GiftCard.GiftCardType = productVariant.GiftCardType;
                model.GiftCard.SenderName = _workContext.CurrentUser.GetFullName();
                model.GiftCard.SenderEmail = _workContext.CurrentUser.Email;
            }

            #endregion

            #region Product attributes

            var productVariantAttributes = _productAttributeService.GetProductVariantAttributesByProductVariantId(productVariant.Id);
            foreach (var attribute in productVariantAttributes)
            {
                var pvaModel = new ProductDetailsModel.ProductVariantModel.ProductVariantAttributeModel()
                {
                    Id = attribute.Id,
                    ProductVariantId = productVariant.Id,
                    ProductAttributeId = attribute.ProductAttributeId,
                    Name = attribute.ProductAttribute.GetLocalized(x => x.Name),
                    Description = attribute.ProductAttribute.GetLocalized(x => x.Description),
                    TextPrompt = attribute.TextPrompt,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                    AllowedFileExtensions = _catalogSettings.FileUploadAllowedExtensions,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var pvaValues = _productAttributeService.GetProductVariantAttributeValues(attribute.Id);
                    foreach (var pvaValue in pvaValues)
                    {
                        var pvaValueModel = new ProductDetailsModel.ProductVariantModel.ProductVariantAttributeValueModel()
                        {
                            Id = pvaValue.Id,
                            Name = pvaValue.GetLocalized(x => x.Name),
                            IsPreSelected = pvaValue.IsPreSelected,
                        };
                        pvaModel.Values.Add(pvaValueModel);

                        //display price if allowed
                        //if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                        //{
                        decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(pvaValue.PriceAdjustment, _workContext.WorkingCurrency);
                        if (pvaValue.PriceAdjustment > decimal.Zero)
                            pvaValueModel.PriceAdjustment = "+" + _priceFormatter.FormatPrice(priceAdjustment, false, false);
                        else if (pvaValue.PriceAdjustment < decimal.Zero)
                            pvaValueModel.PriceAdjustment = "-" + _priceFormatter.FormatPrice(-priceAdjustment, false, false);

                        pvaValueModel.PriceAdjustmentValue = priceAdjustment;
                        //}
                    }
                }

                model.ProductVariantAttributes.Add(pvaModel);
            }

            #endregion

            return model;
        }

        protected IEnumerable<ProductSortingEnum> GetAllowedProductSortingEnums()
        {
            return Enum.GetValues(typeof (ProductSortingEnum)).Cast<ProductSortingEnum>().Where(e => (int) e <= 10);
        }

        protected IList<SelectListItem> GetAvailableSortOptions(int selectedOption, bool showClearOption = false, string clearText = "Clear")
        {
            var sortOptions = new List<SelectListItem>();
            var currentPageUrl = _webHelper.GetThisPageUrl(true);
            foreach (var enumValue in GetAllowedProductSortingEnums())
            {
                var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "orderby=" + ((int)enumValue).ToString(), null);

                var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                sortOptions.Add(new SelectListItem()
                {
                    Text = sortValue,
                    Value = sortUrl,
                    Selected = enumValue == (ProductSortingEnum)selectedOption
                });
            }

            if (showClearOption)
            {
                sortOptions.Add(new SelectListItem
                {
                    Text = clearText,
                    Value = _webHelper.RemoveQueryString(currentPageUrl, "orderby")
                });
            }

            return sortOptions;
        }

        [NonAction]
        protected HomePageProductsModel.HomePageCategoryModel GetHomePageCategory(int siteId, int categoryId = 0, decimal? minimumPrice = null)
        {
            var searchOptions = new ProductSearchOptions
            {
                SearchAllStores = siteId == KnownIds.Website.Corporate,
                SortBy = ProductSortingEnum.DateListed,
                PageSize = 8,
                ExcludeCorporateProducts = true,
                PriceMin = minimumPrice,
                ExcludePriceReduction = _context.SiteId == KnownIds.Website.Corporate,
            };

            if (categoryId != 0) 
                searchOptions.CategoryId = categoryId;
            var productIds = _productService.GetHomePageProductIds(searchOptions);

            var category = categoryId == 0 ? null : _categoryService.GetCategoryById(categoryId);
            var categoryName = categoryId == 0 ? "All" : category.Name;
            var seName = categoryId == 0 ? "all" : category.GetSeName();
            return new HomePageProductsModel.HomePageCategoryModel()
            {
                Name = categoryName,
                SeName = seName,
                Products = PrepareProductOverviewModels(productIds, 244).ToList()
            };
        }

        protected SearchModel GetSearchModel(SearchModel model, SearchPagingFilteringModel command)
        {
            if (model == null)
                model = new SearchModel();

            //Franchisees search when they check all stores gets the location hover as well - RC 9/20/13
            model.IsCorporateSite = KnownIds.Website.Corporate == _context.SiteId;
            var getOnSameDomainInfo = !model.IsCorporateSite && model.AllStores;
            model.AllStores = model.AllStores || model.IsCorporateSite || !string.IsNullOrEmpty(model.Zip);
            model.ShowLocationPopups = model.AllStores;

            //'Continue shopping' URL
            if (_workContext.CurrentUser != null)
                _genericAttributeService.SaveAttribute(_workContext.CurrentUser, SystemCustomerAttributeNames.LastContinueShoppingPage, _webHelper.GetThisPageUrl(false));

            if (command.PageSize <= 0) command.PageSize = _catalogSettings.SearchPageProductsPerPage;
            if (command.PageNumber <= 0) command.PageNumber = 1;
            model.Q = model.Q == null ? "" : model.Q.Trim();

            if (model.SortByClosest)
            {
                command.OrderBy = (int) ProductSortingEnum.DistanceAsc;
            }

            if (command.OrderBy == 0)
            {
                model.PagingFilteringContext.IsDefaultSorting = true;
                command.OrderBy = (int)ProductSortingEnum.PriceDesc;
            }

            model.PagingFilteringContext.AvailableSortOptions = GetAvailableSortOptions(command.OrderBy,
                !model.PagingFilteringContext.IsDefaultSorting, "Clear Sort By");

            var categories = _categoryService.GetAllCategories();
            if (categories.Count > 0)
            {
                //first empty entry
                model.AvailableCategories.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = _localizationService.GetResource("Common.All")
                });
                //all other categories
                foreach (var c in categories)
                {
                    //generate full category name (breadcrumb)
                    string fullCategoryBreadcrumbName = "";
                    var breadcrumb = GetCategoryBreadCrumb(c);
                    for (int i = 0; i <= breadcrumb.Count - 1; i++)
                    {
                        fullCategoryBreadcrumbName += breadcrumb[i].GetLocalized(x => x.Name);
                        if (i != breadcrumb.Count - 1)
                            fullCategoryBreadcrumbName += " >> ";
                    }

                    model.AvailableCategories.Add(new SelectListItem()
                    {
                        Value = c.Id.ToString(),
                        Text = fullCategoryBreadcrumbName,
                        Selected = model.Cid == c.Id
                    });
                }
            }

            var manufacturers = _manufacturerService.GetAllManufacturers();
            if (manufacturers.Count > 0)
            {
                model.AvailableManufacturers.Add(new SelectListItem()
                {
                    Value = "0",
                    Text = _localizationService.GetResource("Common.All")
                });
                foreach (var m in manufacturers.OrderBy(x => x.Name))
                    model.AvailableManufacturers.Add(new SelectListItem()
                    {
                        Value = m.Id.ToString(),
                        Text = m.GetLocalized(x => x.Name),
                        Selected = model.Mid == m.Id
                    });
            }

            IPagedList<int> products = new PagedList<int>(Enumerable.Empty<int>(), 0, 1);
            // only search if query string search keyword is set (used to avoid searching or displaying search term min length error message on /search page load)
            if (Request.Params["Q"] != null || model.Q != null || model.Advanced)
            {
                if (!model.Advanced && model.Q.Length < _catalogSettings.ProductSearchTermMinimumLength)
                {
                    model.Warning = string.Format(_localizationService.GetResource("Search.SearchTermMinimumLengthIsNCharacters"), _catalogSettings.ProductSearchTermMinimumLength);
                }
                else
                {
                    var categoryIds = new List<int>();
                    int manufacturerId = 0;
                    decimal? minPriceConverted = null;
                    decimal? maxPriceConverted = null;
                    bool searchInDescriptions = false;
                    var clearance = false;
                    List<string> storeNumbers = null;

                    if (model.Advanced)
                    {
                        //Advanced search
                        var categoryId = model.Cid;
                        if (categoryId > 0)
                        {
                            categoryIds.Add(categoryId);
                            if (model.Isc)
                            {
                                //include subcategories
                                categoryIds.AddRange(GetChildCategoryIds(categoryId));
                            }
                        }

                        manufacturerId = model.Mid;

                        if (!string.IsNullOrEmpty(model.Zip))
                        {
                            storeNumbers = new List<string>();
                            var locations = _locationService.GetLocationsByPostalCode(model.Zip, 200, model.Dist);
                            storeNumbers.AddRange(locations.Select(l => l.StoreNumber).Distinct());
                        }
                        else if (!string.IsNullOrEmpty(model.Sn))
                        {
                            storeNumbers = new List<string> { model.Sn };
                        }

                        //min price
                        if (!string.IsNullOrEmpty(model.Pf))
                        {
                            decimal minPrice = decimal.Zero;
                            if (decimal.TryParse(model.Pf, out minPrice))
                                minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(minPrice, _workContext.WorkingCurrency);
                        }
                        //max price
                        if (!string.IsNullOrEmpty(model.Pt))
                        {
                            decimal maxPrice = decimal.Zero;
                            if (decimal.TryParse(model.Pt, out maxPrice))
                                maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(maxPrice, _workContext.WorkingCurrency);
                        }

                        clearance = model.Cl;
                    }

                    searchInDescriptions = model.Sid;
                    var searchInProductTags = searchInDescriptions;

                    products = _productService.GetProductIds(new ProductSearchOptions
                    {
                        CategoryIds = categoryIds,
                        ManufacturerId = manufacturerId,
                        PriceMin = minPriceConverted,
                        PriceMax = maxPriceConverted,
                        Keywords = model.Q,
                        SearchDescriptions = searchInDescriptions,
                        SearchProductTags = searchInProductTags,
                        PageIndex = command.PageNumber - 1,
                        PageSize = command.PageSize,
                        SearchAllStores = model.AllStores,
                        ClearanceOnly = clearance,
                        SortBy = (ProductSortingEnum)command.OrderBy,
                        StoreNumbers = storeNumbers
                    });

                    model.Products = PrepareProductOverviewModels(products, null, false, model.AllStores).ToList();
                    model.TotalResults = products.TotalCount;
                }
            }


            model.PagingFilteringContext.LoadPagedList(products);

            if (getOnSameDomainInfo)
            {
                var location = _locationService.GetLocationContactInfoBySiteId(_context.SiteId);
                foreach (var p in model.Products)
                {
                    p.IsNotOnStoreDomain = p.ProductLocation.Id != location.Id;
                }
            }
            return model;
        }

            #endregion

        #region Categories

        [MobileOnly]
        public ActionResult ShopOnline()
        {
            var model = new CategoryModel
            {
                Id = 0,
                Name = "Shop Online",
                IsCorpSite = KnownIds.Website.Corporate == _context.SiteId,
                IsCatalogHomePage = true
            };
            string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_NAVIGATION_MODEL_KEY, "top", "0", _workContext.WorkingLanguage.Id);
            model.SubCategories = _cacheManager.Get(cacheKey, () =>
            {
                var sc = _categoryService
                    .GetAllCategoriesByParentCategoryId(0)
                    .Select(x =>
                    {
                        var subCatName = x.GetLocalized(y => y.Name);
                        var subCatModel = new CategoryModel.SubCategoryModel()
                        {
                            Id = x.Id,
                            Name = subCatName,
                            SeName = x.GetSeName(),
                        };

                        return subCatModel;
                    })
                    .ToList();
                return sc;
            });

            //template
            var templateCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_TEMPLATE_MODEL_KEY, 0);
            var templateViewPath = _cacheManager.Get(templateCacheKey, () =>
            {
                var template = _categoryTemplateService.GetAllCategoryTemplates().FirstOrDefault();
                return template.ViewPath;
            });

            //activity log
            _customerActivityService.InsertActivity("PublicStore.ViewCategory", _localizationService.GetResource("ActivityLog.PublicStore.ViewCategory"), "top");

            return View(templateViewPath, model);
        }

        // Strip down version of Category() for async call
        public ActionResult GetProductsAsync(ProductQueryAsyncModel query)
        {
            var isMobile = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());

            if (isMobile)
                return Json(null);

            var pageSize = 24;
            var category = _categoryService.GetCategoryById(query.CategoryId);
            if (category == null || category.Deleted || !category.Published)
                return Json(null);

            var model = category.ToModel();

            model.SeName = (query.Br > 0 ?
                                (CorporateNav.GetBrandName(query.CategoryId, query.Br) ??
                                ObjectExtensions.IfNotNull(_manufacturerService.GetManufacturerById(query.Br), m => m.GetSeName())) : null)
                            ?? model.SeName;

            if (query.OrderBy == 0)
            {
                model.PagingFilteringContext.IsDefaultSorting = true;
                query.OrderBy = (int)ProductSortingEnum.PriceDesc;
            }
            if (!_catalogSettings.IgnoreFeaturedProducts &&
                _categoryService.GetTotalNumberOfFeaturedProducts(query.CategoryId) > 0)
            {
                //We use the fast GetTotalNumberOfFeaturedProducts before invoking of the slow SearchProducts
                //to ensure that we have at least one featured product
                var featuredProducts = _productService.GetProductIds(new ProductSearchOptions
                {
                    CategoryId = category.Id,
                    FeaturedProducts = true
                });

                model.FeaturedProducts = PrepareProductOverviewModels(featuredProducts).ToList();
            }

            var categoryIds = new List<int>();
            categoryIds.Add(category.Id);
            if (_catalogSettings.ShowProductsFromSubcategories)
            {
                //include subcategories
                categoryIds.AddRange(GetChildCategoryIds(category.Id));
            }

            var productIds = _productService.GetProductIds(new ProductSearchOptions
            {
                CategoryIds = categoryIds,
                ManufacturerId = query.Br,
                FeaturedProducts = _catalogSettings.IncludeFeaturedProductsInNormalLists ? null : (bool?)false,
                Used = (category.ParentCategoryId == KnownIds.Category.Accessories) ? (bool?)query.Used : null,
                PriceMin = query.PriceMin,
                PriceMax = query.PriceMax,
                SortBy = (ProductSortingEnum)query.OrderBy,
                PageIndex = query.PageNumber - 1,
                PageSize = pageSize,
                ClearanceOnly = query.IsClearanceOnly
            });

            //model.Products = PrepareProductOverviewModels(productIds).ToList();
            model.TotalResults = productIds.TotalCount;

            model.PagingFilteringContext.PriceRangeFilter.LoadPriceRangeFilters(category.PriceRanges, _webHelper, _priceFormatter);
            model.PagingFilteringContext.LoadPagedList(productIds);

            if (category.ParentCategoryId != KnownIds.Category.Accessories || (query.Used.HasValue && query.Used.Value))
            {
                model.UsedCategory = true;
            }

            return Content(JsonConvert.SerializeObject(model), "application/json");
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Category(int categoryId, CatalogPagingFilteringModel command, string seName,int view)
        {

            var isMobile = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());

            command.PageSize = view == 0 ? 24 : view;
            var category = _categoryService.GetCategoryById(categoryId);
            if (category == null || category.Deleted)
                return RedirectToRoute("HomePage");

            //Check whether the current user has a "Manage catalog" permission
            //It allows him to preview a category before publishing
            if (!category.Published)
                return RedirectToRoute("HomePage");

            var newSeName = (command.br > 0 ?
                                (CorporateNav.GetBrandName(categoryId, command.br) ??
                                ObjectExtensions.IfNotNull(_manufacturerService.GetManufacturerById(command.br), m => m.GetSeName())) : null)
                            ?? category.GetSeName();
            if (!string.Equals(seName, newSeName, StringComparison.InvariantCultureIgnoreCase))
            {
                var uriBuilder = new UriBuilder(Request.Url);
                if (string.IsNullOrEmpty(seName))
                {
                    uriBuilder.Path = uriBuilder.Path.Replace("/c/" + categoryId, "/c/" + categoryId + "/" + newSeName);
                }
                else
                {
                    uriBuilder.Path = uriBuilder.Path.Replace(seName, newSeName);
                }

                return Redirect/*Permanent*/(uriBuilder.ToString());
            }

            //'Continue shopping' URL
            //if (_workContext.CurrentUser != null)
            //	_genericAttributeService.SaveAttribute(_workContext.CurrentUser, SystemCustomerAttributeNames.LastContinueShoppingPage, _webHelper.GetThisPageUrl(false));

            if (command.PageNumber <= 0) command.PageNumber = 1;

            var model = category.ToModel();
            model.IsCorpSite = KnownIds.Website.Corporate == _context.SiteId;

            if (command.OrderBy == 0)
            {
                model.PagingFilteringContext.IsDefaultSorting = true;
                command.OrderBy = (int)ProductSortingEnum.PriceDesc;
            }

            // todo: cache
            var cLocation = _locationService.GetLocationContactInfoBySiteId(_context.SiteId);

            model.StoreName = cLocation != null ? cLocation.Name : "";

            //sorting
            model.PagingFilteringContext.AllowProductSorting = _catalogSettings.AllowProductSorting;
            if (model.PagingFilteringContext.AllowProductSorting)
            {
                model.PagingFilteringContext.AvailableSortOptions = GetAvailableSortOptions(command.OrderBy,
                    !model.PagingFilteringContext.IsDefaultSorting, "Clear Sort By");
            }

            //view mode
            model.PagingFilteringContext.AllowProductViewModeChanging = _catalogSettings.AllowProductViewModeChanging;
            var viewMode = !string.IsNullOrEmpty(command.ViewMode)
                ? command.ViewMode
                : _catalogSettings.DefaultViewMode;
            if (model.PagingFilteringContext.AllowProductViewModeChanging)
            {
                var currentPageUrl = _webHelper.GetThisPageUrl(true);
                //grid
                model.PagingFilteringContext.AvailableViewModes.Add(new SelectListItem()
                {
                    Text = _localizationService.GetResource("Categories.ViewMode.Grid"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=grid", null),
                    Selected = viewMode == "grid"
                });
                //list
                model.PagingFilteringContext.AvailableViewModes.Add(new SelectListItem()
                {
                    Text = _localizationService.GetResource("Categories.ViewMode.List"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=list", null),
                    Selected = viewMode == "list"
                });
            }

            //page size
            //Nuradin allowed pagesize to true for option and or rather &&.
            model.PagingFilteringContext.AllowCustomersToSelectPageSize = false;
            if (category.AllowCustomersToSelectPageSize && category.PageSizeOptions != null)
            {
                var pageSizes = category.PageSizeOptions.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (pageSizes.Any())
                {
                    // get the first page size entry to use as the default (category page load) or if customer enters invalid value via query string
                    if (command.PageSize <= 0 || !pageSizes.Contains(command.PageSize.ToString()))
                    {
                        int temp = 0;

                        if (int.TryParse(pageSizes.FirstOrDefault(), out temp))
                        {
                            if (temp > 0)
                            {
                                command.PageSize = temp;
                            }
                        }
                    }

                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "pagesize={0}", null);
                    sortUrl = _webHelper.RemoveQueryString(sortUrl, "pagenumber");

                    foreach (var pageSize in pageSizes)
                    {
                        int temp = 0;
                        if (!int.TryParse(pageSize, out temp))
                        {
                            continue;
                        }
                        if (temp <= 0)
                        {
                            continue;
                        }

                        model.PagingFilteringContext.PageSizeOptions.Add(new SelectListItem()
                        {
                            Text = pageSize,
                            Value = String.Format(sortUrl, pageSize),
                            Selected = pageSize.Equals(command.PageSize.ToString(), StringComparison.InvariantCultureIgnoreCase)
                        });
                    }

                    if (model.PagingFilteringContext.PageSizeOptions.Any())
                    {
                        model.PagingFilteringContext.PageSizeOptions = model.PagingFilteringContext.PageSizeOptions.OrderBy(x => int.Parse(x.Text)).ToList();
                        model.PagingFilteringContext.AllowCustomersToSelectPageSize = true;

                        if (command.PageSize <= 0)
                        {
                            command.PageSize = int.Parse(model.PagingFilteringContext.PageSizeOptions.FirstOrDefault().Text);
                        }
                    }
                }
            }
            else
            {
                //customer is not allowed to select a page size
                command.PageSize = 24; //category.PageSize;
            }

            if (command.PageSize <= 0) command.PageSize = 23;//category.PageSize;


            //price ranges
            model.PagingFilteringContext.PriceRangeFilter.LoadPriceRangeFilters(category.PriceRanges, _webHelper, _priceFormatter);
            var selectedPriceRange = model.PagingFilteringContext.PriceRangeFilter.GetSelectedPriceRange(_webHelper, category.PriceRanges);
            decimal? minPriceConverted = null;
            decimal? maxPriceConverted = null;
            if (selectedPriceRange != null)
            {
                if (selectedPriceRange.From.HasValue)
                    minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.From.Value, _workContext.WorkingCurrency);

                if (selectedPriceRange.To.HasValue)
                    maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.To.Value, _workContext.WorkingCurrency);
            }

            //category breadcrumb
            model.DisplayCategoryBreadcrumb = _catalogSettings.CategoryBreadcrumbEnabled;
            if (model.DisplayCategoryBreadcrumb)
            {
                foreach (var catBr in GetCategoryBreadCrumb(category))
                {
                    model.CategoryBreadcrumb.Add(new CategoryModel()
                    {
                        Id = catBr.Id,
                        Name = catBr.GetLocalized(x => x.Name),
                        SeName = catBr.GetSeName()
                    });
                }
            }
            else if (isMobile)
            {
                var parent = _categoryService.GetCategoryById(category.ParentCategoryId);
                if (parent != null)
                {
                    model.ParentCategory = new CategoryModel()
                    {
                        Id = parent.Id,
                        Name = parent.GetLocalized(c => c.Name),
                        SeName = parent.GetSeName()
                    };
                }
            }

            //subcategories
            model.SubCategories = _categoryService
                .GetAllCategoriesByParentCategoryId(categoryId)
                .Select(x =>
                {
                    var subCatName = x.GetLocalized(y => y.Name);
                    var subCatModel = new CategoryModel.SubCategoryModel()
                    {
                        Id = x.Id,
                        Name = subCatName,
                        SeName = x.GetSeName(),
                    };

                    //prepare picture model
                    if (!isMobile)
                    {
                        int pictureSize = _mediaSettings.CategoryThumbPictureSize;
                        var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY,
                            x.Id, pictureSize, true, _workContext.WorkingLanguage.Id,
                            _webHelper.IsCurrentConnectionSecured());
                        subCatModel.PictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                        {
                            var pictureModel = new PictureModel()
                            {
                                FullSizeImageUrl = _pictureService.GetPictureUrl(x.PictureId),
                                MidImageUrl =
                                    _pictureService.GetPictureUrl(x.PictureId, "?width=900&height=900&mode=crop"),
                                MobileImageUrl = 
                                    _pictureService.GetPictureUrl(x.PictureId, "?width=768&height=768&mode=crop&scale=both"),
                                ImageUrl = _pictureService.GetPictureUrl(x.PictureId, "?width=125&height=125&mode=crop"),
                                Title =
                                    string.Format(
                                        _localizationService.GetResource("Media.Category.ImageLinkTitleFormat"),
                                        subCatName),
                                AlternateText =
                                    string.Format(
                                        _localizationService.GetResource("Media.Category.ImageAlternateTextFormat"),
                                        subCatName)
                            };
                            return pictureModel;
                        });
                    }

                    return subCatModel;
                })
                .ToList();

            // Include acccessory as subcategory if it's mobile
            if (isMobile && !_categoryService.IsAccessoryCategory(categoryId) && model.SubCategories.Any())
            {
                var cname = categoryId == KnownIds.Category.Keyboard ? "keyboards" : category.Name;
                var accessory =
                    _categoryService.GetAllCategoriesByParentCategoryId(KnownIds.Category.Accessories)
                        .FirstOrDefault(c => c.Name.Contains(cname, StringComparison.InvariantCultureIgnoreCase));
                if (accessory != null)
                {
                    model.SubCategories.Add(new CategoryModel.SubCategoryModel
                    {
                        Id = accessory.Id,
                        Name = string.Format("{0} Accessories", category.Name),
                        SeName = accessory.SeName,
                        IsAccessoryCategory = true
                    });
                }
            }

            // Skip getting products if it's mobile or has subcategories
            if (!isMobile || !model.SubCategories.Any())
            {
                //featured products
                //Question: should we use '_catalogSettings.ShowProductsFromSubcategories' setting for displaying featured products?
                if (!_catalogSettings.IgnoreFeaturedProducts &&
                    _categoryService.GetTotalNumberOfFeaturedProducts(categoryId) > 0)
                {
                    //We use the fast GetTotalNumberOfFeaturedProducts before invoking of the slow SearchProducts
                    //to ensure that we have at least one featured product
                    var featuredProducts = _productService.GetProductIds(new ProductSearchOptions
                    {
                        CategoryId = category.Id,
                        FeaturedProducts = true
                    });

                    model.FeaturedProducts = PrepareProductOverviewModels(featuredProducts).ToList();
                }


                var categoryIds = new List<int>();
                categoryIds.Add(category.Id);
                if (_catalogSettings.ShowProductsFromSubcategories)
                {
                    //include subcategories
                    categoryIds.AddRange(GetChildCategoryIds(category.Id));
                }

                //products
                IList<int> alreadyFilteredSpecOptionIds =
                    model.PagingFilteringContext.SpecificationFilter.GetAlreadyFilteredSpecOptionIds(_webHelper);
                var productIds = _productService.GetProductIds(new ProductSearchOptions
                {
                    CategoryIds = categoryIds,
                    ManufacturerId = command.br,
                    FeaturedProducts = _catalogSettings.IncludeFeaturedProductsInNormalLists ? null : (bool?) false,
                    Used = (category.ParentCategoryId == KnownIds.Category.Accessories) ? (bool?) command.Used : null,
                    PriceMin = minPriceConverted,
                    PriceMax = maxPriceConverted,
                    FilteredSpecs = alreadyFilteredSpecOptionIds,
                    SortBy = (ProductSortingEnum) command.OrderBy,
                    PageIndex = command.PageNumber - 1,
                    PageSize = command.PageSize,
                    ClearanceOnly = model.PagingFilteringContext.PriceRangeFilter.IsClearance
                });

                if (category.ParentCategoryId == KnownIds.Category.Accessories && command.Used)
                {
                    ViewBag.Used = true;
                    model.UsedCategory = true;
                }
                else if (category.ParentCategoryId != KnownIds.Category.Accessories)
                {
                    model.UsedCategory = true;
                }

                model.Products = PrepareProductOverviewModels(productIds).ToList();
                model.TotalResults = productIds.TotalCount;

                model.PagingFilteringContext.LoadPagedList(productIds);
                model.PagingFilteringContext.ViewMode = viewMode;

                //specs
                model.PagingFilteringContext.SpecificationFilter.PrepareSpecsFilters(alreadyFilteredSpecOptionIds,
                    // Product search used to get specs and populate output param. Removed by Treefort for performance purposes.
                    // If we need this to work, see commented code at end of ProductService.SearchProducts

                    new List<int>(),
                    //filterableSpecificationAttributeOptionIds, 

                    _specificationAttributeService, _webHelper, _workContext);

            }

            //template
            var templateCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_TEMPLATE_MODEL_KEY, category.CategoryTemplateId);
            var templateViewPath = _cacheManager.Get(templateCacheKey, () =>
            {
                var template = _categoryTemplateService.GetCategoryTemplateById(category.CategoryTemplateId);
                if (template == null)
                    template = _categoryTemplateService.GetAllCategoryTemplates().FirstOrDefault();
                return template.ViewPath;
            });

            //activity log
            _customerActivityService.InsertActivity("PublicStore.ViewCategory", _localizationService.GetResource("ActivityLog.PublicStore.ViewCategory"), category.Name);

            return View(templateViewPath, model);
        }

        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult PriceNavigation(int currentCategoryId, int currentProductId)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.PRICE_NAVIGATION_MODEL_KEY, currentCategoryId, currentProductId, _workContext.WorkingLanguage.Id);
            //var cacheModel = _cacheManager.Get(cacheKey, () =>
            //{

            var category = _categoryService.GetCategoryById(currentCategoryId);

            if (category == null)
                return null;

            var model = category.ToModel();

            model.PagingFilteringContext.PriceRangeFilter.LoadPriceRangeFilters(category.PriceRanges, _webHelper, _priceFormatter);
            var selectedPriceRange = model.PagingFilteringContext.PriceRangeFilter.GetSelectedPriceRange(_webHelper, category.PriceRanges);
            decimal? minPriceConverted = null;
            decimal? maxPriceConverted = null;
            if (selectedPriceRange != null)
            {
                if (selectedPriceRange.From.HasValue)
                    minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.From.Value, _workContext.WorkingCurrency);

                if (selectedPriceRange.To.HasValue)
                    maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.To.Value, _workContext.WorkingCurrency);
            }

            var cacheModel = model.PagingFilteringContext.PriceRangeFilter;
            //});

            return PartialView(cacheModel);
        }


        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult CategoryNavigation(int currentCategoryId, int currentProductId, bool used = false)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_NAVIGATION_MODEL_KEY, currentCategoryId, currentProductId, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var currentCategory = _categoryService.GetCategoryById(currentCategoryId);
                if (currentCategory == null && currentProductId > 0)
                {
                    var productCategories = _categoryService.GetProductCategoriesByProductId(currentProductId);
                    if (productCategories.Count > 0)
                        currentCategory = productCategories[0].Category;
                }
                var breadCrumb = currentCategory != null ? GetCategoryBreadCrumb(currentCategory) : new List<Category>();
                var model = GetChildCategoryNavigationModel(breadCrumb, 0, currentCategory, 0);

                foreach (var m in model)
                {
                    if (currentCategory != null && currentCategory.ParentCategoryId == KnownIds.Category.Accessories &&
                        used)
                    {
                        m.Url = Url.Action("Category", "Catalog",new {categoryId = m.Id, SeName = m.SeName, used = true});
                        m.Used = used;
                    }
                    else
                    {
                        m.Url = Url.Action("Category", "Catalog", new { categoryId = m.Id, SeName = m.SeName });
                    }
                }

                return model;
            });

            return PartialView(cacheModel);
        }


        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult HomepageCategories()
        {
            var listModel = _categoryService.GetAllCategoriesDisplayedOnHomePage()
                .Select(x =>
                {
                    var catModel = x.ToModel();

                    //prepare picture model
                    int pictureSize = _mediaSettings.CategoryThumbPictureSize;
                    var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, x.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured());
                    catModel.PictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                    {
                        var pictureModel = new PictureModel()
                        {
                            FullSizeImageUrl = _pictureService.GetPictureUrl(x.PictureId),
                            ImageUrl = _pictureService.GetPictureUrl(x.PictureId, "?width=125&height=125&mode=crop"),
                            MidImageUrl = _pictureService.GetPictureUrl(x.PictureId, "?width=900&height=900&mode=max"),
                            MobileImageUrl = _pictureService.GetPictureUrl(x.PictureId, "?width=768&height=768&mode=crop&scale=both"),
                            Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), catModel.Name),
                            AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), catModel.Name)
                        };
                        return pictureModel;
                    });

                    return catModel;
                })
                .ToList();

            return PartialView(listModel);
        }

        public ActionResult Pagers()
        {
            return PartialView("_Pagers");
        }

        #endregion

        #region Manufacturers

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Manufacturer(int manufacturerId, CatalogPagingFilteringModel command, int categoryId = 0)
        {
            command.PageSize = 24;

            var manufacturer = _manufacturerService.GetManufacturerById(manufacturerId);
            if (manufacturer == null || manufacturer.Deleted)
                return RedirectToRoute("HomePage");

            //Check whether the current user has a "Manage catalog" permission
            //It allows him to preview a manufacturer before publishing
            if (!manufacturer.Published && !_permissionService.Authorize(StandardPermissionProvider.ManageCatalog))
                return RedirectToRoute("HomePage");

            //'Continue shopping' URL
            if (_workContext.CurrentUser != null)
                _genericAttributeService.SaveAttribute(_workContext.CurrentUser, SystemCustomerAttributeNames.LastContinueShoppingPage, _webHelper.GetThisPageUrl(false));

            if (command.PageNumber <= 0) command.PageNumber = 1;

            var model = manufacturer.ToModel();

            if (command.OrderBy == 0)
            {
                model.PagingFilteringContext.IsDefaultSorting = true;
                command.OrderBy = (int)ProductSortingEnum.NameAsc;
            }



            //sorting
            model.PagingFilteringContext.AllowProductSorting = _catalogSettings.AllowProductSorting;
            if (model.PagingFilteringContext.AllowProductSorting)
            {
                foreach (ProductSortingEnum enumValue in Enum.GetValues(typeof(ProductSortingEnum)))
                {
                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "orderby=" + ((int)enumValue).ToString(), null);

                    var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                    model.PagingFilteringContext.AvailableSortOptions.Add(new SelectListItem()
                    {
                        Text = sortValue,
                        Value = sortUrl,
                        Selected = enumValue == (ProductSortingEnum)command.OrderBy
                    });
                }
            }



            //view mode
            model.PagingFilteringContext.AllowProductViewModeChanging = _catalogSettings.AllowProductViewModeChanging;
            var viewMode = !string.IsNullOrEmpty(command.ViewMode)
                ? command.ViewMode
                : _catalogSettings.DefaultViewMode;
            if (model.PagingFilteringContext.AllowProductViewModeChanging)
            {
                var currentPageUrl = _webHelper.GetThisPageUrl(true);
                //grid
                model.PagingFilteringContext.AvailableViewModes.Add(new SelectListItem()
                {
                    Text = _localizationService.GetResource("Manufacturers.ViewMode.Grid"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=grid", null),
                    Selected = viewMode == "grid"
                });
                //list
                model.PagingFilteringContext.AvailableViewModes.Add(new SelectListItem()
                {
                    Text = _localizationService.GetResource("Manufacturers.ViewMode.List"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=list", null),
                    Selected = viewMode == "list"
                });
            }

            //page size
            model.PagingFilteringContext.AllowCustomersToSelectPageSize = false;
            if (manufacturer.AllowCustomersToSelectPageSize && manufacturer.PageSizeOptions != null)
            {
                var pageSizes = manufacturer.PageSizeOptions.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (pageSizes.Any())
                {
                    // get the first page size entry to use as the default (manufacturer page load) or if customer enters invalid value via query string
                    if (command.PageSize <= 0 || !pageSizes.Contains(command.PageSize.ToString()))
                    {
                        int temp = 0;

                        if (int.TryParse(pageSizes.FirstOrDefault(), out temp))
                        {
                            if (temp > 0)
                            {
                                command.PageSize = temp;
                            }
                        }
                    }

                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "pagesize={0}", null);
                    sortUrl = _webHelper.RemoveQueryString(sortUrl, "pagenumber");

                    foreach (var pageSize in pageSizes)
                    {
                        int temp = 0;
                        if (!int.TryParse(pageSize, out temp))
                        {
                            continue;
                        }
                        if (temp <= 0)
                        {
                            continue;
                        }

                        model.PagingFilteringContext.PageSizeOptions.Add(new SelectListItem()
                        {
                            Text = pageSize,
                            Value = String.Format(sortUrl, pageSize),
                            Selected = pageSize.Equals(command.PageSize.ToString(), StringComparison.InvariantCultureIgnoreCase)
                        });
                    }

                    model.PagingFilteringContext.PageSizeOptions = model.PagingFilteringContext.PageSizeOptions.OrderBy(x => int.Parse(x.Text)).ToList();

                    if (model.PagingFilteringContext.PageSizeOptions.Any())
                    {
                        model.PagingFilteringContext.PageSizeOptions = model.PagingFilteringContext.PageSizeOptions.OrderBy(x => int.Parse(x.Text)).ToList();
                        model.PagingFilteringContext.AllowCustomersToSelectPageSize = true;

                        if (command.PageSize <= 0)
                        {
                            command.PageSize = int.Parse(model.PagingFilteringContext.PageSizeOptions.FirstOrDefault().Text);
                        }
                    }
                }
            }
            else
            {
                //customer is not allowed to select a page size
                command.PageSize = manufacturer.PageSize;
            }

            if (command.PageSize <= 0) command.PageSize = manufacturer.PageSize;


            //price ranges
            model.PagingFilteringContext.PriceRangeFilter.LoadPriceRangeFilters(manufacturer.PriceRanges, _webHelper, _priceFormatter);
            var selectedPriceRange = model.PagingFilteringContext.PriceRangeFilter.GetSelectedPriceRange(_webHelper, manufacturer.PriceRanges);
            decimal? minPriceConverted = null;
            decimal? maxPriceConverted = null;
            if (selectedPriceRange != null)
            {
                if (selectedPriceRange.From.HasValue)
                    minPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.From.Value, _workContext.WorkingCurrency);

                if (selectedPriceRange.To.HasValue)
                    maxPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(selectedPriceRange.To.Value, _workContext.WorkingCurrency);
            }

            //featured products
            if (!_catalogSettings.IgnoreFeaturedProducts && _manufacturerService.GetTotalNumberOfFeaturedProducts(manufacturerId) > 0)
            {
                //We use the fast GetTotalNumberOfFeaturedProducts before invoking of the slow SearchProducts
                //to ensure that we have at least one featured product
                var featuredProducts = _productService.GetProductIds(new ProductSearchOptions
                {
                    ManufacturerId = manufacturer.Id,
                    FeaturedProducts = true
                });
                model.FeaturedProducts = PrepareProductOverviewModels(featuredProducts).ToList();
            }

            var products = _productService.GetProductIds(new ProductSearchOptions
            {
                ManufacturerId = manufacturer.Id,
                FeaturedProducts = _catalogSettings.IncludeFeaturedProductsInNormalLists ? null : (bool?)false,
                PriceMin = minPriceConverted,
                PriceMax = maxPriceConverted,
                SortBy = (ProductSortingEnum)command.OrderBy,
                PageIndex = command.PageNumber - 1,
                PageSize = command.PageSize
            });

            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);
            model.PagingFilteringContext.ViewMode = viewMode;


            //template
            var templateCacheKey = string.Format(ModelCacheEventConsumer.MANUFACTURER_TEMPLATE_MODEL_KEY, manufacturer.ManufacturerTemplateId);
            var templateViewPath = _cacheManager.Get(templateCacheKey, () =>
            {
                var template = _manufacturerTemplateService.GetManufacturerTemplateById(manufacturer.ManufacturerTemplateId);
                if (template == null)
                    template = _manufacturerTemplateService.GetAllManufacturerTemplates().FirstOrDefault();
                return template.ViewPath;
            });

            //activity log
            _customerActivityService.InsertActivity("PublicStore.ViewManufacturer", _localizationService.GetResource("ActivityLog.PublicStore.ViewManufacturer"), manufacturer.Name);

            return View(templateViewPath, model);
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ManufacturerAll()
        {
            var model = new List<ManufacturerModel>();
            var manufacturers = _manufacturerService.GetAllManufacturers();
            foreach (var manufacturer in manufacturers)
            {
                var modelMan = manufacturer.ToModel();

                //prepare picture model
                int pictureSize = _mediaSettings.ManufacturerThumbPictureSize;
                var manufacturerPictureCacheKey = string.Format(ModelCacheEventConsumer.MANUFACTURER_PICTURE_MODEL_KEY, manufacturer.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured());
                modelMan.PictureModel = _cacheManager.Get(manufacturerPictureCacheKey, () =>
                {
                    var pictureModel = new PictureModel()
                    {
                        FullSizeImageUrl = _pictureService.GetPictureUrl(manufacturer.PictureId),
                        MidImageUrl = _pictureService.GetPictureUrl(manufacturer.PictureId, "?width=900&height=900&mode=max"),
                        MobileImageUrl = _pictureService.GetPictureUrl(manufacturer.PictureId, "?width=768&height=768&mode=crop&scale=both"),
                        ImageUrl = _pictureService.GetPictureUrl(manufacturer.PictureId, "?width=125&height=125&mode=crop"),
                        Title = string.Format(_localizationService.GetResource("Media.Manufacturer.ImageLinkTitleFormat"), modelMan.Name),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Manufacturer.ImageAlternateTextFormat"), modelMan.Name)
                    };
                    return pictureModel;
                });
                model.Add(modelMan);
            }

            return View(model);
        }

        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult ManufacturerNavigation(int currentCategoryId)
        {
            var br = string.IsNullOrEmpty(_webHelper.QueryString<string>("br")) ? "0" : _webHelper.QueryString<string>("br");

            //I'm only grabbing from cache the manufactorers by currentCategoryId. Otherwise if everything else is added then I need to also have it check current Manufacturer
            string cacheKey = string.Format(ModelCacheEventConsumer.MANUFACTURER_NAVIGATION_MODEL_KEY, currentCategoryId, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var manufacturers = _manufacturerService.GetProductManufacturersByCategoryId(currentCategoryId, _workContext.SiteId);
                return manufacturers;
            });

            var model = new ManufacturerNavigationModel()
            {
                TotalManufacturers = cacheModel.Count
            };

            foreach (var manufacturer in cacheModel.Take(_catalogSettings.ManufacturersBlockItemsToDisplay))
            {
                var modelMan = new ManufacturerBriefInfoModel()
                {
                    Id = manufacturer.Id,
                    Name = manufacturer.GetLocalized(x => x.Name),
                    SeName = manufacturer.GetSeName(),
                    IsActive = br == manufacturer.Id.ToString(),
                    FilterUrl = _webHelper.ModifyQueryString(_webHelper.GetThisPageUrl(true), "br=" + manufacturer.Id, null)
                };
                model.Manufacturers.Add(modelMan);
            }

            return PartialView(model);
        }

        #endregion

        #region Products
     
        //product details page
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Product(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                return RedirectToRoute("HomePage");

            //prepare the model
            var model = PrepareProductDetailsPageModel(product);

            //check whether we have at leat one variant
            //if (model.ProductVariantModels.Count == 0)
            //    return RedirectToRoute("HomePage");

            // If it doens't have recently available variant
            // (i.e. available or sold within the last year)
            if (!model.HasRecentlyAvailableVariant)
                return RedirectToAction("Category",
                    new { categoryId = product.ProductCategories.Select(c => c.CategoryId).FirstOrDefault() });

            //save as recently viewed
            _recentlyViewedProductsService.AddProductToRecentlyViewedList(product.Id);

            //activity log
            _customerActivityService.InsertActivity("PublicStore.ViewProduct", _localizationService.GetResource("ActivityLog.PublicStore.ViewProduct"), product.Name);

            return View(model.ProductTemplateViewPath, model);
        }

        // Treefort enhancement. same as Product action minus the "Published" check (and other unneeded cruft)
        [AdminAuthorize]
        public ActionResult ProductPreview(int productId)
        {
            var product = _productService.GetProductById(productId);
            var model = PrepareProductDetailsPageModel(product, isPreview: true);
            return View(model.ProductTemplateViewPath, model);
        }

        //add product variant to cart using HTTP POST
        //currently we use this method only for mobile device version
        //desktop version uses AJAX version of this method (ShoppingCartController.AddProductVariantToCart)
        [HttpPost, ActionName("Product")]
        [ValidateInput(false)]
        public ActionResult AddProductVariantToCart(int productId, FormCollection form)
        {
            var isMobile = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());

            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published)
                return RedirectToRoute("HomePage");

            //manually process form
            int productVariantId = 0;
            ShoppingCartType cartType = ShoppingCartType.ShoppingCart;
            foreach (string formKey in form.AllKeys)
            {
                if (formKey.StartsWith("addtocartbutton-"))
                {
                    productVariantId = Convert.ToInt32(formKey.Substring(("addtocartbutton-").Length));
                    cartType = ShoppingCartType.ShoppingCart;
                }
                else if (formKey.StartsWith("addtowishlistbutton-"))
                {
                    productVariantId = Convert.ToInt32(formKey.Substring(("addtowishlistbutton-").Length));
                    cartType = ShoppingCartType.Wishlist;
                }
            }

            var productVariant = _productService.GetProductVariantById(productVariantId);
            if (productVariant == null)
                return RedirectToRoute("HomePage");

            #region Customer entered price
            decimal customerEnteredPrice = decimal.Zero;
            decimal customerEnteredPriceConverted = decimal.Zero;
            if (productVariant.CustomerEntersPrice)
            {
                foreach (string formKey in form.AllKeys)
                    if (formKey.Equals(string.Format("addtocart_{0}.CustomerEnteredPrice", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (decimal.TryParse(form[formKey], out customerEnteredPrice))
                            customerEnteredPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(customerEnteredPrice, _workContext.WorkingCurrency);
                        break;
                    }
            }
            #endregion

            #region Quantity
    
            int quantity = 1;
            foreach (string formKey in form.AllKeys)
                if (formKey.Equals(string.Format("addtocart_{0}.EnteredQuantity", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                {
                    int.TryParse(form[formKey], out quantity);
                    break;
                }

            #endregion

            var addToCartWarnings = new List<string>();
            string attributes = "";

            #region Product attributes
            string selectedAttributes = string.Empty;
            var productVariantAttributes = _productAttributeService.GetProductVariantAttributesByProductVariantId(productVariant.Id);
            foreach (var attribute in productVariantAttributes)
            {
                string controlId = string.Format("product_attribute_{0}_{1}_{2}", attribute.ProductVariantId, attribute.ProductAttributeId, attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                        {
                            var ddlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ddlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ddlAttributes);
                                if (selectedAttributeId > 0)
                                    selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.RadioList:
                        {
                            var rblAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(rblAttributes))
                            {
                                int selectedAttributeId = int.Parse(rblAttributes);
                                if (selectedAttributeId > 0)
                                    selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                        {
                            var txtAttribute = form[controlId];
                            if (!String.IsNullOrEmpty(txtAttribute))
                            {
                                string enteredText = txtAttribute.Trim();
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.MultilineTextbox:
                        {
                            var txtAttribute = form[controlId];
                            if (!String.IsNullOrEmpty(txtAttribute))
                            {
                                string enteredText = txtAttribute.Trim();
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            var day = form[controlId + "_day"];
                            var month = form[controlId + "_month"];
                            var year = form[controlId + "_year"];
                            DateTime? selectedDate = null;
                            try
                            {
                                selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                            }
                            catch { }
                            if (selectedDate.HasValue)
                            {
                                selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                    attribute, selectedDate.Value.ToString("D"));
                            }
                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            var httpPostedFile = this.Request.Files[controlId];
                            if ((httpPostedFile != null) && (!String.IsNullOrEmpty(httpPostedFile.FileName)))
                            {
                                int fileMaxSize = _catalogSettings.FileUploadMaximumSizeBytes;
                                if (httpPostedFile.ContentLength > fileMaxSize)
                                {
                                    addToCartWarnings.Add(string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), (int)(fileMaxSize / 1024)));
                                }
                                else
                                {
                                    //save an uploaded file
                                    var download = new Download()
                                    {
                                        DownloadGuid = Guid.NewGuid(),
                                        UseDownloadUrl = false,
                                        DownloadUrl = "",
                                        DownloadBinary = httpPostedFile.GetDownloadBits(),
                                        ContentType = httpPostedFile.ContentType,
                                        Filename = System.IO.Path.GetFileNameWithoutExtension(httpPostedFile.FileName),
                                        Extension = System.IO.Path.GetExtension(httpPostedFile.FileName),
                                        IsNew = true
                                    };
                                    _downloadService.InsertDownload(download);
                                    //save attribute
                                    selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
                                        attribute, download.DownloadGuid.ToString());
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            attributes = selectedAttributes;

            #endregion

            #region Gift cards

            string recipientName = "";
            string recipientEmail = "";
            string senderName = "";
            string senderEmail = "";
            string giftCardMessage = "";
            if (productVariant.IsGiftCard)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("giftcard_{0}.RecipientName", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        recipientName = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.RecipientEmail", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        recipientEmail = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.SenderName", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        senderName = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.SenderEmail", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        senderEmail = form[formKey];
                        continue;
                    }
                    if (formKey.Equals(string.Format("giftcard_{0}.Message", productVariantId), StringComparison.InvariantCultureIgnoreCase))
                    {
                        giftCardMessage = form[formKey];
                        continue;
                    }
                }

                attributes = _productAttributeParser.AddGiftCardAttribute(attributes,
                    recipientName, recipientEmail, senderName, senderEmail, giftCardMessage);
            }

            #endregion

            //save item
            addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentUser,
                productVariant, _workContext.SiteId, cartType, attributes, customerEnteredPriceConverted, quantity, true));

            #region Set already entered values

            //set already entered values (quantity, customer entered price, gift card attributes, product attributes
            //we do it manually because views do not use HTML helpers for rendering controls

            Action<ProductDetailsModel> setEnteredValues = (productModel) =>
            {
                //find product variant model
                var productVariantModel = productModel
                    .ProductVariantModels
                    .Where(x => x.Id == productVariant.Id)
                    .FirstOrDefault();
                if (productVariantModel == null)
                    return;

                #region 'Add to cart' model

                //entered quantity
                productVariantModel.AddToCart.EnteredQuantity = quantity;
                //allowed quantities
                var allowedQuantities = productVariant.ParseAllowedQuatities();
                if (allowedQuantities.Length > 0)
                {
                    var allowedQuantitySelectedItem = productVariantModel.AddToCart.AllowedQuantities
                        .Where(x => x.Text == quantity.ToString())
                        .FirstOrDefault();
                    if (allowedQuantitySelectedItem != null)
                    {
                        allowedQuantitySelectedItem.Selected = true;
                    }
                }

                //customer entered price
                if (productVariantModel.AddToCart.CustomerEntersPrice)
                {
                    productVariantModel.AddToCart.CustomerEnteredPrice = customerEnteredPrice;
                }

                #endregion

                #region Gift card attributes

                if (productVariant.IsGiftCard)
                {
                    productVariantModel.GiftCard.RecipientName = recipientName;
                    productVariantModel.GiftCard.RecipientEmail = recipientEmail;
                    productVariantModel.GiftCard.SenderName = senderName;
                    productVariantModel.GiftCard.SenderEmail = senderEmail;
                    productVariantModel.GiftCard.Message = giftCardMessage;
                }

                #endregion

                #region Product attributes
                //clear pre-defined values)
                foreach (var pvaModel in productVariantModel.ProductVariantAttributes)
                {
                    foreach (var pvavModel in pvaModel.Values)
                        pvavModel.IsPreSelected = false;
                }
                //select the previously entered ones
                foreach (var attribute in productVariantAttributes)
                {
                    string controlId = string.Format("product_attribute_{0}_{1}_{2}", attribute.ProductVariantId, attribute.ProductAttributeId, attribute.Id);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                            {
                                var ddlAttributes = form[controlId];
                                if (!String.IsNullOrEmpty(ddlAttributes))
                                {
                                    int selectedAttributeId = int.Parse(ddlAttributes);
                                    if (selectedAttributeId > 0)
                                    {
                                        var pvavModel = productVariantModel.ProductVariantAttributes
                                            .SelectMany(x => x.Values)
                                            .Where(y => y.Id == selectedAttributeId)
                                            .FirstOrDefault();
                                        if (pvavModel != null)
                                            pvavModel.IsPreSelected = true;
                                    }
                                }
                            }
                            break;
                        case AttributeControlType.RadioList:
                            {
                                var rblAttributes = form[controlId];
                                if (!String.IsNullOrEmpty(rblAttributes))
                                {
                                    int selectedAttributeId = int.Parse(rblAttributes);
                                    if (selectedAttributeId > 0)
                                    {
                                        var pvavModel = productVariantModel.ProductVariantAttributes
                                            .SelectMany(x => x.Values)
                                            .Where(y => y.Id == selectedAttributeId)
                                            .FirstOrDefault();
                                        if (pvavModel != null)
                                            pvavModel.IsPreSelected = true;
                                    }
                                }
                            }
                            break;
                        case AttributeControlType.Checkboxes:
                            {
                                var cblAttributes = form[controlId];
                                if (!String.IsNullOrEmpty(cblAttributes))
                                {
                                    foreach (var item in cblAttributes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                    {
                                        int selectedAttributeId = int.Parse(item);
                                        if (selectedAttributeId > 0)
                                        {
                                            var pvavModel = productVariantModel.ProductVariantAttributes
                                               .SelectMany(x => x.Values)
                                               .Where(y => y.Id == selectedAttributeId)
                                               .FirstOrDefault();
                                            if (pvavModel != null)
                                                pvavModel.IsPreSelected = true;
                                        }
                                    }
                                }
                            }
                            break;
                        case AttributeControlType.TextBox:
                            {
                                var txtAttribute = form[controlId];
                                if (!String.IsNullOrEmpty(txtAttribute))
                                {
                                    var pvaModel = productVariantModel
                                        .ProductVariantAttributes
                                        .Select(x => x)
                                        .Where(y => y.Id == attribute.Id)
                                        .FirstOrDefault();

                                    if (pvaModel != null)
                                        pvaModel.TextValue = txtAttribute;
                                }
                            }
                            break;
                        case AttributeControlType.MultilineTextbox:
                            {
                                var txtAttribute = form[controlId];
                                if (!String.IsNullOrEmpty(txtAttribute))
                                {
                                    var pvaModel = productVariantModel
                                        .ProductVariantAttributes
                                        .Select(x => x)
                                        .Where(y => y.Id == attribute.Id)
                                        .FirstOrDefault();

                                    if (pvaModel != null)
                                        pvaModel.TextValue = txtAttribute;
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                            {
                                var pvaModel = productVariantModel
                                    .ProductVariantAttributes
                                    .Select(x => x)
                                    .Where(y => y.Id == attribute.Id)
                                    .FirstOrDefault();
                                if (pvaModel != null)
                                {
                                    int day, month, year;
                                    if (int.TryParse(form[controlId + "_day"], out day))
                                        pvaModel.SelectedDay = day;
                                    if (int.TryParse(form[controlId + "_month"], out month))
                                        pvaModel.SelectedMonth = month;
                                    if (int.TryParse(form[controlId + "_year"], out year))
                                        pvaModel.SelectedYear = year;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

                #endregion
            };

            #endregion

            #region Return the view

            if (addToCartWarnings.Count == 0)
            {
                switch (cartType)
                {
                    case ShoppingCartType.Wishlist:
                        {
                            if (_shoppingCartSettings.DisplayWishlistAfterAddingProduct)
                            {
                                //redirect to the wishlist page
                                return RedirectToRoute("Wishlist");
                            }
                            else
                            {
                                //redisplay the page with "Product has been added to the wishlist" notification message
                                var model = PrepareProductDetailsPageModel(product);
                                this.SuccessNotification(_localizationService.GetResource("Products.ProductHasBeenAddedToTheWishlist"), false);
                                //set already entered values (quantity, customer entered price, gift card attributes, product attributes)
                                setEnteredValues(model);
                                return View(model.ProductTemplateViewPath, model);
                            }
                        }
                    case ShoppingCartType.ShoppingCart:
                    default:
                        {
                            if (isMobile || _shoppingCartSettings.DisplayCartAfterAddingProduct)
                            {
                                //redirect to the shopping cart page
                                return RedirectToRoute("ShoppingCart");
                            }
                            else
                            {
                                //redisplay the page with "Product has been added to the cart" notification message
                                var model = PrepareProductDetailsPageModel(product);
                                this.SuccessNotification(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart"), false);
                                //set already entered values (quantity, customer entered price, gift card attributes, product attributes)
                                setEnteredValues(model);
                                return View(model.ProductTemplateViewPath, model);
                            }
                        }
                }
            }
            else
            {
                //Errors
                foreach (string error in addToCartWarnings)
                    ModelState.AddModelError("", error);

                //If we got this far, something failed, redisplay form
                var model = PrepareProductDetailsPageModel(product);
                //set already entered values (quantity, customer entered price, gift card attributes, product attributes
                setEnteredValues(model);
                return View(model.ProductTemplateViewPath, model);
            }

            #endregion
        }

        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult ProductBreadcrumb(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            //if (!_catalogSettings.CategoryBreadcrumbEnabled)
            //    return Content("");

            var cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_BREADCRUMB_MODEL_KEY, product.Id, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var model = new ProductDetailsModel.ProductBreadcrumbModel()
                {
                    ProductId = product.Id,
                    ProductName = product.GetLocalized(x => x.Name),
                    ProductSeName = product.GetSeName()
                };
                var productCategories = _categoryService.GetProductCategoriesByProductId(product.Id);
                if (productCategories.Count > 0)
                {
                    var category = productCategories[0].Category;
                    if (category != null)
                    {
                        foreach (var catBr in GetCategoryBreadCrumb(category))
                        {
                            model.CategoryBreadcrumb.Add(new CategoryModel()
                            {
                                Id = catBr.Id,
                                Name = catBr.GetLocalized(x => x.Name),
                                SeName = catBr.GetSeName()
                            });
                        }
                    }
                }
                return model;
            });

            return PartialView(cacheModel);
        }

        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult ProductManufacturers(int productId)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_MANUFACTURERS_MODEL_KEY, productId, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var model = _manufacturerService.GetProductManufacturersByProductId(productId)
                    .Select(x =>
                    {
                        var m = x.Manufacturer.ToModel();
                        return m;
                    })
                    .ToList();
                return model;
            });

            return PartialView(cacheModel);
        }

        [ChildActionOnly]
        public ActionResult ProductReviewOverview(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            var model = new ProductReviewOverviewModel()
            {
                ProductId = product.Id,
                RatingSum = product.ApprovedRatingSum,
                TotalReviews = product.ApprovedTotalReviews,
                AllowCustomerReviews = product.AllowCustomerReviews
            };
            return PartialView(model);
        }

        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult ProductSpecifications(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            var model = PrepareProductSpecificationModel(product);
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ProductTierPrices(int productVariantId)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
            //	return Content(""); //hide prices

            var variant = _productService.GetProductVariantById(productVariantId);
            if (variant == null)
                throw new ArgumentException("No product variant found with the specified id");

            if (!variant.HasTierPrices)
                return Content(""); //no tier prices

            var model = variant.TierPrices
                .OrderBy(x => x.Quantity)
                .ToList()
                .FilterForCustomer(_workContext.CurrentUser)
                .RemoveDuplicatedQuantities()
                .Select(tierPrice =>
                {
                    var m = new ProductDetailsModel.ProductVariantModel.TierPriceModel()
                    {
                        Quantity = tierPrice.Quantity,
                    };
                    decimal priceBase = _priceCalculationService.GetFinalPrice(variant, _workContext.CurrentUser, decimal.Zero, _catalogSettings.DisplayTierPricesWithDiscounts, tierPrice.Quantity);
                    //_taxService.GetProductPrice(variant, tierPrice.Price, out taxRate);
                    decimal price = _currencyService.ConvertFromPrimaryStoreCurrency(priceBase, _workContext.WorkingCurrency);
                    m.Price = _priceFormatter.FormatPrice(price, false, false);
                    return m;
                })
                .ToList();

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult RelatedProducts(int productId, int? productThumbPictureSize)
        {
            var products = new List<Product>();
            var relatedProducts = _productService.GetRelatedProductsByProductId1(productId, siteId: _context.SiteId);
            foreach (var product in _productService.GetProductsByIds(relatedProducts.Select(x => x.ProductId2).ToArray()))
            {
                //ensure that a product has at least one available variant
                var variants = _productService.GetProductVariantsByProductId(product.Id);
                if (variants.Count > 0)
                    products.Add(product);
            }
            products = products.Take(5).ToList();
            var productList = PrepareProductOverviewModels(products, productThumbPictureSize).ToList();

            var model = new RelatedProductModel()
            {
                Products = productList,
                IsCorp = KnownIds.Website.Corporate == _context.SiteId
            };

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ProductsAlsoPurchased(int productId, int? productThumbPictureSize)
        {
            if (!_catalogSettings.ProductsAlsoPurchasedEnabled)
                return Content("");

            var products = _orderReportService.GetProductsAlsoPurchasedById(productId,
                _catalogSettings.ProductsAlsoPurchasedNumber);

            var model = PrepareProductOverviewModels(products, productThumbPictureSize).ToList();

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult ShareButton()
        {
            if (_catalogSettings.ShowShareButton && !String.IsNullOrEmpty(_catalogSettings.PageShareCode))
            {
                var shareCode = _catalogSettings.PageShareCode;
                if (_webHelper.IsCurrentConnectionSecured())
                {
                    //need to change the addthis link to be https linked when the page is, so that the page doesnt ask about mixed mode when viewed in https...
                    shareCode = shareCode.Replace("http://", "https://");
                }

                return PartialView("ShareButton", shareCode);
            }

            return Content("");
        }

        [ChildActionOnly]
        public ActionResult CrossSellProductsDetailPage(int productId)
        {
            var crossSellProducts = _productService.GetCrossSellProductsByProductId1(productId).Take(4).ToList();
            var mainProduct = _productService.GetProductById(productId);
            var category = (from pc in mainProduct.ProductCategories select pc.Category).FirstOrDefault();
            var products = crossSellProducts.Select(x => x.ProductId2);

            var categoryId = 0;

            //Hardcoding the more button for accessories.
            if (category != null)
            {
                switch (category.ParentCategoryId)
                {
                    case KnownIds.Category.Guitars:
                        categoryId = KnownIds.Category.AccessoriesGuitars;
                        break;
                    case KnownIds.Category.Percussion:
                        categoryId = KnownIds.Category.AccessoriesPercussion;
                        break;
                    case KnownIds.Category.Band:
                        categoryId = KnownIds.Category.AccessoriesBandInstruments;
                        break;
                    case KnownIds.Category.Keyboard:
                        categoryId = KnownIds.Category.AccessoriesKeyboards;
                        break;
                    case KnownIds.Category.ProSound:
                        categoryId = KnownIds.Category.AccessoriesProSound;
                        break;
                    default:
                        if (products.Any())
                        {
                            var cats = _productService.GetProductById(products.First()).ProductCategories;
                            categoryId = cats.Any() ? cats.First().CategoryId : 0;
                        }
                        break;
                }
            }

            var model = new CrossSellProductsModel
            {
                Products = PrepareProductOverviewModels(products).ToList(),
                CategoryId = categoryId
            };

            return PartialView("CrossSellProducts", model);
        }

        [ChildActionOnly]
        public ActionResult CrossSellProducts(int? productThumbPictureSize)
        {
            var cart = _workContext.CurrentUser.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();

            var products = _productService.GetCrosssellProductsByShoppingCart(cart, _shoppingCartSettings.CrossSellsNumber);
            //Cross-sell products are dispalyed on the shoppnig cart page.
            //We know that the entire shopping cart page is not refresh
            //even if "ShoppingCartSettings.DisplayCartAfterAddingProduct" setting  is enabled.
            //That's why we force page refresh (redirect) in this case
            var model = new CrossSellProductsModel
            {
                Products = PrepareProductOverviewModels(
                    products, productThumbPictureSize: productThumbPictureSize, forceRedirectionAfterAddingToCart: true).ToList(),
                CategoryId =
                    !products.Any() ? 0 :
                    !products.First().ProductCategories.Any() ? 0 :
                    products.First().ProductCategories.First().CategoryId
            };

            return PartialView(model);
        }

        //recently viewed products
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult RecentlyViewedProducts()
        {
            var model = new List<ProductOverviewModel>();
            if (_catalogSettings.RecentlyViewedProductsEnabled)
            {
                var products = _recentlyViewedProductsService.GetRecentlyViewedProducts(_catalogSettings.RecentlyViewedProductsNumber);
                model.AddRange(PrepareProductOverviewModels(products));
            }
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult RecentlyViewedProductsBlock(int? productThumbPictureSize)
        {
            var model = new List<ProductOverviewModel>();
            if (_catalogSettings.RecentlyViewedProductsEnabled)
            {
                var products = _recentlyViewedProductsService.GetRecentlyViewedProducts(_catalogSettings.RecentlyViewedProductsNumber);
                model.AddRange(PrepareProductOverviewModels(products, productThumbPictureSize));
            }
            return PartialView(model);
        }

        //recently added products
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult RecentlyAddedProducts()
        {
            var model = new List<ProductOverviewModel>();
            if (_catalogSettings.RecentlyAddedProductsEnabled)
            {
                var products = _productService.GetProductIds(new ProductSearchOptions
                {
                    SortBy = ProductSortingEnum.DateListed,
                    PageSize = _catalogSettings.RecentlyAddedProductsNumber
                });
                model.AddRange(PrepareProductOverviewModels(products));
            }
            return View(model);
        }

        public ActionResult RecentlyAddedProductsRss()
        {
            var feed = new SyndicationFeed(
                                    string.Format("{0}: Recently added products", _storeInformationSettings.StoreName),
                                    "Information about products",
                                    new Uri(_webHelper.GetStoreLocation(false)),
                                    "RecentlyAddedProductsRSS",
                                    DateTime.UtcNow);

            if (!_catalogSettings.RecentlyAddedProductsEnabled)
                return new RssActionResult() { Feed = feed };

            var items = new List<SyndicationItem>();
            var products = _productService.SearchProducts(new ProductSearchOptions
            {
                SortBy = ProductSortingEnum.DateListed,
                PageSize = _catalogSettings.RecentlyAddedProductsNumber
            });

            foreach (var product in products)
            {
                string productUrl = Url.RouteUrl("Product", new { productId = product.Id, SeName = product.GetSeName() }, "http");
                items.Add(new SyndicationItem(product.GetLocalized(x => x.Name), product.GetLocalized(x => x.ShortDescription), new Uri(productUrl), String.Format("RecentlyAddedProduct:{0}", product.Id), product.CreatedOnUtc));
            }
            feed.Items = items;
            return new RssActionResult() { Feed = feed };
        }

        [ChildActionOnly]
        public ActionResult HomepageBestSellers(int? productThumbPictureSize)
        {
            if (!_catalogSettings.ShowBestsellersOnHomepage || _catalogSettings.NumberOfBestsellersOnHomepage == 0)
                return Content("");

            //load and cache report
            var report = _cacheManager.Get(ModelCacheEventConsumer.HOMEPAGE_BESTSELLERS_IDS_KEY,
                () => _orderReportService.BestSellersReport(null, null, null, null, null, _catalogSettings.NumberOfBestsellersOnHomepage));
            var products = new List<Product>();
            foreach (var line in report)
            {
                var productVariant = _productService.GetProductVariantById(line.ProductVariantId);
                if (productVariant != null)
                {
                    var product = productVariant.Product;
                    if (product != null)
                    {
                        bool contains = false;
                        foreach (var p in products)
                        {
                            if (p.Id == product.Id)
                            {
                                contains = true;
                                break;
                            }
                        }
                        if (!contains)
                            products.Add(product);
                    }
                }
            }


            var model = new HomePageBestsellersModel()
            {
                UseSmallProductBox = _catalogSettings.UseSmallProductBoxOnHomePage,
            };
            model.Products = PrepareProductOverviewModels(products, productThumbPictureSize).ToList();
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult HomepageProducts(IList<int> visibleCategoryIds, decimal? minimumPrice)
        {
            var siteId = _context.SiteId;
            var categoryIds = visibleCategoryIds ?? new[]
            {
                0,
                KnownIds.Category.Guitars,
                KnownIds.Category.Percussion,
                KnownIds.Category.Band,
                KnownIds.Category.Keyboard,
                KnownIds.Category.ProSound,
                KnownIds.Category.Accessories
            };

            minimumPrice = minimumPrice ?? 0m;

            var model = new HomePageProductsModel();
            foreach (var cId in categoryIds)
            {

                var cacheCategoryKey = string.Format("nop.homepage.products-{0}-{1}-{2}", siteId, cId, minimumPrice);
                var categoryId = cId;
                var cachedCategoryModel = _cacheManager.Get(cacheCategoryKey, 10,
                    () => GetHomePageCategory(siteId, categoryId, minimumPrice));
                model.Categories.Add(cachedCategoryModel);
            }

            return PartialView(model);
        }

        public ActionResult BackInStockSubscribePopup(int productVariantId)
        {
            var variant = _productService.GetProductVariantById(productVariantId);
            if (variant == null || variant.Deleted)
                throw new ArgumentException("No product variant found with the specified id");

            var product = variant.Product;

            var model = new BackInStockSubscribeModel();
            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();
            model.ProductVariantId = variant.Id;
            model.IsCurrentCustomerRegistered = _workContext.CurrentUser.IsRegistered();
            model.MaximumBackInStockSubscriptions = _catalogSettings.MaximumBackInStockSubscriptions;
            model.CurrentNumberOfBackInStockSubscriptions = _backInStockSubscriptionService.GetAllSubscriptionsByCustomerId(_workContext.CurrentUser.Id, 0, 1).TotalCount;
            if (variant.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
                variant.BackorderMode == BackorderMode.NoBackorders &&
                variant.AllowBackInStockSubscriptions &&
                variant.StockQuantity <= 0)
            {
                //out of stock
                model.SubscriptionAllowed = true;
                model.AlreadySubscribed = _backInStockSubscriptionService.FindSubscription(_workContext.CurrentUser.Id, variant.Id) != null;
            }
            return View(model);
        }

        [HttpPost, ActionName("BackInStockSubscribePopup")]
        public ActionResult BackInStockSubscribePopupPOST(int productVariantId)
        {
            var variant = _productService.GetProductVariantById(productVariantId);
            if (variant == null || variant.Deleted)
                throw new ArgumentException("No product variant found with the specified id");

            if (!_workContext.CurrentUser.IsRegistered())
                return Content(_localizationService.GetResource("BackInStockSubscriptions.OnlyRegistered"));

            if (variant.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
                variant.BackorderMode == BackorderMode.NoBackorders &&
                variant.AllowBackInStockSubscriptions &&
                variant.StockQuantity <= 0)
            {
                //out of stock
                var subscription = _backInStockSubscriptionService.FindSubscription(_workContext.CurrentUser.Id,
                                                                                    variant.Id);
                if (subscription != null)
                {
                    //unsubscribe
                    _backInStockSubscriptionService.DeleteSubscription(subscription);
                    return Content("Unsubscribed");
                }
                else
                {
                    if (_backInStockSubscriptionService.GetAllSubscriptionsByCustomerId(_workContext.CurrentUser.Id, 0, 1).TotalCount >= _catalogSettings.MaximumBackInStockSubscriptions)
                        return Content(string.Format(_localizationService.GetResource("BackInStockSubscriptions.MaxSubscriptions"), _catalogSettings.MaximumBackInStockSubscriptions));

                    //subscribe   
                    subscription = new BackInStockSubscription()
                    {
                        Customer = _workContext.CurrentUser,
                        ProductVariant = variant,
                        CreatedOnUtc = DateTime.UtcNow
                    };
                    _backInStockSubscriptionService.InsertSubscription(subscription);
                    return Content("Subscribed");
                }

            }
            else
            {
                return Content(_localizationService.GetResource("BackInStockSubscriptions.NotAllowed"));
            }
        }

        #endregion

        #region Product tags

        //Product tags
        [ChildActionOnly]
        public ActionResult ProductTags(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            var cacheKey = string.Format(ModelCacheEventConsumer.PRODUCTTAG_BY_PRODUCT_MODEL_KEY, product.Id, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var model = product.ProductTags
                    .OrderByDescending(x => x.ProductCount)
                    .Select(x =>
                    {
                        var ptModel = new ProductTagModel()
                        {
                            Id = x.Id,
                            Name = x.GetLocalized(y => y.Name),
                            SeName = x.GetSeName(),
                            ProductCount = x.ProductCount
                        };
                        return ptModel;
                    })
                    .ToList();
                return model;
            });

            return PartialView(cacheModel);
        }

        [ChildActionOnly]
        //[OutputCache(Duration = 120, VaryByCustom = "WorkingLanguage")]
        public ActionResult PopularProductTags()
        {
            var cacheKey = string.Format(ModelCacheEventConsumer.PRODUCTTAG_POPULAR_MODEL_KEY, _workContext.WorkingLanguage.Id);
            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var model = new PopularProductTagsModel();

                //get all tags
                var allTags = _productTagService.GetAllProductTags();
                var tags = allTags
                    .OrderByDescending(x => x.ProductCount)
                    .Where(x => x.ProductCount > 0)
                    .Take(_catalogSettings.NumberOfProductTags)
                    .ToList();
                //sorting
                tags = tags.OrderBy(x => x.GetLocalized(y => y.Name)).ToList();

                model.TotalTags = allTags.Count;

                foreach (var tag in tags)
                    model.Tags.Add(new ProductTagModel()
                    {
                        Id = tag.Id,
                        Name = tag.GetLocalized(y => y.Name),
                        SeName = tag.GetSeName(),
                        ProductCount = tag.ProductCount
                    });
                return model;
            });

            return PartialView(cacheModel);
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductsByTag(int productTagId, CatalogPagingFilteringModel command)
        {
            var productTag = _productTagService.GetProductById(productTagId);
            if (productTag == null)
                return RedirectToRoute("HomePage");

            if (command.PageNumber <= 0) command.PageNumber = 1;

            var model = new ProductsByTagModel()
            {
                TagName = productTag.GetLocalized(y => y.Name)
            };


            //sorting
            model.PagingFilteringContext.AllowProductSorting = _catalogSettings.AllowProductSorting;
            if (model.PagingFilteringContext.AllowProductSorting)
            {
                foreach (ProductSortingEnum enumValue in Enum.GetValues(typeof(ProductSortingEnum)))
                {
                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "orderby=" + ((int)enumValue).ToString(), null);

                    var sortValue = enumValue.GetLocalizedEnum(_localizationService, _workContext);
                    model.PagingFilteringContext.AvailableSortOptions.Add(new SelectListItem()
                    {
                        Text = sortValue,
                        Value = sortUrl,
                        Selected = enumValue == (ProductSortingEnum)command.OrderBy
                    });
                }
            }


            //view mode
            model.PagingFilteringContext.AllowProductViewModeChanging = _catalogSettings.AllowProductViewModeChanging;
            var viewMode = !string.IsNullOrEmpty(command.ViewMode)
                ? command.ViewMode
                : _catalogSettings.DefaultViewMode;
            if (model.PagingFilteringContext.AllowProductViewModeChanging)
            {
                var currentPageUrl = _webHelper.GetThisPageUrl(true);
                //grid
                model.PagingFilteringContext.AvailableViewModes.Add(new SelectListItem()
                {
                    Text = _localizationService.GetResource("Categories.ViewMode.Grid"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=grid", null),
                    Selected = viewMode == "grid"
                });
                //list
                model.PagingFilteringContext.AvailableViewModes.Add(new SelectListItem()
                {
                    Text = _localizationService.GetResource("Categories.ViewMode.List"),
                    Value = _webHelper.ModifyQueryString(currentPageUrl, "viewmode=list", null),
                    Selected = viewMode == "list"
                });
            }

            //page size
            model.PagingFilteringContext.AllowCustomersToSelectPageSize = false;
            if (_catalogSettings.ProductsByTagAllowCustomersToSelectPageSize && _catalogSettings.ProductsByTagPageSizeOptions != null)
            {
                var pageSizes = _catalogSettings.ProductsByTagPageSizeOptions.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (pageSizes.Any())
                {
                    // get the first page size entry to use as the default ('products by tag' page load) or if customer enters invalid value via query string
                    if (command.PageSize <= 0 || !pageSizes.Contains(command.PageSize.ToString()))
                    {
                        int temp = 0;

                        if (int.TryParse(pageSizes.FirstOrDefault(), out temp))
                        {
                            if (temp > 0)
                            {
                                command.PageSize = temp;
                            }
                        }
                    }

                    var currentPageUrl = _webHelper.GetThisPageUrl(true);
                    var sortUrl = _webHelper.ModifyQueryString(currentPageUrl, "pagesize={0}", null);
                    sortUrl = _webHelper.RemoveQueryString(sortUrl, "pagenumber");

                    foreach (var pageSize in pageSizes)
                    {
                        int temp = 0;
                        if (!int.TryParse(pageSize, out temp))
                        {
                            continue;
                        }
                        if (temp <= 0)
                        {
                            continue;
                        }

                        model.PagingFilteringContext.PageSizeOptions.Add(new SelectListItem()
                        {
                            Text = pageSize,
                            Value = String.Format(sortUrl, pageSize),
                            Selected = pageSize.Equals(command.PageSize.ToString(), StringComparison.InvariantCultureIgnoreCase)
                        });
                    }

                    if (model.PagingFilteringContext.PageSizeOptions.Any())
                    {
                        model.PagingFilteringContext.PageSizeOptions = model.PagingFilteringContext.PageSizeOptions.OrderBy(x => int.Parse(x.Text)).ToList();
                        model.PagingFilteringContext.AllowCustomersToSelectPageSize = true;

                        if (command.PageSize <= 0)
                        {
                            command.PageSize = int.Parse(model.PagingFilteringContext.PageSizeOptions.FirstOrDefault().Text);
                        }
                    }
                }
            }
            else
            {
                //customer is not allowed to select a page size
                command.PageSize = _catalogSettings.ProductsByTagPageSize;
            }

            if (command.PageSize <= 0) command.PageSize = _catalogSettings.ProductsByTagPageSize;

            var products = _productService.GetProductIds(new ProductSearchOptions
            {
                ProductTagId = productTag.Id,
                SortBy = (ProductSortingEnum)command.OrderBy,
                PageIndex = command.PageNumber - 1,
                PageSize = command.PageSize
            });

            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);
            model.PagingFilteringContext.ViewMode = viewMode;
            return View(model);
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductTagsAll()
        {
            var model = new PopularProductTagsModel();
            model.Tags = _productTagService.GetAllProductTags()
                .OrderByDescending(x => x.ProductCount)
                .Where(x => x.ProductCount > 0)
                //sort by name
                .OrderBy(x => x.GetLocalized(y => y.Name))
                .Select(x =>
                {
                    var ptModel = new ProductTagModel()
                    {
                        Id = x.Id,
                        Name = x.GetLocalized(y => y.Name),
                        SeName = x.GetSeName(),
                        ProductCount = x.ProductCount
                    };
                    return ptModel;
                })
                .ToList();
            return View(model);
        }

        #endregion

        #region Product reviews

        //products reviews
        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductReviews(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
                return RedirectToRoute("HomePage");

            var model = new ProductReviewsModel();
            PrepareProductReviewsModel(model, product);
            //only registered users can leave reviews
            if (_workContext.CurrentUser.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            //default value
            model.AddProductReview.Rating = _catalogSettings.DefaultProductRatingValue;
            return View(model);
        }

        [HttpPost, ActionName("ProductReviews")]
        [FormValueRequired("add-review")]
        [CaptchaValidator]
        public ActionResult ProductReviewsAdd(int productId, ProductReviewsModel model, bool captchaValid)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
                return RedirectToRoute("HomePage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnProductReviewPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
            }

            if (_workContext.CurrentUser.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
            }

            if (ModelState.IsValid)
            {
                //save review
                int rating = model.AddProductReview.Rating;
                if (rating < 1 || rating > 5)
                    rating = _catalogSettings.DefaultProductRatingValue;
                bool isApproved = !_catalogSettings.ProductReviewsMustBeApproved;

                var productReview = new ProductReview()
                {
                    ProductId = product.Id,
                    CustomerId = _workContext.CurrentUser.Id,
                    IpAddress = _webHelper.GetCurrentIpAddress(),
                    Title = model.AddProductReview.Title,
                    ReviewText = model.AddProductReview.ReviewText,
                    Rating = rating,
                    HelpfulYesTotal = 0,
                    HelpfulNoTotal = 0,
                    IsApproved = isApproved,
                    CreatedOnUtc = DateTime.UtcNow,
                    UpdatedOnUtc = DateTime.UtcNow,
                };
                _customerContentService.InsertCustomerContent(productReview);

                //update product totals
                _productService.UpdateProductReviewTotals(product);

                //notify store owner
                if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
                    _workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);


                PrepareProductReviewsModel(model, product);
                model.AddProductReview.Title = null;
                model.AddProductReview.ReviewText = null;

                model.AddProductReview.SuccessfullyAdded = true;
                if (!isApproved)
                    model.AddProductReview.Result = _localizationService.GetResource("Reviews.SeeAfterApproving");
                else
                    model.AddProductReview.Result = _localizationService.GetResource("Reviews.SuccessfullyAdded");

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            PrepareProductReviewsModel(model, product);
            return View(model);
        }

        [HttpPost]
        public ActionResult SetProductReviewHelpfulness(int productReviewId, bool washelpful)
        {
            var productReview = _customerContentService.GetCustomerContentById(productReviewId) as ProductReview;
            if (productReview == null)
                throw new ArgumentException("No product review found with the specified id");

            if (_workContext.CurrentUser.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
            {
                return Json(new
                {
                    Result = _localizationService.GetResource("Reviews.Helpfulness.OnlyRegistered"),
                    TotalYes = productReview.HelpfulYesTotal,
                    TotalNo = productReview.HelpfulNoTotal
                });
            }

            //customers aren't allowed to vote for their own reviews
            if (productReview.CustomerId == _workContext.CurrentUser.Id)
            {
                return Json(new
                {
                    Result = _localizationService.GetResource("Reviews.Helpfulness.YourOwnReview"),
                    TotalYes = productReview.HelpfulYesTotal,
                    TotalNo = productReview.HelpfulNoTotal
                });
            }

            //delete previous helpfulness
            var oldPrh = (from prh in productReview.ProductReviewHelpfulnessEntries
                          where prh.CustomerId == _workContext.CurrentUser.Id
                          select prh).FirstOrDefault();
            if (oldPrh != null)
                _customerContentService.DeleteCustomerContent(oldPrh);

            //insert new helpfulness
            var newPrh = new ProductReviewHelpfulness()
            {
                ProductReviewId = productReview.Id,
                CustomerId = _workContext.CurrentUser.Id,
                IpAddress = _webHelper.GetCurrentIpAddress(),
                WasHelpful = washelpful,
                IsApproved = true, //always approved
                CreatedOnUtc = DateTime.UtcNow,
                UpdatedOnUtc = DateTime.UtcNow,
            };
            _customerContentService.InsertCustomerContent(newPrh);

            //new totals
            int helpfulYesTotal = (from prh in productReview.ProductReviewHelpfulnessEntries
                                   where prh.WasHelpful
                                   select prh).Count();
            int helpfulNoTotal = (from prh in productReview.ProductReviewHelpfulnessEntries
                                  where !prh.WasHelpful
                                  select prh).Count();

            productReview.HelpfulYesTotal = helpfulYesTotal;
            productReview.HelpfulNoTotal = helpfulNoTotal;
            _customerContentService.UpdateCustomerContent(productReview);

            return Json(new
            {
                Result = _localizationService.GetResource("Reviews.Helpfulness.SuccessfullyVoted"),
                TotalYes = productReview.HelpfulYesTotal,
                TotalNo = productReview.HelpfulNoTotal
            });
        }

        #endregion

        #region Email a friend

        //products email a friend
        [ChildActionOnly]
        public ActionResult ProductEmailAFriendButton(int productId)
        {
            if (!_catalogSettings.EmailAFriendEnabled)
                return Content("");
            var model = new ProductEmailAFriendModel()
            {
                ProductId = productId
            };

            return PartialView("ProductEmailAFriendButton", model);
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult ProductEmailAFriend(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
                return RedirectToRoute("HomePage");

            var model = new ProductEmailAFriendModel();
            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();
            model.YourEmailAddress = _workContext.CurrentUser.Email;
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage;
            return View(model);
        }

        [HttpPost, ActionName("ProductEmailAFriend")]
        [FormValueRequired("send-email")]
        [CaptchaValidator]
        public ActionResult ProductEmailAFriendSend(ProductEmailAFriendModel model, bool captchaValid)
        {
            var product = _productService.GetProductById(model.ProductId);
            if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
                return RedirectToRoute("HomePage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
            }

            //check whether the current customer is guest and ia allowed to email a friend
            if (_workContext.CurrentUser.IsGuest() && !_catalogSettings.AllowAnonymousUsersToEmailAFriend)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Products.EmailAFriend.OnlyRegisteredUsers"));
            }

            if (ModelState.IsValid)
            {
                //email
                _workflowMessageService.SendProductEmailAFriendMessage(_workContext.CurrentUser,
                        _workContext.WorkingLanguage.Id, product,
                        model.YourEmailAddress, model.FriendEmail,
                        Core.Html.HtmlHelper.FormatText(model.PersonalMessage, false, true, false, false, false, false));

                model.ProductId = product.Id;
                model.ProductName = product.GetLocalized(x => x.Name);
                model.ProductSeName = product.GetSeName();

                model.SuccessfullySent = true;
                model.Result = _localizationService.GetResource("Products.EmailAFriend.SuccessfullySent");

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            model.ProductId = product.Id;
            model.ProductName = product.GetLocalized(x => x.Name);
            model.ProductSeName = product.GetSeName();
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage;
            return View(model);
        }

        #endregion

        #region Comparing products

        //compare products
        public ActionResult AddProductToCompareList(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null || product.Deleted || !product.Published)
                return RedirectToRoute("HomePage");

            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            _compareProductsService.AddProductToCompareList(productId);

            return RedirectToRoute("CompareProducts");
        }

        public ActionResult RemoveProductFromCompareList(int productId)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                return RedirectToRoute("HomePage");

            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            _compareProductsService.RemoveProductFromCompareList(productId);

            return RedirectToRoute("CompareProducts");
        }

        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult CompareProducts()
        {
            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            var model = new CompareProductsModel()
            {
                IncludeShortDescriptionInCompareProducts = _catalogSettings.IncludeShortDescriptionInCompareProducts,
                IncludeFullDescriptionInCompareProducts = _catalogSettings.IncludeFullDescriptionInCompareProducts,
            };
            var products = _compareProductsService.GetComparedProducts();
            PrepareProductOverviewModels(products)
                .ToList()
                .ForEach(model.Products.Add);
            return View(model);
        }

        public ActionResult ClearCompareList()
        {
            if (!_catalogSettings.CompareProductsEnabled)
                return RedirectToRoute("HomePage");

            _compareProductsService.ClearCompareProducts();

            return RedirectToRoute("CompareProducts");
        }

        [ChildActionOnly]
        public ActionResult CompareProductsButton(int productId)
        {
            if (!_catalogSettings.CompareProductsEnabled)
                return Content("");

            var model = new AddToCompareListModel()
            {
                ProductId = productId
            };

            return PartialView("CompareProductsButton", model);
        }

        #endregion

        #region Searching

        [NopHttpsRequirement(SslRequirement.No)]
        [ValidateInput(false)]
        public ActionResult SearchAsync(SearchModel model, SearchPagingFilteringModel command)
        {
            model = GetSearchModel(model, command);
            return Content(JsonConvert.SerializeObject(model), "application/json");
        }

        [NopHttpsRequirement(SslRequirement.No)]
        [ValidateInput(false)]
        public ActionResult Search(SearchModel model, SearchPagingFilteringModel command)
        {
            model = GetSearchModel(model, command);

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult SearchBox()
        {
            var model = new SearchBoxModel()
            {
                RandomId = new Random().Next(0, 100),
                AutoCompleteEnabled = _catalogSettings.ProductSearchAutoCompleteEnabled,
                ShowProductImagesInSearchAutoComplete = _catalogSettings.ShowProductImagesInSearchAutoComplete,
                SearchTermMinimumLength = _catalogSettings.ProductSearchTermMinimumLength,
                IsCorpSite = _workContext.SiteId == KnownIds.Website.Corporate
            };
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult AdvancedSearchPopup()
        {
            return PartialView("_AdvancedSearchPopup");
        }

        public ActionResult AdvancedSearch()
        {
            var isMobile = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());

            if (isMobile)
            {
                var model = GetAdvancedModel();
                return PartialView("_AdvancedSearch", model);
            }
            return PartialView("_AdvancedSearch");
        }

        public ActionResult GetAdvancedSearchModel()
        {
            var model = GetAdvancedModel();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavedSearchComplete(string slug, bool cancel = false)
        {
            var ss = _productService.GetSavedSearch(slug);
            var model = new SavedSearchCompleteModel { Cancelled = cancel };

            if (ss == null)
            {
                if (!cancel)
                    return HttpNotFound();
            }
            else
            {
                model.Id = ss.Id;
                model.Slug = ss.Slug;
                model.Email = ss.EmailAddress;
                model.Frequency = ss.Frequency;
                model.Duration = ss.Duration;
                model.NotifyOnlyWhenNewResults = ss.NotifyOnlyWhenNewResults;
                model.FillFromQuerystring(ss.SearchParametersQuerystring);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavedSearch(SavedSearchModel model, string action)
        {
            var ss = model.Id == 0
                         ? new SavedSearch()
                         : _productService.GetSavedSearch(model.Id);

            if (action.ToLower().Contains("cancel"))
            {
                _productService.CancelSavedSearch(ss);
                return RedirectToAction("SavedSearchComplete", new { slug = ss.Slug, cancel = true });
            }

            if (!ModelState.IsValid)
            {
                FillSearchFormModel(model);
                return Json(new {model = model, error = true});
            }

            ss.EmailAddress = model.Email;
            ss.Frequency = model.Frequency;
            ss.Duration = model.Duration;
            ss.StartDate = DateTime.Today;
            ss.EndDate = ss.StartDate.Add(TimeSpan.Parse(model.Duration));
            ss.SiteId = _context.SiteId;
            ss.NotifyOnlyWhenNewResults = model.NotifyOnlyWhenNewResults;
            ss.SearchParametersQuerystring = model.ToQuerystring();

            if (ss.Id == 0)
                _productService.InsertSavedSearch(ss);
            else
                _productService.UpdateSavedSearch(ss);

            if (Request.IsAjaxRequest())
                return SavedSearchComplete(ss.Slug);
            else
                return RedirectToAction("SavedSearchComplete", new { slug = ss.Slug });
        }

        [HttpGet]
        public ActionResult SavedSearch(bool? showLearnMore, SavedSearchModel model)
        {
            return View(model);
        }

        public ActionResult GetSavedSearchPartial(bool? showLearnMore, SavedSearchModel model)
        {
            model = model ?? new SavedSearchModel();
            FillSearchFormModel(model);
            model.IsCorporateSite = _context.SiteId == KnownIds.Website.Corporate;
            model.ShowLearnMore = showLearnMore ?? false;

            SavedSearch ss = null;
            if (!string.IsNullOrEmpty(model.Slug))
            {
                ss = _productService.GetSavedSearch(model.Slug);
                if (ss == null)
                    return HttpNotFound();
            }

            if (ss == null && model.Id > 0)
            {
                ss = _productService.GetSavedSearch(model.Id);
                if (ss == null)
                    return HttpNotFound();
            }

            if (ss != null && ss.DeletedOnUtc <= DateTime.UtcNow)
            {
                model.Cancelled = true;
                return PartialView("_SavedSearchPartial", model);
            }
            else if (ss != null && (ss.EndDate <= DateTime.Now))
            {
                model.Ended = true;
                return PartialView("_SavedSearchPartial", model);
            }

            if (ss != null)
            {
                model.Id = ss.Id;
                model.Slug = ss.Slug;
                model.Email = ss.EmailAddress;
                model.Frequency = ss.Frequency;
                model.Duration = ss.Duration;
                model.NotifyOnlyWhenNewResults = ss.NotifyOnlyWhenNewResults;
                model.FillFromQuerystring(ss.SearchParametersQuerystring);
            }

            return PartialView("_SavedSearchPartial", model);
        }


        public ActionResult SendSavedSearch(string secret)
        {
            if (secret == "itisok")
            {
                var searches = _productService.GetSavedSearchesToNotify();
                foreach (var search in searches)
                {
                    var s = search;
                    _workflowMessageService.SendSavedSearchResultsNotification(search, 1, o => MaterializeSavedSearchProducts(o, s));
                }
            }
            return Content("ok");
        }

        private ProductSearchTokenModel MaterializeSavedSearchProducts(ProductSearchOptions options, SavedSearch savedSearch)
        {
            var model = new ProductSearchTokenModel();

            options.PageIndex = 0;
            options.PageSize = 100;
            var productIds = _productService.GetProductIds(options);
            var products = PrepareProductOverviewModels(productIds, AllSearch: options.SearchAllStores);

            var ageForNew = new TimeSpan(1, 0, 0, 0);
            if (savedSearch.Frequency == "weekly")
                ageForNew = new TimeSpan(7, 0, 0, 0);

            var tokenProducts = new List<ProductSearchTokenModel.Product>();
            foreach (var product in products)
            {
                var isNewResult = product.CreatedOnUtc.ToLocalTime() >= DateTime.Today.Subtract(ageForNew);
                if (isNewResult)
                    model.NewProducts += 1;

                tokenProducts.Add(new ProductSearchTokenModel.Product
                {
                    //Id = product.Id,
                    ProductUrl = _webHelper.GetUrl(savedSearch.SiteId, false, "p", product.Id.ToString(), product.SeName),
                    Title = product.Name,
                    ImageUrl = product.DefaultPictureModel.ImageUrl,
                    Location = product.ProductLocation.City + ", " + product.ProductLocation.Region,
                    Price = product.ProductPrice.Price,
                    Clearance = product.ProductPrice.Clearance,
                    IsNewResult = isNewResult
                });
            }
            model.Products = tokenProducts;

            return model;
        }

        private SearchModel GetAdvancedModel()
        {
            var model = new SearchModel();
            FillSearchFormModel(model);

            model.IsCorporateSite = (_context.SiteId == KnownIds.Website.Corporate);
            model.AllStores = false;
            model.Cl = false;
            model.Advanced = true;
            model.Sid = true;
            return model;
        }

        private void FillSearchFormModel(ISearchFormModel model)
        {
            var filledModel = _cacheManager.Get(ADVANCED_SEARCH_MODEL_KEY, 60, () =>
            {
                // Use the most extensive model, JSON deserializer will crap out when attempting to deserialize interface
                var cachedModel = new SearchModel();
                cachedModel.AvailableDistances = new[]
                {
                    new SelectListItem {Text = "25 miles", Value = "25", Selected = true},
                    new SelectListItem {Text = "50 miles", Value = "50", Selected = false},
                    new SelectListItem {Text = "100 miles", Value = "100", Selected = false}
                };

                var topCategoryIds = new[]
                {
                    KnownIds.Category.Guitars, KnownIds.Category.Percussion, KnownIds.Category.Band,
                    KnownIds.Category.Keyboard,
                    KnownIds.Category.ProSound, KnownIds.Category.Accessories
                };

                var manufacturers = _manufacturerService.GetAllManufacturers();

                var allCategoryOption = new CategoryOption
                {
                    Value = "0",
                    Text = _localizationService.GetResource("Common.All")
                };
                allCategoryOption.AvailableCategories.Add(new SelectListItem
                {
                    Value="0",
                    Text = _localizationService.GetResource("Common.All")
                });
                cachedModel.CategoryOptions.Add(allCategoryOption);

                foreach (var cId in topCategoryIds)
                {
                    var category = _categoryService.GetCategoryById(cId);
                    var categoryOption = new CategoryOption
                    {
                        Value = category.Id.ToString(),
                        Text = category.Name
                    };

                    categoryOption.AvailableCategories.Add(new CategorySelectListItem()
                    {
                        Value = category.Id.ToString(),
                        Text = _localizationService.GetResource("Common.All"),
                        BrandListId = CatalogExtension.GetBrandListId(category.DrsCode)
                    });

                    var subCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
                    foreach (var sc in subCategories)
                    {
                        categoryOption.AvailableCategories.Add(new CategorySelectListItem()
                        {
                            Value = sc.Id.ToString(),
                            Text = sc.Name,
                            BrandListId = CatalogExtension.GetBrandListId(sc.DrsCode)
                        });
                    }

                    categoryOption.Selected = categoryOption.AvailableCategories.Any(x => x.Selected);
                    cachedModel.CategoryOptions.Add(categoryOption);
                }

                // Legacy advanced search form -- FOR MOBILE
                var categories = _categoryService.GetAllCategories();
                if (categories.Count > 0)
                {
                    //first empty entry
                    cachedModel.AvailableCategories.Add(new SelectListItem()
                    {
                        Value = "0",
                        Text = _localizationService.GetResource("Common.All")
                    });
                    //all other categories
                    foreach (var c in categories)
                    {
                        //generate full category name (breadcrumb)
                        string fullCategoryBreadcrumbName = "";
                        var breadcrumb = GetCategoryBreadCrumb(c);
                        for (int i = 0; i <= breadcrumb.Count - 1; i++)
                        {
                            fullCategoryBreadcrumbName += breadcrumb[i].GetLocalized(x => x.Name);
                            if (i != breadcrumb.Count - 1)
                                fullCategoryBreadcrumbName += " >> ";
                        }

                        cachedModel.AvailableCategories.Add(new SelectListItem()
                        {
                            Value = c.Id.ToString(),
                            Text = fullCategoryBreadcrumbName
                        });
                    }
                }

                if (manufacturers.Count > 0)
                {
                    cachedModel.AvailableManufacturers.Add(new BrandSelectListItem()
                    {
                        Value = "0",
                        Text = _localizationService.GetResource("Common.All"),
                        BrandListIds = null
                    });
                    foreach (var m in manufacturers.OrderBy(x => x.Name))
                        cachedModel.AvailableManufacturers.Add(new BrandSelectListItem()
                        {
                            Value = m.Id.ToString(),
                            Text = m.GetLocalized(x => x.Name),
                            BrandListIds = m.GetBrandListIds()
                        });
                }

                cachedModel.Stores = _locationService.GetAllLocations().Select(l => new SelectListItem
                {
                    Text = l.Region + " - " + l.City,
                    Value = l.StoreNumber
                }).ToList();

                cachedModel.Stores.Insert(0, new SelectListItem {Text = "All", Value = ""});

                return cachedModel;
            });

            model.CategoryOptions = filledModel.CategoryOptions;
            model.AvailableDistances = filledModel.AvailableDistances;
            model.AvailableCategories = filledModel.AvailableCategories;
            model.AvailableManufacturers = filledModel.AvailableManufacturers;
            model.Stores = filledModel.Stores;

            var selectedCategory = model.AvailableCategories.FirstOrDefault(x => x.Value == model.Cid.ToString());
            if (selectedCategory != null) selectedCategory.Selected = true;

            var selectedManufacturer = model.AvailableManufacturers.FirstOrDefault(x => x.Value == model.Mid.ToString());
            if (selectedManufacturer != null) selectedManufacturer.Selected = true;
        }

        public ActionResult SearchTermAutoComplete(string term)
        {
            if (String.IsNullOrWhiteSpace(term) || term.Length < _catalogSettings.ProductSearchTermMinimumLength)
                return Content("");

            //products
            var productNumber = _catalogSettings.ProductSearchAutoCompleteNumberOfProducts > 0 ?
                _catalogSettings.ProductSearchAutoCompleteNumberOfProducts : 10;

            var products = _productService.GetProductIds(new ProductSearchOptions
            {
                Keywords = term,
                PageSize = productNumber,
                SearchCategoryName = false
            });

            var models = PrepareProductOverviewModels(products, _mediaSettings.AutoCompleteSearchThumbPictureSize).ToList();
            var result = (from p in models
                          select new
                          {
                              label = p.Name,
                              producturl = Url.RouteUrl("Product", new { productId = p.Id, SeName = p.SeName }),
                              productpictureurl = p.DefaultPictureModel.ImageUrl
                          })
                          .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region EmailStoreForm

        [HttpGet]
        public ActionResult EmailStoreAboutProduct(string topMessage, int productId)
        {
            return PartialView("_EmailStoreForm", new EmailStoreAboutProductModel() { TopMessage = topMessage, ProductId = productId });
        }


        [Honeypot(true)]
        [HttpPost]
        public ActionResult EmailStoreAboutProduct(EmailStoreAboutProductModel model)
        {
            var returnText = "";

            if (Request.HoneypotFaild())
                return Json(new { message = "Email Sent!" });

            if (string.IsNullOrEmpty(model.Note) || string.IsNullOrEmpty(model.Name))
                returnText = "You must fill in the required fields.";

            if (!string.IsNullOrEmpty(returnText))
            {
                Response.StatusCode = 400;
            }
            else
            {
                var product = _productService.GetProductById(model.ProductId);

                if (product == null)
                {
                    Response.StatusCode = 400;
                    return Json(new { message = "Could Not Find Product" });
                }

                var location = _locationService.GetLocationContactInfoBySiteId(product.SiteId);

                if (location == null || string.IsNullOrEmpty(location.Email))
                {
                    Response.StatusCode = 400;
                    return Json(new { message = "Could Not Find An Email Address for this location" });
                }

                var productVariant = product.ProductVariants.First();

                var bodyText = string.Format(@"A new Email from a customer about product {4}.)
                                EmailAddress:Reply to {0}
                                Customer Name: {1}
                                Email Address: {0}
                                Phone Number: {2}
                                Notes: {3}
                                Product SKU: {5}
                                ", model.Email, model.Name, string.IsNullOrEmpty(model.Phone) ? "Not given" : model.Phone, model.Note, product.Name, productVariant.Sku);

                // note the double braces in <style>, which is escaping for string formatting later

                var bodyHtml =
                        string.Format(@"<!DOCTYPE html>
                    <html>
                    <head>
	                    <style>
		                    table {{ border: none; }}
		                    table td {{ padding: 4px; }}
	                    </style>
                    </head>
                    <body>
	                    <p>A new Email from a customer about <a href=http://www.musicgoround.com{0}>product</a>.</p>
	                    Email Address: <p><b>Reply to <a href='mailto:{1}'>{1}</a></b></p>
	                    <p>
		                    <table border='0'>
			                    <tr><td>Customer Name:</td><td>{2}</td></tr>
			                    <tr><td>Email Address:</td><td>{1}</td></tr>
			                    <tr><td>Phone Number:</td><td>{3}</td></tr>
			                    <tr><td>Notes:</td><td>{4}</td></tr>
			                    <tr><td>Product SKU:</td><td>{5}</td></tr>
		                    </table>
	                    </p>
                    </body>
                    </html>
                    ", Url.Action("Product", new { productId = product.Id }), model.Email, model.Name, string.IsNullOrEmpty(model.Phone) ? "Not given" : model.Phone, model.Note, productVariant.Sku);

                var htmlView = AlternateView.CreateAlternateViewFromString(bodyHtml, new ContentType("text/html"));

                var sender = "noreply@musicgoround.com";
                var subject = string.Format("Email from customer about the {0} - sku {1}", product.Name, productVariant.Sku);
                var msg = new MailMessage();
                msg.From = new MailAddress(sender);

                msg.To.Add(location.Email);
                msg.Subject = subject;
                msg.Body = bodyText;
                msg.AlternateViews.Add(htmlView);

                if (!string.IsNullOrEmpty(model.Email))
                {
                    msg.ReplyToList.Add(model.Email);
                }

                using (var client = new SmtpClient())
                {
                    client.Send(msg);
                }

                returnText = "Email Sent!";
            }

            return Json(new { message = returnText });
        }


        #endregion

    }
}
