﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Nop.Core;
using Nop.Services.Catalog;
using Treefort.Common.Mvc;
using Treefort.Portal;
using Treefort.Portal.Queries.Redirect;

namespace Nop.Web.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class ErrorsController : BaseNopController
    {
        private readonly IProductService _productService;
        private readonly IRedirectProduct _redirectProduct;
        private readonly IWorkContext _context;

        public ErrorsController(IProductService productService, IRedirectProduct redirectProduct, IWorkContext context)
        {
            _productService = productService;
            _redirectProduct = redirectProduct;
            _context = context;
        }

        public ActionResult Error()
        {
			Response.StatusCode = (int)HttpStatusCode.InternalServerError;
			Response.TrySkipIisCustomErrors = true;
            return View(_context != null && _context.SiteId != KnownIds.Website.Corporate);
        }

        public ActionResult NotFound()
        {
			Response.StatusCode = (int)HttpStatusCode.NotFound;
			Response.TrySkipIisCustomErrors = true;
            return View(_context != null && _context.SiteId != KnownIds.Website.Corporate);
        }

        public ActionResult NoRevision()
        {
            return View();
        }

        public ActionResult RedirectProducts(string id)
        {
	        int productId;
			if (string.IsNullOrEmpty(id) || !int.TryParse(id, out productId)) 
			{
				return RedirectToActionPermanent("Index", "Home");
			}

            var oldProd = _redirectProduct.Execute(productId);

            if (oldProd == null)
            {
                return RedirectToActionPermanent("Index", "Home");
            }

            var productVariant = _productService.GetProductVariantBySku(oldProd.SKU);
            var product = productVariant == null ? null : _productService.GetProductById(productVariant.ProductId);

            if (productVariant == null || product == null || 
                productVariant.Deleted || product.Deleted || 
                product.StoreNumber != oldProd.StoreNumber.ToString())
            {
                return RedirectToActionPermanent("Index", "Home");
            }

            return RedirectToActionPermanent("Product", "Catalog", new {productId = product.Id });
        }

        public virtual ActionResult Redirect(string redirectActionName, string redirectControllerName)
        {
            if (_context.SiteId != KnownIds.Website.Corporate)
            {
                switch (redirectActionName)
                {
                    case "AboutUs":
                        return RedirectToActionPermanent("AboutUs", "Franchisee");
                    case "Index":
                        return RedirectToActionPermanent("CashForGear", "Franchisee");
                    case "HowItWorks":
                        return RedirectToActionPermanent("HowItWorks", "Franchisee");
                    case "NewGear":
                        return RedirectToActionPermanent("NewGear", "Franchisee");
                }
            }

            //Needed since franchisee's have this page too
            if (redirectActionName == "NewGear")
                redirectActionName = "Index";

            return RedirectToActionPermanent(redirectActionName, redirectControllerName);
        }

        public ActionResult RedirectCategoryPages(int id)
        {
            return RedirectToActionPermanent("Category", "Catalog", new {categoryId = id});
        }

        public ActionResult RedirectOldSearch(string q, int? cid, int? scid)
        {
            //These are set to handle actual links they used on the website to show sub categories.  
            //They will also catch some other searches and at least give some people results without throwing a 404
            switch (q)
            {
                case "acoustic guitar":
                    return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 48 });
                case "electric guitar":
                    return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 51 });
                case "Guitar Amps":
                    return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 54 });
                case "acoustic bass":
                    return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 50 });
                case "bass guitar":
                    return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 50 });
                case "bass amps":
                    return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 49 });
            }

			if (!cid.HasValue || !scid.HasValue) 
			{
                return RedirectToActionPermanent("Index", "Home");
			}

            switch (cid)
            {
                case 1:
                    switch (scid)
                    {
                        case 112://Vintage Guitars
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 57 });
                        case 147://Guitar Effects
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 57 });
                        case 159://Tube Amps
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 56 });
                        case 115://Speaker Cabinets
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 55 });
                    }
                    break;
                case 4:
                    switch (scid)
                    {
                        case 139://World Percussion
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 72 });
                        case 163://Snare Drums
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 71 });
                        case 144://Drum Kits
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 67 });
                        case 158://Cymbals
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 66 });
                        case 143://Electronic Drums
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 68 });
                            
                    }
                    break;
                case 6:
                    switch (scid)
                    {
                        case 124://Power Amps
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 78 });
                        case 134://DJ Gear
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 74 });
                        case 135://Effects Gear
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 75 });
                        case 162://Microphones
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 76 });
                        case 146://Mixers
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 77 });
                        case 142://Recording Gear
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 80 });
                        case 138://Speaker Cabinets
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 81 });

                    }
                    break;
                case 3:
                    switch (scid)
                    {
                        case 124://Keyboard Amps
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 61 });
                        case 134://Controllers
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 58 });
                        case 135://Drum Machines
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 60 });
                        case 162://Keyboards
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 62 });
                        case 146://Pianos
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 59 });
                        case 142://Keyboard Software
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 64 });
                        case 138://Synthesizers
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 65 });

                    }
                    break;
                case 2:
                    switch (scid)
                    {
                        case 118://Alto Saxes
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 34 });
                        case 119://Tenor Saxes
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 43 });
                        case 121://Soprano Saxes
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 42 });
                        case 128://Brass Instruments
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 37 });
                        case 114://Clarinets
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 38 });
                        case 116://Flutes
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 39 });
                        case 117://Trombones
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 44 });
                        case 122://Trumpets
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 45 });
                        case 123://Violins
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 46 });
                        case 125://Orchestral Instruments
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 40 });
                        case 140://Snare Kits
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 41 });
                        case 157://Bell Kits
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 36 });

                    }
                    break;
                case 5:
                    switch (scid)
                    {
                        case 150://Guitar Access
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 28 });
                        case 137://Instructional Material
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 33 });
                        case 151://Band Instr Access
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 25 });
                        case 152://Keyboard Access
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 29 });
                        case 154://Percussion Access
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 31 });
                        case 153://Pro Sound Access
                            return RedirectToActionPermanent("Category", "Catalog", new { categoryId = 32 });
                    }
                    break;
            }

            return RedirectToActionPermanent("Index", "Home");
        }
    }
}
