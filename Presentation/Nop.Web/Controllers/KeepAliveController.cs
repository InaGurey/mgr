﻿using System.Web.Mvc;
using System.Web.SessionState;



namespace Nop.Web.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public partial class KeepAliveController : Controller
    {
        public ActionResult Index()
        {
            return Content("I am alive!");
        }
    }
}
