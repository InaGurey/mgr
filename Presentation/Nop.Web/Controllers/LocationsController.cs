﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Services.Common;
using Nop.Web.Framework.Security;
using Treefort.Portal;
using Treefort.Portal.Queries.Geo;
using Treefort.Portal.Queries.Locations;
using Treefort.Portal.Services;

namespace Nop.Web.Controllers
{
    public class LocationsController : BaseNopController
    {
        private readonly IWorkContext _context;
        private readonly IIndexViewModelQuery _indexViewModelQuery;
        private readonly IHoursViewModelQuery _hoursViewModelQuery;
        private readonly ISearchViewModelQuery _searchViewModelQuery;
        private readonly IStoresNearYouViewModelQuery _storesNearYouViewModelQuery;
        private readonly ILocationService _locationService;
        private readonly IGeoListViewModelQuery _geoListViewModelQuery;
        private readonly IMobileDeviceHelper _mobileDeviceHelper;
        private readonly IConstantContactLocationsViewModelQuery _constantContactLocationsViewModelQuery;

        public LocationsController(IWorkContext context, IIndexViewModelQuery indexViewModelQuery,
            IHoursViewModelQuery hoursViewModelQuery, IStoresNearYouViewModelQuery storesNearYouViewModelQuery,
            IGeoListViewModelQuery geoListViewModelQuery, ISearchViewModelQuery searchViewModelQuery,
            ILocationService locationService, IMobileDeviceHelper mobileDeviceHelper,
            IConstantContactLocationsViewModelQuery constantContactLocationsViewModelQuery)
        {
            _context = context;
            _indexViewModelQuery = indexViewModelQuery;
            _hoursViewModelQuery = hoursViewModelQuery;
            _storesNearYouViewModelQuery = storesNearYouViewModelQuery;
            _geoListViewModelQuery = geoListViewModelQuery;
            _searchViewModelQuery = searchViewModelQuery;
            _locationService = locationService;
            _mobileDeviceHelper = mobileDeviceHelper;
            _constantContactLocationsViewModelQuery = constantContactLocationsViewModelQuery;
        }

        [NopHttpsRequirement(SslRequirement.No)]
        [OutputCache(Duration = 900)]
        public ActionResult Index()
        {
            if (_context.SiteId != KnownIds.Website.Corporate) { return RedirectToCorporateVersionOfUrl(); }
            var model = _indexViewModelQuery.Execute();

            var offset = 7 - (int)DateTime.Today.DayOfWeek;
            var baseDays =
                new List<DayOfWeek>((DayOfWeek[])Enum.GetValues(typeof(DayOfWeek))).OrderBy(
                    x => ((int)x + offset) % 7).ToList();

            foreach (var l in model.Locations)
            {
                l.Hours = GetStoreHours(l.Id, baseDays);
            }
            return View(model);
        }

        [NonAction]
        protected SearchViewModel GetListModel(SearchViewModel sv, string region)
        {

            var model = string.IsNullOrEmpty(region)
                ? _searchViewModelQuery.Execute(sv)
                : _searchViewModelQuery.GetyByRegion(region);

            if (model.Locations.All(x => x.Country == "CA"))
            {
                model.Units = "KM";
                foreach (var l in model.Locations)
                {
                    l.Distance = l.Distance * 1.609344;
                }
            }
            else
            {
                model.Units = "Miles";
            }

            model.SearchedLocation = sv.SearchedLocation;

            if (!string.IsNullOrEmpty(region))
                model.SearchedLocation = region;

            var ip = Request.UserHostAddress;
            if (Request.IsLocal || (ip != null && ip.StartsWith("192.168.1.")))
            {
                ip = "173.11.59.141";
                ip = "24.36.0.211";
            }
            ViewBag.CountryCode = _locationService.GetCountryCodeByIpAddress(ip);


            var offset = 7 - (int)DateTime.Today.DayOfWeek;
            var baseDays =
                new List<DayOfWeek>((DayOfWeek[])Enum.GetValues(typeof(DayOfWeek))).OrderBy(
                    x => ((int)x + offset) % 7).ToList();

            foreach (var l in model.Locations)
            {
                l.Hours = GetStoreHours(l.Id, baseDays);
            }

            return model;
        }

        [NonAction]
        protected IList<LocationModel.HoursLine> GetStoreHours(int locationId, IList<DayOfWeek> baseDays)
        {
            var hours = _hoursViewModelQuery.Execute(locationId, "htt");
            var storeHours = baseDays.Select(x =>
            {
                var line = new LocationModel.HoursLine
                {
                    DayName = x == DateTime.Today.DayOfWeek ? "Open Today" : x.ToString()
                };
                switch (x)
                {
                    case DayOfWeek.Monday:
                        line.Open = hours.monopen;
                        line.Close = hours.monclose;
                        break;
                    case DayOfWeek.Tuesday:
                        line.Open = hours.tueopen;
                        line.Close = hours.tueclose;
                        break;
                    case DayOfWeek.Wednesday:
                        line.Open = hours.wedopen;
                        line.Close = hours.wedclose;
                        break;
                    case DayOfWeek.Thursday:
                        line.Open = hours.thuopen;
                        line.Close = hours.thuclose;
                        break;
                    case DayOfWeek.Friday:
                        line.Open = hours.friopen;
                        line.Close = hours.friclose;
                        break;
                    case DayOfWeek.Saturday:
                        line.Open = hours.satopen;
                        line.Close = hours.satclose;
                        break;
                    case DayOfWeek.Sunday:
                        line.Open = hours.sunopen;
                        line.Close = hours.sunclose;
                        break;
                }
                return line;
            }).ToList();

            return storeHours;
        }

        public virtual ActionResult List(SearchViewModel sv, string region)
        {
            var model = GetListModel(sv, region);

            return View(model);
        }

        public virtual ActionResult ListAsync(SearchViewModel sv, string region)
        {
            var model = GetListModel(sv, region);

            return Json(model);
        }

        public ActionResult FindLocationBox()
        {
            return PartialView("_FindLocationBox");
        }

        public ActionResult ConstantContactForm()
        {
            return PartialView("_ConstantContactForm");
        }

        [OutputCache(Duration=900)]
        public ActionResult ConstantContactLocations()
        {
            var model = _constantContactLocationsViewModelQuery.Execute();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(int id, double? distance = null)
        {
            var loc = _locationService.GetSingleLocation(id);
            loc.DisplayWebsiteLink = true;
            loc.Distance = distance;
            return View(loc);
        }

        [ChildActionOnly]
        public ActionResult LocationBox(bool useLocationText = false)
        {
            var loc = _locationService.GetSingleLocation(Website.Account.PrimaryLocation.Id);
            loc.DisplayWebsiteLink = false;


            var offset = 7 - (int)DateTime.Today.DayOfWeek;
            var baseDays =
                new List<DayOfWeek>((DayOfWeek[])Enum.GetValues(typeof(DayOfWeek))).OrderBy(
                    x => ((int)x + offset) % 7).ToList();

            loc.Hours = GetStoreHours(loc.Id, baseDays);

            if (useLocationText)
            {
                loc.Name = WebsiteSettings.WebsiteOptions.LocationTextLine1;
            }

            var isMobile = MobileService.IsMobile(System.Web.HttpContext.Current.Request,
                _mobileDeviceHelper.CustomerDontUseMobileVersion());
            var viewName = isMobile ? "_LocationBox" : "_BigLocationBox";

            return PartialView(viewName, loc);
        }

        public virtual ActionResult Hours(int id)
        {
            var model = _hoursViewModelQuery.Execute(id);
            return PartialView("_Hours", model);
        }

        public virtual ActionResult Geo(double? lat, double? ln)
        {
            var model = _geoListViewModelQuery.Execute(lat, ln);
            return PartialView("_UserLocationList", model);
        }

        public virtual ActionResult Postal(string code)
        {
            var model = _storesNearYouViewModelQuery.Execute(code);
            return PartialView("_UserLocationList", model);
        }

        //public virtual ActionResult GeoStoresNear(double? lat, double? ln)
        //{
        //    var model = _geoListViewModelQuery.Execute(lat, ln);
        //    return PartialView(MVC.Location.Views._StoresNearYouILocation, model);
        //}

        //public virtual ActionResult PostalStoresNear(string code)
        //{
        //    var model = _storesNearYouViewModelQuery.Execute(code);
        //    return PartialView(MVC.Location.Views._StoresNearYouILocation, model);
        //}

    }
}
