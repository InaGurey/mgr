﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Extensions
{
    public class ImageResizeCommands
    {
        public enum ScaleCommand
        {
            None,
            Both,
            UpscaleOnly,
            DownscaleOnly
        }

        public enum FlipCommand
        {
            None,
            Horizontal,
            Vertical,
            Both
        }

        public enum ImageFormat
        {
            None,
            Jpg,
            Png,
            Gif
        }

        public struct CropCoordinates
        {
            public int x1;
            public int y1;
            public int x2;
            public int y2;

            public bool HasValues()
            {
                return x1 != null && y1 != null && x2 != null && y2 != null && x1 < x2 && y1 < y2;
            }

            public override string ToString()
            {
                return string.Format("({0},{1},{2},{3})", x1, y1, x2, y2);
            }
        }

        public struct Sharpen
        {
            public double radius;
            public double amount;

            public bool HasValues()
            {
                return radius != null && amount != null && radius > 0 && amount > 0;
            }

            public override string ToString()
            {
                return string.Format("{0},{1}", radius, amount);
            }
        }

        public class ResizeParams
        {
            public int? Width { get; set; }
            public int? Height { get; set; }
            public int? MaxWidth { get; set; }
            public int? MaxHeight { get; set; }
            public CropCoordinates Crop { get; set; }
            public bool AutoCrop { get; set; }
            public decimal? Rotate { get; set; }
            public string BackgroundColor { get; set; }
            public bool Stretch { get; set; }
            public ScaleCommand Scale { get; set; }
            public FlipCommand Flip { get; set; }
            public FlipCommand SourceFlip { get; set; }
            public int? PaddingWidth { get; set; }
            public string PaddingColor { get; set; }
            public int? BorderWidth { get; set; }
            public string BorderColor { get; set; }
            public ImageFormat Format { get; set; }
            public int? Colors { get; set; }
            public int? Frame { get; set; }
            public int? Page { get; set; }
            public int? Quality { get; set; }
            public Sharpen Sharpen { get; set; }

            public bool HasValues
            {
                get { return this.ToString().Length > 0; }
            }

            //public ResizeParams() 
            //{
            //    Scale = ScaleCommand.None;
            //    Flip = FlipCommand.None;
            //    SourceFlip = FlipCommand.None;
            //    Format = ImageFormat.None;
            //}

            public override string ToString()
            {
                var list = new List<string>();

                if (Width.HasValue)
                    list.Add(string.Format("width={0}", Width.Value));

                if (Height.HasValue)
                    list.Add(string.Format("height={0}", Height.Value));

                if (MaxWidth.HasValue)
                    list.Add(string.Format("maxwidth={0}", MaxWidth.Value));

                if (MaxHeight.HasValue)
                    list.Add(string.Format("maxheight={0}", MaxHeight.Value));

                if (AutoCrop)
                    list.Add(("crop=auto"));
                else if (Crop.HasValues())
                    list.Add(string.Format("crop={0}", Crop));

                if (Rotate.HasValue)
                    list.Add(string.Format("rotate={0}", Rotate.Value));

                if (!string.IsNullOrEmpty(BackgroundColor))
                    list.Add(string.Format("bgcolor={0}", HttpUtility.UrlEncode(BackgroundColor)));

                if (Stretch)
                    list.Add(("stretch=fill"));

                if (Scale != ScaleCommand.None)
                    list.Add(string.Format("scale={0}", Scale.ToString().ToLower()));

                if (Flip != FlipCommand.None)
                    list.Add(string.Format("flip={0}", FlipCommandToString(Flip)));

                if (SourceFlip != FlipCommand.None)
                    list.Add(string.Format("sourceflip={0}", FlipCommandToString(SourceFlip)));

                if (PaddingWidth.HasValue)
                    list.Add(string.Format("paddingWidth={0}", PaddingWidth.Value));

                if (!string.IsNullOrEmpty(PaddingColor))
                    list.Add(string.Format("paddingColor={0}", HttpUtility.UrlEncode(PaddingColor)));

                if (BorderWidth.HasValue)
                    list.Add(string.Format("borderWidth={0}", BorderWidth.Value));

                if (!string.IsNullOrEmpty(BorderColor))
                    list.Add(string.Format("borderColor={0}", HttpUtility.UrlEncode(BorderColor)));

                if (Format != ImageFormat.None)
                    list.Add(string.Format("format={0}", Format.ToString().ToLower()));

                if (Colors.HasValue)
                    list.Add(string.Format("colors={0}", Colors.Value));

                if (Frame.HasValue)
                    list.Add(string.Format("frame={0}", Frame.Value));

                if (Page.HasValue)
                    list.Add(string.Format("page={0}", Page.Value));

                if (Quality.HasValue)
                    list.Add(string.Format("quality={0}", Quality.Value));

                if (this.Sharpen.HasValues())
                    list.Add(string.Format("sharpen={0}", this.Sharpen));

                if (list.Count > 0)
                    return string.Join("&", list.ToArray());

                return string.Empty;
            }

            private string FlipCommandToString(FlipCommand fc)
            {
                switch (fc)
                {
                    case FlipCommand.Both: return "both";
                    case FlipCommand.Horizontal: return "h";
                    case FlipCommand.Vertical: return "v";
                    default: return string.Empty;
                }
            }



            //width
            //height – force the width and/or height to certain dimensions. Whitespace will be added if the aspect ratio is different.
            //maxwidth
            //maxheight – fit the image within the specified bounds. (Most often used)
            //crop=auto – Crop the image the the size specified by width and height. Centers and minimally crops to preserve aspect ratio.
            //crop=(x1,y1,x2,y2) – Crop the image to the specified rectangle on the source image. You can use negative coordinates to specifiy bottom-right relative locations.
            //rotate=degress – Rotate the image.
            //bgcolor=color name| hex code (6-char). Sets the background/whitespace color.
            //stretch=fill – Stretches the image to width and height if both are specified. This is the only way to lose aspect ratio.
            //scale=both|upscaleonly|downscaleonly – By default, images are never upscaled. Use &scale=both to grow an image.
            //flip=h|v|both – Flips the image after resizing.
            //sourceFlip=h|v|both – Flips the source image before resizing/rotation.
            //paddingWidth=px
            //paddingColor=color|hex. paddingColor defaults to bgcolor, which defaults to white.
            //borderWidth=px
            //borderColor=color|hex.
            //format=jpg|png|gif
            //colors=2-255 – Control the palette size of PNG and GIF images. If omitted, PNGs will be 24-bit.
            //frame=x – Choose which frame of an animated GIF to display.
            //page=x – Choose which page of a multi-page TIFF document to display.
            //quality=1-100

        }
    }
}