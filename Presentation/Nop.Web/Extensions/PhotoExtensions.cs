﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Extensions
{
    public static class PhotoExtensions
    {
        public static IHtmlString CroppedPhoto(this HtmlHelper helper, string src, int width, int height, string alt, string id)
        {
            var p = new ImageResizeCommands.ResizeParams
            {
                AutoCrop = true,
                Width = width,
                Height = height
            };

            return Image(helper, src, width, height, p, alt, id);
        }

        public static IHtmlString BoundedImage(this HtmlHelper helper, string src, int maxwidth, int maxheight, string alt, string id)
        {
            var p = new ImageResizeCommands.ResizeParams
            {
                MaxWidth = maxwidth,
                MaxHeight = maxheight
            };

            return Image(helper, src, p, alt, id);
        }

        public static IHtmlString Image(this HtmlHelper helper, string src, ImageResizeCommands.ResizeParams resizeParams, string alt = "", string id = "")
        {
            return Image(helper, src, null, null, resizeParams, alt, id);
        }

        public static IHtmlString Image(this HtmlHelper helper, string src, int? width, int? height, ImageResizeCommands.ResizeParams resizeParams, string alt = "", string id = "")
        {
            if (resizeParams.HasValues)
            {
                var sep = src.Contains("?") ? "&" : "?";
                src = src + sep + resizeParams.ToString();
            }

            src = Regex.Replace(src, @"^\/users\/(?<user>.+?)\/", "http://rcfile.treefortrack.com/${user}/", RegexOptions.IgnoreCase);

            TagBuilder tagBuilder = new TagBuilder("img");
            tagBuilder.MergeAttribute("src", src);

            var html = string.Format("<img src=\"{0}\" ", src);


            var size = GetPhotoSize(width, height, resizeParams);

            if (size.Width > 0)
            {
                tagBuilder.MergeAttribute("width", size.Width.ToString());
            }

            if (size.Height > 0)
            {
                tagBuilder.MergeAttribute("height", size.Height.ToString());
            }

            if (!string.IsNullOrEmpty(alt))
            {
                tagBuilder.MergeAttribute("alt", alt);
            }

            if (!string.IsNullOrEmpty(id))
            {
                tagBuilder.MergeAttribute("id", id);
            }

            return new HtmlString(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }



        public static IHtmlString Image(this UrlHelper helper, string src, ImageResizeCommands.ResizeParams resizeParams)
        {
            if (resizeParams.HasValues)
            {
                var sep = src.Contains("?") ? "&" : "?";
                src = src + sep + resizeParams.ToString();
            }

            src = Regex.Replace(src, @"^\/users\/(?<user>.+?)\/", "http://rcfile.treefortrack.com/${user}/", RegexOptions.IgnoreCase);

            return new HtmlString(src);
        }


        public static IHtmlString Image(this HtmlHelper helper, string src, int? width, int? height, string alt = "")
        {

            TagBuilder tagBuilder = new TagBuilder("img");
            src = Regex.Replace(src, @"^\/users\/(?<user>.+?)\/", "http://rcfile.treefortrack.com/${user}/", RegexOptions.IgnoreCase);


            tagBuilder.MergeAttribute("src", src);


            var html = string.Format("<img src=\"{0}\" ", src);

            if (width.HasValue)
            {
                tagBuilder.MergeAttribute("width", width.Value.ToString());
            }

            if (height.HasValue)
            {
                tagBuilder.MergeAttribute("height", height.Value.ToString());
            }

            if (!string.IsNullOrEmpty(alt))
            {
                tagBuilder.MergeAttribute("alt", alt);
            }

            return new HtmlString(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }

        private static Size GetPhotoSize(int? width, int? height, ImageResizeCommands.ResizeParams p)
        {
            var s = new Size();
            s.Height = (height.HasValue ? height.Value : p.Height.HasValue ? p.Height.Value : 0) + (p.BorderWidth.GetValueOrDefault(0) * 2);
            s.Width = (width.HasValue ? width.Value : p.Width.HasValue ? p.Width.Value : 0) + (p.BorderWidth.GetValueOrDefault(0) * 2);
            return s;
        }

    }
}