﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Extensions
{
    public static class MapUrlExtension
    {
        public static string GoogleMap(this UrlHelper helper, Treefort.Portal.Queries.Geo.LocationModel a)
        {
            return GoogleDirections(helper, a.AddressLine1, a.City, a.Region, a.PostalCode, a.Directions);
        }

        //They do not want it to be Directions even though it is "directions" so changing it to q
        public static string GoogleDirections(this UrlHelper helper, Treefort.Portal.Queries.Geo.LocationModel a)
        {
            return GoogleDirections(helper, a.AddressLine1, a.City, a.Region, a.PostalCode, a.Directions);
        }

        public static string GoogleDirections(this UrlHelper helper, Treefort.Portal.Queries.Locations.GenericLocation a)
        {
            return GoogleDirections(helper, a.Address1, a.City, a.Region, a.PostalCode, a.Directions);
        }

        public static string GoogleDirections(this UrlHelper helper, Treefort.Portal.Queries.Franchisee.Location.GenericLocation a)
        {
            return GoogleDirections(helper, a.Address1, a.City, a.Region, a.PostalCode, a.Directions);
        }

        public static string GoogleDirections(this UrlHelper helper, string address1, string city, string region, string postalCode, string directions = null)
        {
            var qs = string.IsNullOrWhiteSpace(directions) ?
                HttpUtility.UrlEncode(string.Format("Music Go Round {0} {1} {2} {3}", address1, city, region, postalCode).Trim()) :
                HttpUtility.UrlEncode(directions);
            return string.Format("http://maps.google.com/?q={0}", qs);
        }
    }
}
