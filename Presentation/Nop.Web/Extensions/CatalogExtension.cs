﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Treefort.Common;
using WinmarkFranchise.Domain.Ecommerce;

namespace Nop.Web.Extensions
{
    public static class CatalogExtension
    {
        private const string CATEGORY_BRANDLISTID_KEY = "Nop.category.brandlistid-{0}";
        private const string MANUFACTURER_BRANDLISTID_KEY = "Nop.manufacturer.brandlistids-{0}";

        private static readonly IStaticData _staticData;
        private static readonly IOutOfProcessCacheManager _cacheManager;

        static CatalogExtension()
        {
            _staticData = EngineContext.Current.Resolve<IStaticData>();
            _cacheManager = EngineContext.Current.Resolve<IOutOfProcessCacheManager>();
        }

        public static int GetBrandListId(this string categoryDrsCode)
        {
            if (string.IsNullOrEmpty(categoryDrsCode)) return -1;

            var key = string.Format(CATEGORY_BRANDLISTID_KEY, categoryDrsCode);
            var brandListId = _cacheManager.Get(key, 120, () =>
            {
                var buyGroup = _staticData.BuyGroups.FirstOrDefault(x => x.BrandListId != 0 && x.CategoryCode == categoryDrsCode);

                if (buyGroup != null) return buyGroup.BrandListId;

                var cachedId = -1;
                var drsCat = _staticData.Categories.FirstOrDefault(x => x.Code == categoryDrsCode);
                if (drsCat == null) return -1;
                else if (!string.IsNullOrEmpty(drsCat.ParentCode))
                {
                    return drsCat.ParentCode.GetBrandListId();
                }
                else if (drsCat.ParentCode == null)
                {
                    var children = _staticData.Categories.Where(x => x.ParentCode == categoryDrsCode);
                    foreach (var sc in children)
                    {
                        buyGroup = _staticData.BuyGroups.FirstOrDefault(x => x.BrandListId != 0 && x.CategoryCode == sc.Code);
                        if (buyGroup != null) break;
                    }
                }

                return buyGroup.IfNotNull(x => x.BrandListId, -1);
            });

            return brandListId;
        }

        public static IList<int> GetBrandListIds(this Manufacturer manufacturer)
        {
            if (manufacturer.DrsId == null) return null;

            var key = string.Format(MANUFACTURER_BRANDLISTID_KEY, manufacturer.DrsId);
            var brandListIds = _cacheManager.Get(key, 120, () =>
            {
                return
                    _staticData.BrandChildren.Where(x => x.BrandId == manufacturer.DrsId)
                        .Select(x => x.BrandListId)
                        .Distinct()
                        .ToList();
            });

            return brandListIds;
        }

        public static IList<int> GetBrandDrsIds(this int brandListId)
        {
            return _staticData.BrandChildren.Where(x => x.BrandListId == brandListId).Select(x => x.BrandId).ToList();
        }
    }
}