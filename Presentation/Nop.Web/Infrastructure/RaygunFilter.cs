﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mindscape.Raygun4Net.Messages;
using Treefort.Common;
using WinmarkFranchise.Raygun;


namespace Nop.Web.Infrastructure
{
    public class MgrRaygunFilters : NopCommonFilters
    {
        public MgrRaygunFilters() : this(Enumerable.Empty<Func<RaygunMessage, bool>>()) { }

        public MgrRaygunFilters(IEnumerable<Func<RaygunMessage, bool>> filters)
            : base(filters.Concat(new Func<RaygunMessage, bool>[] {
            }))
        { }
    }
}