﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FluentValidation.Mvc;
using Mindscape.Raygun4Net;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Infrastructure;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Web.App_Start;
using Nop.Web.Controllers;
using Nop.Web.Framework;
using Nop.Web.Framework.EmbeddedViews;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Themes;
using Nop.Web.Infrastructure;
using Nop.Web.Models.Catalog;
using StackExchange.Profiling;
using Treefort.Portal;
using WinmarkFranchise.Raygun;


namespace Nop.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication, IRaygunApplication
    {
        private static readonly RaygunClient _raygunClient = new RaygunClient().AttachFilters(new MgrRaygunFilters());
        public RaygunClient GenerateRaygunClient() { return _raygunClient; }


        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //do not register HandleErrorAttribute. use classic error handling mode
            //filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("apple-touch-icon.png/{*pathInfo}");
            routes.IgnoreRoute("apple-touch-icon-precomposed.png/{*pathInfo}");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("sifr/{*pathInfo}");
			routes.IgnoreRoute("content/files");
            
            //register custom routes (plugins, etc)
            var routePublisher = EngineContext.Current.Resolve<IRoutePublisher>();
            routePublisher.RegisterRoutes(routes);

            routes.MapRoute(
                "Internal Server Error", // Route name
                "error/internal", // URL with parameters
                new { controller = "Errors", action = "Error", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Error Not Found", // Route name
                "error/not-found", // URL with parameters
                new { controller = "Errors", action = "NotFound", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Error", // Route name
                "error/{action}", // URL with parameters
                new { controller = "Errors", action = "NotFound", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Sell Your Gear", // Route name
                "sell-us-your-gear/{action}", // URL with parameters
                new { controller = "SellYourGear", action = "Index", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "About Us", // Route name
                "about-us", // URL with parameters
                new { controller = "Franchisee", action = "AboutUs", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Additional Page",
                "promotions/{pageId}/{rewriteUrl}",
                new {controller = "Franchisee", action = "AdditionalPage", rewriteUrl = UrlParameter.Optional},
                new[] {"Nop.Web.Controllers"}
                );

            routes.MapRoute(
                "Cash For Gear", // Route name
                "cash-for-gear", // URL with parameters
                new { controller = "Franchisee", action = "CashForGear", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "New Gear", // Route name
                "new-gear", // URL with parameters
                new { controller = "Franchisee", action = "NewGear", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Community", // Route name
                "community-links", // URL with parameters
                new { controller = "Franchisee", action = "CommunityLinks", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Promotions", // Route name
                "promotions", // URL with parameters
                new { controller = "Franchisee", action = "Promotions", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Most Wanted", // Route name
                "most-wanted", // URL with parameters
                new { controller = "Franchisee", action = "MostWanted", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Jobs", // Route name
                "jobs", // URL with parameters
                new { controller = "Franchisee", action = "Jobs", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "What To Expect", // Route name
                "what-to-expect", // URL with parameters
                new { controller = "Home", action = "WhatToExpect", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Lessons", // Route name
                "lessons", // URL with parameters
                new { controller = "Franchisee", action = "Lessons", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Repairs", // Route name
                "repairs", // URL with parameters
                new { controller = "Franchisee", action = "Repairs", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Special Promo", // Route name
                "special-promotion", // URL with parameters
                new { controller = "Franchisee", action = "SpecialPromotion", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Cyber Monday", // Route name
                "special-events", // URL with parameters
                new { controller = "Home", action = "SpecialEvents", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "SpecialPromotionalEvent", // Route name
                "special-promotional-event", // URL with parameters
                new { controller = "Franchisee", action = "SpecialPromotionalEvent", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Shop Online", // Route name
                "shop-online", // URL with parameters
                new { controller = "Catalog", action = "ShopOnline" }, new[] { "Nop.Web.Controllers" }
            );

            routes.MapRoute(
                "Robots", "robots.txt", new {controller = "Home", action = "Robots"}, new[] {"Nop.Web.Controllers"});

            routes.MapRoute("TestMobile", "test-mobile", new { controller = "Home", action = "TestMobile", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("Franchise Movies", "franchisemovies", new { controller = "Own", action = "FranchiseMovies", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Return Policy","return-policy",new { controller = "Home", action = "ReturnPolicy", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" } );
            routes.MapRoute("Contact Us", "contact-us", new { controller = "Home", action = "ContactUs", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("FAQ", "faq", new { controller = "Home", action = "Faq", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Conditions of Use", "conditions-of-use", new { controller = "Home", action = "ConditionsOfUse", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Privacy Policy", "privacy-policy", new { controller = "Home", action = "PrivacyPolicy", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Conference Vendors", "conference_vendors", new { controller = "Home", action = "ConferenceVendors", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Shipping Policy", "shipping-policy", new { controller = "Home", action = "ShippingPolicy", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("How It Works", "how-it-works", new { controller = "Home", action = "HowItWorks", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Press Detail", "press/detail/{urlSlug}", new { controller = "Press", action = "Detail", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Franchisee Testimonials", "own/franchisee_testimonials", new { controller = "Own", action = "FranchiseeTestimonials", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("Old Sitemap route", "sitemap.ashx", new { controller = "Common", action = "SitemapSeo", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });

            RegisterOldRoutes(routes);


			// TODO: create action constraint
	        var controllerConstraint = "(" + string.Join("|", GetControllerNames()) + ")";
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
				new { controller = controllerConstraint },
                new[] { "Nop.Web.Controllers" }
            );

			routes.MapRoute("NotFound", "{*url}", new { controller = "Errors", action = "NotFound" }, new[] { "Nop.Web.Controllers" });
        }

		static private IEnumerable<string> GetControllerNames()
		{
			return
				typeof (MvcApplication).Assembly.GetTypes()
										.Where(t => typeof(Controller).IsAssignableFrom(t))
										.Select(t => t.Name.Substring(0, t.Name.LastIndexOf("Controller")));
		}

        protected void Application_Start()
        {
            //initialize engine context
            EngineContext.Initialize(false);

            bool databaseInstalled = DataSettingsHelper.DatabaseIsInstalled();

            //set dependency resolver
            var dependencyResolver = new NopDependencyResolver();
            DependencyResolver.SetResolver(dependencyResolver);

            //model binders
            ModelBinders.Binders.Add(typeof(BaseNopModel), new NopModelBinder());

            if (databaseInstalled)
            {
                //remove all view engines
                ViewEngines.Engines.Clear();
                //except the themeable razor view engine we use
                ViewEngines.Engines.Add(new ThemeableRazorViewEngine());
            }

            //Add some functionality on top of the default ModelMetadataProvider
            ModelMetadataProviders.Current = new NopMetadataProvider();

            //Registering some regular mvc stuf
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            //RouteDebug.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);

            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            ModelValidatorProviders.Providers.Add(new FluentValidationModelValidatorProvider(new NopValidatorFactory()));

            //register virtual path provider for embedded views
            var embeddedViewResolver = EngineContext.Current.Resolve<IEmbeddedViewResolver>();
            var embeddedProvider = new EmbeddedViewVirtualPathProvider(embeddedViewResolver.GetEmbeddedViews());
            HostingEnvironment.RegisterVirtualPathProvider(embeddedProvider);

            //mobile device support
            if (databaseInstalled)
            {
                if (EngineContext.Current.Resolve<StoreInformationSettings>().MobileDevicesSupported)
                {
                    //Enable the mobile detection provider (if enabled)
                    HttpCapabilitiesBase.BrowserCapabilitiesProvider = new FiftyOne.Foundation.Mobile.Detection.MobileCapabilitiesProvider();
                }
                else
                {
                    //set BrowserCapabilitiesProvider to null because 51Degrees assembly always sets it to MobileCapabilitiesProvider
                    //which does not support our browserCaps.config file
                    HttpCapabilitiesBase.BrowserCapabilitiesProvider = null;
                }
            }

            //MicroData Configuration-embeded by Nuradin.
            ModelMetadataProviders.Current = new ProductDetailsModel.ProductVariantModel.MicrodataModelMetadataProvider();


            //start scheduled tasks
            if (databaseInstalled)
            {
                TaskManager.Instance.Initialize();
                TaskManager.Instance.Start();
            }

            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SimpleHoneypot.Core.Honeypot.SetCssClassName("honeypot-input");
        }

        private const string ignoreMobileKey = "ignoreMobile";
        private const string ignoreMobileValue = "1";

        private string MobileRedirectPath(string requestedPath)
        {
            switch (requestedPath) {
                case "/":
                    return "/";

                case "/sell-us-your-gear":
                    return "/Home/SellGear";

                case "/Own":
                    return "/Home/Own";

                default:
                    return null;
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["ShowMiniProfiler"] == "true") {
				MiniProfiler.Start();
			}

			if (Request.Url.AbsolutePath == "/lbcheck.html") { return; }

            //// mobile redirect
            //bool ignoreMobile = !string.IsNullOrEmpty(Context.Request.QueryString[ignoreMobileKey]);
            //if (!ignoreMobile) {
            //    var cookie = Context.Request.Cookies[ignoreMobileKey];
            //    ignoreMobile = cookie != null && cookie.Value == ignoreMobileValue;
            //}
            //else {
            //    var ignoreMobileCookie = new HttpCookie(ignoreMobileKey);
            //    ignoreMobileCookie.Value = ignoreMobileValue;
            //    Context.Response.Cookies.Add(ignoreMobileCookie);
            //}

            //var mobileRedirectPath = MobileRedirectPath(Request.Url.AbsolutePath);
            //if (
            //    IsMobileBrowser() && 
            //    !ignoreMobile && 
            //    mobileRedirectPath != null &&
            //    !Request.Url.IsLoopback && 
            //    !Request.Url.Host.StartsWith("m."))
            //{
            //    UriBuilder builder = new UriBuilder(Request.Url);
            //    builder.Host = "m." + (Request.Url.Host.StartsWith("www.") ? Request.Url.Host.Substring(4) : Request.Url.Host);
            //    builder.Path = mobileRedirectPath;
            //    Response.Redirect(builder.ToString());
            //    return;
            //}

            // www canonical redirect
            if (ConfigurationManager.AppSettings["RedirectToWww"] != "false")
            {
				if (!Request.Url.IsLoopback &&
                    !Request.Url.Host.StartsWith("www.") &&
                    !Request.Url.Host.StartsWith("m.") &&
                    !Request.Url.Host.Contains("mgrstore.") &&
					!Request.Url.Host.Contains("treefort"))
				{
					UriBuilder builder = new UriBuilder(Request.Url);
					builder.Host = "www." + Request.Url.Host;
					var codeText = ConfigurationManager.AppSettings["RedirectToWwwCode"];
					int code;
					HttpStatusCode status;
					if (Enum.TryParse(codeText, out status)) {
						code = (int)status;
					}
					else if (!int.TryParse(codeText, out code)) { 
						code = (int)HttpStatusCode.Redirect;
					}
					Response.StatusCode = code;
					Response.AddHeader("Location", builder.ToString());
					Response.End();
				    return;
				}
			}
        }

        // https://gist.github.com/dalethedeveloper/1503252
        private const string MobileDetectionRegex =
            @"Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune";

        private bool IsMobileBrowser()
        {
            string u = Request.ServerVariables["HTTP_USER_AGENT"];
	        if (u == null || u.IndexOf("ipad", StringComparison.InvariantCultureIgnoreCase) >= 0) { return false; }
            Regex b = new Regex(MobileDetectionRegex, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return b.IsMatch(u);
        }


        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["ShowMiniProfiler"] == "true") {
                //stop as early as you can, even earlier with MvcMiniProfiler.MiniProfiler.Stop(discardResults: true);
                MiniProfiler.Stop();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        { 
            //we don't do it in Application_BeginRequest because a user is not authenticated yet
            SetWorkingCulture();
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
			var exception = Server.GetLastError();

			var httpException = exception as HttpException;
			if (httpException != null) {
				var statusCode = httpException.GetHttpCode();
				if ((statusCode == 404) || ((statusCode == 400) && httpException.Message.Contains("Request.Path"))) {
					Response.Clear();
					Server.ClearError();
					var routeData = new RouteData();
					routeData.Values["controller"] = "Errors";
					routeData.Values["action"] = "NotFound";
					Response.StatusCode = statusCode;
					Response.TrySkipIisCustomErrors = true;
					IController errorsController = DependencyResolver.Current.GetService<Controllers.ErrorsController>();
					var wrapper = new HttpContextWrapper(Context);
					var rc = new RequestContext(wrapper, routeData);
					errorsController.Execute(rc);
					return;
				}
			}

            //disable compression (if enabled). More info - http://stackoverflow.com/questions/3960707/asp-net-mvc-weird-characters-in-error-page
            //log error
            LogException(exception);
        }
        
        protected void EnsureDatabaseIsInstalled()
        {
            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            string installUrl = string.Format("{0}install", webHelper.GetStoreLocation());
            if (!webHelper.IsStaticResource(this.Request) &&
                !DataSettingsHelper.DatabaseIsInstalled() &&
                !webHelper.GetThisPageUrl(false).StartsWith(installUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                this.Response.Redirect(installUrl);
            }
        }

        protected void SetWorkingCulture()
        {
            if (!DataSettingsHelper.DatabaseIsInstalled())
                return;

            var webHelper = EngineContext.Current.Resolve<IWebHelper>();
            if (webHelper.IsStaticResource(this.Request))
                return;

            //keep alive page requested (we ignore it to prevnt creating a guest customer records)
            string keepAliveUrl = string.Format("{0}keepalive", webHelper.GetStoreLocation());
            if (webHelper.GetThisPageUrl(false).StartsWith(keepAliveUrl, StringComparison.InvariantCultureIgnoreCase))
                return;

			var culture = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentCulture = culture;
			Thread.CurrentThread.CurrentUICulture = culture;
        }

        protected void LogException(Exception exc)
        {
            if (exc == null)
                return;
            
            if (!DataSettingsHelper.DatabaseIsInstalled())
                return;
            
            try
            {
                var logger = EngineContext.Current.Resolve<ILogger>();
                var workContext = EngineContext.Current.Resolve<IWorkContext>();
                logger.Error(exc.Message, exc, workContext.CurrentUser);
            }
            catch (Exception)
            {
                //don't throw new exception if occurs
            }
        }

		// http://stackoverflow.com/a/4850886/62600
		public override string GetVaryByCustomString(HttpContext context, string custom) {
            switch ((custom ?? "").ToLower()) {
                case "host":
                    var host = context.IfNotNull(c => c.Request.IfNotNull(r => r.Url.IfNotNull(u => u.Host)));
		            return string.IsNullOrEmpty(host) ||
		                   host.Contains("treefort") ||
		                   host.EndsWith("preview.com") ||
		                   context.IfNotNull(c => c.Request.IfNotNull(r => r.IsLocal, false), false)
		                       ? Guid.NewGuid().ToString()
		                       : host;

                default: return "";
            }
		}

        //Added Configuration for Microdata -Nuradin.
       // protected void Application_Start()


        private static void RegisterOldRoutes(RouteCollection routes)
        {
            //301 Redirects
            routes.MapRoute("Old Product Detail", "detail.aspx", new { controller = "Errors", action = "RedirectProducts", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("Old Product", "product.aspx", new { controller = "Errors", action = "RedirectProducts", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("email-store.aspx", "email-store.aspx", new { controller = "Errors", action = "RedirectProducts", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("the-brand.aspx", "the-brand.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("own-a-music-go-round.aspx", "own-a-music-go-round.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("own a store link", "own_a_store", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("investment-requirements.aspx", "investment-requirements.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Investment", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("franchise-support.aspx", "franchise-support.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Support", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("request-information.aspx", "request-information.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Qualify", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("territories.aspx", "territories.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Territories", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("franchisee_testimonials.aspx", "franchisee_testimonials.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Testimonials", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("franchise_faqs.aspx", "franchise_faqs.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "FAQ", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("used-guitars.aspx", "used-guitars.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = KnownIds.Category.Guitars }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("used-bass-guitars.aspx", "used-bass-guitars.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = 50 }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("used-drums.aspx", "used-drums.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = KnownIds.Category.Percussion }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("used-pro-sound.aspx", "used-pro-sound.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = KnownIds.Category.ProSound }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("used-keyboards-midi.aspx", "used-keyboards-midi.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = KnownIds.Category.Keyboard }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("used-band-equipment.aspx", "used-band-equipment.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = KnownIds.Category.Band }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("used-accessories.aspx", "used-accessories.aspx", new { controller = "Errors", action = "RedirectCategoryPages", id = KnownIds.Category.Accessories }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("new-gear.aspx", "new-gear.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "NewGear", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("newgear.aspx", "newgear.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "NewGear", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("gear-grabber.aspx", "gear-grabber.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("reviews.aspx", "reviews.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("bom.aspx", "bom.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("most-wanted.aspx", "most-wanted.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("cash-for-gear.aspx", "cash-for-gear.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("cashforgear.aspx", "cashforgear.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("how-we-buy-used-gear.aspx", "how-we-buy-used-gear.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "HowItWorks", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("make-a-trade.aspx", "make-a-trade.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "SellUsYourGear", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("about-us.aspx", "about-us.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "AboutUs", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("aboutus.aspx", "aboutus.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "AboutUs", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("press.aspx", "press.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Press", id = UrlParameter.Optional }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("careers.aspx", "careers.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("all-locations.aspx", "all-locations.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Index", redirectControllerName = "Locations" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("franchisemovies.aspx", "franchisemovies.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "FranchiseMovies", redirectControllerName = "Own" }, new[] { "Nop.Web.Controllers" });


            routes.MapRoute("my-music-go-round.aspx", "my-music-go-round.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Register", redirectControllerName = "Customer" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("create-account.aspx", "create-account.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Register", redirectControllerName = "Customer" }, new[] { "Nop.Web.Controllers" });
           
            routes.MapRoute("old search", "search.aspx", new { controller = "Errors", action = "RedirectOldSearch" }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("privacy_policy.aspx", "privacy_policy.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "PrivacyPolicy", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("return_policy.aspx", "return_policy.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "ReturnPolicy", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("shipping.aspx", "shipping.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "ShippingPolicy", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("conditions_of_use.aspx", "conditions_of_use.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "ConditionsOfUse", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("contact_us.aspx", "contact_us.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "ContactUs", redirectControllerName = "Home" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("site_map.aspx", "site_map.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Sitemap", redirectControllerName = "Common" }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("community.aspx", "community.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "CommunityLinks", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("mostwanted.aspx", "mostwanted.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "MostWanted", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("promotions.aspx", "promotions.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Promotions", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("lessons.aspx", "lessons.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "LessonsAndRepairs", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("jobs.aspx", "jobs.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "Jobs", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });
            routes.MapRoute("howitworks.aspx", "howitworks.aspx", new { controller = "Errors", action = "Redirect", redirectActionName = "HowItWorks", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });

            routes.MapRoute("mgr/about-us", "mgr/about-us", new { controller = "Errors", action = "Redirect", redirectActionName = "AboutUs", redirectControllerName = "Franchisee" }, new[] { "Nop.Web.Controllers" });
        }
    }
}