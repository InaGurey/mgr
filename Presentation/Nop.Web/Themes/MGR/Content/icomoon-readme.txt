﻿For icons, we generate a font using IcoMoon that contains only the icons we need. To add more:

1. Go to http://icomoon.io/app, login: purchasing@screenfeed.com, boom6500
2. Bottom right button, Load Session
3. Select iconmoon-session.json (in this folder)
4. Add icon(s) needed
5. Click Generate Font (bottom)
6. Click Download, extract the zip
7. Copy contents of style.css to icomoon.css
8. Copy contents of fonts subfolder to Fonts (overwrite)
9. Back on site, click Store Session (bottom right)
10. Copy contents of downloaded file to icomoon-session.json
