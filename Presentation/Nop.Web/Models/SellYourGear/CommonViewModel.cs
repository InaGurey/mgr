﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Treefort.Portal.Queries.Home;



namespace Nop.Web.Models.SellYourGear
{
    public class CommonViewModel
    {
        public bool IsFranchiseSite { get; set; }
        public RemoteControlPage RcContent { get; set; }
    }
}