﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treefort.Portal.Queries.Home;


namespace Nop.Web.Models.SellYourGear
{
    public class FormViewModel
    {
        public int LocationId { get; set; }
        [Required] public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
		public string ContactPreference { get; set; }
        
		public string City { get; set; }
        public string State { get; set; }
        [Required] public string Zip { get; set; }

        public bool IsFranchiseSite { get; set; }

        public RemoteControlPage RcContent { get; set; }

        public SellUsPicture PictureOne { get; set; }
        public SellUsPicture PictureTwo { get; set; }
        public SellUsPicture PictureThree { get; set; }
        public SellUsPicture PictureFour { get; set; }

        [Display(Name = "Make and Model")]
        [Required] public string MakeAndModel { get; set; }
        public string Color { get; set; }
        public string Year { get; set; }

        [Display(Name = "Describe Condition")]
        public string Describe { get; set; }
        public string Modifications { get; set; }

        [Display(Name="Gear Type")]
        public string GearType { get; set; }
        public string Condition { get; set; }

        public IEnumerable<SelectListItem> GearTypeOptions { get; set; }
        public IEnumerable<SelectListItem> ConditionOptions { get; set; }
		public IEnumerable<SelectListItem> ContactPreferenceOptions { get; set; }

        public class SellUsPicture
        {
            [UIHint("Picture")]
            public int PictureId { get; set; }
            public string PictureUrl { get; set; }
        }
    }
}