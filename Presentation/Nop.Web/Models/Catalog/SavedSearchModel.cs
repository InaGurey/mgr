﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Treefort.Common;


namespace Nop.Web.Models.Catalog
{
    public interface ISearchFormModel
    {
        int Cid { get; set; }
        int Mid { get; set; }
        IList<CategoryOption> CategoryOptions { get; set; }
        IList<SelectListItem> AvailableCategories { get; set; }
        IList<SelectListItem> AvailableManufacturers { get; set; }
        IList<SelectListItem> Stores { get; set; }
        IList<SelectListItem> AvailableDistances { get; set; }
    }

    public class SavedSearchCompleteModel : SavedSearchModel
    {
    }

    public class SavedSearchModel : SearchModel, IProductSearchParameters
    {
        public SavedSearchModel() : base()
        {
            this.Frequency = this.AvailableFrequencies.FirstOrDefault(o => o.Selected).IfNotNull(o => o.Value);
            this.Duration = this.AvailableDurations.FirstOrDefault(o => o.Selected).IfNotNull(o => o.Value);
        }
        public bool Cancelled { get; set; }
        public bool Ended { get; set; }

        string IProductSearchParameters.Keywords
        {
            get { return Q; }
            set { Q = value; }
        }

        IEnumerable<int> IProductSearchParameters.CategoryIds
        {
            get { return Cid == 0 ? null : new[] {Cid}; }
            set { Cid = value.IsNullOrEmpty() ? 0 : value.First(); }
        }

        int? IProductSearchParameters.CategoryId
        {
            get { return Cid == 0 ? (int?)null : Cid; }
            set { Cid = value ?? 0; }
        }

        int? IProductSearchParameters.ManufacturerId
        {
            get { return Mid == 0 ? (int?)null : Mid; }
            set { Mid = value ?? 0; }
        }

        decimal? IProductSearchParameters.MinimumPrice
        {
            get { return Pf.IfNotNullOrEmpty(o => decimal.Parse(o), (decimal?)null); }
            set { Pf = value.IfHasValue(o => o.ToString()); }
        }

        decimal? IProductSearchParameters.MaximumPrice
        {
            get { return Pt.IfNotNullOrEmpty(o => decimal.Parse(o), (decimal?)null); }
            set { Pt = value.IfHasValue(o => o.ToString()); }
        }

        bool IProductSearchParameters.ClearanceOnly
        {
            get { return Cl; }
            set { Cl = value; }
        }

        IEnumerable<string> IProductSearchParameters.StoreNumbers
        {
            get { return new[] {Sn}; }
            set { Sn = value.IfNotNullOrEmpty(o => o.First()); }
        }

        string IProductSearchParameters.PostalCode
        {
            get { return Zip; }
            set { Zip = value; }
        }

        int? IProductSearchParameters.MilesFromPostalCode
        {
            get { return Dist == 0 ? (int?)null : Dist; }
            set { Dist = value ?? 0; }
        }

        public int Id { get; set; }
        public string Slug { get; set; }
        [Required(ErrorMessage = "Please enter your email address.")] 
        public string Email { get; set; }
        [Required] public string Frequency { get; set; }
        [Required] public string Duration { get; set; }
        public bool NotifyOnlyWhenNewResults { get; set; }

        public bool ShowLearnMore { get; set; }

        public IEnumerable<SelectListItem> AvailableFrequencies
        {
            get {
                return new[] {
				    new SelectListItem {Text = "Send Them Daily", Value = "daily", Selected = true},
				    new SelectListItem {Text = "Send Them Weekly", Value = "weekly", Selected = false},
			    };
            }
        }

        public IEnumerable<SelectListItem> AvailableDurations
        {
            get {
                return new[] {
				    new SelectListItem {Text = "For One Week", Value = "7.00:00:00", Selected = true},
				    new SelectListItem {Text = "For One Month", Value = "30.00:00:00", Selected = false},
				    new SelectListItem {Text = "For Three Months", Value = "90.00:00:00", Selected = false}
			    };
            }
        }
    }

    public class CategoryOption
    {
        public CategoryOption()
        {
            this.AvailableCategories = new List<SelectListItem>();
        }

        public string Text { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }

        public IList<SelectListItem> AvailableCategories { get; set; }
    }

    public class CategorySelectListItem : SelectListItem
    {
        public int BrandListId { get; set; }
    }

    public class BrandSelectListItem : SelectListItem
    {
        public IList<int> BrandListIds { get; set; }
    }
}