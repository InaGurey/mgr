﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Catalog
{
    public partial class HomePageProductsModel : BaseNopModel
    {
        public HomePageProductsModel()
        {
            Categories = new List<HomePageCategoryModel>();
        }

        public IList<HomePageCategoryModel> Categories { get; set; }


        public class HomePageCategoryModel
        {
            public HomePageCategoryModel()
            {
                Products = new List<ProductOverviewModel>();
            }

            public string Name { get; set; }

            public string SeName { get; set; }

            public IList<ProductOverviewModel> Products { get; set; }
        }
    }
}