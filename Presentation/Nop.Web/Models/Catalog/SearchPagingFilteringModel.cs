﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using Nop.Web.Framework;
using Nop.Web.Framework.UI.Paging;

namespace Nop.Web.Models.Catalog
{
    [JsonObject]
    public partial class SearchPagingFilteringModel : BasePageableModel
    {
        public SearchPagingFilteringModel()
        {
            this.AvailableSortOptions = new List<SelectListItem>();
        }

        public IList<SelectListItem> AvailableSortOptions { get; set; }

        /// <summary>
        /// Order by
        /// </summary>
        [NopResourceDisplayName("Categories.OrderBy")]
        public int OrderBy { get; set; }
        public bool IsDefaultSorting { get; set; }
    }
}