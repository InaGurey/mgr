﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Catalog
{
    public partial class SearchModel : BaseNopModel, ISearchFormModel
    {
        public SearchModel()
        {
            PagingFilteringContext = new SearchPagingFilteringModel();
            Products = new List<ProductOverviewModel>();

            this.CategoryOptions = new List<CategoryOption>();
            this.AvailableCategories = new List<SelectListItem>();
            this.AvailableManufacturers = new List<SelectListItem>();
			this.Stores = new List<SelectListItem>();
        }

        public bool IsCorporateSite { get; set; }
		public bool ShowLocationPopups { get; set; }
        public string Warning { get; set; }

        /// <summary>
        /// Query string
        /// </summary>
        [NopResourceDisplayName("Search.SearchTerm")]
        [AllowHtml]
        public string Q { get; set; }

        /// <summary>
        /// Category ID
        /// </summary>
        [NopResourceDisplayName("Search.Category")]
        public int Cid { get; set; }

        [NopResourceDisplayName("Search.IncludeSubCategories")]
        public bool Isc { get; set; }

		[Display(Name = "Store")]
		public string Sn { get; set; }

        //Include all Stores
        public bool AllStores { get; set; }

        //Cleance
        public bool Cl { get; set; }

        [Display(Name = "Brand")]
        public int Mid { get; set; }

        /// <summary>
        /// Price - From 
        /// </summary>
        [AllowHtml]
        public string Pf { get; set; }

        /// <summary>
        /// Price - To
        /// </summary>
        [AllowHtml]
        public string Pt { get; set; }

        public string Dca { get; set; }

        /// <summary>
        /// A value indicating whether to search in descriptions
        /// </summary>
        [NopResourceDisplayName("Search.SearchInDescriptions")]
        public bool Sid { get; set; }

        /// <summary>
        /// A value indicating whether to search in descriptions
        /// </summary>
        [NopResourceDisplayName("Search.AdvancedSearch")]
        public bool Advanced { get; set; }


        [Display(Name = "Zip code")]
        public string Zip { get; set; }

        [Display(Name = "Distance")]
        public int Dist { get; set; }

        public bool SortByClosest { get; set; }

        public IList<CategoryOption> CategoryOptions { get; set; }
        public IList<SelectListItem> AvailableCategories { get; set; }
        public IList<SelectListItem> AvailableManufacturers { get; set; }
		public IList<SelectListItem> Stores { get; set; } 

        public SearchPagingFilteringModel PagingFilteringContext { get; set; }
        public IList<ProductOverviewModel> Products { get; set; }

		public int TotalResults { get; set; }

	    public IList<SelectListItem> AvailableDistances { get; set; }
    }
}