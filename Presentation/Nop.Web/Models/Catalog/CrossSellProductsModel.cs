﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
	public class CrossSellProductsModel
	{
		public IList<ProductOverviewModel> Products { get; set; }
		public int CategoryId { get; set; }
	}
}
