﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class EmailStoreAboutProductModel
    {
        public int ProductId { get; set; }
        public string TopMessage { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
        public string Product { get; set; }
        public string Hverify { get; set; }
    }
}