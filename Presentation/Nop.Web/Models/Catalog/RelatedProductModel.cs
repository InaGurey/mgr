﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Catalog
{
    public class RelatedProductModel
    {
        public IEnumerable<ProductOverviewModel> Products { get; set; }
        public bool IsCorp { get; set; }
    }
}