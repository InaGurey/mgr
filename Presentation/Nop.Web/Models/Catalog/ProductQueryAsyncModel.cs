﻿
namespace Nop.Web.Models.Catalog
{
    public class ProductQueryAsyncModel
    {
        public int CategoryId { get; set; }
        public int PageNumber { get; set; }
        public int OrderBy { get; set; }
        public decimal? PriceMin { get; set; }
        public decimal? PriceMax { get; set; }
        public int Br { get; set; }
        public bool? Used { get; set; }
        public bool IsClearanceOnly { get; set; }
    }
}