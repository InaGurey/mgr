﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Media;
using Treefort.Portal.Queries.Locations;

namespace Nop.Web.Models.Catalog
{
    [ProductVariantModel.MicrodataAttribute(ItemScope = true, ItemType = "http://schema.org/itemCondition")]
    public partial class ProductDetailsModel : BaseNopEntityModel
    {
        public ProductDetailsModel()
        {
            DefaultPictureModel = new PictureModel();
            PictureModels = new List<PictureModel>();
            ProductVariantModels = new List<ProductVariantModel>();
            SpecificationAttributeModels = new List<ProductSpecificationModel>();
            IsClearance = false;
        }

        public string Name { get; set; }
        public string CategoryName { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string ProductTemplateViewPath { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }
        public bool IsCorpProduct { get; set; }
        public bool IsClearance { get; set; }
        public bool IsSpecialPrice { get; set; }
        public bool Deleted { get; set; }
        public bool IsCorporateSite { get; set; }
        public bool HasRecentlyAvailableVariant { get; set; }

        public GenericLocation ProductLocation { get; set; }

        //picture(s)
        public bool DefaultPictureZoomEnabled { get; set; }
        public PictureModel DefaultPictureModel { get; set; }
        public IList<PictureModel> PictureModels { get; set; }

        //product variant(s)
        public IList<ProductVariantModel> ProductVariantModels { get; set; }
        //specification attributes
        public IList<ProductSpecificationModel> SpecificationAttributeModels { get; set; }

		#region Nested Classes

        public partial class ProductBreadcrumbModel : BaseNopModel
        {
            public ProductBreadcrumbModel()
            {
                CategoryBreadcrumb = new List<CategoryModel>();
            }

            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductSeName { get; set; }
            public IList<CategoryModel> CategoryBreadcrumb { get; set; }
        }

        [Microdata(ItemScope = true, ItemType = "http://schema.org/itemCondition")]
        public partial class ProductVariantModel : BaseNopEntityModel
        {
            public ProductVariantModel()
            {
                GiftCard = new GiftCardModel();
                ProductVariantPrice = new ProductVariantPriceModel();
                PictureModel = new PictureModel();
                AddToCart = new AddToCartModel();
                ProductVariantAttributes = new List<ProductVariantAttributeModel>();
            }

            public string Name { get; set; }
            public string ProductName { get; set; }
            public bool ShowSku { get; set; }
            public string Sku { get; set; }

            public string Description { get; set; }

            public bool ShowManufacturerPartNumber { get; set; }
            public string ManufacturerPartNumber { get; set; }

            public bool ShowGtin { get; set; }
            public string Gtin { get; set; }

            public bool HasSampleDownload { get; set; }

            public GiftCardModel GiftCard { get; set; }

            public bool HasStoreId { get; set; }

            public string StockAvailabilityGsFormat { get; set; }

            public string StockAvailablity { get; set; }
            [Required]
            [Microdata("condition", ItemReference="UsedCondition")]
            public string Condition { get; set; }

            public bool IsCurrentCustomerRegistered { get; set; }
            public bool DisplayBackInStockSubscription { get; set; }
            public bool BackInStockAlreadySubscribed { get; set; }

            public ProductVariantPriceModel ProductVariantPrice { get; set; }

            public AddToCartModel AddToCart { get; set; }

            public PictureModel PictureModel { get; set; }

            public IList<ProductVariantAttributeModel> ProductVariantAttributes { get; set; }

            #region Nested Classes

            public partial class AddToCartModel : BaseNopModel
            {
                public AddToCartModel()
                {
                    this.AllowedQuantities = new List<SelectListItem>();
                }
                public int ProductVariantId { get; set; }

                [NopResourceDisplayName("Products.Qty")]
                public int EnteredQuantity { get; set; }

                [NopResourceDisplayName("Products.EnterProductPrice")]
                public bool CustomerEntersPrice { get; set; }
                [NopResourceDisplayName("Products.EnterProductPrice")]
                public decimal CustomerEnteredPrice { get; set; }
                public String CustomerEnteredPriceRange { get; set; }
                
                public bool DisableBuyButton { get; set; }
                public bool DisableWishlistButton { get; set; }
                public List<SelectListItem> AllowedQuantities { get; set; }
                public bool AvailableForPreOrder { get; set; }
                public GenericLocation ProductLocation { get; set; }
                public int CategoryId { get; set; }
				public bool InStorePickupOnly { get; set; }
			}

            public partial class ProductVariantPriceModel : BaseNopModel
            {
                public string OldPrice { get; set; }
                public bool HasSpecialPrice { get; set; }

                public string Price { get; set; }
                public string PriceWithDiscount { get; set; }

                public decimal PriceValue { get; set; }
                public decimal PriceWithDiscountValue { get; set; }

                public bool CustomerEntersPrice { get; set; }

                public bool CallForPrice { get; set; }

                public int ProductVariantId { get; set; }

                public bool HidePrices { get; set; }

                public bool DynamicPriceUpdate { get; set; }

				public string Shipping { get; set; }
				public decimal ShippingValue { get; set; }
			}

            public partial class GiftCardModel : BaseNopModel
            {
                public bool IsGiftCard { get; set; }

                [NopResourceDisplayName("Products.GiftCard.RecipientName")]
                [AllowHtml]
                public string RecipientName { get; set; }
                [NopResourceDisplayName("Products.GiftCard.RecipientEmail")]
                [AllowHtml]
                public string RecipientEmail { get; set; }
                [NopResourceDisplayName("Products.GiftCard.SenderName")]
                [AllowHtml]
                public string SenderName { get; set; }
                [NopResourceDisplayName("Products.GiftCard.SenderEmail")]
                [AllowHtml]
                public string SenderEmail { get; set; }
                [NopResourceDisplayName("Products.GiftCard.Message")]
                [AllowHtml]
                public string Message { get; set; }

                public GiftCardType GiftCardType { get; set; }
            }

            public partial class TierPriceModel : BaseNopModel
            {
                public string Price { get; set; }
                [Required]
                [Microdata("Quantity", ItemId = "1")]
                public int Quantity { get; set; }
            }

            public partial class ProductVariantAttributeModel : BaseNopEntityModel
            {
                public ProductVariantAttributeModel()
                {
                    AllowedFileExtensions = new List<string>();
                    Values = new List<ProductVariantAttributeValueModel>();
                }

                public int ProductVariantId { get; set; }

                public int ProductAttributeId { get; set; }

                public string Name { get; set; }

                public string Description { get; set; }

                public string TextPrompt { get; set; }

                public bool IsRequired { get; set; }

                /// <summary>
                /// Selected value for textboxes
                /// </summary>
                public string TextValue { get; set; }
                /// <summary>
                /// Selected day value for datepicker
                /// </summary>
                public int? SelectedDay { get; set; }
                /// <summary>
                /// Selected month value for datepicker
                /// </summary>
                public int? SelectedMonth { get; set; }
                /// <summary>
                /// Selected year value for datepicker
                /// </summary>
                public int? SelectedYear { get; set; }
                /// <summary>
                /// Allowed file extensions for customer uploaded files
                /// </summary>
                public IList<string> AllowedFileExtensions { get; set; }

                public AttributeControlType AttributeControlType { get; set; }
                
                public IList<ProductVariantAttributeValueModel> Values { get; set; }

            }

            public partial class ProductVariantAttributeValueModel : BaseNopEntityModel
            {
                public string Name { get; set; }

                public string PriceAdjustment { get; set; }

                public decimal PriceAdjustmentValue { get; set; }

                public bool IsPreSelected { get; set; }
            }

            [AttributeUsage(AttributeTargets.Property | 
                AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
            public class MicrodataAttribute : Attribute
            {

                public bool ItemScope { get; set; }
                public string ItemType { get; set; }
                public string ItemId { get; set; }
                public string ItemProperty { get; set; }
                public string ItemReference { get; set; }
               // public string property;

                public MicrodataAttribute()
                {
                    
                }
                public MicrodataAttribute(string property)
                {
                    this.ItemProperty = property;
                }

              public RouteValueDictionary GetAttributes()
                {
                    var attributes = new RouteValueDictionary();
                    if(this.ItemScope)
                        attributes.Add("itemscope", "itemscope");
                    if (!string.IsNullOrEmpty(this.ItemType))
                        attributes.Add("itemtype", this.ItemType);
                    if(!string.IsNullOrEmpty(this.ItemId))
                        attributes.Add("itemid", this.ItemId);
                    if(!string.IsNullOrEmpty(this.ItemProperty))
                        attributes.Add("itemprop", this.ItemProperty);
                    if(!string.IsNullOrEmpty(this.ItemReference))
                        attributes.Add("itemref", this.ItemReference);

                    return attributes;
                }

              
            }

            public class MicrodataModelMetadataProvider : DataAnnotationsModelMetadataProvider
            {
                protected override ModelMetadata CreateMetadata(
                    IEnumerable<Attribute> attributes,
                    Type containerType,
                    Func<object> modelAccessor,
                    Type modelType,
                    string propertyName)
                {
                    ModelMetadata metadata = base.CreateMetadata(
                        attributes,
                        containerType,
                        modelAccessor,
                        modelType,
                        propertyName);
                    //if no propery name is specified, use fully qualified name of the Type
                    string key = string.IsNullOrEmpty(propertyName) ?
                        modelType.FullName :
                        propertyName;
                    var microdata = attributes.OfType<MicrodataAttribute>().FirstOrDefault();

                    if (microdata != null)
                        metadata.AdditionalValues[key] = microdata.GetAttributes();

                    return metadata;
                }
            }

           
            #endregion
        }

		#endregion
    }
}