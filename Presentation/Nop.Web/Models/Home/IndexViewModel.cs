﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treefort.Portal.Queries.Franchisee;
using Treefort.Portal.Queries.Franchisee.Website;
using Treefort.Portal.Queries.Home;

namespace Nop.Web.Models.Home
{
    public class IndexViewModel
    {
		public WebsiteOptions WebsiteOptions { get; set; }
        public FranchiseeMobileNavigationViewModel MobileNavigation { get; set; }
        public GenericPageViewModel FranchiseeIndex { get; set; }
        public RemoteControlPage RemoteControlPage { get; set; }
        public bool IsFranchiseHomepage { get; set; }
        public bool IsCoop { get; set; }
    }
}
