﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Order
{
	public class OrderDetailsPageModel
	{
        public OrderDetailsPageModel()
	    {
	        AdwordsConversion = new AdwordsConversionModel();
	    }
		public bool WasJustSubmitted { get; set; }
		public bool PrintMode { get; set; }
		public IList<OrderDetailsModel> Orders { get; set; }
		public IList<object> GaCommands { get; set; }

        public AdwordsConversionModel AdwordsConversion { get; set; }
	}
}