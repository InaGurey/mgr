﻿namespace Nop.Web.Models.Order
{
    public class AdwordsConversionModel
    {
        public bool HasAdwordsConversion
        {
            get { return !string.IsNullOrWhiteSpace(ConversionId); }
        }

        public string ConversionId { get; set; }
        public decimal Value { get; set; }
        public string CurrencyCode { get { return "USD"; } }
    }
}