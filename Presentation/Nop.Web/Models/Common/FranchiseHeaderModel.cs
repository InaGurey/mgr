﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Treefort.Portal.Queries.Franchisee.Website;

namespace Nop.Web.Models.Common
{
    public class FranchiseHeaderModel
    {
        public string LogoSubtext { get; set; }
        public bool IsCoop { get; set; }
        public bool HideSellUsYourGear { get; set; }
        public string BackgroundImageUrl { get; set; }
        public HeaderViewModel FranchiseeLinks { get; set; }
    }
}