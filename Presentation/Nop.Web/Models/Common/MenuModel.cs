﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Web.Framework.Mvc;
using Treefort.Portal;
using Treefort.Portal.Queries.Franchisee.Website;

namespace Nop.Web.Models.Common
{
    public partial class MenuModel : BaseNopModel
    {
        public bool BlogEnabled { get; set; }
        public bool RecentlyAddedProductsEnabled { get; set; }
        public bool ForumEnabled { get; set; }
        public string CurrentUrl { get; set; }
        public HeaderViewModel MenuVisible { get; set; }
        public bool IsCoop { get; set; }
        public MenuCategories Categories { get; set; }
        public List<CategoryInfo> GuitarBrands { get; set; }
        public List<CategoryInfo> PercussionBrands { get; set; }
        public List<CategoryInfo> BandBrands { get; set; }
        public List<CategoryInfo> KeyboardBrands { get; set; }
        public List<CategoryInfo> ProSoundBrands { get; set; }
        public List<CategoryInfo> AccessoriesBrands { get; set; }


        public class CategoryInfo
        {
            public string Name { get; set; }
            public int Id { get; set; }
            public string DrsCode { get; set; }
			public string SeName { get; set; }
            public bool HideUsedFromNav { get; set; }
            public string filterUrl { get; set; }
        }

        public class MenuCategories 
        {
            public CategoryInfo Guitars { get; set; }
            public CategoryInfo Percussion { get; set; }
            public CategoryInfo Band { get; set; }
            public CategoryInfo Keyboard { get; set; }
            public CategoryInfo ProSound { get; set; }
            public CategoryInfo Accessories { get; set; }
            public CategoryInfo AccessoriesBandInstruments { get; set; }
            public CategoryInfo AccessoriesDrumHeads { get; set; }
            public CategoryInfo AccessoriesGuitarStrings { get; set; }
            public CategoryInfo AccessoriesGuitars { get; set; }
            public CategoryInfo AccessoriesKeyboards { get; set; }
            public CategoryInfo AccessoriesOther { get; set; }
            public CategoryInfo AccessoriesPercussion { get; set; }
            public CategoryInfo AccessoriesProSound { get; set; }

            public List<CategoryInfo> GuitarSubC { get; set; }
            public List<CategoryInfo> PercussionSubC { get; set; }
            public List<CategoryInfo> BandSubC { get; set; }
            public List<CategoryInfo> KeyboardSubC { get; set; }
            public List<CategoryInfo> ProSoundSubC { get; set; }
            public List<CategoryInfo> AccessoriesSubC { get; set; }
        }
    }

    public static class CorporateNav
    {
        //private static IDictionary<int, IList<string>> _lookup = new Dictionary<int, IList<string>>();
        //static CorporateNav()
        //{
        //    var all = Guitars.Concat(Percussion).Concat(Band).Concat(Keyboard).Concat(ProSound).Concat(Accessories);
            
        //    foreach (var c in all) {
        //        if (!_lookup.ContainsKey(c.Id)) { _lookup.Add(c.Id, new List<string>()); }
        //        var names = _lookup[c.Id];
        //        if (!names.Contains(c.Name)) { names.Add(c.Name); }
        //    }
        //}

        //public static string GetBrandName(int brandId)
        //{
        //    return _lookup.ContainsKey(brandId) && _lookup[brandId].Contains(name, StringComparer.InvariantCultureIgnoreCase);
        //}

        public static string GetBrandName(int categoryId, int brandId)
        {
            var list = Enumerable.Empty<MenuModel.CategoryInfo>();
            switch (categoryId) {
                case KnownIds.Category.Guitars: list = Guitars; break;
                case KnownIds.Category.Percussion: list = Percussion; break;
                case KnownIds.Category.Band: list = Band; break;
                case KnownIds.Category.Keyboard: list = Keyboard; break;
                case KnownIds.Category.ProSound: list = ProSound; break;
                case KnownIds.Category.Accessories: list = Accessories; break;
            }
            return list.FirstOrDefault(b => b.Id == brandId).IfNotNull(x => x.Name);
        }

        public static List<MenuModel.CategoryInfo> Guitars = new List<MenuModel.CategoryInfo>()
            {
                new MenuModel.CategoryInfo() {Id = 70, Name = "Fender"},
                new MenuModel.CategoryInfo() {Id = 83, Name = "Gibson"},
                new MenuModel.CategoryInfo() {Id = 108, Name = "Marshall"},
                new MenuModel.CategoryInfo() {Id = 186, Name = "Yamaha"},
                new MenuModel.CategoryInfo() {Id = 131, Name = "Peavey"},
                new MenuModel.CategoryInfo() {Id = 66, Name = "ESP"},
                new MenuModel.CategoryInfo() {Id = 104, Name = "LTD"},
                new MenuModel.CategoryInfo() {Id = 155, Name = "Schecter"},
                new MenuModel.CategoryInfo() {Id = 64, Name = "Epiphone"},
                new MenuModel.CategoryInfo() {Id = 113, Name = "Mesa Boogie"},
                new MenuModel.CategoryInfo() {Id = 102, Name = "Line 6"},
                new MenuModel.CategoryInfo() {Id = 91, Name = "Ibanez"}
            };

        public static List<MenuModel.CategoryInfo> Percussion = new List<MenuModel.CategoryInfo>()
            {
                new MenuModel.CategoryInfo() {Id = 188, Name = "Zildjian"},
                new MenuModel.CategoryInfo() {Id = 153, Name = "Sabian"},
                new MenuModel.CategoryInfo() {Id = 129, Name = "Paiste"},
                new MenuModel.CategoryInfo() {Id = 172, Name = "Tama"},
                new MenuModel.CategoryInfo() {Id = 130, Name = "Pearl"},
                new MenuModel.CategoryInfo() {Id = 55, Name = "DW"},
                new MenuModel.CategoryInfo() {Id = 186, Name = "Yamaha"},
                new MenuModel.CategoryInfo() {Id = 105, Name = "Ludwig"},
                new MenuModel.CategoryInfo() {Id = 144, Name = "Ddrum"},
                new MenuModel.CategoryInfo() {Id = 103, Name = "LP"},
                new MenuModel.CategoryInfo() {Id = 137, Name = "Premier"},
                new MenuModel.CategoryInfo() {Id = 107, Name = "Mapex"},
                new MenuModel.CategoryInfo() {Id = 128, Name = "Pacific"},
                new MenuModel.CategoryInfo() {Id = 178, Name = "Toca"},
                new MenuModel.CategoryInfo() {Id = 146, Name = "Remo"}
            };

        public static List<MenuModel.CategoryInfo> Band = new List<MenuModel.CategoryInfo>()
            {
                new MenuModel.CategoryInfo() {Id = 186, Name = "Yamaha"},
                new MenuModel.CategoryInfo() {Id = 158, Name = "Selmer"},
                new MenuModel.CategoryInfo() {Id = 23, Name = "Bach"},
                new MenuModel.CategoryInfo() {Id = 33, Name = "Bundy"},
                new MenuModel.CategoryInfo() {Id = 182, Name = "Vito"},
                new MenuModel.CategoryInfo() {Id = 95, Name = "King"},
                new MenuModel.CategoryInfo() {Id = 36, Name = "Conn"},
                new MenuModel.CategoryInfo() {Id = 80, Name = "Getzen"},
                new MenuModel.CategoryInfo() {Id = 77, Name = "Gemeinhardt"}
            };

        public static List<MenuModel.CategoryInfo> Keyboard = new List<MenuModel.CategoryInfo>()
            {
                new MenuModel.CategoryInfo() {Id = 152, Name = "Roland"},
                new MenuModel.CategoryInfo() {Id = 131, Name = "Peavey"},
                new MenuModel.CategoryInfo() {Id = 186, Name = "Yamaha"},
                new MenuModel.CategoryInfo() {Id = 110, Name = "M-Audio"},
                new MenuModel.CategoryInfo() {Id = 122, Name = "Novation"},
                new MenuModel.CategoryInfo() {Id = 10, Name = "Alesis"},
                new MenuModel.CategoryInfo() {Id = 97, Name = "Korg"},
                new MenuModel.CategoryInfo() {Id = 34, Name = "Casio"},
                new MenuModel.CategoryInfo() {Id = 30, Name = "Boss"},
                new MenuModel.CategoryInfo() {Id = 98, Name = "Kurzweil"},
                new MenuModel.CategoryInfo() {Id = 63, Name = "Ensoniq"},
                new MenuModel.CategoryInfo() {Id = 94, Name = "Kawai"},
                new MenuModel.CategoryInfo() {Id = 189, Name = "Zoom"}
            };
        
        public static List<MenuModel.CategoryInfo> ProSound = new List<MenuModel.CategoryInfo>()
            {
                new MenuModel.CategoryInfo() {Id = 142, Name = "QSC"},
                new MenuModel.CategoryInfo() {Id = 41, Name = "Crown"},
                new MenuModel.CategoryInfo() {Id = 40, Name = "Crest"},
                new MenuModel.CategoryInfo() {Id = 131, Name = "Peavey"},
                new MenuModel.CategoryInfo() {Id = 11, Name = "Allen & Heath"},
                new MenuModel.CategoryInfo() {Id = 176, Name = "TC Electronics"},
                new MenuModel.CategoryInfo() {Id = 160, Name = "Shure"},
                new MenuModel.CategoryInfo() {Id = 20, Name = "Audio Technica"},
                new MenuModel.CategoryInfo() {Id = 59, Name = "Electro-Voice"},
                new MenuModel.CategoryInfo() {Id = 8, Name = "AKG"},
                new MenuModel.CategoryInfo() {Id = 152, Name = "Roland"},
                new MenuModel.CategoryInfo() {Id = 173, Name = "Tascam"},
                new MenuModel.CategoryInfo() {Id = 186, Name = "Yamaha"},
                new MenuModel.CategoryInfo() {Id = 106, Name = "Mackie"},
                new MenuModel.CategoryInfo() {Id = 93, Name = "JBL"}
            };

        public static List<MenuModel.CategoryInfo> Accessories = new List<MenuModel.CategoryInfo>()
            {
                new MenuModel.CategoryInfo() {Id = 42, Name = "D'Addario"},
                new MenuModel.CategoryInfo() {Id = 135, Name = "Planet Waves"},
                new MenuModel.CategoryInfo() {Id = 67, Name = "Evans"},
                new MenuModel.CategoryInfo() {Id = 140, Name = "Promark"},
                new MenuModel.CategoryInfo() {Id = 147, Name = "Rico"},
                new MenuModel.CategoryInfo() {Id = 124, Name = "On Stage"},
                new MenuModel.CategoryInfo() {Id = 139, Name = "Pro Rock Gear"},
                new MenuModel.CategoryInfo() {Id = 117, Name = "Music Nomad"},
                new MenuModel.CategoryInfo() {Id = 81, Name = "GHS"},
                new MenuModel.CategoryInfo() {Id = 125, Name = "One Spot"},
                new MenuModel.CategoryInfo() {Id = 88, Name = "Hearos"},
                new MenuModel.CategoryInfo() {Id = 19, Name = "ATUS"}
            };


    }
}