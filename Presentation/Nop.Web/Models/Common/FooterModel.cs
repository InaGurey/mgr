﻿using Nop.Web.Framework.Mvc;
using Treefort.Portal.Queries.Franchisee.Website;

namespace Nop.Web.Models.Common
{
    public partial class FooterModel : BaseNopModel
    {
        public string StoreName { get; set; }
        public string ConstantContactUrl { get; set; }
        public bool IsFranchiseSite { get; set; }
        public bool IsCoop { get; set; }
        public bool HideSellYourGear { get; set; }
        public MenuModel.MenuCategories Categories { get; set; }
        public HeaderViewModel FranchiseeLinks { get; set; }
    }
}