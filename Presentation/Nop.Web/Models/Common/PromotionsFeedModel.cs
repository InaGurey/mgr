﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Treefort.Portal.Queries.Franchisee;

namespace Nop.Web.Models.Common
{
    public class PromotionsFeedModel
    {
        public PromotionsFeedModel()
        {
            Promotions = new List<GenericPageViewModel.CouponWidget>();
        }

        public string ConstantContactUrl { get; set; }
        public IList<GenericPageViewModel.CouponWidget> Promotions { get; set; }
    }
}