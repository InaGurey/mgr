﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Tax.CountryStateZip
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Tax.CountryStateZip.Configure",
                 "Plugins/TaxCountryStateZip/Configure",
                 new { controller = "TaxCountryStateZip", action = "Configure" },
                 new[] { "Nop.Plugin.Tax.CountryStateZip.Controllers" }
            );

            routes.MapRoute("Plugin.Tax.CountryStateZip.AddTaxRate",
                 "Plugins/TaxCountryStateZip/AddTaxRate",
                 new { controller = "TaxCountryStateZip", action = "AddTaxRate" },
                 new[] { "Nop.Plugin.Tax.CountryStateZip.Controllers" }
            );

            routes.MapRoute("Plugin.Tax.CountryStateZip.RateUpdate",
                 "Plugins/TaxCountryStateZip/RateUpdate",
                 new { controller = "TaxCountryStateZip", action = "RateUpdate" },
                 new[] { "Nop.Plugin.Tax.CountryStateZip.Controllers" }
            );

            routes.MapRoute("Plugin.Tax.CountryStateZip.RateDelete",
                 "Plugins/TaxCountryStateZip/RateDelete",
                 new { controller = "TaxCountryStateZip", action = "RateDelete" }
                 ,new[] { "Nop.Plugin.Tax.CountryStateZip.Controllers" }
            );

            routes.MapRoute("Plugin.Tax.CountryStateZip.RatesList",
                 "Plugins/TaxCountryStateZip/RatesList",
                 new { controller = "TaxCountryStateZip", action = "RatesList" }
                 , new[] { "Nop.Plugin.Tax.CountryStateZip.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
