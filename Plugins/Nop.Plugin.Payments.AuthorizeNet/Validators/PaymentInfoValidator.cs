﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Nop.Plugin.Payments.AuthorizeNet.Models;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Payments.AuthorizeNet.Validators
{
    public class PaymentInfoValidator : AbstractValidator<PaymentInfoModel>
    {
        public PaymentInfoValidator(ILocalizationService localizationService)
        {
            //useful links:
            //http://fluentvalidation.codeplex.com/wikipage?title=Custom&referringTitle=Documentation&ANCHOR#CustomValidator
            //http://benjii.me/2010/11/credit-card-validator-attribute-for-asp-net-mvc-3/

            //RuleFor(x => x.CardNumber).NotEmpty().WithMessage(localizationService.GetResource("Payment.CardNumber.Required"));
            //RuleFor(x => x.CardCode).NotEmpty().WithMessage(localizationService.GetResource("Payment.CardCode.Required"));

            RuleFor(x => x.CardholderName).NotEmpty().WithMessage("Cardholder name is required.");
			RuleFor(x => x.CardNumber).IsCreditCard().WithMessage("Card number is invalid.");
			Custom(ValidateDate);
			RuleFor(x => x.CardCode).Matches(@"^[0-9]{3,4}$").WithMessage("Card security code is invalid.");
		}

		// Treefort enhancement
		private ValidationFailure ValidateDate(PaymentInfoModel x) {
			int month, year;
			if (!int.TryParse(x.ExpireMonth, out month))
				return new ValidationFailure("ExpireMonth", "Expiration month is invalid.");
			if (!int.TryParse(x.ExpireYear, out year))
				return new ValidationFailure("ExpireYear", "Expiration year is invalid.");
			var expDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
			if (DateTime.Today > expDate)
				return new ValidationFailure("ExpireMonth", "Expiration date cannot be in the past.");
			return null;			
		}
	}
}