﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Nop.Core.Configuration;
using Nop.Plugin.Shipping.UPS.Domain;
using System.Runtime.Serialization.Json;

namespace Nop.Plugin.Shipping.UPS
{
    public class UPSSettings : ISettings
    {
        public string Url { get; set; }
        public string ShipUrl { get; set; }
        public string AddressValidationUrl { get; set; }

        public string AccessKey { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public decimal AdditionalHandlingCharge { get; set; }

        public UPSCustomerClassification CustomerClassification { get; set; }

        public UPSPickupType PickupType { get; set; }

        public UPSPackagingType PackagingType { get; set; }

        public int DefaultShippedFromCountryId { get; set; }

        public string DefaultShippedFromZipPostalCode { get; set; }

        public string CarrierServicesOffered { get; set; }
        //{
        //    get
        //    {
        //        return DeserializeString(SerializedCarrierServicesOffered);

        //        //var services = new[] { UPSService.UpsGround.ToString(), UPSService.UpsStandard.ToString() };
        //        //SerializedCarrierServicesOffered = SerializeString(services);
        //        //return services;
        //    }
        //    set 
        //    {
        //        SerializedCarrierServicesOffered = SerializeString(value);
        //    }
        //}

        public bool InsurePackage { get; set; }

        public static string SerializeString(string[] raw)
        {
            var stream = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(string[]));
            ser.WriteObject(stream, raw);
            stream.Position = 0;
            var result = new StreamReader(stream).ReadToEnd();
            stream.Dispose();
            return result;
        }

        public static string[] DeserializeString(string serialized)
        {
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(serialized)) {Position = 0};
            var ser = new DataContractJsonSerializer(typeof (string[]));
            stream.Position = 0;
            var result = ser.ReadObject(stream) as string[];
            return result;
        }
    }
}