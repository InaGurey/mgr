﻿
using System.ComponentModel;

namespace Nop.Plugin.Shipping.UPS.Domain
{
    /// <summary>
    /// UPS pickup type
    /// </summary>
    public enum UPSPickupType
    {
        [Description("One Time Pickup")]
        OneTimePickup,
        [Description("Daily Pickup")]
        DailyPickup,
        [Description("Customer Counter")]
        CustomerCounter,
        [Description("On Call Air")]
        OnCallAir,
        [Description("Suggested Retail Rates")]
        SuggestedRetailRates,
        [Description("Letter Center")]
        LetterCenter,
        [Description("Air Service Center")]
        AirServiceCenter
    }
}
