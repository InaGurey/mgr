﻿//------------------------------------------------------------------------------
// Contributor(s): mb 10/20/2010. 
//------------------------------------------------------------------------------

using Nop.Core;

namespace Nop.Plugin.Shipping.UPS.Domain
{
    public enum UPSService
    {
        [Name("UPS Ground")]
        [Code("03")]
        UpsGround,
        [Name("UPS Next Day Air")]
        [Code("01")]
        UpsNextDayAir,
        [Name("UPS 2nd Day Air")]
        [Code("02")]
        Ups2ndDayAir,
        [Name("UPS Worldwide Express")]
        [Code("07")]
        UpsWorldwideExpress,
        [Name("UPS Worldwide Expedited")]
        [Code("08")]
        UpsWorldwideExpedited,
        [Name("UPS Standard")]
        [Code("11")]
        UpsStandard,
        [Name("UPS 3 Day Select")]
        [Code("12")]
        Ups3DaySelect,
        [Name("UPS Next Day Air Saver")]
        [Code("13")]
        UpsNextDayAirSaver,
        [Name("UPS Next Day Air Early A.M.")]
        [Code("14")]
        UpsNextDayAirEarlyAM,
        [Name("UPS Worldwide Express Plus")]
        [Code("54")]
        UpsWorldwideExpressPlus,
        [Name("UPS 2nd Day Air A.M.")]
        [Code("59")]
        Ups2ndDayAirAM,
        [Name("UPS Saver")]
        [Code("65")]
        UpsSaver,
        [Name("UPS Today Standard")]
        [Code("82")]
        UpsTodayStandard,
        [Name("UPS Today Dedicated Courrier")]
        [Code("83")]
        UpsTodayDedicatedCourrier,
        [Name("UPS Today Express")]
        [Code("85")]
        UpsTodayExpress,
        [Name("UPS Today Express Saver")]
        [Code("86")]
        UpsTodayExpressSaver
    }
}
