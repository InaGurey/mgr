﻿
using System.ComponentModel;
using Nop.Core;

namespace Nop.Plugin.Shipping.UPS.Domain
{
    /// <summary>
    /// UPSP packaging type
    /// </summary>
    public enum UPSPackagingType
    {
        [Description("Customer Supplied Package")]
        [Code("02")]
        CustomerSuppliedPackage,
        [Description("UPS Letter")]
        [Code("01")]
        Letter,
        [Description("UPS Tube")]
        [Code("03")]
        Tube,
        [Description("UPS PAK")]
        [Code("04")]
        PAK,
        [Description("UPS Express Box")]
        [Code("21")]
        ExpressBox,
        [Description("10 Kg Box")]
        [Code("25")]
        _10KgBox,
        [Description("25 Kg Box")]
        [Code("24")]
        _25KgBox
    }
}
