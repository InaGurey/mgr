﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.Shipping.UPS.Domain;
using Nop.Plugin.Shipping.UPS.Models;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Shipping.UPS.Controllers
{
    [AdminAuthorize]
    public class ShippingUPSController : Controller
    {
        private readonly UPSSettings _upsSettings;
        private readonly ISettingService _settingService;
        private readonly ICountryService _countryService;

        public ShippingUPSController(UPSSettings upsSettings, ISettingService settingService,
            ICountryService countryService)
        {
            this._upsSettings = upsSettings;
            this._settingService = settingService;
            this._countryService = countryService;
        }

        public ActionResult Configure()
        {
            var model = new UPSShippingModel();
            model.Url = _upsSettings.Url;
            model.ShipUrl = _upsSettings.ShipUrl;
            model.AddressValidationUrl = _upsSettings.AddressValidationUrl;
            model.AccessKey = _upsSettings.AccessKey;
            model.Username = _upsSettings.Username;
            model.Password = _upsSettings.Password;
            model.AdditionalHandlingCharge = _upsSettings.AdditionalHandlingCharge;
            model.InsurePackage = _upsSettings.InsurePackage;

            foreach (UPSCustomerClassification customerClassification in Enum.GetValues(typeof(UPSCustomerClassification)))
            {
                model.AvailableCustomerClassifications.Add(new SelectListItem()
                    {
                        Text = CommonHelper.ConvertEnum(customerClassification.ToString()),
                        Value = customerClassification.ToString(),
                        Selected = customerClassification == _upsSettings.CustomerClassification
                    });
            }
            foreach (UPSPickupType pickupType in Enum.GetValues(typeof(UPSPickupType)))
            {
                model.AvailablePickupTypes.Add(new SelectListItem()
                {
                    Text = CommonHelper.ConvertEnum(pickupType.ToString()),
                    Value = pickupType.ToString(),
                    Selected = pickupType == _upsSettings.PickupType
                });
            }
            foreach (UPSPackagingType packagingType in Enum.GetValues(typeof(UPSPackagingType)))
            {
                model.AvailablePackagingTypes.Add(new SelectListItem()
                {
                    Text = CommonHelper.ConvertEnum(packagingType.ToString()),
                    Value = packagingType.ToString(),
                    Selected = packagingType == _upsSettings.PackagingType
                });
            }

            foreach (var country in _countryService.GetAllCountries(true))
            {
                model.AvailableCountries.Add(new SelectListItem()
                {
                    Text = country.Name.ToString(),
                    Value = country.Id.ToString(),
                    Selected = country.Id == _upsSettings.DefaultShippedFromCountryId
                });
            }
            model.DefaultShippedFromZipPostalCode = _upsSettings.DefaultShippedFromZipPostalCode;

            // Load Domestic service names

            if (_upsSettings.CarrierServicesOffered != null)
            {
                model.CarrierServicesOffered =
                    UPSSettings.DeserializeString(_upsSettings.CarrierServicesOffered).Select(
                        s => (UPSService) Enum.Parse(typeof (UPSService), s)).ToList();
            }

            return View("Nop.Plugin.Shipping.UPS.Views.ShippingUPS.Configure", model);
        }

        [HttpPost]
        public ActionResult Configure(UPSShippingModel model)
        {
            if (!ModelState.IsValid)
            {
                return Configure();
            }

            //save settings
            _upsSettings.Url = model.Url;
            _upsSettings.ShipUrl = model.ShipUrl;
            _upsSettings.AddressValidationUrl = model.AddressValidationUrl;
            _upsSettings.AccessKey = model.AccessKey;
            _upsSettings.Username = model.Username;
            _upsSettings.Password = model.Password;
            _upsSettings.AdditionalHandlingCharge = model.AdditionalHandlingCharge;
            _upsSettings.InsurePackage = model.InsurePackage;
            _upsSettings.CustomerClassification = (UPSCustomerClassification)Enum.Parse(typeof(UPSCustomerClassification), model.CustomerClassification);
            _upsSettings.PickupType = (UPSPickupType)Enum.Parse(typeof(UPSPickupType), model.PickupType);
            _upsSettings.PackagingType = (UPSPackagingType)Enum.Parse(typeof(UPSPackagingType), model.PackagingType);
            _upsSettings.DefaultShippedFromCountryId = model.DefaultShippedFromCountryId;
            _upsSettings.DefaultShippedFromZipPostalCode = model.DefaultShippedFromZipPostalCode;


            // Save selected services

            if (model.CheckedCarrierServices != null)
            {
                _upsSettings.CarrierServicesOffered =
                    UPSSettings.SerializeString(model.CheckedCarrierServices.Count() != 0
                        ? model.CheckedCarrierServices
                        : new[]
                        {
                            UPSService.UpsGround.ToString(), UPSService.UpsStandard.ToString(),
                            UPSService.Ups3DaySelect.ToString(), UPSService.UpsNextDayAir.ToString(),
                            UPSService.UpsNextDayAirSaver.ToString(), UPSService.UpsNextDayAirEarlyAM.ToString()
                        });

                _settingService.SaveSetting(_upsSettings);
            }

            return Configure();
        }

    }
}
