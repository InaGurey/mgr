﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Nop.Plugin.Shipping.UPS.Domain;
using Nop.Web.Framework;

namespace Nop.Plugin.Shipping.UPS.Models
{
    public class UPSShippingModel
    {
        public UPSShippingModel()
        {
            CarrierServicesOffered = new List<UPSService>();
            AvailableCarrierServices = Enum.GetValues(typeof (UPSService)).Cast<UPSService>().ToList();
            AvailableCustomerClassifications = new List<SelectListItem>();
            AvailablePickupTypes = new List<SelectListItem>();
            AvailablePackagingTypes = new List<SelectListItem>();
            AvailableCountries = new List<SelectListItem>();
        }
        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.Url")]
        public string Url { get; set; }
        [DisplayName("Shipping API Url")]
        public string ShipUrl { get; set; }
        [DisplayName("Address Validation API Url")]
        public string AddressValidationUrl { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.AccessKey")]
        public string AccessKey { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.Username")]
        public string Username { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.Password")]
        public string Password { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.AdditionalHandlingCharge")]
        public decimal AdditionalHandlingCharge { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.InsurePackage")]
        public bool InsurePackage { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.CustomerClassification")]
        public string CustomerClassification { get; set; }
        public IList<SelectListItem> AvailableCustomerClassifications { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.PickupType")]
        public string PickupType { get; set; }
        public IList<SelectListItem> AvailablePickupTypes { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.PackagingType")]
        public string PackagingType { get; set; }
        public IList<SelectListItem> AvailablePackagingTypes { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.DefaultShippedFromCountry")]
        public int DefaultShippedFromCountryId { get; set; }
        public IList<SelectListItem> AvailableCountries { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.DefaultShippedFromZipPostalCode")]
        public string DefaultShippedFromZipPostalCode { get; set; }


        public IList<UPSService> CarrierServicesOffered { get; set; }
        [NopResourceDisplayName("Plugins.Shipping.UPS.Fields.AvailableCarrierServices")]
        public IList<UPSService> AvailableCarrierServices { get; set; }
        public string[] CheckedCarrierServices { get; set; }
    }
}