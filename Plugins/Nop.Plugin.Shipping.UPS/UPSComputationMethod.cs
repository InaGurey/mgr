﻿//------------------------------------------------------------------------------
// Contributor(s): mb 10/20/2010. 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Routing;
using System.Web.Services.Protocols;
using System.Xml;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Plugin.Shipping.UPS.Domain;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
using Treefort.Portal;
using Nop.Plugin.Shipping.UPS.Ship;
using Nop.Plugin.Shipping.UPS.XAV;
using UPSSecurity = Nop.Plugin.Shipping.UPS.Ship.UPSSecurity;
using UPSSecurityServiceAccessToken = Nop.Plugin.Shipping.UPS.Ship.UPSSecurityServiceAccessToken;
using UPSSecurityUsernameToken = Nop.Plugin.Shipping.UPS.Ship.UPSSecurityUsernameToken;

namespace Nop.Plugin.Shipping.UPS
{
    /// <summary>
    /// UPS computation method
    /// </summary>
    public class UPSComputationMethod : BasePlugin, IShippingRateComputationMethod
    {
        #region Constants

        private const int MAXPACKAGEWEIGHT = 150;
        private const string MEASUREWEIGHTSYSTEMKEYWORD = "lb";
        private const string MEASUREDIMENSIONSYSTEMKEYWORD = "inches";


        #endregion

        #region Fields

        private readonly IMeasureService _measureService;
        private readonly IShippingService _shippingService;
        private readonly ISettingService _settingService;
        private readonly UPSSettings _upsSettings;
        private readonly UPSSecurity _upsSecurity;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ILogger _logger;
        private readonly ILocalizationService _localizationService;
	    private readonly ICategoryService _categoryService;
	    private readonly IPriceCalculationService _priceCalculationService;
        private readonly IStateProvinceService _stateProvinceService;

	    #endregion

        #region Ctor
        public UPSComputationMethod(IMeasureService measureService,
            IShippingService shippingService, ISettingService settingService,
            UPSSettings upsSettings, ICountryService countryService,
            ICurrencyService currencyService, CurrencySettings currencySettings,
            IOrderTotalCalculationService orderTotalCalculationService, ILogger logger,
            ILocalizationService localizationService, ICategoryService categoryService, IPriceCalculationService priceCalculationService,
            IStateProvinceService stateProvinceService)
        {
            this._measureService = measureService;
            this._shippingService = shippingService;
            this._settingService = settingService;
            this._upsSettings = upsSettings;
            this._countryService = countryService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._logger = logger;
            this._localizationService = localizationService;
	        _categoryService = categoryService;
	        _priceCalculationService = priceCalculationService;
            _stateProvinceService = stateProvinceService;

            _upsSecurity = new UPSSecurity
            {
                UsernameToken = new UPSSecurityUsernameToken
                {
                    Username = upsSettings.Username,
                    Password = upsSettings.Password
                },
                ServiceAccessToken = new UPSSecurityServiceAccessToken
                {
                    AccessLicenseNumber = upsSettings.AccessKey
                }
            };
        }
        #endregion

        #region Utilities

        private string CreateRequest(string accessKey, string username, string password,
            GetShippingOptionRequest getShippingOptionRequest, UPSCustomerClassification customerClassification,
            UPSPickupType pickupType, UPSPackagingType packagingType)
        {
            var usedMeasureWeight = _measureService.GetMeasureWeightBySystemKeyword(MEASUREWEIGHTSYSTEMKEYWORD);
            if (usedMeasureWeight == null)
                throw new NopException(string.Format("UPS shipping service. Could not load \"{0}\" measure weight", MEASUREWEIGHTSYSTEMKEYWORD));

            var usedMeasureDimension = _measureService.GetMeasureDimensionBySystemKeyword(MEASUREDIMENSIONSYSTEMKEYWORD);
            if (usedMeasureDimension == null)
                throw new NopException(string.Format("UPS shipping service. Could not load \"{0}\" measure dimension", MEASUREDIMENSIONSYSTEMKEYWORD));

            int length = Convert.ToInt32(Math.Ceiling(_measureService.ConvertFromPrimaryMeasureDimension(getShippingOptionRequest.GetTotalLength(), usedMeasureDimension)));
            int height = Convert.ToInt32(Math.Ceiling(_measureService.ConvertFromPrimaryMeasureDimension(getShippingOptionRequest.GetTotalHeight(), usedMeasureDimension)));
            int width = Convert.ToInt32(Math.Ceiling(_measureService.ConvertFromPrimaryMeasureDimension(getShippingOptionRequest.GetTotalWidth(), usedMeasureDimension)));
            int weight = Convert.ToInt32(Math.Ceiling(_measureService.ConvertFromPrimaryMeasureWeight(_shippingService.GetShoppingCartTotalWeight(getShippingOptionRequest.Items), usedMeasureWeight)));
            if (length < 1)
                length = 1;
            if (height < 1)
                height = 1;
            if (width < 1)
                width = 1;
            if (weight < 1)
                weight = 1;

            string zipPostalCodeFrom = getShippingOptionRequest.ZipPostalCodeFrom;
            string zipPostalCodeTo = getShippingOptionRequest.ShippingAddress.ZipPostalCode;
	        string countryCodeFrom = "US"; //getShippingOptionRequest.CountryFrom.TwoLetterIsoCode;
            string countryCodeTo = getShippingOptionRequest.ShippingAddress.Country.TwoLetterIsoCode;

            var subtotal = _orderTotalCalculationService.GetShoppingCartSubTotal(getShippingOptionRequest.Items);

            var sb = new StringBuilder();
            sb.Append("<?xml version='1.0'?>");
            sb.Append("<AccessRequest xml:lang='en-US'>");
            sb.AppendFormat("<AccessLicenseNumber>{0}</AccessLicenseNumber>", accessKey);
            sb.AppendFormat("<UserId>{0}</UserId>", username);
            sb.AppendFormat("<Password>{0}</Password>", password);
            sb.Append("</AccessRequest>");
            sb.Append("<?xml version='1.0'?>");
            sb.Append("<RatingServiceSelectionRequest xml:lang='en-US'>");
            sb.Append("<Request>");
            sb.Append("<TransactionReference>");
            sb.Append("<CustomerContext>Bare Bones Rate Request</CustomerContext>");
            sb.Append("<XpciVersion>1.0001</XpciVersion>");
            sb.Append("</TransactionReference>");
            sb.Append("<RequestAction>Rate</RequestAction>");
            sb.Append("<RequestOption>Shop</RequestOption>");
            sb.Append("</Request>");
            if (String.Equals(countryCodeFrom, "US", StringComparison.InvariantCultureIgnoreCase) == true)
            {
                sb.Append("<PickupType>");
                sb.AppendFormat("<Code>{0}</Code>", GetPickupTypeCode(pickupType));
                sb.Append("</PickupType>");
                sb.Append("<CustomerClassification>");
                sb.AppendFormat("<Code>{0}</Code>", GetCustomerClassificationCode(customerClassification));
                sb.Append("</CustomerClassification>");
            }
            sb.Append("<Shipment>");
            sb.Append("<Shipper>");
            sb.Append("<Address>");
            sb.AppendFormat("<PostalCode>{0}</PostalCode>", zipPostalCodeFrom);
            sb.AppendFormat("<CountryCode>{0}</CountryCode>", countryCodeFrom);
            sb.Append("</Address>");
            sb.Append("</Shipper>");
            sb.Append("<ShipTo>");
            sb.Append("<Address>");
            sb.Append("<ResidentialAddressIndicator/>");
            sb.AppendFormat("<PostalCode>{0}</PostalCode>", zipPostalCodeTo);
            sb.AppendFormat("<CountryCode>{0}</CountryCode>", countryCodeTo);
            sb.Append("</Address>");
            sb.Append("</ShipTo>");
            sb.Append("<ShipFrom>");
            sb.Append("<Address>");
            sb.AppendFormat("<PostalCode>{0}</PostalCode>", zipPostalCodeFrom);
            sb.AppendFormat("<CountryCode>{0}</CountryCode>", countryCodeFrom);
            sb.Append("</Address>");
            sb.Append("</ShipFrom>");
            sb.Append("<Service>");
            sb.Append("<Code>03</Code>");
            sb.Append("</Service>");


            if ((!IsPackageTooHeavy(weight)) && (!IsPackageTooLarge(length, height, width)))
            {
                sb.Append("<Package>");
                sb.Append("<PackagingType>");
                sb.AppendFormat("<Code>{0}</Code>", GetPackagingTypeCode(packagingType));
                sb.Append("</PackagingType>");
                sb.Append("<Dimensions>");
                sb.AppendFormat("<Length>{0}</Length>", length);
                sb.AppendFormat("<Width>{0}</Width>", width);
                sb.AppendFormat("<Height>{0}</Height>", height);
                sb.Append("</Dimensions>");
                sb.Append("<PackageWeight>");
                sb.AppendFormat("<Weight>{0}</Weight>", weight);
                sb.Append("</PackageWeight>");

                if (_upsSettings.InsurePackage)
                {
                    //The maximum declared amount per package: 50000 USD.
                    int packageInsurancePrice = Convert.ToInt32(subtotal);
                    sb.Append("<PackageServiceOptions>");
                    sb.Append("<InsuredValue>");
                    sb.AppendFormat("<CurrencyCode>{0}</CurrencyCode>", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);
                    sb.AppendFormat("<MonetaryValue>{0}</MonetaryValue>", packageInsurancePrice);
                    sb.Append("</InsuredValue>");
                    sb.Append("</PackageServiceOptions>");
                }
                sb.Append("</Package>");
            }
            else
            {
                int totalPackages = 1;
                int totalPackagesDims = 1;
                int totalPackagesWeights = 1;
                if (IsPackageTooHeavy(weight))
                {
                    totalPackagesWeights = Convert.ToInt32(Math.Ceiling((decimal)weight / (decimal)MAXPACKAGEWEIGHT));
                }
                if (IsPackageTooLarge(length, height, width))
                {
                    totalPackagesDims = Convert.ToInt32(Math.Ceiling((decimal)TotalPackageSize(length, height, width) / (decimal)108));
                }
                totalPackages = totalPackagesDims > totalPackagesWeights ? totalPackagesDims : totalPackagesWeights;
                if (totalPackages == 0)
                    totalPackages = 1;

                int weight2 = weight / totalPackages;
                int height2 = height / totalPackages;
                int width2 = width / totalPackages;
                int length2 = length / totalPackages;
                if (weight2 < 1)
                    weight2 = 1;
                if (height2 < 1)
                    height2 = 1;
                if (width2 < 1)
                    width2 = 1;
                if (length2 < 1)
                    length2 = 1;

                //The maximum declared amount per package: 50000 USD.
                int packageInsurancePrice = Convert.ToInt32(subtotal / totalPackages);

                for (int i = 0; i < totalPackages; i++)
                {
                    sb.Append("<Package>");
                    sb.Append("<PackagingType>");
                    sb.AppendFormat("<Code>{0}</Code>", GetPackagingTypeCode(packagingType));
                    sb.Append("</PackagingType>");
                    sb.Append("<Dimensions>");
                    sb.AppendFormat("<Length>{0}</Length>", length2);
                    sb.AppendFormat("<Width>{0}</Width>", width2);
                    sb.AppendFormat("<Height>{0}</Height>", height2);
                    sb.Append("</Dimensions>");
                    sb.Append("<PackageWeight>");
                    sb.AppendFormat("<Weight>{0}</Weight>", weight2);
                    sb.Append("</PackageWeight>");

                    if (_upsSettings.InsurePackage)
                    {
                        sb.Append("<PackageServiceOptions>");
                        sb.Append("<InsuredValue>");
                        sb.AppendFormat("<CurrencyCode>{0}</CurrencyCode>", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);
                        sb.AppendFormat("<MonetaryValue>{0}</MonetaryValue>", packageInsurancePrice);
                        sb.Append("</InsuredValue>");
                        sb.Append("</PackageServiceOptions>");
                    }

                    sb.Append("</Package>");
                }
            }


            sb.Append("</Shipment>");
            sb.Append("</RatingServiceSelectionRequest>");
            string requestString = sb.ToString();
            return requestString;
        }

        private string DoRequest(string url, string requestString)
        {
            byte[] bytes = new ASCIIEncoding().GetBytes(requestString);
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;
            var requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            var response = request.GetResponse();
            string responseXML = string.Empty;
            using (var reader = new StreamReader(response.GetResponseStream()))
                responseXML = reader.ReadToEnd();

            return responseXML;
        }

        private string GetCustomerClassificationCode(UPSCustomerClassification customerClassification)
        {
            switch (customerClassification)
            {
                case UPSCustomerClassification.Wholesale:
                    return "01";
                case UPSCustomerClassification.Occasional:
                    return "03";
                case UPSCustomerClassification.Retail:
                    return "04";
                default:
                    throw new NopException("Unknown UPS customer classification code");
            }
        }

        private string GetPackagingTypeCode(UPSPackagingType packagingType)
        {
            switch (packagingType)
            {
                case UPSPackagingType.Letter:
                    return "01";
                case UPSPackagingType.CustomerSuppliedPackage:
                    return "02";
                case UPSPackagingType.Tube:
                    return "03";
                case UPSPackagingType.PAK:
                    return "04";
                case UPSPackagingType.ExpressBox:
                    return "21";
                case UPSPackagingType._10KgBox:
                    return "25";
                case UPSPackagingType._25KgBox:
                    return "24";
                default:
                    throw new NopException("Unknown UPS packaging type code");
            }
        }

        private string GetPickupTypeCode(UPSPickupType pickupType)
        {
            switch (pickupType)
            {
                case UPSPickupType.DailyPickup:
                    return "01";
                case UPSPickupType.CustomerCounter:
                    return "03";
                case UPSPickupType.OneTimePickup:
                    return "06";
                case UPSPickupType.OnCallAir:
                    return "07";
                case UPSPickupType.SuggestedRetailRates:
                    return "11";
                case UPSPickupType.LetterCenter:
                    return "19";
                case UPSPickupType.AirServiceCenter:
                    return "20";
                default:
                    throw new NopException("Unknown UPS pickup type code");
            }
        }

        private string GetServiceName(string serviceId)
        {
            switch (serviceId)
            {
                case "01":
                    return "UPS Next Day Air";
                case "02":
                    return "UPS 2nd Day Air";
                case "03":
                    return "UPS Ground";
                case "07":
                    return "UPS Worldwide Express";
                case "08":
                    return "UPS Worldwide Expedited";
                case "11":
                    return "UPS Standard";
                case "12":
                    return "UPS 3 Day Select";
                case "13":
                    return "UPS Next Day Air Saver";
                case "14":
                    return "UPS Next Day Air Early A.M.";
                case "54":
                    return "UPS Worldwide Express Plus";
                case "59":
                    return "UPS 2nd Day Air A.M.";
                case "65":
                    return "UPS Saver";
                case "82": //82-86, for Polish Domestic Shipments
                    return "UPS Today Standard";
                case "83":
                    return "UPS Today Dedicated Courier";
                case "85":
                    return "UPS Today Express";
                case "86":
                    return "UPS Today Express Saver";
                default:
                    return "Unknown";
            }
        }

        private bool IsPackageTooLarge(int length, int height, int width)
        {
            int total = TotalPackageSize(length, height, width);
            if (total > 165)
                return true;
            else
                return false;
        }

        private int TotalPackageSize(int length, int height, int width)
        {
            int girth = height + height + width + width;
            int total = girth + length;
            return total;
        }

        private bool IsPackageTooHeavy(int weight)
        {
            if (weight > MAXPACKAGEWEIGHT)
                return true;
            else
                return false;
        }

        private List<ShippingOption> ParseResponse(string response, ref string error)
        {
            var shippingOptions = new List<ShippingOption>();

            string carrierServicesOffered =
                UPSSettings.DeserializeString(_upsSettings.CarrierServicesOffered).Select(s => ((UPSService)Enum.Parse(typeof(UPSService), s)).GetAttribute<UPSService, CodeAttribute>().Value)
                    .Aggregate((a, b) => string.Format("[{0}]:[{1}]", a, b));

            using (var sr = new StringReader(response))
            using (var tr = new XmlTextReader(sr))
                while (tr.Read())
                {
                    if ((tr.Name == "Error") && (tr.NodeType == XmlNodeType.Element))
                    {
                        string errorText = "";
                        while (tr.Read())
                        {
                            if ((tr.Name == "ErrorCode") && (tr.NodeType == XmlNodeType.Element))
                            {
                                errorText += "UPS Rating Error, Error Code: " + tr.ReadString() + ", ";
                            }
                            if ((tr.Name == "ErrorDescription") && (tr.NodeType == XmlNodeType.Element))
                            {
                                errorText += "Error Desc: " + tr.ReadString();
                            }
                        }
                        error = "UPS Error returned: " + errorText;
                    }
                    if ((tr.Name == "RatedShipment") && (tr.NodeType == XmlNodeType.Element))
                    {
                        string serviceCode = "";
                        string monetaryValue = "";
                        while (tr.Read())
                        {
                            if ((tr.Name == "Service") && (tr.NodeType == XmlNodeType.Element))
                            {
                                while (tr.Read())
                                {
                                    if ((tr.Name == "Code") && (tr.NodeType == XmlNodeType.Element))
                                    {
                                        serviceCode = tr.ReadString();
                                        tr.ReadEndElement();
                                    }
                                    if ((tr.Name == "Service") && (tr.NodeType == XmlNodeType.EndElement))
                                    {
                                        break;
                                    }
                                }
                            }
                            if (((tr.Name == "RatedShipment") && (tr.NodeType == XmlNodeType.EndElement)) || ((tr.Name == "RatedPackage") && (tr.NodeType == XmlNodeType.Element)))
                            {
                                break;
                            }
                            if ((tr.Name == "TotalCharges") && (tr.NodeType == XmlNodeType.Element))
                            {
                                while (tr.Read())
                                {
                                    if ((tr.Name == "MonetaryValue") && (tr.NodeType == XmlNodeType.Element))
                                    {
                                        monetaryValue = tr.ReadString();
                                        tr.ReadEndElement();
                                    }
                                    if ((tr.Name == "TotalCharges") && (tr.NodeType == XmlNodeType.EndElement))
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        string service = GetServiceName(serviceCode);
                        string serviceId = String.Format("[{0}]", serviceCode);

                        // Go to the next rate if the service ID is not in the list of services to offer
                        if (!String.IsNullOrEmpty(carrierServicesOffered) && !carrierServicesOffered.Contains(serviceId))
                        {
                            continue;
                        }

                        //Weed out unwanted or unkown service rates
                        if (service.ToUpper() != "UNKNOWN")
                        {
                            var shippingOption = new ShippingOption();
                            shippingOption.Rate = Convert.ToDecimal(monetaryValue, new CultureInfo("en-US"));
                            shippingOption.Name = service;
                            shippingOptions.Add(shippingOption);
                        }

                    }
                }

            return shippingOptions;
        }

        private ShipConfirmRequest PrepareShippingRequest(UpsShippingOptionRequest upsRequest)
        {
            if (upsRequest == null) return new ShipConfirmRequest();

            var shipRequest = new ShipConfirmRequest
            {
                Request = new Ship.RequestType {RequestOption = new[] {"validate"}},
                Shipment = new ShipmentType
                {
                    Service = new ServiceType
                    {
                        Code =
                            ((UPSService) Enum.Parse(typeof (UPSService), upsRequest.ShippingMethod))
                                .GetAttribute<UPSService, CodeAttribute>().Value,
                        Description =
                            ((UPSService) Enum.Parse(typeof (UPSService), upsRequest.ShippingMethod))
                                .GetAttribute<UPSService, NameAttribute>().Value
                    },
                    PaymentInformation = new PaymentInfoType
                    {
                        ShipmentCharge = new[]
                        {
                            new ShipmentChargeType
                            {
                                BillShipper = new BillShipperType
                                {
                                    AccountNumber = upsRequest.ShipperNumber
                                },
                                Type = "01"
                            }
                        }
                    },
                    Shipper = new ShipperType
                    {
                        ShipperNumber = upsRequest.ShipperNumber,
                        Name = upsRequest.ShipperAddress.Company,
                        Address = new ShipAddressType
                        {
                            AddressLine = new[] {upsRequest.ShipperAddress.Address1, upsRequest.ShipperAddress.Address2},
                            City = upsRequest.ShipperAddress.City,
                            StateProvinceCode = upsRequest.ShipperAddress.StateProvince.Abbreviation,
                            CountryCode = upsRequest.ShipperAddress.Country.TwoLetterIsoCode,
                            PostalCode = upsRequest.ShipperAddress.ZipPostalCode
                        },
                        EMailAddress = upsRequest.ShipperAddress.Email
                    },
                    ShipFrom = new ShipFromType
                    {
                        Name = upsRequest.ShipperAddress.Company,
                        Address = new ShipAddressType
                        {
                            AddressLine = new[] {upsRequest.ShipperAddress.Address1, upsRequest.ShipperAddress.Address2},
                            City = upsRequest.ShipperAddress.City,
                            StateProvinceCode = upsRequest.ShipperAddress.StateProvince.Abbreviation,
                            CountryCode = upsRequest.ShipperAddress.Country.TwoLetterIsoCode,
                            PostalCode = upsRequest.ShipperAddress.ZipPostalCode
                        }
                    },
                    ShipTo = new ShipToType
                    {
                        Name =
                            string.Format("{0} {1}", upsRequest.Customer.ShippingAddress.FirstName,
                                upsRequest.Customer.ShippingAddress.LastName),
                        Address = new ShipToAddressType
                        {
                            AddressLine =
                                new[] {upsRequest.ShippingAddress.Address1, upsRequest.ShippingAddress.Address2},
                            City = upsRequest.ShippingAddress.City,
                            StateProvinceCode = upsRequest.ShippingAddress.StateProvince.Abbreviation,
                            CountryCode = upsRequest.ShippingAddress.Country.TwoLetterIsoCode,
                            PostalCode = upsRequest.ShippingAddress.ZipPostalCode,
                            ResidentialAddressIndicator = upsRequest.ResidentialAddress ? "" : null
                        }
                    },
                    ShipmentRatingOptions = new RateInfoType
                    {
                        NegotiatedRatesIndicator = ""
                    },
                    Package = upsRequest.Packages.Select(p => new PackageType
                    {
                        PackageWeight = new PackageWeightType
                        {
                            Weight = p.PackageWeight.ToString("F4"),
                            UnitOfMeasurement = new ShipUnitOfMeasurementType {Code = "LBS", Description = "pounds"}
                        },
                        Dimensions = new DimensionsType
                        {
                            Length = p.Length.ToString(),
                            Width = p.Width.ToString(),
                            Height = p.Height.ToString(),
                            UnitOfMeasurement = new ShipUnitOfMeasurementType {Code = "IN", Description = "inches"}
                        },
                        Packaging = new PackagingType
                        {
                            Code =
                                ((UPSPackagingType) Enum.Parse(typeof (UPSPackagingType), upsRequest.PackageType))
                                    .GetAttribute<UPSPackagingType, CodeAttribute>().Value,
                            Description =
                                ((UPSPackagingType) Enum.Parse(typeof (UPSPackagingType), upsRequest.PackageType))
                                    .GetAttribute<UPSPackagingType, DescriptionAttribute>().Description
                        },
                        ReferenceNumber = new[]
                        {
                            new ReferenceNumberType
                            {
                                Value = upsRequest.OrderId.ToString()
                            }
                        },
                        PackageServiceOptions = new PackageServiceOptionsType
                        {
                            DeclaredValue =
                                p.DeclaredValue > 0
                                    ? new PackageDeclaredValueType
                                    {
                                        MonetaryValue = p.DeclaredValue.ToString(),
                                        CurrencyCode = "USD"
                                    }
                                    : null,
                            DeliveryConfirmation =
                                (upsRequest.ConfirmationDelivery || upsRequest.RequireSignature)
                                    ? new DeliveryConfirmationType {DCISType = upsRequest.RequireSignature ? "2" : "1"}
                                    : null
                        }
                    }).ToArray()
                },
                LabelSpecification = new LabelSpecificationType
                {
                    LabelImageFormat = new LabelImageFormatType
                    {
                        Code = "GIF"
                    },
                    HTTPUserAgent = "Mozilla/4.5",
                    LabelStockSize = new LabelStockSizeType
                    {
                        Width = "4",
                        Height = "6"
                    }
                },
                ReceiptSpecification = new ReceiptSpecificationType
                {
                    ImageFormat = new ReceiptImageFormatType
                    {
                        Code = "HTML"
                    }
                }
            };
            var serviceOptions = new ShipmentTypeShipmentServiceOptions();

            if (upsRequest.SendEmailNotification)
            {
                serviceOptions.Notification = new[]
                {
                    new NotificationType
                    {
                        EMail = new EmailDetailsType
                        {
                            EMailAddress = new[] {upsRequest.ShippingAddress.Email, upsRequest.ShipperAddress.Email}
                        },
                        NotificationCode = "6"
                    }
                };
            }

            if (upsRequest.SaturdayDelivery)
            {
                serviceOptions.SaturdayDeliveryIndicator = "";
            }

            if (upsRequest.UpsCarbonNeutral)
            {
                serviceOptions.UPScarbonneutralIndicator = "";
            }

            shipRequest.Shipment.ShipmentServiceOptions = serviceOptions;

            if (upsRequest.OversizePackage)
            {
                shipRequest.Shipment.IrregularIndicator = "2";
            }

            return shipRequest;
        }

        private GetShippingOptionResponse PrepareShippingOptionResponse(ShipConfirmResponse upsResponse, UPSService service)
        {
            var response = new GetShippingOptionResponse();
            var serviceName = service.GetAttribute<UPSService, NameAttribute>().Value;

            var shippingOptions = new List<ShippingOption>
            {
                new ShippingOption
                {
                    Name = serviceName,
                    Description = upsResponse.ShipmentResults.ShipmentDigest,
                    Rate =
                        Convert.ToDecimal(upsResponse.ShipmentResults.NegotiatedRateCharges != null
                            ? upsResponse.ShipmentResults.NegotiatedRateCharges.TotalCharge.MonetaryValue
                            : upsResponse.ShipmentResults.ShipmentCharges.TotalCharges.MonetaryValue)
                }
            };

            response.ShippingOptions = shippingOptions;
            return response;
        }

        private GetShippingOptionResponse PrepareShippingOptionResponse(ShipAcceptResponse upsResponse)
        {
            var response = new GetShippingOptionResponse();
            var shippingOptions = new List<ShippingOption>();
            var hasClr = upsResponse.ShipmentResults.ControlLogReceipt != null
                         && upsResponse.ShipmentResults.ControlLogReceipt[0] != null
                         && !string.IsNullOrWhiteSpace(upsResponse.ShipmentResults.ControlLogReceipt[0].GraphicImage);
            var clr = string.Empty;

            if (hasClr)
            {
                var filebytes = Convert.FromBase64String(upsResponse.ShipmentResults.ControlLogReceipt[0].GraphicImage);
                clr = System.Text.Encoding.Default.GetString(filebytes);
            }

            upsResponse.ShipmentResults.PackageResults.ToList().ForEach(package =>
            {
                var so = new ShippingOption
                {
                    Name = "UPS",
                    Description = package.TrackingNumber,
                    Rate =
                        Convert.ToDecimal(upsResponse.ShipmentResults.NegotiatedRateCharges != null
                            ? upsResponse.ShipmentResults.NegotiatedRateCharges.TotalCharge.MonetaryValue
                            : upsResponse.ShipmentResults.ShipmentCharges.TotalCharges.MonetaryValue)
                };

                // Rotating 90 degree
                var graphicImage = package.ShippingLabel.GraphicImage;
                var imgData = Convert.FromBase64String(graphicImage);
                var ms = new MemoryStream(imgData);

                var bitmap = new Bitmap(Image.FromStream(ms));
                bitmap = bitmap.Clone(new Rectangle(0, 0, 1200, 800), bitmap.PixelFormat);
                bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);

                var ms2 = new MemoryStream();
                bitmap.Save(ms2, ImageFormat.Gif);
                ms2.Position = 0;
                graphicImage = Convert.ToBase64String(ms2.ToArray());

                bitmap.Dispose();

                so.AdditionalInfo = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("ReceiptUrl", package.ShippingReceipt),
                    new KeyValuePair<string, object>("Label", graphicImage),
                    new KeyValuePair<string, object>("TrackingNumber", package.TrackingNumber)
                };

                if (hasClr)
                    so.AdditionalInfo.Add(new KeyValuePair<string, object>("ControlLogReceipt", clr));

                shippingOptions.Add(so);
            });

            response.ShippingOptions = shippingOptions;
            return response;
            
        }

        private XAVRequest PrepareAddressValidationRequest(Address address)
        {
            var xavRequest = new XAVRequest
            {
                Request = new XAV.RequestType
                {
                    RequestOption = new[] { "1" },
                },
                MaximumCandidateListSize = "50",
                AddressKeyFormat = new AddressKeyFormatType
                {
                    AddressLine = new[] {address.Address1, address.Address2},
                    PoliticalDivision2 = address.City,
                    PoliticalDivision1 = address.StateProvince.Abbreviation,
                    PostcodePrimaryLow = address.ZipPostalCode,
                    CountryCode = address.Country.TwoLetterIsoCode
                }
            };

            return xavRequest;
        }

        private AddressValidationResponse PrepareAddressValidationResponse(XAVResponse xavResponse)
        {
            var response = new AddressValidationResponse
            {
                StatusCode = (AddressValidationResponse.ValidationStatus)
                    Enum.Parse(typeof (AddressValidationResponse.ValidationStatus),
                        xavResponse.ItemElementName.ToString())
            };

            if (xavResponse.Candidate != null && xavResponse.Candidate.Any())
            {
                response.Candidates = xavResponse.Candidate.Select(c =>
                {
                    var state =
                        _stateProvinceService.GetStateProvinceByAbbreviation(c.AddressKeyFormat.PoliticalDivision1);
                    return new Address
                    {
                        Address1 = c.AddressKeyFormat.AddressLine.ToList().ElementAtOrDefault(0),
                        Address2 = c.AddressKeyFormat.AddressLine.ToList().ElementAtOrDefault(1),
                        City = c.AddressKeyFormat.PoliticalDivision2,
                        StateProvince =
                            new StateProvince {Abbreviation = state.Abbreviation, Name = state.Name, Id = state.Id},
                        Country = _countryService.GetCountryByTwoLetterIsoCode(c.AddressKeyFormat.CountryCode),
                        ZipPostalCode = c.AddressKeyFormat.PostcodePrimaryLow
                    };
                }).ToList();
            }

            return response;
        }

        private XAV.UPSSecurity ToXavUpsSecurity(UPSSecurity upsSecurity)
        {
            return new XAV.UPSSecurity
            {
                UsernameToken = new XAV.UPSSecurityUsernameToken
                {
                    Username = upsSecurity.UsernameToken.Username,
                    Password = upsSecurity.UsernameToken.Password
                },
                ServiceAccessToken = new XAV.UPSSecurityServiceAccessToken
                {
                    AccessLicenseNumber = upsSecurity.ServiceAccessToken.AccessLicenseNumber
                }
            };
        }

        #endregion

        #region Methods

        /// <summary>
        ///  Gets available shipping options
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Represents a response of getting shipping rate options</returns>
        public GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest)
        {
            if (getShippingOptionRequest == null)
                throw new ArgumentNullException("getShippingOptionRequest");

            var response = new GetShippingOptionResponse();

            if (getShippingOptionRequest.Items == null || !getShippingOptionRequest.Items.Any())
            {
                response.AddError("No shipment items");
                return response;
            }

            if (getShippingOptionRequest.ShippingAddress == null)
            {
                response.AddError("Shipping address is not set");
                return response;
            }

            if (getShippingOptionRequest.ShippingAddress.Country == null)
            {
                response.AddError("Shipping country is not set");
                return response;
            }

			var accessories = getShippingOptionRequest.Items
				.Where(i => i.ProductVariant.Product.SiteId == KnownIds.Website.Corporate)
				.ToList();

			var usedItems = getShippingOptionRequest.Items.Except(accessories).ToList();

			decimal rate;

			if (accessories.Any() && !usedItems.Any()) {
				// for accessories only, charge a flat $8 or free if total is over $25
				var total = _orderTotalCalculationService.GetShoppingCartSubTotal(accessories);
				rate = total > 25m ? 0m : 8m;
			}
			else {
				// for used products or mixed, calc shipping based only on used products (accessories ship free)
				rate = usedItems.Sum(i => _priceCalculationService.GetMgrUsedProductShippingPrice(i.ProductVariant) * i.Quantity);
			}

	        response.ShippingOptions.Add(new ShippingOption {
				Name = "Standard Ground Delivery",
				Rate = rate
	        });

	        return response;

			//***************************************************************************************************************************************
			// WE ARE NO LONGER ACTUALLY GETTING RATES FROM UPS - WE'RE GOING STRICTLY FLAT RATE. WINMARK WILL CHANGE THEIR MIND AGAIN, AND WHEN THEY
			// DO, WE NEED TO GET THE CODE BELOW WORKING AGAIN (TM 9/19/13)
			//***************************************************************************************************************************************

			// remove items with flat-rate shipping from calculation
			//getShippingOptionRequest.Items = getShippingOptionRequest.Items.Except(flatRateItems).ToList();

			// if only flat-rate items are left, return now (calling the service will always return some minumum amount, and we don't want that)
			//if (!getShippingOptionRequest.Items.Any()) {
			//	response.ShippingOptions.Add(new ShippingOption {
			//		Name = "UPS Ground",
			//		Rate = flatRateTotal
			//	});
			//	return response;				
			//}

//			if (getShippingOptionRequest.CountryFrom == null)
//			{
//				getShippingOptionRequest.CountryFrom = _countryService.GetCountryById(_upsSettings.DefaultShippedFromCountryId);
//				if (getShippingOptionRequest.CountryFrom == null)
//					getShippingOptionRequest.CountryFrom = _countryService.GetAllCountries(true).ToList().FirstOrDefault();
//			}
//			if (String.IsNullOrEmpty(getShippingOptionRequest.ZipPostalCodeFrom))
//				getShippingOptionRequest.ZipPostalCodeFrom = _upsSettings.DefaultShippedFromZipPostalCode;

//			string requestXml = CreateRequest(
//				_upsSettings.AccessKey, _upsSettings.Username, _upsSettings.Password, getShippingOptionRequest,
//				_upsSettings.CustomerClassification, _upsSettings.PickupType, _upsSettings.PackagingType);

//			string responseXml = DoRequest(_upsSettings.Url, requestXml);

//#if (DEBUG)
//			File.WriteAllText("c:\\ups-request.xml", requestXml.Substring(requestXml.LastIndexOf("<?xml")));
//			File.WriteAllText("c:\\ups-response.xml", responseXml);
//#endif

//			string error = "";
//			var shippingOptions = ParseResponse(responseXml, ref error);
//			if (String.IsNullOrEmpty(error))
//			{
//				foreach (var shippingOption in shippingOptions)
//				{
//					System.Diagnostics.Debug.WriteLine("UPS shipping rate: $" + shippingOption.Rate);

//					if (!shippingOption.Name.ToLower().StartsWith("ups"))
//						shippingOption.Name = string.Format("UPS {0}", shippingOption.Name);

//					shippingOption.Rate += _upsSettings.AdditionalHandlingCharge;

//					// Treefort enhancement - Winmark wants a 25% freight upcharge on top of UPS rates
//					// ...nope, UPS rates aren't reflecting Winmark's discount so rates are coming back too high. skip the markup until they change their mind again tomorrow.
//					// shippingOption.Rate *= 1.25m;

//					// Yes, another Treefort enhancement. We removed these from the rate calc, but still want to include them in the UPS option
//					shippingOption.Rate += flatRateTotal;

//					response.ShippingOptions.Add(shippingOption);
//				}
//			}
//			else
//			{
//				response.AddError(error);
//			}

//			return response;
        }

        /// <summary>
        /// Gets fixed shipping rate (if shipping rate computation method allows it and the rate can be calculated before checkout).
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Fixed shipping rate; or null in case there's no fixed shipping rate</returns>
        public decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest)
        {
            return null;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "ShippingUPS";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Shipping.UPS.Controllers" }, { "area", null } };
        }

        public GetShippingOptionResponse GetShippingRate(GetShippingOptionRequest request)
        {
            var upsRequest = request as UpsShippingOptionRequest;
            var shipRequest = PrepareShippingRequest(upsRequest);
            var serviceUrl = _upsSettings.ShipUrl;

            var upsUserInfo = new UPSSecurity
            {
                UsernameToken = new UPSSecurityUsernameToken
                {
                    Username = upsRequest.ShipperUsername,
                    Password = upsRequest.UpsPassword
                },
                ServiceAccessToken =
                    new UPSSecurityServiceAccessToken {AccessLicenseNumber = upsRequest.ShipperDeveloperKey}
            };
            var shipService = new ShipService(serviceUrl) {UPSSecurityValue = upsUserInfo};

            var shipResponse = new ShipConfirmResponse();

            var response = new GetShippingOptionResponse();

            try
            {
                shipResponse = shipService.ProcessShipConfirm(shipRequest);
                response = PrepareShippingOptionResponse(shipResponse, (UPSService)Enum.Parse(typeof(UPSService), upsRequest.ShippingMethod));
            }
            catch (SoapException se)
            {
                // Sanitize UPS nasty xml node names
                response.AddError(se.Detail.InnerXml.Replace("err:", ""));
            }
            catch (Exception e)
            {
                response.AddError(e.Message);
            }

            return response;
        }

        public GetShippingOptionResponse GetAllShippingOptionsAdmin()
        {
            var shippingOptions = UPSSettings.DeserializeString(_upsSettings.CarrierServicesOffered).Select(s =>
            {
                var service = (UPSService) Enum.Parse(typeof (UPSService), s);
                return new ShippingOption
                {
                    ShippingRateComputationMethodSystemName = service.ToString(),
                    Name = service.ToString(),
                    Description = service.GetAttribute<UPSService, NameAttribute>().Value
                };
            }).ToList();

            return new GetShippingOptionResponse {ShippingOptions = shippingOptions};
        }

        public IList<KeyValuePair<string, string>> GetPackagingTypes()
        {
            var enums = Enum.GetValues(typeof (UPSPackagingType)).Cast<UPSPackagingType>();
            return enums.Select(p => new KeyValuePair<string, string>(p.ToString(),
                p.GetAttribute<UPSPackagingType, DescriptionAttribute>().Description)).ToList();
        }

        public AddressValidationResponse ValidateAddress(Address address)
        {
            var xavRequest = PrepareAddressValidationRequest(address);
            var serviceUrl = _upsSettings.AddressValidationUrl;

            var service = new XAVService(serviceUrl)
            {
                UPSSecurityValue = ToXavUpsSecurity(_upsSecurity)
            };
            var xavResponse = new XAVResponse();
            try
            {
                xavResponse = service.ProcessXAV(xavRequest);
            }
            catch (Exception e)
            {
                var error = e.Message;
            }
            var response = PrepareAddressValidationResponse(xavResponse);

            return response;
        }

        public GetShippingOptionResponse AcceptShipmentQuote(Object request)
        {
            var shipmentDigest = request as string;
            var acceptRequest = new ShipAcceptRequest
            {
                Request = new Ship.RequestType {RequestOption = new[] {"nonvalidate"}},
                ShipmentDigest = shipmentDigest
            };

            var serviceUrl = _upsSettings.ShipUrl;
            var shipService = new ShipService(serviceUrl) { UPSSecurityValue = _upsSecurity };
            var shipResponse = new ShipAcceptResponse();

            var response = new GetShippingOptionResponse();

            try
            {
                shipResponse = shipService.ProcessShipAccept(acceptRequest);
                response = PrepareShippingOptionResponse(shipResponse);
            }
            catch (SoapException se)
            {
                response.AddError(se.Detail.InnerText);
            }
            catch (Exception e)
            {
                response.AddError(e.Message);
            }

            return response;
        }
        
        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new UPSSettings()
            {
                Url = "https://www.ups.com/ups.app/xml/Rate",
                ShipUrl = "https://wwwcie.ups.com/webservices/Ship",
                AddressValidationUrl = "https://wwwcie.ups.com/webservices/XAV",
                AccessKey = "6CE4FBB8EB9E7695",
                Username = "treefort_MGR",
                Password = "Crash4317",
                CustomerClassification = UPSCustomerClassification.Retail,
                PickupType = UPSPickupType.OneTimePickup,
                PackagingType = UPSPackagingType.ExpressBox,
                DefaultShippedFromZipPostalCode = "10001",
                CarrierServicesOffered =
                    UPSSettings.SerializeString(new[]
                    {
                        UPSService.UpsGround.ToString(), UPSService.UpsStandard.ToString(),
                        UPSService.Ups3DaySelect.ToString(), UPSService.UpsNextDayAir.ToString(),
                        UPSService.UpsNextDayAirSaver.ToString(), UPSService.UpsNextDayAirEarlyAM.ToString()
                    })
            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.Url", "URL");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.Url.Hint", "Specify UPS URL.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.AccessKey", "Access Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.AccessKey.Hint", "Specify UPS access key.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.Username", "Username");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.Username.Hint", "Specify UPS username.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.Password", "Password");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.Password.Hint", "Specify UPS password.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.AdditionalHandlingCharge", "Additional handling charge");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.AdditionalHandlingCharge.Hint", "Enter additional handling fee to charge your customers.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.InsurePackage", "Insure package");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.InsurePackage.Hint", "Check to ensure packages.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.CustomerClassification", "UPS Customer Classification");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.CustomerClassification.Hint", "Choose customer classification.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.PickupType", "UPS Pickup Type");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.PickupType.Hint", "Choose UPS pickup type.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.PackagingType", "UPS Packaging Type");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.PackagingType.Hint", "Choose UPS packaging type.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromCountry", "Shipped from country");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromCountry.Hint", "Specify origin country.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromZipPostalCode", "Shipped from zip");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromZipPostalCode.Hint", "Specify origin zip code.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.AvailableCarrierServices", "Carrier Services");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Fields.AvailableCarrierServices.Hint", "Select the services you want to offer to customers.");
            //tracker events
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Departed", "Departed");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.ExportScanned", "Export scanned");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.OriginScanned", "Origin scanned");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Arrived", "Arrived");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.NotDelivered", "Not delivered");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Booked", "Booked");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Delivered", "Delivered");

            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<UPSSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.Url");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.Url.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.AccessKey");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.AccessKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.Username");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.Username.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.Password");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.Password.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.AdditionalHandlingCharge");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.AdditionalHandlingCharge.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.InsurePackage");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.InsurePackage.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.CustomerClassification");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.CustomerClassification.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.PickupType");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.PickupType.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.PackagingType");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.PackagingType.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromCountry");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromCountry.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromZipPostalCode");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.DefaultShippedFromZipPostalCode.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.AvailableCarrierServices");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Fields.AvailableCarrierServices.Hint");
            //tracker events
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Departed");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.ExportScanned");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.OriginScanned");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Arrived");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.NotDelivered");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Booked");
            this.DeletePluginLocaleResource("Plugins.Shipping.UPS.Tracker.Delivered");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a shipping rate computation method type
        /// </summary>
        public ShippingRateComputationMethodType ShippingRateComputationMethodType
        {
            get
            {
                return ShippingRateComputationMethodType.Realtime;
            }
        }

        /// <summary>
        /// Gets a shipment tracker
        /// </summary>
        public IShipmentTracker ShipmentTracker
        {
            get { return new UPSShipmentTracker(_logger, _localizationService, _upsSettings); }
        }

        #endregion
    }
}