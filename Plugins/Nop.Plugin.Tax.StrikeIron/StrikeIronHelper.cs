﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Plugin.Tax.StrikeIron.SpeedTax;
using Nop.Services.Tax;

namespace Nop.Plugin.Tax.StrikeIron
{
    public class StrikeIronHelper
    {
        private const string TAXRATE_KEY = "Nop.taxrate.zipFrom-{0}.zipTo-{1}.price-{2}";

        private readonly CalculateTaxRequest _calculateTaxRequest;
        private readonly StrikeIronTaxSettings _strikeIronTaxSettings;
        private readonly IOutOfProcessCacheManager _cacheManager;

        private LicenseInfo _licenseInfo;
        private LicenseInfo licenseInfo
        {
            get
            {
                return _licenseInfo ?? (_licenseInfo = new LicenseInfo()
                {
                    RegisteredUser = new RegisteredUser()
                    {
                        UserID = _strikeIronTaxSettings.UserId,
                        Password = _strikeIronTaxSettings.Password
                    }
                });
            }
        }

        public IOutOfProcessCacheManager CacheManager
        {
            get { return _cacheManager; }
        }


        public StrikeIronHelper(CalculateTaxRequest calculateTaxRequest, StrikeIronTaxSettings strikeIronTaxSettings, IOutOfProcessCacheManager cacheManager)
        {
            this._calculateTaxRequest = calculateTaxRequest;
            this._strikeIronTaxSettings = strikeIronTaxSettings;
            this._cacheManager = cacheManager;
        }

        public decimal GetTaxRate(out List<string> errors)
        {
            decimal taxRate;
            errors = new List<string>();

            var shipFrom = toSpeedTaxAddress(_calculateTaxRequest.StoreAddress);
            var shipTo = _calculateTaxRequest.InStorePickup ? shipFrom : toSpeedTaxAddress(_calculateTaxRequest.CustomerAddress);

            if (!CheckCache(shipFrom.ZIPCode.Trim(), shipTo.ZIPCode.Trim(), _calculateTaxRequest.Price, out taxRate))
            {
                var speedTax = new SpeedTaxSoapClient();

                try
                {
                    SIWsOutputOfTaxValueOutput retVal;
                    var taxValueRequests = new[]
                    {
                        new TaxValueRequest
                        {
                            Amount = (double)_calculateTaxRequest.Price,
                            SalesTaxCategoryOrCategoryID = "General Merchandise"
                        }
                    };

                    speedTax.GetSalesTaxValue(licenseInfo, shipFrom, shipTo, taxValueRequests, out retVal);

                    if (retVal.ServiceStatus.StatusNbr >= 300)
                    {
                        errors.Add(string.Format("{0} {1}", retVal.ServiceStatus.StatusNbr,
                            retVal.ServiceStatus.StatusDescription));
                    }
                    else
                    {
                        taxRate = retVal.ServiceResult.SalesTaxTotal / (_calculateTaxRequest.Price/100);
                        taxRate = decimal.Round(taxRate, 4);
                        CacheManager.Set(GetKey(shipFrom.ZIPCode.Trim(), shipTo.ZIPCode.Trim(), _calculateTaxRequest.Price), taxRate, 60);
                    }
                }
                catch (Exception exception)
                {
                    errors.Add(exception.Message);
                }
            }

            return taxRate;
        }

        /// <summary>
        /// Check whether cache already has the tax rate needed
        /// </summary>
        /// <param name="taxRate"></param>
        /// <returns></returns>
        private bool CheckCache(string from, string to, decimal price, out decimal taxRate)
        {
            taxRate = decimal.Zero;
            var found = false;
            if (CacheManager.IsSet(GetKey(from, to, price)))
            {
                found = true;
                taxRate = CacheManager.Get<decimal>(GetKey(from, to, price));
            }

            return found;
        }

        private string GetKey(string from, string to, decimal price)
        {
            return string.Format(TAXRATE_KEY, from, to, price);
        }

        private SpeedTaxAddress toSpeedTaxAddress(Address address)
        {
            return new SpeedTaxAddress()
            {
                //StreetAddress = string.Format("{0} {1}", address.Address1, address.Address2),
                StreetAddress = address.Address1,
                City = address.City,
                State = address.StateProvince.Abbreviation,
                ZIPCode = address.ZipPostalCode
            };
        }

    }
}
