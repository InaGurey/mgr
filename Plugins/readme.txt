40+ nopCommerce plugins are in the repository but I've excluded them from the solution to improve build performance. Many are potentially useful - typical e-commerce stuff
like payment methods, tax, shipping, authentication methods, and Google analytics.

To use one, just add it back to the solution (in this folder). I think the binaries will need to get copied to Web\Plugins as well - all 40+ of them got copied individually
in the MSBuild xml. Yuck - I'd rather do this in a depolyment script. (Once we figure it out for one, update this info.)
