using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.PayPalExpress
{
    public class PayPalExpressPaymentSettings : ISettings
    {
        public bool UseSandbox { get; set; }
        public bool UseStoreCredentials { get; set; }
        public TransactMode TransactMode { get; set; }
        public string TransactionKey { get; set; }
        public string LoginId { get; set; }
        public string PayPalMerchantId { get; set; }
        public decimal AdditionalFee { get; set; }
    }
}
