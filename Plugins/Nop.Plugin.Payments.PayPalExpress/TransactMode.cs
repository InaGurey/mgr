namespace Nop.Plugin.Payments.PayPalExpress
{
    /// <summary>
    /// Represents Authorize.Net payment processor transaction mode
    /// </summary>
    public enum TransactMode : int
    {
        /// <summary>
        /// Authorize and capture
        /// </summary>
        AuthorizeAndCapture = 0,
        /// <summary>
        /// Disabled: Authorize
        /// </summary>
        Authorize = 1
    }
}
