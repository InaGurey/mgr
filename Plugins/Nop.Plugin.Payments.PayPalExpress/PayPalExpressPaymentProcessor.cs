using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.PayPalExpress.Controllers;
using Nop.Plugin.Payments.PayPalExpress.Models;
using Nop.Plugin.Payments.PayPalExpress.net.authorize.api;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Treefort.Portal;

namespace Nop.Plugin.Payments.PayPalExpress
{
    /// <summary>
    /// PayPalExpress payment processor
    /// </summary>
    public class PayPalExpressPaymentProcessor : BasePlugin, IPaymentMethod
    {
        private const string TOKEN_PATTERN_KEY = "Nop.Payments.PayPalExpress-token-{0}-{1}-{2}";
        private const string TRANSACTIONID_PATTERN_KEY = "Nop.Payments.PayPalExpress-transactionid-{0}-{1}-{2}";
        public static string PAYERID_PATTERN_KEY = "Nop.Payments.PayPalExpress-payerid-{0}-{1}-{2}";


        #region Fields

        private readonly PayPalExpressPaymentSettings _authorizeNetPaymentSettings;
        private readonly ISettingService _settingService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly CurrencySettings _currencySettings;
        private readonly IWebHelper _webHelper;
        private readonly StoreInformationSettings _storeInformationSettings;
	    private readonly IPortalQueries _portalQueries;
        private readonly IMemoryCacheManager _cacheManager;
        private readonly IOrderService _orderService;

	    #endregion

        #region Ctor

        public PayPalExpressPaymentProcessor(PayPalExpressPaymentSettings authorizeNetPaymentSettings,
            ISettingService settingService, ICurrencyService currencyService,
            ICustomerService customerService,
            CurrencySettings currencySettings, IWebHelper webHelper,
            StoreInformationSettings storeInformationSettings, IPortalQueries portalQueries,
            IMemoryCacheManager cacheManager,
            IOrderService orderService,
            ILogger logger)
        {
            this._authorizeNetPaymentSettings = authorizeNetPaymentSettings;
            this._settingService = settingService;
            this._currencyService = currencyService;
            this._customerService = customerService;
            this._currencySettings = currencySettings;
            this._webHelper = webHelper;
            this._storeInformationSettings = storeInformationSettings;
	        _portalQueries = portalQueries;
            _cacheManager = cacheManager;
            _orderService = orderService;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Gets Authorize.NET URL
        /// </summary>
        /// <returns></returns>
        private string GetAuthorizeNETUrl()
        {
            return _authorizeNetPaymentSettings.UseSandbox ? "https://test.authorize.net/gateway/transact.dll" :
                "https://secure2.authorize.net/gateway/transact.dll";
        }

        /// <summary>
        /// Gets Authorize.NET API version
        /// </summary>
        private string GetApiVersion()
        {
            return "3.1";
        }

		private class Credentials
		{
		    public Credentials()
		    {
		    }

            public bool UsePayPalExpress { get; set; }
			public string ApiLogin { get; set; }
			public string TransactionKey { get; set; }
            public string MerchantId { get; set; }
		}

	    private Credentials GetCredentials(int siteId) {
	        if (_authorizeNetPaymentSettings.UseSandbox && !_authorizeNetPaymentSettings.UseStoreCredentials)
	        {
	            return new Credentials
	            {
                    UsePayPalExpress = true,
	                ApiLogin = _authorizeNetPaymentSettings.LoginId,
	                TransactionKey = _authorizeNetPaymentSettings.TransactionKey,
                    MerchantId = _authorizeNetPaymentSettings.PayPalMerchantId
	            };
	        }

		    var opt = _portalQueries.GetMgrOptions(siteId);
		    if (opt == null)
			    return null;
		    return new Credentials {
                UsePayPalExpress = opt.UsePayPalExpress,
			    ApiLogin = opt.AuthorizeNetApiLogin,
			    TransactionKey = opt.AuthorizeNetTxKey,
                MerchantId = opt.PayPalMerchantId
		    };
	    }

	    public bool VerifyCredentials(int siteId) {
			var creds = GetCredentials(siteId);
		    return creds != null && !string.IsNullOrWhiteSpace(creds.ApiLogin) && !string.IsNullOrWhiteSpace(creds.TransactionKey);
	    }

		public bool VerifyCredentials(int siteId, out string whyNot) {
			whyNot = null;
			var creds = GetCredentials(siteId);
			if (creds == null)
				whyNot = "Cannot use PayPal Express Checkout. No MgrOptions found for site ID " + siteId;
            else if (!creds.UsePayPalExpress)
                whyNot = "PayPal Express Checkout is not enabled in Portal for site ID " + siteId;
			else if (string.IsNullOrWhiteSpace(creds.ApiLogin) || string.IsNullOrWhiteSpace(creds.TransactionKey) || string.IsNullOrWhiteSpace(creds.MerchantId))
				whyNot = "Authorize.NET API Login, Transaction Key, or PayPal Merchant ID not set in Portal for site ID " + siteId;
			return whyNot == null;
		}


        // Populate merchant authentication (ARB Support)
        private MerchantAuthenticationType PopulateMerchantAuthentication(int siteId) {
	        var creds = GetCredentials(siteId);
	        return new MerchantAuthenticationType {
		        name = creds.ApiLogin,
		        transactionKey = creds.TransactionKey
	        };
        }
        /// <summary>
        ///  Get errors (ARB Support)
        /// </summary>
        /// <param name="response"></param>
        private static string GetErrors(ANetApiResponseType response)
        {
            var sb = new StringBuilder();
            sb.AppendLine("The API request failed with the following errors:");
            for (int i = 0; i < response.messages.Length; i++)
            {
                sb.AppendLine("[" + response.messages[i].code + "] " + response.messages[i].text);
            }
            return sb.ToString();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            var orderTotal = processPaymentRequest.OrderTotal;

            // TODO: Use setting to determine method (auth only vs. auth and capture)
            var authorization = AuthorizeAndCaptureContinue(processPaymentRequest);

            result.AuthorizationTransactionId = authorization.TransactionId;
            result.AuthorizationTransactionCode = authorization.ResponseCode;
            result.AuthorizationTransactionResult = authorization.ResponseReasonText;
            result.CaptureTransactionId = authorization.TransactionId;
            result.CaptureTransactionResult = authorization.ResponseReasonText;
            //result.AuthorizationTransactionId = authorization.TransactionId;
            //result.AuthorizationTransactionCode = authorization.ResponseCode;
            //result.AuthorizationTransactionResult = authorization.ResponseReasonText;
            result.PayerId = authorization.PayerId;

            if (authorization.Success && authorization.Approved && authorization.Amount >= orderTotal)
            {
                result.NewPaymentStatus = PaymentStatus.Paid;
                //result.NewPaymentStatus = PaymentStatus.Authorized;
            }

            if (authorization.Amount < orderTotal)
            {
                result.NewPaymentStatus = PaymentStatus.Pending;
                authorization.AddError(
                    string.Format(
                        "Error: Insufficient fund. Authorized amount of {0} is less than the expected amount of {1}",
                        authorization.Amount, orderTotal));
            }

            result.Errors = authorization.Errors;

            return result;
        }

		/// <summary>
		/// Something about A.NET/TSYS can't handle "123 45th St" (space gets lost somewhere and they end up with "12345th St").
		/// Convert only these cases to "123 street". public static so we can test it.
		/// </summary>
		public static string FixAddressForAvs(string addr) {
			addr = addr.Trim();
			var match = Regex.Match(addr, @"(^\d+)\s+\d"); // match digit(s)-space(s)-digit, grab first set of digits  
			return (match.Success) ? match.Groups[1].Value + " street" : addr;
		}


        public void SavePayerId(string payerId, int siteId, int customerId, decimal orderTotal)
        {

            var payerIdKey = string.Format(PAYERID_PATTERN_KEY, siteId,
                customerId, orderTotal);
            _cacheManager.Set(payerIdKey, payerId, 60);
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var order = postProcessPaymentRequest.Order;

            // TODO: Use setting to determine method (auth only vs. auth and capture)
            // Check if already captured
            if (order.PaymentStatusId == (int)PaymentStatus.Paid)
            {
                order.OrderNotes.Add(new OrderNote()
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = false,
                    OrderId = order.Id,
                    Note = string.Format("Payment capture: {0}", order.CaptureTransactionResult)
                });
                return;
            }

            order.OrderNotes.Add(new OrderNote()
            {
                CreatedOnUtc = DateTime.UtcNow,
                DisplayToCustomer = false,
                OrderId = order.Id,
                Note = string.Format("Capturing payment from order ID {0}", order.Id)
            });

            var captureResult = Capture(new CapturePaymentRequest {Order = order});

            order.CaptureTransactionId = captureResult.CaptureTransactionId;
            order.CaptureTransactionResult = captureResult.CaptureTransactionResult;
            order.PaymentStatus = captureResult.NewPaymentStatus;

            order.OrderNotes.Add(new OrderNote()
            {
                CreatedOnUtc = DateTime.UtcNow,
                DisplayToCustomer = false,
                OrderId = order.Id,
                Note = string.Format("Payment capture: {0}", captureResult.CaptureTransactionResult)
            });

            _orderService.UpdateOrder(order);
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee()
        {
            return _authorizeNetPaymentSettings.AdditionalFee;
        }

        public PayPalAuthorizationModel AuthorizeAndCapture(ProcessPaymentRequest processPaymentRequest)
        {
            var result = GetAuthorizationDetailsFromCache(processPaymentRequest.SiteId, processPaymentRequest.CustomerId,
                processPaymentRequest.OrderTotal);
            if (result.Success) return result;

            var creds = GetCredentials(processPaymentRequest.SiteId);

            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            var webClient = new WebClient();
            var form = new NameValueCollection();

            // Basic fields
            form.Add("x_login", creds.ApiLogin);
            form.Add("x_tran_key", creds.TransactionKey);
            form.Add("x_delim_data", "TRUE");
            form.Add("x_delim_char", "|");
            form.Add("x_encap_char", "");
            form.Add("x_relay_response", "FALSE");
            form.Add("x_version", GetApiVersion());
            form.Add("x_test_request", "FALSE");

            form.Add("x_method", "paypal");
            form.Add("x_type", "AUTH_CAPTURE");
            var corporateStoreUrl = ConfigurationManager.AppSettings["CorporateStoreUrl"];
            var successUrl = string.Format("{0}{1}/{2}/{3}/{4}", corporateStoreUrl,
                "Plugins/PaymentPayPalExpress/PayPalAuthorizeSuccess", processPaymentRequest.SiteId,
                processPaymentRequest.CustomerId, processPaymentRequest.OrderTotal);
            var cancelUrl = string.Format("{0}{1}/{2}/{3}/{4}", corporateStoreUrl,
                "Plugins/PaymentPayPalExpress/PayPalAuthorizeCancel", processPaymentRequest.SiteId,
                processPaymentRequest.CustomerId, processPaymentRequest.OrderTotal);
            form.Add("x_success_url", successUrl);
            form.Add("x_cancel_url", cancelUrl);
            form.Add("x_paypal_lc", "US");
            form.Add("x_paypal_hdrimg", "http://www.musicgoround.com/Themes/MGR/Content/images/logo.png");
            form.Add("x_paypal_payflowcolor", "191919");

            var orderTotal = Math.Round(processPaymentRequest.OrderTotal, 2);
            form.Add("x_currency_code", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);
            form.Add("x_amount", orderTotal.ToString("0.00", CultureInfo.InvariantCulture));

            form.Add("x_first_name", customer.BillingAddress.FirstName);
            form.Add("x_last_name", customer.BillingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.BillingAddress.Company))
                form.Add("x_company", customer.BillingAddress.Company);
            form.Add("x_address", FixAddressForAvs(customer.BillingAddress.Address1));
            form.Add("x_city", customer.BillingAddress.City);
            if (customer.BillingAddress.StateProvince != null)
                form.Add("x_state", customer.BillingAddress.StateProvince.Abbreviation);
            form.Add("x_zip", customer.BillingAddress.ZipPostalCode);
            if (customer.BillingAddress.Country != null)
                form.Add("x_country", customer.BillingAddress.Country.TwoLetterIsoCode);

            form.Add("x_email", customer.BillingAddress.Email); // FB 4416 - prevent a.net from emailing customers
            if (!string.IsNullOrEmpty(customer.BillingAddress.PhoneNumber))
            {
                form.Add("x_phone", customer.BillingAddress.PhoneNumber);
                form.Add("x_ship_to_phone", customer.BillingAddress.PhoneNumber);
            }
            form.Add("x_ship_to_first_name", customer.ShippingAddress.FirstName);
            form.Add("x_ship_to_last_name", customer.ShippingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.ShippingAddress.Company))
                form.Add("x_ship_to_company", customer.ShippingAddress.Company);
            form.Add("x_ship_to_address", customer.ShippingAddress.Address1);
            form.Add("x_ship_to_city", customer.ShippingAddress.City);
            if (customer.ShippingAddress.StateProvince != null)
                form.Add("x_ship_to_state", customer.ShippingAddress.StateProvince.Abbreviation);
            form.Add("x_ship_to_zip", customer.ShippingAddress.ZipPostalCode);
            if (customer.ShippingAddress.Country != null)
                form.Add("x_ship_to_country", customer.ShippingAddress.Country.TwoLetterIsoCode);

            //20 chars maximum
            form.Add("x_invoice_num", processPaymentRequest.OrderGuid.ToString().Substring(0, 20));
            form.Add("x_customer_ip", _webHelper.GetCurrentIpAddress());


            var reply = Encoding.ASCII.GetString(webClient.UploadValues(GetAuthorizeNETUrl(), form));

            if (!String.IsNullOrEmpty(reply))
            {
                // http://www.authorize.net/support/merchant/Transaction_Response/Transaction_Response.htm
                string[] responseFields = reply.Split('|');

                result.ResponseCode = responseFields[0];
                result.ResponseReasonCode = responseFields[2];
                result.ResponseReasonText = string.Format("{0}: {1}: {2}", responseFields[57], responseFields[2], responseFields[3]); ;
                result.TransactionId = responseFields[6];
                result.RawResponseCode = responseFields[57];
                result.MerchantId = creds.MerchantId;
                result.SiteId = processPaymentRequest.SiteId;
                result.CustomerId = processPaymentRequest.CustomerId;

                if (result.TransactionId == "0")
                {
                    result.AddError("Invalid Transaction ID");
                }

                switch (responseFields[0])
                {
                    case "1":
                    case "5":
                        result.Success = true;
                        result.Amount = decimal.Parse(responseFields[9]);
                        var redirectUrl = new Uri(System.Web.HttpUtility.HtmlDecode(responseFields[55]));
                        var token = System.Web.HttpUtility.ParseQueryString(redirectUrl.Query)["token"];
                        result.SecureUrl = redirectUrl.ToString();
                        result.Token = token;
                        result.TransactionId = responseFields[6];

                        var tokenKey = string.Format(TOKEN_PATTERN_KEY, processPaymentRequest.SiteId,
                            processPaymentRequest.CustomerId, orderTotal);
                        var transactionIdKey = string.Format(TRANSACTIONID_PATTERN_KEY, processPaymentRequest.SiteId,
                            processPaymentRequest.CustomerId, orderTotal);

                        _cacheManager.Set(tokenKey, token, 60);
                        _cacheManager.Set(transactionIdKey, responseFields[6], 60);
                        break;
                    case "2":
                        result.AddError(string.Format("{0} Your payment has been declined.", responseFields[3]));
                        break;
                    case "3":
                        result.AddError(string.Format("{0}: {1} An error has occured.", responseFields[2], responseFields[3]));
                        break;

                    default:
                        break;
                }

                result.Success = result.Success && result.Errors.Count == 0;
            }
            else
            {
                result.AddError("PayPal Express w/Authorize.NET unknown error");
            }

            return result;
        }
        public PayPalAuthorizationModel AuthorizeAndCaptureContinue(ProcessPaymentRequest processPaymentRequest)
        {
            var model = new PayPalAuthorizationModel
            {
                PayerId = processPaymentRequest.PaypalPayerId,
                TransactionId = processPaymentRequest.PayPalTransactionId,
                MerchantId = processPaymentRequest.PayPalMerchantId
            };

            var creds = GetCredentials(processPaymentRequest.SiteId);
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

            var webClient = new WebClient();
            var form = new NameValueCollection();

            // Basic fields
            form.Add("x_login", creds.ApiLogin);
            form.Add("x_tran_key", creds.TransactionKey);
            form.Add("x_delim_data", "TRUE");
            form.Add("x_delim_char", "|");
            form.Add("x_encap_char", "");
            form.Add("x_relay_response", "FALSE");
            form.Add("x_version", GetApiVersion());
            form.Add("x_test_request", "FALSE");

            form.Add("x_first_name", customer.BillingAddress.FirstName);
            form.Add("x_name", customer.BillingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.BillingAddress.Company))
                form.Add("x_company", customer.BillingAddress.Company);
            form.Add("x_address", FixAddressForAvs(customer.BillingAddress.Address1));
            form.Add("x_city", customer.BillingAddress.City);
            if (customer.BillingAddress.StateProvince != null)
                form.Add("x_state", customer.BillingAddress.StateProvince.Abbreviation);
            form.Add("x_zip", customer.BillingAddress.ZipPostalCode);
            if (customer.BillingAddress.Country != null)
                form.Add("x_country", customer.BillingAddress.Country.TwoLetterIsoCode);

            form.Add("x_email", customer.BillingAddress.Email); // FB 4416 - prevent a.net from emailing customers
            if (!string.IsNullOrEmpty(customer.BillingAddress.PhoneNumber))
            {
                form.Add("x_phone", customer.BillingAddress.PhoneNumber);
                form.Add("x_ship_to_phone", customer.BillingAddress.PhoneNumber);
            }
            form.Add("x_ship_to_first_name", customer.ShippingAddress.FirstName);
            form.Add("x_ship_to_last_name", customer.ShippingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.ShippingAddress.Company))
                form.Add("x_ship_to_company", customer.ShippingAddress.Company);
            form.Add("x_ship_to_address", customer.ShippingAddress.Address1);
            form.Add("x_ship_to_city", customer.ShippingAddress.City);
            if (customer.ShippingAddress.StateProvince != null)
                form.Add("x_ship_to_state", customer.ShippingAddress.StateProvince.Abbreviation);
            form.Add("x_ship_to_zip", customer.ShippingAddress.ZipPostalCode);
            if (customer.ShippingAddress.Country != null)
                form.Add("x_ship_to_country", customer.ShippingAddress.Country.TwoLetterIsoCode);

            form.Add("x_method", "paypal");
            form.Add("x_type", "AUTH_CAPTURE_CONTINUE");
            form.Add("x_ref_trans_id", processPaymentRequest.PayPalTransactionId);
            form.Add("x_payer_id", processPaymentRequest.PaypalPayerId);

            var reply = Encoding.ASCII.GetString(webClient.UploadValues(GetAuthorizeNETUrl(), form));

            if (!string.IsNullOrEmpty(reply))
            {
                // http://www.authorize.net/content/dam/authorize/documents/PayPal_NVP_guide.pdf
                string[] responseFields = reply.Split('|');

                model.ResponseCode = responseFields[0];
                model.ResponseReasonCode = responseFields[2];
                model.ResponseReasonText = string.Format("{0}: {1}: {2}", responseFields[57], responseFields[2], responseFields[3]);
                model.TransactionId = responseFields[6];
                model.RawResponseCode = responseFields[57];
                model.MerchantId = creds.MerchantId;

                switch (responseFields[0])
                {
                    case "1":
                        model.Success = true;
                        model.Approved = true;
                        var orderTotal = decimal.Parse(responseFields[9]);
                        model.Amount = orderTotal;

                        break;
                    case "2":
                        model.AddError(string.Format("{0} Your payment has been declined.", responseFields[3]));
                        break;
                    case "3":
                        model.AddError(string.Format("{0}: {1} An error has occured.", responseFields[2], responseFields[3]));
                        break;

                    default:
                        break;
                }
            }
            else
            {
                model.AddError("PayPal Express w/Authorize.NET unknown error");
            }

            return model;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public PayPalAuthorizationModel Authorize(ProcessPaymentRequest processPaymentRequest)
        {
            var result = GetAuthorizationDetailsFromCache(processPaymentRequest.SiteId, processPaymentRequest.CustomerId,
                processPaymentRequest.OrderTotal);
            if (result.Success) return result;
            
            var creds = GetCredentials(processPaymentRequest.SiteId);

            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            var webClient = new WebClient();
            var form = new NameValueCollection();

            // Basic fields
            form.Add("x_login", creds.ApiLogin);
            form.Add("x_tran_key", creds.TransactionKey);
            form.Add("x_delim_data", "TRUE");
            form.Add("x_delim_char", "|");
            form.Add("x_encap_char", "");
            form.Add("x_relay_response", "FALSE");
            form.Add("x_version", GetApiVersion());
            form.Add("x_test_request", "FALSE");

            form.Add("x_method", "paypal");
            form.Add("x_type", "AUTH_ONLY");
            var corporateStoreUrl = ConfigurationManager.AppSettings["CorporateStoreUrl"];
            var successUrl = string.Format("{0}{1}/{2}/{3}/{4}", corporateStoreUrl,
                "Plugins/PaymentPayPalExpress/PayPalAuthorizeSuccess", processPaymentRequest.SiteId,
                processPaymentRequest.CustomerId, processPaymentRequest.OrderTotal);
            var cancelUrl = string.Format("{0}{1}/{2}/{3}/{4}", corporateStoreUrl,
                "Plugins/PaymentPayPalExpress/PayPalAuthorizeCancel", processPaymentRequest.SiteId,
                processPaymentRequest.CustomerId, processPaymentRequest.OrderTotal);
            form.Add("x_success_url", successUrl);
            form.Add("x_cancel_url", cancelUrl);
            form.Add("x_paypal_lc", "US");
            form.Add("x_paypal_hdrimg", "http://www.musicgoround.com/Themes/MGR/Content/images/logo.png");
            form.Add("x_paypal_payflowcolor", "191919");

            var orderTotal = Math.Round(processPaymentRequest.OrderTotal, 2);
            form.Add("x_currency_code", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);
            form.Add("x_amount", orderTotal.ToString("0.00", CultureInfo.InvariantCulture));

            form.Add("x_first_name", customer.BillingAddress.FirstName);
            form.Add("x_name", customer.BillingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.BillingAddress.Company))
                form.Add("x_company", customer.BillingAddress.Company);
            form.Add("x_address", FixAddressForAvs(customer.BillingAddress.Address1));
            form.Add("x_city", customer.BillingAddress.City);
            if (customer.BillingAddress.StateProvince != null)
                form.Add("x_state", customer.BillingAddress.StateProvince.Abbreviation);
            form.Add("x_zip", customer.BillingAddress.ZipPostalCode);
            if (customer.BillingAddress.Country != null)
                form.Add("x_country", customer.BillingAddress.Country.TwoLetterIsoCode);

            form.Add("x_email", customer.BillingAddress.Email); // FB 4416 - prevent a.net from emailing customers
            if (!string.IsNullOrEmpty(customer.BillingAddress.PhoneNumber))
            {
                form.Add("x_phone", customer.BillingAddress.PhoneNumber);
                form.Add("x_ship_to_phone", customer.BillingAddress.PhoneNumber);
            }
            form.Add("x_ship_to_first_name", customer.ShippingAddress.FirstName);
            form.Add("x_ship_to_last_name", customer.ShippingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.ShippingAddress.Company))
                form.Add("x_ship_to_company", customer.ShippingAddress.Company);
            form.Add("x_ship_to_address", customer.ShippingAddress.Address1);
            form.Add("x_ship_to_city", customer.ShippingAddress.City);
            if (customer.ShippingAddress.StateProvince != null)
                form.Add("x_ship_to_state", customer.ShippingAddress.StateProvince.Abbreviation);
            form.Add("x_ship_to_zip", customer.ShippingAddress.ZipPostalCode);
            if (customer.ShippingAddress.Country != null)
                form.Add("x_ship_to_country", customer.ShippingAddress.Country.TwoLetterIsoCode);

            //20 chars maximum
            form.Add("x_invoice_num", processPaymentRequest.OrderGuid.ToString().Substring(0, 20));
            form.Add("x_customer_ip", _webHelper.GetCurrentIpAddress());


            var reply = Encoding.ASCII.GetString(webClient.UploadValues(GetAuthorizeNETUrl(), form));

            if (!String.IsNullOrEmpty(reply))
            {
                // http://www.authorize.net/support/merchant/Transaction_Response/Transaction_Response.htm
                string[] responseFields = reply.Split('|');

                result.ResponseCode = responseFields[0];
                result.ResponseReasonCode = responseFields[2];
                result.ResponseReasonText = string.Format("{0}: {1}: {2}", responseFields[57], responseFields[2], responseFields[3]); ;
                result.TransactionId = responseFields[6];
                result.RawResponseCode = responseFields[57];
                result.MerchantId = creds.MerchantId;
                result.SiteId = processPaymentRequest.SiteId;
                result.CustomerId = processPaymentRequest.CustomerId;

                if (result.TransactionId == "0")
                {
                    result.AddError("Invalid Transaction ID");
                }

                switch (responseFields[0])
                {
                    case "1":
                    case "5":
                        result.Success = true;
                        result.Amount = decimal.Parse(responseFields[9]);
                        var redirectUrl = new Uri(System.Web.HttpUtility.HtmlDecode(responseFields[55]));
                        var token = System.Web.HttpUtility.ParseQueryString(redirectUrl.Query)["token"];
                        result.SecureUrl = redirectUrl.ToString();
                        result.Token = token;

                        var tokenKey = string.Format(TOKEN_PATTERN_KEY, processPaymentRequest.SiteId,
                            processPaymentRequest.CustomerId, orderTotal);
                        var transactionIdKey = string.Format(TRANSACTIONID_PATTERN_KEY, processPaymentRequest.SiteId,
                            processPaymentRequest.CustomerId, orderTotal);

                        _cacheManager.Set(tokenKey, token, 60);
                        _cacheManager.Set(transactionIdKey, responseFields[6], 60);
                        break;
                    case "2":
                        result.AddError(string.Format("{0} Your payment has been declined.", responseFields[3]));
                        break;
                    case "3":
                        result.AddError(string.Format("{0}: {1} An error has occured.", responseFields[2], responseFields[3]));
                        break;

                    default:
                        break;
                }

                result.Success = result.Success && result.Errors.Count == 0;
            }
            else
            {
                result.AddError("PayPal Express w/Authorize.NET unknown error");
            }

            return result;
        }

        public PayPalAuthorizationModel AuthorizeContinue(ProcessPaymentRequest processPaymentRequest)
        {
            var model = new PayPalAuthorizationModel
            {
                PayerId = processPaymentRequest.PaypalPayerId,
                TransactionId = processPaymentRequest.PayPalTransactionId,
                MerchantId = processPaymentRequest.PayPalMerchantId
            };

            var creds = GetCredentials(processPaymentRequest.SiteId);
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

            var webClient = new WebClient();
            var form = new NameValueCollection();

            // Basic fields
            form.Add("x_login", creds.ApiLogin);
            form.Add("x_tran_key", creds.TransactionKey);
            form.Add("x_delim_data", "TRUE");
            form.Add("x_delim_char", "|");
            form.Add("x_encap_char", "");
            form.Add("x_relay_response", "FALSE");
            form.Add("x_version", GetApiVersion());
            form.Add("x_test_request", "FALSE");

            form.Add("x_first_name", customer.BillingAddress.FirstName);
            form.Add("x_name", customer.BillingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.BillingAddress.Company))
                form.Add("x_company", customer.BillingAddress.Company);
            form.Add("x_address", FixAddressForAvs(customer.BillingAddress.Address1));
            form.Add("x_city", customer.BillingAddress.City);
            if (customer.BillingAddress.StateProvince != null)
                form.Add("x_state", customer.BillingAddress.StateProvince.Abbreviation);
            form.Add("x_zip", customer.BillingAddress.ZipPostalCode);
            if (customer.BillingAddress.Country != null)
                form.Add("x_country", customer.BillingAddress.Country.TwoLetterIsoCode);

            form.Add("x_email", customer.BillingAddress.Email); // FB 4416 - prevent a.net from emailing customers
            if (!string.IsNullOrEmpty(customer.BillingAddress.PhoneNumber))
            {
                form.Add("x_phone", customer.BillingAddress.PhoneNumber);
                form.Add("x_ship_to_phone", customer.BillingAddress.PhoneNumber);
            }
            form.Add("x_ship_to_first_name", customer.ShippingAddress.FirstName);
            form.Add("x_ship_to_last_name", customer.ShippingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.ShippingAddress.Company))
                form.Add("x_ship_to_company", customer.ShippingAddress.Company);
            form.Add("x_ship_to_address", customer.ShippingAddress.Address1);
            form.Add("x_ship_to_city", customer.ShippingAddress.City);
            if (customer.ShippingAddress.StateProvince != null)
                form.Add("x_ship_to_state", customer.ShippingAddress.StateProvince.Abbreviation);
            form.Add("x_ship_to_zip", customer.ShippingAddress.ZipPostalCode);
            if (customer.ShippingAddress.Country != null)
                form.Add("x_ship_to_country", customer.ShippingAddress.Country.TwoLetterIsoCode);

            form.Add("x_method", "paypal");
            form.Add("x_type", "AUTH_ONLY_CONTINUE");
            form.Add("x_ref_trans_id", processPaymentRequest.PayPalTransactionId);
            form.Add("x_payer_id", processPaymentRequest.PaypalPayerId);

            var reply = Encoding.ASCII.GetString(webClient.UploadValues(GetAuthorizeNETUrl(), form));

            if (!string.IsNullOrEmpty(reply))
            {
                // http://www.authorize.net/content/dam/authorize/documents/PayPal_NVP_guide.pdf
                string[] responseFields = reply.Split('|');

                model.ResponseCode = responseFields[0];
                model.ResponseReasonCode = responseFields[2];
                model.ResponseReasonText = string.Format("{0}: {1}: {2}", responseFields[57], responseFields[2], responseFields[3]);
                model.TransactionId = responseFields[6];
                model.RawResponseCode = responseFields[57];
                model.MerchantId = creds.MerchantId;

                switch (responseFields[0])
                {
                    case "1":
                        model.Success = true;
                        model.Approved = true;
                        var orderTotal = decimal.Parse(responseFields[9]);
                        model.Amount = orderTotal;

                        break;
                    case "2":
                        model.AddError(string.Format("{0} Your payment has been declined.", responseFields[3]));
                        break;
                    case "3":
                        model.AddError(string.Format("{0}: {1} An error has occured.", responseFields[2], responseFields[3]));
                        break;

                    default:
                        break;
                }
            }
            else
            {
                model.AddError("PayPal Express w/Authorize.NET unknown error");
            }

            return model;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();

            var creds = GetCredentials(capturePaymentRequest.Order.SiteId);

            var webClient = new WebClient();
            var form = new NameValueCollection();
            var customer = capturePaymentRequest.Order.Customer;

            // Basic fields
            form.Add("x_login", creds.ApiLogin);
            form.Add("x_tran_key", creds.TransactionKey);
            form.Add("x_delim_data", "TRUE");
            form.Add("x_delim_char", "|");
            form.Add("x_encap_char", "");
            form.Add("x_relay_response", "FALSE");
            form.Add("x_version", GetApiVersion());
            form.Add("x_test_request", "FALSE");

            form.Add("x_first_name", customer.BillingAddress.FirstName);
            form.Add("x_name", customer.BillingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.BillingAddress.Company))
                form.Add("x_company", customer.BillingAddress.Company);
            form.Add("x_address", FixAddressForAvs(customer.BillingAddress.Address1));
            form.Add("x_city", customer.BillingAddress.City);
            if (customer.BillingAddress.StateProvince != null)
                form.Add("x_state", customer.BillingAddress.StateProvince.Abbreviation);
            form.Add("x_zip", customer.BillingAddress.ZipPostalCode);
            if (customer.BillingAddress.Country != null)
                form.Add("x_country", customer.BillingAddress.Country.TwoLetterIsoCode);

            form.Add("x_email", customer.BillingAddress.Email); // FB 4416 - prevent a.net from emailing customers
            if (!string.IsNullOrEmpty(customer.BillingAddress.PhoneNumber))
            {
                form.Add("x_phone", customer.BillingAddress.PhoneNumber);
                form.Add("x_ship_to_phone", customer.BillingAddress.PhoneNumber);
            }
            form.Add("x_ship_to_first_name", customer.ShippingAddress.FirstName);
            form.Add("x_ship_to_last_name", customer.ShippingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.ShippingAddress.Company))
                form.Add("x_ship_to_company", customer.ShippingAddress.Company);
            form.Add("x_ship_to_address", customer.ShippingAddress.Address1);
            form.Add("x_ship_to_city", customer.ShippingAddress.City);
            if (customer.ShippingAddress.StateProvince != null)
                form.Add("x_ship_to_state", customer.ShippingAddress.StateProvince.Abbreviation);
            form.Add("x_ship_to_zip", customer.ShippingAddress.ZipPostalCode);
            if (customer.ShippingAddress.Country != null)
                form.Add("x_ship_to_country", customer.ShippingAddress.Country.TwoLetterIsoCode);

            form.Add("x_method", "paypal");
            form.Add("x_type", "PRIOR_AUTH_CAPTURE");
            form.Add("x_ref_trans_id", capturePaymentRequest.Order.AuthorizationTransactionId);

            var reply = Encoding.ASCII.GetString(webClient.UploadValues(GetAuthorizeNETUrl(), form));

            if (!string.IsNullOrEmpty(reply))
            {
                // http://www.authorize.net/content/dam/authorize/documents/PayPal_NVP_guide.pdf
                string[] responseFields = reply.Split('|');
                result.CaptureTransactionId = responseFields[6];
                result.CaptureTransactionResult = string.Format("({0}: {1})", responseFields[2], responseFields[3]);

                switch (responseFields[0])
                {
                    case "1":

                        if (decimal.Parse(responseFields[9]) < capturePaymentRequest.Order.OrderTotal)
                        {
                            result.AddError(
                                string.Format(
                                    "Error: the authorized payment of {0} is less than the expected amount of {1}",
                                    responseFields[9], capturePaymentRequest.Order.OrderTotal));
                            result.NewPaymentStatus = PaymentStatus.Pending;
                        }
                        else
                        {
                            result.NewPaymentStatus = PaymentStatus.Paid;
                            // clear all cache
                            ClearCache(capturePaymentRequest.Order.SiteId, capturePaymentRequest.Order.CustomerId,
                                capturePaymentRequest.Order.OrderTotal);
                        }

                        break;
                    case "2":
                        result.AddError(string.Format("{0} : {1}", responseFields[2], responseFields[3]));
                        break;
                    case "3":
                        result.AddError(string.Format("{0}: {1} Please click on 'Payment Information' above to correct the problem.", responseFields[2], responseFields[3]));
                        break;
                }
            }
            else
            {
                result.CaptureTransactionResult = "Authorize.NET/PayPal Express Checkout unknown error";
                result.AddError("Authorize.NET/PayPal Express Checkout unknown error");
            }

            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

			var authentication = PopulateMerchantAuthentication(processPaymentRequest.SiteId);
            if (!processPaymentRequest.IsRecurringPayment)
            {
				var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

                var subscription = new ARBSubscriptionType();
                var creditCard = new net.authorize.api.CreditCardType();

                subscription.name = processPaymentRequest.OrderGuid.ToString();

                creditCard.cardNumber = processPaymentRequest.CreditCardNumber;
                creditCard.expirationDate = processPaymentRequest.CreditCardExpireYear + "-" + processPaymentRequest.CreditCardExpireMonth; // required format for API is YYYY-MM
                creditCard.cardCode = processPaymentRequest.CreditCardCvv2;

                subscription.payment = new PaymentType();
                subscription.payment.Item = creditCard;

                subscription.billTo = new NameAndAddressType();
                subscription.billTo.firstName = customer.BillingAddress.FirstName;
                subscription.billTo.lastName = customer.BillingAddress.LastName;
                subscription.billTo.address = customer.BillingAddress.Address1 + " " + customer.BillingAddress.Address2;
                subscription.billTo.city = customer.BillingAddress.City;
                if (customer.BillingAddress.StateProvince != null)
                {
                    subscription.billTo.state = customer.BillingAddress.StateProvince.Abbreviation;
                }
                subscription.billTo.zip = customer.BillingAddress.ZipPostalCode;

                if (customer.ShippingAddress != null)
                {
                    subscription.shipTo = new NameAndAddressType();
                    subscription.shipTo.firstName = customer.ShippingAddress.FirstName;
                    subscription.shipTo.lastName = customer.ShippingAddress.LastName;
                    subscription.shipTo.address = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;
                    subscription.shipTo.city = customer.ShippingAddress.City;
                    if (customer.ShippingAddress.StateProvince != null)
                    {
                        subscription.shipTo.state = customer.ShippingAddress.StateProvince.Abbreviation;
                    }
                    subscription.shipTo.zip = customer.ShippingAddress.ZipPostalCode;

                }

                subscription.customer = new CustomerType();
                subscription.customer.email = customer.BillingAddress.Email;
                subscription.customer.phoneNumber = customer.BillingAddress.PhoneNumber;

                subscription.order = new OrderType();
                subscription.order.description = string.Format("{0} {1}", _storeInformationSettings.StoreName, "Recurring payment");

                // Create a subscription that is leng of specified occurrences and interval is amount of days ad runs

                subscription.paymentSchedule = new PaymentScheduleType();
                DateTime dtNow = DateTime.UtcNow;
                subscription.paymentSchedule.startDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day);
                subscription.paymentSchedule.startDateSpecified = true;

                subscription.paymentSchedule.totalOccurrences = Convert.ToInt16(processPaymentRequest.RecurringTotalCycles);
                subscription.paymentSchedule.totalOccurrencesSpecified = true;

                var orderTotal = Math.Round(processPaymentRequest.OrderTotal, 2);
                subscription.amount = orderTotal;
                subscription.amountSpecified = true;

                // Interval can't be updated once a subscription is created.
                subscription.paymentSchedule.interval = new PaymentScheduleTypeInterval();
                switch (processPaymentRequest.RecurringCyclePeriod)
                {
                    case RecurringProductCyclePeriod.Days:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.days;
                        break;
                    case RecurringProductCyclePeriod.Weeks:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength * 7);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.days;
                        break;
                    case RecurringProductCyclePeriod.Months:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                        break;
                    case RecurringProductCyclePeriod.Years:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength * 12);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                        break;
                    default:
                        throw new NopException("Not supported cycle period");
                }

                using (var webService = new net.authorize.api.Service())
                {
                    if (_authorizeNetPaymentSettings.UseSandbox)
                        webService.Url = "https://apitest.authorize.net/soap/v1/Service.asmx";
                    else
                        webService.Url = "https://api.authorize.net/soap/v1/Service.asmx";

                    var response = webService.ARBCreateSubscription(authentication, subscription);

                    if (response.resultCode == MessageTypeEnum.Ok)
                    {
                        result.SubscriptionTransactionId = response.subscriptionId.ToString();
                        result.AuthorizationTransactionCode = response.resultCode.ToString();
                        result.AuthorizationTransactionResult = string.Format("Approved ({0}: {1})", response.resultCode.ToString(), response.subscriptionId.ToString());

                        result.NewPaymentStatus = PaymentStatus.Pending;
                    }
                    else
                    {
                        result.AddError(string.Format("Error processing recurring payment. {0}", GetErrors(response)));
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            var authentication = PopulateMerchantAuthentication(cancelPaymentRequest.Order.SiteId);
            long subscriptionId = 0;
            long.TryParse(cancelPaymentRequest.Order.SubscriptionTransactionId, out subscriptionId);


            using (var webService = new net.authorize.api.Service())
            {
                if (_authorizeNetPaymentSettings.UseSandbox)
                    webService.Url = "https://apitest.authorize.net/soap/v1/Service.asmx";
                else
                    webService.Url = "https://api.authorize.net/soap/v1/Service.asmx";

                var response = webService.ARBCancelSubscription(authentication, subscriptionId);

                if (response.resultCode == MessageTypeEnum.Ok)
                {
                    //ok
                }
                else
                {
                    result.AddError("Error cancelling subscription, please contact customer support. " + GetErrors(response));
                }
            }
            return result;
        }

        public GetDetailsResponseModel GetPaymentDetails(string transactionId, string payerId, int siteId, int customerId)
        {
            var creds = GetCredentials(siteId);
            var webClient = new WebClient();
            var form = new NameValueCollection();
            var customer = _customerService.GetCustomerById(customerId);

            // Basic fields
            form.Add("x_login", creds.ApiLogin);
            form.Add("x_tran_key", creds.TransactionKey);
            form.Add("x_delim_data", "TRUE");
            form.Add("x_delim_char", "|");
            form.Add("x_encap_char", "");
            form.Add("x_relay_response", "FALSE");
            form.Add("x_version", GetApiVersion());
            form.Add("x_test_request", "FALSE");

            form.Add("x_first_name", customer.BillingAddress.FirstName);
            form.Add("x_name", customer.BillingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.BillingAddress.Company))
                form.Add("x_company", customer.BillingAddress.Company);
            form.Add("x_address", FixAddressForAvs(customer.BillingAddress.Address1));
            form.Add("x_city", customer.BillingAddress.City);
            if (customer.BillingAddress.StateProvince != null)
                form.Add("x_state", customer.BillingAddress.StateProvince.Abbreviation);
            form.Add("x_zip", customer.BillingAddress.ZipPostalCode);
            if (customer.BillingAddress.Country != null)
                form.Add("x_country", customer.BillingAddress.Country.TwoLetterIsoCode);

            form.Add("x_email", customer.BillingAddress.Email); // FB 4416 - prevent a.net from emailing customers
            if (!string.IsNullOrEmpty(customer.BillingAddress.PhoneNumber))
            {
                form.Add("x_phone", customer.BillingAddress.PhoneNumber);
                form.Add("x_ship_to_phone", customer.BillingAddress.PhoneNumber);
            }
            form.Add("x_ship_to_first_name", customer.ShippingAddress.FirstName);
            form.Add("x_ship_to_last_name", customer.ShippingAddress.LastName);
            if (!string.IsNullOrEmpty(customer.ShippingAddress.Company))
                form.Add("x_ship_to_company", customer.ShippingAddress.Company);
            form.Add("x_ship_to_address", customer.ShippingAddress.Address1);
            form.Add("x_ship_to_city", customer.ShippingAddress.City);
            if (customer.ShippingAddress.StateProvince != null)
                form.Add("x_ship_to_state", customer.ShippingAddress.StateProvince.Abbreviation);
            form.Add("x_ship_to_zip", customer.ShippingAddress.ZipPostalCode);
            if (customer.ShippingAddress.Country != null)
                form.Add("x_ship_to_country", customer.ShippingAddress.Country.TwoLetterIsoCode);

            form.Add("x_type", "GET_DETAILS");
            form.Add("x_ref_trans_id", transactionId);

            var reply = Encoding.ASCII.GetString(webClient.UploadValues(GetAuthorizeNETUrl(), form));
            var result = new GetDetailsResponseModel();

            if (!string.IsNullOrEmpty(reply))
            {
                string[] responseFields = reply.Split('|');

                result.ResponseCode = responseFields[0];
                result.ResponseReasonCode = responseFields[2];
                result.ResponseReasonText = responseFields[3];
                result.TransactionId = responseFields[6];
                result.RawResponseCode = responseFields[57];

                switch (responseFields[0])
                {
                    case "1":
                        result.Success = true;
                        decimal orderAmount;
                        decimal.TryParse(responseFields[9], out orderAmount);
                        result.Amount = orderAmount;
                        break;
                    case "2":
                        result.Errors.Add(string.Format("{0} Your payment has been declined.", responseFields[3]));
                        break;
                    case "3":
                        result.Errors.Add(string.Format("{0} An error has occured.", responseFields[3]));
                        break;

                    default:
                        break;
                }
            }
            else
            {
                result.Errors.Add("Unknown error has occured.");
            }

            return result;
        }

        public PayPalAuthorizationModel GetAuthorizationDetailsFromCache(int siteId, int customerId, decimal orderTotal)
        {

            var result = new PayPalAuthorizationModel();

            var creds = GetCredentials(siteId);

            var tokenKey = string.Format(TOKEN_PATTERN_KEY, siteId, customerId, orderTotal);
            var transactionIdKey = string.Format(TRANSACTIONID_PATTERN_KEY, siteId, customerId, orderTotal);

            if (!_cacheManager.IsSet(tokenKey) || !_cacheManager.IsSet(transactionIdKey)) return result;

            result.Token = _cacheManager.Get<string>(tokenKey);
            result.TransactionId = _cacheManager.Get<string>(transactionIdKey);

            if (result.TransactionId == "0") return result;

            result.MerchantId = creds.MerchantId;
            result.Amount = orderTotal;
            result.SiteId = siteId;
            result.CustomerId = customerId;
            result.Success = true;

            var payerIdKey = string.Format(PAYERID_PATTERN_KEY, siteId,
                customerId, orderTotal);
            if (_cacheManager.IsSet(payerIdKey))
            {
                result.PayerId = _cacheManager.Get<string>(payerIdKey);
            }


            return result;
        }

        private void ClearCache(int siteId, int customerId, decimal orderTotal)
        {

            var tokenKey = string.Format(TOKEN_PATTERN_KEY, siteId, customerId, orderTotal);
            var transactionIdKey = string.Format(TRANSACTIONID_PATTERN_KEY, siteId, customerId, orderTotal);
            var payerIdKey = string.Format(PAYERID_PATTERN_KEY, siteId, customerId, orderTotal);

            _cacheManager.Remove(tokenKey);
            _cacheManager.Remove(transactionIdKey);
            _cacheManager.Remove(payerIdKey);
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            
            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentPayPalExpress";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.PayPalExpress.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentPayPalExpress";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.PayPalExpress.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(PaymentPayPalExpressController);
        }

        public override void Install()
        {
            //settings
            var settings = new PayPalExpressPaymentSettings()
            {
                UseSandbox = true,
                UseStoreCredentials = true,
                TransactMode = TransactMode.AuthorizeAndCapture,
                TransactionKey = "3Zmt4Kj45784A6pD",
                LoginId = "7X2tdxT6MLj",
                PayPalMerchantId = "PYTJSG54LYL3E"
            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Notes", "If you're using this gateway, ensure that your primary store currency is supported by Authorize.NET.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseSandbox", "Use Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseStoreCredentials", "Use Store Credentials");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseStoreCredentials.Hint", "Check to use store credentials instead of global merchant credentials.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactModeValues", "Transaction mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactModeValues.Hint", "Choose transaction mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactionKey", "Transaction key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactionKey.Hint", "Specify transaction key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.LoginId", "Login ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.PayPalMerchantId", "PayPal Merchant ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.PayPalMerchantId.Hint", "Default PayPal Merchant ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.LoginId.Hint", "Specify login identifier.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.AdditionalFee", "Additional fee");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.AdditionalFee.Hint", "Enter additional fee to charge your customers.");
            
            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<PayPalExpressPaymentSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Notes");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseStoreCredentials");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.UseStoreCredentials.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactModeValues");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactModeValues.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactionKey");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.TransactionKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.LoginId");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.PayPalMerchantId");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.PayPalMerchantId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.LoginId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.PayPalExpress.Fields.AdditionalFee.Hint");
            
            base.Uninstall();
        }

        #endregion

        #region Properies

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.Manual;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Standard;
            }
        }

        #endregion
    }
}
