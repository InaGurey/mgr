﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.PayPalExpress.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        public bool SandboxMode { get; set; }
        public string Token { get; set; }
        public string TransactionId { get; set; }
        public string PayerId { get; set; }
        public string AuthorizationContinueSuccessUrl { get; set; }
        public string AuthorizationContinueCancelUrl { get; set; }
        public string MerchantId { get; set; }
        public bool Approved { get; set; }

        public string Error { get; set; }

    }
}