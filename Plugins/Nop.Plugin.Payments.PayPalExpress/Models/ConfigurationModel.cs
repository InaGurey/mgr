﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.PayPalExpress.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }
        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.UseStoreCredentials")]
        public bool UseStoreCredentials { get; set; }

        public int TransactModeId { get; set; }
        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.TransactModeValues")]
        public SelectList TransactModeValues { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.TransactionKey")]
        public string TransactionKey { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.LoginId")]
        public string LoginId { get; set; }
        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.PayPalMerchantId")]
        public string PayPalMerchantId { get; set; }

        [NopResourceDisplayName("Plugins.Payments.PayPalExpress.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
    }
}