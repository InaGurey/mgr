﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.PayPalExpress.Models
{
    public class GetDetailsResponseModel
    {
        public GetDetailsResponseModel()
        {
            Errors = new List<string>();
        }
        public bool Success { get; set; }
        public decimal Amount { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseReasonCode { get; set; }
        public string ResponseReasonText { get; set; }
        public string TransactionId { get; set; }
        public string RawResponseCode { get; set; }
        public IList<string> Errors { get; set; }
    }
}
