﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.PayPalExpress.Models
{
    public class PayPalAuthorizationModel
    {
        public PayPalAuthorizationModel()
        {
            Errors = new List<string>();
        }
        public bool Success { get; set; }
        public bool Approved { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseReasonCode { get; set; }
        public string ResponseReasonText { get; set; }
        public string RawResponseCode { get; set; }
        public string Token { get; set; }
        public string TransactionId { get; set; }
        public string SecureUrl { get; set; }
        public string MerchantId { get; set; }
        public int SiteId { get; set; }
        public int CustomerId { get; set; }
        public decimal Amount { get; set; }
        public string PayerId { get; set; }
        public IList<string> Errors { get; set; }

        public void AddError(string error)
        {
            Errors.Add(error);
        }
    }
}
