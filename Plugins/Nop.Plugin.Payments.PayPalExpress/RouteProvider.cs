﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Payments.PayPalExpress
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Payments.PayPalExpress.Configure",
                 "Plugins/PaymentPayPalExpress/Configure",
                 new { controller = "PaymentPayPalExpress", action = "Configure" },
                 new[] { "Nop.Plugin.Payments.PayPalExpress.Controllers" }
            );

            routes.MapRoute("Plugin.Payments.PayPalExpress.PaymentInfo",
                 "Plugins/PaymentPayPalExpress/PaymentInfo",
                 new { controller = "PaymentPayPalExpress", action = "PaymentInfo" },
                 new[] { "Nop.Plugin.Payments.PayPalExpress.Controllers" }
            );

            routes.MapRoute("Plugin.Payments.PayPalExpress.PaypalAuthorizeSuccess",
                 "Plugins/PaymentPayPalExpress/PayPalAuthorizeSuccess/{siteId}/{customerId}/{orderTotal}",
                 new { controller = "PaymentPayPalExpress", action = "PayPalAuthorizeSuccess" },
                 new[] { "Nop.Plugin.Payments.PayPalExpress.Controllers" }
            );

            routes.MapRoute("Plugin.Payments.PayPalExpress.PaypalAuthorizeCancel",
                 "Plugins/PaymentPayPalExpress/PayPalAuthorizeCancel/{siteId}/{customerId}/{orderTotal}",
                 new { controller = "PaymentPayPalExpress", action = "PayPalAuthorizeCancel" },
                 new[] { "Nop.Plugin.Payments.PayPalExpress.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
