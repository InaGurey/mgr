﻿using System;
using FluentValidation;
using FluentValidation.Results;
using Nop.Plugin.Payments.PayPalExpress.Models;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Payments.PayPalExpress.Validators
{
    public class PaymentInfoValidator : AbstractValidator<PaymentInfoModel>
    {
        public PaymentInfoValidator(ILocalizationService localizationService)
        {
            //useful links:
            //http://fluentvalidation.codeplex.com/wikipage?title=Custom&referringTitle=Documentation&ANCHOR#CustomValidator
            //http://benjii.me/2010/11/credit-card-validator-attribute-for-asp-net-mvc-3/

            RuleFor(x => x.PayerId).NotEmpty().WithMessage("PayPal payment has not been authorized.");
            RuleFor(x => x.TransactionId).NotEmpty().WithMessage("PayPal payment has not been authorized.");
            RuleFor(x => x.Token).NotEmpty().WithMessage("PayPal payment has not been authorized.");
        }

		// Treefort enhancement
		private ValidationFailure ValidateDate(PaymentInfoModel x) {

            if (string.IsNullOrWhiteSpace(x.PayerId))
                return new ValidationFailure("PayerID", "Paypal payment has not been authorized");
            if (string.IsNullOrWhiteSpace(x.TransactionId))
                return new ValidationFailure("PayerID", "Paypal payment has not been authorized");
            if (string.IsNullOrWhiteSpace(x.Token))
                return new ValidationFailure("PayerID", "Paypal payment has not been authorized");

			return null;			
		}
	}
}