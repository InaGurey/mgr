﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.PayPalExpress.Models;
using Nop.Plugin.Payments.PayPalExpress.Validators;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Payments.PayPalExpress.Controllers
{
    public class PaymentPayPalExpressController : BaseNopPaymentController
    {
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly PayPalExpressPaymentSettings _paypalExpressPaymentSettings;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPaymentService _paymentService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWorkContext _workContext;
        private readonly ILogger _logger;
        private readonly HttpContextBase _httpContext;

        public PaymentPayPalExpressController(ISettingService settingService, 
            ILocalizationService localizationService, PayPalExpressPaymentSettings paypalExpressPaymentSettings,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            IPaymentService paymentService,
            IWorkflowMessageService workflowMessageService,
            IWorkContext workContext,
            ILogger logger,
            HttpContextBase httpContext)
        {
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._paypalExpressPaymentSettings = paypalExpressPaymentSettings;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._paymentService = paymentService;
            this._workflowMessageService = workflowMessageService;
            this._workContext = workContext;
            this._logger = logger;
            this._httpContext = httpContext;
        }
        
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new ConfigurationModel();
            model.UseSandbox = _paypalExpressPaymentSettings.UseSandbox;
            model.UseStoreCredentials = _paypalExpressPaymentSettings.UseStoreCredentials;
            model.TransactModeId = Convert.ToInt32(_paypalExpressPaymentSettings.TransactMode);
            model.TransactionKey = _paypalExpressPaymentSettings.TransactionKey;
            model.LoginId = _paypalExpressPaymentSettings.LoginId;
            model.PayPalMerchantId = _paypalExpressPaymentSettings.PayPalMerchantId;
            model.AdditionalFee = _paypalExpressPaymentSettings.AdditionalFee;
            model.TransactModeValues = _paypalExpressPaymentSettings.TransactMode.ToSelectList();
            
            return View("Nop.Plugin.Payments.PayPalExpress.Views.PaymentPayPalExpress.Configure", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //save settings
            _paypalExpressPaymentSettings.UseSandbox = model.UseSandbox;
            _paypalExpressPaymentSettings.UseStoreCredentials = model.UseStoreCredentials;
            _paypalExpressPaymentSettings.TransactMode = (TransactMode)model.TransactModeId;
            _paypalExpressPaymentSettings.TransactionKey = model.TransactionKey;
            _paypalExpressPaymentSettings.LoginId = model.LoginId;
            _paypalExpressPaymentSettings.PayPalMerchantId = model.PayPalMerchantId;
            _paypalExpressPaymentSettings.AdditionalFee = model.AdditionalFee;
            _settingService.SaveSetting(_paypalExpressPaymentSettings);
            
            model.TransactModeValues = _paypalExpressPaymentSettings.TransactMode.ToSelectList();

            return View("Nop.Plugin.Payments.PayPalExpress.Views.PaymentPayPalExpress.Configure", model);
        }

        [ChildActionOnly]
        public ActionResult PaymentInfo(int customerId, int siteId, decimal orderTotal, string transactionId, string merchantId, string token, string payerId)
        {
            var model = new PaymentInfoModel();
            model.SandboxMode = _paypalExpressPaymentSettings.UseSandbox;

            // Skip authorization request if token already present
            if (!string.IsNullOrWhiteSpace(transactionId) && !string.IsNullOrWhiteSpace(merchantId) &&
                !string.IsNullOrWhiteSpace(token))
            {
                model.TransactionId = transactionId;
                model.MerchantId = merchantId;
                model.Token = token;
                model.PayerId = payerId;

                return View("Nop.Plugin.Payments.PayPalExpress.Views.PaymentPayPalExpress.PaymentInfo", model);
            }

            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.PayPalExpress") as PayPalExpressPaymentProcessor;
            if (processor == null) throw new Exception("PayPal Express Checkout failed to load.");

            var request = new ProcessPaymentRequest()
            {
                CustomerId = customerId,
                SiteId = siteId,
                OrderTotal = orderTotal
            };

            // TODO: Use setting to determine method (auth only vs. auth and capture)
            var authResult = processor.AuthorizeAndCapture(request);

            model.MerchantId = authResult.MerchantId;
            if (authResult.Success)
            {
                model.TransactionId = authResult.TransactionId;
                model.Token = authResult.Token;

                if (!string.IsNullOrWhiteSpace(authResult.PayerId))
                {
                    var paymentDetails = processor.GetPaymentDetails(authResult.TransactionId, authResult.PayerId,
                        authResult.SiteId, customerId);
                    model.PayerId = authResult.PayerId;
                    model.Approved = paymentDetails.Success;
                }
            }
            else
            {
                foreach (var error in authResult.Errors)
                {
                    _logger.Error(string.Format("PayPal Express Error: {0}", error));
                }
                model.Error = authResult.Errors.FirstOrDefault();
            }
            var viewName = model.Approved
                ? "Nop.Plugin.Payments.PayPalExpress.Views.PaymentPayPalExpress.Approved"
                : "Nop.Plugin.Payments.PayPalExpress.Views.PaymentPayPalExpress.PaymentInfo";
            return View(viewName, model);
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var validator = new PaymentInfoValidator(_localizationService);
            var model = new PaymentInfoModel()
            {
                PayerId = form["PayerId"],
                TransactionId = form["TransactionId"],
                Token = form["Token"]
            };
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                foreach (var error in validationResult.Errors)
                    warnings.Add(error.ErrorMessage);
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            paymentInfo.PayPalTransactionId = form["TransactionId"];
            paymentInfo.PaypalPayerId = form["PayerId"];
            paymentInfo.PaypalToken = form["Token"];
            paymentInfo.PayPalMerchantId = form["MerchantId"];
            return paymentInfo;
        }

        public ActionResult PayPalAuthorizeSuccess(int siteId, int customerId, decimal orderTotal, string token, string payerId)
        {
            var processor = _paymentService.LoadPaymentMethodBySystemName("Payments.PayPalExpress") as PayPalExpressPaymentProcessor;
            if (processor == null) throw new Exception("PayPal Express Checkout failed to load.");

            var authorization = processor.GetAuthorizationDetailsFromCache(siteId, customerId, orderTotal);
            authorization.PayerId = payerId;
            processor.SavePayerId(payerId, siteId, customerId, orderTotal);
            var paymentInfo = new ProcessPaymentRequest
            {
                PayPalTransactionId = authorization.TransactionId,
                PayPalMerchantId = authorization.MerchantId,
                PaypalToken = authorization.Token,
                PaypalPayerId = authorization.PayerId
            };

            // TODO: Use redis cache instead of session
            _httpContext.Session["OrderPaymentInfo"] = paymentInfo;
            return RedirectToAction("Confirm", "Checkout", new {jumpToSection = true});

        }

        public ActionResult PayPalAuthorizeCancel(int siteId, int customerId, decimal orderTotal, string token)
        {
            return RedirectToAction("PaymentMethod", "Checkout", new { jumpToSection = true });
        }
    }
}