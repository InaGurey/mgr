--These indexes have needed some tweaking based on suggest_drop_create_indexes script.
--I've been careful not to just blindly created all those suggested. It's not an excact
--science, and I've tried to avoid over-indexing. (TM)

use mgr_store

DROP INDEX IX_Product_Visible ON Product
GO
CREATE NONCLUSTERED INDEX IX_Product_Visible 
ON [Product] ([SiteId], [Published])
INCLUDE ([Id], [CreatedOnUtc], [Name])
WHERE Deleted <> 1
WITH FILLFACTOR = 90


DROP INDEX IX_ProductVariant_Visible ON ProductVariant
GO
CREATE NONCLUSTERED INDEX IX_ProductVariant_Visible 
ON [ProductVariant] ([AvailableStartDateTimeUtc], [AvailableEndDateTimeUtc])
INCLUDE ([Id], [ProductId], [Price], [SpecialPrice], [SpecialPriceStartDateTimeUtc], [SpecialPriceEndDateTimeUtc])
WHERE Published = 1
AND Deleted <> 1
WITH FILLFACTOR = 90
