-- http://sqlserverperformance.wordpress.com/2011/01/06/sql-server-index-tuning-for-mere-mortals
use mgr_store

-- Candidates for dropping (writes > reads)
-- Look for indexes with high numbers of writes and zero or a very low numbers of reads.
-- Consider your complete workload. Investigate further before dropping an index.
SELECT OBJECT_NAME(s.object_id) + '.' + i.name as [Index],
SUM(user_updates) AS [Total Writes],
SUM(user_seeks + user_scans + user_lookups) AS [Total Reads],
SUM(user_updates - (user_seeks + user_scans + user_lookups)) AS [Difference],
'USE ' + DB_NAME() + '; DROP INDEX ' + i.name + ' ON ' + OBJECT_NAME(s.[object_id]) AS DropScript
FROM sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
INNER JOIN sys.indexes AS i WITH (NOLOCK)
	ON s.[object_id] = i.[object_id]
	AND i.index_id = s.index_id
WHERE OBJECTPROPERTY(s.[object_id],'IsUserTable') = 1
AND is_primary_key = 0
AND user_updates > (user_seeks + user_scans + user_lookups)
AND i.index_id > 1
GROUP BY s.object_id, i.name
ORDER BY [Difference] DESC, [Total Writes] DESC, [Total Reads] ASC OPTION (RECOMPILE);

-- Candidates for new indexes.
-- Look at last user seek time, number of user seeks to help determine source and importance.
-- SQL Server is overly eager to add included columns, so beware.
-- http://msdn.microsoft.com/en-us/library/ms345421.aspx, http://msdn.microsoft.com/en-us/library/ms345405.aspx
-- Use  the following guidelines for ordering columns in the CREATE INDEX statements you write from the missing indexes feature component output:
--  * List the equality columns first (leftmost in the column list).
--  * List the inequality columns after the equality columns (to the right of equality columns listed).
--  * List the include columns in the INCLUDE clause of the CREATE INDEX statement.
--  * To determine an effective order for the equality columns, order them based on their selectivity; that is, list the most selective columns first.

SELECT
  IndexAdvantage = user_seeks * avg_total_user_cost * (avg_user_impact * 0.01),
  migs.user_seeks,
  migs.last_user_seek, 
  migs.avg_total_user_cost,
  migs.avg_user_impact,
  CreateIndex =
	'USE ' + DB_NAME(mid.database_id) + '; ' +
    'CREATE NONCLUSTERED INDEX IX_' +
    OBJECT_NAME(mid.object_id, mid.database_id) + '_' +
    REPLACE(REPLACE(REPLACE(CASE
		WHEN mid.equality_columns IS NULL THEN mid.inequality_columns
		WHEN mid.inequality_columns IS NULL THEN mid.equality_columns
		ELSE mid.equality_columns + '_' + mid.inequality_columns
	END, '], [', '_'), '[', ''), ']', '') +
    ' ON [' + OBJECT_NAME(mid.object_id, mid.database_id) + '] ' +
    '(' + CASE
		WHEN mid.equality_columns IS NULL THEN mid.inequality_columns
		WHEN mid.inequality_columns IS NULL THEN mid.equality_columns
		ELSE mid.equality_columns + ', /* inequality: */ ' + mid.inequality_columns
	END + ')' +
    ISNULL(' INCLUDE (' + mid.included_columns + ')', '') +
    ' WITH FILLFACTOR = 90',
  [Table] = mid.[statement],
  mid.equality_columns,
  mid.inequality_columns,
  mid.included_columns
FROM sys.dm_db_missing_index_group_stats AS migs WITH (NOLOCK)
INNER JOIN sys.dm_db_missing_index_groups AS mig WITH (NOLOCK)
  ON migs.group_handle = mig.index_group_handle
INNER JOIN sys.dm_db_missing_index_details AS mid WITH (NOLOCK)
  ON mig.index_handle = mid.index_handle
--WHERE last_user_seek > DATEADD(MINUTE, -60, GETDATE())
ORDER BY IndexAdvantage DESC OPTION (RECOMPILE);
