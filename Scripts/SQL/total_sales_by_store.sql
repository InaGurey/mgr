SELECT
	StoreNumber AS Store,
	COUNT(*) AS Orders,
	SUM(OrderTotal) AS Sales

FROM [Order]
WHERE (PaymentStatusId=30 OR PaymentStatusId=35 OR PaymentStatusId=40)
AND DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), PaidDateUtc) BETWEEN '2013-09-30' AND '2013-10-30'

GROUP BY StoreNumber