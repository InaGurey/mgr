param ($buildDir)
	Add-Type -AssemblyName System.Core

Write-Output 'Cleaning up old builds.'

if (Test-Path $buildDir) {
	$allBuildSubDirs = ls $buildDir | ?{ $_.PSIsContainer } | sort creationtime -desc

    $all = @($allBuildSubDirs | select -ExpandProperty FullName)
	$lastTwoGood = @($allBuildSubDirs | where {$_.Name -like '*-good'} | select -first 2 -ExpandProperty FullName)
	$lastFive = @($allBuildSubDirs | where {$_.Name -notlike '*-good'} | select -first (5 - $lastTwoGood.length) -ExpandProperty FullName)

	$lastFiveSet = new-object 'System.Collections.Generic.HashSet[string]' (,[string[]]$lastFive)
	$lastTwoGoodSet = new-object 'System.Collections.Generic.HashSet[string]' (,[string[]]$lastTwoGood)
	$allSet = new-object 'System.Collections.Generic.HashSet[string]' (,[string[]]$all)

	$allSet.ExceptWith($lastFiveSet)
	$allSet.ExceptWith($lastTwoGoodSet)

	if ($allSet.count -gt 0) {
		$allSet | del -recurse
	}
}
