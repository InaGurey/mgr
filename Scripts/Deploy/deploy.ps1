param($webRootSource, $configSource, $dbconn, $robocopySwitches, $destinations)

function RemoveAppOfflineFiles($destinations) {
	Write-Output "Bringing application online."
	foreach ($dest in $destinations)  {
		$appOfflinePath = "$dest\app_offline.htm"
		if (Test-Path $appOfflinePath) {
    		Remove-Item $appOfflinePath
			if (-not $?) {
				$onlineFailed = $TRUE
			    Write-Warning "$appOfflinePath could not be removed. Application is OFFLINE."
            }
		}
	}
	if (-not $onlineFailed) {
		Write-Output "Application is online."
	}
}

# I think we can skip unzipping if it looks like it's already been done
if ((Test-Path "files.7z") -and -not (Test-Path $webRootSource)) {
	Write-Output "Unzipping files."
	& 7z.exe x files.7z -aoa -y
	if (-not $?) {
		Write-Error "Unzipping files FAILED. Aborting deploy."
		Return
	}
}

if (-not (Test-Path "$webRootSource\Administration")) {
	Write-Error "Admin build directory missing. Aborting deploy."
	Return
}

$appOfflineSource = "$webRootSource\_app_offline.htm"
$hasAppOffline = Test-Path $appOfflineSource

if ($hasAppOffline) {
	Write-Output "Taking application offline."
	foreach ($dest in $destinations)  {
		cp $appOfflineSource "$dest\app_offline.htm"
		if (-not $?) {
			Write-Error "Copying to $dest FAILED. Aborting deploy."
			RemoveAppOfflineFiles $destinations
			Return
		}
	}
	Write-Output "Application is offline."
	$offlineDelay = 3
	Write-Output "Pausing $offlineDelay seconds for requests to finish."
	Start-Sleep -s $offlineDelay
} else {
	Write-Warning "It would be better to use app_offline.htm files or switch the site home directory."
}

if ($dbconn -and (Test-Path "Database\migrate.exe")) {
	$migrationAssemblies = @(ls "Database\*Migrations.dll")
	if ($migrationAssemblies.Count -eq 0) {
		Write-Warning "No database migration assemblies found."
	} elseif ($migrationAssemblies.Count -gt 1) {
		Write-Error "Multiple database migration assemblies found. Unable to cope. Aborting deploy."
		RemoveAppOfflineFiles $destinations
		Return
	} else {
		Write-Output "Migrating database."
		cp "Config\$configSource\conn*.config" "Database"
		Database\migrate.exe -a Database\$($migrationAssemblies[0].Name) -db SqlServer2008 -c "$dbconn" --verbose true
		if (-not $?) {
			Write-Error "Database migration FAILED. Aborting deploy."
			RemoveAppOfflineFiles $destinations
			Return
		}
		Write-Output "Database has been migrated."
	}
} else {
	Write-Warning "No database connection supplied for migrations."
}

foreach ($dest in $destinations)  {
	Write-Output "Copying application build files."
	robocopy $webRootSource $dest /mir /nfl /ndl /njh /w:1 /r:2 /xf app_offline.htm $robocopySwitches.Split(" ")
	$webCopyFailed = $LASTEXITCODE -ge 8  #http://ss64.com/nt/robocopy-exit.html
	
	Write-Output "Copying application configuration files."
	cp "Config\$configSource\*.config" "$dest" -recurse
	$configCopyFailed = -not $?
	
	if ($webCopyFailed -or $configCopyFailed) {
		if ($hasAppOffline) {
			$appStateText = "OFFLINE"
		} else {
			$appStateText = "in an indeterminate state"
		}
		Write-Error ("Copying deployment files to $dest failed. Application is " + $appStateText + ".")
		Return
	}
}
Write-Output "Application files deployed."

$onlineDelay = 1
Write-Output "Pausing $onlineDelay seconds for the dust to settle."
Start-Sleep -s $onlineDelay

RemoveAppOfflineFiles $destinations

# TODO: Don't say complete if removing the app offline files failed
Write-Output 'Deployment complete!'
