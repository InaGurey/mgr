param ($app, $branch, $builds_dir)

  $app_and_branch = "$($app)$($branch)"
  $app_builds_dir = "$builds_dir\$app_and_branch"
  $build_dir_name = (gci "$app_builds_dir" | sort creationtime -desc)[0].Name
  $build_dir = "$builds_dir\$app_and_branch\$build_dir_name"
  $temp_dir = "$build_dir\_push"

  if (Test-Path $temp_dir) { del $temp_dir -recurse }
  md $temp_dir

  & 7z.exe a "$temp_dir\files.7z" "$build_dir\*\*"
  cp "$build_dir\Scripts\Deploy\*.*" $temp_dir -Exclude "deploy-*.cmd"
  cp "$build_dir\Scripts\Deploy\deploy-cloud$branch*.cmd" $temp_dir

  winscp /command "option batch abort" "option confirm off" "open winmark-db" "put $temp_dir /Builds/$app_and_branch/$build_dir_name" "exit"
  
  del $temp_dir -recurse
