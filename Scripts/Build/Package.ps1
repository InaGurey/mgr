$framework = '4.0x64'

properties {
	$base_dir = resolve-path ..\..
	$date = get-date -f yyyy.MM.dd
	$build_num = ([int]$build_num).ToString("0000")
	$folder = "$date.$build_num.$rev_num"
	$project_builds_dir = "$builds_dir\$app$branch"
	$release_dir = "$project_builds_dir\$folder"
	$package_dir = "$base_dir\_packaged"
}

task Package {
}

task Assemble -depends Package {
	New-Item -ItemType directory -Path "$release_dir\Web"
	cp "$base_dir\Deployable\nop_2.65\*" "$release_dir\Web" -recurse
	cp "$base_dir\Presentation\Nop.Web\Plugins" "$release_dir\Web\Plugins" -recurse
	cp "$base_dir\_config" "$release_dir\Config" -recurse
	cp "$base_dir\Scripts" "$release_dir\Scripts" -recurse
	cp "$base_dir\Scripts\Deploy\*.*" "$release_dir" -Exclude "deploy-*.cmd"
	cp "$base_dir\Scripts\Deploy\deploy-staging*.cmd" "$release_dir"
	cp "$base_dir\MgrStore.Migrations\bin\Release" "$release_dir\Database" -recurse
	cp "$base_dir\packages\FluentMigrator.*\tools\Migrate.exe" "$release_dir\Database"
}

task Cleanup -depends Assemble {
	gci "$project_builds_dir" | sort creationtime -desc | select -skip 5 | del -recurse
}

task default -depends Cleanup
